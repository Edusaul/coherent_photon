//
// Created by eduardo on 12/07/19.
//

#include <Nucleus_FF_DeVries.h>
#include <dsdEg_dphig_dthg_dth.h>
#include <Hadronic_Current_R_D1910.h>
#include <dsdThG_dEg_dphig.h>
#include <dsdThG_dEg.h>
#include <dsdThGamma.h>
#include "Admin_of_Clases_D1910_dThg.h"

Admin_of_Clases_D1910_dThg::Admin_of_Clases_D1910_dThg(Arguments_base *parameters) : Admin_of_Clases_base(parameters) {
    this->nuclearFF = new Nucleus_FF_DeVries(this->parameters->getNucleus());
//    this->nuclearFF = new Nuclear_FF_HO;

    this->current_R = new Hadronic_Current_R_D1910(this->nuclearFF);
    this->create_dsdThg_clases();
}

Admin_of_Clases_D1910_dThg::~Admin_of_Clases_D1910_dThg() {
    this->delete_dsdThg_clases();
    delete this->nuclearFF;
}