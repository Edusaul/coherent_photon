//
// Created by eduardo on 5/07/19.
//

#include <Nucleus_FF_DeVries.h>
#include <Hadronic_Current_R_D1910.h>
#include <dsdEg_dphig_dthg_dth.h>
#include <dsdEg_dphig.h>
#include <dsdEg.h>
#include <dsdEg_dphig_dthg.h>
#include "Admin_of_Clases_D1910.h"

Admin_of_Clases_D1910::Admin_of_Clases_D1910(Arguments_base *parameters) : Admin_of_Clases_base(parameters) {
    this->nuclearFF = new Nucleus_FF_DeVries(this->parameters->getNucleus());
//    this->nuclearFF = new Nuclear_FF_HO;
    this->current_R = new Hadronic_Current_R_D1910(this->nuclearFF);
    this->create_dsdEg_clases();

}

Admin_of_Clases_D1910::~Admin_of_Clases_D1910() {
    this->delete_dsdEg_clases();
    delete this->nuclearFF;
}
