//
// Created by edusaul on 10/04/19.
//

#include <Parameters_GeV.h>
#include <IntegralGauss20.h>
#include <iostream>
#include "dsdEg_dphig_dthg.h"

dsdEg_dphig_dthg::dsdEg_dphig_dthg(Integrable_w_change_param<double> *dsdEgDphigDthgDth, double kappaM)
        : dsdEg_dphig_dthg_dth(dsdEgDphigDthgDth), kappa_m(kappaM) {
    this->th_min = 0.0;
    this->th_max = Param::pi;
    this->npts = 1;
}

dsdEg_dphig_dthg::dsdEg_dphig_dthg(Integrable_w_change_param<double> *dsdEgDphigDthgDth, int npts, double kappaM)
        : dsdEg_dphig_dthg_dth(dsdEgDphigDthgDth), npts(npts), kappa_m(kappaM) {
    this->th_min = 0.0;
    this->th_max = Param::pi;
}

dsdEg_dphig_dthg::dsdEg_dphig_dthg(Integrable_w_change_param<double> *dsdEgDphigDthgDth) : dsdEg_dphig_dthg_dth(
        dsdEgDphigDthgDth) {
    this->th_min = 0.0;
    this->th_max = Param::pi;
    this->npts = 1;
    this->kappa_m=0.0;
}

dsdEg_dphig_dthg::dsdEg_dphig_dthg(Integrable_w_change_param<double> *dsdEgDphigDthgDth, int npts)
        : dsdEg_dphig_dthg_dth(dsdEgDphigDthgDth), npts(npts) {
    this->th_min = 0.0;
    this->th_max = Param::pi;
    this->kappa_m=0.0;
}

double dsdEg_dphig_dthg::integrand(double thg) {
    std::vector<double> param = {this->k0, this->k0g, this->phig, thg};
    this->dsdEg_dphig_dthg_dth->change_other_parameters(param);

    if(this->kappa_m!=0.0) {
        this->th_max = Param::pi;
        double aux_l2 = acos(-this->kappa_m/this->kp0 + (this->k0g/this->kp0)*(1.0-cos(thg)) + 1.0);

        double l3_one_part =  (this->k0g/this->kp0) * sin(thg) * cos(this->phig);
        double l3_minor_lim = -this->kappa_m/this->kp0 - l3_one_part;
        double l3_mayor_lim = this->kappa_m/this->kp0 - l3_one_part;
        double l3min = asin(l3_minor_lim);
        double l3max = asin(l3_mayor_lim);

        if(aux_l2 < Param::pi){
            this->th_max = aux_l2;
        }

        if(l3min > this->th_min) this->th_min = l3min;
        if(l3max < this->th_max) this->th_max = l3max;
    }

    auto ds = IntegralGauss20::integrate<double>(this->dsdEg_dphig_dthg_dth, this->th_min, this->th_max, this->npts);
    return ds * sin(thg);
}

void dsdEg_dphig_dthg::change_other_parameters(std::vector<double>k0_k0g_phig) {
    this->k0 = k0_k0g_phig[0];
    this->k0g = k0_k0g_phig[1];
    this->phig = k0_k0g_phig[2];

    this->kp0 = this->k0 - this->k0g;
}




