//
// Created by edusaul on 10/04/19.
//

#include <Parameters_GeV.h>
#include <IntegralGauss20.h>
#include "dsdEg.h"

dsdEg::dsdEg(Integrable_w_change_param<double> *dsdEgPhig) : dsdEg_phig(dsdEgPhig) {
    double e2 = 4.0 * Param::pi * Param::alpha;
    this->constant_factors = Param::Gf2 * e2 / (2.0 * 8.0 * pow(2.0*Param::pi,4) );
    this->from_GeV2_to_cm2 = Param::hccm2;
    this->phig_min = -Param::pi;
    this->phig_max = Param::pi;
    this->npts = 1;
}

dsdEg::dsdEg(Integrable_w_change_param<double> *dsdEgPhig, int npts) : dsdEg_phig(dsdEgPhig), npts(npts) {
    double e2 = 4.0 * Param::pi * Param::alpha;
    this->constant_factors = Param::Gf2 * e2 / (2.0 * 8.0 * pow(2.0*Param::pi,4) );
    this->from_GeV2_to_cm2 = Param::hccm2;
    this->phig_min = -Param::pi;
    this->phig_max = Param::pi;
}

double dsdEg::integrand(double k0g) {
    double kp0 = this->k0 - k0g;
    double factors = k0g * kp0 / this->k0;

    std::vector<double> param = {this->k0, k0g};
    this->dsdEg_phig->change_other_parameters(param);

    auto ds = IntegralGauss20::integrate<double>(this->dsdEg_phig, this->phig_min, this->phig_max, this->npts);
    return ds * factors * this->constant_factors * this->from_GeV2_to_cm2;
}

void dsdEg::change_other_parameters(std::vector<double> k0) {
    this->k0 = k0[0];
}
