//
// Created by edusaul on 10/04/19.
//

#ifndef COHERENT_PHOTON_DSDEG_DPHIG_H
#define COHERENT_PHOTON_DSDEG_DPHIG_H

#include "Integrable_w_change_param.h"

class dsdEg_dphig : public Integrable_w_change_param<double> {
protected:
    Integrable_w_change_param<double> *dsdEg_dphig_thg;
    double thg_min;
    double thg_max;
    int npts;

    double k0;
    double k0g;

    double kappa_m;

public:
    dsdEg_dphig(Integrable_w_change_param<double> *dsdEgDphigThg, double kappaM);

    dsdEg_dphig(Integrable_w_change_param<double> *dsdEgDphigThg, int npts, double kappaM);

    explicit dsdEg_dphig(Integrable_w_change_param<double> *dsdEgDphigThg);

    dsdEg_dphig(Integrable_w_change_param<double> *dsdEgDphigThg, int npts);

    double integrand(double) override;

    void change_other_parameters(std::vector<double>) override;
};


#endif //COHERENT_PHOTON_DSDEG_DPHIG_H
