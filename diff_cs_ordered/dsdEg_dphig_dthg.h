//
// Created by edusaul on 10/04/19.
//

#ifndef COHERENT_PHOTON_DSDEG_DPHIG_DTHG_H
#define COHERENT_PHOTON_DSDEG_DPHIG_DTHG_H

#include <Integrable_w_change_param.h>

class dsdEg_dphig_dthg : public Integrable_w_change_param<double> {
protected:
    Integrable_w_change_param<double> *dsdEg_dphig_dthg_dth;
    double th_min;
    double th_max;
    int npts;

    double k0;
    double k0g;
    double phig;
    double kp0;

    double kappa_m;

public:
    dsdEg_dphig_dthg(Integrable_w_change_param<double> *dsdEgDphigDthgDth, double kappaM);

    dsdEg_dphig_dthg(Integrable_w_change_param<double> *dsdEgDphigDthgDth, int npts, double kappaM);

    explicit dsdEg_dphig_dthg(Integrable_w_change_param<double> *dsdEgDphigDthgDth);

    dsdEg_dphig_dthg(Integrable_w_change_param<double> *dsdEgDphigDthgDth, int npts);

    double integrand(double) override;

    void change_other_parameters(std::vector<double>) override;
};


#endif //COHERENT_PHOTON_DSDEG_DPHIG_DTHG_H
