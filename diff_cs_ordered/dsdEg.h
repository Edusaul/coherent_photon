//
// Created by edusaul on 10/04/19.
//

#ifndef COHERENT_PHOTON_DSDEG_H
#define COHERENT_PHOTON_DSDEG_H

#include "Integrable_w_change_param.h"


class dsdEg : public Integrable_w_change_param<double> {
protected:
    Integrable_w_change_param<double> *dsdEg_phig;
    double constant_factors;
    double from_GeV2_to_cm2;
    double k0;
    double phig_min;
    double phig_max;
    int npts;

public:
    explicit dsdEg(Integrable_w_change_param<double> *dsdEgPhig);

    dsdEg(Integrable_w_change_param<double> *dsdEgPhig, int npts);

    double integrand(double) override;

    void change_other_parameters(std::vector<double>) override;

};


#endif //COHERENT_PHOTON_DSDEG_H
