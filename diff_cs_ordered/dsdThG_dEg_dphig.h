//
// Created by edusaul on 10/04/19.
//

#ifndef COHERENT_PHOTON_DSDTHG_DEG_DPHIG_H
#define COHERENT_PHOTON_DSDTHG_DEG_DPHIG_H

#include "Integrable_w_change_param.h"

class dsdThG_dEg_dphig  : public Integrable_w_change_param<double> {
protected:
    Integrable_w_change_param<double> *dsdEg_dphig_dthg_dth;
    double th_min;
    double th_max;
    int npts;

    double k0;
    double k0g;
    double thg;

public:
    explicit dsdThG_dEg_dphig(Integrable_w_change_param<double> *dsdEgDphigDthgDth);

    dsdThG_dEg_dphig(Integrable_w_change_param<double> *dsdEgDphigDthgDth, int npts);

    double integrand(double) override;

    void change_other_parameters(std::vector<double>) override;

};


#endif //COHERENT_PHOTON_DSDTHG_DEG_DPHIG_H
