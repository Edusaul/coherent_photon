//
// Created by edusaul on 10/04/19.
//

#ifndef COHERENT_PHOTON_DSDEG_DPHIG_DTHG_DTH_H
#define COHERENT_PHOTON_DSDEG_DPHIG_DTHG_DTH_H

#include <Integrable_w_change_param.h>
#include <dsdEgamma_dth_dthg_dphig.h>
#include "Hadronic_Current_R.h"

class dsdEg_dphig_dthg_dth : public Integrable_w_change_param<double> {
protected:
    std::string mode;

    dsdEgamma_dth_dthg_dphig *dsdEg_dth_dthg_dphig;

    double k0;
    double k0g;
    double phig;
    double thg;
    double kappa_m;

    Hadronic_Current_R *current_R;

public:
    dsdEg_dphig_dthg_dth(std::string mode, Hadronic_Current_R *currentR);

    dsdEg_dphig_dthg_dth(std::string mode, Hadronic_Current_R *currentR, double kappaM);

    virtual ~dsdEg_dphig_dthg_dth();

    double integrand(double) override;

    void change_other_parameters(std::vector<double>) override;
};


#endif //COHERENT_PHOTON_DSDEG_DPHIG_DTHG_DTH_H
