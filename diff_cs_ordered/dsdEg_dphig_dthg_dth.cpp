#include <utility>

#include <utility>

//
// Created by edusaul on 10/04/19.
//

#include "dsdEg_dphig_dthg_dth.h"

dsdEg_dphig_dthg_dth::dsdEg_dphig_dthg_dth(std::string mode, Hadronic_Current_R *currentR) : mode(std::move(mode)),
                                                                                                    current_R(
                                                                                                            currentR) {
    this->dsdEg_dth_dthg_dphig = new dsdEgamma_dth_dthg_dphig(this->mode, this->current_R);
}

dsdEg_dphig_dthg_dth::dsdEg_dphig_dthg_dth(std::string mode, Hadronic_Current_R *currentR, double kappaM) : mode(std::move(
        mode)), current_R(currentR), kappa_m(kappaM) {
    this->dsdEg_dth_dthg_dphig = new dsdEgamma_dth_dthg_dphig(this->mode, this->current_R, this->kappa_m);
}

double dsdEg_dphig_dthg_dth::integrand(double th) {

    std::vector<double> param(4);
    param[0] = this->k0;
    param[1] = this->k0g;
    param[2] = th;
    param[3] = this->thg;
    this->dsdEg_dth_dthg_dphig->change_other_parameters(param);
    double ds = this->dsdEg_dth_dthg_dphig->integrand(this->phig);

    return ds * sin(th);
}

void dsdEg_dphig_dthg_dth::change_other_parameters(std::vector<double> k0_k0g_phig_thg) {
    this->k0 = k0_k0g_phig_thg[0];
    this->k0g = k0_k0g_phig_thg[1];
    this->phig = k0_k0g_phig_thg[2];
    this->thg = k0_k0g_phig_thg[3];
}

dsdEg_dphig_dthg_dth::~dsdEg_dphig_dthg_dth() {
    delete this->dsdEg_dth_dthg_dphig;
}


