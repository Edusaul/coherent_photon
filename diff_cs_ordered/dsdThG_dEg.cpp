//
// Created by edusaul on 10/04/19.
//

#include <Parameters_GeV.h>
#include <IntegralGauss20.h>
#include "dsdThG_dEg.h"

dsdThG_dEg::dsdThG_dEg(Integrable_w_change_param<double> *dsdthgDEgDphig) : dsdthg_dEg_dphig(dsdthgDEgDphig) {
    this->phig_min = -Param::pi;
    this->phig_max = Param::pi;
    this->npts = 1;
}


dsdThG_dEg::dsdThG_dEg(Integrable_w_change_param<double> *dsdthgDEgDphig, int npts) : dsdthg_dEg_dphig(dsdthgDEgDphig),
                                                                                      npts(npts) {
    this->phig_min = -Param::pi;
    this->phig_max = Param::pi;
}

double dsdThG_dEg::integrand(double k0g) {
    double kp0 = this->k0 - k0g;
    double factors = k0g * kp0 / this->k0;

    std::vector<double> param = {this->k0, this->thg, k0g};
    this->dsdthg_dEg_dphig->change_other_parameters(param);

    auto ds = IntegralGauss20::integrate<double>(this->dsdthg_dEg_dphig, this->phig_min, this->phig_max, this->npts);

    return ds * factors;
}

void dsdThG_dEg::change_other_parameters(std::vector<double>k0_thg) {
    this->k0 = k0_thg[0];
    this->thg = k0_thg[1];
}




