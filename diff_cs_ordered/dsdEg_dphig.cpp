//
// Created by edusaul on 10/04/19.
//

#include <Parameters_GeV.h>
#include <IntegralGauss20.h>
#include "dsdEg_dphig.h"

dsdEg_dphig::dsdEg_dphig(Integrable_w_change_param<double> *dsdEgDphigThg, double kappaM) : dsdEg_dphig_thg(
        dsdEgDphigThg), kappa_m(kappaM) {
    this->thg_min = 0.0;
    this->thg_max = Param::pi;
    this->npts = 1;
}

dsdEg_dphig::dsdEg_dphig(Integrable_w_change_param<double> *dsdEgDphigThg, int npts, double kappaM) : dsdEg_dphig_thg(
        dsdEgDphigThg), npts(npts), kappa_m(kappaM) {
    this->thg_min = 0.0;
    this->thg_max = Param::pi;
}

dsdEg_dphig::dsdEg_dphig(Integrable_w_change_param<double> *dsdEgDphigThg) : dsdEg_dphig_thg(dsdEgDphigThg) {
    this->thg_min = 0.0;
    this->thg_max = Param::pi;
    this->npts = 1;
    this->kappa_m = 0.0;
}

dsdEg_dphig::dsdEg_dphig(Integrable_w_change_param<double> *dsdEgDphigThg, int npts) : dsdEg_dphig_thg(dsdEgDphigThg),
                                                                                       npts(npts) {
    this->thg_min = 0.0;
    this->thg_max = Param::pi;
    this->kappa_m = 0.0;
}

double dsdEg_dphig::integrand(double phig) {

    std::vector<double> param = {this->k0, this->k0g, phig};
    this->dsdEg_dphig_thg->change_other_parameters(param);

    if(this->kappa_m!=0.0) {
        this->thg_max = Param::pi;
        double auxx = asin(this->kappa_m/(this->k0g*fabs(sin(phig))));
        if(auxx < Param::pi){
            this->thg_max = auxx;
        }
    }

    auto ds = IntegralGauss20::integrate<double>(this->dsdEg_dphig_thg, this->thg_min, this->thg_max, this->npts);
    return ds;
}

void dsdEg_dphig::change_other_parameters(std::vector<double>k0_k0g) {
    this->k0 = k0_k0g[0];
    this->k0g = k0_k0g[1];
}






