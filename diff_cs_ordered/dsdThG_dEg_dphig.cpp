//
// Created by edusaul on 10/04/19.
//

#include <Parameters_GeV.h>
#include <IntegralGauss20.h>
#include "dsdThG_dEg_dphig.h"

dsdThG_dEg_dphig::dsdThG_dEg_dphig(Integrable_w_change_param<double> *dsdEgDphigDthgDth) : dsdEg_dphig_dthg_dth(
        dsdEgDphigDthgDth) {
    this->th_min = 0.0;
    this->th_max = Param::pi;
    this->npts = 1;
}

dsdThG_dEg_dphig::dsdThG_dEg_dphig(Integrable_w_change_param<double> *dsdEgDphigDthgDth, int npts)
        : dsdEg_dphig_dthg_dth(dsdEgDphigDthgDth), npts(npts) {
    this->th_min = 0.0;
    this->th_max = Param::pi;
}

double dsdThG_dEg_dphig::integrand(double phig) {
    std::vector<double> param = {this->k0, this->k0g, phig, this->thg};
    this->dsdEg_dphig_dthg_dth->change_other_parameters(param);
    auto ds = IntegralGauss20::integrate<double>(this->dsdEg_dphig_dthg_dth, this->th_min, this->th_max, this->npts);

    return ds;
}

void dsdThG_dEg_dphig::change_other_parameters(std::vector<double> k0_Thg_k0g) {
    this->k0 = k0_Thg_k0g[0];
    this->thg = k0_Thg_k0g[1];
    this->k0g = k0_Thg_k0g[2];
}
