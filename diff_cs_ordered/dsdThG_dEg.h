//
// Created by edusaul on 10/04/19.
//

#ifndef COHERENT_PHOTON_DSDTHG_DEG_H
#define COHERENT_PHOTON_DSDTHG_DEG_H

#include "Integrable_w_change_param.h"

class dsdThG_dEg : public Integrable_w_change_param<double> {
protected:
    Integrable_w_change_param<double> *dsdthg_dEg_dphig;
    double phig_min;
    double phig_max;
    int npts;

    double k0;
    double thg;

public:
    explicit dsdThG_dEg(Integrable_w_change_param<double> *dsdthgDEgDphig);

    dsdThG_dEg(Integrable_w_change_param<double> *dsdthgDEgDphig, int npts);

    double integrand(double) override;

    void change_other_parameters(std::vector<double>) override;

};


#endif //COHERENT_PHOTON_DSDTHG_DEG_H
