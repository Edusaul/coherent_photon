//
// Created by edusaul on 22/03/19.
//

#ifndef COHERENT_PHOTON_ADMIN_OF_CLASES_DELTA_INT_AVG_H
#define COHERENT_PHOTON_ADMIN_OF_CLASES_DELTA_INT_AVG_H


#include <Admin_of_Clases_base.h>
#include <Delta_in_medium.h>

class Admin_of_Clases_delta_Int_avg : public Admin_of_Clases_base {
protected:
    std::string nucleon;
    Delta_in_medium *Delta;

public:
    explicit Admin_of_Clases_delta_Int_avg(Arguments_base *parameters);

    ~Admin_of_Clases_delta_Int_avg() override;

};


#endif //COHERENT_PHOTON_ADMIN_OF_CLASES_DELTA_AVG_H
