//
// Created by edusaul on 26/03/19.
//

#include <Measure_time.h>
#include <Parameters_GeV.h>
#include <Arguments_d5.h>
#include "Arguments_base.h"
#include "Admin_of_Clases_delta_int_avg.h"

int main(int argc, char** argv) {
    Measure_time time;

    std::string distribution_type = "d5_Delta_avg";

    Arguments_d5 parameters(argc, argv, distribution_type);

    Admin_of_Clases_delta_Int_avg admin(&parameters);

    double kg0 = parameters.getKg0();
    double theta = parameters.getTheta();
    double thetag = parameters.getThetag();
    double phig = parameters.getPhig();

    admin.get_d5s(kg0,theta,thetag,phig);

    time.getTime();
    return 0;
}