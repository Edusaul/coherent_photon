//
// Created by edusaul on 11/04/19.
//

#include <Delta_in_medium_with_Int_avg.h>
#include <Hadronic_Current_R_Delta_with_Int.h>
#include <dsdEg_dphig_dthg_dth.h>
#include <dsdThG_dEg_dphig.h>
#include <dsdThG_dEg.h>
#include <dsdThGamma.h>
#include <Nuclear_FF_HO.h>
#include <Hadronic_Current_R_Delta_avg.h>
#include <Nucleus_FF_DeVries.h>
#include "Admin_of_Clases_delta_avg_dThg.h"

Admin_of_Clases_delta_avg_dThg::Admin_of_Clases_delta_avg_dThg(Arguments_base *parameters) : Admin_of_Clases_base(
        parameters) {
    this->nuclearFF = new Nucleus_FF_DeVries(this->parameters->getNucleus());
//    this->nuclearFF = new Nuclear_FF_HO;

    this->current_R = new Hadronic_Current_R_Delta_avg(this->nuclearFF);
    this->create_dsdThg_clases();
}

Admin_of_Clases_delta_avg_dThg::~Admin_of_Clases_delta_avg_dThg() {
    this->delete_dsdThg_clases();
    delete this->nuclearFF;
}


