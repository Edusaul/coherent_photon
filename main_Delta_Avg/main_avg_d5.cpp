//
// Created by edusaul on 11/04/19.
//

#include <Measure_time.h>
#include <Arguments_d5.h>
#include <iostream>
#include "Arguments_base.h"
#include "Admin_of_Clases_delta_avg.h"
#include <fstream>

int main(int argc, char** argv) {
    Measure_time time;

    std::ofstream file;
    file.open("/home/edusaul/Desktop/d5_Delta_avg.dat");

    std::string distribution_type = "d5_Delta_avg";

    Arguments_d5 parameters(argc, argv, distribution_type);

    Admin_of_Clases_delta_avg admin(&parameters);

//    double kg0 = parameters.getKg0();
    double theta = parameters.getTheta();
    double thetag = parameters.getThetag();
    double phig = parameters.getPhig();

    double kg0_min = 0.0;
    double kg0_max = parameters.getK0();
    double kg0 = kg0_min;


    int npts = 50;

    double d5;

    for (int i = 0; i < npts; ++i) {
        d5 = admin.get_d5s(kg0,theta,thetag,phig);

        file<<kg0<<"  "<<d5<<std::endl;

        kg0 += (kg0_max-kg0_min)/double(npts-1);

    }

    file.close();

    time.getTime();
    return 0;
}