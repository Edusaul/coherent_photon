//
// Created by edusaul on 29/04/19.
//

#include <Measure_time.h>
#include <Arguments_base.h>
#include "Admin_of_Clases_delta_avg.h"

int main(int argc, char** argv) {
    Measure_time time;

    std::string distribution_type = "dsdEg_Delta_avg";

    Arguments_base parameters(argc, argv, distribution_type);

    Admin_of_Clases_delta_avg admin(&parameters);

    admin.write_Integrated_CS();

    time.getTime();
    return 0;
}