//
// Created by edusaul on 17/04/19.
//

#include <Measure_time.h>
#include "Arguments_base.h"
#include "Admin_of_Clases_delta_avg_dThg.h"

int main(int argc, char** argv) {
    Measure_time time;

    std::string distribution_type = "MicroBooNE_dsdthg_Delta_avg";

    Arguments_base parameters(argc, argv, distribution_type);

    Admin_of_Clases_delta_avg_dThg admin(&parameters);

    admin.write_dNdthg_MicroBooNE();

    time.getTime();
    return 0;
}