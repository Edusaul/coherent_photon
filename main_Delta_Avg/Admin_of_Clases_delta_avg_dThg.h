//
// Created by edusaul on 11/04/19.
//

#ifndef COHERENT_PHOTON_ADMIN_OF_CLASES_DELTA_AVG_DTHG_H
#define COHERENT_PHOTON_ADMIN_OF_CLASES_DELTA_AVG_DTHG_H

#include <Admin_of_Clases_base.h>
#include "Nuclear_FF.h"



class Admin_of_Clases_delta_avg_dThg  : public Admin_of_Clases_base {
protected:
    std::string nucleon;
    Nuclear_FF *nuclearFF;


public:
    Admin_of_Clases_delta_avg_dThg(Arguments_base *parameters);

    virtual ~Admin_of_Clases_delta_avg_dThg();

};


#endif //COHERENT_PHOTON_ADMIN_OF_CLASES_DELTA_AVG_DTHG_H
