//
// Created by edusaul on 11/04/19.
//

#ifndef COHERENT_PHOTON_ADMIN_OF_CLASES_DELTA_AVG_H
#define COHERENT_PHOTON_ADMIN_OF_CLASES_DELTA_AVG_H

#include <Admin_of_Clases_base.h>
#include "Nuclear_FF.h"


class Admin_of_Clases_delta_avg  : public Admin_of_Clases_base {
protected:
    std::string nucleon;
    Nuclear_FF *nuclearFF;

public:
    explicit Admin_of_Clases_delta_avg(Arguments_base *parameters);

    ~Admin_of_Clases_delta_avg() override;
};


#endif //COHERENT_PHOTON_ADMIN_OF_CLASES_DELTA_AVG_H
