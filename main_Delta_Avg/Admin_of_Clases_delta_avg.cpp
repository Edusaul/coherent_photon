//
// Created by edusaul on 11/04/19.
//

#include <Delta_in_medium_with_Int_avg.h>
#include <dsdEg_dphig_dthg_dth.h>
#include <dsdEg_dphig_dthg.h>
#include <dsdEg_dphig.h>
#include <dsdEg.h>
#include <Nuclear_FF_HO.h>
#include <Hadronic_Current_R_Delta_avg.h>
#include <Nucleus_FF_DeVries.h>
#include "Admin_of_Clases_delta_avg.h"

Admin_of_Clases_delta_avg::Admin_of_Clases_delta_avg(Arguments_base *parameters) : Admin_of_Clases_base(parameters) {
    this->nuclearFF = new Nucleus_FF_DeVries(this->parameters->getNucleus());
//    this->nuclearFF = new Nuclear_FF_HO;

    this->current_R = new Hadronic_Current_R_Delta_avg(this->nuclearFF);
    this->create_dsdEg_clases();
}

Admin_of_Clases_delta_avg::~Admin_of_Clases_delta_avg() {
    this->delete_dsdEg_clases();
    delete this->nuclearFF;
}
