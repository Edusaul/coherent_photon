//
// Created by edusaul on 22/03/19.
//

#include <Hadronic_Current_R_Delta.h>
#include <dsdEgamma.h>
#include <Nucleus_FF_DeVries.h>
#include <Parameters_GeV.h>
#include <dsdThGamma.h>
#include <Hadronic_Current_R_Delta_approx.h>
#include <dsdEg_dphig_dthg_dth.h>
#include <dsdThGamma_dEg.h>
#include <dsdThG_dEg_dphig.h>
#include <dsdThG_dEg.h>
#include <Nuclear_FF_HO.h>
#include "Admin_of_Clases_delta_thg.h"

Admin_of_Clases_delta_thg::Admin_of_Clases_delta_thg(Arguments_base *parameters) : Admin_of_Clases_base(parameters) {
//    this->nuclearFF = new Nucleus_FF_DeVries(this->parameters->getNucleus());
    this->nuclearFF = new Nuclear_FF_HO;
    this->current_R = new Hadronic_Current_R_Delta_approx(this->nuclearFF);
    this->dsdEgamma_dphig_dthg_dth = new dsdEg_dphig_dthg_dth(this->parameters->getMode(), this->current_R);
    this->dsdthg_dEg_dphig = new dsdThG_dEg_dphig(this->dsdEgamma_dphig_dthg_dth);
    this->dsdthg_dEg = new dsdThG_dEg(this->dsdthg_dEg_dphig);
    this->dsdthg = new dsdThGamma(this->dsdthg_dEg);

    std::vector<double> k0 = {this->k0};
    this->dsdthg->change_other_parameters(k0);
}

Admin_of_Clases_delta_thg::~Admin_of_Clases_delta_thg() {
    delete this->dsdthg;
    delete this->dsdthg_dEg;
    delete this->dsdthg_dEg_dphig;
    delete this->dsdEgamma_dphig_dthg_dth;
    delete this->current_R;
    delete this->nuclearFF;
}
