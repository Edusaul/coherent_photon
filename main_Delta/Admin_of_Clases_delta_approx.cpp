//
// Created by edusaul on 22/03/19.
//


#include <Nucleus_FF_DeVries.h>
#include <Parameters_GeV.h>
#include <iostream>
#include <Nuclear_FF_HO.h>
#include <Hadronic_Current_R_Delta_approx.h>
#include <dsdEg_dphig_dthg_dth.h>
#include <dsdEg_dphig_dthg.h>
#include <dsdEg_dphig.h>
#include <dsdEg.h>
#include "Admin_of_Clases_delta_approx.h"

Admin_of_Clases_delta_approx::Admin_of_Clases_delta_approx(Arguments_base *parameters) : Admin_of_Clases_base(parameters) {
//    this->nuclearFF = new Nucleus_FF_DeVries(this->parameters->getNucleus());
    this->nuclearFF = new Nuclear_FF_HO;
    this->current_R = new Hadronic_Current_R_Delta_approx(this->nuclearFF);

    this->create_dsdEg_clases();

//    this->dsdEgamma_dphig_dthg_dth = new dsdEg_dphig_dthg_dth(this->parameters->getMode(), this->current_R);
//    this->dsdk0_dphig_dthg = new dsdEg_dphig_dthg(this->dsdEgamma_dphig_dthg_dth);
//    this->dsdk0_dphig = new dsdEg_dphig(this->dsdk0_dphig_dthg);
//    this->dsdk0 = new dsdEg(this->dsdk0_dphig);
//
//    std::vector<double> k0 = {this->k0};
//    this->dsdk0->change_other_parameters(k0);
}

Admin_of_Clases_delta_approx::~Admin_of_Clases_delta_approx() {
    this->delete_dsdEg_clases();
    delete this->nuclearFF;
}
