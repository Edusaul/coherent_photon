//
// Created by eduardo on 30/7/19.
//

#ifndef COHERENT_PHOTON_DECAY_WIDTH_N1440_H
#define COHERENT_PHOTON_DECAY_WIDTH_N1440_H


#include "Decay_width.h"

class Decay_width_N1440 : public Decay_width{
protected:

public:

    Decay_width_N1440();

    void PartialWidth(double W) override;

};


#endif //COHERENT_PHOTON_DECAY_WIDTH_N1440_H
