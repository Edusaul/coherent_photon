//
// Created by eduardo on 29/7/19.
//

#include <cmath>
#include <utility>
#include <IntegralGauss20.h>
#include "Decay_width.h"
#include "integral_for_decay_width.h"

Decay_width::Decay_width() {
    this->mRes2 = this->mRes * this->mRes;
//    std::vector<int> vect(this->n_of_integrals, 1);
//    this->npts = vect;
}

Decay_width::Decay_width(std::vector<int> npts) : npts(std::move(npts)) {
    this->mRes2 = this->mRes * this->mRes;
}

Decay_width::~Decay_width() {
    delete this->integral_one_stable;
}
double Decay_width::rho_ab_one_stable(double W, double m_a, double m_b, int L) {
    std::vector<double> inv_mass = {W};
    this->integral_one_stable->change_other_parameters(inv_mass);
//    double integral = IntegralGauss20::integrate<double>(this->integral_one_stable, this->thg_min, this->thg_max, this->npts);
    return m_b/(this->pi * W);
}

double Decay_width::rho_ab_both_stable(double W, double m_a, double m_b, int L) {
    double W2 = W*W;
    if(W - m_a - m_b > 0.0) return pow(cm_momentum(W2, m_a, m_b), 2 * L + 1) / W;
    else return 0.0;
}

double Decay_width::getDecayWidth(double W) {

//    if(this->oneOrTwoStablePart == 1){
//        gamma = this->Gamma_at_MR * rho_ab_one_stable(W) / this->rho_ab_at_MR;
//    } else {
//        gamma = this->Gamma_at_MR * rho_ab_both_stable(W) / this->rho_ab_at_MR;
//    }

    double gamma =0.0;
    for (int i = 0; i < this->channels.size(); ++i) {
        gamma += this->branching[i] * this->partial_widths[i];
    }

    return gamma;
}


double Decay_width::cm_momentum(double W2, double pa2, double pb2) {
    return this->lambda_func(W2, pa2, pb2)/(4.0 * W2);
}

double Decay_width::lambda_func(double x, double y, double z) {
    return x*x + y*y + z*z - 2.0*x*y - 2.0*y*z - 2.0*x*z;
}

void Decay_width::PartialWidth(double W) {
    double rho;
    double rho_MR;
    for (int i = 0; i < this->branching.size(); ++i) {
        rho_MR = this->rho_ab_both_stable(this->mRes, this->mass_values[i][0], this->mass_values[i][1],
                )
    }
}









