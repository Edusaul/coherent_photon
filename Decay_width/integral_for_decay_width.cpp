//
// Created by eduardo on 29/7/19.
//

#include "integral_for_decay_width.h"

integral_for_decay_width::integral_for_decay_width(double mB) : m_b(mB) {

}

double integral_for_decay_width::integrand(double) {
    return 0;
}

void integral_for_decay_width::change_other_parameters(std::vector<double> inv_mass) {
    this->W = inv_mass[0];
}


