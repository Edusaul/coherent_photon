//
// Created by eduardo on 29/7/19.
//

#ifndef COHERENT_PHOTON_INTEGRAL_FOR_DECAY_WIDTH_H
#define COHERENT_PHOTON_INTEGRAL_FOR_DECAY_WIDTH_H

#include <Integrable_w_change_param.h>

class integral_for_decay_width : public Integrable_w_change_param<double>{
protected:

    double m_b;
    double W;

public:
    explicit integral_for_decay_width(double mB);

    double integrand(double) override;

    void change_other_parameters(std::vector<double>) override;
};


#endif //COHERENT_PHOTON_INTEGRAL_FOR_DECAY_WIDTH_H
