//
// Created by eduardo on 30/7/19.
//

#include <Parameters_GeV.h>
#include "Decay_width_N1440.h"

Decay_width_N1440::Decay_width_N1440() {
    this->mRes = Param::mN1440;
    this->Gamma_at_MR = Param::Gamma_N1440;

    this->channels[0][0] = "N";
    this->channels[0][1] = "pi";
    this->mass_values[0][0] = Param::mp;
    this->mass_values[0][1] = Param::mpi;
    this->branching[0] = 0.65;
    this->relative_angular_momenta[0] = 1;
    this->oneOrTwoStablePart[0] = 2;

    this->channels[1][0] = "Delta";
    this->channels[1][1] = "pi";
    this->mass_values[1][0] = Param::mDelta;
    this->mass_values[1][1] = Param::mpi;
    this->branching[1] = 0.20;
    this->relative_angular_momenta[1] = 1;
    this->oneOrTwoStablePart[1] = 1;

    this->channels[2][0] = "N";
    this->channels[2][1] = "sigma";
    this->mass_values[0][0] = Param::mp;
    this->mass_values[0][1] = Param::mSigma_1190;
    this->branching[2] = 0.15;
    this->relative_angular_momenta[2] = 0;
    this->oneOrTwoStablePart[2] = 2;

}

void Decay_width_N1440::PartialWidth(double W) {
    double rho_MR = this->rho_ab_both_stable(this->mRes, this->mass_values[0], this->mass_values[1], this->relative_angular_momenta[0]);
    double rho = this->rho_ab_both_stable(W, this->mass_values[0], this->mass_values[1], this->relative_angular_momenta[0]);
    this->partial_widths[0] = this->Gamma_at_MR * rho/rho_MR;

}
