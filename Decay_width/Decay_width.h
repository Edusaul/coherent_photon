//
// Created by eduardo on 29/7/19.
//

#ifndef COHERENT_PHOTON_DECAY_WIDTH_H
#define COHERENT_PHOTON_DECAY_WIDTH_H


#include <Integrable_w_change_param.h>

class Decay_width {
protected:
    double mRes;
    double mRes2;
    double pi;
    double Gamma_at_MR;

    std::vector<std::vector<std::string> > channels;
    std::vector<std::vector<std::string> > mass_names;
    std::vector<std::vector<double> > mass_values;
    std::vector<double> branching;
    std::vector<int> relative_angular_momenta;
    std::vector<double> partial_widths;
    std::vector<int> oneOrTwoStablePart;

    std::vector<int> npts;

    double rho_ab_one_stable(double W, double m_a, double m_b, int L);
    double rho_ab_both_stable(double W, double m_a, double m_b, int L);
    double rho_ab_at_MR;

    Integrable_w_change_param<double> *integral_one_stable;

public:
    Decay_width();

    Decay_width(std::vector<int> npts);

    virtual ~Decay_width();

    virtual void PartialWidth(double W);

    double getDecayWidth(double W);

    double cm_momentum(double W2, double pa2, double pb2);

    double lambda_func(double x, double y, double z);
};


#endif //COHERENT_PHOTON_DECAY_WIDTH_H
