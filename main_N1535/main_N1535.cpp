//
// Created by eduardo on 14/06/19.
//

#include <Measure_time.h>
#include <string>
#include <Arguments_base.h>
#include "Admin_of_Clases_N1535.h"

int main(int argc, char** argv) {
    Measure_time time;

    std::string distribution_type = "dsdEg_N1535";

    Arguments_base parameters(argc, argv, distribution_type);

    Admin_of_Clases_N1535 admin(&parameters);

    admin.write_dsdEgamma();

    time.getTime();
    return 0;
}