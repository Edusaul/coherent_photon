//
// Created by eduardo on 14/06/19.
//

#ifndef COHERENT_PHOTON_ADMIN_OF_CLASES_N1535_H
#define COHERENT_PHOTON_ADMIN_OF_CLASES_N1535_H

#include <Nuclear_FF.h>
#include <Admin_of_Clases_base.h>

class Admin_of_Clases_N1535 : public Admin_of_Clases_base {
protected:
    Nuclear_FF *nuclearFF;

public:
    explicit Admin_of_Clases_N1535(Arguments_base *parameters);

    ~Admin_of_Clases_N1535() override;
};


#endif //COHERENT_PHOTON_ADMIN_OF_CLASES_N1535_H
