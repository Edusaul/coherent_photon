//
// Created by eduardo on 1/07/19.
//

#ifndef COHERENT_PHOTON_ADMIN_OF_CLASES_N1535_DTHG_H
#define COHERENT_PHOTON_ADMIN_OF_CLASES_N1535_DTHG_H


#include <Nuclear_FF.h>
#include <Admin_of_Clases_base.h>

class Admin_of_Clases_N1535_dThg : public Admin_of_Clases_base {
protected:
    std::string nucleon;
    Nuclear_FF *nuclearFF;

public:
    explicit Admin_of_Clases_N1535_dThg(Arguments_base *parameters);

    virtual ~Admin_of_Clases_N1535_dThg();

};


#endif //COHERENT_PHOTON_ADMIN_OF_CLASES_N1535_DTHG_H
