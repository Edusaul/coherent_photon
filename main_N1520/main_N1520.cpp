//
// Created by edusaul on 28/04/19.
//

#include <Measure_time.h>
#include <Arguments_base.h>
#include "Admin_of_Clases_N1520.h"

int main(int argc, char** argv) {
    Measure_time time;

    std::string distribution_type = "dsdEg_N1520";

    Arguments_base parameters(argc, argv, distribution_type);

    Admin_of_Clases_N1520 admin(&parameters);

    admin.write_dsdEgamma();

    time.getTime();
    return 0;
}