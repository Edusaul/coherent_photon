//
// Created by eduardo on 1/07/19.
//

#include <Measure_time.h>
#include <Arguments_base.h>
#include "Admin_of_Clases_N1520_dThg.h"

int main(int argc, char** argv) {
    Measure_time time;

    std::string distribution_type = "dsdthg_N1520";

    Arguments_base parameters(argc, argv, distribution_type);

    Admin_of_Clases_N1520_dThg admin(&parameters);

    admin.write_dsdthg();

    time.getTime();
    return 0;
}