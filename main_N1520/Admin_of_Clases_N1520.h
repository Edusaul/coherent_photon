//
// Created by edusaul on 28/04/19.
//

#ifndef COHERENT_PHOTON_ADMIN_OF_CLASES_N1520_H
#define COHERENT_PHOTON_ADMIN_OF_CLASES_N1520_H


#include <Admin_of_Clases_base.h>
#include <Nuclear_FF.h>

class Admin_of_Clases_N1520 : public Admin_of_Clases_base {
protected:
    Nuclear_FF *nuclearFF;

public:
    explicit Admin_of_Clases_N1520(Arguments_base *parameters);

    ~Admin_of_Clases_N1520() override;
};


#endif //COHERENT_PHOTON_ADMIN_OF_CLASES_N1520_H
