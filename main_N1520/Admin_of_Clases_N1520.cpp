//
// Created by edusaul on 28/04/19.
//

#include <Nucleus_FF_DeVries.h>
#include <Hadronic_Current_R_N1520.h>
#include <dsdEg_dphig_dthg_dth.h>
#include <dsdEg_dphig_dthg.h>
#include <dsdEg_dphig.h>
#include <dsdEg.h>
#include "Admin_of_Clases_N1520.h"

Admin_of_Clases_N1520::Admin_of_Clases_N1520(Arguments_base *parameters) : Admin_of_Clases_base(parameters) {
    this->nuclearFF = new Nucleus_FF_DeVries(this->parameters->getNucleus());
//    this->nuclearFF = new Nuclear_FF_HO;
    this->current_R = new Hadronic_Current_R_N1520(this->nuclearFF);
    this->create_dsdEg_clases();
}

Admin_of_Clases_N1520::~Admin_of_Clases_N1520() {
    this->delete_dsdEg_clases();
    delete this->nuclearFF;
}
