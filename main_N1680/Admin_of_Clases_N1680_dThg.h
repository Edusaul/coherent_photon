//
// Created by eduardo on 12/07/19.
//

#ifndef COHERENT_PHOTON_ADMIN_OF_CLASES_N1680_DTHG_H
#define COHERENT_PHOTON_ADMIN_OF_CLASES_N1680_DTHG_H


#include <Admin_of_Clases_base.h>
#include <Nuclear_FF.h>

class Admin_of_Clases_N1680_dThg : public Admin_of_Clases_base {
protected:
    Nuclear_FF *nuclearFF;

public:
    explicit Admin_of_Clases_N1680_dThg(Arguments_base *parameters);

    virtual ~Admin_of_Clases_N1680_dThg();

    };


#endif //COHERENT_PHOTON_ADMIN_OF_CLASES_N1680_DTHG_H
