//
// Created by eduardo on 5/07/19.
//

#ifndef COHERENT_PHOTON_ADMIN_OF_CLASES_N1680_H
#define COHERENT_PHOTON_ADMIN_OF_CLASES_N1680_H


#include <Admin_of_Clases_base.h>
#include <Nuclear_FF.h>

class Admin_of_Clases_N1680 : public Admin_of_Clases_base {
protected:
    Nuclear_FF *nuclearFF;

public:
    explicit Admin_of_Clases_N1680(Arguments_base *parameters);

    ~Admin_of_Clases_N1680() override;
};


#endif //COHERENT_PHOTON_ADMIN_OF_CLASES_N1680_H
