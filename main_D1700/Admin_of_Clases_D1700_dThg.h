//
// Created by eduardo on 12/07/19.
//

#ifndef COHERENT_PHOTON_ADMIN_OF_CLASES_D1700_DTHG_H
#define COHERENT_PHOTON_ADMIN_OF_CLASES_D1700_DTHG_H


#include <Admin_of_Clases_base.h>
#include <Nuclear_FF.h>

class Admin_of_Clases_D1700_dThg : public Admin_of_Clases_base {
protected:
    Nuclear_FF *nuclearFF;

public:
    explicit Admin_of_Clases_D1700_dThg(Arguments_base *parameters);

    ~Admin_of_Clases_D1700_dThg() override;
};


#endif //COHERENT_PHOTON_ADMIN_OF_CLASES_D1700_DTHG_H
