//
// Created by edusaul on 24/03/19.
//

#ifndef COHERENT_PHOTON_NUCLEAR_FF_HO_H
#define COHERENT_PHOTON_NUCLEAR_FF_HO_H


#include <complex>
#include "Nuclear_FF.h"

class Nuclear_FF_HO : public Nuclear_FF {
public:
    void setFF(double) override;

};


#endif //COHERENT_PHOTON_NUCLEAR_FF_HO_H
