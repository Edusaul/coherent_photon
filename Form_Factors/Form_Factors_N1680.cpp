//
// Created by eduardo on 4/07/19.
//

#include <Parameters_GeV.h>
#include "Form_Factors_N1680.h"

Form_Factors_N1680::Form_Factors_N1680() {
    this->parity = "positive";
    this->isospin_by_2 = 1;
    this->set_aNC();

    this->mn = Param::mp;
    this->mn2 = Param::mp2;
    this->MR = Param::mN1680;
    this->MR2 = this->MR*this->MR;
    this->gamma = Param::Gamma_N1680;
    this->branching_pi_N = Param::branching_pi_N_N1680;
    this->c5a0 = this->setC5a0();
    this->MA_R_2 = Param::MA_c5a*Param::MA_c5a;
}

void Form_Factors_N1680::set_Helicity_Amplitudes(double Q2) {
    this->a_12_p =-25.1*1e-3*(1.+3.780*Q2-0.292*Q2*Q2+0.080*pow(Q2,4))*exp(-1.25*Q2);
    this->a_32_p =134.3*1e-3*(1.+1.016*Q2+0.222*Q2*Q2+0.237*pow(Q2,4))*exp(-2.41*Q2);
    this->s_12_p =-44.0*1e-3*(1.+3.783*Q2)*exp(-1.85*Q2);

    this->a_12_n = 27.9*1e-3*exp(-1.20*Q2);
    this->a_32_n =-38.4*1e-3*(1.+4.09*Q2)*exp(-1.75*Q2);
    this->s_12_n = 0.;
}
