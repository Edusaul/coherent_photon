//
// Created by edusaul on 28/04/19.
//

#ifndef COHERENT_PHOTON_FORM_FACTORS_N1520_H
#define COHERENT_PHOTON_FORM_FACTORS_N1520_H


#include "Form_Factors_J32.h"

class Form_Factors_N1520 : public Form_Factors_J32{


public:
    Form_Factors_N1520();

    void set_Helicity_Amplitudes(double) override ;
};


#endif //COHERENT_PHOTON_FORM_FACTORS_N1520_H
