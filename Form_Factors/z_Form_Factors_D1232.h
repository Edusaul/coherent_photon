//
// Created by eduardo on 8/07/19.
//

#ifndef COHERENT_PHOTON_Z_FORM_FACTORS_D1232_H
#define COHERENT_PHOTON_Z_FORM_FACTORS_D1232_H


#include "Form_Factors_J32.h"

class z_Form_Factors_D1232 : public Form_Factors_J32{


public:
    z_Form_Factors_D1232();

    void set_Helicity_Amplitudes(double) override ;

};


#endif //COHERENT_PHOTON_Z_FORM_FACTORS_D1232_H
