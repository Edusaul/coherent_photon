//
// Created by eduardo on 3/07/19.
//

#ifndef COHERENT_PHOTON_FORM_FACTORS_J12_H
#define COHERENT_PHOTON_FORM_FACTORS_J12_H


#include <string>

class Form_Factors_J12 {
private:
    double a;
    double s;

protected:
    std::string parity;
    int isospin_by_2;

    double MR;
    double MR2;
    double mn;
    double mn2;
    double aNC;
    double Fa0;
    double MA_R_2;
    double gamma;
    double branching_pi_N;
    double coupling;

    double F1p;
    double F1n;
    double F2p;
    double F2n;

    double F1s;
    double F2s;
    double F1Zp;
    double F2Zp;
    double F1Zn;
    double F2Zn;
    double FAN;
    double FAZp;
    double FAZn;

    double F1pn;
    double F2pn;
    double F1Zpn;
    double F2Zpn;
    double FAZpn;

    double a_p;
    double s_p;
    double a_n;
    double s_n;

    double setF1_positive(double Q2);
    double setF2_positive(double Q2);

    double setF1_negative(double Q2);
    double setF2_negative(double Q2);

    virtual void set_Helicity_Amplitudes(double Q2) = 0;

    void set_aNC();

    double setCoupling();
    double setFa0();

public:
    Form_Factors_J12();

    void setFF(double);

    double getF1P() const;

    double getF1N() const;

    double getF2P() const;

    double getF2N() const;

    double getF1Zp() const;

    double getF2Zp() const;

    double getF1Zn() const;

    double getF2Zn() const;

    double getFaZp() const;

    double getFaZn() const;

    double getF1Pn() const;

    double getF2Pn() const;

    double getF1Zpn() const;

    double getF2Zpn() const;

    double getFaZpn() const;

    double getAP() const;

    double getSP() const;

    double getAN() const;

    double getSN() const;

    double getCoupling() const;

    double getBranchingPiN() const;

    double getGamma() const;
};


#endif //COHERENT_PHOTON_FORM_FACTORS_J12_H
