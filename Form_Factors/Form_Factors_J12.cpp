//
// Created by eduardo on 3/07/19.
//

#include <Parameters_GeV.h>
#include <iostream>
#include "Form_Factors_J12.h"

Form_Factors_J12::Form_Factors_J12() {}

void Form_Factors_J12::set_aNC() {
    if(this->isospin_by_2 == 1) {
        this->aNC = 1.0 - 4.0 * Param::sinw2;
    } else if(this->isospin_by_2 == 3) {
        this->aNC = 1.0 - 2.0 * Param::sinw2;
    }
}

void Form_Factors_J12::setFF(double Q2) {
    this->set_Helicity_Amplitudes(Q2);

    this->F1s = 0.0;
    this->F2s = 0.0;

    double t=-Q2;
    this->FAN = this->Fa0/((1.0 - t/this->MA_R_2)*(1.0 - t/this->MA_R_2));

    this->a=this->a_p;
    this->s=this->s_p;
    if(this->parity == "positive") {
        this->F1p = this->setF1_positive(Q2);
        this->F2p = this->setF2_positive(Q2);
    } if(this->parity == "negative") {
        this->F1p = this->setF1_negative(Q2);
        this->F2p = this->setF2_negative(Q2);
    }

    if (this->isospin_by_2 == 1) {
        this->a = this->a_n;
        this->s = this->s_n;
        if (this->parity == "positive") {
            this->F1n = this->setF1_positive(Q2);
            this->F2n = this->setF2_positive(Q2);
        } if (this->parity == "negative") {
            this->F1n = this->setF1_negative(Q2);
            this->F2n = this->setF2_negative(Q2);
        }

        this->F1Zp = (this->aNC * this->F1p - this->F1n - this->F1s) / 2.0;
        this->F2Zp = (this->aNC * this->F2p - this->F2n - this->F2s) / 2.0;
        this->F1Zn = (this->aNC * this->F1n - this->F1p - this->F1s) / 2.0;
        this->F2Zn = (this->aNC * this->F2n - this->F2p - this->F2s) / 2.0;

        this->FAZp = this->FAN / 2.0;
        this->FAZn = -this->FAN / 2.0;

    } else if (this->isospin_by_2 == 3) {
        this->F1pn = this->F1p;
        this->F2pn = this->F2p;
        this->F1Zpn = this->aNC * this->F1pn;
        this->F2Zpn = this->aNC * this->F2pn;
        this->FAZpn = - this->FAN;
    }

}

double Form_Factors_J12::setF1_positive(double Q2) {
    return (2*this->mn2*((this->a*sqrt(2/Param::pi))/
                         sqrt(-((Param::alpha*(this->mn2 - 2*this->mn*this->MR + this->MR2 + Q2))/
                                (this->mn2*this->mn - this->mn*this->MR2))) -
                         (4*this->MR*(this->mn + this->MR)*this->s)/
                         (sqrt(Param::pi)*(this->mn2 - 2*this->mn*this->MR + this->MR2 + Q2)*
                          sqrt(-((Param::alpha*(this->mn2 + 2*this->mn*this->MR + this->MR2 + Q2))/
                                 (this->mn2*this->mn - this->mn*this->MR2))))))/
           (this->mn2 + 2*this->mn*this->MR + this->MR2 + Q2);
}

double Form_Factors_J12::setF2_positive(double Q2) {
    return (this->mn*((this->a*(this->mn + this->MR)*sqrt(2/Param::pi))/
                      sqrt(-((Param::alpha*(this->mn2 - 2*this->mn*this->MR + this->MR2 + Q2))/
                             (this->mn2*this->mn - this->mn*this->MR2))) +
                      (4*this->MR*Q2*this->s)/
                      (sqrt(Param::pi)*(this->mn2 - 2*this->mn*this->MR + this->MR2 + Q2)*
                       sqrt(-((Param::alpha*(this->mn2 + 2*this->mn*this->MR + this->MR2 + Q2))/
                              (this->mn2*this->mn - this->mn*this->MR2))))))/
           (this->mn2 + 2*this->mn*this->MR + this->MR2 + Q2);
}

double Form_Factors_J12::setF1_negative(double Q2) {
    return (2.0*this->mn2*((a*sqrt(2.0/Param::pi))/
                           sqrt(-((Param::alpha*(this->mn2 + 2*this->mn*this->MR + this->MR2 + Q2))/
                                  (this->mn2*this->mn - this->mn*this->MR2))) -
                           (4*(this->mn - this->MR)*this->MR*s)/
                           (sqrt(Param::pi)*sqrt(-((Param::alpha*(this->mn2 - 2*this->mn*this->MR + this->MR2 + Q2))/
                                                   (this->mn2*this->mn - this->mn*this->MR2)))*(this->mn2 + 2*this->mn*this->MR + this->MR2 + Q2))))/
           (this->mn2 - 2.0*this->mn*this->MR + this->MR2 + Q2);
}

double Form_Factors_J12::setF2_negative(double Q2) {
    return (this->mn*(-((a*(this->mn - this->MR)*sqrt(2/Param::pi))/
                        sqrt(-((Param::alpha*(this->mn2 + 2*this->mn*this->MR + this->MR2 + Q2))/
                               (this->mn2*this->mn - this->mn*this->MR2)))) -
                      (4*this->MR*Q2*s)/(sqrt(Param::pi)*sqrt(-((Param::alpha*(this->mn2 - 2*this->mn*this->MR + this->MR2 + Q2))/
                                                                (this->mn2*this->mn - this->mn*this->MR2)))*(this->mn2 + 2*this->mn*this->MR + this->MR2 + Q2))))/
           (this->mn2 - 2*this->mn*this->MR + this->MR2 + Q2);
}


double Form_Factors_J12::setCoupling() {
    double iR = 3.0;
    if(this->isospin_by_2 == 3) iR = 1.0;
    double mpi2 = Param::mpi*Param::mpi;
    double qcm = sqrt((this->MR2 - mpi2 - this->mn2)*(this->MR2 - mpi2 - this->mn2) - 4.0*mpi2*this->mn2)/(2.0*this->MR);
    double eN = (this->MR2 + this->mn2 - mpi2)/(2.0*this->MR);
    double sign = 1.0;
    if(this->parity == "negative") sign = -1.0;

    this->coupling = (2.0*sqrt(this->gamma*this->branching_pi_N))/sqrt((iR*(eN - sign * this->mn)*(this->mn + sign * this->MR)*
                                                                       (this->mn + sign * this->MR)*qcm)/(mpi2*MR*Param::pi));
    return this->coupling;
}

double Form_Factors_J12::setFa0() {
    double c_iso = sqrt(2.0);
    if(this->isospin_by_2 == 3) c_iso = -sqrt(1.0/3.0);
    double f = this->setCoupling();
    double fa_0 = -c_iso * sqrt(2.0) * Param::fpi * f/Param::mpi;

    std::cout<<" Gamma = " << this->gamma << "    ;   branching_pi_N = " << branching_pi_N <<"     ;   Fa0 = "<< fa_0<<std::endl;
    return fa_0;
}

double Form_Factors_J12::getF1P() const {
    return F1p;
}

double Form_Factors_J12::getF1N() const {
    return F1n;
}

double Form_Factors_J12::getF2P() const {
    return F2p;
}

double Form_Factors_J12::getF2N() const {
    return F2n;
}

double Form_Factors_J12::getF1Zp() const {
    return F1Zp;
}

double Form_Factors_J12::getF2Zp() const {
    return F2Zp;
}

double Form_Factors_J12::getF1Zn() const {
    return F1Zn;
}

double Form_Factors_J12::getF2Zn() const {
    return F2Zn;
}

double Form_Factors_J12::getFaZp() const {
    return FAZp;
}

double Form_Factors_J12::getFaZn() const {
    return FAZn;
}

double Form_Factors_J12::getF1Pn() const {
    return F1pn;
}

double Form_Factors_J12::getF2Pn() const {
    return F2pn;
}

double Form_Factors_J12::getF1Zpn() const {
    return F1Zpn;
}

double Form_Factors_J12::getF2Zpn() const {
    return F2Zpn;
}

double Form_Factors_J12::getFaZpn() const {
    return FAZpn;
}

double Form_Factors_J12::getAP() const {
    return a_p;
}

double Form_Factors_J12::getSP() const {
    return s_p;
}

double Form_Factors_J12::getAN() const {
    return a_n;
}

double Form_Factors_J12::getSN() const {
    return s_n;
}

double Form_Factors_J12::getCoupling() const {
    return coupling;
}

double Form_Factors_J12::getBranchingPiN() const {
    return branching_pi_N;
}

double Form_Factors_J12::getGamma() const {
    return gamma;
}















