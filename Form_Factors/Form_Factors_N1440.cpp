//
// Created by edusaul on 22/04/19.
//

#include <Parameters_GeV.h>
#include "Form_Factors_N1440.h"

Form_Factors_N1440::Form_Factors_N1440() : Form_Factors_J12() {

    this->parity = "positive";
    this->isospin_by_2 = 1;
    this->set_aNC();

    this->MR= Param::mN1440;
    this->MR2= this->MR*this->MR;
    this->mn= Param::mp;
    this->mn2= Param::mp2;
    this->gamma = Param::Gamma_N1440;
    this->branching_pi_N = Param::branching_pi_N_N1440;
    this->Fa0 = this->setFa0();
    this->MA_R_2 = Param::MA_R_2;
}

void Form_Factors_N1440::set_Helicity_Amplitudes(double Q2) {

    this->a_p = -61.4*1.0e-3*(1.+0.871*Q2-3.516*Q2*Q2-0.158*pow(Q2,4))*exp(-1.36*Q2);
    this->s_p = 4.2*1.0e-3*(1.+ 40.*Q2+1.5*pow(Q2,4))*exp(-1.75*Q2);
    this->a_n = 54.1*1.0e-3*(1.+0.95*Q2)*exp(-1.77*Q2);
    this->s_n = -41.5*1.0e-3*(1.+ 2.98*Q2)*exp(-1.55*Q2);

}







