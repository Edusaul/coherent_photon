//
// Created by eduardo on 8/07/19.
//

#include <Parameters_GeV.h>
#include "z_Form_Factors_D1232.h"

z_Form_Factors_D1232::z_Form_Factors_D1232() {
    this->parity = "positive";
    this->isospin_by_2 = 3;
    this->set_aNC();

    this->mn = Param::mp;
    this->mn2 = Param::mp2;
    this->MR = Param::mDelta;
    this->MR2 = this->MR*this->MR;
    this->gamma = 0.118;
    this->branching_pi_N = 1.0;
    this->c5a0 = this->setC5a0();
    this->MA_R_2 = Param::MA_c5a*Param::MA_c5a;
}

void z_Form_Factors_D1232::set_Helicity_Amplitudes(double Q2) {
    double kgcm0 = (this->MR2-this->mn2)/2./this->MR;
    double egcm = (this->MR2-Q2-this->mn2)/2./this->MR;
    double qcm=sqrt(egcm*egcm+Q2);
    double tau=Q2/4./this->mn2;

    double Fq=1./((1+Q2/0.71)*(1+Q2/0.71))*(qcm/kgcm0);


    double AM=300*(1.+ 0.01*Q2)*exp(-0.23*Q2)*Fq;
    double AE=-6.37*(1.- 0.021*Q2)*exp(-0.16*Q2)*Fq;
    double AS=-12.40*(1.+0.12*Q2)/(1.+4.9*tau)*(qcm/kgcm0)*exp(-0.23*Q2)*Fq;

    this->a_12_p =1.0e-3*-.5*(3*AE+AM);
    this->a_32_p =1.0e-3*sqrt(3.)/2.*(AE-AM);
    this->s_12_p =1.0e-3*-sqrt(2.)*AS;
}
