//
// Created by eduardo on 4/07/19.
//

#ifndef COHERENT_PHOTON_FORM_FACTORS_D1905_H
#define COHERENT_PHOTON_FORM_FACTORS_D1905_H


#include "Form_Factors_J32.h"

class Form_Factors_D1905 : public Form_Factors_J32{


public:
    Form_Factors_D1905();

    void set_Helicity_Amplitudes(double) override ;

};


#endif //COHERENT_PHOTON_FORM_FACTORS_D1905_H
