//
// Created by edusaul on 28/04/19.
//

#include <Parameters_GeV.h>
#include "Form_Factors_N1520.h"

Form_Factors_N1520::Form_Factors_N1520() {
    this->parity = "negative";
    this->isospin_by_2 = 1;
    this->set_aNC();

    this->mn = Param::mp;
    this->mn2 = Param::mp2;
    this->MR = Param::mN1520;
    this->MR2 = this->MR*this->MR;
    this->gamma = Param::Gamma_N1520;
    this->branching_pi_N = Param::branching_pi_N_N1520;
    this->c5a0 = this->setC5a0();
    this->MA_R_2 = Param::MA_c5a*Param::MA_c5a;
}

void Form_Factors_N1520::set_Helicity_Amplitudes(double Q2) {
    this->a_12_p =-27.*1.0e-3*(1.+8.580* Q2-0.252*Q2*Q2+0.357*pow(Q2,4))*exp(-1.20*Q2); //GeV^{-1/2}
    this->a_32_p =160.6*1.0e-3*(1.-0.820*Q2-0.541*Q2*Q2-0.016*pow(Q2,4))*exp(-1.06*Q2); //GeV^{-1/2}
    this->s_12_p = -63.6*1.0e-3*(1.+4.19*Q2)*exp(-3.40*Q2); //GeV^{-1/2}

    this->a_12_n = -76.5*1.0e-3*(1.-0.53*Q2)*exp(-1.55*Q2); //GeV^{-1/2}
    this->a_32_n = -154.*1.0e-3*(1.+0.58*Q2)*exp(-1.75*Q2); //GeV^{-1/2}
    this->s_12_n = 13.6*1.0e-3*(1.+15.7* Q2)*exp(-1.57*Q2); //GeV^{-1/2}
}
