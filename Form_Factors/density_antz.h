//
// Created by edusaul on 1/04/19.
//

#ifndef COHERENT_PHOTON_DENSITY_ANTZ_H
#define COHERENT_PHOTON_DENSITY_ANTZ_H


#include <Integrable_w_change_param.h>

class density_antz : public Integrable_w_change_param<double>{
    int a;
    double rg;
    double ag;

public:
    density_antz(int a, double rg, double ag);

    double integrand(double) override;

    double antz(double r);
};


#endif //COHERENT_PHOTON_DENSITY_ANTZ_H
