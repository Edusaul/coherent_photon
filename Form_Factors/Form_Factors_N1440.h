//
// Created by edusaul on 22/04/19.
//

#ifndef COHERENT_PHOTON_FORM_FACTORS_N1440_H
#define COHERENT_PHOTON_FORM_FACTORS_N1440_H


#include "Form_Factors_J12.h"

class Form_Factors_N1440 : public Form_Factors_J12{

public:
    Form_Factors_N1440();

    void set_Helicity_Amplitudes(double) override;

};


#endif //COHERENT_PHOTON_FORM_FACTORS_N1440_H
