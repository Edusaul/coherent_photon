//
// Created by edusaul on 22/03/19.
//

#include <Parameters_GeV.h>
#include <iostream>
#include "Form_Factors_Delta.h"

Form_Factors_Delta::Form_Factors_Delta() {
    this->mn = Param::mp;
    this->mn2 = Param::mp2;
    this->mDelta = Param::mDelta;
    this->mDelta2 = this->mDelta*this->mDelta;
    this->N_Delta_MA = Param::N_Delta_MA;
    this->aNC = 1.0 - 2.0*Param::sinw2;

    this->kgcm0 = (this->mDelta2-this->mn2)/2.0/this->mDelta;
    this->mpw2 = (this->mn+this->mDelta)*(this->mn+this->mDelta);
    this->mmw2 = (this->mn - this->mDelta)*(this->mn - this->mDelta);

    this->C4v = 0.0;
    this->C5v = 0.0;
    this->C3a = 0.0;
    this->C4a = 0.0;
    this->C5a = 0.0;
    this->C6a = 0.0;

    this->C4vNC = 0.0;
    this->C5vNC = 0.0;
    this->C3aNC = 0.0;
    this->C4aNC = 0.0;
    this->C6aNC = 0.0;

}

void Form_Factors_Delta::setFF(double Q2) {

    double egcm = (this->mDelta2-Q2-this->mn2)/2.0/this->mDelta;
    double qcm = sqrt(egcm*egcm + Q2);

    double Fq = 1.0/((1.0 + Q2/Param::parameter_071)*(1.0 + Q2/Param::parameter_071));
    double AM = Param::parameter_03*(1.0 + Param::parameter_001*Q2)*exp(-Param::parameter_023*Q2)*(qcm/this->kgcm0)*Fq;
    double A32= sqrt(3.0)/2.0*(-AM);

    double r = sqrt(2.0*this->kgcm0/Param::pi/Param::alpha*this->mn*this->mDelta/(this->mmw2+Q2));

    this->C3v=-r*this->mn*this->mDelta/(this->mpw2+Q2)*(2.0*A32);

    double Fd=pow((1.0 + Q2/(this->N_Delta_MA*this->N_Delta_MA)),-2);
    this->C5aNC=1.2*Fd;
//    this->C5aNC=1.0*Fd;

    this->C3vNC = this->C3v * this->aNC;

//    std::cout<<r <<"   "<< Q2<<"  "<< this->mn<<"  "<<this->mDelta<<std::endl;
//    std::cout<<this->C3vNC <<"   "<< this->C3v<<"  "<< this->aNC<<std::endl<<std::endl;

}

double Form_Factors_Delta::getC3v() const {
    return C3v;
}

double Form_Factors_Delta::getC4v() const {
    return C4v;
}

double Form_Factors_Delta::getC5v() const {
    return C5v;
}

double Form_Factors_Delta::getC3a() const {
    return C3a;
}

double Form_Factors_Delta::getC4a() const {
    return C4a;
}

double Form_Factors_Delta::getC5a() const {
    return C5a;
}

double Form_Factors_Delta::getC6a() const {
    return C6a;
}

double Form_Factors_Delta::getC3vNC() const {
    return C3vNC;
}

double Form_Factors_Delta::getC4vNC() const {
    return C4vNC;
}

double Form_Factors_Delta::getC5vNC() const {
    return C5vNC;
}

double Form_Factors_Delta::getC3aNC() const {
    return C3aNC;
}

double Form_Factors_Delta::getC4aNC() const {
    return C4aNC;
}

double Form_Factors_Delta::getC5aNC() const {
    return C5aNC;
}

double Form_Factors_Delta::getC6aNC() const {
    return C6aNC;
}
