//
// Created by eduardo on 13/03/19.
//

#ifndef COHERENT_PHOTON_NUCLEUS_FF_12C_DEVRIES_H
#define COHERENT_PHOTON_NUCLEUS_FF_12C_DEVRIES_H
#include <string>
#include <complex>
#include "Nuclear_FF.h"

class Nucleus_FF_DeVries : public Nuclear_FF{
protected:
    std::string nucleus;

    double a[16];
    double R;
    double pi;
    double hc;

public:
    explicit Nucleus_FF_DeVries(const std::string &nucleus);

    void setFF(double) override;

    void set_12C();

    void set_40Ar();

};


#endif //COHERENT_PHOTON_NUCLEUS_FF_12C_DEVRIES_H
