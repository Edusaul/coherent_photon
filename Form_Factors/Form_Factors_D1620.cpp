//
// Created by eduardo on 4/07/19.
//

#include <Parameters_GeV.h>
#include "Form_Factors_D1620.h"

Form_Factors_D1620::Form_Factors_D1620() {
    this->parity = "negative";
    this->isospin_by_2 = 3;
    this->set_aNC();

    this->MR= Param::mD1620;
    this->MR2= this->MR*this->MR;
    this->mn= Param::mp;
    this->mn2= Param::mp2;
    this->gamma = Param::Gamma_D1620;
    this->branching_pi_N = Param::branching_pi_N_D1620;
    this->Fa0 = this->setFa0();
    this->MA_R_2 = Param::MA_R_2;
}

void Form_Factors_D1620::set_Helicity_Amplitudes(double Q2) {
    this->a_p = 65.6*1e-3*(1.+1.86*Q2)*exp(-2.50*Q2);
    this->s_p = 16.2*1e-3*(1.+2.83*Q2)*exp(-2.00*Q2);
}
