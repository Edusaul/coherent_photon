//
// Created by eduardo on 4/07/19.
//

#ifndef COHERENT_PHOTON_FORM_FACTORS_N1650_H
#define COHERENT_PHOTON_FORM_FACTORS_N1650_H


#include "Form_Factors_J12.h"

class Form_Factors_N1650 : public Form_Factors_J12{


public:
    Form_Factors_N1650();

    void set_Helicity_Amplitudes(double) override ;

};


#endif //COHERENT_PHOTON_FORM_FACTORS_N1650_H
