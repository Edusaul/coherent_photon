//
// Created by edusaul on 8/04/19.
//

#ifndef COHERENT_PHOTON_NUCLEAR_FF_H
#define COHERENT_PHOTON_NUCLEAR_FF_H
#include <complex>


class Nuclear_FF {
protected:
    std::complex<double> FF_p;
    std::complex<double> FF_n;
public:

    const std::complex<double> &getFfP() const;

    const std::complex<double> &getFfN() const;

    virtual void setFF(double) = 0;

};


#endif //COHERENT_PHOTON_NUCLEAR_FF_H
