//
// Created by edusaul on 11/03/19.
//

#include <Parameters_GeV.h>
#include "Form_Factors_Nucleon.h"

Form_Factors_Nucleon_EM::Form_Factors_Nucleon_EM() {
    this->lambdap=Param::mup;
    this->lambdan=Param::mun;
    this->MD=Param::MD;
    this->lambdanN=Param::lambdanN;
    this->mp2= Param::mp2;
}

void Form_Factors_Nucleon_EM::setFF(double Q2) {
    double t=-Q2;
	double tau=Q2/(4.0*this->mp2);
	double mup=1.0+ this->lambdap;
	double mun= this->lambdan;

	this->GEp= (1.0/(1.0-t/(this->MD* this->MD)))*(1.0/(1.0-t/(this->MD* this->MD)));
	this->GMp=GEp*mup;
	this->GMn=GEp*mun;
	this->GEn=-GEp*mun*tau/(1.0+ this->lambdanN*tau);
	this->F1p=(GEp+tau*GMp)/(1.0+tau);
	this->F2p=(GMp-GEp)/(1.0+tau);
	this->F1n=(GEn+tau*GMn)/(1.0+tau);
	this->F2n=(GMn-GEn)/(1.0+tau);
	this->F1v=(F1p-F1n);
	this->F2v=(F2p-F2n);

	this->F1s = 0.0;
	this->F2s = 0.0;

	this->F1Zp = ((1.0 - 4.0*Param::sinw2) * this->F1p - this->F1n - this->F1s)/2.0;
	this->F2Zp = ((1.0 - 4.0*Param::sinw2) * this->F2p - this->F2n - this->F2s)/2.0;
	this->F1Zn = ((1.0 - 4.0*Param::sinw2) * this->F1n - this->F1p - this->F1s)/2.0;
	this->F2Zn = ((1.0 - 4.0*Param::sinw2) * this->F2n - this->F2p - this->F2s)/2.0;

	this->FAs = Param::gas/((1.0 - t/Param::MA2)*(1.0 - t/Param::MA2));
	this->FAN = Param::ga/((1.0 - t/Param::MA2)*(1.0 - t/Param::MA2));

	this->FAZp = (+ this->FAN + this->FAs)/2.0;
	this->FAZn = (- this->FAN + this->FAs)/2.0;

	this->FPs = 2.0*this->mp2/(Param::mpi2 - t) * this->FAs;
	this->FPN = 2.0*this->mp2/(Param::mpi2 - t) * this->FAN;

	this->FPZp = (+ this->FPN + this->FPs)/2.0;
	this->FPZn = (- this->FPN + this->FPs)/2.0;
}

double Form_Factors_Nucleon_EM::getF1Zp() const {
	return F1Zp;
}

double Form_Factors_Nucleon_EM::getF2Zp() const {
	return F2Zp;
}

double Form_Factors_Nucleon_EM::getF1Zn() const {
	return F1Zn;
}

double Form_Factors_Nucleon_EM::getF2Zn() const {
	return F2Zn;
}

double Form_Factors_Nucleon_EM::getFAZp() const {
	return FAZp;
}

double Form_Factors_Nucleon_EM::getFAZn() const {
	return FAZn;
}

double Form_Factors_Nucleon_EM::getFPZp() const {
	return FPZp;
}

double Form_Factors_Nucleon_EM::getFPZn() const {
	return FPZn;
}

double Form_Factors_Nucleon_EM::getF1p() const {
	return F1p;
}

double Form_Factors_Nucleon_EM::getF1n() const {
	return F1n;
}

double Form_Factors_Nucleon_EM::getF2p() const {
	return F2p;
}

double Form_Factors_Nucleon_EM::getF2n() const {
	return F2n;
}

double Form_Factors_Nucleon_EM::getF1v() const {
	return F1v;
}

double Form_Factors_Nucleon_EM::getF2v() const {
	return F2v;
}

double Form_Factors_Nucleon_EM::getGEn() const {
	return GEn;
}

double Form_Factors_Nucleon_EM::getGEp() const {
	return GEp;
}

double Form_Factors_Nucleon_EM::getGMn() const {
	return GMn;
}

double Form_Factors_Nucleon_EM::getGMp() const {
	return GMp;
}




