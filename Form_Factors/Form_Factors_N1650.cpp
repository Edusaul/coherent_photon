//
// Created by eduardo on 4/07/19.
//

#include <Parameters_GeV.h>
#include "Form_Factors_N1650.h"


Form_Factors_N1650::Form_Factors_N1650() {
    this->parity = "negative";
    this->isospin_by_2 = 1;
    this->set_aNC();

    this->MR= Param::mN1650;
    this->MR2= this->MR*this->MR;
    this->mn= Param::mp;
    this->mn2= Param::mp2;
    this->gamma = Param::Gamma_N1650;
    this->branching_pi_N = Param::branching_pi_N_N1650;
    this->Fa0 = this->setFa0();
    this->MA_R_2 = Param::MA_R_2;
}

void Form_Factors_N1650::set_Helicity_Amplitudes(double Q2) {

    this->a_p = 33.3*1e-3*(1.+1.45*Q2)*exp(-0.62*Q2);
    this->s_p = -3.5*1e-3*(1.+2.88*Q2)*exp(-0.76*Q2);
    this->a_n = 9.3*1e-3*(1.+0.13*Q2)*exp(-1.55*Q2);
    this->s_n = 10.*1e-3*(1.-0.50*Q2)*exp(-1.55*Q2);
}
