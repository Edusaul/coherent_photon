//
// Created by eduardo on 4/07/19.
//

#ifndef COHERENT_PHOTON_FORM_FACTORS_D1950_H
#define COHERENT_PHOTON_FORM_FACTORS_D1950_H


#include "Form_Factors_J32.h"

class Form_Factors_D1950 : public Form_Factors_J32{


public:
    Form_Factors_D1950();

    void set_Helicity_Amplitudes(double) override ;

};


#endif //COHERENT_PHOTON_FORM_FACTORS_D1950_H
