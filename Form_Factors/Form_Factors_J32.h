//
// Created by eduardo on 4/07/19.
//

#ifndef COHERENT_PHOTON_FORM_FACTORS_J32_H
#define COHERENT_PHOTON_FORM_FACTORS_J32_H

#include <string>

class Form_Factors_J32 {
private:
    double A12;
    double A32;
    double S12;
    
protected:
    std::string parity;
    int isospin_by_2;

    double MR;
    double MR2;
    double mn;
    double mn2;
    double aNC;
    double c5a0;
    double MA_R_2;
    double gamma;
    double branching_pi_N;
    double coupling;

    double a_12_p;
    double a_32_p;
    double s_12_p;
    double a_12_n;
    double a_32_n;
    double s_12_n;

    double c3n;
    double c4n;
    double c5n;
    double c3p;
    double c4p;
    double c5p;

//    double C3v;
//    double C4v;
//    double C5v;

    double C3a;
    double C4a;
    double C5a;
    double C6a;

    double C3aNC_p;
    double C3aNC_n;
    double C4aNC_p;
    double C4aNC_n;
    double C5aNC_p;
    double C5aNC_n;
    double C6aNC_p;
    double C6aNC_n;

    double C3vNC_p;
    double C4vNC_p;
    double C5vNC_p;

    double C3vNC_n;
    double C4vNC_n;
    double C5vNC_n;

    double c3pn;
    double c4pn;
    double c5pn;
    double C3vNC_pn;
    double C4vNC_pn;
    double C5vNC_pn;
    double C3aNC_pn;
    double C4aNC_pn;
    double C5aNC_pn;
    double C6aNC_pn;

    virtual void set_Helicity_Amplitudes(double Q2) = 0;

    void set_aNC();

    double setCoupling();
    double setC5a0();

    double C3_N_positive(double);
    double C4_N_positive(double);
    double C5_N_positive(double);

    double C3_N_negative(double);
    double C4_N_negative(double);
    double C5_N_negative(double);

public:
    Form_Factors_J32();

    void setFF(double);

    double getA12P() const;

    double getA32P() const;

    double getS12P() const;

    double getA12N() const;

    double getA32N() const;

    double getS12N() const;

    double getC3N() const;

    double getC4N() const;

    double getC5N() const;

    double getC3P() const;

    double getC4P() const;

    double getC5P() const;

    double getC3ANcP() const;

    double getC3ANcN() const;

    double getC4ANcP() const;

    double getC4ANcN() const;

    double getC5ANcP() const;

    double getC5ANcN() const;

    double getC6ANcP() const;

    double getC6ANcN() const;

    double getC3VNcP() const;

    double getC4VNcP() const;

    double getC5VNcP() const;

    double getC3VNcN() const;

    double getC4VNcN() const;

    double getC5VNcN() const;

    double getC3Pn() const;

    double getC4Pn() const;

    double getC5Pn() const;

    double getC3VNcPn() const;

    double getC4VNcPn() const;

    double getC5VNcPn() const;

    double getC3ANcPn() const;

    double getC4ANcPn() const;

    double getC5ANcPn() const;

    double getC6ANcPn() const;

    double getCoupling() const;

    double getBranchingPiN() const;

    double getGamma() const;

};


#endif //COHERENT_PHOTON_FORM_FACTORS_J32_H
