//
// Created by eduardo on 4/07/19.
//

#include <Parameters_GeV.h>
#include "Form_Factors_N1675.h"

Form_Factors_N1675::Form_Factors_N1675() {
    this->parity = "negative";
    this->isospin_by_2 = 1;
    this->set_aNC();

    this->mn = Param::mp;
    this->mn2 = Param::mp2;
    this->MR = Param::mN1675;
    this->MR2 = this->MR*this->MR;
    this->gamma = Param::Gamma_N1675;
    this->branching_pi_N = Param::branching_pi_N_N1675;
    this->c5a0 = this->setC5a0();
    this->MA_R_2 = Param::MA_c5a*Param::MA_c5a;
}

void Form_Factors_N1675::set_Helicity_Amplitudes(double Q2) {
    this->a_12_p =15.3*1e-3*(1.+0.10*Q2)*exp(-2.00*Q2);
    this->a_32_p =21.6*1e-3*(1.+1.91*Q2+0.18*Q2*Q2)*exp(-0.69*Q2);
    this->s_12_p = 1.1*1e-3*exp(-2.00*Q2);

    this->a_12_n = -61.7*1e-3*(1.+0.01*Q2)*exp(-2.00*Q2);
    this->a_32_n = -83.7*1e-3*(1.+0.01*Q2)*exp(-2.00*Q2);
    this->s_12_n = 0.0;
}

