//
// Created by edusaul on 1/04/19.
//

#ifndef COHERENT_PHOTON_DENSITY_PROFILE_H
#define COHERENT_PHOTON_DENSITY_PROFILE_H

//     This routine calculates the density of matter (p and n)
//     and the density of centers ( p and n number densities )
//     following J. Nieves et al., Pionic atoms...
//     everything in fm

#include <string>
#include "density_antz.h"

class density_profile {
protected:
    std::string nucleus;
    int a;
    int z;
    double r2;
    double pi2;
    double rmin;
    double rmax;
    int npts;

    double rp;
    double ap;
    double rn;
    double an;

    double rpc;
    double apc;
    double rnc;
    double anc;

    density_antz *antz_p;
    density_antz *antz_n;
    density_antz *antz_p_center;
    density_antz *antz_n_center;

    void set_nucleus();

    void set_centers();

    double get_rho(density_antz *, double r);

public:
    explicit density_profile(std::string nucleus);

    virtual ~density_profile();

    double get_rhop(double r);

    double get_rhon(double r);

    double get_rhop_cent(double r);

    double get_rhon_cent(double r);

    int getA() const;

    double getRmax() const;

};


#endif //COHERENT_PHOTON_DENSITY_PROFILE_H
