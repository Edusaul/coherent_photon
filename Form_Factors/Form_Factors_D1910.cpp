//
// Created by eduardo on 4/07/19.
//

#include <Parameters_GeV.h>
#include "Form_Factors_D1910.h"

Form_Factors_D1910::Form_Factors_D1910() {
    this->parity = "positive";
    this->isospin_by_2 = 3;
    this->set_aNC();

    this->mn = Param::mp;
    this->mn2 = Param::mp2;
    this->MR = Param::mD1910;
    this->MR2 = this->MR*this->MR;
    this->gamma = Param::Gamma_D1910;
    this->branching_pi_N = Param::branching_pi_N_D1910;
    this->Fa0 = this->setFa0();
    this->MA_R_2 = Param::MA_R_2;
}

void Form_Factors_D1910::set_Helicity_Amplitudes(double Q2) {
    this->a_p =18.*1e-3*exp(-2.00*Q2); //GeV^{-1/2}
    this->s_p =0.0;
}
