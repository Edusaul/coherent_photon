//
// Created by edusaul on 24/03/19.
//

#include <cmath>
#include <iostream>
#include <Parameters_GeV.h>
#include "Nuclear_FF_HO.h"

void Nuclear_FF_HO::setFF(double pm) {
    pm/=Param::hc;
    double z=6;
    double a=12;
    double r2=0.69;

    double rp=1.692;
    double ap=1.082;
    double rn=rp;
    double an=ap;

//    Density of CENTERS (taking into account the finite nucleon size)
//   protons
    double rpc=sqrt(rp*rp-2.0/3.0*r2);
    double x=ap*rp*rp/(1.0+3.0/2.0*ap)/(rpc*rpc);
    double apc=2.0*x/(2.0-3.0*x);
//    !       neutrons
    double rnc=sqrt(rn*rn-2.0/3.0*r2);
    x=an*rn*rn/(1.0+3.0/2.0*an)/(rnc*rnc);
    double anc=2.0*x/(2.0-3.0*x);

    this->FF_p = z/4.*(4.-apc*((pm*rpc)*(pm*rpc)-6.))/(3.*apc+2.)*exp(-(pm*rpc)*(pm*rpc)/4.);
    this->FF_n = (a-z)/4.*(4.-anc*((pm*rnc)*(pm*rnc)-6.))/(3.*anc+2.)*exp(-(pm*rnc)*(pm*rnc)/4.);

    this->FF_p *= 2.0;
    this->FF_n *= 2.0;
}
