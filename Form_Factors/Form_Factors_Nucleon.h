//
// Created by edusaul on 11/03/19.
//

#ifndef COHERENT_PHOTON_FORM_FACTORS_NUCLEON_H
#define COHERENT_PHOTON_FORM_FACTORS_NUCLEON_H


class Form_Factors_Nucleon_EM {
protected:
    double lambdap;
    double lambdan;
    double MD;
    double lambdanN;
    double mp2;

    double F1p;
    double F1n;
    double F2p;
    double F2n;
    double F1v;
	double F2v;
	double GEn;
	double GEp;
	double GMn;
	double GMp;

	double F1s;
	double F2s;
	double F1Zp;
	double F2Zp;
	double F1Zn;
	double F2Zn;
	double FAN;
	double FAs;
	double FPs;
	double FAZp;
	double FAZn;
	double FPN;
	double FPZp;
	double FPZn;




//	double

public:
    Form_Factors_Nucleon_EM();

    void setFF(double);

	double getF1p() const;

	double getF1n() const;

	double getF2p() const;

	double getF2n() const;

	double getF1v() const;

    double getF2v() const;

    double getGEn() const;

    double getGEp() const;

    double getGMn() const;

    double getGMp() const;

	double getF1Zp() const;

	double getF2Zp() const;

	double getF1Zn() const;

	double getF2Zn() const;

	double getFAZp() const;

	double getFAZn() const;

	double getFPZp() const;

	double getFPZn() const;

};


#endif //COHERENT_PHOTON_FORM_FACTORS_NUCLEON_H
