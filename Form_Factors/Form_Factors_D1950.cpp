//
// Created by eduardo on 4/07/19.
//

#include <Parameters_GeV.h>
#include "Form_Factors_D1950.h"

Form_Factors_D1950::Form_Factors_D1950() {
    this->parity = "positive";
    this->isospin_by_2 = 3;
    this->set_aNC();

    this->mn = Param::mp;
    this->mn2 = Param::mp2;
    this->MR = Param::mD1950;
    this->MR2 = this->MR*this->MR;
    this->gamma = Param::Gamma_D1950;
    this->branching_pi_N = Param::branching_pi_N_D1950;
    this->c5a0 = this->setC5a0();
    this->MA_R_2 = Param::MA_c5a*Param::MA_c5a;
}

void Form_Factors_D1950::set_Helicity_Amplitudes(double Q2) {
    this->a_12_p =-94.*1e-3*exp(-2.00*Q2);
    this->a_32_p =-121.*1e-3*exp(-2.00*Q2);
    this->s_12_p =0.0;
}
