//
// Created by eduardo on 4/07/19.
//

#ifndef COHERENT_PHOTON_FORM_FACTORS_N1675_H
#define COHERENT_PHOTON_FORM_FACTORS_N1675_H


#include "Form_Factors_J32.h"

class Form_Factors_N1675 : public Form_Factors_J32{


public:
    Form_Factors_N1675();

    void set_Helicity_Amplitudes(double) override ;

};


#endif //COHERENT_PHOTON_FORM_FACTORS_N1675_H
