//
// Created by eduardo on 13/03/19.
//

#include <cmath>
#include <Parameters_GeV.h>
#include <iostream>
#include "Nucleus_FF_DeVries.h"

Nucleus_FF_DeVries::Nucleus_FF_DeVries(const std::string &nucleus) : nucleus(nucleus) {
    this->pi= Param::pi;
    this->hc=Param::hc;

    if(this->nucleus == "12C") this->set_12C();
    else if(this->nucleus == "40Ar") this->set_40Ar();
    else {
        std::cout<<"WARNING: wrong nucleus, setting 12C as default..."<<std::endl;
        this->set_12C();
    }

}


void Nucleus_FF_DeVries::setFF(double Q) {

    double aux,nu;
    int i;

    aux=0.0;

    for (i=1;i<16; i++){
        nu=double(i);
        aux += pow(-1.0,nu+1.0)*R*R*R*a[i]*hc*hc*hc/(pi*pi*nu*nu-Q*Q*R*R);
    }

    this->FF_p = 4.0*pi*sin(Q*R)/(Q*R)*aux;// /6.0;
    this->FF_n = this->FF_p;
}

void Nucleus_FF_DeVries::set_12C() {
    //a(i) en fm^-3
    this->a[1]=0.15737e-1;
    this->a[2]=0.38897e-1;
    this->a[3]=0.37085e-1;
    this->a[4]=0.14795e-1;
    this->a[5]=-0.44834e-2;

    this->a[6]=-0.10057e-1;
    this->a[7]=-0.68696e-2;
    this->a[8]=-0.28813e-2;
    this->a[9]=-0.77229e-3;
    this->a[10]=0.66908e-4;

    this->a[11]=0.10636e-3;
    this->a[12]=-0.36864e-4;
    this->a[13]=-0.50135e-5;
    this->a[14]=0.9455e-5;
    this->a[15]=-0.47687e-5;

    this->R=8.0/hc; // [fm]/hc
}

void Nucleus_FF_DeVries::set_40Ar() {
    //a(i) en fm^-3
    this->a[1]=0.30451e-1;
    this->a[2]=0.55337e-1;
    this->a[3]=0.20203e-1;
    this->a[4]=-0.16765e-1;
    this->a[5]=-0.13578e-1;

    this->a[6]=-0.43204e-4;
    this->a[7]=0.91988e-3;
    this->a[8]=-0.41205e-3;
    this->a[9]=0.11971e-3;
    this->a[10]=-0.19801e-4;

    this->a[11]=-0.43204e-5;
    this->a[12]=0.61205e-5;
    this->a[13]=-0.37803e-5;
    this->a[14]=0.18001e-5;
    this->a[15]=-0.77407e-6;

    this->R=9.0/hc; // [fm]/hc
}




