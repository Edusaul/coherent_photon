//
// Created by edusaul on 1/04/19.
//

#include <cmath>
#include <Parameters_GeV.h>
#include <iostream>
#include "density_antz.h"

density_antz::density_antz(int a, double rg, double ag) : a(a), rg(rg), ag(ag) {}

double density_antz::integrand(double r) {

    return 4.0 * Param::pi * r*r * this->antz(r);

}

double density_antz::antz(double r) {

    if(this->a < 18) {
        // harmonic oscillator

//        std::cout<<"------"<<r<<"  "<<(1.+ this->ag*(r/this->rg)*(r/this->rg))*exp(-(r/rg)*(r/rg))<<std::endl;

        return (1.+ this->ag*(r/this->rg)*(r/this->rg))*exp(-(r/rg)*(r/rg));
    } else {
        // Fermi liquid
        return 1.0/(1.0+exp((r-this->rg)/this->ag));
    }
}




