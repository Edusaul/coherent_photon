//
// Created by eduardo on 4/07/19.
//

#include <Parameters_GeV.h>
#include <iostream>
#include "Form_Factors_J32.h"

Form_Factors_J32::Form_Factors_J32() {
    this->C3a = 0.0;
    this->C4a = 0.0;
    this->C6a = 0.0;
}

void Form_Factors_J32::set_aNC() {
    if(this->isospin_by_2 == 1) {
        this->aNC = 1.0 - 4.0 * Param::sinw2;
    } else if(this->isospin_by_2 == 3) {
        this->aNC = 1.0 - 2.0 * Param::sinw2;
    }
}

void Form_Factors_J32::setFF(double Q2) {
    this->set_Helicity_Amplitudes(Q2);

    double q2 = -Q2;

    this->C5a=this->c5a0/((1.0-q2/this->MA_R_2)*(1.0-q2/this->MA_R_2));

    this->A12=this->a_12_p;
    this->A32=this->a_32_p;
    this->S12=this->s_12_p;
    if(this->parity == "positive") {
        this->c3p = this->C3_N_positive(q2);
        this->c4p = this->C4_N_positive(q2);
        this->c5p = this->C5_N_positive(q2);
    } if(this->parity == "negative") {
        this->c3p = this->C3_N_negative(q2);
        this->c4p = this->C4_N_negative(q2);
        this->c5p = this->C5_N_negative(q2);
    }

    if (this->isospin_by_2 == 1) {
        this->A12=this->a_12_n;
        this->A32=this->a_32_n;
        this->S12=this->s_12_n;
        if (this->parity == "positive") {
            this->c3n = this->C3_N_positive(q2);
            this->c4n = this->C4_N_positive(q2);
            this->c5n = this->C5_N_positive(q2);
        } if (this->parity == "negative") {
            this->c3n = this->C3_N_negative(q2);
            this->c4n = this->C4_N_negative(q2);
            this->c5n = this->C5_N_negative(q2);
        }

        this->C3aNC_p = this->C3a/2.0;
        this->C3aNC_n = -this->C3a/2.0;
        this->C4aNC_p = this->C4a/2.0;
        this->C4aNC_n = -this->C4a/2.0;
        this->C5aNC_p = this->C5a/2.0;
        this->C5aNC_n = -this->C5a/2.0;
        this->C6aNC_p = this->C6a/2.0;
        this->C6aNC_n = -this->C6a/2.0;


        this->C3vNC_p = (this->c3p * this->aNC - this->c3n)/2.0;
        this->C4vNC_p = (this->c4p * this->aNC - this->c4n)/2.0;
        this->C5vNC_p = (this->c5p * this->aNC - this->c5n)/2.0;

        this->C3vNC_n = (this->c3n * this->aNC - this->c3p)/2.0;
        this->C4vNC_n = (this->c4n * this->aNC - this->c4p)/2.0;
        this->C5vNC_n = (this->c5n * this->aNC - this->c5p)/2.0;

    } else if (this->isospin_by_2 == 3) {
        this->c3pn = this->c3p;
        this->c4pn = this->c4p;
        this->c5pn = this->c5p;

        this->C3vNC_pn = this->c3pn * this->aNC;
        this->C4vNC_pn = this->c4pn * this->aNC;
        this->C5vNC_pn = this->c5pn * this->aNC;

        this->C3aNC_pn = -this->C3a;
        this->C4aNC_pn = -this->C4a;
        this->C5aNC_pn = -this->C5a;
        this->C6aNC_pn = -this->C6a;

    }

}

double Form_Factors_J32::C3_N_positive(double q2) {
    q2=-q2;
    return ((3*this->A12 + sqrt(3)*this->A32)*this->mn*MR)/
           (sqrt(3*Param::pi)*sqrt(-((Param::alpha*(this->mn2 - 2*this->mn*MR + pow(MR,2) + q2))/
                              (pow(this->mn,3) - this->mn*pow(MR,2))))*(this->mn2 + 2*this->mn*MR + pow(MR,2) + q2));
}

double Form_Factors_J32::C4_N_positive(double q2) {
    q2=-q2;
    return (-2 * this->A32 * this->mn2 * sqrt(-((Param::alpha * (this->mn2 + 2 * this->mn * MR + pow(MR, 2) + q2)) /
                                                (pow(this->mn, 3) - this->mn * pow(MR, 2)))) *
            (pow(this->mn, 4) - 3 * pow(this->mn, 3) * MR - 3 * this->mn * MR * (pow(MR, 2) + q2) +
             pow(pow(MR, 2) + q2, 2) +
             2 * this->mn2 * (2 * pow(MR, 2) + q2)) +
            sqrt(6) * this->mn2 * MR *
            (sqrt(2) * this->A12 * this->mn * (this->mn2 - 2 * this->mn * MR + pow(MR, 2) + q2) *
             sqrt(-((Param::alpha * (this->mn2 + 2 * this->mn * MR + pow(MR, 2) + q2)) /
                    (pow(this->mn, 3) - this->mn * pow(MR, 2)))) +
             2 * MR * (-this->mn2 + pow(MR, 2) + q2) *
             sqrt(-((Param::alpha * (this->mn2 - 2 * this->mn * MR + pow(MR, 2) + q2)) /
                    (pow(this->mn, 3) - this->mn * pow(MR, 2)))) * this->S12)) /
           (sqrt(Param::pi) * pow(this->mn2 - 2 * this->mn * MR + pow(MR, 2) + q2, 2) *
            sqrt(-((Param::alpha * (this->mn2 - 2 * this->mn * MR + pow(MR, 2) + q2)) /
                   (pow(this->mn, 3) - this->mn * pow(MR, 2)))) *
            (this->mn2 + 2 * this->mn * MR + pow(MR, 2) + q2) *
            sqrt(-((Param::alpha * (this->mn2 + 2 * this->mn * MR + pow(MR, 2) + q2)) /
                   (pow(this->mn, 3) - this->mn * pow(MR, 2)))));
}

double Form_Factors_J32::C5_N_positive(double q2) {
    q2=-q2;
    return (2*this->mn2*pow(MR,2)*((this->A32*(this->mn2 - 2*this->mn*MR + pow(MR,2) + q2))/
                                   (sqrt(Param::pi)*sqrt(-((Param::alpha*(this->mn2 - 2*this->mn*MR + pow(MR,2) + q2))/
                                                           (pow(this->mn,3) - this->mn*pow(MR,2))))) +
                                   (this->A12*(pow(this->mn,3) - this->mn*pow(MR,2))*sqrt(3/Param::pi)*
                                    sqrt(-((Param::alpha*(this->mn2 - 2*this->mn*MR + pow(MR,2) + q2))/
                                           (pow(this->mn,3) - this->mn*pow(MR,2)))))/Param::alpha +
                                   (sqrt(6/Param::pi)*(this->mn2 - pow(MR,2) + q2)*this->S12)/
    sqrt(-((Param::alpha*(this->mn2 + 2*this->mn*MR + pow(MR,2) + q2))/
           (pow(this->mn,3) - this->mn*pow(MR,2))))))/
    (pow(this->mn2 - 2*this->mn*MR + pow(MR,2) + q2,2)*(this->mn2 + 2*this->mn*MR + pow(MR,2) + q2));
}

double Form_Factors_J32::C3_N_negative(double q2) {
    return ((3*A12 - sqrt(3.)*A32)*mn*this->MR*sqrt(-((mn*(mn - this->MR)*(mn + this->MR))/((mn + this->MR)*(mn + this->MR) - q2))))/
           (sqrt(3*Param::pi)*((mn - this->MR)*(mn - this->MR) - q2)*sqrt(Param::alpha));
}

double Form_Factors_J32::C4_N_negative(double q2) {
    return -((pow(mn,3)*(mn - this->MR)*(mn + this->MR)*sqrt(2/(3.*Param::pi))*
              (sqrt(6.)*A32*(mn2 + mn*this->MR + this->MR2 - q2)*((mn + this->MR)*(mn + this->MR) - q2)*
               sqrt(-(((mn2 - 2*mn*this->MR + this->MR2 - q2)*Param::alpha)/(mn2*mn - mn*this->MR2))) -
               3*this->MR*(sqrt(2.)*A12*mn*((mn + this->MR)*(mn + this->MR) - q2)*sqrt(-(((mn2 - 2*mn*this->MR + this->MR2 - q2)*Param::alpha)/(mn*mn2 - mn*this->MR2))) -
                               2*this->MR*(mn2 - this->MR2 + q2)*S12*sqrt(-((((mn + this->MR)*(mn + this->MR) - q2)*Param::alpha)/(mn*(mn - this->MR)*(mn + this->MR)))))))/
             (pow(((mn - this->MR)*(mn - this->MR) - q2),1.5)*pow(((mn + this->MR)*(mn + this->MR) - q2),2.5)*Param::alpha));
}

double Form_Factors_J32::C5_N_negative(double q2) {
    return (mn2*mn*(mn - this->MR)*this->MR2*(mn + this->MR)*sqrt(2/(3.*Param::pi))*
            (3*sqrt(2.)*A12*((mn + this->MR)*(mn + this->MR) - q2)*sqrt(-(((mn2 - 2*mn*this->MR + this->MR2 - q2)*Param::alpha)/(mn2*mn - mn*this->MR2))) +
             sqrt(6.)*A32*((mn + this->MR)*(mn + this->MR) - q2)*sqrt(-(((mn2 - 2*mn*this->MR + this->MR2 - q2)*Param::alpha)/(mn2*mn - mn*this->MR2))) +
             6*(mn2 - this->MR2 - q2)*S12*sqrt(-((((mn + this->MR)*(mn + this->MR) - q2)*Param::alpha)/(mn*(mn - this->MR)*(mn + this->MR))))))/
           (pow(((mn - this->MR)*(mn - this->MR) - q2),1.5)*pow(((mn + this->MR)*(mn + this->MR) - q2),2.5)*Param::alpha);
}


double Form_Factors_J32::setCoupling() {
    double iR = 3.0;
    if(this->isospin_by_2 == 3) iR = 1.0;
    double mpi2 = Param::mpi*Param::mpi;
    double qcm = sqrt((this->MR2 - mpi2 - this->mn2)*(this->MR2 - mpi2 - this->mn2) - 4.0*mpi2*this->mn2)/(2.0*this->MR);
    double eN = (this->MR2 + this->mn2 - mpi2)/(2.0*this->MR);
    double sign = 1.0;
    if(this->parity == "negative") sign = -1.0;

    this->coupling = (2.0*sqrt(3.0 * this->gamma*this->branching_pi_N))/sqrt((iR*(eN + sign*this->mn)*qcm*qcm*qcm)/(mpi2*this->MR*Param::pi));

    return this->coupling;
}

double Form_Factors_J32::setC5a0() {
    double c_iso = sqrt(2.0);
    if(this->isospin_by_2 == 3) c_iso = -sqrt(1.0/3.0);
    double f = this->setCoupling();
    double C5a_0 = -c_iso * sqrt(2.0) * Param::fpi * f/Param::mpi;
    std::cout<<" Gamma = " << this->gamma << "    ;   branching_pi_N = " << branching_pi_N <<"     ;   C5a_0 = "<< C5a_0<<std::endl;
    return C5a_0;
}

double Form_Factors_J32::getA12P() const {
    return a_12_p;
}

double Form_Factors_J32::getA32P() const {
    return a_32_p;
}

double Form_Factors_J32::getS12P() const {
    return s_12_p;
}

double Form_Factors_J32::getA12N() const {
    return a_12_n;
}

double Form_Factors_J32::getA32N() const {
    return a_32_n;
}

double Form_Factors_J32::getS12N() const {
    return s_12_n;
}

double Form_Factors_J32::getC3N() const {
    return c3n;
}

double Form_Factors_J32::getC4N() const {
    return c4n;
}

double Form_Factors_J32::getC5N() const {
    return c5n;
}

double Form_Factors_J32::getC3P() const {
    return c3p;
}

double Form_Factors_J32::getC4P() const {
    return c4p;
}

double Form_Factors_J32::getC5P() const {
    return c5p;
}

double Form_Factors_J32::getC3ANcP() const {
    return C3aNC_p;
}

double Form_Factors_J32::getC3ANcN() const {
    return C3aNC_n;
}

double Form_Factors_J32::getC4ANcP() const {
    return C4aNC_p;
}

double Form_Factors_J32::getC4ANcN() const {
    return C4aNC_n;
}

double Form_Factors_J32::getC5ANcP() const {
    return C5aNC_p;
}

double Form_Factors_J32::getC5ANcN() const {
    return C5aNC_n;
}

double Form_Factors_J32::getC6ANcP() const {
    return C6aNC_p;
}

double Form_Factors_J32::getC6ANcN() const {
    return C6aNC_n;
}

double Form_Factors_J32::getC3VNcP() const {
    return C3vNC_p;
}

double Form_Factors_J32::getC4VNcP() const {
    return C4vNC_p;
}

double Form_Factors_J32::getC5VNcP() const {
    return C5vNC_p;
}

double Form_Factors_J32::getC3VNcN() const {
    return C3vNC_n;
}

double Form_Factors_J32::getC4VNcN() const {
    return C4vNC_n;
}

double Form_Factors_J32::getC5VNcN() const {
    return C5vNC_n;
}

double Form_Factors_J32::getC3Pn() const {
    return c3pn;
}

double Form_Factors_J32::getC4Pn() const {
    return c4pn;
}

double Form_Factors_J32::getC5Pn() const {
    return c5pn;
}

double Form_Factors_J32::getC3VNcPn() const {
    return C3vNC_pn;
}

double Form_Factors_J32::getC4VNcPn() const {
    return C4vNC_pn;
}

double Form_Factors_J32::getC5VNcPn() const {
    return C5vNC_pn;
}

double Form_Factors_J32::getC3ANcPn() const {
    return C3aNC_pn;
}

double Form_Factors_J32::getC4ANcPn() const {
    return C4aNC_pn;
}

double Form_Factors_J32::getC5ANcPn() const {
    return C5aNC_pn;
}

double Form_Factors_J32::getC6ANcPn() const {
    return C6aNC_pn;
}

double Form_Factors_J32::getCoupling() const {
    return coupling;
}

double Form_Factors_J32::getBranchingPiN() const {
    return branching_pi_N;
}

double Form_Factors_J32::getGamma() const {
    return gamma;
}




