//
// Created by eduardo on 14/06/19.
//

#ifndef COHERENT_PHOTON_FORM_FACTORS_N1535_H
#define COHERENT_PHOTON_FORM_FACTORS_N1535_H


#include "Form_Factors_J12.h"

class Form_Factors_N1535 : public Form_Factors_J12{


public:
    Form_Factors_N1535();

    void set_Helicity_Amplitudes(double) override ;

};


#endif //COHERENT_PHOTON_FORM_FACTORS_N1535_H
