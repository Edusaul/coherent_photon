#include <utility>
#include <cmath>
#include <Parameters_GeV.h>
#include <IntegralGauss20.h>
#include <iostream>

//
// Created by edusaul on 1/04/19.
//

#include "density_profile.h"
#include "density_antz.h"

density_profile::density_profile(std::string nucleus) : nucleus(std::move(nucleus)) {
    this->set_nucleus();
    this->r2 = 0.69;
    this->pi2 = Param::pi*Param::pi;
    this->set_centers();
    this->npts = 3;
    this->rmin = 0.0;
    this->rmax = 3.0 * pow(double(this->a),1.0/3.0);

    this->antz_p = new density_antz(this->a, this->rp, this->ap);
    this->antz_n = new density_antz(this->a, this->rn, this->an);
    this->antz_p_center = new density_antz(this->a, this->rpc, this->apc);
    this->antz_n_center = new density_antz(this->a, this->rnc, this->anc);
}

density_profile::~density_profile() {
    delete this->antz_p;
    delete this->antz_n;
    delete this->antz_p_center;
    delete this->antz_n_center;
}


void density_profile::set_nucleus() {
    if(this->nucleus == "12C") {
        this->a=12;
        this->z=6;
        this->rp = 1.692;
        this->ap = 1.082;
        this->rn = this->rp;
        this->an = this->ap;
    }
    else if(this->nucleus == "16O") {
        this->a=16;
        this->z=8;
        this->rp = 1.833;
        this->ap = 1.544;
        this->rn = 1.815;
        this->an = 1.529;
    }
    else if(this->nucleus == "18O") {
        this->a=18;
        this->z=8;
        this->rp = 1.881;
        this->ap = 1.544;
        this->rn = 1.975;
        this->an = 2.048;
    }
    else if(this->nucleus == "27Al") {
        this->a=27;
        this->z=13;
        this->rp = 3.05;
        this->ap = 0.535;
        this->rn = this->rp;
        this->an = this->ap;
    }
    else if(this->nucleus == "40Ca") {
        this->a=40;
        this->z=20;
        this->rp = 3.51;
        this->ap = 0.563;
        this->rn = 3.43;
        this->an = this->ap;
    }
    else if(this->nucleus == "44Ca") {
        this->a=44;
        this->z=20;
        this->rp = 3.573;
        this->ap = 0.563;
        this->rn = 3.714;
        this->an = this->ap;
    }
    else if(this->nucleus == "56Fe") {
        this->a=56;
        this->z=26;
        this->rp = 3.971;
        this->ap = 0.5935;
        this->rn = 4.05;
        this->an = this->ap;
    }
    else if(this->nucleus == "197Au") {
        this->a=197;
        this->z=79;
        this->rp = 6.55;
        this->ap = 0.522;
        this->rn = 6.79;
        this->an = this->ap;
    }
    else if(this->nucleus == "208Pb") {
        this->a=208;
        this->z=82;
        this->rp = 6.624;
        this->ap = 0.549;
        this->rn = 6.89;
        this->an = 0.549;
    }
}

void density_profile::set_centers() {
    double x;
    if(this->a < 18) {
        // For light nuclei, harmonic oscilator densities are used
        // protons
        this->rpc = sqrt(this->rp*this->rp-2.0/3.0*this->r2);
        x = this->ap*this->rp*this->rp/(1.0+3.0/2.0*this->ap)/(this->rpc*this->rpc);
        this->apc=2.0*x/(2.0-3.0*x);
        // neutrons
        this->rnc = sqrt(this->rn*this->rn-2.0/3.0*this->r2);
        x = this->an*this->rn*this->rn/(1.0+3.0/2.0*this->an)/(this->rnc*this->rnc);
        this->anc=2.0*x/(2.0-3.0*x);
    } else {
        // Fermi liquid type
        // protons
        this->rpc = this->rp+5.0*this->r2*this->rp/(15.0*this->rp*this->rp+7.0*this->pi2*this->ap*this->ap);
        this->apc = sqrt((this->rp*this->rp*this->rp+this->pi2*this->ap*this->ap*this->rpc-this->rpc*this->rpc*this->rpc)/this->pi2/this->rpc);
        // neutrons;
        this->rnc = this->rn+5.*r2*this->rn/(15.*this->rn*this->rn+7.*this->pi2*this->an*this->an);
        this->anc = sqrt((this->rn*this->rn*this->rn+this->pi2*this->an*this->an*this->rnc-this->rnc*this->rnc*this->rnc)/this->pi2/this->rnc);
    }

//    std::cout<<"rpc   "<<rpc<<"  "<<apc<<std::endl;

}

double density_profile::get_rho(density_antz *antz, double r) {

    auto normalization = IntegralGauss20::integrate<double>(antz, this->rmin, this->rmax, this->npts);
    double rho0_mat = double(this->z)/normalization;

//    std::cout<<"----    "<<r<<"   " <<rho0_mat<<"  "<<antz->antz(r)<<std::endl;

    return rho0_mat*antz->antz(r);
}

double density_profile::get_rhop(double r) {
    return this->get_rho(this->antz_p, r);
}

double density_profile::get_rhon(double r) {
    return this->get_rho(this->antz_n, r);
}

double density_profile::get_rhop_cent(double r) {
    return this->get_rho(this->antz_p_center, r);
}

double density_profile::get_rhon_cent(double r) {
    return this->get_rho(this->antz_n_center, r);
}

int density_profile::getA() const {
    return a;
}

double density_profile::getRmax() const {
    return rmax;
}


