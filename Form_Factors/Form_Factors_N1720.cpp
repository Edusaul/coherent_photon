//
// Created by eduardo on 4/07/19.
//

#include <Parameters_GeV.h>
#include "Form_Factors_N1720.h"

Form_Factors_N1720::Form_Factors_N1720() {
    this->parity = "positive";
    this->isospin_by_2 = 1;
    this->set_aNC();

    this->mn = Param::mp;
    this->mn2 = Param::mp2;
    this->MR = Param::mN1720;
    this->MR2 = this->MR*this->MR;
    this->gamma = Param::Gamma_N1720;
    this->branching_pi_N = Param::branching_pi_N_N1720;
    this->c5a0 = this->setC5a0();
    this->MA_R_2 = Param::MA_c5a*Param::MA_c5a;
}

void Form_Factors_N1720::set_Helicity_Amplitudes(double Q2) {
    this->a_12_p =73.*1e-3*(1.+1.89*Q2)*exp(-1.55*Q2);
    this->a_32_p =-11.5*1e-3*(1.+10.83*Q2-0.66*Q2*Q2)*exp(-0.43*Q2);
    this->s_12_p =-53.0*1e-3*(1.+2.46*Q2)*exp(-1.55*Q2);

    this->a_12_n =-2.9*1e-3*(1.+12.70*Q2)*exp(-1.55*Q2);
    this->a_32_n =-31.*1e-3*(1.+5.*Q2)*exp(-1.55*Q2);
    this->s_12_n =0.0;
}
