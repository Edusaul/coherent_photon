//
// Created by eduardo on 14/06/19.
//

#include <Parameters_GeV.h>
#include "Form_Factors_N1535.h"

Form_Factors_N1535::Form_Factors_N1535() {
    this->parity = "negative";
    this->isospin_by_2 = 1;
    this->set_aNC();

    this->MR= Param::mN1535;
    this->MR2= this->MR*this->MR;
    this->mn= Param::mp;
    this->mn2= Param::mp2;
    this->gamma = Param::Gamma_N1535;
    this->branching_pi_N = Param::branching_pi_N_N1535;
    this->Fa0 = this->setFa0();
    this->MA_R_2 = Param::MA_R_2;
}

void Form_Factors_N1535::set_Helicity_Amplitudes(double Q2) {

    this->a_p = 66.0 * 1.0e-3 * (1.+1.608*Q2)*exp(-0.70*Q2);
    this->s_p = -2.0 * 1.0e-3 * (1.+23.9*Q2)*exp(-0.81*Q2);
    this->a_n = -50.7* 1.0e-3 * (1.+4.75*Q2)*exp(-1.69*Q2);
    this->s_n = 28.5 * 1.0e-3 * (1.+0.36*Q2)*exp(-1.55*Q2);
}
