//
// Created by edusaul on 8/04/19.
//

#include "Nuclear_FF.h"

const std::complex<double> &Nuclear_FF::getFfP() const {
    return FF_p;
}

const std::complex<double> &Nuclear_FF::getFfN() const {
    return FF_n;
}
