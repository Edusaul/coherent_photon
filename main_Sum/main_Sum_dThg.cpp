//
// Created by eduardo on 1/07/19.
//

#include <Measure_time.h>
#include <Arguments_base.h>
#include "Admin_of_Clases_Sum_dThg.h"

int main(int argc, char** argv) {
    Measure_time time;

//    std::string distribution_type = "dsdthg_Sum_Delta_N1520_D1700";
    std::string distribution_type = "dsdthg_Sum_All";

    Arguments_base parameters(argc, argv, distribution_type);

    Admin_of_Clases_Sum_dThg admin(&parameters);

    admin.write_dsdthg();

    time.getTime();
    return 0;
}