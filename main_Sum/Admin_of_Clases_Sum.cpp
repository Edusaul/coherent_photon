//
// Created by edusaul on 12/04/19.
//

#include <Nuclear_FF_HO.h>
#include <Hadronic_Current_R_Delta_avg.h>
#include <Hadronic_Current_R_Sum.h>
#include <dsdEg_dphig_dthg_dth.h>
#include <dsdEg_dphig_dthg.h>
#include <dsdEg_dphig.h>
#include <dsdEg.h>
#include <Nucleus_FF_DeVries.h>
#include <Hadronic_Current_R_N1440.h>
#include <Hadronic_Current_R_N1520.h>
#include <Hadronic_Current_R_N1535.h>
#include <Hadronic_Current_R_N1650.h>
#include <Hadronic_Current_R_D1620.h>
#include <Hadronic_Current_R_N1675.h>
#include <Hadronic_Current_R_N1680.h>
#include <Hadronic_Current_R_D1700.h>
#include <Hadronic_Current_R_D1905.h>
#include <Hadronic_Current_R_N1720.h>
#include <Hadronic_Current_R_D1910.h>
#include <Hadronic_Current_R_D1950.h>
#include "Admin_of_Clases_Sum.h"

Admin_of_Clases_Sum::Admin_of_Clases_Sum(Arguments_base *parameters) : Admin_of_Clases_base(parameters) {
    this->nuclearFF = new Nucleus_FF_DeVries(this->parameters->getNucleus());
//    this->nuclearFF = new Nuclear_FF_HO;

    this->vector_of_currents.push_back(new Hadronic_Current_R_Delta_avg(this->nuclearFF));
//    this->vector_of_currents.push_back(new Hadronic_Current_R_N1440(this->nuclearFF));
    this->vector_of_currents.push_back(new Hadronic_Current_R_N1520(this->nuclearFF));
//    this->vector_of_currents.push_back(new Hadronic_Current_R_N1535(this->nuclearFF));
    
//    this->vector_of_currents.push_back(new Hadronic_Current_R_D1620(this->nuclearFF));
//    this->vector_of_currents.push_back(new Hadronic_Current_R_N1650(this->nuclearFF));
//    this->vector_of_currents.push_back(new Hadronic_Current_R_N1675(this->nuclearFF));
//    this->vector_of_currents.push_back(new Hadronic_Current_R_N1680(this->nuclearFF));
    this->vector_of_currents.push_back(new Hadronic_Current_R_D1700(this->nuclearFF));
//    this->vector_of_currents.push_back(new Hadronic_Current_R_N1720(this->nuclearFF));
//    this->vector_of_currents.push_back(new Hadronic_Current_R_D1905(this->nuclearFF));
//    this->vector_of_currents.push_back(new Hadronic_Current_R_D1910(this->nuclearFF));
//    this->vector_of_currents.push_back(new Hadronic_Current_R_D1950(this->nuclearFF));
    
    this->current_R = new Hadronic_Current_R_Sum(this->vector_of_currents);

    this->create_dsdEg_clases();
}

Admin_of_Clases_Sum::~Admin_of_Clases_Sum() {
    delete this->dsdk0;
    delete this->dsdk0_dphig;
    delete this->dsdk0_dphig_dthg;
    delete this->dsdEgamma_dphig_dthg_dth;
    for (int i = 0; i < this->vector_of_currents.size(); ++i) {
        delete this->vector_of_currents[i];
    }
    delete this->current_R;
    delete this->nuclearFF;
}
