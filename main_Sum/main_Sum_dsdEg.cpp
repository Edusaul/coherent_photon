//
// Created by edusaul on 12/04/19.
//

#include <Measure_time.h>
#include "Arguments_base.h"
#include "Admin_of_Clases_Sum.h"

int main(int argc, char** argv) {
    Measure_time time;

    std::string distribution_type = "dsdEg_Sum_Delta_N1520_D1700";

    Arguments_base parameters(argc, argv, distribution_type);

    Admin_of_Clases_Sum admin(&parameters);

    admin.write_dsdEgamma();

    time.getTime();
    return 0;
}