//
// Created by edusaul on 12/04/19.
//

#ifndef COHERENT_PHOTON_ADMIN_OF_CLASES_SUM_H
#define COHERENT_PHOTON_ADMIN_OF_CLASES_SUM_H


#include <Admin_of_Clases_base.h>
#include <Nuclear_FF.h>
#include <vector>
#include "Hadronic_Current_R.h"

class Admin_of_Clases_Sum : public Admin_of_Clases_base {
protected:
    std::string nucleon;
    Nuclear_FF *nuclearFF;
    std::vector<Hadronic_Current_R*> vector_of_currents;

public:
    explicit Admin_of_Clases_Sum(Arguments_base *parameters);

    ~Admin_of_Clases_Sum() override;

};


#endif //COHERENT_PHOTON_ADMIN_OF_CLASES_SUM_H
