//
// Created by eduardo on 1/07/19.
//

#include <Nucleus_FF_DeVries.h>
#include <Hadronic_Current_R_Delta_avg.h>
#include <Hadronic_Current_R_N1520.h>
#include <Hadronic_Current_R_N1535.h>
#include <Hadronic_Current_R_N1440.h>
#include <Hadronic_Current_R_Sum.h>
#include <dsdEg_dphig_dthg_dth.h>
#include <dsdThG_dEg_dphig.h>
#include <dsdThG_dEg.h>
#include <dsdThGamma.h>
#include <Hadronic_Current_R_D1620.h>
#include <Hadronic_Current_R_N1650.h>
#include <Hadronic_Current_R_N1675.h>
#include <Hadronic_Current_R_N1680.h>
#include <Hadronic_Current_R_D1700.h>
#include <Hadronic_Current_R_N1720.h>
#include <Hadronic_Current_R_D1905.h>
#include <Hadronic_Current_R_D1910.h>
#include <Hadronic_Current_R_D1950.h>
#include "Admin_of_Clases_Sum_dThg.h"

Admin_of_Clases_Sum_dThg::Admin_of_Clases_Sum_dThg(Arguments_base *parameters) : Admin_of_Clases_base(parameters) {
    this->nuclearFF = new Nucleus_FF_DeVries(this->parameters->getNucleus());
//    this->nuclearFF = new Nuclear_FF_HO;
    int precision = 10;

    this->vector_of_currents.push_back(new Hadronic_Current_R_Delta_avg(this->nuclearFF));
    this->vector_of_currents.push_back(new Hadronic_Current_R_N1440(this->nuclearFF));
    this->vector_of_currents.push_back(new Hadronic_Current_R_N1520(this->nuclearFF));
    this->vector_of_currents.push_back(new Hadronic_Current_R_N1535(this->nuclearFF));

    this->vector_of_currents.push_back(new Hadronic_Current_R_D1620(this->nuclearFF));
    this->vector_of_currents.push_back(new Hadronic_Current_R_N1650(this->nuclearFF));
    this->vector_of_currents.push_back(new Hadronic_Current_R_N1675(this->nuclearFF));
    this->vector_of_currents.push_back(new Hadronic_Current_R_N1680(this->nuclearFF));
    this->vector_of_currents.push_back(new Hadronic_Current_R_D1700(this->nuclearFF));
    this->vector_of_currents.push_back(new Hadronic_Current_R_N1720(this->nuclearFF));
    this->vector_of_currents.push_back(new Hadronic_Current_R_D1905(this->nuclearFF));
    this->vector_of_currents.push_back(new Hadronic_Current_R_D1910(this->nuclearFF));
    this->vector_of_currents.push_back(new Hadronic_Current_R_D1950(this->nuclearFF));

    this->current_R = new Hadronic_Current_R_Sum(this->vector_of_currents);

    this->create_dsdThg_clases();
}


Admin_of_Clases_Sum_dThg::~Admin_of_Clases_Sum_dThg() {
    delete this->dsdthg;
    delete this->dsdthg_dEg;
    delete this->dsdthg_dEg_dphig;
    delete this->dsdEgamma_dphig_dthg_dth;
    for (int i = 0; i < this->vector_of_currents.size(); ++i) {
        delete this->vector_of_currents[i];
    }
    delete this->current_R;
    delete this->nuclearFF;
}
