//
// Created by eduardo on 1/07/19.
//

#ifndef COHERENT_PHOTON_ADMIN_OF_CLASES_SUM_DTHG_H
#define COHERENT_PHOTON_ADMIN_OF_CLASES_SUM_DTHG_H


#include <Admin_of_Clases_base.h>
#include <Nuclear_FF.h>

class Admin_of_Clases_Sum_dThg : public Admin_of_Clases_base{
protected:
    std::string nucleon;
    Nuclear_FF *nuclearFF;
    std::vector<Hadronic_Current_R*> vector_of_currents;

public:
    explicit Admin_of_Clases_Sum_dThg(Arguments_base *parameters);

    virtual ~Admin_of_Clases_Sum_dThg();
};


#endif //COHERENT_PHOTON_ADMIN_OF_CLASES_SUM_DTHG_H
