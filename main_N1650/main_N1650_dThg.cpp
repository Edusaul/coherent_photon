//
// Created by eduardo on 12/07/19.
//

#include <Measure_time.h>
#include <Arguments_base.h>
#include "Admin_of_Clases_N1650_dThg.h"

int main(int argc, char** argv) {
    Measure_time time;

    std::string distribution_type = "dsdthg_N1650";

    Arguments_base parameters(argc, argv, distribution_type);

    Admin_of_Clases_N1650_dThg admin(&parameters);

    admin.write_dsdthg();

    time.getTime();
    return 0;
}