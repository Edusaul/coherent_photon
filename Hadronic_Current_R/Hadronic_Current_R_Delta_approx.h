//
// Created by edusaul on 8/04/19.
//

#ifndef COHERENT_PHOTON_HADRONIC_CURRENT_R_DELTA_APPROX_H
#define COHERENT_PHOTON_HADRONIC_CURRENT_R_DELTA_APPROX_H


#include "Hadronic_Current_R_Delta.h"

class Hadronic_Current_R_Delta_approx  : public Hadronic_Current_R_Delta {
protected:
    Nuclear_FF *nuclearFF;

public:
    explicit Hadronic_Current_R_Delta_approx(Nuclear_FF *nuclearFf);

    std::complex<double> getR(int, int) override;

    void setKg(const std::vector<double> &kg) override;

    void setP(std::vector<double>) override;

    std::complex<double> Propagator_Delta(std::vector<double>) override;

    double gamma_Delta_to_pi(double);

};


#endif //COHERENT_PHOTON_HADRONIC_CURRENT_R_DELTA_APPROX_H
