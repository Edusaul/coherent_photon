//
// Created by eduardo on 3/07/19.
//

#ifndef COHERENT_PHOTON_HADRONIC_CURRENT_TR_J12_H
#define COHERENT_PHOTON_HADRONIC_CURRENT_TR_J12_H


#include <Nuclear_FF.h>
#include <Form_Factors_J12.h>
#include "Hadronic_Current_R.h"

class Hadronic_Current_tr_J12 : public Hadronic_Current_R {
protected:
    Form_Factors_J12 *ff_Res;
    Nuclear_FF *nuclearFF;
    std::string parity;
    int isospin_by_2;

    double mRes;
    double mRes_2;
    double gamma;

    double F1p;
    double F2p;
    double F1n;
    double F2n;
    double F1pn;
    double F2pn;

    double F1;
    double F2;
    double F1NC;
    double F2NC;
    double FANC;
    double FPNC;
    std::complex<double> N_FF_p;
    std::complex<double> N_FF_n;

    double qv_minus_kgv;
    std::complex<double> propagator_dir;
    std::complex<double> propagator_crs;

    void set_tr_dir(Array4x4&) override;
    void set_tr_crs(Array4x4&) override;

    void set_tr_dir_positive(Array4x4&);
    void set_tr_crs_positive(Array4x4&);

    void set_tr_dir_negative(Array4x4&);
    void set_tr_crs_negative(Array4x4&);

    void set_EM_FF();

    double Gamma_pi_N(double W2);

public:

    explicit Hadronic_Current_tr_J12(Nuclear_FF *nuclearFf);

    std::complex<double> getR(int, int) override;

    void setFF(double) override;

    void setKg(const std::vector<double> &kg) override;

    std::complex<double> Propagator(std::vector<double>);

    void setP(std::vector<double>) override;

};


#endif //COHERENT_PHOTON_HADRONIC_CURRENT_TR_J12_H
