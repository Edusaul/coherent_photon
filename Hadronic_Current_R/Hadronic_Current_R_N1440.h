//
// Created by edusaul on 19/04/19.
//

#ifndef COHERENT_PHOTON_HADRONIC_CURRENT_R_N1440_H
#define COHERENT_PHOTON_HADRONIC_CURRENT_R_N1440_H

#include "Hadronic_Current_tr_J12.h"
#include <Form_Factors_N1440.h>
#include <Nuclear_FF.h>

class Hadronic_Current_R_N1440 : public Hadronic_Current_tr_J12 {


public:

    explicit Hadronic_Current_R_N1440(Nuclear_FF *nuclearFf);

    virtual ~Hadronic_Current_R_N1440();

};


#endif //COHERENT_PHOTON_HADRONIC_CURRENT_R_N1440_H
