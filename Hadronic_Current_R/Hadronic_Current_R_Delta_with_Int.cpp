//
// Created by edusaul on 26/03/19.
//

#include <iostream>
#include <algorithm>
#include <complex>
#include <IntegralGauss20.h>
#include <Parameters_GeV.h>
#include "Hadronic_Current_R_Delta_with_Int.h"

Hadronic_Current_R_Delta_with_Int::Hadronic_Current_R_Delta_with_Int(double rmax,
                                                                     Delta_in_medium *integralInR)
        : rmax(rmax), integral_in_r(integralInR) {
    this->npts = 1;
    this->rmin = 0.0;
}

Hadronic_Current_R_Delta_with_Int::Hadronic_Current_R_Delta_with_Int(double npts, double rmax,
                                                                     Delta_in_medium *integralInR)
        : npts(npts), rmax(rmax), integral_in_r(integralInR) {
    this->rmin = 0.0;
}

std::complex<double> Hadronic_Current_R_Delta_with_Int::getR(int i, int j) {
    // the propagator includes the nuclear FF
    std::complex<double> tr_p = this->tr_p_dir[i][j]*this->propagator_dir_p
                                + this->tr_p_crs[i][j]*this->propagator_crs_p;
    std::complex<double> tr_n = this->tr_n_dir[i][j]*this->propagator_dir_n
                                + this->tr_n_crs[i][j]*this->propagator_crs_n;

//    std::cout<<" ----  "<<this->tr_p_dir[i][j]<<"   "<<this->propagator_dir<<std::endl;

    return (tr_p + tr_n) * this->mn / this->p0 / 2.0; //Factor 1/2 from the trace, see notes
//    return (tr_p ) * this->mn / this->p0 / 2.0; //Factor 1/2 from the trace, see notes
}


void Hadronic_Current_R_Delta_with_Int::setKg(const std::vector<double> &kg) {
    Hadronic_Current_R_Delta::setKg(kg);

//    this->C3vNC = this->ff_Delta->getC3vNC();
//    this->C4vNC = this->ff_Delta->getC4vNC();
//    this->C5vNC = this->ff_Delta->getC5vNC();
//    this->C3aNC = this->ff_Delta->getC3aNC();
//    this->C4aNC = this->ff_Delta->getC4aNC();
//    this->C5aNC = this->ff_Delta->getC5aNC();
//    this->set_tr_dir(tr_p_dir);
//    this->set_tr_crs(tr_p_crs);
//
//    this->set_tr_dir(tr_n_dir);
//    this->set_tr_crs(tr_n_crs);
//
//    std::vector<double> pm = this->q;
//    std::transform(pm.begin( ), pm.end( ), this->kg.begin( ), pm.begin( ),std::minus<>( ));
//    this->q_minus_kg = sqrt(pm[1]*pm[1]+pm[2]*pm[2]+pm[3]*pm[3]);


    this->integral_in_r->setNucleon("p");
    this->integral_in_r->setDirOrCrs("dir");
    this->propagator_dir_p = Propagator_Delta(this->p_dir);
    this->integral_in_r->setDirOrCrs("crs");
    this->propagator_crs_p = Propagator_Delta(this->p_crs);

    this->integral_in_r->setNucleon("n");
    this->integral_in_r->setDirOrCrs("dir");
    this->propagator_dir_n = Propagator_Delta(this->p_dir);
    this->integral_in_r->setDirOrCrs("crs");
    this->propagator_crs_n = Propagator_Delta(this->p_crs);

}

void Hadronic_Current_R_Delta_with_Int::setP(std::vector<double> p) {
    Hadronic_Current_R::setP(p);

    this->p_dir = this->p;
    std::transform(this->p_dir.begin( ), this->p_dir.end( ), this->q.begin( ), this->p_dir.begin( ),std::plus<>( ));

    this->p_crs = this->pp;
    std::transform(this->p_crs.begin( ), this->p_crs.end( ), this->q.begin( ), this->p_crs.begin( ),std::minus<>( ));


}

std::complex<double> Hadronic_Current_R_Delta_with_Int::Propagator_Delta(std::vector<double> p) {
    double p2 =  p[0] * p[0] - p[1] * p[1] - p[2] * p[2] - p[3] * p[3] ;

    std::vector<std::complex<double>> param(2);
    param[0] = p2;
    param[1] = this->q_minus_kg;

    this->integral_in_r->change_other_parameters(param);
    auto int_r = IntegralGauss20::integrate<std::complex<double>>(this->integral_in_r, this->rmin, this->rmax, this->npts);

    std::complex<double> res = 4.0 * Param::pi / this->q_minus_kg * int_r;

//    std::cout<<"    p2  = "<<p2<<"  ;  q-kg  = "<<this->q_minus_kg<<" ; res = "<<res<<std::endl;


//    double real_sigma = 0.0; // real part of the Delta selfenergy
//    double fr = 0.47; // average density in units of rho0
//    double gamspr0=Param::Delta_V0; // in meadium spreading at rho0
//    double gamspr=gamspr0*fr;
//
//    if(p2<this->mn2) {
//        res *= 1.0/(p2 - this->mDelta2);
//    } else {
//        res *= 1.0/(p2 - (this->mDelta + real_sigma)*(this->mDelta + real_sigma) +
//                   this->c_i * (this->mDelta + real_sigma)*(this->gamma_Delta_to_pi(p2) + gamspr));
//    }


    return res;
}












