#include <utility>
#include <iostream>

//
// Created by edusaul on 12/04/19.
//

#include "Hadronic_Current_R_Sum.h"

Hadronic_Current_R_Sum::Hadronic_Current_R_Sum(std::vector<Hadronic_Current_R *> vectorOfCurrents)
        : vector_of_currents(std::move(vectorOfCurrents)) {}


void Hadronic_Current_R_Sum::setQ(const std::vector<double> &q) {
    for (int i = 0; i < this->vector_of_currents.size(); ++i) {
        vector_of_currents[i]->setQ(q);
    }
}

void Hadronic_Current_R_Sum::setKg(const std::vector<double> &kg) {
    for (int i = 0; i < this->vector_of_currents.size(); ++i) {
        vector_of_currents[i]->setKg(kg);
    }
}

void Hadronic_Current_R_Sum::setP(std::vector<double> p) {
    for (int i = 0; i < this->vector_of_currents.size(); ++i) {
        vector_of_currents[i]->setP(p);
    }
}

std::complex<double> Hadronic_Current_R_Sum::getR(int i, int j) {
    std::complex<double> R  (0.0,0.0);
    for (int k = 0; k < this->vector_of_currents.size(); ++k) {
        R += vector_of_currents[k]->getR(i,j);
    }
    return R;
}

void Hadronic_Current_R_Sum::setFF(double p) {
    for (int i = 0; i < this->vector_of_currents.size(); ++i) {
        vector_of_currents[i]->setFF(p);
    }
}





