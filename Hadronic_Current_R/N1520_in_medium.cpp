//
// Created by edusaul on 28/04/19.
//

#include <Parameters_GeV.h>
#include "N1520_in_medium.h"

N1520_in_medium::N1520_in_medium() {
    this->mN1520 = Param::mDelta;
    this->mN1520_2 = Param::mDelta*Param::mDelta;
    this->mn = Param::mp;
    this->mn2 = Param::mp2;
    this->mpi = Param::mpi;
    this->V0 = Param::Delta_V0;
    this->coupling_N1520_Npi = Param::coupling_DeltaNpi;
    this->c_i = {0,1};
    this->hc3 = Param::hc * Param::hc * Param::hc;
    this->dir_or_crs = "dir";

//    this->rho = new density_profile(this->nucleus);
//    this->rho0 = (this->rho->get_rhop_cent(0) + this->rho->get_rhon_cent(0)) * this->hc3;
    this->rho0 = Param::rho0;

    this->rho_avg = 3.0/(4.0*Param::pi*(1.2*1.2*1.2)) * this->hc3; // average density in fm^-3 * hc^3
    this->rho_r = this->rho_avg ;
}

N1520_in_medium::~N1520_in_medium() {
//    delete this->rho;
}

void N1520_in_medium::setNucleon(const std::string &nucleon) {
    N1520_in_medium::nucleon = nucleon;
}

std::complex<double> N1520_in_medium::integrand(double r) {
//    std::complex<double> propagator;
//    if(this->dir_or_crs == "crs") propagator = this->propagator_crs;
//    else propagator = this->propagator_avg;
//
//    double rho_r_N = 0.0;
//    if(nucleon == "p") {
//        rho_r_N = this->rho->get_rhop_cent(r) * this->hc3;
//    } else if(nucleon == "n") rho_r_N = this->rho->get_rhon_cent(r) * this->hc3;
//
//    return (r/Param::hc * sin(this->q_minus_kg * r/Param::hc) * rho_r_N * propagator)/Param::hc;
    return 0;
}

std::complex<double> N1520_in_medium::N1520_propagator_avg_dir() {

    std::complex<double> sigma = this->Sigma();

    std::complex<double> D_Delta_med = 1.0/(this->p2 - (this->mN1520 + sigma.real() )*(this->mN1520 + sigma.real() )
                                            + this->c_i * (this->mN1520 + sigma.real()) * (Gamma_tilde_N1520(this->rho_r) - 2.0 * sigma.imag() ) );

    return D_Delta_med;
}

std::complex<double> N1520_in_medium::N1520_propagator_crs() {
    return 1.0/(this->p2 - this->mN1520_2 + this->c_i * this->mN1520 * this->Gamma_vacuum(this->p2));
}

void N1520_in_medium::change_other_parameters(std::vector<std::complex<double>> param) {
    this->p2 = param[0].real();
    this->q_minus_kg = param[1].real();

//    std::cout<<"                "<<p2<<"  "<<q_minus_kg<<std::endl;

    this->p = sqrt(this->p2);
    this->qcm = sqrt(this->lambda_func(p2, this->mpi*this->mpi, this->mn*this->mn))/(2.0 * this->p);
    this->gamma_vac = this->Gamma_vacuum(this->p2);

    this->propagator_crs = this->N1520_propagator_crs();

    this->propagator_avg = this->N1520_propagator_avg_dir();
}

std::complex<double> N1520_in_medium::Sigma() {
    double reSigma = 0.0;

    double imSigma = - 1.0/2.0 * this->V0 * this->rho_r/this->rho0;

    std::complex<double> Sigma(reSigma, imSigma);
    return Sigma;
}

double N1520_in_medium::Gamma_tilde_N1520(double rho_r) {
    double q_tilde = this->qcm/this->kf(rho_r);
    return this->gamma_vac * this->I_series(q_tilde);
}

double N1520_in_medium::Gamma_vacuum(double p2) {
    if(p2 > (this->mn + this->mpi)*(this->mn + this->mpi)) {

        return 1.0/(6.0*Param::pi) * (this->coupling_N1520_Npi/this->mpi)*(this->coupling_N1520_Npi/this->mpi)
               *this->mn/sqrt(p2) * this->qcm*qcm*qcm;
    }else return 0;
}

double N1520_in_medium::I_series(double q) {
    double res = 1.0;

    if (q != 0) {
        if (q > 1.0) res += -2.0 / (5.0 * q * q) + 9.0 / (35.0 * pow(q, 4)) - 2.0 / (21.0 * pow(q, 6));
        else if (q < 1.0) res += 34.0 / 35.0 * q - 22.0 / 105.0 * q * q * q - 1.0;
    }

    return res;
}

double N1520_in_medium::lambda_func(double x, double y, double z) {
    return x*x + y*y + z*z - 2.0*x*y - 2.0*y*z - 2.0*x*z;
}

double N1520_in_medium::kf(double rho_r) {
    return pow(3.0 * Param::pi*Param::pi * rho_r/2.0, 1.0/3.0);
}

//int N1520_in_medium::getA() const {
//    return this->rho->getA();
//}
//
//double N1520_in_medium::getRmax() const {
//    return this->rho->getRmax();
//}

//double N1520_in_medium::getRho_r(double r) {
//    if(nucleon == "p") {
//        this->rho_r = this->rho->get_rhop_cent(r);
//    } else if(nucleon == "n") this->rho_r = this->rho->get_rhon_cent(r);
//    return this->rho_r;
//}

void N1520_in_medium::setDirOrCrs(const std::string &dirOrCrs) {
    this->dir_or_crs = dirOrCrs;
}




