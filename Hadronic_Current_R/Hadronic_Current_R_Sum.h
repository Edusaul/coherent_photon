//
// Created by edusaul on 12/04/19.
//

#ifndef COHERENT_PHOTON_HADRONIC_CURRENT_R_SUM_H
#define COHERENT_PHOTON_HADRONIC_CURRENT_R_SUM_H

#include <vector>
#include "Hadronic_Current_R.h"

class Hadronic_Current_R_Sum : public Hadronic_Current_R {
protected:
    std::vector<Hadronic_Current_R*> vector_of_currents;

public:
    explicit Hadronic_Current_R_Sum(std::vector<Hadronic_Current_R *> vectorOfCurrents);

    void setQ(const std::vector<double> &q) override;

    void setKg(const std::vector<double> &kg) override;

    void setP(std::vector<double>) override;

    std::complex<double> getR(int, int) override;

    void setFF(double) override;

};


#endif //COHERENT_PHOTON_HADRONIC_CURRENT_R_SUM_H
