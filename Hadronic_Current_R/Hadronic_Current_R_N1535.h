//
// Created by eduardo on 14/06/19.
//

#ifndef COHERENT_PHOTON_HADRONIC_CURRENT_R_N1535_H
#define COHERENT_PHOTON_HADRONIC_CURRENT_R_N1535_H

#include <Form_Factors_N1535.h>
#include <Nuclear_FF.h>
#include "Hadronic_Current_tr_J12.h"

class Hadronic_Current_R_N1535 : public Hadronic_Current_tr_J12 {

public:

    explicit Hadronic_Current_R_N1535(Nuclear_FF *nuclearFf);

    virtual ~Hadronic_Current_R_N1535();

};


#endif //COHERENT_PHOTON_HADRONIC_CURRENT_R_N1535_H
