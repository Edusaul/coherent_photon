//
// Created by edusaul on 5/04/19.
//

#include <Parameters_GeV.h>
#include <iostream>
#include "Delta_in_medium_approx.h"

Delta_in_medium_approx::Delta_in_medium_approx(const std::string &nucleus, const std::string &nucleon) : Delta_in_medium(nucleus, nucleon) {
    this->real_sigma = 0.0; // real part of the Delta selfenergy
    this->fr = 0.47; // average density in units of rho0
    this->gamspr0=Param::Delta_V0; // in meadium spreading at rho0
    this->gamspr=gamspr0*fr;
}

std::complex<double> Delta_in_medium_approx::Delta_propagator_dir(double r) {

    std::complex<double> res;
    if(this->p2<this->mn2) {
        res = 1.0/(this->p2 - this->mDelta2);
    } else {
        res = 1.0/(this->p2 - (this->mDelta + this->real_sigma)*(this->mDelta + this->real_sigma) +
                   this->c_i * (this->mDelta + this->real_sigma)*(this->gamma_vac  + this->gamspr));
    }
//    std::cout<<this->p2<<"   "<<this->mn2<<std::endl;
//    std::cout<<r<<"   "<<this->gamma_vac  + this->gamspr<<"   "<<res<<std::endl;

    return res;
}






