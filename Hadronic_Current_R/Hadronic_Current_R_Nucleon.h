//
// Created by edusaul on 11/03/19.
//

#ifndef COHERENT_PHOTON_HADRONIC_CURRENT_R_NUCLEON_H
#define COHERENT_PHOTON_HADRONIC_CURRENT_R_NUCLEON_H


#include <Nuclear_FF.h>
#include "Hadronic_Current_R.h"
#include "Form_Factors_Nucleon.h"

class Hadronic_Current_R_Nucleon : public Hadronic_Current_R {
protected:
    Form_Factors_Nucleon_EM *ff_nucleon;
    Nuclear_FF *nuclearFF;

    double mn;
    double mn2;

    double F1p;
    double F2p;
    double F1n;
    double F2n;
    
    double F1;
    double F2;
    double F1NC;
    double F2NC;
    double FANC;
    double FPNC;
    std::complex<double> N_FF_p;
    std::complex<double> N_FF_n;

    double qv_minus_kgv;
    double propagator_dir;
    double propagator_crs;

    void set_tr_dir(Array4x4&) override;

    void set_tr_crs(Array4x4&) override;

public:

    explicit Hadronic_Current_R_Nucleon(Nuclear_FF *nuclearFf);

    virtual ~Hadronic_Current_R_Nucleon();

    std::complex<double> getR(int, int) override;

    void setFF(double) override;

    void setKg(const std::vector<double> &kg) override;

    double Propagator_nucleon(std::vector<double>);

    void setP(std::vector<double>) override;
};


#endif //COHERENT_PHOTON_HADRONIC_CURRENT_R_NUCLEON_H


