//
// Created by edusaul on 29/03/19.
//

#ifndef COHERENT_PHOTON_DELTA_IN_MEDIUM_H
#define COHERENT_PHOTON_DELTA_IN_MEDIUM_H


#include <Integrable_w_change_param.h>
#include <complex>
#include <density_profile.h>

class Delta_in_medium : public Integrable_w_change_param<std::complex<double>>{
protected:
    std::string nucleus;
    std::string nucleon;
    std::string dir_or_crs;
    double p;
    double p2;
    double q_minus_kg;
    double qcm;
    double mDelta;
    double mDelta2;
    double mn;
    double mn2;
    double mpi;
    double V0;
    double rho0;
    double coupling_DeltaNpi;
    std::complex<double>c_i = {0,1};
    double hc3;

    double gamma_vac;
    double rho_r;

    density_profile *rho;
    
    std::complex<double> propagator_crs;

public:

    Delta_in_medium(std::string nucleus, std::string nucleon);

    virtual ~Delta_in_medium();

    void setNucleon(const std::string &nucleon);

    std::complex<double> integrand (double r) override;

    virtual std::complex<double> Delta_propagator_dir(double);

    virtual std::complex<double> Delta_propagator_crs();

    void change_other_parameters(std::vector<std::complex<double>>) override;

    std::complex<double> Sigma();

    double Gamma_tilde_Delta(double);

    double Gamma_vacuum(double);

    double I_series(double);

    double lambda_func(double, double, double);

    double kf(double);

    int getA() const;

    double getRmax() const;

    double getRho_r(double);

    void setDirOrCrs(const std::string &dirOrCrs);

};


#endif //COHERENT_PHOTON_DELTA_IN_MEDIUM_H
