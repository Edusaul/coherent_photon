//
// Created by edusaul on 8/04/19.
//

#include <Parameters_GeV.h>
#include <iostream>
#include "Delta_in_medium_with_Int_avg.h"

Delta_in_medium_with_Int_avg::Delta_in_medium_with_Int_avg(const std::string &nucleus, const std::string &nucleon)
        : Delta_in_medium(nucleus, nucleon) {
    this->rho_avg = 3.0/(4.0*Param::pi*(1.2*1.2*1.2)) * this->hc3; // average density in fm^-3 * hc^3
    this->rho_r = this->rho_avg ;
}

std::complex<double> Delta_in_medium_with_Int_avg::integrand(double r) {
    std::complex<double> propagator;
    if(this->dir_or_crs == "crs") propagator = this->propagator_crs;
    else propagator = this->propagator_avg;

    double rho_r_N;
    if(nucleon == "p") {
        rho_r_N = this->rho->get_rhop_cent(r) * this->hc3;
    } else if(nucleon == "n") rho_r_N = this->rho->get_rhon_cent(r) * this->hc3;

//    std::cout<<"   fffff    "<<r<<"  "<<this->q_minus_kg<<"  "<<r/Param::hc<<std::endl;
//    std::cout<<"            "<<r/Param::hc <<"  "<< sin(this->q_minus_kg * r/Param::hc) <<"  "<< rho_r_N <<"  "<< propagator<<std::endl;
//    std::cout<<"    "<<r <<"  "<< propagator<<std::endl;

//    std::cout<<"    "<<r <<"  "<< propagator.real()<<"  "<< propagator.imag()<<std::endl;

    return (r/Param::hc * sin(this->q_minus_kg * r/Param::hc) * rho_r_N * propagator)/Param::hc; // the extra /Param::hc comes from the differential units

}

std::complex<double> Delta_in_medium_with_Int_avg::Delta_propagator_avg_dir() {


    std::complex<double> sigma = this->Sigma();

    std::complex<double> D_Delta_med = 1.0/(this->p2 - (this->mDelta + sigma.real() )*(this->mDelta + sigma.real() )
                                            + this->c_i * (this->mDelta + sigma.real()) * (Gamma_tilde_Delta(this->rho_r) - 2.0 * sigma.imag() ) );

//    std::cout<<"  "<<sigma<<"    "<<Gamma_tilde_Delta(this->rho_r) - 2.0 * sigma.imag()<<"  "<<D_Delta_med<<std::endl;

    return D_Delta_med;
}

void Delta_in_medium_with_Int_avg::change_other_parameters(std::vector<std::complex<double>> param) {
    Delta_in_medium::change_other_parameters(param);
    this->propagator_avg = this->Delta_propagator_avg_dir();
}

