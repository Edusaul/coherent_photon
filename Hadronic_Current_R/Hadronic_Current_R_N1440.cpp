//
// Created by edusaul on 19/04/19.
//

#include <Parameters_GeV.h>
#include <algorithm>
#include "Hadronic_Current_R_N1440.h"

Hadronic_Current_R_N1440::Hadronic_Current_R_N1440(Nuclear_FF *nuclearFf) : Hadronic_Current_tr_J12(nuclearFf) {
    this->mRes = Param::mN1440;
    this->mRes_2 = this->mRes*this->mRes;
    this->ff_Res = new Form_Factors_N1440();
    this->gamma = Param::Gamma_N1440;

    this->parity = "positive";
    this->isospin_by_2 = 1;

    this->set_EM_FF();
}

Hadronic_Current_R_N1440::~Hadronic_Current_R_N1440() {
    delete this->ff_Res;
}


