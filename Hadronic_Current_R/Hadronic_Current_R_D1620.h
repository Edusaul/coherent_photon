//
// Created by eduardo on 5/07/19.
//

#ifndef COHERENT_PHOTON_HADRONIC_CURRENT_R_D1620_H
#define COHERENT_PHOTON_HADRONIC_CURRENT_R_D1620_H


#include "Hadronic_Current_tr_J12.h"

class Hadronic_Current_R_D1620 : public Hadronic_Current_tr_J12 {

public:

    explicit Hadronic_Current_R_D1620(Nuclear_FF *nuclearFf);

    virtual ~Hadronic_Current_R_D1620();
};


#endif //COHERENT_PHOTON_HADRONIC_CURRENT_R_D1620_H
