//
// Created by eduardo on 5/07/19.
//

#include <Parameters_GeV.h>
#include <Form_Factors_N1720.h>
#include "Hadronic_Current_R_N1720.h"

Hadronic_Current_R_N1720::Hadronic_Current_R_N1720(Nuclear_FF *nuclearFf) : Hadronic_Current_tr_J32(nuclearFf) {
    this->mRes = Param::mN1720;
    this->mRes_2 = this->mRes * this->mRes;
    this->ff_Res = new Form_Factors_N1720();
    this->gamma = Param::Gamma_N1720;

    this->parity = "positive";
    this->isospin_by_2 = 1;

    this->set_EM_FF();
}

Hadronic_Current_R_N1720::~Hadronic_Current_R_N1720() {
    delete this->ff_Res;
}
