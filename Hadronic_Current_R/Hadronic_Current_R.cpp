//
// Created by edusaul on 11/03/19.
//

#include <Parameters_GeV.h>
#include "Hadronic_Current_R.h"

Hadronic_Current_R::Hadronic_Current_R() {
    this->mn = Param::mp;
    this->mn2 = Param::mp2;
    this->mpi = Param::mpi;
    this->mpi2 = Param::mpi2;
    this->c_i = {0,1};
}

void Hadronic_Current_R::set_tr_dir(Array4x4 &tr) {}

void Hadronic_Current_R::set_tr_crs(Array4x4 &tr) {}

void Hadronic_Current_R::setQ(const std::vector<double> &q) {
    Hadronic_Current_R::q = q;
    double q2 = q[0] * q[0] - q[1] * q[1] -q[2] * q[2] -q[3] * q[3];
    this->setFF(q2);
}

void Hadronic_Current_R::setKg(const std::vector<double> &kg) {
    Hadronic_Current_R::kg = kg;
}

void Hadronic_Current_R::setP(std::vector<double> p) {
    Hadronic_Current_R::p = p;
    this->p0 = p[0];
    this->pp = {p[0], -p[1], -p[2], -p[3]};
}

std::complex<double> Hadronic_Current_R::getR(int i, int j) {
    return 0;
}

void Hadronic_Current_R::setFF(double q2) {}

double Hadronic_Current_R::q_cm(double W) {
    double W2 = W*W;
    return sqrt((W2 - this->mpi2 - this->mn2)*(W2 - this->mpi2 - this->mn2) - 4.0*this->mpi2*this->mn2)/(2.0 * W);
}









