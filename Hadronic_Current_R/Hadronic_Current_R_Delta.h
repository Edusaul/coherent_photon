//
// Created by edusaul on 17/03/19.
//

#ifndef COHERENT_PHOTON_HADRONIC_CURRENT_R_DELTA_H
#define COHERENT_PHOTON_HADRONIC_CURRENT_R_DELTA_H


#include "Hadronic_Current_R.h"
#include "Form_Factors_Delta.h"
#include <complex>
#include <Nuclear_FF.h>

class Hadronic_Current_R_Delta : public Hadronic_Current_R {
protected:
    Form_Factors_Delta *ff_Delta;

    double mDelta;
    double mDelta2;
    double q_minus_kg;

    double C3v;
    double C4v;
    double C5v;
    double C3vNC;
    double C4vNC;
    double C5vNC;
    double C3aNC;
    double C4aNC;
    double C5aNC;
    double C6aNC;

    std::complex<double> N_FF_p;
    std::complex<double> N_FF_n;

    std::complex<double> propagator_dir_p;
    std::complex<double> propagator_crs_p;
    std::complex<double> propagator_dir_n;
    std::complex<double> propagator_crs_n;

    void set_tr_dir(Array4x4&) override;

    void set_tr_crs(Array4x4&) override;

public:

    Hadronic_Current_R_Delta();

    virtual ~Hadronic_Current_R_Delta();

    void setFF(double) override;

    void setKg(const std::vector<double> &kg) override;

    virtual std::complex<double> Propagator_Delta(std::vector<double>) = 0;

    virtual double qcm(double);


};


#endif //COHERENT_PHOTON_HADRONIC_CURRENT_R_DELTA_H
