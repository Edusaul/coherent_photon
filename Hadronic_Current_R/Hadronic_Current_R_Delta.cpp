//
// Created by edusaul on 17/03/19.
//

#include <Parameters_GeV.h>
#include <algorithm>
#include <iostream>
#include "Hadronic_Current_R_Delta.h"

Hadronic_Current_R_Delta::Hadronic_Current_R_Delta() {
    this->mDelta = Param::mDelta;
    this->mDelta2 = Param::mDelta*Param::mDelta;
    this->ff_Delta = new Form_Factors_Delta();

    this->ff_Delta->setFF(0.0);
    this->C3v = this->ff_Delta->getC3v();
    this->C4v = this->ff_Delta->getC4v();
    this->C5v = this->ff_Delta->getC5v();
}

Hadronic_Current_R_Delta::~Hadronic_Current_R_Delta() {
    delete this->ff_Delta;
}


void Hadronic_Current_R_Delta::setFF(double q2) {
    double Q2 = -q2;
    this->ff_Delta->setFF(Q2);
}

void Hadronic_Current_R_Delta::setKg(const std::vector<double> &kg) {
    Hadronic_Current_R::setKg(kg);

    this->C3vNC = this->ff_Delta->getC3vNC();
    this->C4vNC = this->ff_Delta->getC4vNC();
    this->C5vNC = this->ff_Delta->getC5vNC();
    this->C3aNC = this->ff_Delta->getC3aNC();
    this->C4aNC = this->ff_Delta->getC4aNC();
    this->C5aNC = this->ff_Delta->getC5aNC();

    this->set_tr_dir(tr_p_dir);
    this->set_tr_crs(tr_p_crs);

    this->set_tr_dir(tr_n_dir);
    this->set_tr_crs(tr_n_crs);

    std::vector<double> pm = this->q;
    std::transform(pm.begin( ), pm.end( ), this->kg.begin( ), pm.begin( ),std::minus<>( ));
    this->q_minus_kg = sqrt(pm[1]*pm[1]+pm[2]*pm[2]+pm[3]*pm[3]);
}


void Hadronic_Current_R_Delta::set_tr_dir(Array4x4 &tr) {

    tr[0][0] = -(C3v*C3vNC*(mDelta*p0 + this->mn*(p0 + this->q[0]))*(4*mDelta2*this->q[1]*this->kg[1] - 4*pow(p0,2)*this->q[1]*this->kg[1] + pow(this->q[1],3)*this->kg[1] + this->q[1]*this->q[3] * this->q[3]*this->kg[1] +
                                                                     this->q[1] * this->q[1]*this->kg[1] * this->kg[1] - this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - this->q[3] * this->q[3]*this->kg[2] * this->kg[2] +
                                                                     this->q[0] * this->q[0]*(this->q[1] * this->q[1] - 3*this->q[1]*this->kg[1] + this->q[3]*(2*this->q[3] - 3*this->kg[3])) +
                                                                     this->q[3]*(4*mDelta2 - 4*pow(p0,2) + this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1])*this->kg[3] - 8*p0*this->q[0]*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3])))/
               (3.*mDelta2*pow(this->mn,3));

    tr[0][1] = (this->c_i*C3v*C5aNC*mDelta*(mDelta*p0 + this->mn*(p0 + this->q[0]))*this->q[3]*this->kg[2]*
                (8*this->mn2 - 2*this->q[0]*(p0 + this->q[0]) + this->q[1]*(this->q[1] + this->kg[1]) + this->q[3]*(this->q[3] + this->kg[3])) -
                C3v*C3vNC*this->mn*(16*mDelta2*this->mn*this->q[0]*(p0 + this->q[0])*this->kg[1] + 4*pow(mDelta,3)*(4*p0*this->q[0]*this->kg[1] + this->q[3]*(-(this->q[3]*this->kg[1]) + this->q[1]*this->kg[3])) -
                                    4*this->mn*(p0 + this->q[0])*(4*pow(p0,2)*this->q[0]*this->kg[1] + 8*p0*this->q[0] * this->q[0]*this->kg[1] + 4*pow(this->q[0],3)*this->kg[1] - this->q[0]*this->q[1] * this->q[1]*this->kg[1] +
                                                                  2*p0*this->q[3]*(-(this->q[3]*this->kg[1]) + this->q[1]*this->kg[3]) - this->q[0]*this->q[1]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3]*(-this->q[3] + this->kg[3])) -
                                                                  this->q[0]*this->kg[1]*(this->q[0] * this->q[0] + this->q[3]*(2*this->q[3] + this->kg[3]))) +
                                    mDelta*(-16*pow(p0,3)*this->q[0]*this->kg[1] - this->q[3]*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3])*(-3*this->q[0] * this->q[0] + this->q[1] * this->q[1] + 2*this->q[1]*this->kg[1] + this->q[3]*(this->q[3] + 2*this->kg[3])) -
                                            4*pow(p0,2)*(8*this->q[0] * this->q[0]*this->kg[1] + 3*this->q[3]*(-(this->q[3]*this->kg[1]) + this->q[1]*this->kg[3])) +
                                            4*p0*this->q[0]*(this->q[0] * this->q[0]*(this->q[1] - 3*this->kg[1]) + this->q[1] * this->q[1]*this->kg[1] + this->q[3]*this->kg[1]*(4*this->q[3] + this->kg[3]) + this->q[1]*(this->kg[1] * this->kg[1] - 3*this->q[3]*this->kg[3])))))/
               (12.*mDelta2*pow(this->mn,4));

    tr[0][2] = (C3v*(std::complex<double>(0,-1)*C5aNC*mDelta*(mDelta*p0 + this->mn*(p0 + this->q[0]))*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3])*
                     (8*this->mn2 - 2*this->q[0]*(p0 + this->q[0]) + this->q[1]*(this->q[1] + this->kg[1]) + this->q[3]*(this->q[3] + this->kg[3])) +
                     C3vNC*this->mn*this->kg[2]*(-16*mDelta2*this->mn*this->q[0]*(p0 + this->q[0]) + 4*pow(mDelta,3)*(-4*p0*this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3]) +
                                                 4*this->mn*(p0 + this->q[0])*(4*pow(p0,2)*this->q[0] + 8*p0*this->q[0] * this->q[0] + 3*pow(this->q[0],3) - 2*p0*(this->q[1] * this->q[1] + this->q[3] * this->q[3]) -
                                                                               this->q[0]*this->q[1]*(2*this->q[1] + this->kg[1]) - this->q[0]*this->q[3]*(2*this->q[3] + this->kg[3])) +
                                                 mDelta*(16*pow(p0,3)*this->q[0] + 4*pow(p0,2)*(8*this->q[0] * this->q[0] - 3*(this->q[1] * this->q[1] + this->q[3] * this->q[3])) +
                                                         (this->q[1] * this->q[1] + this->q[3] * this->q[3])*(-3*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1] + 2*this->q[3]*this->kg[3]) +
                                                         4*p0*this->q[0]*(3*this->q[0] * this->q[0] - this->q[1]*(4*this->q[1] + this->kg[1]) - this->q[3]*(4*this->q[3] + this->kg[3]))))))/(12.*mDelta2*pow(this->mn,4));

    tr[0][3] = (C3v*(std::complex<double>(0,-1)*C5aNC*mDelta*(mDelta*p0 + this->mn*(p0 + this->q[0]))*this->q[1]*this->kg[2]*
                     (8*this->mn2 - 2*this->q[0]*(p0 + this->q[0]) + this->q[1]*(this->q[1] + this->kg[1]) + this->q[3]*(this->q[3] + this->kg[3])) +
                     C3vNC*this->mn*(-16*mDelta2*this->mn*this->q[0]*(p0 + this->q[0])*this->kg[3] + 4*pow(mDelta,3)*(-(this->q[1]*this->q[3]*this->kg[1]) - 4*p0*this->q[0]*this->kg[3] + this->q[1] * this->q[1]*this->kg[3]) +
                                     4*this->mn*(p0 + this->q[0])*(this->q[0]*this->q[3]*(this->kg[1]*(this->q[1] + this->kg[1]) + this->kg[2] * this->kg[2]) + 8*p0*this->q[0] * this->q[0]*this->kg[3] -
                                                                   this->q[0]*(-4*pow(p0,2) + this->q[3] * this->q[3] + this->q[1]*(2*this->q[1] + this->kg[1]))*this->kg[3] + pow(this->q[0],3)*(-2*this->q[3] + 3*this->kg[3]) + 2*p0*this->q[1]*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3]))\
           + mDelta*(16*pow(p0,3)*this->q[0]*this->kg[3] + 4*pow(p0,2)*(3*this->q[1]*this->q[3]*this->kg[1] + 8*this->q[0] * this->q[0]*this->kg[3] - 3*this->q[1] * this->q[1]*this->kg[3]) +
                     this->q[1]*(-(this->q[3]*this->kg[1]) + this->q[1]*this->kg[3])*(-3*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1] + 2*this->q[3]*this->kg[3]) -
                     4*p0*this->q[0]*(-(this->q[3]*(3*this->q[1]*this->kg[1] + this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])) + this->q[0] * this->q[0]*(2*this->q[3] - 3*this->kg[3]) +
                                      (this->q[3] * this->q[3] + this->q[1]*(4*this->q[1] + this->kg[1]))*this->kg[3])))))/(12.*mDelta2*pow(this->mn,4));


    tr[1][0] = (C3v*(this->c_i*C5aNC*this->q[3]*this->kg[2]*(8*pow(mDelta,3)*this->mn*(p0 + this->q[0]) -
                                                             4*this->mn2*(p0 + this->q[0])*(2*p0*this->q[0] + this->q[0] * this->q[0] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3]) -
                                                             2*mDelta*this->mn*(p0 + this->q[0])*(8*this->mn2 + 2*p0*this->q[0] + this->q[0] * this->q[0] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3]) +
                                                             mDelta2*(8*pow(p0,3) + 12*pow(p0,2)*this->q[0] + 4*p0*this->q[0] * this->q[0] + 8*this->mn2*(-2*p0 + this->q[0]) +
                                                                      this->q[0]*(this->q[1] * this->q[1] - this->q[1]*this->kg[1] + this->q[3]*(this->q[3] - this->kg[3])) - 2*p0*(this->q[1] * this->q[1] + 2*this->q[1]*this->kg[1] + this->q[3]*(this->q[3] + 2*this->kg[3])))) -
                     2*C3vNC*this->mn*(16*mDelta2*this->mn*this->q[0]*(p0 + this->q[0])*this->q[1] +
                                       4*pow(mDelta,3)*(this->q[1]*(4*p0*this->q[0] - this->q[0] * this->q[0] + this->kg[1] * this->kg[1]) + this->q[3]*this->kg[1]*this->kg[3]) +
                                       mDelta*(-16*pow(p0,3)*this->q[0]*this->q[1] + 4*p0*this->q[0]*(pow(this->q[1],3) + 2*this->q[1] * this->q[1]*this->kg[1] - 3*this->q[1]*this->kg[1] * this->kg[1] + this->q[3]*this->kg[1]*(this->q[3] - 3*this->kg[3]) +
                                                                                                      this->q[1]*this->q[3]*(this->q[3] + this->kg[3])) + (3*this->q[0] * this->q[0] - this->q[1]*(this->q[1] + 2*this->kg[1]) - this->q[3]*(this->q[3] + 2*this->kg[3]))*(this->q[0] * this->q[0]*this->q[1] - this->kg[1]*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3])) -
                                               4*pow(p0,2)*(5*this->q[0] * this->q[0]*this->q[1] + 3*this->kg[1]*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]))) -
                                       4*this->mn*(p0 + this->q[0])*(4*pow(p0,2)*this->q[0]*this->q[1] + 6*p0*this->q[0] * this->q[0]*this->q[1] + 2*p0*this->kg[1]*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) +
                                                                     this->q[0]*(2*this->q[0] * this->q[0]*this->q[1] - pow(this->q[1],3) - 2*this->q[1] * this->q[1]*this->kg[1] + this->q[3]*this->kg[1]*(-this->q[3] + this->kg[3]) + this->q[1]*(this->kg[1] * this->kg[1] - this->q[3]*(this->q[3] + this->kg[3])))))))/
               (24.*mDelta2*pow(this->mn,4));

    tr[1][1] = (C3v*(this->c_i*C5aNC*this->q[3]*this->kg[2]*(4*pow(mDelta,3)*this->mn*(this->q[1] + this->kg[1]) + 2*this->mn2*(this->q[1] + this->kg[1])*(-(this->q[0]*(2*p0 + this->q[0])) + this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) +
                                                             mDelta*this->mn*(this->q[1] + this->kg[1])*(-4*p0*this->q[0] - 3*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1] + 2*this->q[3]*this->kg[3]) +
                                                             mDelta2*(this->q[0] * this->q[0]*(this->q[1] - this->kg[1]) + 8*this->mn2*this->kg[1] - 2*p0*this->q[0]*this->kg[1] + this->q[3] * this->q[3]*this->kg[1] +
                                                                      4*pow(p0,2)*(this->q[1] + this->kg[1]) - this->q[1]*this->q[3]*this->kg[3])) -
                     2*C3vNC*this->mn*(16*mDelta2*this->mn*(p0 + this->q[0])*(this->q[0] * this->q[0] - this->q[3]*this->kg[3]) +
                                       4*pow(mDelta,3)*(-(this->q[0]*(-4*p0*this->q[0] + this->q[0] * this->q[0] + this->q[3] * this->q[3] - this->kg[1] * this->kg[1])) + 2*(-2*p0 + this->q[0])*this->q[3]*this->kg[3]) -
                                       4*this->mn*(p0 + this->q[0])*(4*pow(p0,2)*this->q[0] * this->q[0] + 2*pow(this->q[0],4) -
                                                                     this->q[0] * this->q[0]*(this->q[1] * this->q[1] + 2*this->q[1]*this->kg[1] - this->kg[1] * this->kg[1] + 3*this->q[3]*this->kg[3]) +
                                                                     this->q[3]*(-(this->q[3]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])) + (this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1])*this->kg[3]) +
                                                                     2*p0*this->q[0]*(3*this->q[0] * this->q[0] + this->kg[1] * this->kg[1] - this->q[3]*(this->q[3] + 2*this->kg[3]))) -
                                       mDelta*(16*pow(p0,3)*this->q[0] * this->q[0] + 4*pow(p0,2)*this->q[0]*(5*this->q[0] * this->q[0] - 3*this->q[3] * this->q[3] + 3*this->kg[1] * this->kg[1] - 2*this->q[3]*this->kg[3]) +
                                               4*p0*(-2*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - this->q[0] * this->q[0]*(2*this->q[3] * this->q[3] + (this->q[1] - this->kg[1])*(this->q[1] + 3*this->kg[1])) +
                                                     this->q[3]*(this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1])*this->kg[3]) -
                                               this->q[0]*(this->q[0] * this->q[0] + this->q[3] * this->q[3] - this->kg[1] * this->kg[1] - 2*this->q[3]*this->kg[3])*(3*this->q[0] * this->q[0] - this->q[1]*(this->q[1] + 2*this->kg[1]) - this->q[3]*(this->q[3] + 2*this->kg[3]))))))/
               (24.*mDelta2*pow(this->mn,4));

    tr[1][2] = (C3v*(-2*C3vNC*this->mn*this->kg[2]*(16*mDelta2*this->mn*(p0 + this->q[0])*this->q[1] + 4*pow(mDelta,3)*(4*p0*this->q[1] + this->q[0]*(-2*this->q[1] + this->kg[1])) +
                                                    4*this->mn*(p0 + this->q[0])*(pow(this->q[1],3) + this->q[1]*this->q[3] * this->q[3] + 2*this->q[1] * this->q[1]*this->kg[1] + this->q[3] * this->q[3]*this->kg[1] - 2*p0*this->q[0]*(2*this->q[1] + this->kg[1]) -
                                                                                  this->q[0] * this->q[0]*(2*this->q[1] + this->kg[1]) + this->q[1]*this->q[3]*this->kg[3]) +
                                                    mDelta*(-4*pow(p0,2)*this->q[0]*(2*this->q[1] + 3*this->kg[1]) +
                                                            4*p0*(this->q[0] * this->q[0]*(2*this->q[1] - 3*this->kg[1]) + (this->q[1] * this->q[1] + this->q[3] * this->q[3])*(this->q[1] + 2*this->kg[1])) -
                                                            this->q[0]*(2*this->q[1] - this->kg[1])*(-3*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1] + 2*this->q[3]*this->kg[3]))) +
                     this->c_i*C5aNC*(2*this->mn2*this->q[3]*this->kg[2] * this->kg[2]*(-(this->q[0]*(2*p0 + this->q[0])) + this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) +
                                      4*pow(mDelta,3)*this->mn*(-(this->q[3]*this->kg[1] * this->kg[1]) + this->q[0] * this->q[0]*(this->q[3] - 2*this->kg[3]) - 2*p0*this->q[0]*this->kg[3] +
                                                                (8*this->mn2 + this->q[3] * this->q[3] + this->q[1]*(this->q[1] + this->kg[1]))*this->kg[3]) +
                                      mDelta2*(-2*p0*this->q[3]*(2*p0*(-2*this->q[0] * this->q[0] + this->kg[1] * this->kg[1]) +
                                                                 this->q[0]*(-3*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] + this->q[1]*this->kg[1] + this->kg[1] * this->kg[1])) +
                                               this->q[3]*(-(this->q[0]*(4*p0 + this->q[0])) + this->q[1] * this->q[1] + this->q[3] * this->q[3])*this->kg[2] * this->kg[2] +
                                               2*p0*(-4*pow(p0,2)*this->q[0] - 2*pow(this->q[0],3) + this->q[0]*this->q[1]*(this->q[1] + this->kg[1]) +
                                                     2*p0*(-3*this->q[0] * this->q[0] + this->q[3] * this->q[3] + this->q[1]*(this->q[1] + this->kg[1])))*this->kg[3] +
                                               8*this->mn2*(this->q[3]*(-2*p0*this->q[0] + this->kg[2] * this->kg[2]) + 2*p0*(2*p0 + this->q[0])*this->kg[3])) +
                                      mDelta*this->mn*(this->q[3]*(-pow(this->q[0],4) - this->kg[1] * this->kg[1]*(3*(this->q[1] * this->q[1] + this->q[3] * this->q[3]) + 4*this->q[1]*this->kg[1]) -
                                                                   2*(this->q[3] * this->q[3] + this->q[1]*(this->q[1] + this->kg[1]))*this->kg[2] * this->kg[2] +
                                                                   this->q[0] * this->q[0]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1] + 5*this->kg[1] * this->kg[1] + 2*this->kg[2] * this->kg[2])) +
                                                       (2*pow(this->q[0],4) + pow(this->q[1] * this->q[1] + this->q[3] * this->q[3],2) + 3*this->q[1]*(this->q[1] * this->q[1] + this->q[3] * this->q[3])*this->kg[1] +
                                                        2*(this->q[1] - this->q[3])*(this->q[1] + this->q[3])*this->kg[1] * this->kg[1] - this->q[0] * this->q[0]*(3*(this->q[1] * this->q[1] + this->q[3] * this->q[3]) + 5*this->q[1]*this->kg[1]))*this->kg[3] +
                                                       4*pow(p0,2)*this->q[0] * this->q[0]*(this->q[3] + this->kg[3]) -
                                                       8*this->mn2*(2*p0*this->q[0]*(this->q[3] + this->kg[3]) + this->q[0] * this->q[0]*(2*this->q[3] + this->kg[3]) -
                                                                    this->kg[3]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1] + 2*this->q[3]*this->kg[3])) +
                                                       2*p0*this->q[0]*(-(this->q[1] * this->q[1]*(this->q[3] + 2*this->kg[3])) + this->q[0] * this->q[0]*(this->q[3] + 3*this->kg[3]) - this->q[1]*this->kg[1]*(this->q[3] + 3*this->kg[3]) +
                                                                        this->q[3]*(3*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] - this->q[3]*(this->q[3] + 3*this->kg[3])))))))/(24.*mDelta2*pow(this->mn,4));

    tr[1][3] = -(C3v*(this->c_i*C5aNC*this->kg[2]*(4*pow(mDelta,3)*this->mn*(8*this->mn2 - 2*this->q[0]*(p0 + this->q[0]) + this->q[1]*(this->q[1] + this->kg[1])) -
                                                   2*this->mn2*this->q[3]*(this->q[3] + this->kg[3])*(-(this->q[0]*(2*p0 + this->q[0])) + this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) +
                                                   mDelta*this->mn*(4*pow(p0,2)*this->q[0] * this->q[0] + 6*p0*pow(this->q[0],3) + 2*pow(this->q[0],4) - 4*p0*this->q[0]*this->q[1] * this->q[1] -
                                                                    3*this->q[0] * this->q[0]*this->q[1] * this->q[1] + pow(this->q[1],4) + this->q[1] * this->q[1]*this->q[3] * this->q[3] - 6*p0*this->q[0]*this->q[1]*this->kg[1] - 5*this->q[0] * this->q[0]*this->q[1]*this->kg[1] +
                                                                    3*pow(this->q[1],3)*this->kg[1] + this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 2*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] + 2*this->q[3]*(-(this->q[0]*(p0 + this->q[0])) + this->q[1]*(this->q[1] + this->kg[1]))*this->kg[3] +
                                                                    8*this->mn2*(-(this->q[0]*(2*p0 + this->q[0])) + this->q[1] * this->q[1] + 2*this->q[1]*this->kg[1] + this->q[3]*(this->q[3] + 2*this->kg[3]))) +
                                                   mDelta2*(-8*pow(p0,3)*this->q[0] + 4*pow(p0,2)*(-3*this->q[0] * this->q[0] + this->q[1]*(this->q[1] + this->kg[1])) +
                                                            8*this->mn2*(2*p0*(2*p0 + this->q[0]) - this->q[3]*this->kg[3]) + 2*p0*this->q[0]*(-2*this->q[0] * this->q[0] + this->q[1]*(this->q[1] + this->kg[1]) + this->q[3]*(this->q[3] + 2*this->kg[3])) +
                                                            this->q[3]*(this->q[0] * this->q[0]*(-this->q[3] + this->kg[3]) + this->q[1]*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3])))) +
                      2*C3vNC*this->mn*(16*mDelta2*this->mn*(p0 + this->q[0])*this->q[1]*this->kg[3] + 4*pow(mDelta,3)*(this->q[0]*this->q[1]*(this->q[3] - 2*this->kg[3]) + 4*p0*this->q[1]*this->kg[3] + this->q[0]*this->kg[1]*this->kg[3]) -
                                        4*this->mn*(p0 + this->q[0])*(this->q[1]*this->q[3]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) - this->q[1]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1])*this->kg[3] +
                                                                      this->q[0] * this->q[0]*(-(this->q[3]*(this->q[1] + this->kg[1])) + (2*this->q[1] + this->kg[1])*this->kg[3]) + 2*p0*this->q[0]*(this->kg[1]*this->kg[3] + this->q[1]*(this->q[3] + 2*this->kg[3]))) +
                                        mDelta*(-4*pow(p0,2)*this->q[0]*(3*this->q[1]*this->q[3] + 2*this->q[1]*this->kg[3] + 3*this->kg[1]*this->kg[3]) -
                                                this->q[0]*(this->q[1]*(this->q[3] - 2*this->kg[3]) + this->kg[1]*this->kg[3])*(3*this->q[0] * this->q[0] - this->q[1]*(this->q[1] + 2*this->kg[1]) - this->q[3]*(this->q[3] + 2*this->kg[3])) +
                                                4*p0*(-2*this->q[1]*this->q[3]*this->kg[1] * this->kg[1] + this->q[1]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1])*this->kg[3] +
                                                      this->q[0] * this->q[0]*(-(this->q[1]*this->q[3]) + this->q[3]*this->kg[1] + 2*this->q[1]*this->kg[3] - 3*this->kg[1]*this->kg[3]))))))/(24.*mDelta2*pow(this->mn,4));


    tr[2][0] = (C3v*(-2*C3vNC*this->mn*this->kg[2]*(4*pow(mDelta,3)*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) -
                                                    4*this->mn*(p0 + this->q[0])*(-(this->q[0]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3])) + 2*p0*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3])) +
                                                    mDelta*(4*p0*this->q[0]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] - 3*this->q[1]*this->kg[1] - 3*this->q[3]*this->kg[3]) - 12*pow(p0,2)*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) +
                                                            (this->q[1]*this->kg[1] + this->q[3]*this->kg[3])*(-3*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1] + 2*this->q[3]*this->kg[3]))) -
                     this->c_i*C5aNC*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3])*(8*pow(mDelta,3)*this->mn*(p0 + this->q[0]) -
                                                                                        4*this->mn2*(p0 + this->q[0])*(2*p0*this->q[0] + this->q[0] * this->q[0] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3]) -
                                                                                        2*mDelta*this->mn*(p0 + this->q[0])*(8*this->mn2 + 2*p0*this->q[0] + this->q[0] * this->q[0] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3]) +
                                                                                        mDelta2*(8*pow(p0,3) + 12*pow(p0,2)*this->q[0] + 4*p0*this->q[0] * this->q[0] + 8*this->mn2*(-2*p0 + this->q[0]) +
                                                                                                 this->q[0]*(this->q[1] * this->q[1] - this->q[1]*this->kg[1] + this->q[3]*(this->q[3] - this->kg[3])) - 2*p0*(this->q[1] * this->q[1] + 2*this->q[1]*this->kg[1] + this->q[3]*(this->q[3] + 2*this->kg[3]))))))/
               (24.*mDelta2*pow(this->mn,4));

    tr[2][1] = (C3v*(-2*C3vNC*this->mn*this->kg[2]*(4*pow(mDelta,3)*this->q[0]*this->kg[1] - 4*this->mn*(p0 + this->q[0])*
                                                                                             (2*p0*this->q[0]*this->kg[1] + this->q[0] * this->q[0]*(-this->q[1] + this->kg[1]) + this->q[3]*(-(this->q[3]*this->kg[1]) + this->q[1]*this->kg[3])) +
                                                    mDelta*(4*p0*this->q[0] * this->q[0]*(this->q[1] - 3*this->kg[1]) - 12*pow(p0,2)*this->q[0]*this->kg[1] + 8*p0*this->q[3]*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3]) +
                                                            this->q[0]*this->kg[1]*(-3*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1] + 2*this->q[3]*this->kg[3]))) -
                     this->c_i*C5aNC*(2*this->mn2*(this->q[1] + this->kg[1])*(-(this->q[3]*this->kg[1]) + this->q[1]*this->kg[3])*(2*p0*this->q[0] + this->q[0] * this->q[0] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3]) +
                                      4*pow(mDelta,3)*this->mn*(this->q[3]*(this->q[0] * this->q[0] + this->q[1]*this->kg[1] - this->kg[2] * this->kg[2]) + (8*this->mn2 - 2*this->q[0]*(p0 + this->q[0]) + this->q[3] * this->q[3])*this->kg[3]) +
                                      mDelta2*(-8*pow(p0,3)*this->q[0]*this->kg[3] + 2*p0*this->q[0]*
                                                                                     (-(this->q[3]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] + this->q[1]*this->kg[1] + 2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])) + this->q[0] * this->q[0]*(3*this->q[3] - 2*this->kg[3]) +
                                                                                      this->q[1]*(this->q[1] + 2*this->kg[1])*this->kg[3]) + 4*pow(p0,2)*(this->q[3]*this->kg[1]*(this->q[1] + this->kg[1]) + this->q[0] * this->q[0]*(this->q[3] - 3*this->kg[3]) + this->q[3]*this->kg[3]*(this->q[3] + this->kg[3])) +
                                               (this->q[3]*this->kg[1] - this->q[1]*this->kg[3])*(this->q[0] * this->q[0]*(this->q[1] - this->kg[1]) + this->q[3]*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3])) +
                                               8*this->mn2*(4*pow(p0,2)*this->kg[3] + 2*p0*this->q[0]*(-this->q[3] + this->kg[3]) + this->kg[1]*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3]))) +
                                      mDelta*this->mn*(this->q[3]*(-pow(this->q[0],4) + this->kg[1]*(pow(this->q[1],3) + this->q[1]*this->q[3] * this->q[3] + 2*this->q[1] * this->q[1]*this->kg[1] - 2*this->q[3] * this->q[3]*this->kg[1]) -
                                                                   (this->q[1] * this->q[1] + 3*this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1])*this->kg[2] * this->kg[2] +
                                                                   this->q[0] * this->q[0]*(-this->q[1] * this->q[1] + this->q[3] * this->q[3] - 3*this->q[1]*this->kg[1] + 2*this->kg[1] * this->kg[1] + 5*this->kg[2] * this->kg[2])) +
                                                       (2*pow(this->q[0],4) - this->q[0] * this->q[0]*(3*this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1]) +
                                                        this->q[3] * this->q[3]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] + 4*this->q[1]*this->kg[1] - 2*this->kg[2] * this->kg[2]))*this->kg[3] + 4*pow(p0,2)*this->q[0] * this->q[0]*(this->q[3] + this->kg[3]) -
                                                       8*this->mn2*(2*p0*this->q[0]*(this->q[3] + this->kg[3]) + this->q[0] * this->q[0]*(2*this->q[3] + this->kg[3]) -
                                                                    this->kg[3]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1] + 2*this->q[3]*this->kg[3])) +
                                                       2*p0*this->q[0]*(-(this->q[1] * this->q[1]*this->q[3]) - this->q[1]*this->kg[1]*(3*this->q[3] + this->kg[3]) + this->q[0] * this->q[0]*(this->q[3] + 3*this->kg[3]) +
                                                                        this->q[3]*(this->kg[1] * this->kg[1] + 3*this->kg[2] * this->kg[2] - this->q[3]*(this->q[3] + 3*this->kg[3])))))))/(24.*mDelta2*pow(this->mn,4));

    tr[2][2] = (C3v*(std::complex<double>(0,-1)*C5aNC*this->kg[2]*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3])*(4*pow(mDelta,3)*this->mn +
                                                                                                                     mDelta2*(8*this->mn2 + 4*pow(p0,2) - 2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3]) +
                                                                                                                     2*this->mn2*(-(this->q[0]*(2*p0 + this->q[0])) + this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) +
                                                                                                                     mDelta*this->mn*(-4*p0*this->q[0] - 3*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1] + 2*this->q[3]*this->kg[3])) +
                     2*C3vNC*this->mn*(-16*mDelta2*this->mn*(p0 + this->q[0])*(this->q[0] * this->q[0] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3]) +
                                       4*pow(mDelta,3)*(this->q[0]*(this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] - 2*this->q[1]*this->kg[1] - this->kg[2] * this->kg[2] - 2*this->q[3]*this->kg[3]) +
                                                        4*p0*(-this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3])) +
                                       4*this->mn*(p0 + this->q[0])*(4*pow(p0,2)*this->q[0] * this->q[0] + 2*pow(this->q[0],4) + pow(this->q[1],3)*this->kg[1] + this->q[1]*this->q[3] * this->q[3]*this->kg[1] +
                                                                     this->q[1] * this->q[1]*this->kg[1] * this->kg[1] - this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - this->q[1] * this->q[1]*this->kg[2] * this->kg[2] - 2*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] +
                                                                     this->q[3]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1])*this->kg[3] - this->q[0] * this->q[0]*(this->q[1] * this->q[1] + 3*this->q[1]*this->kg[1] - this->kg[2] * this->kg[2] + 3*this->q[3]*this->kg[3]) +
                                                                     2*p0*this->q[0]*(3*this->q[0] * this->q[0] - this->q[1]*(this->q[1] + 2*this->kg[1]) + this->kg[2] * this->kg[2] - this->q[3]*(this->q[3] + 2*this->kg[3]))) +
                                       mDelta*(16*pow(p0,3)*this->q[0] * this->q[0] + 4*pow(p0,2)*this->q[0]*
                                                                                      (5*this->q[0] * this->q[0] - 3*this->q[1] * this->q[1] - 3*this->q[3] * this->q[3] - 2*this->q[1]*this->kg[1] + 3*this->kg[2] * this->kg[2] - 2*this->q[3]*this->kg[3]) -
                                               this->q[0]*(this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] - 2*this->q[1]*this->kg[1] - this->kg[2] * this->kg[2] - 2*this->q[3]*this->kg[3])*
                                               (3*this->q[0] * this->q[0] - this->q[1]*(this->q[1] + 2*this->kg[1]) - this->q[3]*(this->q[3] + 2*this->kg[3])) +
                                               4*p0*(pow(this->q[1],3)*this->kg[1] + this->q[1]*this->q[3] * this->q[3]*this->kg[1] + this->q[3] * this->q[3]*(-2*this->kg[2] * this->kg[2] + this->q[3]*this->kg[3]) +
                                                     this->q[0] * this->q[0]*(this->q[1]*(-4*this->q[1] + this->kg[1]) + 3*this->kg[2] * this->kg[2] + this->q[3]*(-2*this->q[3] + this->kg[3])) + this->q[1] * this->q[1]*(2*this->kg[1] * this->kg[1] + this->kg[3]*(this->q[3] + 2*this->kg[3]))
                                               )))))/(24.*mDelta2*pow(this->mn,4));

    tr[2][3] = (C3v*(-2*C3vNC*this->mn*this->kg[2]*(4*pow(mDelta,3)*this->q[0]*this->kg[3] - 4*this->mn*(p0 + this->q[0])*(2*p0*this->q[0]*this->kg[3] + this->q[0] * this->q[0]*(-this->q[3] + this->kg[3]) + this->q[1]*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3])) +
                                                    mDelta*(4*p0*this->q[0] * this->q[0]*(this->q[3] - 3*this->kg[3]) - 12*pow(p0,2)*this->q[0]*this->kg[3] + 8*p0*this->q[1]*(-(this->q[3]*this->kg[1]) + this->q[1]*this->kg[3]) +
                                                            this->q[0]*this->kg[3]*(-3*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1] + 2*this->q[3]*this->kg[3]))) +
                     this->c_i*C5aNC*(-2*this->mn2*(this->q[3] + this->kg[3])*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3])*(-(this->q[0]*(2*p0 + this->q[0])) + this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) +
                                      4*pow(mDelta,3)*this->mn*(this->q[0] * this->q[0]*(this->q[1] - 2*this->kg[1]) + 8*this->mn2*this->kg[1] - 2*p0*this->q[0]*this->kg[1] + this->q[1]*(this->q[1]*this->kg[1] - this->kg[2] * this->kg[2] + this->q[3]*this->kg[3])) +
                                      mDelta2*(-8*pow(p0,3)*this->q[0]*this->kg[1] + 4*pow(p0,2)*
                                                                                     (this->q[0] * this->q[0]*(this->q[1] - 3*this->kg[1]) + this->q[1]*this->kg[1]*(this->q[1] + this->kg[1]) + this->q[1]*this->kg[3]*(this->q[3] + this->kg[3])) +
                                               (this->q[3]*this->kg[1] - this->q[1]*this->kg[3])*(this->q[0] * this->q[0]*(-this->q[3] + this->kg[3]) + this->q[1]*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3])) +
                                               8*this->mn2*(4*pow(p0,2)*this->kg[1] + 2*p0*this->q[0]*(-this->q[1] + this->kg[1]) + this->kg[3]*(-(this->q[3]*this->kg[1]) + this->q[1]*this->kg[3])) +
                                               2*p0*this->q[0]*(-pow(this->q[1],3) + this->q[0] * this->q[0]*(this->q[1] - 2*this->kg[1]) + this->q[3]*this->kg[1]*(this->q[3] + 2*this->kg[3]) +
                                                                this->q[1]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] - this->q[3]*(this->q[3] + this->kg[3])))) +
                                      mDelta*this->mn*(pow(this->q[0],4)*this->q[1] - this->q[0] * this->q[0]*pow(this->q[1],3) + this->q[0] * this->q[0]*this->q[1]*this->q[3] * this->q[3] + 2*pow(this->q[0],4)*this->kg[1] -
                                                       3*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[1] + pow(this->q[1],4)*this->kg[1] + this->q[1] * this->q[1]*this->q[3] * this->q[3]*this->kg[1] - 2*this->q[0] * this->q[0]*this->q[1]*this->kg[1] * this->kg[1] +
                                                       2*pow(this->q[1],3)*this->kg[1] * this->kg[1] - 2*this->q[1]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 4*pow(p0,2)*this->q[0] * this->q[0]*(this->q[1] + this->kg[1]) +
                                                       3*this->q[0] * this->q[0]*this->q[1]*this->kg[2] * this->kg[2] - pow(this->q[1],3)*this->kg[2] * this->kg[2] - 3*this->q[1]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] -
                                                       2*this->q[1] * this->q[1]*this->kg[1]*this->kg[2] * this->kg[2] - 3*this->q[0] * this->q[0]*this->q[1]*this->q[3]*this->kg[3] + pow(this->q[1],3)*this->q[3]*this->kg[3] + this->q[1]*pow(this->q[3],3)*this->kg[3] -
                                                       2*this->q[0] * this->q[0]*this->q[3]*this->kg[1]*this->kg[3] + 4*this->q[1] * this->q[1]*this->q[3]*this->kg[1]*this->kg[3] - 2*this->q[1]*this->q[3]*this->kg[2] * this->kg[2]*this->kg[3] -
                                                       8*this->mn2*(2*p0*this->q[0]*(this->q[1] + this->kg[1]) + this->q[0] * this->q[0]*(2*this->q[1] + this->kg[1]) -
                                                                    this->kg[1]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1] + 2*this->q[3]*this->kg[3])) +
                                                       2*p0*this->q[0]*(-pow(this->q[1],3) - 3*this->q[1] * this->q[1]*this->kg[1] + 2*this->q[0] * this->q[0]*(this->q[1] + 2*this->kg[1]) -
                                                                        this->q[1]*(this->q[3] * this->q[3] + this->kg[1] * this->kg[1] - 2*this->kg[2] * this->kg[2] + 3*this->q[3]*this->kg[3]) - this->kg[1]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3]*(this->q[3] + this->kg[3]))))))
               )/(24.*mDelta2*pow(this->mn,4));


    tr[3][0] = -(C3v*(this->c_i*C5aNC*this->q[1]*this->kg[2]*(8*pow(mDelta,3)*this->mn*(p0 + this->q[0]) -
                                                              4*this->mn2*(p0 + this->q[0])*(2*p0*this->q[0] + this->q[0] * this->q[0] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3]) -
                                                              2*mDelta*this->mn*(p0 + this->q[0])*(8*this->mn2 + 2*p0*this->q[0] + this->q[0] * this->q[0] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3]) +
                                                              mDelta2*(8*pow(p0,3) + 12*pow(p0,2)*this->q[0] + 4*p0*this->q[0] * this->q[0] + 8*this->mn2*(-2*p0 + this->q[0]) +
                                                                       this->q[0]*(this->q[1] * this->q[1] - this->q[1]*this->kg[1] + this->q[3]*(this->q[3] - this->kg[3])) - 2*p0*(this->q[1] * this->q[1] + 2*this->q[1]*this->kg[1] + this->q[3]*(this->q[3] + 2*this->kg[3])))) +
                      2*C3vNC*this->mn*(16*mDelta2*this->mn*this->q[0]*(p0 + this->q[0])*this->q[3] +
                                        4*pow(mDelta,3)*(4*p0*this->q[0]*this->q[3] - this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + this->q[1]*this->kg[1]*this->kg[3]) +
                                        4*this->mn*(p0 + this->q[0])*(-4*pow(p0,2)*this->q[0]*this->q[3] - 8*p0*this->q[0] * this->q[0]*this->q[3] - 3*pow(this->q[0],3)*this->q[3] +
                                                                      2*p0*this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + this->q[0]*this->q[3]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] + this->q[1]*this->kg[1] + this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) -
                                                                      2*p0*this->q[1]*this->kg[1]*this->kg[3] + this->q[0]*(this->q[1] * this->q[1] + 2*this->q[3] * this->q[3] - this->q[1]*this->kg[1])*this->kg[3]) +
                                        mDelta*(-16*pow(p0,3)*this->q[0]*this->q[3] - 4*pow(p0,2)*(8*this->q[0] * this->q[0]*this->q[3] - 3*this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + 3*this->q[1]*this->kg[1]*this->kg[3]) -
                                                (this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) - this->q[1]*this->kg[1]*this->kg[3])*(-3*this->q[0] * this->q[0] + this->q[1] * this->q[1] + 2*this->q[1]*this->kg[1] + this->q[3]*(this->q[3] + 2*this->kg[3])) +
                                                4*p0*this->q[0]*(-3*this->q[0] * this->q[0]*this->q[3] + this->q[1]*this->kg[1]*(this->q[3] - 3*this->kg[3]) + this->q[1] * this->q[1]*(this->q[3] + this->kg[3]) +
                                                                 this->q[3]*(3*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + this->q[3]*(this->q[3] + 2*this->kg[3])))))))/(24.*mDelta2*pow(this->mn,4));

    tr[3][1] = (this->c_i*C3v*C5aNC*this->kg[2]*(-2*this->mn2*this->q[1]*(this->q[1] + this->kg[1])*(-(this->q[0]*(2*p0 + this->q[0])) + this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) +
                                                 4*pow(mDelta,3)*this->mn*(8*this->mn2 - 2*this->q[0]*(p0 + this->q[0]) + this->q[3]*(this->q[3] + this->kg[3])) -
                                                 mDelta*this->mn*(-4*pow(p0,2)*this->q[0] * this->q[0] - 2*pow(this->q[0],4) + this->q[0] * this->q[0]*this->q[3] * this->q[3] - this->q[1] * this->q[1]*this->q[3] * this->q[3] - pow(this->q[3],4) +
                                                                  2*this->q[0] * this->q[0]*this->q[1]*this->kg[1] - 2*this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 2*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 2*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] -
                                                                  this->q[3]*(-5*this->q[0] * this->q[0] + this->q[1] * this->q[1] + 3*this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1])*this->kg[3] +
                                                                  2*p0*this->q[0]*(-3*this->q[0] * this->q[0] + 2*this->q[3] * this->q[3] + this->q[1]*this->kg[1] + 3*this->q[3]*this->kg[3]) +
                                                                  8*this->mn2*(2*p0*this->q[0] + this->q[0] * this->q[0] - this->q[1]*(this->q[1] + 2*this->kg[1]) - this->q[3]*(this->q[3] + 2*this->kg[3]))) +
                                                 mDelta2*(-8*pow(p0,3)*this->q[0] + 8*this->mn2*(2*p0*(2*p0 + this->q[0]) - this->q[1]*this->kg[1]) +
                                                          4*pow(p0,2)*(-3*this->q[0] * this->q[0] + this->q[3]*(this->q[3] + this->kg[3])) + 2*p0*this->q[0]*(-2*this->q[0] * this->q[0] + this->q[1]*(this->q[1] + 2*this->kg[1]) + this->q[3]*(this->q[3] + this->kg[3])) +
                                                          this->q[1]*(this->q[0] * this->q[0]*(-this->q[1] + this->kg[1]) + this->q[3]*(-(this->q[3]*this->kg[1]) + this->q[1]*this->kg[3])))) -
                2*C3v*C3vNC*this->mn*(16*mDelta2*this->mn*(p0 + this->q[0])*this->q[3]*this->kg[1] + 4*pow(mDelta,3)*(this->q[0]*this->q[1]*this->q[3] + 4*p0*this->q[3]*this->kg[1] + this->q[0]*this->kg[1]*(-2*this->q[3] + this->kg[3])) -
                                      4*this->mn*(p0 + this->q[0])*(2*p0*this->q[0]*(this->q[1]*this->q[3] + this->kg[1]*(2*this->q[3] + this->kg[3])) + this->q[0] * this->q[0]*(this->q[1]*(this->q[3] - this->kg[3]) + this->kg[1]*(2*this->q[3] + this->kg[3])) -
                                                                    this->q[3]*(this->q[1] * this->q[1]*this->kg[1] + this->q[1]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + this->q[3]*this->kg[1]*(this->q[3] + 2*this->kg[3]))) +
                                      mDelta*(-4*pow(p0,2)*this->q[0]*(3*this->q[1]*this->q[3] + 2*this->q[3]*this->kg[1] + 3*this->kg[1]*this->kg[3]) +
                                              this->q[0]*(this->q[1]*this->q[3] + this->kg[1]*(-2*this->q[3] + this->kg[3]))*(-3*this->q[0] * this->q[0] + this->q[1] * this->q[1] + 2*this->q[1]*this->kg[1] + this->q[3]*(this->q[3] + 2*this->kg[3])) +
                                              4*p0*(this->q[0] * this->q[0]*(-3*this->q[1]*this->q[3] + 2*this->q[3]*this->kg[1] + this->q[1]*this->kg[3] - 3*this->kg[1]*this->kg[3]) +
                                                    this->q[3]*(this->q[1] * this->q[1]*this->kg[1] + 2*this->q[1]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + this->q[3]*this->kg[1]*(this->q[3] + 2*this->kg[3]))))))/(24.*mDelta2*pow(this->mn,4));

    tr[3][2] = (C3v*(-2*C3vNC*this->mn*this->kg[2]*(16*mDelta2*this->mn*(p0 + this->q[0])*this->q[3] -
                                                    4*this->mn*(p0 + this->q[0])*(-(this->q[3]*(-2*this->q[0]*(2*p0 + this->q[0]) + this->q[3] * this->q[3] + this->q[1]*(this->q[1] + this->kg[1]))) +
                                                                                  (2*p0*this->q[0] + this->q[0] * this->q[0] - this->q[1] * this->q[1] - 2*this->q[3] * this->q[3])*this->kg[3]) + 4*pow(mDelta,3)*(4*p0*this->q[3] + this->q[0]*(-2*this->q[3] + this->kg[3])) +
                                                    mDelta*(-4*pow(p0,2)*this->q[0]*(2*this->q[3] + 3*this->kg[3]) -
                                                            this->q[0]*(2*this->q[3] - this->kg[3])*(-3*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1] + 2*this->q[3]*this->kg[3]) +
                                                            4*p0*(this->q[0] * this->q[0]*(2*this->q[3] - 3*this->kg[3]) + (this->q[1] * this->q[1] + this->q[3] * this->q[3])*(this->q[3] + 2*this->kg[3])))) -
                     this->c_i*C5aNC*(2*this->mn2*this->q[1]*this->kg[2] * this->kg[2]*(-(this->q[0]*(2*p0 + this->q[0])) + this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) +
                                      4*pow(mDelta,3)*this->mn*(8*this->mn2*this->kg[1] - 2*p0*this->q[0]*this->kg[1] - 2*this->q[0] * this->q[0]*this->kg[1] + this->q[1] * this->q[1]*this->kg[1] + this->q[3] * this->q[3]*this->kg[1] +
                                                                this->q[1]*this->kg[1] * this->kg[1] + this->q[1]*this->kg[2] * this->kg[2] + this->q[3]*this->kg[1]*this->kg[3]) +
                                      mDelta2*(-8*pow(p0,3)*this->q[0]*this->kg[1] + this->q[1]*(-this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3])*this->kg[2] * this->kg[2] +
                                               8*this->mn2*(4*pow(p0,2)*this->kg[1] + 2*p0*this->q[0]*(-this->q[1] + this->kg[1]) + this->q[1]*this->kg[2] * this->kg[2]) +
                                               4*pow(p0,2)*(this->q[0] * this->q[0]*(this->q[1] - 3*this->kg[1]) + this->q[1] * this->q[1]*this->kg[1] + this->q[1]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + this->q[3]*this->kg[1]*(this->q[3] + this->kg[3])) +
                                               2*p0*this->q[0]*(-pow(this->q[1],3) + 2*this->q[0] * this->q[0]*(this->q[1] - this->kg[1]) + this->q[3]*this->kg[1]*(this->q[3] + this->kg[3]) -
                                                                this->q[1]*(-this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->q[3]*(this->q[3] + this->kg[3])))) +
                                      mDelta*this->mn*(4*pow(this->q[0],4)*this->q[1] - 2*this->q[0] * this->q[0]*pow(this->q[1],3) - 2*this->q[0] * this->q[0]*this->q[1]*this->q[3] * this->q[3] + 2*pow(this->q[0],4)*this->kg[1] -
                                                       5*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[1] + pow(this->q[1],4)*this->kg[1] - this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[1] + 2*this->q[1] * this->q[1]*this->q[3] * this->q[3]*this->kg[1] +
                                                       pow(this->q[3],4)*this->kg[1] - 5*this->q[0] * this->q[0]*this->q[1]*this->kg[1] * this->kg[1] + 3*pow(this->q[1],3)*this->kg[1] * this->kg[1] + 3*this->q[1]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] +
                                                       2*this->q[1] * this->q[1]*pow(this->kg[1],3) - 2*this->q[3] * this->q[3]*pow(this->kg[1],3) + 4*pow(p0,2)*this->q[0] * this->q[0]*(this->q[1] + this->kg[1]) -
                                                       3*this->q[0] * this->q[0]*this->q[1]*this->kg[2] * this->kg[2] + pow(this->q[1],3)*this->kg[2] * this->kg[2] + this->q[1]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] +
                                                       2*this->q[1] * this->q[1]*this->kg[1]*this->kg[2] * this->kg[2] - 2*this->q[3] * this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] - 2*this->q[0] * this->q[0]*this->q[1]*this->q[3]*this->kg[3] - 5*this->q[0] * this->q[0]*this->q[3]*this->kg[1]*this->kg[3] +
                                                       3*this->q[1] * this->q[1]*this->q[3]*this->kg[1]*this->kg[3] + 3*pow(this->q[3],3)*this->kg[1]*this->kg[3] + 4*this->q[1]*this->q[3]*this->kg[1] * this->kg[1]*this->kg[3] + 2*this->q[1]*this->q[3]*this->kg[2] * this->kg[2]*this->kg[3] -
                                                       8*this->mn2*(2*p0*this->q[0]*(this->q[1] + this->kg[1]) + this->q[0] * this->q[0]*(2*this->q[1] + this->kg[1]) -
                                                                    this->kg[1]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1] + 2*this->q[3]*this->kg[3])) +
                                                       2*p0*this->q[0]*(-pow(this->q[1],3) - 3*this->q[1] * this->q[1]*this->kg[1] + this->q[0] * this->q[0]*(4*this->q[1] + 3*this->kg[1]) - this->q[3]*this->kg[1]*(2*this->q[3] + 3*this->kg[3]) -
                                                                        this->q[1]*(3*this->kg[1] * this->kg[1] + 2*this->kg[2] * this->kg[2] + this->q[3]*(this->q[3] + this->kg[3])))))))/(24.*mDelta2*pow(this->mn,4));

    tr[3][3] = -(C3v*(this->c_i*C5aNC*this->q[1]*this->kg[2]*(4*pow(mDelta,3)*this->mn*(this->q[3] + this->kg[3]) +
                                                              2*this->mn2*(this->q[3] + this->kg[3])*(-(this->q[0]*(2*p0 + this->q[0])) + this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) +
                                                              mDelta*this->mn*(this->q[3] + this->kg[3])*(-4*p0*this->q[0] - 3*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1] + 2*this->q[3]*this->kg[3]) +
                                                              mDelta2*(-(this->q[1]*this->q[3]*this->kg[1]) + this->q[0] * this->q[0]*(this->q[3] - this->kg[3]) + 8*this->mn2*this->kg[3] - 2*p0*this->q[0]*this->kg[3] + this->q[1] * this->q[1]*this->kg[3] +
                                                                       4*pow(p0,2)*(this->q[3] + this->kg[3]))) + 2*C3vNC*this->mn*
                                                                                                                  (16*mDelta2*this->mn*(p0 + this->q[0])*(this->q[0] * this->q[0] - this->q[1]*this->kg[1]) +
                                                                                                                   4*pow(mDelta,3)*(4*p0*(this->q[0] * this->q[0] - this->q[1]*this->kg[1]) - this->q[0]*(pow(this->q[1] - this->kg[1],2) + this->kg[2] * this->kg[2])) -
                                                                                                                   4*this->mn*(p0 + this->q[0])*(4*pow(p0,2)*this->q[0] * this->q[0] + 3*pow(this->q[0],4) +
                                                                                                                                                 2*p0*this->q[0]*(4*this->q[0] * this->q[0] - pow(this->q[1] + this->kg[1],2) - this->kg[2] * this->kg[2]) -
                                                                                                                                                 this->q[0] * this->q[0]*(this->q[3] * this->q[3] + (this->q[1] + this->kg[1])*(2*this->q[1] + this->kg[1]) + this->kg[2] * this->kg[2] + 2*this->q[3]*this->kg[3]) +
                                                                                                                                                 this->q[1]*(this->q[1] * this->q[1]*this->kg[1] + this->q[1]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + this->q[3]*this->kg[1]*(this->q[3] + 2*this->kg[3]))) -
                                                                                                                   mDelta*(16*pow(p0,3)*this->q[0] * this->q[0] + 4*pow(p0,2)*this->q[0]*
                                                                                                                                                                  (8*this->q[0] * this->q[0] - 3*this->q[1] * this->q[1] - 2*this->q[1]*this->kg[1] - 3*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])) +
                                                                                                                           this->q[0]*(pow(this->q[1] - this->kg[1],2) + this->kg[2] * this->kg[2])*(-3*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1] + 2*this->q[3]*this->kg[3]) +
                                                                                                                           4*p0*(3*pow(this->q[0],4) - this->q[0] * this->q[0]*(4*this->q[1] * this->q[1] - this->q[1]*this->kg[1] + 3*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + this->q[3]*(this->q[3] + 2*this->kg[3])) +
                                                                                                                                 this->q[1]*(this->q[1] * this->q[1]*this->kg[1] + 2*this->q[1]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + this->q[3]*this->kg[1]*(this->q[3] + 2*this->kg[3])))))))/
               (24.*mDelta2*pow(this->mn,4));

}

void Hadronic_Current_R_Delta::set_tr_crs(Array4x4 &tr) {

    tr[0][0] = -(C3v*C3vNC*((mDelta + this->mn)*p0 - this->mn*this->q[0])*(4*mDelta2*this->q[1]*this->kg[1] - 4*pow(p0,2)*this->q[1]*this->kg[1] + pow(this->q[1],3)*this->kg[1] + this->q[1]*this->q[3] * this->q[3]*this->kg[1] +
                                                                           this->q[1] * this->q[1]*this->kg[1] * this->kg[1] - this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - this->q[3] * this->q[3]*this->kg[2] * this->kg[2] +
                                                                           this->q[0] * this->q[0]*(this->q[1] * this->q[1] - 3*this->q[1]*this->kg[1] + this->q[3]*(2*this->q[3] - 3*this->kg[3])) +
                                                                           this->q[3]*(4*mDelta2 - 4*pow(p0,2) + this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1])*this->kg[3] + 8*p0*this->q[0]*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3])))/
               (3.*mDelta2*pow(this->mn,3));

    tr[0][1] = -(C3v*(this->c_i*C5aNC*mDelta*((mDelta + this->mn)*p0 - this->mn*this->q[0])*this->q[3]*this->kg[2]*
                      (8*this->mn2 + 2*(p0 - this->q[0])*this->q[0] + this->q[1]*(this->q[1] + this->kg[1]) + this->q[3]*(this->q[3] + this->kg[3])) +
                      C3vNC*this->mn*(16*mDelta2*this->mn*(p0 - this->q[0])*this->q[0]*this->kg[1] + 4*pow(mDelta,3)*(4*p0*this->q[0]*this->kg[1] + this->q[3]*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3])) +
                                      4*this->mn*(p0 - this->q[0])*(pow(this->q[0],3)*(this->q[1] - 3*this->kg[1]) + 8*p0*this->q[0] * this->q[0]*this->kg[1] + 2*p0*this->q[3]*(-(this->q[3]*this->kg[1]) + this->q[1]*this->kg[3]) +
                                                                    this->q[0]*(-4*pow(p0,2)*this->kg[1] + 2*this->q[3] * this->q[3]*this->kg[1] + this->q[1]*this->kg[1]*(this->q[1] + this->kg[1]) + this->q[3]*(-this->q[1] + this->kg[1])*this->kg[3])) +
                                      mDelta*(-16*pow(p0,3)*this->q[0]*this->kg[1] + this->q[3]*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3])*(-3*this->q[0] * this->q[0] + this->q[1] * this->q[1] + 2*this->q[1]*this->kg[1] + this->q[3]*(this->q[3] + 2*this->kg[3])) +
                                              4*pow(p0,2)*(8*this->q[0] * this->q[0]*this->kg[1] + 3*this->q[3]*(-(this->q[3]*this->kg[1]) + this->q[1]*this->kg[3])) +
                                              4*p0*this->q[0]*(this->q[0] * this->q[0]*(this->q[1] - 3*this->kg[1]) + this->q[1] * this->q[1]*this->kg[1] + this->q[3]*this->kg[1]*(4*this->q[3] + this->kg[3]) + this->q[1]*(this->kg[1] * this->kg[1] - 3*this->q[3]*this->kg[3]))))))/
               (12.*mDelta2*pow(this->mn,4));

    tr[0][2] = (this->c_i*C3v*C5aNC*mDelta*((mDelta + this->mn)*p0 - this->mn*this->q[0])*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3])*
                (8*this->mn2 + 2*(p0 - this->q[0])*this->q[0] + this->q[1]*(this->q[1] + this->kg[1]) + this->q[3]*(this->q[3] + this->kg[3])) -
                C3v*C3vNC*this->mn*this->kg[2]*(16*mDelta2*this->mn*(p0 - this->q[0])*this->q[0] + 4*pow(mDelta,3)*(4*p0*this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3]) +
                                                4*this->mn*(p0 - this->q[0])*(-4*pow(p0,2)*this->q[0] + 8*p0*this->q[0] * this->q[0] - 3*pow(this->q[0],3) - 2*p0*(this->q[1] * this->q[1] + this->q[3] * this->q[3]) +
                                                                              this->q[0]*this->q[1]*(2*this->q[1] + this->kg[1]) + this->q[0]*this->q[3]*(2*this->q[3] + this->kg[3])) +
                                                mDelta*(-16*pow(p0,3)*this->q[0] + 4*pow(p0,2)*(8*this->q[0] * this->q[0] - 3*(this->q[1] * this->q[1] + this->q[3] * this->q[3])) +
                                                        (this->q[1] * this->q[1] + this->q[3] * this->q[3])*(-3*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1] + 2*this->q[3]*this->kg[3]) +
                                                        4*p0*this->q[0]*(-3*this->q[0] * this->q[0] + this->q[1]*(4*this->q[1] + this->kg[1]) + this->q[3]*(4*this->q[3] + this->kg[3])))))/(12.*mDelta2*pow(this->mn,4));

    tr[0][3] = (this->c_i*C3v*C5aNC*mDelta*((mDelta + this->mn)*p0 - this->mn*this->q[0])*this->q[1]*this->kg[2]*(8*this->mn2 + 2*(p0 - this->q[0])*this->q[0] + this->q[1]*(this->q[1] + this->kg[1]) + this->q[3]*(this->q[3] + this->kg[3])) -
                C3v*C3vNC*this->mn*(16*mDelta2*this->mn*(p0 - this->q[0])*this->q[0]*this->kg[3] + 4*pow(mDelta,3)*(-(this->q[1]*this->q[3]*this->kg[1]) + 4*p0*this->q[0]*this->kg[3] + this->q[1] * this->q[1]*this->kg[3]) +
                                    4*this->mn*(p0 - this->q[0])*(-(this->q[0]*this->q[3]*(this->kg[1]*(this->q[1] + this->kg[1]) + this->kg[2] * this->kg[2])) + pow(this->q[0],3)*(2*this->q[3] - 3*this->kg[3]) + 8*p0*this->q[0] * this->q[0]*this->kg[3] +
                                                                  this->q[0]*(-4*pow(p0,2) + this->q[3] * this->q[3] + this->q[1]*(2*this->q[1] + this->kg[1]))*this->kg[3] + 2*p0*this->q[1]*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3])) +
                                    mDelta*(-16*pow(p0,3)*this->q[0]*this->kg[3] + 4*pow(p0,2)*(3*this->q[1]*this->q[3]*this->kg[1] + 8*this->q[0] * this->q[0]*this->kg[3] - 3*this->q[1] * this->q[1]*this->kg[3]) +
                                            this->q[1]*(-(this->q[3]*this->kg[1]) + this->q[1]*this->kg[3])*(-3*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1] + 2*this->q[3]*this->kg[3]) +
                                            4*p0*this->q[0]*(-(this->q[3]*(3*this->q[1]*this->kg[1] + this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])) + this->q[0] * this->q[0]*(2*this->q[3] - 3*this->kg[3]) +
                                                             (this->q[3] * this->q[3] + this->q[1]*(4*this->q[1] + this->kg[1]))*this->kg[3]))))/(12.*mDelta2*pow(this->mn,4));


    tr[1][0] = (this->c_i*C3v*C5aNC*this->q[3]*this->kg[2]*(8*pow(mDelta,3)*this->mn*(-p0 + this->q[0]) +
                                                            2*mDelta*this->mn*(p0 - this->q[0])*(8*this->mn2 - 2*p0*this->q[0] + this->q[0] * this->q[0] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3]) -
                                                            4*this->mn2*(p0 - this->q[0])*(2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) +
                                                            mDelta2*(-8*pow(p0,3) + 12*pow(p0,2)*this->q[0] + 8*this->mn2*(2*p0 + this->q[0]) +
                                                                     this->q[0]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3]) +
                                                                     2*p0*(-2*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1] + 2*this->q[3]*this->kg[3]))) -
                2*C3v*C3vNC*this->mn*(16*mDelta2*this->mn*(p0 - this->q[0])*this->q[0]*this->q[1] +
                                      4*pow(mDelta,3)*(4*p0*this->q[0]*this->q[1] + this->q[0] * this->q[0]*this->q[1] - this->kg[1]*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3])) +
                                      mDelta*(-16*pow(p0,3)*this->q[0]*this->q[1] + 4*p0*this->q[0]*(pow(this->q[1],3) + 2*this->q[1] * this->q[1]*this->kg[1] - 3*this->q[1]*this->kg[1] * this->kg[1] + this->q[3]*this->kg[1]*(this->q[3] - 3*this->kg[3]) +
                                                                                                     this->q[1]*this->q[3]*(this->q[3] + this->kg[3])) - (3*this->q[0] * this->q[0] - this->q[1]*(this->q[1] + 2*this->kg[1]) - this->q[3]*(this->q[3] + 2*this->kg[3]))*(this->q[0] * this->q[0]*this->q[1] - this->kg[1]*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3])) +
                                              4*pow(p0,2)*(5*this->q[0] * this->q[0]*this->q[1] + 3*this->kg[1]*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]))) -
                                      4*this->mn*(p0 - this->q[0])*(4*pow(p0,2)*this->q[0]*this->q[1] - 2*p0*(3*this->q[0] * this->q[0]*this->q[1] + this->kg[1]*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3])) +
                                                                    this->q[0]*(2*this->q[0] * this->q[0]*this->q[1] - pow(this->q[1],3) - 2*this->q[1] * this->q[1]*this->kg[1] + this->q[3]*this->kg[1]*(-this->q[3] + this->kg[3]) + this->q[1]*(this->kg[1] * this->kg[1] - this->q[3]*(this->q[3] + this->kg[3]))))))/
               (24.*mDelta2*pow(this->mn,4));

    tr[1][1] = (this->c_i*C3v*C5aNC*this->q[3]*this->kg[2]*(4*pow(mDelta,3)*this->mn*(this->q[1] + this->kg[1]) +
                                                            2*this->mn2*(this->q[1] + this->kg[1])*(2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) +
                                                            mDelta*this->mn*(this->q[1] + this->kg[1])*(4*p0*this->q[0] - 3*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1] + 2*this->q[3]*this->kg[3]) +
                                                            mDelta2*(this->q[0] * this->q[0]*(this->q[1] - this->kg[1]) + 8*this->mn2*this->kg[1] + 2*p0*this->q[0]*this->kg[1] + this->q[3] * this->q[3]*this->kg[1] + 4*pow(p0,2)*(this->q[1] + this->kg[1]) -
                                                                     this->q[1]*this->q[3]*this->kg[3])) - 2*C3v*C3vNC*this->mn*(16*mDelta2*this->mn*(p0 - this->q[0])*(this->q[0] * this->q[0] - this->q[3]*this->kg[3]) +
                                                                                                                                 4*pow(mDelta,3)*(this->q[0]*(4*p0*this->q[0] + this->q[0] * this->q[0] + this->q[3] * this->q[3] - this->kg[1] * this->kg[1]) - 2*(2*p0 + this->q[0])*this->q[3]*this->kg[3]) -
                                                                                                                                 4*this->mn*(p0 - this->q[0])*(4*pow(p0,2)*this->q[0] * this->q[0] + 2*pow(this->q[0],4) +
                                                                                                                                                               2*p0*this->q[0]*(-3*this->q[0] * this->q[0] + this->q[3] * this->q[3] - this->kg[1] * this->kg[1] + 2*this->q[3]*this->kg[3]) -
                                                                                                                                                               this->q[0] * this->q[0]*(this->q[1] * this->q[1] + 2*this->q[1]*this->kg[1] - this->kg[1] * this->kg[1] + 3*this->q[3]*this->kg[3]) +
                                                                                                                                                               this->q[3]*(-(this->q[3]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])) + (this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1])*this->kg[3])) +
                                                                                                                                 mDelta*(-16*pow(p0,3)*this->q[0] * this->q[0] + 4*pow(p0,2)*this->q[0]*(5*this->q[0] * this->q[0] - 3*this->q[3] * this->q[3] + 3*this->kg[1] * this->kg[1] - 2*this->q[3]*this->kg[3]) -
                                                                                                                                         this->q[0]*(this->q[0] * this->q[0] + this->q[3] * this->q[3] - this->kg[1] * this->kg[1] - 2*this->q[3]*this->kg[3])*(3*this->q[0] * this->q[0] - this->q[1]*(this->q[1] + 2*this->kg[1]) - this->q[3]*(this->q[3] + 2*this->kg[3])) +
                                                                                                                                         4*p0*(this->q[0] * this->q[0]*((this->q[1] - this->kg[1])*(this->q[1] + 3*this->kg[1]) + this->q[3]*(2*this->q[3] - this->kg[3])) +
                                                                                                                                               this->q[3]*(2*this->q[3]*this->kg[1] * this->kg[1] - (this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1])*this->kg[3])))))/(24.*mDelta2*pow(this->mn,4));

    tr[1][2] = (C3v*(-2*C3vNC*this->mn*this->kg[2]*(16*mDelta2*this->mn*(p0 - this->q[0])*this->q[1] + 4*pow(mDelta,3)*(4*p0*this->q[1] + 2*this->q[0]*this->q[1] - this->q[0]*this->kg[1]) +
                                                    4*this->mn*(p0 - this->q[0])*(pow(this->q[1],3) + this->q[1]*this->q[3] * this->q[3] + 2*this->q[1] * this->q[1]*this->kg[1] + this->q[3] * this->q[3]*this->kg[1] + 2*p0*this->q[0]*(2*this->q[1] + this->kg[1]) -
                                                                                  this->q[0] * this->q[0]*(2*this->q[1] + this->kg[1]) + this->q[1]*this->q[3]*this->kg[3]) +
                                                    mDelta*(4*pow(p0,2)*this->q[0]*(2*this->q[1] + 3*this->kg[1]) + 4*p0*
                                                                                                                    (this->q[0] * this->q[0]*(2*this->q[1] - 3*this->kg[1]) + (this->q[1] * this->q[1] + this->q[3] * this->q[3])*(this->q[1] + 2*this->kg[1])) +
                                                            this->q[0]*(2*this->q[1] - this->kg[1])*(-3*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1] + 2*this->q[3]*this->kg[3]))) +
                     this->c_i*C5aNC*(2*this->mn2*this->q[3]*this->kg[2] * this->kg[2]*(2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) +
                                      4*pow(mDelta,3)*this->mn*(-(this->q[3]*this->kg[1] * this->kg[1]) + this->q[0] * this->q[0]*(this->q[3] - 2*this->kg[3]) + 2*p0*this->q[0]*this->kg[3] +
                                                                (8*this->mn2 + this->q[3] * this->q[3] + this->q[1]*(this->q[1] + this->kg[1]))*this->kg[3]) +
                                      mDelta2*(2*p0*this->q[3]*(this->q[0]*(4*p0*this->q[0] - 3*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3]) + this->q[0]*this->q[1]*this->kg[1] +
                                                                (-2*p0 + this->q[0])*this->kg[1] * this->kg[1]) + this->q[3]*(4*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3])*this->kg[2] * this->kg[2] +
                                               2*p0*(4*pow(p0,2)*this->q[0] + 2*pow(this->q[0],3) - this->q[0]*this->q[1]*(this->q[1] + this->kg[1]) + 2*p0*(-3*this->q[0] * this->q[0] + this->q[3] * this->q[3] + this->q[1]*(this->q[1] + this->kg[1])))*
                                               this->kg[3] + 8*this->mn2*(this->q[3]*this->kg[2] * this->kg[2] + 2*p0*this->q[0]*(this->q[3] - this->kg[3]) + 4*pow(p0,2)*this->kg[3])) +
                                      mDelta*this->mn*(this->q[3]*(4*pow(p0,2)*this->q[0] * this->q[0] - pow(this->q[0],4) - this->kg[1] * this->kg[1]*(3*(this->q[1] * this->q[1] + this->q[3] * this->q[3]) + 4*this->q[1]*this->kg[1]) -
                                                                   2*(this->q[3] * this->q[3] + this->q[1]*(this->q[1] + this->kg[1]))*this->kg[2] * this->kg[2] +
                                                                   2*p0*this->q[0]*(-this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] + this->q[1]*this->kg[1] - 3*this->kg[1] * this->kg[1] - this->kg[2] * this->kg[2]) +
                                                                   this->q[0] * this->q[0]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1] + 5*this->kg[1] * this->kg[1] + 2*this->kg[2] * this->kg[2])) +
                                                       (4*pow(p0,2)*this->q[0] * this->q[0] + 2*pow(this->q[0],4) + pow(this->q[1] * this->q[1] + this->q[3] * this->q[3],2) +
                                                        3*this->q[1]*(this->q[1] * this->q[1] + this->q[3] * this->q[3])*this->kg[1] + 2*(this->q[1] - this->q[3])*(this->q[1] + this->q[3])*this->kg[1] * this->kg[1] +
                                                        2*p0*this->q[0]*(-3*this->q[0] * this->q[0] + 2*this->q[1] * this->q[1] + 3*this->q[3] * this->q[3] + 3*this->q[1]*this->kg[1]) -
                                                        this->q[0] * this->q[0]*(3*(this->q[1] * this->q[1] + this->q[3] * this->q[3]) + 5*this->q[1]*this->kg[1]))*this->kg[3] +
                                                       8*this->mn2*(2*p0*this->q[0]*(this->q[3] + this->kg[3]) - this->q[0] * this->q[0]*(2*this->q[3] + this->kg[3]) + this->kg[3]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1] + 2*this->q[3]*this->kg[3]))
                                      ))))/(24.*mDelta2*pow(this->mn,4));

    tr[1][3] = (C3v*(2*C3vNC*this->mn*(16*mDelta2*this->mn*(-p0 + this->q[0])*this->q[1]*this->kg[3] + 4*pow(mDelta,3)*(this->q[0]*this->q[1]*(this->q[3] - 2*this->kg[3]) - 4*p0*this->q[1]*this->kg[3] + this->q[0]*this->kg[1]*this->kg[3]) -
                                       4*this->mn*(p0 - this->q[0])*(-(this->q[1]*this->q[3]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])) + this->q[1]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1])*this->kg[3] +
                                                                     this->q[0] * this->q[0]*(this->q[3]*(this->q[1] + this->kg[1]) - (2*this->q[1] + this->kg[1])*this->kg[3]) + 2*p0*this->q[0]*(this->kg[1]*this->kg[3] + this->q[1]*(this->q[3] + 2*this->kg[3]))) +
                                       mDelta*(4*p0*this->q[3]*(this->q[0] * this->q[0]*(this->q[1] - this->kg[1]) + 2*this->q[1]*this->kg[1] * this->kg[1]) -
                                               4*p0*(this->q[0] * this->q[0]*(2*this->q[1] - 3*this->kg[1]) + this->q[1]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1]))*this->kg[3] -
                                               4*pow(p0,2)*this->q[0]*(3*this->q[1]*this->q[3] + 2*this->q[1]*this->kg[3] + 3*this->kg[1]*this->kg[3]) +
                                               this->q[0]*(this->q[1]*(this->q[3] - 2*this->kg[3]) + this->kg[1]*this->kg[3])*(-3*this->q[0] * this->q[0] + this->q[1] * this->q[1] + 2*this->q[1]*this->kg[1] + this->q[3]*(this->q[3] + 2*this->kg[3])))) -
                     this->c_i*C5aNC*this->kg[2]*(4*pow(mDelta,3)*this->mn*(8*this->mn2 + 2*(p0 - this->q[0])*this->q[0] + this->q[1]*(this->q[1] + this->kg[1])) -
                                                  2*this->mn2*this->q[3]*(this->q[3] + this->kg[3])*(2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) +
                                                  mDelta*this->mn*(4*pow(p0,2)*this->q[0] * this->q[0] - 6*p0*pow(this->q[0],3) + 2*pow(this->q[0],4) + 4*p0*this->q[0]*this->q[1] * this->q[1] -
                                                                   3*this->q[0] * this->q[0]*this->q[1] * this->q[1] + pow(this->q[1],4) + this->q[1] * this->q[1]*this->q[3] * this->q[3] + 6*p0*this->q[0]*this->q[1]*this->kg[1] - 5*this->q[0] * this->q[0]*this->q[1]*this->kg[1] +
                                                                   3*pow(this->q[1],3)*this->kg[1] + this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 2*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] + 2*this->q[3]*((p0 - this->q[0])*this->q[0] + this->q[1]*(this->q[1] + this->kg[1]))*this->kg[3] +
                                                                   8*this->mn2*(2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1] + 2*this->q[3]*this->kg[3])) +
                                                  mDelta2*(8*pow(p0,3)*this->q[0] + 4*pow(p0,2)*(-3*this->q[0] * this->q[0] + this->q[1]*(this->q[1] + this->kg[1])) +
                                                           8*this->mn2*(4*pow(p0,2) - 2*p0*this->q[0] - this->q[3]*this->kg[3]) + 2*p0*this->q[0]*(2*this->q[0] * this->q[0] - this->q[1]*(this->q[1] + this->kg[1]) - this->q[3]*(this->q[3] + 2*this->kg[3])) +
                                                           this->q[3]*(this->q[0] * this->q[0]*(-this->q[3] + this->kg[3]) + this->q[1]*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3]))))))/(24.*mDelta2*pow(this->mn,4));


    tr[2][0] = (C3v*(2*C3vNC*this->mn*this->kg[2]*(4*pow(mDelta,3)*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) -
                                                   4*this->mn*(p0 - this->q[0])*(this->q[0]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3]) + 2*p0*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3])) +
                                                   mDelta*(-4*p0*this->q[0]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] - 3*this->q[1]*this->kg[1] - 3*this->q[3]*this->kg[3]) - 12*pow(p0,2)*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) +
                                                           (this->q[1]*this->kg[1] + this->q[3]*this->kg[3])*(-3*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1] + 2*this->q[3]*this->kg[3]))) -
                     this->c_i*C5aNC*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3])*(8*pow(mDelta,3)*this->mn*(-p0 + this->q[0]) +
                                                                                        2*mDelta*this->mn*(p0 - this->q[0])*(8*this->mn2 - 2*p0*this->q[0] + this->q[0] * this->q[0] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3]) -
                                                                                        4*this->mn2*(p0 - this->q[0])*(2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) +
                                                                                        mDelta2*(-8*pow(p0,3) + 12*pow(p0,2)*this->q[0] + 8*this->mn2*(2*p0 + this->q[0]) +
                                                                                                 this->q[0]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3]) +
                                                                                                 2*p0*(-2*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1] + 2*this->q[3]*this->kg[3])))))/(24.*mDelta2*pow(this->mn,4));

    tr[2][1] = (C3v*(2*C3vNC*this->mn*this->kg[2]*(4*pow(mDelta,3)*this->q[0]*this->kg[1] - 4*this->mn*(p0 - this->q[0])*(this->q[0] * this->q[0]*(this->q[1] - this->kg[1]) + 2*p0*this->q[0]*this->kg[1] + this->q[3]*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3])) +
                                                   mDelta*(-12*pow(p0,2)*this->q[0]*this->kg[1] + this->q[0]*this->kg[1]*(-3*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1] + 2*this->q[3]*this->kg[3]) -
                                                           4*p0*(this->q[0] * this->q[0]*(this->q[1] - 3*this->kg[1]) + 2*this->q[3]*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3])))) -
                     this->c_i*C5aNC*(-2*this->mn2*(this->q[1] + this->kg[1])*(-(this->q[3]*this->kg[1]) + this->q[1]*this->kg[3])*(2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) +
                                      4*pow(mDelta,3)*this->mn*(this->q[3]*(this->q[0] * this->q[0] + this->q[1]*this->kg[1] - this->kg[2] * this->kg[2]) + (8*this->mn2 + 2*(p0 - this->q[0])*this->q[0] + this->q[3] * this->q[3])*this->kg[3]) +
                                      mDelta2*(8*pow(p0,3)*this->q[0]*this->kg[3] + 4*pow(p0,2)*
                                                                                    (this->q[3]*this->kg[1]*(this->q[1] + this->kg[1]) + this->q[0] * this->q[0]*(this->q[3] - 3*this->kg[3]) + this->q[3]*this->kg[3]*(this->q[3] + this->kg[3])) +
                                               2*p0*this->q[0]*(this->q[3]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] + this->q[1]*this->kg[1] + 2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) - this->q[1]*(this->q[1] + 2*this->kg[1])*this->kg[3] +
                                                                this->q[0] * this->q[0]*(-3*this->q[3] + 2*this->kg[3])) + (this->q[3]*this->kg[1] - this->q[1]*this->kg[3])*(this->q[0] * this->q[0]*(this->q[1] - this->kg[1]) + this->q[3]*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3])) +
                                               8*this->mn2*(2*p0*this->q[0]*(this->q[3] - this->kg[3]) + 4*pow(p0,2)*this->kg[3] + this->kg[1]*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3]))) +
                                      mDelta*this->mn*(this->q[3]*(-pow(this->q[0],4) + this->kg[1]*(pow(this->q[1],3) + this->q[1]*this->q[3] * this->q[3] + 2*this->q[1] * this->q[1]*this->kg[1] - 2*this->q[3] * this->q[3]*this->kg[1]) -
                                                                   (this->q[1] * this->q[1] + 3*this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1])*this->kg[2] * this->kg[2] +
                                                                   this->q[0] * this->q[0]*(-this->q[1] * this->q[1] + this->q[3] * this->q[3] - 3*this->q[1]*this->kg[1] + 2*this->kg[1] * this->kg[1] + 5*this->kg[2] * this->kg[2])) +
                                                       (2*pow(this->q[0],4) - this->q[0] * this->q[0]*(3*this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1]) +
                                                        this->q[3] * this->q[3]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] + 4*this->q[1]*this->kg[1] - 2*this->kg[2] * this->kg[2]))*this->kg[3] + 4*pow(p0,2)*this->q[0] * this->q[0]*(this->q[3] + this->kg[3]) +
                                                       8*this->mn2*(2*p0*this->q[0]*(this->q[3] + this->kg[3]) - this->q[0] * this->q[0]*(2*this->q[3] + this->kg[3]) +
                                                                    this->kg[3]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1] + 2*this->q[3]*this->kg[3])) +
                                                       2*p0*this->q[0]*(this->q[1] * this->q[1]*this->q[3] + this->q[1]*this->kg[1]*(3*this->q[3] + this->kg[3]) - this->q[0] * this->q[0]*(this->q[3] + 3*this->kg[3]) +
                                                                        this->q[3]*(this->q[3] * this->q[3] - this->kg[1] * this->kg[1] - 3*this->kg[2] * this->kg[2] + 3*this->q[3]*this->kg[3]))))))/(24.*mDelta2*pow(this->mn,4));

    tr[2][2] = -(C3v*(this->c_i*C5aNC*this->kg[2]*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3])*(4*pow(mDelta,3)*this->mn +
                                                                                                     mDelta2*(8*this->mn2 + 4*pow(p0,2) + 2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3]) +
                                                                                                     2*this->mn2*(2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) +
                                                                                                     mDelta*this->mn*(4*p0*this->q[0] - 3*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1] + 2*this->q[3]*this->kg[3])) +
                      2*C3vNC*this->mn*(16*mDelta2*this->mn*(p0 - this->q[0])*(this->q[0] * this->q[0] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3]) +
                                        4*pow(mDelta,3)*(this->q[0]*(this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] - 2*this->q[1]*this->kg[1] - this->kg[2] * this->kg[2] - 2*this->q[3]*this->kg[3]) +
                                                         4*p0*(this->q[0] * this->q[0] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3])) -
                                        4*this->mn*(p0 - this->q[0])*(4*pow(p0,2)*this->q[0] * this->q[0] + 2*pow(this->q[0],4) + pow(this->q[1],3)*this->kg[1] + this->q[1]*this->q[3] * this->q[3]*this->kg[1] +
                                                                      this->q[1] * this->q[1]*this->kg[1] * this->kg[1] - this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - this->q[1] * this->q[1]*this->kg[2] * this->kg[2] - 2*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] +
                                                                      this->q[3]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1])*this->kg[3] +
                                                                      2*p0*this->q[0]*(-3*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1] - this->kg[2] * this->kg[2] + 2*this->q[3]*this->kg[3]) -
                                                                      this->q[0] * this->q[0]*(this->q[1] * this->q[1] + 3*this->q[1]*this->kg[1] - this->kg[2] * this->kg[2] + 3*this->q[3]*this->kg[3])) -
                                        mDelta*(16*pow(p0,3)*this->q[0] * this->q[0] + 4*pow(p0,2)*this->q[0]*
                                                                                       (-5*this->q[0] * this->q[0] + 3*this->q[1] * this->q[1] + 3*this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1] - 3*this->kg[2] * this->kg[2] + 2*this->q[3]*this->kg[3]) +
                                                this->q[0]*(this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] - 2*this->q[1]*this->kg[1] - this->kg[2] * this->kg[2] - 2*this->q[3]*this->kg[3])*
                                                (3*this->q[0] * this->q[0] - this->q[1]*(this->q[1] + 2*this->kg[1]) - this->q[3]*(this->q[3] + 2*this->kg[3])) +
                                                4*p0*(pow(this->q[1],3)*this->kg[1] + this->q[1]*this->q[3] * this->q[3]*this->kg[1] + this->q[3] * this->q[3]*(-2*this->kg[2] * this->kg[2] + this->q[3]*this->kg[3]) +
                                                      this->q[0] * this->q[0]*(this->q[1]*(-4*this->q[1] + this->kg[1]) + 3*this->kg[2] * this->kg[2] + this->q[3]*(-2*this->q[3] + this->kg[3])) +
                                                      this->q[1] * this->q[1]*(2*this->kg[1] * this->kg[1] + this->kg[3]*(this->q[3] + 2*this->kg[3])))))))/(24.*mDelta2*pow(this->mn,4));

    tr[2][3] = (C3v*(2*C3vNC*this->mn*this->kg[2]*(4*pow(mDelta,3)*this->q[0]*this->kg[3] - 4*this->mn*(p0 - this->q[0])*
                                                                                            (this->q[0] * this->q[0]*(this->q[3] - this->kg[3]) + 2*p0*this->q[0]*this->kg[3] + this->q[1]*(-(this->q[3]*this->kg[1]) + this->q[1]*this->kg[3])) +
                                                   mDelta*(-12*pow(p0,2)*this->q[0]*this->kg[3] + this->q[0]*this->kg[3]*(-3*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1] + 2*this->q[3]*this->kg[3]) -
                                                           4*p0*(this->q[0] * this->q[0]*(this->q[3] - 3*this->kg[3]) + 2*this->q[1]*(-(this->q[3]*this->kg[1]) + this->q[1]*this->kg[3])))) +
                     this->c_i*C5aNC*(-2*this->mn2*(this->q[3] + this->kg[3])*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3])*(2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) +
                                      4*pow(mDelta,3)*this->mn*(this->q[0] * this->q[0]*(this->q[1] - 2*this->kg[1]) + 8*this->mn2*this->kg[1] + 2*p0*this->q[0]*this->kg[1] + this->q[1]*(this->q[1]*this->kg[1] - this->kg[2] * this->kg[2] + this->q[3]*this->kg[3])) +
                                      mDelta2*(8*pow(p0,3)*this->q[0]*this->kg[1] + 4*pow(p0,2)*
                                                                                    (this->q[0] * this->q[0]*(this->q[1] - 3*this->kg[1]) + this->q[1]*this->kg[1]*(this->q[1] + this->kg[1]) + this->q[1]*this->kg[3]*(this->q[3] + this->kg[3])) +
                                               (this->q[3]*this->kg[1] - this->q[1]*this->kg[3])*(this->q[0] * this->q[0]*(-this->q[3] + this->kg[3]) + this->q[1]*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3])) +
                                               8*this->mn2*(2*p0*this->q[0]*(this->q[1] - this->kg[1]) + 4*pow(p0,2)*this->kg[1] + this->kg[3]*(-(this->q[3]*this->kg[1]) + this->q[1]*this->kg[3])) -
                                               2*p0*this->q[0]*(-pow(this->q[1],3) + this->q[0] * this->q[0]*(this->q[1] - 2*this->kg[1]) + this->q[3]*this->kg[1]*(this->q[3] + 2*this->kg[3]) +
                                                                this->q[1]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] - this->q[3]*(this->q[3] + this->kg[3])))) +
                                      mDelta*this->mn*(pow(this->q[0],4)*this->q[1] - this->q[0] * this->q[0]*pow(this->q[1],3) + this->q[0] * this->q[0]*this->q[1]*this->q[3] * this->q[3] + 2*pow(this->q[0],4)*this->kg[1] -
                                                       3*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[1] + pow(this->q[1],4)*this->kg[1] + this->q[1] * this->q[1]*this->q[3] * this->q[3]*this->kg[1] - 2*this->q[0] * this->q[0]*this->q[1]*this->kg[1] * this->kg[1] +
                                                       2*pow(this->q[1],3)*this->kg[1] * this->kg[1] - 2*this->q[1]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 4*pow(p0,2)*this->q[0] * this->q[0]*(this->q[1] + this->kg[1]) +
                                                       3*this->q[0] * this->q[0]*this->q[1]*this->kg[2] * this->kg[2] - pow(this->q[1],3)*this->kg[2] * this->kg[2] - 3*this->q[1]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] -
                                                       2*this->q[1] * this->q[1]*this->kg[1]*this->kg[2] * this->kg[2] - 3*this->q[0] * this->q[0]*this->q[1]*this->q[3]*this->kg[3] + pow(this->q[1],3)*this->q[3]*this->kg[3] + this->q[1]*pow(this->q[3],3)*this->kg[3] -
                                                       2*this->q[0] * this->q[0]*this->q[3]*this->kg[1]*this->kg[3] + 4*this->q[1] * this->q[1]*this->q[3]*this->kg[1]*this->kg[3] - 2*this->q[1]*this->q[3]*this->kg[2] * this->kg[2]*this->kg[3] +
                                                       8*this->mn2*(2*p0*this->q[0]*(this->q[1] + this->kg[1]) - this->q[0] * this->q[0]*(2*this->q[1] + this->kg[1]) +
                                                                    this->kg[1]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1] + 2*this->q[3]*this->kg[3])) +
                                                       2*p0*this->q[0]*(pow(this->q[1],3) + 3*this->q[1] * this->q[1]*this->kg[1] - 2*this->q[0] * this->q[0]*(this->q[1] + 2*this->kg[1]) +
                                                                        this->q[1]*(this->q[3] * this->q[3] + this->kg[1] * this->kg[1] - 2*this->kg[2] * this->kg[2] + 3*this->q[3]*this->kg[3]) + this->kg[1]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3]*(this->q[3] + this->kg[3]))))))
               )/(24.*mDelta2*pow(this->mn,4));


    tr[3][0] = -(C3v*(this->c_i*C5aNC*this->q[1]*this->kg[2]*(8*pow(mDelta,3)*this->mn*(-p0 + this->q[0]) +
                                                              2*mDelta*this->mn*(p0 - this->q[0])*(8*this->mn2 - 2*p0*this->q[0] + this->q[0] * this->q[0] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3]) -
                                                              4*this->mn2*(p0 - this->q[0])*(2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) +
                                                              mDelta2*(-8*pow(p0,3) + 12*pow(p0,2)*this->q[0] + 8*this->mn2*(2*p0 + this->q[0]) +
                                                                       this->q[0]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3]) +
                                                                       2*p0*(-2*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1] + 2*this->q[3]*this->kg[3]))) +
                      2*C3vNC*this->mn*(16*mDelta2*this->mn*(p0 - this->q[0])*this->q[0]*this->q[3] +
                                        4*pow(mDelta,3)*(this->q[3]*(4*p0*this->q[0] + this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) - this->q[1]*this->kg[1]*this->kg[3]) +
                                        4*this->mn*(p0 - this->q[0])*(-4*pow(p0,2)*this->q[0]*this->q[3] + 8*p0*this->q[0] * this->q[0]*this->q[3] - 3*pow(this->q[0],3)*this->q[3] -
                                                                      2*p0*this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + this->q[0]*this->q[3]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] + this->q[1]*this->kg[1] + this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) +
                                                                      2*p0*this->q[1]*this->kg[1]*this->kg[3] + this->q[0]*(this->q[1] * this->q[1] + 2*this->q[3] * this->q[3] - this->q[1]*this->kg[1])*this->kg[3]) +
                                        mDelta*(-16*pow(p0,3)*this->q[0]*this->q[3] + 4*pow(p0,2)*(8*this->q[0] * this->q[0]*this->q[3] - 3*this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + 3*this->q[1]*this->kg[1]*this->kg[3]) +
                                                (this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) - this->q[1]*this->kg[1]*this->kg[3])*(-3*this->q[0] * this->q[0] + this->q[1] * this->q[1] + 2*this->q[1]*this->kg[1] + this->q[3]*(this->q[3] + 2*this->kg[3])) +
                                                4*p0*this->q[0]*(-3*this->q[0] * this->q[0]*this->q[3] + this->q[1]*this->kg[1]*(this->q[3] - 3*this->kg[3]) + this->q[1] * this->q[1]*(this->q[3] + this->kg[3]) +
                                                                 this->q[3]*(3*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + this->q[3]*(this->q[3] + 2*this->kg[3])))))))/(24.*mDelta2*pow(this->mn,4));

    tr[3][1] = (C3v*(this->c_i*C5aNC*this->kg[2]*(-2*this->mn2*this->q[1]*(this->q[1] + this->kg[1])*(2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) +
                                                  4*pow(mDelta,3)*this->mn*(8*this->mn2 + 2*(p0 - this->q[0])*this->q[0] + this->q[3]*(this->q[3] + this->kg[3])) +
                                                  mDelta*this->mn*(4*pow(p0,2)*this->q[0] * this->q[0] - 6*p0*pow(this->q[0],3) + 2*pow(this->q[0],4) + 4*p0*this->q[0]*this->q[3] * this->q[3] -
                                                                   this->q[0] * this->q[0]*this->q[3] * this->q[3] + this->q[1] * this->q[1]*this->q[3] * this->q[3] + pow(this->q[3],4) + 2*p0*this->q[0]*this->q[1]*this->kg[1] - 2*this->q[0] * this->q[0]*this->q[1]*this->kg[1] +
                                                                   2*this->q[1]*this->q[3] * this->q[3]*this->kg[1] - 2*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 2*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] +
                                                                   this->q[3]*(6*p0*this->q[0] - 5*this->q[0] * this->q[0] + this->q[1] * this->q[1] + 3*this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1])*this->kg[3] +
                                                                   8*this->mn2*(2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1] + 2*this->q[3]*this->kg[3])) +
                                                  mDelta2*(8*pow(p0,3)*this->q[0] + 8*this->mn2*(4*pow(p0,2) - 2*p0*this->q[0] - this->q[1]*this->kg[1]) +
                                                           2*p0*this->q[0]*(2*this->q[0] * this->q[0] - this->q[1]*(this->q[1] + 2*this->kg[1]) - this->q[3]*(this->q[3] + this->kg[3])) + 4*pow(p0,2)*(-3*this->q[0] * this->q[0] + this->q[3]*(this->q[3] + this->kg[3])) +
                                                           this->q[1]*(this->q[0] * this->q[0]*(-this->q[1] + this->kg[1]) + this->q[3]*(-(this->q[3]*this->kg[1]) + this->q[1]*this->kg[3])))) +
                     2*C3vNC*this->mn*(16*mDelta2*this->mn*(-p0 + this->q[0])*this->q[3]*this->kg[1] + 4*pow(mDelta,3)*(this->q[0]*this->q[1]*this->q[3] - 4*p0*this->q[3]*this->kg[1] + this->q[0]*this->kg[1]*(-2*this->q[3] + this->kg[3])) -
                                       4*this->mn*(p0 - this->q[0])*(2*p0*this->q[0]*(this->q[1]*this->q[3] + this->kg[1]*(2*this->q[3] + this->kg[3])) - this->q[0] * this->q[0]*(this->q[1]*(this->q[3] - this->kg[3]) + this->kg[1]*(2*this->q[3] + this->kg[3])) +
                                                                     this->q[3]*(this->q[1] * this->q[1]*this->kg[1] + this->q[1]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + this->q[3]*this->kg[1]*(this->q[3] + 2*this->kg[3]))) +
                                       mDelta*(-4*pow(p0,2)*this->q[0]*(3*this->q[1]*this->q[3] + 2*this->q[3]*this->kg[1] + 3*this->kg[1]*this->kg[3]) +
                                               this->q[0]*(-3*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1] + 2*this->q[3]*this->kg[3])*(this->q[1]*this->q[3] + this->kg[1]*(-2*this->q[3] + this->kg[3])) -
                                               4*p0*(this->q[0] * this->q[0]*(-3*this->q[1]*this->q[3] + 2*this->q[3]*this->kg[1] + this->q[1]*this->kg[3] - 3*this->kg[1]*this->kg[3]) +
                                                     this->q[3]*(this->q[1] * this->q[1]*this->kg[1] + 2*this->q[1]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + this->q[3]*this->kg[1]*(this->q[3] + 2*this->kg[3])))))))/(24.*mDelta2*pow(this->mn,4));

    tr[3][2] = (C3v*(-2*C3vNC*this->mn*this->kg[2]*(16*mDelta2*this->mn*(p0 - this->q[0])*this->q[3] + 4*pow(mDelta,3)*(4*p0*this->q[3] + 2*this->q[0]*this->q[3] - this->q[0]*this->kg[3]) +
                                                    4*this->mn*(p0 - this->q[0])*(this->q[3]*(4*p0*this->q[0] - 2*this->q[0] * this->q[0] + this->q[3] * this->q[3] + this->q[1]*(this->q[1] + this->kg[1])) +
                                                                                  (2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1] * this->q[1] + 2*this->q[3] * this->q[3])*this->kg[3]) +
                                                    mDelta*(4*pow(p0,2)*this->q[0]*(2*this->q[3] + 3*this->kg[3]) + this->q[0]*(2*this->q[3] - this->kg[3])*
                                                                                                                    (-3*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1] + 2*this->q[3]*this->kg[3]) +
                                                            4*p0*(this->q[0] * this->q[0]*(2*this->q[3] - 3*this->kg[3]) + (this->q[1] * this->q[1] + this->q[3] * this->q[3])*(this->q[3] + 2*this->kg[3])))) -
                     this->c_i*C5aNC*(2*this->mn2*this->q[1]*this->kg[2] * this->kg[2]*(2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) +
                                      4*pow(mDelta,3)*this->mn*(8*this->mn2*this->kg[1] + 2*p0*this->q[0]*this->kg[1] - 2*this->q[0] * this->q[0]*this->kg[1] + this->q[1] * this->q[1]*this->kg[1] + this->q[3] * this->q[3]*this->kg[1] +
                                                                this->q[1]*this->kg[1] * this->kg[1] + this->q[1]*this->kg[2] * this->kg[2] + this->q[3]*this->kg[1]*this->kg[3]) +
                                      mDelta2*(8*pow(p0,3)*this->q[0]*this->kg[1] + this->q[1]*(-this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3])*this->kg[2] * this->kg[2] +
                                               8*this->mn2*(2*p0*this->q[0]*(this->q[1] - this->kg[1]) + 4*pow(p0,2)*this->kg[1] + this->q[1]*this->kg[2] * this->kg[2]) +
                                               4*pow(p0,2)*(this->q[0] * this->q[0]*(this->q[1] - 3*this->kg[1]) + this->q[1] * this->q[1]*this->kg[1] + this->q[1]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + this->q[3]*this->kg[1]*(this->q[3] + this->kg[3])) -
                                               2*p0*this->q[0]*(-pow(this->q[1],3) + 2*this->q[0] * this->q[0]*(this->q[1] - this->kg[1]) + this->q[3]*this->kg[1]*(this->q[3] + this->kg[3]) -
                                                                this->q[1]*(-this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->q[3]*(this->q[3] + this->kg[3])))) +
                                      mDelta*this->mn*(4*pow(this->q[0],4)*this->q[1] - 2*this->q[0] * this->q[0]*pow(this->q[1],3) - 2*this->q[0] * this->q[0]*this->q[1]*this->q[3] * this->q[3] + 2*pow(this->q[0],4)*this->kg[1] -
                                                       5*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[1] + pow(this->q[1],4)*this->kg[1] - this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[1] + 2*this->q[1] * this->q[1]*this->q[3] * this->q[3]*this->kg[1] +
                                                       pow(this->q[3],4)*this->kg[1] - 5*this->q[0] * this->q[0]*this->q[1]*this->kg[1] * this->kg[1] + 3*pow(this->q[1],3)*this->kg[1] * this->kg[1] + 3*this->q[1]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] +
                                                       2*this->q[1] * this->q[1]*pow(this->kg[1],3) - 2*this->q[3] * this->q[3]*pow(this->kg[1],3) + 4*pow(p0,2)*this->q[0] * this->q[0]*(this->q[1] + this->kg[1]) -
                                                       3*this->q[0] * this->q[0]*this->q[1]*this->kg[2] * this->kg[2] + pow(this->q[1],3)*this->kg[2] * this->kg[2] + this->q[1]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] +
                                                       2*this->q[1] * this->q[1]*this->kg[1]*this->kg[2] * this->kg[2] - 2*this->q[3] * this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] +
                                                       this->q[3]*(-(this->q[0] * this->q[0]*(2*this->q[1] + 5*this->kg[1])) + this->kg[1]*(3*(this->q[1] * this->q[1] + this->q[3] * this->q[3]) + 4*this->q[1]*this->kg[1]) + 2*this->q[1]*this->kg[2] * this->kg[2])*this->kg[3] +
                                                       8*this->mn2*(2*p0*this->q[0]*(this->q[1] + this->kg[1]) - this->q[0] * this->q[0]*(2*this->q[1] + this->kg[1]) +
                                                                    this->kg[1]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1] + 2*this->q[3]*this->kg[3])) +
                                                       2*p0*this->q[0]*(pow(this->q[1],3) + 3*this->q[1] * this->q[1]*this->kg[1] - this->q[0] * this->q[0]*(4*this->q[1] + 3*this->kg[1]) + this->q[3]*this->kg[1]*(2*this->q[3] + 3*this->kg[3]) +
                                                                        this->q[1]*(3*this->kg[1] * this->kg[1] + 2*this->kg[2] * this->kg[2] + this->q[3]*(this->q[3] + this->kg[3])))))))/(24.*mDelta2*pow(this->mn,4));

    tr[3][3] = -(C3v*(this->c_i*C5aNC*this->q[1]*this->kg[2]*(4*pow(mDelta,3)*this->mn*(this->q[3] + this->kg[3]) +
                                                              2*this->mn2*(this->q[3] + this->kg[3])*(2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) +
                                                              mDelta*this->mn*(this->q[3] + this->kg[3])*(4*p0*this->q[0] - 3*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1] + 2*this->q[3]*this->kg[3]) +
                                                              mDelta2*(-(this->q[1]*this->q[3]*this->kg[1]) + this->q[0] * this->q[0]*(this->q[3] - this->kg[3]) + 8*this->mn2*this->kg[3] + 2*p0*this->q[0]*this->kg[3] + this->q[1] * this->q[1]*this->kg[3] +
                                                                       4*pow(p0,2)*(this->q[3] + this->kg[3]))) + 2*C3vNC*this->mn*
                                                                                                                  (16*mDelta2*this->mn*(p0 - this->q[0])*(this->q[0] * this->q[0] - this->q[1]*this->kg[1]) +
                                                                                                                   4*pow(mDelta,3)*(4*p0*(this->q[0] * this->q[0] - this->q[1]*this->kg[1]) + this->q[0]*(pow(this->q[1] - this->kg[1],2) + this->kg[2] * this->kg[2])) -
                                                                                                                   4*this->mn*(p0 - this->q[0])*(4*pow(p0,2)*this->q[0] * this->q[0] + 3*pow(this->q[0],4) +
                                                                                                                                                 2*p0*this->q[0]*(-4*this->q[0] * this->q[0] + pow(this->q[1] + this->kg[1],2) + this->kg[2] * this->kg[2]) -
                                                                                                                                                 this->q[0] * this->q[0]*(this->q[3] * this->q[3] + (this->q[1] + this->kg[1])*(2*this->q[1] + this->kg[1]) + this->kg[2] * this->kg[2] + 2*this->q[3]*this->kg[3]) +
                                                                                                                                                 this->q[1]*(this->q[1] * this->q[1]*this->kg[1] + this->q[1]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + this->q[3]*this->kg[1]*(this->q[3] + 2*this->kg[3]))) +
                                                                                                                   mDelta*(-16*pow(p0,3)*this->q[0] * this->q[0] + 4*pow(p0,2)*this->q[0]*
                                                                                                                                                                   (8*this->q[0] * this->q[0] - 3*this->q[1] * this->q[1] - 2*this->q[1]*this->kg[1] - 3*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])) +
                                                                                                                           this->q[0]*(pow(this->q[1] - this->kg[1],2) + this->kg[2] * this->kg[2])*(-3*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1] + 2*this->q[3]*this->kg[3]) -
                                                                                                                           4*p0*(3*pow(this->q[0],4) - this->q[0] * this->q[0]*(4*this->q[1] * this->q[1] - this->q[1]*this->kg[1] + 3*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + this->q[3]*(this->q[3] + 2*this->kg[3])) +
                                                                                                                                 this->q[1]*(this->q[1] * this->q[1]*this->kg[1] + 2*this->q[1]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + this->q[3]*this->kg[1]*(this->q[3] + 2*this->kg[3])))))))/
               (24.*mDelta2*pow(this->mn,4));

}

double Hadronic_Current_R_Delta::qcm(double s) {
//    Returns the 3-momentum of the pion formed after the decay of a
//    resonance (R->N pi) of inv. mass s in the rest frame of the resonance
    return sqrt((s-this->mpi2-this->mn2)*(s-this->mpi2-this->mn2) - 4.0 * this->mpi2 * this->mn2)/2.0/sqrt(s);
}










