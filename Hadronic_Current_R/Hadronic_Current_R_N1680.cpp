//
// Created by eduardo on 5/07/19.
//

#include <Form_Factors_N1680.h>
#include <Parameters_GeV.h>
#include "Hadronic_Current_R_N1680.h"

Hadronic_Current_R_N1680::Hadronic_Current_R_N1680(Nuclear_FF *nuclearFf) : Hadronic_Current_tr_J32(nuclearFf) {
    this->mRes = Param::mN1680;
    this->mRes_2 = this->mRes * this->mRes;
    this->ff_Res = new Form_Factors_N1680();
    this->gamma = Param::Gamma_N1680;

    this->parity = "positive";
    this->isospin_by_2 = 1;

    this->set_EM_FF();
}

Hadronic_Current_R_N1680::~Hadronic_Current_R_N1680() {
    delete this->ff_Res;
}