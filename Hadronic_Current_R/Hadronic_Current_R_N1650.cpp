//
// Created by eduardo on 5/07/19.
//

#include <Parameters_GeV.h>
#include <Form_Factors_N1650.h>
#include "Hadronic_Current_R_N1650.h"

Hadronic_Current_R_N1650::Hadronic_Current_R_N1650(Nuclear_FF *nuclearFf) : Hadronic_Current_tr_J12(nuclearFf) {
    this->mRes = Param::mN1650;
    this->mRes_2 = this->mRes * this->mRes;
    this->ff_Res = new Form_Factors_N1650();
    this->gamma = Param::Gamma_N1650;

    this->parity = "negative";
    this->isospin_by_2 = 1;

    this->set_EM_FF();
}

Hadronic_Current_R_N1650::~Hadronic_Current_R_N1650() {
    delete this->ff_Res;
}
