//
// Created by eduardo on 3/07/19.
//

#include <algorithm>
#include <iostream>
#include <Parameters_GeV.h>
#include "Hadronic_Current_tr_J12.h"
#include <fstream>

Hadronic_Current_tr_J12::Hadronic_Current_tr_J12(Nuclear_FF *nuclearFf) : nuclearFF(nuclearFf) {}

std::complex<double> Hadronic_Current_tr_J12::getR(int i, int j) {
    std::complex<double> tr_p = this->tr_p_dir[i][j]*this->propagator_dir
                                + this->tr_p_crs[i][j]*this->propagator_crs;
    std::complex<double> tr_n = this->tr_n_dir[i][j]*this->propagator_dir
                                + this->tr_n_crs[i][j]*this->propagator_crs;

    return (tr_p * this->N_FF_p + tr_n * this->N_FF_n) * this->mn / this->p0 / 2.0; //Factor 1/2 from the trace, see notes
}

void Hadronic_Current_tr_J12::setFF(double q2) {
    double Q2 = -q2;
    this->ff_Res->setFF(Q2);
}

void Hadronic_Current_tr_J12::setKg(const std::vector<double> &kg) {
    Hadronic_Current_R::setKg(kg);

    if(isospin_by_2 == 1) {
        this->F1 = this->F1p;
        this->F2 = this->F2p;
        this->F1NC = this->ff_Res->getF1Zp();
        this->F2NC = this->ff_Res->getF2Zp();
        this->FANC = this->ff_Res->getFaZp();
        this->set_tr_dir(tr_p_dir);
        this->set_tr_crs(tr_p_crs);

        this->F1 = this->F1n;
        this->F2 = this->F2n;
        this->F1NC = this->ff_Res->getF1Zn();
        this->F2NC = this->ff_Res->getF2Zn();
        this->FANC = this->ff_Res->getFaZn();
        this->set_tr_dir(tr_n_dir);
        this->set_tr_crs(tr_n_crs);
    } else if(isospin_by_2 == 3) {
        this->F1 = this->F1pn;
        this->F2 = this->F2pn;
        this->F1NC = this->ff_Res->getF1Zpn();
        this->F2NC = this->ff_Res->getF2Zpn();
        this->FANC = this->ff_Res->getFaZpn();

        this->set_tr_dir(tr_p_dir);
        this->set_tr_crs(tr_p_crs);

        for (int i = 0; i < 4; ++i) {
            for (int j = 0; j < 4; ++j) {
                this->tr_n_dir[i][j] = this->tr_p_dir[i][j];
                this->tr_n_crs[i][j] = this->tr_p_crs[i][j];
            }
        }
//        this->set_tr_dir(tr_n_dir);
//        this->set_tr_crs(tr_n_crs);
    }

    std::vector<double> pm = this->q;
    std::transform(pm.begin( ), pm.end( ), this->kg.begin( ), pm.begin( ),std::minus<>( ));
    this->qv_minus_kgv = sqrt(pm[1]*pm[1]+pm[2]*pm[2]+pm[3]*pm[3]);
    this->nuclearFF->setFF(qv_minus_kgv);
    this->N_FF_p = this->nuclearFF->getFfP();
    this->N_FF_n = this->nuclearFF->getFfN();
}

std::complex<double> Hadronic_Current_tr_J12::Propagator(std::vector<double> p) {
    double p2 =  p[0] * p[0] - p[1] * p[1] - p[2] * p[2] - p[3] * p[3] ;

    double Gamma = 0.0;
    double threshold = (this->mn + this->mpi)*(this->mn + this->mpi);
    if(p2 > threshold) {
//        Gamma = this->gamma; // GeV
        Gamma = this->Gamma_pi_N(p2) + (1.0 - this->ff_Res->getBranchingPiN()) * this->gamma;
    }
    return 1.0/(p2 - this->mRes_2 + this->c_i * this->mRes * Gamma);
}

void Hadronic_Current_tr_J12::setP(std::vector<double> p) {
    Hadronic_Current_R::setP(p);

    std::vector<double> p_dir = this->p;
    std::transform(p_dir.begin( ), p_dir.end( ), this->q.begin( ), p_dir.begin( ),std::plus<>( ));

    this->propagator_dir = Propagator(p_dir);

    std::vector<double> p_crs = this->pp;
    std::transform(p_crs.begin( ), p_crs.end( ), this->q.begin( ), p_crs.begin( ),std::minus<>( ));
    this->propagator_crs = Propagator(p_crs);
}

void Hadronic_Current_tr_J12::set_tr_dir(Array4x4 &tr){
    if(this->parity == "positive") this->set_tr_dir_positive(tr);
    else if(this->parity == "negative") this->set_tr_dir_negative(tr);
    else std::cout<<"ERROR: wrong parity!!!!!!!"<< std::endl;
}

void Hadronic_Current_tr_J12::set_tr_crs(Array4x4 &tr){
    if(this->parity == "positive") this->set_tr_crs_positive(tr);
    else if(this->parity == "negative") this->set_tr_crs_negative(tr);
    else std::cout<<"ERROR: wrong parity!!!!!!!"<< std::endl;
}

void Hadronic_Current_tr_J12::set_EM_FF() {
    this->ff_Res->setFF(0.0);
    if(isospin_by_2 == 1) {
        this->F1p = this->ff_Res->getF1P();
        this->F2p = this->ff_Res->getF2P();
        this->F1n = this->ff_Res->getF1N();
        this->F2n = this->ff_Res->getF2N();
    } else if(isospin_by_2 == 3) {
        this->F1pn = this->ff_Res->getF1Pn();
        this->F2pn = this->ff_Res->getF2Pn();
    }
}

double Hadronic_Current_tr_J12::Gamma_pi_N(double W2) {
    double iR = 3.0;
    if(this->isospin_by_2 == 3) iR = 1.0;
    double W = sqrt(W2);
    double qcm = this->q_cm(W);
    double eN = (W2 + this->mn2 - this->mpi2)/(2.0 * W);
    double sign = 1.0;
    if(this->parity == "negative") sign = -1.0;

    return iR/(4.0 * Param::pi * W) * (this->ff_Res->getCoupling()/this->mpi)* (this->ff_Res->getCoupling()/this->mpi)
        * (W + sign*this->mn) * (W + sign*this->mn) * (eN - sign*this->mn) * qcm;

}
