//
// Created by eduardo on 4/07/19.
//

#ifndef COHERENT_PHOTON_HADRONIC_CURRENT_TR_J32_H
#define COHERENT_PHOTON_HADRONIC_CURRENT_TR_J32_H


#include <Form_Factors_J32.h>
#include <Nuclear_FF.h>
#include "Hadronic_Current_R.h"

class Hadronic_Current_tr_J32 : public Hadronic_Current_R{
protected:
    Form_Factors_J32 *ff_Res;
    Nuclear_FF *nuclearFF;
    std::string parity;
    int isospin_by_2;

    double mRes;
    double mRes_2;
    double gamma;
    double q_minus_kg;

    double C3p;
    double C4p;
    double C5p;
    double C3n;
    double C4n;
    double C5n;
    double C3pn;
    double C4pn;
    double C5pn;

    double C3v;
    double C4v;
    double C5v;
    double C3vNC;
    double C4vNC;
    double C5vNC;
    double C3a;
    double C4a;
    double C5a;
    double C6a;

    std::complex<double> N_FF_p;
    std::complex<double> N_FF_n;

    double qv_minus_kgv;
    std::complex<double> propagator_dir;
    std::complex<double> propagator_crs;

    std::string dir_or_crs;

    void set_tr_dir(Array4x4&) override;
    void set_tr_crs(Array4x4&) override;

    void set_tr_dir_positive(Array4x4&);
    void set_tr_crs_positive(Array4x4&);

    void set_tr_dir_negative(Array4x4&);
    void set_tr_crs_negative(Array4x4&);

    void set_EM_FF();

    double Gamma_pi_N(double W2);

public:

    explicit Hadronic_Current_tr_J32(Nuclear_FF *nuclearFf);

    std::complex<double> getR(int, int) override;

    void setFF(double) override;

    void setKg(const std::vector<double> &kg) override;

    std::complex<double> Propagator(std::vector<double>);

    void setP(std::vector<double>) override;

};


#endif //COHERENT_PHOTON_HADRONIC_CURRENT_TR_J32_H
