//
// Created by eduardo on 5/07/19.
//

#ifndef COHERENT_PHOTON_HADRONIC_CURRENT_R_N1675_H
#define COHERENT_PHOTON_HADRONIC_CURRENT_R_N1675_H


#include "Hadronic_Current_tr_J32.h"

class Hadronic_Current_R_N1675 : public Hadronic_Current_tr_J32 {

public:

    explicit Hadronic_Current_R_N1675(Nuclear_FF *nuclearFf);

    virtual ~Hadronic_Current_R_N1675();

};


#endif //COHERENT_PHOTON_HADRONIC_CURRENT_R_N1675_H
