//
// Created by edusaul on 28/04/19.
//

#include <Parameters_GeV.h>
#include <algorithm>
#include <iostream>
#include <Form_Factors_N1520.h>
#include "Hadronic_Current_R_N1520.h"

Hadronic_Current_R_N1520::Hadronic_Current_R_N1520(Nuclear_FF *nuclearFf) : Hadronic_Current_tr_J32(nuclearFf) {
    this->mRes = Param::mN1520;
    this->mRes_2 = this->mRes*this->mRes;
    this->ff_Res = new Form_Factors_N1520();
    this->gamma = Param::Gamma_N1520;

    this->parity = "negative";
    this->isospin_by_2 = 1;

    this->set_EM_FF();
}

Hadronic_Current_R_N1520::~Hadronic_Current_R_N1520() {
    delete this->ff_Res;

}
