//
// Created by eduardo on 8/07/19.
//

#ifndef COHERENT_PHOTON_Z_HADRONIC_CURRENT_R_D1232_H
#define COHERENT_PHOTON_Z_HADRONIC_CURRENT_R_D1232_H


#include "Hadronic_Current_tr_J32.h"

class z_Hadronic_Current_R_D1232 : public Hadronic_Current_tr_J32 {

public:

    explicit z_Hadronic_Current_R_D1232(Nuclear_FF *nuclearFf);

    virtual ~z_Hadronic_Current_R_D1232();
};


#endif //COHERENT_PHOTON_Z_HADRONIC_CURRENT_R_D1232_H
