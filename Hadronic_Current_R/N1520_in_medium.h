//
// Created by edusaul on 28/04/19.
//

#ifndef COHERENT_PHOTON_N1520_IN_MEDIUM_H
#define COHERENT_PHOTON_N1520_IN_MEDIUM_H

#include <Integrable_w_change_param.h>
#include <complex>
#include <density_profile.h>

class N1520_in_medium : public Integrable_w_change_param<std::complex<double>>{
protected:
    std::string nucleus;
    std::string nucleon;
    std::string dir_or_crs;
    double p;
    double p2;
    double q_minus_kg;
    double qcm;
    double mN1520;
    double mN1520_2;
    double mn;
    double mn2;
    double mpi;
    double V0;
    double rho0;
    double coupling_N1520_Npi;
    std::complex<double>c_i = {0,1};
    double hc3;

    double gamma_vac;
    double rho_r;

//    density_profile *rho;

    std::complex<double> propagator_crs;
    double rho_avg; // average density in units of rho0
    std::complex<double> propagator_avg;

public:

    N1520_in_medium();

    virtual ~N1520_in_medium();

    void setNucleon(const std::string &nucleon);

    std::complex<double> integrand (double r) override;

    std::complex<double> N1520_propagator_avg_dir();

    virtual std::complex<double> N1520_propagator_crs();

    void change_other_parameters(std::vector<std::complex<double>>) override;

    std::complex<double> Sigma();

    double Gamma_tilde_N1520(double);

    double Gamma_vacuum(double);

    double I_series(double);

    double lambda_func(double, double, double);

    double kf(double);

//    int getA() const;
//
//    double getRmax() const;

//    double getRho_r(double);

    void setDirOrCrs(const std::string &dirOrCrs);
};


#endif //COHERENT_PHOTON_N1520_IN_MEDIUM_H
