//
// Created by edusaul on 11/03/19.
//

#ifndef COHERENT_PHOTON_HADRONIC_CURRENT_R_H
#define COHERENT_PHOTON_HADRONIC_CURRENT_R_H

#include <vector>
#include <complex>
#include "Integrable_w_change_param.h"

class Hadronic_Current_R {
protected:
    std::complex<double> c_i;
    double mn;
    double mn2;
    double mpi;
    double mpi2;

    std::vector<double> q;
    std::vector<double> kg;
    std::vector<double> p;
    std::vector<double> pp;
    double p0;

    typedef std::complex<double> Array4x4[4][4];

    Array4x4 tr_p_dir;
    Array4x4 tr_p_crs;
    Array4x4 tr_n_dir;
    Array4x4 tr_n_crs;

    virtual void set_tr_dir(Array4x4&);
    virtual void set_tr_crs(Array4x4&);

public:

    explicit Hadronic_Current_R();

    virtual void setQ(const std::vector<double> &q);

    virtual void setKg(const std::vector<double> &kg);

    virtual void setP(std::vector<double>);

    virtual std::complex<double> getR(int, int);

    virtual void setFF(double);

    double q_cm(double);
};


#endif //COHERENT_PHOTON_HADRONIC_CURRENT_R_H
