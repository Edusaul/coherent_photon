//
// Created by edusaul on 26/03/19.
//

#ifndef COHERENT_PHOTON_HADRONIC_CURRENT_R_DELTA_WITH_INT_H
#define COHERENT_PHOTON_HADRONIC_CURRENT_R_DELTA_WITH_INT_H


#include "Hadronic_Current_R_Delta.h"
#include <complex>
#include "Form_Factors_Delta.h"
#include "Delta_in_medium.h"

class Hadronic_Current_R_Delta_with_Int : public Hadronic_Current_R_Delta{
protected:
    double npts;
    double rmin;
    double rmax;

    std::vector<double> p_dir;
    std::vector<double> p_crs;

    Delta_in_medium *integral_in_r;

public:
    Hadronic_Current_R_Delta_with_Int(double rmax, Delta_in_medium *integralInR);

    Hadronic_Current_R_Delta_with_Int(double npts, double rmax, Delta_in_medium *integralInR);

    std::complex<double> getR(int, int) override;

    void setKg(const std::vector<double> &kg) override;

    void setP(std::vector<double>) override;

    std::complex<double> Propagator_Delta(std::vector<double>) override;


};


#endif //COHERENT_PHOTON_HADRONIC_CURRENT_R_DELTA_WITH_INT_H
