//
// Created by eduardo on 5/07/19.
//

#ifndef COHERENT_PHOTON_HADRONIC_CURRENT_R_N1720_H
#define COHERENT_PHOTON_HADRONIC_CURRENT_R_N1720_H


#include "Hadronic_Current_tr_J32.h"

class Hadronic_Current_R_N1720 : public Hadronic_Current_tr_J32 {

public:

    explicit Hadronic_Current_R_N1720(Nuclear_FF *nuclearFf);

    virtual ~Hadronic_Current_R_N1720();

};


#endif //COHERENT_PHOTON_HADRONIC_CURRENT_R_N1720_H
