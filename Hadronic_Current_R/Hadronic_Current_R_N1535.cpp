//
// Created by eduardo on 14/06/19.
//

#include <algorithm>
#include <Parameters_GeV.h>
#include "Hadronic_Current_R_N1535.h"

Hadronic_Current_R_N1535::Hadronic_Current_R_N1535(Nuclear_FF *nuclearFf) : Hadronic_Current_tr_J12(nuclearFf) {
    this->mRes = Param::mN1535;
    this->mRes_2 = this->mRes * this->mRes;
    this->ff_Res = new Form_Factors_N1535();
    this->gamma = Param::Gamma_N1535;

    this->parity = "negative";
    this->isospin_by_2 = 1;

    this->set_EM_FF();
}

Hadronic_Current_R_N1535::~Hadronic_Current_R_N1535() {
    delete this->ff_Res;
}
