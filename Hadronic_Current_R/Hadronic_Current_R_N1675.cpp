//
// Created by eduardo on 5/07/19.
//

#include <Form_Factors_N1675.h>
#include <Parameters_GeV.h>
#include "Hadronic_Current_R_N1675.h"

Hadronic_Current_R_N1675::Hadronic_Current_R_N1675(Nuclear_FF *nuclearFf) : Hadronic_Current_tr_J32(nuclearFf) {
    this->mRes = Param::mN1675;
    this->mRes_2 = this->mRes * this->mRes;
    this->ff_Res = new Form_Factors_N1675();
    this->gamma = Param::Gamma_N1675;

    this->parity = "negative";
    this->isospin_by_2 = 1;

    this->set_EM_FF();
}

Hadronic_Current_R_N1675::~Hadronic_Current_R_N1675() {
    delete this->ff_Res;
}
