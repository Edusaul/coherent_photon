//
// Created by edusaul on 11/04/19.
//

#ifndef COHERENT_PHOTON_HADRONIC_CURRENT_R_DELTA_AVG_H
#define COHERENT_PHOTON_HADRONIC_CURRENT_R_DELTA_AVG_H

#include "Hadronic_Current_R_Delta.h"
#include "Delta_in_medium_with_Int_avg.h"
#include <complex>

class Hadronic_Current_R_Delta_avg : public Hadronic_Current_R_Delta {
protected:
    std::vector<double> p_dir;
    std::vector<double> p_crs;

    Delta_in_medium_with_Int_avg *propagator;
    std::string dir_or_crs;

    Nuclear_FF *nuclearFF;

public:
    explicit Hadronic_Current_R_Delta_avg(Nuclear_FF *nuclearFf);

    ~Hadronic_Current_R_Delta_avg() override;

    std::complex<double> getR(int, int) override;

    void setKg(const std::vector<double> &kg) override;

    void setP(std::vector<double>) override;

    std::complex<double> Propagator_Delta(std::vector<double>) override;

};


#endif //COHERENT_PHOTON_HADRONIC_CURRENT_R_DELTA_AVG_H
