//
// Created by edusaul on 8/04/19.
//

#include <Parameters_GeV.h>
#include <algorithm>
#include "Hadronic_Current_R_Delta_approx.h"

Hadronic_Current_R_Delta_approx::Hadronic_Current_R_Delta_approx(Nuclear_FF *nuclearFf) : nuclearFF(nuclearFf) {

}

std::complex<double> Hadronic_Current_R_Delta_approx::getR(int i, int j) {
    std::complex<double> tr_p = this->tr_p_dir[i][j]*this->propagator_dir_p
                                + this->tr_p_crs[i][j]*this->propagator_crs_p;
    std::complex<double> tr_n = this->tr_n_dir[i][j]*this->propagator_dir_n
                                + this->tr_n_crs[i][j]*this->propagator_crs_n;

    return (tr_p * this->N_FF_p + tr_n * this->N_FF_n) * this->mn / this->p0 / 2.0; //Factor 1/2 from the trace, see notes
//    return (tr_p + tr_n) * this->N_FF * this->mn / this->p0 ;
}

void Hadronic_Current_R_Delta_approx::setKg(const std::vector<double> &kg) {
    Hadronic_Current_R_Delta::setKg(kg);
    this->nuclearFF->setFF(this->q_minus_kg);
    this->N_FF_p = this->nuclearFF->getFfP();
    this->N_FF_n = this->nuclearFF->getFfN();
}

std::complex<double> Hadronic_Current_R_Delta_approx::Propagator_Delta(std::vector<double> p) {
    double p2 =  p[0] * p[0] - p[1] * p[1] - p[2] * p[2] - p[3] * p[3] ;

    double real_sigma = 0.0; // real part of the Delta selfenergy
    double fr = 0.47; // average density in units of rho0
    double gamspr0=Param::Delta_V0; // in meadium spreading at rho0
    double gamspr=gamspr0*fr;

    std::complex<double> res;
    if(p2<this->mn2) {
        res = 1.0/(p2 - this->mDelta2);
    } else {
        res = 1.0/(p2 - (this->mDelta + real_sigma)*(this->mDelta + real_sigma) +
                   this->c_i * (this->mDelta + real_sigma)*(this->gamma_Delta_to_pi(p2) + gamspr));
    }

    return res;
}

double Hadronic_Current_R_Delta_approx::gamma_Delta_to_pi(double s) {
    if(s <= (this->mn + this->mpi)*(this->mn + this->mpi)){
        return 0;
    } else {
        double res = 1.0/(6.0*Param::pi)*(Param::coupling_DeltaNpi/this->mpi)*(Param::coupling_DeltaNpi/this->mpi)*
                     this->mn*pow(qcm(s),3)/sqrt(s);

        return res;
    }

}

void Hadronic_Current_R_Delta_approx::setP(std::vector<double> p) {
    Hadronic_Current_R::setP(p);

    std::vector<double> p_dir = this->p;
    std::transform(p_dir.begin( ), p_dir.end( ), this->q.begin( ), p_dir.begin( ),std::plus<>( ));

    this->propagator_dir_p = Propagator_Delta(p_dir);
    this->propagator_dir_n = this->propagator_dir_p;

    std::vector<double> p_crs = this->pp;
    std::transform(p_crs.begin( ), p_crs.end( ), this->q.begin( ), p_crs.begin( ),std::minus<>( ));
    this->propagator_crs_p = Propagator_Delta(p_crs);
    this->propagator_crs_n = this->propagator_crs_p;
}


