//
// Created by edusaul on 29/03/19.
//

#include <Parameters_GeV.h>
#include <iostream>
#include "Delta_in_medium.h"

Delta_in_medium::Delta_in_medium(std::string nucleus, std::string nucleon)
        : nucleus(std::move(nucleus)), nucleon(std::move(nucleon)) {
    this->mDelta = Param::mDelta;
    this->mDelta2 = Param::mDelta*Param::mDelta;
    this->mn = Param::mp;
    this->mn2 = Param::mp2;
    this->mpi = Param::mpi;
    this->V0 = Param::Delta_V0;
    this->coupling_DeltaNpi = Param::coupling_DeltaNpi;
    this->c_i = {0,1};
    this->hc3 = Param::hc * Param::hc * Param::hc;
    this->dir_or_crs = "dir";

    this->rho = new density_profile(this->nucleus);
//    this->rho0 = (this->rho->get_rhop_cent(0) + this->rho->get_rhon_cent(0)) * this->hc3;
    this->rho0 = Param::rho0;
}

Delta_in_medium::~Delta_in_medium() {
    delete this->rho;
}

std::complex<double> Delta_in_medium::integrand(double r) {
    double rho_r_N;
    double rho_r_p = this->rho->get_rhop_cent(r) * this->hc3;
    double rho_r_n = this->rho->get_rhon_cent(r) * this->hc3;

    this->rho_r = rho_r_p + rho_r_n;

    if(nucleon == "p") {
        rho_r_N = rho_r_p;
    } else if(nucleon == "n") rho_r_N = rho_r_n;

    std::complex<double> propagator;
    if(this->dir_or_crs == "crs") propagator = this->propagator_crs;
    else propagator = this->Delta_propagator_dir(r);

//    std::cout<<propagator<<"  "<<this->q_minus_kg<<"  "<<rho_r<<std::endl;
//    std::cout<<"   fffff    "<<r<<"  "<<this->q_minus_kg<<"  "<<r/Param::hc<<std::endl;
//    std::cout<<r/Param::hc <<"  "<< sin(this->q_minus_kg * r/Param::hc) <<"  "<< this->rho_r <<"  "<< propagator<<std::endl;
//    std::cout<<"            "<<r/Param::hc <<"  "<< sin(this->q_minus_kg * r/Param::hc) <<"  "<< rho_r_N <<"  "<< propagator<<std::endl;
//    std::cout<<"   "<<this->p2<<"   "<<this->q_minus_kg<<"    "<<r <<"  "<< propagator.real()<<"  "<< propagator.imag()<<std::endl;

    return (r/Param::hc * sin(this->q_minus_kg * r/Param::hc) * rho_r_N * propagator)/Param::hc; // the extra /Param::hc comes from the differential units
}

std::complex<double> Delta_in_medium::Delta_propagator_dir(double r) {

    std::complex<double> sigma = this->Sigma();

    std::complex<double> D_Delta_med = 1.0/(this->p2 - (this->mDelta + sigma.real() )*(this->mDelta + sigma.real() )
                                            + this->c_i * (this->mDelta + sigma.real()) * (Gamma_tilde_Delta(this->rho_r) - 2.0 * sigma.imag() ) );

//    std::cout<<r<<" ..... "<<sigma<<"    "<<Gamma_tilde_Delta(this->rho_r) - 2.0 * sigma.imag()<<"  "<<D_Delta_med<<std::endl;

    return D_Delta_med;
}

std::complex<double> Delta_in_medium::Delta_propagator_crs() {
    return 1.0/(this->p2 - this->mDelta2 + this->c_i * this->mDelta * this->Gamma_vacuum(this->p2));
}

void Delta_in_medium::change_other_parameters(std::vector<std::complex<double>> param) {
    this->p2 = param[0].real();
    this->q_minus_kg = param[1].real();

//    std::cout<<"                "<<p2<<"  "<<q_minus_kg<<std::endl;

    this->p = sqrt(this->p2);
    this->qcm = sqrt(this->lambda_func(p2, this->mpi*this->mpi, this->mn*this->mn))/(2.0 * this->p);
    this->gamma_vac = this->Gamma_vacuum(this->p2);

    this->propagator_crs = this->Delta_propagator_crs();
}

std::complex<double> Delta_in_medium::Sigma() {
    double reSigma = 0.0;

    double imSigma = - 1.0/2.0 * this->V0 * this->rho_r/this->rho0;

    std::complex<double> Sigma(reSigma, imSigma);
    return Sigma;
}

double Delta_in_medium::Gamma_tilde_Delta(double rho_r) {

    double q_tilde = this->qcm/this->kf(rho_r);

//    std::cout<<q_tilde<<"  "<<this->qcm<<"  "<<this->kf(rho_r)<<std::endl;
//    std::cout<<"                "<<this->gamma_vac<<"  "<<this->I_series(q_tilde)<<std::endl;

    return this->gamma_vac * this->I_series(q_tilde);
}

double Delta_in_medium::Gamma_vacuum(double p2) {
    if(p2 > (this->mn + this->mpi)*(this->mn + this->mpi)) {

        return 1.0/(6.0*Param::pi) * (this->coupling_DeltaNpi/this->mpi)*(this->coupling_DeltaNpi/this->mpi)
        *this->mn/sqrt(p2) * this->qcm*qcm*qcm;
    }else return 0;
}

double Delta_in_medium::I_series(double q) {
    double res = 1.0;

    if (q != 0) {
//        if (q > 1.0) res += -3.0 / (5.0 * q * q) + 18.0 / (35.0 * pow(q, 4)) - 4.0 / (21.0 * pow(q, 6));
        if (q > 1.0) res += -2.0 / (5.0 * q * q) + 9.0 / (35.0 * pow(q, 4)) - 2.0 / (21.0 * pow(q, 6));
        else if (q < 1.0) res += 34.0 / 35.0 * q - 22.0 / 105.0 * q * q * q - 1.0;
//        else if (q < 1.0) res += 33.0 / 35.0 * q - 23.0 / 105.0 * q * q * q - 1.0;
    }

//    if (q == 0) {
//        std::cout << "   iiiiiiiiiiii    " << q << "  " << res << std::endl;
//        std::cout << "   sssssssss    " << "  "
//                  << -2.0 / (5.0 * q * q) + 9.0 / (35.0 * pow(q, 4)) - 2.0 / (21.0 * pow(q, 6)) << std::endl;
//        std::cout << "   dddddddddd    " << "  " << 34.0 / 35.0 * q - 22.0 / 105.0 * q * q * q - 1.0 << std::endl;
//    }

    return res;
}

void Delta_in_medium::setNucleon(const std::string &nucleon) {
    Delta_in_medium::nucleon = nucleon;
}

double Delta_in_medium::lambda_func(double x, double y, double z) {
    return x*x + y*y + z*z - 2.0*x*y - 2.0*y*z - 2.0*x*z;
}

double Delta_in_medium::kf(double rho_r) {
//    std::cout<<rho_r<<"  "<< pow(3.0 * Param::pi*Param::pi * rho_r/2.0, 1.0/3.0) <<std::endl;
    return pow(3.0 * Param::pi*Param::pi * rho_r/2.0, 1.0/3.0);
}

int Delta_in_medium::getA() const {
    return this->rho->getA();
}

double Delta_in_medium::getRmax() const {
    return this->rho->getRmax();
}

double Delta_in_medium::getRho_r(double r) {
    if(nucleon == "p") {
        this->rho_r = this->rho->get_rhop_cent(r);
    } else if(nucleon == "n") this->rho_r = this->rho->get_rhon_cent(r);
    return this->rho_r;
}

void Delta_in_medium::setDirOrCrs(const std::string &dirOrCrs) {
    this->dir_or_crs = dirOrCrs;
}

















