//
// Created by eduardo on 5/07/19.
//

#include <Parameters_GeV.h>
#include <Form_Factors_D1950.h>
#include "Hadronic_Current_R_D1950.h"

Hadronic_Current_R_D1950::Hadronic_Current_R_D1950(Nuclear_FF *nuclearFf) : Hadronic_Current_tr_J32(nuclearFf) {
    this->mRes = Param::mD1950;
    this->mRes_2 = this->mRes * this->mRes;
    this->ff_Res = new Form_Factors_D1950();
    this->gamma = Param::Gamma_D1950;

    this->parity = "positive";
    this->isospin_by_2 = 3;

    this->set_EM_FF();
}

Hadronic_Current_R_D1950::~Hadronic_Current_R_D1950() {
    delete this->ff_Res;
}