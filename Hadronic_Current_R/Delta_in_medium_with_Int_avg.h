//
// Created by edusaul on 8/04/19.
//

#ifndef COHERENT_PHOTON_DELTA_IN_MEDIUM_WITH_INT_AVG_H
#define COHERENT_PHOTON_DELTA_IN_MEDIUM_WITH_INT_AVG_H


#include "Delta_in_medium.h"

class Delta_in_medium_with_Int_avg : public Delta_in_medium{
protected:
    double rho_avg; // average density in units of rho0
    std::complex<double> propagator_avg;

public:
    Delta_in_medium_with_Int_avg(const std::string &nucleus, const std::string &nucleon);

    std::complex<double> integrand (double) override;

    std::complex<double> Delta_propagator_avg_dir();

    void change_other_parameters(std::vector<std::complex<double>>) override;

};


#endif //COHERENT_PHOTON_DELTA_IN_MEDIUM_WITH_INT_AVG_H
