//
// Created by edusaul on 11/03/19.
//

#include <Parameters_GeV.h>
#include <iostream>
#include <algorithm>
#include "Hadronic_Current_R_Nucleon.h"

Hadronic_Current_R_Nucleon::Hadronic_Current_R_Nucleon(Nuclear_FF *nuclearFf) : nuclearFF(nuclearFf) {
    this->mn = Param::mp;
    this->mn2 = Param::mp2;
    this->ff_nucleon = new Form_Factors_Nucleon_EM();

    this->ff_nucleon->setFF(0.0);
    this->F1p = this->ff_nucleon->getF1p();
    this->F2p = this->ff_nucleon->getF2p();
    this->F1n = this->ff_nucleon->getF1n();
    this->F2n = this->ff_nucleon->getF2n();
}

Hadronic_Current_R_Nucleon::~Hadronic_Current_R_Nucleon() {
    delete this->ff_nucleon;
}

std::complex<double> Hadronic_Current_R_Nucleon::getR(int i, int j) {
    std::complex<double> tr_p = this->tr_p_dir[i][j]*this->propagator_dir
            + this->tr_p_crs[i][j]*this->propagator_crs;
    std::complex<double> tr_n = this->tr_n_dir[i][j]*this->propagator_dir
            + this->tr_n_crs[i][j]*this->propagator_crs;

    return (tr_p * this->N_FF_p + tr_n * this->N_FF_n) * this->mn / this->p0 / 2.0; //Factor 1/2 from the trace, see notes
//    return (tr_p + tr_n) * this->N_FF * this->mn / this->p0; //Factor 1/2 from the trace, see notes
}

void Hadronic_Current_R_Nucleon::setFF(double q2) {
    double Q2 = -q2;
    this->ff_nucleon->setFF(Q2);
}

void Hadronic_Current_R_Nucleon::set_tr_dir(Array4x4 &tr) {

    tr[0][0] = (4*F1*F1NC*this->mn2*(2*p0 + this->q[0]) - F1NC*F2*
                                                          (this->kg[1] * this->kg[1]*(2*p0 + this->q[0]) + this->kg[2] * this->kg[2]*(2*p0 + this->q[0]) - this->kg[1]*this->q[0]*this->q[1] +
                                                           this->kg[3]*(2*this->kg[3]*p0 + this->kg[3]*this->q[0] - this->q[0]*this->q[3])) +
                F2NC*(F2*this->q[0]*(this->kg[1]*this->q[1] + this->kg[3]*this->q[3]) + F1*
                                                                                        (this->kg[1]*this->q[0]*this->q[1] - 2*p0*(this->q[1] * this->q[1] + this->q[3] * this->q[3]) -
                                                                                         this->q[0]*(this->q[1] * this->q[1] - this->kg[3]*this->q[3] + this->q[3] * this->q[3]))))/(2.*this->mn2);

    tr[0][1] = (8*F1*F1NC*this->kg[1]*this->mn2 - F1NC*F2*(pow(this->kg[1],3) - 2*this->kg[3]*this->q[1]*this->q[3] +
                                                           this->kg[1]*(this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 4*this->mn2 + 4*pow(p0,2) + 4*p0*this->q[0] -
                                                                        this->q[1] * this->q[1] + this->q[3] * this->q[3])) +
                2.0*(this->c_i*F2*FANC*this->kg[2]*(2*p0 + this->q[0])*this->q[3] -
                   F1*F2NC*this->q[1]*(2*p0*this->q[0] + this->q[0] * this->q[0] - this->kg[3]*this->q[3]) +
                   F1*F2NC*this->kg[1]*(this->q[0] * this->q[0] - this->q[3] * this->q[3]) +
                   F2*F2NC*(this->kg[3]*this->q[1]*this->q[3] + this->kg[1]*(this->q[0] * this->q[0] - this->q[3] * this->q[3]))))/(4.*this->mn2);

    tr[0][2] = (8*F1*F1NC*this->kg[2]*this->mn2 + std::complex<double>(0,2)*F2*FANC*(2*p0 + this->q[0])*(this->kg[3]*this->q[1] - this->kg[1]*this->q[3]) +
                2*F1*F2NC*this->kg[2]*(this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3]) +
                2*F2*F2NC*this->kg[2]*(this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3]) -
                F1NC*F2*this->kg[2]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 4*this->mn2 +
                                     4*pow(p0,2) + 4*p0*this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3]))/(4.*this->mn2);

    tr[0][3] = (8*F1*F1NC*this->kg[3]*this->mn2 + 2.0*(std::complex<double>(0,-1)*F2*FANC*this->kg[2]*(2*p0 + this->q[0])*this->q[1] +
                                                     F1*F2NC*this->kg[3]*(this->q[0] * this->q[0] - this->q[1] * this->q[1]) -
                                                     F1*F2NC*(2*p0*this->q[0] + this->q[0] * this->q[0] - this->kg[1]*this->q[1])*this->q[3] +
                                                     F2*F2NC*(this->kg[3]*(this->q[0] * this->q[0] - this->q[1] * this->q[1]) + this->kg[1]*this->q[1]*this->q[3])) -
                F1NC*F2*(this->kg[1] * this->kg[1]*this->kg[3] - 2*this->kg[1]*this->q[1]*this->q[3] +
                         this->kg[3]*(this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 4*this->mn2 + 4*pow(p0,2) + 4*p0*this->q[0] +
                                      this->q[1] * this->q[1] - this->q[3] * this->q[3])))/(4.*this->mn2);


    tr[1][0] = (std::complex<double>(0,4)*F1*FPNC*this->kg[2]*this->mn*this->q[0]*this->q[3] + std::complex<double>(0,4)*F2*FPNC*this->kg[2]*this->mn*this->q[0]*this->q[3] +
                2*F2*F2NC*(-(this->kg[2] * this->kg[2]*this->q[1]) - this->kg[3] * this->kg[3]*this->q[1] + this->q[0] * this->q[0]*this->q[1] + this->kg[1]*this->kg[3]*this->q[3]) -
                2*F1NC*(-4*F1*this->mn2*this->q[1] + F2*(this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - this->q[0] * this->q[0])*this->q[1] +
                        F2*this->kg[1]*(2*p0*this->q[0] + this->q[0] * this->q[0] - this->kg[3]*this->q[3])) +
                F1*F2NC*(this->kg[1] * this->kg[1]*this->q[1] + 2*this->kg[1]*this->kg[3]*this->q[3] -
                         this->q[1]*(this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 4*this->mn2 + 4*pow(p0,2) + 4*p0*this->q[0] +
                                     this->q[1] * this->q[1] + this->q[3] * this->q[3])))/(4.*this->mn2);

    tr[1][1] = (8*F1*F1NC*this->mn2*this->q[0] + std::complex<double>(0,4)*F1*FPNC*this->kg[2]*this->mn*this->q[1]*this->q[3] +
                std::complex<double>(0,2)*F2*this->kg[2]*(FANC + 2*FPNC*this->mn)*this->q[1]*this->q[3] +
                F1*F2NC*(this->kg[1] * this->kg[1]*this->q[0] - this->kg[2] * this->kg[2]*this->q[0] - this->kg[3] * this->kg[3]*this->q[0] + 4*this->mn2*this->q[0] -
                         4*pow(p0,2)*this->q[0] - 4*p0*this->q[0] * this->q[0] - this->q[0]*this->q[1] * this->q[1] + 4*this->kg[3]*p0*this->q[3] + 2*this->kg[3]*this->q[0]*this->q[3] -
                         this->q[0]*this->q[3] * this->q[3]) + 2*F2*F2NC*(-(this->kg[2] * this->kg[2]*this->q[0]) - this->kg[3] * this->kg[3]*this->q[0] + pow(this->q[0],3) +
                                                                          this->kg[3]*(2*p0 + this->q[0])*this->q[3] - this->q[0]*this->q[3] * this->q[3]) -
                F1NC*F2*(this->kg[1] * this->kg[1]*this->q[0] + this->kg[2] * this->kg[2]*this->q[0] + this->kg[3] * this->kg[3]*this->q[0] - 4*this->mn2*this->q[0] +
                         4*pow(p0,2)*this->q[0] + 4*p0*this->q[0] * this->q[0] - this->q[0]*this->q[1] * this->q[1] - 4*this->kg[3]*p0*this->q[3] - 2*this->kg[3]*this->q[0]*this->q[3] +
                         this->q[0]*this->q[3] * this->q[3]))/(4.*this->mn2);

    tr[1][2] = (std::complex<double>(0,8)*F1*FANC*this->kg[3]*this->mn2 + 2*F1*F2NC*this->kg[1]*this->kg[2]*this->q[0] - 2*F1*F2NC*this->kg[2]*(2*p0 + this->q[0])*this->q[1] -
                2*F2*this->kg[2]*(-(F2NC*this->kg[1]*this->q[0]) + F1NC*(2*p0 + this->q[0])*this->q[1] + F2NC*(2*p0 + this->q[0])*this->q[1]) -
                this->c_i*F2*FANC*(this->kg[1] * this->kg[1]*this->kg[3] + this->kg[2] * this->kg[2]*this->kg[3] + pow(this->kg[3],3) -
                                   4*this->kg[3]*this->mn2 - 4*this->kg[3]*pow(p0,2) - 4*this->kg[3]*p0*this->q[0] - 2*this->kg[3]*this->q[0] * this->q[0] -
                                   this->kg[3]*this->q[1] * this->q[1] + 4*p0*this->q[0]*this->q[3] + 2*this->q[0] * this->q[0]*this->q[3] - this->kg[3]*this->q[3] * this->q[3]))/(4.*this->mn2);

    tr[1][3] = (std::complex<double>(0,0.25)*(F2*FANC*this->kg[2]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 4*this->mn2 -
                                                                   4*pow(p0,2) - 4*p0*this->q[0] - 2*this->q[0] * this->q[0] - this->q[1] * this->q[1] + this->q[3] * this->q[3]) +
                                              2*F1*(-4*FANC*this->kg[2]*this->mn2 - this->c_i*F2NC*this->kg[3]*(this->kg[1]*this->q[0] - (2*p0 + this->q[0])*this->q[1]) +
                                                    2*FPNC*this->kg[2]*this->mn*this->q[3] * this->q[3]) +
                                              std::complex<double>(0,2)*F2*(-(F2NC*this->kg[1]*this->kg[3]*this->q[0]) - std::complex<double>(0,2)*FPNC*this->kg[2]*this->mn*this->q[3] * this->q[3] +
                                                                            F1NC*this->q[1]*(this->kg[3]*(2*p0 + this->q[0]) - this->q[0]*this->q[3]) + F2NC*this->q[1]*(this->kg[3]*(2*p0 + this->q[0]) - this->q[0]*this->q[3]))))/this->mn2;


    tr[2][0] = (F1NC*F2*this->kg[2]*(-2*p0*this->q[0] - this->q[0] * this->q[0] + this->kg[1]*this->q[1] + this->kg[3]*this->q[3]) +
                (F1 + F2)*(std::complex<double>(0,2)*FPNC*this->mn*this->q[0]*(this->kg[3]*this->q[1] - this->kg[1]*this->q[3]) + F2NC*this->kg[2]*(this->kg[1]*this->q[1] + this->kg[3]*this->q[3])))/
               (2.*this->mn2);

    tr[2][1] = (std::complex<double>(0,0.25)*(-2*F1*(4*FANC*this->kg[3]*this->mn2 + this->c_i*F2NC*this->kg[1]*this->kg[2]*this->q[0] +
                                                     2*FPNC*this->mn*this->q[1]*(-(this->kg[3]*this->q[1]) + this->kg[1]*this->q[3])) +
                                              F2*(std::complex<double>(0,-2)*F2NC*this->kg[1]*this->kg[2]*this->q[0] + 4*FPNC*this->mn*this->q[1]*(this->kg[3]*this->q[1] - this->kg[1]*this->q[3]) +
                                                  FANC*(this->kg[1] * this->kg[1]*this->kg[3] + this->kg[2] * this->kg[2]*this->kg[3] + pow(this->kg[3],3) - 4*this->kg[3]*this->mn2 -
                                                        4*this->kg[3]*pow(p0,2) - 4*this->kg[3]*p0*this->q[0] - 2*this->kg[3]*this->q[0] * this->q[0] + this->kg[3]*this->q[1] * this->q[1] +
                                                        4*p0*this->q[0]*this->q[3] + 2*this->q[0] * this->q[0]*this->q[3] - 2*this->kg[1]*this->q[1]*this->q[3] - this->kg[3]*this->q[3] * this->q[3]))))/this->mn2;

    tr[2][2] = -(-8*F1*F1NC*this->mn2*this->q[0] + F1NC*F2*
                                                   (this->kg[1] * this->kg[1]*this->q[0] + this->kg[2] * this->kg[2]*this->q[0] + this->kg[3] * this->kg[3]*this->q[0] - 4*this->mn2*this->q[0] +
                                                    4*pow(p0,2)*this->q[0] + 4*p0*this->q[0] * this->q[0] - 2*this->kg[1]*(2*p0 + this->q[0])*this->q[1] + this->q[0]*this->q[1] * this->q[1] -
                                                    4*this->kg[3]*p0*this->q[3] - 2*this->kg[3]*this->q[0]*this->q[3] + this->q[0]*this->q[3] * this->q[3]) +
                 F2NC*(F1*(this->kg[1] * this->kg[1]*this->q[0] - this->kg[2] * this->kg[2]*this->q[0] + this->kg[3] * this->kg[3]*this->q[0] - 4*this->mn2*this->q[0] +
                           4*pow(p0,2)*this->q[0] + 4*p0*this->q[0] * this->q[0] - 2*this->kg[1]*(2*p0 + this->q[0])*this->q[1] + this->q[0]*this->q[1] * this->q[1] -
                           4*this->kg[3]*p0*this->q[3] - 2*this->kg[3]*this->q[0]*this->q[3] + this->q[0]*this->q[3] * this->q[3]) +
                       2*F2*(this->kg[1] * this->kg[1]*this->q[0] + this->kg[3] * this->kg[3]*this->q[0] - this->kg[1]*(2*p0 + this->q[0])*this->q[1] - this->kg[3]*(2*p0 + this->q[0])*this->q[3] +
                             this->q[0]*(-this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3]))))/(4.*this->mn2);

    tr[2][3] = (std::complex<double>(0,-0.25)*(-8*F1*FANC*this->kg[1]*this->mn2 + std::complex<double>(0,2)*F1*F2NC*this->kg[2]*this->kg[3]*this->q[0] +
                                               std::complex<double>(0,2)*F2*F2NC*this->kg[2]*this->kg[3]*this->q[0] + 4*F1*FPNC*this->mn*this->q[3]*(-(this->kg[3]*this->q[1]) + this->kg[1]*this->q[3]) +
                                               4*F2*FPNC*this->mn*this->q[3]*(-(this->kg[3]*this->q[1]) + this->kg[1]*this->q[3]) +
                                               F2*FANC*(pow(this->kg[1],3) + 2*this->q[1]*(2*p0*this->q[0] + this->q[0] * this->q[0] - this->kg[3]*this->q[3]) +
                                                        this->kg[1]*(this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 4*this->mn2 - 4*pow(p0,2) - 4*p0*this->q[0] -
                                                                     2*this->q[0] * this->q[0] - this->q[1] * this->q[1] + this->q[3] * this->q[3]))))/this->mn2;


    tr[3][0] = -(std::complex<double>(0,4)*F1*FPNC*this->kg[2]*this->mn*this->q[0]*this->q[1] + std::complex<double>(0,4)*F2*FPNC*this->kg[2]*this->mn*this->q[0]*this->q[1] +
                 2*F2*F2NC*(-(this->kg[1]*this->kg[3]*this->q[1]) + this->kg[1] * this->kg[1]*this->q[3] + (this->kg[2] * this->kg[2] - this->q[0] * this->q[0])*this->q[3]) +
                 2*F1NC*(F2*this->kg[3]*(2*p0*this->q[0] + this->q[0] * this->q[0] - this->kg[1]*this->q[1]) - 4*F1*this->mn2*this->q[3] +
                         F2*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] - this->q[0] * this->q[0])*this->q[3]) +
                 F1*F2NC*(-2*this->kg[1]*this->kg[3]*this->q[1] + this->kg[1] * this->kg[1]*this->q[3] +
                          this->q[3]*(this->kg[2] * this->kg[2] - this->kg[3] * this->kg[3] - 4*this->mn2 + 4*pow(p0,2) + 4*p0*this->q[0] +
                                      this->q[1] * this->q[1] + this->q[3] * this->q[3])))/(4.*this->mn2);

    tr[3][1] = (std::complex<double>(0,8)*F1*FANC*this->kg[2]*this->mn2 - std::complex<double>(0,4)*F1*FPNC*this->kg[2]*this->mn*this->q[1] * this->q[1] +
                2*F1*F2NC*this->kg[1]*(this->kg[3]*this->q[0] - (2*p0 + this->q[0])*this->q[3]) -
                this->c_i*F2*FANC*this->kg[2]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 4*this->mn2 -
                                               4*pow(p0,2) - 4*p0*this->q[0] - 2*this->q[0] * this->q[0] + this->q[1] * this->q[1] - this->q[3] * this->q[3]) -
                2*F2*(std::complex<double>(0,2)*FPNC*this->kg[2]*this->mn*this->q[1] * this->q[1] - F2NC*this->q[0]*this->q[1]*this->q[3] +
                      F1NC*(this->kg[1]*(2*p0 + this->q[0]) - this->q[0]*this->q[1])*this->q[3] + F2NC*this->kg[1]*(-(this->kg[3]*this->q[0]) + (2*p0 + this->q[0])*this->q[3])))/
               (4.*this->mn2);

    tr[3][2] = (std::complex<double>(0,-8)*F1*FANC*this->kg[1]*this->mn2 + 2*F1*F2NC*this->kg[2]*this->kg[3]*this->q[0] -
                2*F1*F2NC*this->kg[2]*(2*p0 + this->q[0])*this->q[3] - 2*F2*this->kg[2]*
                                                                       (-(F2NC*this->kg[3]*this->q[0]) + F1NC*(2*p0 + this->q[0])*this->q[3] + F2NC*(2*p0 + this->q[0])*this->q[3]) +
                this->c_i*F2*FANC*(pow(this->kg[1],3) + 2*this->q[0]*(2*p0 + this->q[0])*this->q[1] +
                                   this->kg[1]*(this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 4*this->mn2 - 4*pow(p0,2) - 4*p0*this->q[0] -
                                                2*this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3])))/(4.*this->mn2);

    tr[3][3] = -(2*F2*F2NC*(this->kg[1] * this->kg[1]*this->q[0] - this->kg[1]*(2*p0 + this->q[0])*this->q[1] +
                            this->q[0]*(this->kg[2] * this->kg[2] - this->q[0] * this->q[0] + this->q[1] * this->q[1])) + std::complex<double>(0,4)*F1*FPNC*this->kg[2]*this->mn*this->q[1]*this->q[3] +
                 std::complex<double>(0,2)*F2*this->kg[2]*(FANC + 2*FPNC*this->mn)*this->q[1]*this->q[3] +
                 F1*F2NC*(this->kg[1] * this->kg[1]*this->q[0] - 2*this->kg[1]*(2*p0 + this->q[0])*this->q[1] +
                          this->q[0]*(this->kg[2] * this->kg[2] - this->kg[3] * this->kg[3] - 4*this->mn2 + 4*pow(p0,2) + 4*p0*this->q[0] +
                                      this->q[1] * this->q[1] + this->q[3] * this->q[3])) +
                 F1NC*(-8*F1*this->mn2*this->q[0] + F2*
                                                    (this->kg[1] * this->kg[1]*this->q[0] - 2*this->kg[1]*(2*p0 + this->q[0])*this->q[1] +
                                                     this->q[0]*(this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 4*this->mn2 + 4*pow(p0,2) + 4*p0*this->q[0] +
                                                                 this->q[1] * this->q[1] - this->q[3] * this->q[3]))))/(4.*this->mn2);

}

void Hadronic_Current_R_Nucleon::set_tr_crs(Array4x4 &tr) {

    tr[0][0] = (F1NC*(4*F1*this->mn2*(2*p0 - this->q[0]) +
                      F2*(this->kg[1] * this->kg[1]*(-2*p0 + this->q[0]) + this->kg[2] * this->kg[2]*(-2*p0 + this->q[0]) - this->kg[1]*this->q[0]*this->q[1] +
                          this->kg[3]*(-2*this->kg[3]*p0 + this->kg[3]*this->q[0] - this->q[0]*this->q[3]))) -
                F2NC*(F2*this->q[0]*(this->kg[1]*this->q[1] + this->kg[3]*this->q[3]) + F1*
                                                                                        (this->kg[1]*this->q[0]*this->q[1] + 2*p0*(this->q[1] * this->q[1] + this->q[3] * this->q[3]) -
                                                                                         this->q[0]*(this->q[1] * this->q[1] - this->kg[3]*this->q[3] + this->q[3] * this->q[3]))))/(2.*this->mn2);

    tr[0][1] = (-2.0*(this->c_i*F2*FANC*this->kg[2]*(2*p0 - this->q[0])*this->q[3] + F1*F2NC*this->q[1]*(2*p0*this->q[0] - this->q[0] * this->q[0] + this->kg[3]*this->q[3]) +
                    F1*F2NC*this->kg[1]*(this->q[0] * this->q[0] - this->q[3] * this->q[3]) +
                    F2*F2NC*(this->kg[3]*this->q[1]*this->q[3] + this->kg[1]*(this->q[0] * this->q[0] - this->q[3] * this->q[3]))) +
                F1NC*(-8*F1*this->kg[1]*this->mn2 + F2*
                                                    (pow(this->kg[1],3) - 2*this->kg[3]*this->q[1]*this->q[3] +
                                                     this->kg[1]*(this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 4*this->mn2 + 4*pow(p0,2) - 4*p0*this->q[0] -
                                                                  this->q[1] * this->q[1] + this->q[3] * this->q[3]))))/(4.*this->mn2);

    tr[0][2] = (-8*F1*F1NC*this->kg[2]*this->mn2 - std::complex<double>(0,2)*F2*FANC*(2*p0 - this->q[0])*(this->kg[3]*this->q[1] - this->kg[1]*this->q[3]) +
                F1NC*F2*this->kg[2]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 4*this->mn2 +
                                     4*pow(p0,2) - 4*p0*this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3]) +
                2*F1*F2NC*this->kg[2]*(-this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3]) +
                2*F2*F2NC*this->kg[2]*(-this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3]))/(4.*this->mn2);

    tr[0][3] = (-2.0*(std::complex<double>(0,-1)*F2*FANC*this->kg[2]*(2*p0 - this->q[0])*this->q[1] + F1*F2NC*this->kg[3]*(this->q[0] * this->q[0] - this->q[1] * this->q[1]) +
                    F1*F2NC*(2*p0*this->q[0] - this->q[0] * this->q[0] + this->kg[1]*this->q[1])*this->q[3] +
                    F2*F2NC*(this->kg[3]*(this->q[0] * this->q[0] - this->q[1] * this->q[1]) + this->kg[1]*this->q[1]*this->q[3])) +
                F1NC*(-8*F1*this->kg[3]*this->mn2 + F2*
                                                    (this->kg[1] * this->kg[1]*this->kg[3] - 2*this->kg[1]*this->q[1]*this->q[3] +
                                                     this->kg[3]*(this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 4*this->mn2 + 4*pow(p0,2) - 4*p0*this->q[0] +
                                                                  this->q[1] * this->q[1] - this->q[3] * this->q[3]))))/(4.*this->mn2);


    tr[1][0] = (std::complex<double>(0,4)*F1*FPNC*this->kg[2]*this->mn*this->q[0]*this->q[3] + std::complex<double>(0,4)*F2*FPNC*this->kg[2]*this->mn*this->q[0]*this->q[3] +
                2*F2*F2NC*(this->kg[2] * this->kg[2]*this->q[1] + this->kg[3] * this->kg[3]*this->q[1] - this->q[0] * this->q[0]*this->q[1] - this->kg[1]*this->kg[3]*this->q[3]) -
                2*F1NC*(4*F1*this->mn2*this->q[1] - F2*(this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - this->q[0] * this->q[0])*this->q[1] +
                        F2*this->kg[1]*(2*p0*this->q[0] - this->q[0] * this->q[0] + this->kg[3]*this->q[3])) +
                F1*F2NC*(-(this->kg[1] * this->kg[1]*this->q[1]) - 2*this->kg[1]*this->kg[3]*this->q[3] +
                         this->q[1]*(this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 4*this->mn2 + 4*pow(p0,2) - 4*p0*this->q[0] +
                                     this->q[1] * this->q[1] + this->q[3] * this->q[3])))/(4.*this->mn2);

    tr[1][1] = (-8*F1*F1NC*this->mn2*this->q[0] + std::complex<double>(0,4)*F1*FPNC*this->kg[2]*this->mn*this->q[1]*this->q[3] +
                std::complex<double>(0,2)*F2*this->kg[2]*(FANC + 2*FPNC*this->mn)*this->q[1]*this->q[3] +
                2*F2*F2NC*(this->kg[2] * this->kg[2]*this->q[0] + this->kg[3] * this->kg[3]*this->q[0] - pow(this->q[0],3) + this->kg[3]*(2*p0 - this->q[0])*this->q[3] +
                           this->q[0]*this->q[3] * this->q[3]) + F1NC*F2*(this->kg[1] * this->kg[1]*this->q[0] + this->kg[2] * this->kg[2]*this->q[0] + this->kg[3] * this->kg[3]*this->q[0] -
                                                                          4*this->mn2*this->q[0] + 4*pow(p0,2)*this->q[0] - 4*p0*this->q[0] * this->q[0] - this->q[0]*this->q[1] * this->q[1] +
                                                                          4*this->kg[3]*p0*this->q[3] - 2*this->kg[3]*this->q[0]*this->q[3] + this->q[0]*this->q[3] * this->q[3]) +
                F1*F2NC*(-(this->kg[1] * this->kg[1]*this->q[0]) + this->kg[2] * this->kg[2]*this->q[0] + this->kg[3] * this->kg[3]*this->q[0] - 4*this->mn2*this->q[0] +
                         4*pow(p0,2)*this->q[0] - 4*p0*this->q[0] * this->q[0] + this->q[0]*this->q[1] * this->q[1] + 4*this->kg[3]*p0*this->q[3] - 2*this->kg[3]*this->q[0]*this->q[3] +
                         this->q[0]*this->q[3] * this->q[3]))/(4.*this->mn2);

    tr[1][2] = (std::complex<double>(0,8)*F1*FANC*this->kg[3]*this->mn2 - 2*F1*F2NC*this->kg[2]*(this->kg[1]*this->q[0] + 2*p0*this->q[1] - this->q[0]*this->q[1]) +
                2*F2*this->kg[2]*(-(F2NC*this->kg[1]*this->q[0]) + F1NC*(-2*p0 + this->q[0])*this->q[1] + F2NC*(-2*p0 + this->q[0])*this->q[1]) -
                this->c_i*F2*FANC*(this->kg[1] * this->kg[1]*this->kg[3] + this->kg[2] * this->kg[2]*this->kg[3] + pow(this->kg[3],3) -
                                   4*this->kg[3]*this->mn2 - 4*this->kg[3]*pow(p0,2) + 4*this->kg[3]*p0*this->q[0] - 2*this->kg[3]*this->q[0] * this->q[0] -
                                   this->kg[3]*this->q[1] * this->q[1] - 4*p0*this->q[0]*this->q[3] + 2*this->q[0] * this->q[0]*this->q[3] - this->kg[3]*this->q[3] * this->q[3]))/(4.*this->mn2);

    tr[1][3] = (std::complex<double>(0,0.25)*(F2*FANC*this->kg[2]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 4*this->mn2 -
                                                                   4*pow(p0,2) + 4*p0*this->q[0] - 2*this->q[0] * this->q[0] - this->q[1] * this->q[1] + this->q[3] * this->q[3]) +
                                              2*F1*(-4*FANC*this->kg[2]*this->mn2 + this->c_i*F2NC*this->kg[3]*(this->kg[1]*this->q[0] + 2*p0*this->q[1] - this->q[0]*this->q[1]) +
                                                    2*FPNC*this->kg[2]*this->mn*this->q[3] * this->q[3]) +
                                              std::complex<double>(0,2)*F2*(F2NC*this->kg[1]*this->kg[3]*this->q[0] - std::complex<double>(0,2)*FPNC*this->kg[2]*this->mn*this->q[3] * this->q[3] +
                                                                            F1NC*this->q[1]*(2*this->kg[3]*p0 - this->kg[3]*this->q[0] + this->q[0]*this->q[3]) + F2NC*this->q[1]*(2*this->kg[3]*p0 - this->kg[3]*this->q[0] + this->q[0]*this->q[3]))))/
               this->mn2;


    tr[2][0] = -(F1NC*F2*this->kg[2]*(2*p0*this->q[0] - this->q[0] * this->q[0] + this->kg[1]*this->q[1] + this->kg[3]*this->q[3]) +
                 (F1 + F2)*(std::complex<double>(0,2)*FPNC*this->mn*this->q[0]*(-(this->kg[3]*this->q[1]) + this->kg[1]*this->q[3]) + F2NC*this->kg[2]*(this->kg[1]*this->q[1] + this->kg[3]*this->q[3])))/
               (2.*this->mn2);

    tr[2][1] = (std::complex<double>(0,0.25)*(-8*F1*FANC*this->kg[3]*this->mn2 + std::complex<double>(0,2)*F1*F2NC*this->kg[1]*this->kg[2]*this->q[0] +
                                              std::complex<double>(0,2)*F2*F2NC*this->kg[1]*this->kg[2]*this->q[0] + 4*F1*FPNC*this->mn*this->q[1]*(this->kg[3]*this->q[1] - this->kg[1]*this->q[3]) +
                                              4*F2*FPNC*this->mn*this->q[1]*(this->kg[3]*this->q[1] - this->kg[1]*this->q[3]) +
                                              F2*FANC*(this->kg[1] * this->kg[1]*this->kg[3] + this->kg[2] * this->kg[2]*this->kg[3] + pow(this->kg[3],3) - 4*this->kg[3]*this->mn2 -
                                                       4*this->kg[3]*pow(p0,2) + 4*this->kg[3]*p0*this->q[0] - 2*this->kg[3]*this->q[0] * this->q[0] + this->kg[3]*this->q[1] * this->q[1] -
                                                       4*p0*this->q[0]*this->q[3] + 2*this->q[0] * this->q[0]*this->q[3] - 2*this->kg[1]*this->q[1]*this->q[3] - this->kg[3]*this->q[3] * this->q[3])))/this->mn2;

    tr[2][2] = (-8*F1*F1NC*this->mn2*this->q[0] + F1*F2NC*(this->kg[1] * this->kg[1]*this->q[0] - this->kg[2] * this->kg[2]*this->q[0] + this->kg[3] * this->kg[3]*this->q[0] -
                                                           4*this->mn2*this->q[0] + 4*pow(p0,2)*this->q[0] - 4*p0*this->q[0] * this->q[0] + 4*this->kg[1]*p0*this->q[1] - 2*this->kg[1]*this->q[0]*this->q[1] +
                                                           this->q[0]*this->q[1] * this->q[1] + 4*this->kg[3]*p0*this->q[3] - 2*this->kg[3]*this->q[0]*this->q[3] + this->q[0]*this->q[3] * this->q[3]) +
                F1NC*F2*(this->kg[1] * this->kg[1]*this->q[0] + this->kg[2] * this->kg[2]*this->q[0] + this->kg[3] * this->kg[3]*this->q[0] - 4*this->mn2*this->q[0] +
                         4*pow(p0,2)*this->q[0] - 4*p0*this->q[0] * this->q[0] + 4*this->kg[1]*p0*this->q[1] - 2*this->kg[1]*this->q[0]*this->q[1] + this->q[0]*this->q[1] * this->q[1] +
                         4*this->kg[3]*p0*this->q[3] - 2*this->kg[3]*this->q[0]*this->q[3] + this->q[0]*this->q[3] * this->q[3]) +
                2*F2*F2NC*(this->kg[1] * this->kg[1]*this->q[0] + this->kg[3] * this->kg[3]*this->q[0] + this->kg[1]*(2*p0 - this->q[0])*this->q[1] + this->kg[3]*(2*p0 - this->q[0])*this->q[3] +
                           this->q[0]*(-this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3])))/(4.*this->mn2);

    tr[2][3] = (std::complex<double>(0,0.25)*(8*F1*FANC*this->kg[1]*this->mn2 + std::complex<double>(0,2)*F1*F2NC*this->kg[2]*this->kg[3]*this->q[0] +
                                              std::complex<double>(0,2)*F2*F2NC*this->kg[2]*this->kg[3]*this->q[0] + 4*F1*FPNC*this->mn*this->q[3]*(this->kg[3]*this->q[1] - this->kg[1]*this->q[3]) -
                                              4*F2*FPNC*this->mn*this->q[3]*(-(this->kg[3]*this->q[1]) + this->kg[1]*this->q[3]) -
                                              F2*FANC*(pow(this->kg[1],3) + 2*this->q[1]*(-2*p0*this->q[0] + this->q[0] * this->q[0] - this->kg[3]*this->q[3]) +
                                                       this->kg[1]*(this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 4*this->mn2 - 4*pow(p0,2) + 4*p0*this->q[0] -
                                                                    2*this->q[0] * this->q[0] - this->q[1] * this->q[1] + this->q[3] * this->q[3]))))/this->mn2;


    tr[3][0] = (std::complex<double>(0,-4)*F1*FPNC*this->kg[2]*this->mn*this->q[0]*this->q[1] - std::complex<double>(0,4)*F2*FPNC*this->kg[2]*this->mn*this->q[0]*this->q[1] +
                2*F2*F2NC*(-(this->kg[1]*this->kg[3]*this->q[1]) + this->kg[1] * this->kg[1]*this->q[3] + (this->kg[2] * this->kg[2] - this->q[0] * this->q[0])*this->q[3]) -
                2*F1NC*(F2*this->kg[3]*(2*p0*this->q[0] - this->q[0] * this->q[0] + this->kg[1]*this->q[1]) + 4*F1*this->mn2*this->q[3] -
                        F2*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] - this->q[0] * this->q[0])*this->q[3]) +
                F1*F2NC*(-2*this->kg[1]*this->kg[3]*this->q[1] + this->kg[1] * this->kg[1]*this->q[3] +
                         this->q[3]*(this->kg[2] * this->kg[2] - this->kg[3] * this->kg[3] - 4*this->mn2 + 4*pow(p0,2) - 4*p0*this->q[0] +
                                     this->q[1] * this->q[1] + this->q[3] * this->q[3])))/(4.*this->mn2);

    tr[3][1] = -(std::complex<double>(0,-8)*F1*FANC*this->kg[2]*this->mn2 + std::complex<double>(0,4)*F1*FPNC*this->kg[2]*this->mn*this->q[1] * this->q[1] +
                 2*F1*F2NC*this->kg[1]*(this->kg[3]*this->q[0] + 2*p0*this->q[3] - this->q[0]*this->q[3]) +
                 this->c_i*F2*FANC*this->kg[2]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 4*this->mn2 -
                                                4*pow(p0,2) + 4*p0*this->q[0] - 2*this->q[0] * this->q[0] + this->q[1] * this->q[1] - this->q[3] * this->q[3]) +
                 2*F2*(std::complex<double>(0,2)*FPNC*this->kg[2]*this->mn*this->q[1] * this->q[1] + F2NC*this->q[0]*this->q[1]*this->q[3] +
                       F1NC*(2*this->kg[1]*p0 - this->kg[1]*this->q[0] + this->q[0]*this->q[1])*this->q[3] + F2NC*this->kg[1]*(this->kg[3]*this->q[0] + 2*p0*this->q[3] - this->q[0]*this->q[3])))/
               (4.*this->mn2);

    tr[3][2] = -(std::complex<double>(0,8)*F1*FANC*this->kg[1]*this->mn2 + 2*F1*F2NC*this->kg[2]*(this->kg[3]*this->q[0] + 2*p0*this->q[3] - this->q[0]*this->q[3]) +
                 2*F2*this->kg[2]*(F1NC*(2*p0 - this->q[0])*this->q[3] + F2NC*(this->kg[3]*this->q[0] + 2*p0*this->q[3] - this->q[0]*this->q[3])) -
                 this->c_i*F2*FANC*(pow(this->kg[1],3) + 2*this->q[0]*(-2*p0 + this->q[0])*this->q[1] +
                                    this->kg[1]*(this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 4*this->mn2 - 4*pow(p0,2) + 4*p0*this->q[0] -
                                                 2*this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3])))/(4.*this->mn2);

    tr[3][3] = (2*F2*F2NC*(this->kg[1] * this->kg[1]*this->q[0] + this->kg[1]*(2*p0 - this->q[0])*this->q[1] +
                           this->q[0]*(this->kg[2] * this->kg[2] - this->q[0] * this->q[0] + this->q[1] * this->q[1])) - std::complex<double>(0,4)*F1*FPNC*this->kg[2]*this->mn*this->q[1]*this->q[3] -
                std::complex<double>(0,2)*F2*this->kg[2]*(FANC + 2*FPNC*this->mn)*this->q[1]*this->q[3] +
                F1*F2NC*(this->kg[1] * this->kg[1]*this->q[0] + this->kg[1]*(4*p0*this->q[1] - 2*this->q[0]*this->q[1]) +
                         this->q[0]*(this->kg[2] * this->kg[2] - this->kg[3] * this->kg[3] - 4*this->mn2 + 4*pow(p0,2) - 4*p0*this->q[0] +
                                     this->q[1] * this->q[1] + this->q[3] * this->q[3])) +
                F1NC*(-8*F1*this->mn2*this->q[0] + F2*(this->kg[1] * this->kg[1]*this->q[0] + this->kg[1]*(4*p0*this->q[1] - 2*this->q[0]*this->q[1]) +
                                                       this->q[0]*(this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 4*this->mn2 + 4*pow(p0,2) - 4*p0*this->q[0] +
                                                                   this->q[1] * this->q[1] - this->q[3] * this->q[3]))))/(4.*this->mn2);
}

void Hadronic_Current_R_Nucleon::setKg(const std::vector<double> &kg) {
    Hadronic_Current_R::setKg(kg);
    this->F1 = this->F1p;
    this->F2 = this->F2p;
    this->F1NC = this->ff_nucleon->getF1Zp();
    this->F2NC = this->ff_nucleon->getF2Zp();
    this->FANC = this->ff_nucleon->getFAZp();
    this->FPNC = this->ff_nucleon->getFPZp();
    this->set_tr_dir(tr_p_dir);
    this->set_tr_crs(tr_p_crs);

    this->F1 = this->F1n;
    this->F2 = this->F2n;
    this->F1NC = this->ff_nucleon->getF1Zn();
    this->F2NC = this->ff_nucleon->getF2Zn();
    this->FANC = this->ff_nucleon->getFAZn();
    this->FPNC = this->ff_nucleon->getFPZn();
    this->set_tr_dir(tr_n_dir);
    this->set_tr_crs(tr_n_crs);

//    this->qv_minus_kgv = sqrt( (q[1]-kg[1])*(q[1]-kg[1]) + (q[2]-kg[2])*(q[2]-kg[2]) + (q[3]-kg[3])*(q[3]-kg[3]) );

    std::vector<double> pm = this->q;
    std::transform(pm.begin( ), pm.end( ), this->kg.begin( ), pm.begin( ),std::minus<>( ));
    double kapm = sqrt(pm[1]*pm[1]+pm[2]*pm[2]+pm[3]*pm[3]);
//    this->N_FF = this->integral_in_r->integrand(kapm);
    this->nuclearFF->setFF(kapm);
    this->N_FF_p = this->nuclearFF->getFfP();
    this->N_FF_n = this->nuclearFF->getFfN();

}

double Hadronic_Current_R_Nucleon::Propagator_nucleon(std::vector<double> p) {
    double p2 =  p[0] * p[0] - p[1] * p[1] - p[2] * p[2] - p[3] * p[3] ;

    double aux =  1.0/(p2 - Param::mp2);
    return aux;
}

void Hadronic_Current_R_Nucleon::setP(std::vector<double> p) {
    Hadronic_Current_R::setP(p);

    std::vector<double> p_dir = this->p;
    std::transform(p_dir.begin( ), p_dir.end( ), this->q.begin( ), p_dir.begin( ),std::plus<>( ));

    this->propagator_dir = Propagator_nucleon(p_dir);

    std::vector<double> p_crs = this->pp;
    std::transform(p_crs.begin( ), p_crs.end( ), this->q.begin( ), p_crs.begin( ),std::minus<>( ));
    this->propagator_crs = Propagator_nucleon(p_crs);
}







