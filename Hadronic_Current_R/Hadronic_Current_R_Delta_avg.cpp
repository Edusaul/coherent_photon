//
// Created by edusaul on 11/04/19.
//

#include <algorithm>
#include <iostream>
#include "Hadronic_Current_R_Delta_avg.h"


Hadronic_Current_R_Delta_avg::Hadronic_Current_R_Delta_avg(Nuclear_FF *nuclearFf) : nuclearFF(nuclearFf) {
    this->propagator = new Delta_in_medium_with_Int_avg("12C","p");//the nucleus is not used in this class for this verion of the program



}

Hadronic_Current_R_Delta_avg::~Hadronic_Current_R_Delta_avg() {
    delete propagator;
}

std::complex<double> Hadronic_Current_R_Delta_avg::getR(int i, int j) {
    std::complex<double> tr_p = this->tr_p_dir[i][j]*this->propagator_dir_p
                                + this->tr_p_crs[i][j]*this->propagator_crs_p;
    std::complex<double> tr_n = this->tr_n_dir[i][j]*this->propagator_dir_n
                                + this->tr_n_crs[i][j]*this->propagator_crs_n;

    return (tr_p * this->N_FF_p + tr_n * this->N_FF_n) * this->mn / this->p0 / 2.0; //Factor 1/2 from the trace, see notes

}

void Hadronic_Current_R_Delta_avg::setKg(const std::vector<double> &kg) {
    Hadronic_Current_R_Delta::setKg(kg);
    this->nuclearFF->setFF(this->q_minus_kg);
    this->N_FF_p = this->nuclearFF->getFfP();
    this->N_FF_n = this->nuclearFF->getFfN();


    this->dir_or_crs = "dir";
    Propagator_Delta(this->p_dir);
    this->dir_or_crs = "crs";
    Propagator_Delta(this->p_crs);


}

std::complex<double> Hadronic_Current_R_Delta_avg::Propagator_Delta(std::vector<double> p) {
    double p2 = p[0] * p[0] - p[1] * p[1] - p[2] * p[2] - p[3] * p[3];

    std::vector<std::complex<double>> param(2);
    param[0] = p2;
    param[1] = this->q_minus_kg;
    this->propagator->change_other_parameters(param);

    if (this->dir_or_crs == "dir") {
        this->propagator->setNucleon("p");
        this->propagator_dir_p = this->propagator->Delta_propagator_avg_dir();
        this->propagator->setNucleon("n");
        this->propagator_dir_n = this->propagator->Delta_propagator_avg_dir();
//        std::cout<<p2<<"   "<<this->q_minus_kg<<"   "<<this->propagator_dir_p<<std::endl;
//        std::cout<<p2<<"   "<<this->q_minus_kg<<"   "<<this->propagator_dir_n<<std::endl;
    } else if (this->dir_or_crs == "crs") {
        this->propagator->setNucleon("p");
        this->propagator_crs_p = this->propagator->Delta_propagator_crs();
        this->propagator->setNucleon("n");
        this->propagator_crs_n = this->propagator->Delta_propagator_crs();
//        std::cout<<p2<<"   "<<this->q_minus_kg<<"   "<<this->propagator_crs_p<<std::endl;
//        std::cout<<p2<<"   "<<this->q_minus_kg<<"   "<<this->propagator_crs_n<<std::endl<<std::endl;
    }



    return 0;
}

void Hadronic_Current_R_Delta_avg::setP(std::vector<double> p) {
    Hadronic_Current_R::setP(p);

    this->p_dir = this->p;
    std::transform(this->p_dir.begin( ), this->p_dir.end( ), this->q.begin( ), this->p_dir.begin( ),std::plus<>( ));

    this->p_crs = this->pp;
    std::transform(this->p_crs.begin( ), this->p_crs.end( ), this->q.begin( ), this->p_crs.begin( ),std::minus<>( ));

}


