//
// Created by eduardo on 3/07/19.
//

#include "Hadronic_Current_tr_J12.h"



void Hadronic_Current_tr_J12::set_tr_crs_negative(Array4x4 &tr) {
    tr[
            0
    ][
            0
    ]=
            (-(F1*this->q[0]*(this->q[1]*(-2*F1NC*this->kg[1]*this->q[0]*(2*p0 + this->q[0]) + 2*F1NC*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3])*this->q[1] + F2NC*(-(this->kg[1]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 2*(2*(this->mn2 + pow(p0,2)) + 2*p0*this->q[0] + this->q[0] * this->q[0]))) - 2*this->q[0]*(2*p0 + this->q[0])*this->q[1] + this->kg[1]*this->q[1] * this->q[1])) - this->kg[3]*(2*F1NC*this->q[0]*(2*p0 + this->q[0]) + F2NC*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 2*(2*(this->mn2 + pow(p0,2)) + 2*p0*this->q[0] + this->q[0] * this->q[0]) - this->q[1] * this->q[1]))*this->q[3] + (2*F1NC*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3]) - 2*F2NC*this->q[0]*(2*p0 + this->q[0]) + F2NC*this->kg[1]*this->q[1])*this->q[3] * this->q[3] + F2NC*this->kg[3]*pow(this->q[3],3))) + F2*(-8*F2NC*this->mn2*(2*p0 + this->q[0])*(this->kg[1]*this->q[1] + this->kg[3]*this->q[3]) + F1NC*(this->q[1]*(this->kg[1]*this->q[0]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] + 4*(this->mn2 + p0*(p0 + this->q[0]))) - 2*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3])*(2*p0 + this->q[0])*this->q[1] + this->kg[1]*this->q[0]*this->q[1] * this->q[1]) + this->kg[3]*this->q[0]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] + 4*(this->mn2 + p0*(p0 + this->q[0])) + this->q[1] * this->q[1])*this->q[3] + (-2*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3])*(2*p0 + this->q[0]) + this->kg[1]*this->q[0]*this->q[1])*this->q[3] * this->q[3] + this->kg[3]*this->q[0]*pow(this->q[3],3))))/(16.*pow(this->mn,4))
            ;

    tr[
            0
    ][
            1
    ]=
            (F1*this->q[0]*(std::complex<double>(0,-8)*FANC*this->kg[2]*this->mn2*this->q[3] + 2*F1NC*(-(this->kg[1] * this->kg[1]*this->q[0]*this->q[1]) - (this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3])*this->q[0]*this->q[1] + this->kg[3]*(2*p0 + this->q[0])*this->q[1]*this->q[3] + this->kg[1]*(2*p0 + this->q[0])*(this->q[0] - this->q[3])*(this->q[0] + this->q[3])) + F2NC*this->q[0]*(pow(this->kg[1],3) + 2*this->q[1]*(2*p0*this->q[0] + this->q[0] * this->q[0] - this->kg[3]*this->q[3]) + this->kg[1]*(this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 4*this->mn2 - 2*(2*pow(p0,2) + 2*p0*this->q[0] + this->q[0] * this->q[0]) - this->q[1] * this->q[1] + this->q[3] * this->q[3]))) + F2*(-8*F2NC*this->kg[1]*this->mn2*this->q[0]*(2*p0 + this->q[0]) - std::complex<double>(0,8)*FANC*this->kg[2]*this->mn2*(2*p0 + this->q[0])*this->q[3] + F1NC*(pow(this->kg[1],3)*(this->q[0] - this->q[3])*(this->q[0] + this->q[3]) + this->kg[1] * this->kg[1]*this->q[1]*(-2*this->q[0]*(2*p0 + this->q[0]) + this->kg[3]*this->q[3]) + this->kg[1]*(this->q[0] - this->q[3])*(this->q[0] + this->q[3])*(this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] + 4*this->mn2 + 4*p0*(p0 + this->q[0]) + this->q[1] * this->q[1] + this->q[3] * this->q[3]) + this->q[1]*(-2*(this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3])*this->q[0]*(2*p0 + this->q[0]) + this->kg[3]*(this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] + 4*(this->mn2 + p0*(p0 + this->q[0])) + this->q[1] * this->q[1])*this->q[3] + this->kg[3]*pow(this->q[3],3)))))/(16.*pow(this->mn,4))
            ;

    tr[
            0
    ][
            2
    ]=
            (F2*(-8*F2NC*this->kg[2]*this->mn2*this->q[0]*(2*p0 + this->q[0]) - std::complex<double>(0,8)*FANC*this->mn2*(2*p0 + this->q[0])*(this->kg[3]*this->q[1] - this->kg[1]*this->q[3]) + F1NC*this->kg[2]*(this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3])*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] + 4*this->mn2 + 4*p0*(p0 + this->q[0]) + this->q[1] * this->q[1] + this->q[3] * this->q[3])) + F1*this->q[0]*(std::complex<double>(0,8)*FANC*this->mn2*(-(this->kg[3]*this->q[1]) + this->kg[1]*this->q[3]) + 2*F1NC*this->kg[2]*(2*p0 + this->q[0])*(this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3]) + F2NC*this->kg[2]*this->q[0]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 4*this->mn2 - 2*(2*pow(p0,2) + 2*p0*this->q[0] + this->q[0] * this->q[0]) + this->q[1] * this->q[1] + this->q[3] * this->q[3])))/(16.*pow(this->mn,4))
            ;

    tr[
            0
    ][
            3
    ]=
            (F1*this->q[0]*(std::complex<double>(0,8)*FANC*this->kg[2]*this->mn2*this->q[1] + 2*F1NC*(this->kg[3]*(2*p0 + this->q[0])*(this->q[0] - this->q[1])*(this->q[0] + this->q[1]) - (this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3])*this->q[0]*this->q[3] + this->kg[1]*(2*p0 + this->q[0])*this->q[1]*this->q[3]) + F2NC*this->q[0]*(this->kg[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 2*(2*(this->mn2 + pow(p0,2)) + 2*p0*this->q[0] + this->q[0] * this->q[0]) + this->q[1] * this->q[1]) + 2*(2*p0*this->q[0] + this->q[0] * this->q[0] - this->kg[1]*this->q[1])*this->q[3] - this->kg[3]*this->q[3] * this->q[3])) + F2*(-8*F2NC*this->kg[3]*this->mn2*this->q[0]*(2*p0 + this->q[0]) + std::complex<double>(0,8)*FANC*this->kg[2]*this->mn2*(2*p0 + this->q[0])*this->q[1] + F1NC*(this->kg[3]*(this->q[0] - this->q[1])*(this->q[0] + this->q[1])*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] + 4*(this->mn2 + p0*(p0 + this->q[0])) + this->q[1] * this->q[1]) + (-2*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3])*this->q[0]*(2*p0 + this->q[0]) + this->kg[1]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] + 4*(this->mn2 + p0*(p0 + this->q[0])))*this->q[1] + this->kg[1]*pow(this->q[1],3))*this->q[3] + this->kg[3]*(this->q[0] - this->q[1])*(this->q[0] + this->q[1])*this->q[3] * this->q[3] + this->kg[1]*this->q[1]*pow(this->q[3],3))))/(16.*pow(this->mn,4))
            ;

    tr[
            1
    ][
            0
    ]=
            (F1*this->kg[1]*(this->q[1]*(2*F1NC*this->kg[1]*this->q[0]*(2*p0 + this->q[0]) - 2*F1NC*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3])*this->q[1] + F2NC*(pow(this->kg[1],3) + 2*this->q[0]*(2*p0 + this->q[0])*this->q[1] + this->kg[1]*(this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 2*(2*(this->mn2 + pow(p0,2)) + 2*p0*this->q[0] + this->q[0] * this->q[0]) - this->q[1] * this->q[1]))) + this->kg[3]*(2*F1NC*this->q[0]*(2*p0 + this->q[0]) + F2NC*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 2*(2*(this->mn2 + pow(p0,2)) + 2*p0*this->q[0] + this->q[0] * this->q[0]) - this->q[1] * this->q[1]))*this->q[3] - (2*F1NC*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3]) - 2*F2NC*this->q[0]*(2*p0 + this->q[0]) + F2NC*this->kg[1]*this->q[1])*this->q[3] * this->q[3] - F2NC*this->kg[3]*pow(this->q[3],3)) + F2*(-8*F2NC*this->mn2*this->q[0]*(2*p0 + this->q[0])*this->q[1] + F1NC*(this->q[1]*(this->q[0] * this->q[0]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] + 4*(this->mn2 + p0*(p0 + this->q[0]))) - 2*this->kg[1]*this->q[0]*(2*p0 + this->q[0])*this->q[1] + (-2*(this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3]) + this->q[0] * this->q[0])*this->q[1] * this->q[1]) + 2*this->kg[1]*this->kg[3]*this->q[1] * this->q[1]*this->q[3] - (2*this->kg[1]*this->q[0]*(2*p0 + this->q[0]) + 2*(this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3])*this->q[1] - this->q[0] * this->q[0]*this->q[1])*this->q[3] * this->q[3] + 2*this->kg[1]*this->kg[3]*pow(this->q[3],3))))/(16.*pow(this->mn,4))
            ;

    tr[
            1
    ][
            1
    ]=
            (F2*(std::complex<double>(0,-8)*FANC*this->kg[2]*this->mn2*this->q[1]*this->q[3] - 8*F2NC*this->mn2*this->q[0]*(2*p0*this->q[0] + this->q[0] * this->q[0] - this->kg[3]*this->q[3]) + F1NC*(this->q[0]*(this->q[0] * this->q[0]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] + 4*(this->mn2 + p0*(p0 + this->q[0]))) - 2*this->kg[1]*this->q[0]*(2*p0 + this->q[0])*this->q[1] + (-2*(this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3]) + this->q[0] * this->q[0])*this->q[1] * this->q[1]) + 2*this->kg[3]*(-(this->q[0] * this->q[0]*(2*p0 + this->q[0])) + this->kg[1]*this->q[0]*this->q[1] + (2*p0 + this->q[0])*this->q[1] * this->q[1])*this->q[3] - this->q[0]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] + 4*this->mn2 + 4*pow(p0,2) + 4*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1] * this->q[1])*this->q[3] * this->q[3] + 2*this->kg[3]*(2*p0 + this->q[0])*pow(this->q[3],3) - this->q[0]*pow(this->q[3],4))) + F1*this->kg[1]*(std::complex<double>(0,-8)*FANC*this->kg[2]*this->mn2*this->q[3] + 2*F1NC*(-(this->kg[1] * this->kg[1]*this->q[0]*this->q[1]) - (this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3])*this->q[0]*this->q[1] + this->kg[3]*(2*p0 + this->q[0])*this->q[1]*this->q[3] + this->kg[1]*(2*p0 + this->q[0])*(this->q[0] - this->q[3])*(this->q[0] + this->q[3])) + F2NC*this->q[0]*(pow(this->kg[1],3) + 2*this->q[1]*(2*p0*this->q[0] + this->q[0] * this->q[0] - this->kg[3]*this->q[3]) + this->kg[1]*(this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 4*this->mn2 - 2*(2*pow(p0,2) + 2*p0*this->q[0] + this->q[0] * this->q[0]) - this->q[1] * this->q[1] + this->q[3] * this->q[3]))))/(16.*pow(this->mn,4))
            ;

    tr[
            1
    ][
            2
    ]=
            (std::complex<double>(0,4)*F2*FANC*this->mn2*(this->kg[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] + 4*this->mn2 - 2*(2*pow(p0,2) + 2*p0*this->q[0] + this->q[0] * this->q[0]) - this->q[1] * this->q[1]) + 2*this->q[0]*(2*p0 + this->q[0])*this->q[3] - this->kg[3]*this->q[3] * this->q[3]) - 2*F2*this->kg[2]*this->q[1]*(4*F2NC*this->mn2*this->q[0] + F1NC*(2*p0 + this->q[0])*(-this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3])) + F1*this->kg[1]*(std::complex<double>(0,8)*FANC*this->mn2*(-(this->kg[3]*this->q[1]) + this->kg[1]*this->q[3]) + 2*F1NC*this->kg[2]*(2*p0 + this->q[0])*(this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3]) + F2NC*this->kg[2]*this->q[0]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 4*this->mn2 - 2*(2*pow(p0,2) + 2*p0*this->q[0] + this->q[0] * this->q[0]) + this->q[1] * this->q[1] + this->q[3] * this->q[3])))/(16.*pow(this->mn,4))
            ;

    tr[
            1
    ][
            3
    ]=
            (F1*this->kg[1]*(std::complex<double>(0,8)*FANC*this->kg[2]*this->mn2*this->q[1] + 2*F1NC*(this->kg[3]*(2*p0 + this->q[0])*(this->q[0] - this->q[1])*(this->q[0] + this->q[1]) - (this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3])*this->q[0]*this->q[3] + this->kg[1]*(2*p0 + this->q[0])*this->q[1]*this->q[3]) + F2NC*this->q[0]*(this->kg[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 2*(2*(this->mn2 + pow(p0,2)) + 2*p0*this->q[0] + this->q[0] * this->q[0]) + this->q[1] * this->q[1]) + 2*(2*p0*this->q[0] + this->q[0] * this->q[0] - this->kg[1]*this->q[1])*this->q[3] - this->kg[3]*this->q[3] * this->q[3])) + F2*(-8*F2NC*this->kg[3]*this->mn2*this->q[0]*this->q[1] - std::complex<double>(0,4)*FANC*this->kg[2]*this->mn2*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] + 4*this->mn2 - 2*(2*pow(p0,2) + 2*p0*this->q[0] + this->q[0] * this->q[0]) - this->q[1] * this->q[1] + this->q[3] * this->q[3]) + F1NC*(2*this->kg[3]*(2*p0 + this->q[0])*(this->q[0] - this->q[1])*this->q[1]*(this->q[0] + this->q[1]) + this->q[0]*(-2*this->kg[1]*this->q[0]*(2*p0 + this->q[0]) + (this->kg[1] * this->kg[1] - this->kg[2] * this->kg[2] - this->kg[3] * this->kg[3] + 4*(this->mn2 + p0*(p0 + this->q[0])))*this->q[1] + pow(this->q[1],3))*this->q[3] + 2*this->kg[3]*(this->kg[1]*this->q[0] - (2*p0 + this->q[0])*this->q[1])*this->q[3] * this->q[3] + this->q[0]*this->q[1]*pow(this->q[3],3))))/(16.*pow(this->mn,4))
            ;

    tr[
            2
    ][
            0
    ]=
            (this->kg[2]*(-2*F1NC*F2*(2*p0*this->q[0] + this->q[0] * this->q[0] - this->kg[1]*this->q[1] - this->kg[3]*this->q[3])*(this->q[1] * this->q[1] + this->q[3] * this->q[3]) + F1*(this->q[1]*(2*F1NC*this->kg[1]*this->q[0]*(2*p0 + this->q[0]) - 2*F1NC*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3])*this->q[1] + F2NC*(pow(this->kg[1],3) + 2*this->q[0]*(2*p0 + this->q[0])*this->q[1] + this->kg[1]*(this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 2*(2*(this->mn2 + pow(p0,2)) + 2*p0*this->q[0] + this->q[0] * this->q[0]) - this->q[1] * this->q[1]))) + this->kg[3]*(2*F1NC*this->q[0]*(2*p0 + this->q[0]) + F2NC*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 2*(2*(this->mn2 + pow(p0,2)) + 2*p0*this->q[0] + this->q[0] * this->q[0]) - this->q[1] * this->q[1]))*this->q[3] - (2*F1NC*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3]) - 2*F2NC*this->q[0]*(2*p0 + this->q[0]) + F2NC*this->kg[1]*this->q[1])*this->q[3] * this->q[3] - F2NC*this->kg[3]*pow(this->q[3],3))))/(16.*pow(this->mn,4))
            ;

    tr[
            2
    ][
            1
    ]=
            (2*F1NC*F2*this->kg[2]*this->q[0]*this->q[1]*(-(this->q[0]*(2*p0 + this->q[0])) + this->kg[1]*this->q[1] + this->kg[3]*this->q[3]) - std::complex<double>(0,4)*F2*FANC*this->mn2*(this->kg[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] + 4*this->mn2 - 2*(2*pow(p0,2) + 2*p0*this->q[0] + this->q[0] * this->q[0]) + this->q[1] * this->q[1]) + 2*(2*p0*this->q[0] + this->q[0] * this->q[0] - this->kg[1]*this->q[1])*this->q[3] - this->kg[3]*this->q[3] * this->q[3]) + F1*this->kg[2]*(std::complex<double>(0,-8)*FANC*this->kg[2]*this->mn2*this->q[3] + 2*F1NC*(-(this->kg[1] * this->kg[1]*this->q[0]*this->q[1]) - (this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3])*this->q[0]*this->q[1] + this->kg[3]*(2*p0 + this->q[0])*this->q[1]*this->q[3] + this->kg[1]*(2*p0 + this->q[0])*(this->q[0] - this->q[3])*(this->q[0] + this->q[3])) + F2NC*this->q[0]*(pow(this->kg[1],3) + 2*this->q[1]*(2*p0*this->q[0] + this->q[0] * this->q[0] - this->kg[3]*this->q[3]) + this->kg[1]*(this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 4*this->mn2 - 2*(2*pow(p0,2) + 2*p0*this->q[0] + this->q[0] * this->q[0]) - this->q[1] * this->q[1] + this->q[3] * this->q[3]))))/(16.*pow(this->mn,4))
            ;

    tr[
            2
    ][
            2
    ]=
            (8*F2*F2NC*this->mn2*this->q[0]*(-(this->q[0]*(2*p0 + this->q[0])) + this->kg[1]*this->q[1] + this->kg[3]*this->q[3]) + F1NC*F2*(this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3])*(this->kg[1] * this->kg[1]*this->q[0] - 2*this->kg[1]*(2*p0 + this->q[0])*this->q[1] + this->q[0]*(this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] + 4*(this->mn2 + p0*(p0 + this->q[0])) + this->q[1] * this->q[1]) - 2*this->kg[3]*(2*p0 + this->q[0])*this->q[3] + this->q[0]*this->q[3] * this->q[3]) + F1*this->kg[2]*(std::complex<double>(0,8)*FANC*this->mn2*(-(this->kg[3]*this->q[1]) + this->kg[1]*this->q[3]) + 2*F1NC*this->kg[2]*(2*p0 + this->q[0])*(this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3]) + F2NC*this->kg[2]*this->q[0]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 4*this->mn2 - 2*(2*pow(p0,2) + 2*p0*this->q[0] + this->q[0] * this->q[0]) + this->q[1] * this->q[1] + this->q[3] * this->q[3])))/(16.*pow(this->mn,4))
            ;

    tr[
            2
    ][
            3
    ]=
            (F1*this->kg[2]*(std::complex<double>(0,8)*FANC*this->kg[2]*this->mn2*this->q[1] + 2*F1NC*(this->kg[3]*(2*p0 + this->q[0])*(this->q[0] - this->q[1])*(this->q[0] + this->q[1]) - (this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3])*this->q[0]*this->q[3] + this->kg[1]*(2*p0 + this->q[0])*this->q[1]*this->q[3]) + F2NC*this->q[0]*(this->kg[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 2*(2*(this->mn2 + pow(p0,2)) + 2*p0*this->q[0] + this->q[0] * this->q[0]) + this->q[1] * this->q[1]) + 2*(2*p0*this->q[0] + this->q[0] * this->q[0] - this->kg[1]*this->q[1])*this->q[3] - this->kg[3]*this->q[3] * this->q[3])) + 2*F2*(F1NC*this->kg[2]*this->q[0]*this->q[3]*(-(this->q[0]*(2*p0 + this->q[0])) + this->kg[1]*this->q[1] + this->kg[3]*this->q[3]) + std::complex<double>(0,2)*FANC*this->mn2*(pow(this->kg[1],3) + 2*this->q[1]*(2*p0*this->q[0] + this->q[0] * this->q[0] - this->kg[3]*this->q[3]) + this->kg[1]*(this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] + 4*this->mn2 - 2*(2*pow(p0,2) + 2*p0*this->q[0] + this->q[0] * this->q[0]) - this->q[1] * this->q[1] + this->q[3] * this->q[3]))))/(16.*pow(this->mn,4))
            ;

    tr[
            3
    ][
            0
    ]=
            (F1*this->kg[3]*(this->q[1]*(2*F1NC*this->kg[1]*this->q[0]*(2*p0 + this->q[0]) - 2*F1NC*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3])*this->q[1] + F2NC*(pow(this->kg[1],3) + 2*this->q[0]*(2*p0 + this->q[0])*this->q[1] + this->kg[1]*(this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 2*(2*(this->mn2 + pow(p0,2)) + 2*p0*this->q[0] + this->q[0] * this->q[0]) - this->q[1] * this->q[1]))) + this->kg[3]*(2*F1NC*this->q[0]*(2*p0 + this->q[0]) + F2NC*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 2*(2*(this->mn2 + pow(p0,2)) + 2*p0*this->q[0] + this->q[0] * this->q[0]) - this->q[1] * this->q[1]))*this->q[3] - (2*F1NC*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3]) - 2*F2NC*this->q[0]*(2*p0 + this->q[0]) + F2NC*this->kg[1]*this->q[1])*this->q[3] * this->q[3] - F2NC*this->kg[3]*pow(this->q[3],3)) + F2*(-8*F2NC*this->mn2*this->q[0]*(2*p0 + this->q[0])*this->q[3] + F1NC*(-2*this->kg[3]*this->q[1] * this->q[1]*(2*p0*this->q[0] + this->q[0] * this->q[0] - this->kg[1]*this->q[1]) + (this->q[0] * this->q[0]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] + 4*(this->mn2 + p0*(p0 + this->q[0]))) + (-2*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + this->q[0] * this->q[0])*this->q[1] * this->q[1])*this->q[3] - 2*this->kg[3]*(2*p0*this->q[0] + this->q[0] * this->q[0] - this->kg[1]*this->q[1])*this->q[3] * this->q[3] + (-2*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + this->q[0] * this->q[0])*pow(this->q[3],3))))/(16.*pow(this->mn,4))
            ;

    tr[
            3
    ][
            1
    ]=
            (F2*(-2*F1NC*this->kg[3]*this->q[0]*this->q[1]*(2*p0*this->q[0] + this->q[0] * this->q[0] - this->kg[1]*this->q[1]) + (-8*F2NC*this->kg[1]*this->mn2*this->q[0] + F1NC*(-(this->kg[1] * this->kg[1]*this->q[0]*this->q[1]) + 2*this->kg[1]*(2*p0 + this->q[0])*(this->q[0] - this->q[1])*(this->q[0] + this->q[1]) + this->q[0]*this->q[1]*(-this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] + 4*(this->mn2 + p0*(p0 + this->q[0])) + this->q[1] * this->q[1])))*this->q[3] + F1NC*(-2*this->kg[1]*(2*p0 + this->q[0]) + this->q[0]*this->q[1])*pow(this->q[3],3) + std::complex<double>(0,4)*FANC*this->kg[2]*this->mn2*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] + 4*this->mn2 - 4*pow(p0,2) - 4*p0*this->q[0] - 2*this->q[0] * this->q[0] + this->q[1] * this->q[1] - this->q[3] * this->q[3])) + F1*this->kg[3]*(std::complex<double>(0,-8)*FANC*this->kg[2]*this->mn2*this->q[3] + 2*F1NC*(-(this->kg[1] * this->kg[1]*this->q[0]*this->q[1]) - (this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3])*this->q[0]*this->q[1] + this->kg[3]*(2*p0 + this->q[0])*this->q[1]*this->q[3] + this->kg[1]*(2*p0 + this->q[0])*(this->q[0] - this->q[3])*(this->q[0] + this->q[3])) + F2NC*this->q[0]*(pow(this->kg[1],3) + 2*this->q[1]*(2*p0*this->q[0] + this->q[0] * this->q[0] - this->kg[3]*this->q[3]) + this->kg[1]*(this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 4*this->mn2 - 2*(2*pow(p0,2) + 2*p0*this->q[0] + this->q[0] * this->q[0]) - this->q[1] * this->q[1] + this->q[3] * this->q[3]))))/(16.*pow(this->mn,4))
            ;

    tr[
            3
    ][
            2
    ]=
            (std::complex<double>(0,-4)*F2*FANC*this->mn2*(pow(this->kg[1],3) + 2*this->q[0]*(2*p0 + this->q[0])*this->q[1] + this->kg[1]*(this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] + 4*this->mn2 - 2*(2*pow(p0,2) + 2*p0*this->q[0] + this->q[0] * this->q[0]) - this->q[1] * this->q[1] - this->q[3] * this->q[3])) - 2*F2*this->kg[2]*this->q[3]*(4*F2NC*this->mn2*this->q[0] + F1NC*(2*p0 + this->q[0])*(-this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3])) + F1*this->kg[3]*(std::complex<double>(0,8)*FANC*this->mn2*(-(this->kg[3]*this->q[1]) + this->kg[1]*this->q[3]) + 2*F1NC*this->kg[2]*(2*p0 + this->q[0])*(this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3]) + F2NC*this->kg[2]*this->q[0]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 4*this->mn2 - 2*(2*pow(p0,2) + 2*p0*this->q[0] + this->q[0] * this->q[0]) + this->q[1] * this->q[1] + this->q[3] * this->q[3])))/(16.*pow(this->mn,4))
            ;

    tr[
            3
    ][
            3
    ]=
            (F2*(-8*F2NC*this->mn2*this->q[0]*(2*p0*this->q[0] + this->q[0] * this->q[0] - this->kg[1]*this->q[1]) + F1NC*(this->q[0] - this->q[1])*(this->q[0] + this->q[1])*(this->kg[1] * this->kg[1]*this->q[0] - 2*this->kg[1]*(2*p0 + this->q[0])*this->q[1] + this->q[0]*(this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] + 4*(this->mn2 + p0*(p0 + this->q[0])) + this->q[1] * this->q[1])) + std::complex<double>(0,8)*FANC*this->kg[2]*this->mn2*this->q[1]*this->q[3] - 2*F1NC*this->kg[3]*this->q[0]*(2*p0*this->q[0] + this->q[0] * this->q[0] - this->kg[1]*this->q[1])*this->q[3] + F1NC*(-2*this->kg[1] * this->kg[1]*this->q[0] - 2*this->kg[2] * this->kg[2]*this->q[0] + pow(this->q[0],3) + 2*this->kg[1]*(2*p0 + this->q[0])*this->q[1] - this->q[0]*this->q[1] * this->q[1])*this->q[3] * this->q[3]) + F1*this->kg[3]*(std::complex<double>(0,8)*FANC*this->kg[2]*this->mn2*this->q[1] + 2*F1NC*(this->kg[3]*(2*p0 + this->q[0])*(this->q[0] - this->q[1])*(this->q[0] + this->q[1]) - (this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3])*this->q[0]*this->q[3] + this->kg[1]*(2*p0 + this->q[0])*this->q[1]*this->q[3]) + F2NC*this->q[0]*(this->kg[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 2*(2*(this->mn2 + pow(p0,2)) + 2*p0*this->q[0] + this->q[0] * this->q[0]) + this->q[1] * this->q[1]) + 2*(2*p0*this->q[0] + this->q[0] * this->q[0] - this->kg[1]*this->q[1])*this->q[3] - this->kg[3]*this->q[3] * this->q[3])))/(16.*pow(this->mn,4))
            ;

}

void Hadronic_Current_tr_J12::set_tr_dir_negative(Array4x4 &tr) {
    tr[
            0
    ][
            0
    ]=
            (F1*this->q[0]*(this->q[1]*(this->kg[1]*(F2NC*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 4*(this->mn2 + pow(p0,2))) + 4*(F1NC + F2NC)*p0*this->q[0] - 2*(F1NC + F2NC)*this->q[0] * this->q[0]) + 2*(F1NC*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3]) + F2NC*this->q[0]*(-2*p0 + this->q[0]))*this->q[1] - F2NC*this->kg[1]*this->q[1] * this->q[1]) + this->kg[3]*(2*F1NC*(2*p0 - this->q[0])*this->q[0] + F2NC*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 4*this->mn2 - 4*pow(p0,2) + 4*p0*this->q[0] - 2*this->q[0] * this->q[0] - this->q[1] * this->q[1]))*this->q[3] + (2*F1NC*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3]) + 2*F2NC*this->q[0]*(-2*p0 + this->q[0]) - F2NC*this->kg[1]*this->q[1])*this->q[3] * this->q[3] - F2NC*this->kg[3]*pow(this->q[3],3)) + F2*(-8*F2NC*this->mn2*(2*p0 - this->q[0])*(this->kg[1]*this->q[1] + this->kg[3]*this->q[3]) + F1NC*(this->q[1]*(this->kg[1]*this->q[0]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] + 4*(this->mn2 + pow(p0,2) - p0*this->q[0])) - 2*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3])*(-2*p0 + this->q[0])*this->q[1] + this->kg[1]*this->q[0]*this->q[1] * this->q[1]) + this->kg[3]*this->q[0]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] + 4*(this->mn2 + pow(p0,2) - p0*this->q[0]) + this->q[1] * this->q[1])*this->q[3] + (2*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3])*(2*p0 - this->q[0]) + this->kg[1]*this->q[0]*this->q[1])*this->q[3] * this->q[3] + this->kg[3]*this->q[0]*pow(this->q[3],3))))/(16.*pow(this->mn,4))
            ;

    tr[
            0
    ][
            1
    ]=
            (F1*this->q[0]*(this->q[0]*(this->kg[1]*(F2NC*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 4*(this->mn2 + pow(p0,2))) + 4*(F1NC + F2NC)*p0*this->q[0] - 2*(F1NC + F2NC)*this->q[0] * this->q[0]) + 2*(F1NC*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3]) + F2NC*this->q[0]*(-2*p0 + this->q[0]))*this->q[1] - F2NC*this->kg[1]*this->q[1] * this->q[1]) - 2.0*(std::complex<double>(0,4)*FANC*this->kg[2]*this->mn2 + this->kg[3]*(-2*F1NC*p0 + (F1NC + F2NC)*this->q[0])*this->q[1])*this->q[3] + this->kg[1]*(F2NC*this->q[0] + 2*F1NC*(-2*p0 + this->q[0]))*this->q[3] * this->q[3]) + F2*(8*F2NC*this->kg[1]*this->mn2*this->q[0]*(-2*p0 + this->q[0]) - std::complex<double>(0,8)*FANC*this->kg[2]*this->mn2*(2*p0 - this->q[0])*this->q[3] + F1NC*(pow(this->kg[1],3)*(this->q[0] - this->q[3])*(this->q[0] + this->q[3]) + this->kg[1] * this->kg[1]*this->q[1]*(4*p0*this->q[0] - 2*this->q[0] * this->q[0] + this->kg[3]*this->q[3]) + this->kg[1]*(this->q[0] - this->q[3])*(this->q[0] + this->q[3])*(this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] + 4*this->mn2 + 4*p0*(p0 - this->q[0]) + this->q[1] * this->q[1] + this->q[3] * this->q[3]) + this->q[1]*(2*(this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3])*(2*p0 - this->q[0])*this->q[0] + this->kg[3]*(this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] + 4*(this->mn2 + pow(p0,2) - p0*this->q[0]) + this->q[1] * this->q[1])*this->q[3] + this->kg[3]*pow(this->q[3],3)))))/(16.*pow(this->mn,4))
            ;

    tr[
            0
    ][
            2
    ]=
            (F2*(8*F2NC*this->kg[2]*this->mn2*this->q[0]*(-2*p0 + this->q[0]) - std::complex<double>(0,8)*FANC*this->mn2*(2*p0 - this->q[0])*(this->kg[3]*this->q[1] - this->kg[1]*this->q[3]) + F1NC*this->kg[2]*(this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3])*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] + 4*this->mn2 + 4*p0*(p0 - this->q[0]) + this->q[1] * this->q[1] + this->q[3] * this->q[3])) + F1*this->q[0]*(std::complex<double>(0,8)*FANC*this->mn2*(-(this->kg[3]*this->q[1]) + this->kg[1]*this->q[3]) + 2*F1NC*this->kg[2]*(2*p0 - this->q[0])*(this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3]) + F2NC*this->kg[2]*this->q[0]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 4*this->mn2 - 4*pow(p0,2) + 4*p0*this->q[0] - 2*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3])))/(16.*pow(this->mn,4))
            ;

    tr[
            0
    ][
            3
    ]=
            (F1*this->q[0]*(2.0*(std::complex<double>(0,4)*FANC*this->kg[2]*this->mn2*this->q[1] + F1NC*this->kg[3]*(2*p0 - this->q[0])*(this->q[0] - this->q[1])*(this->q[0] + this->q[1]) + F1NC*((this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3])*this->q[0] + this->kg[1]*(2*p0 - this->q[0])*this->q[1])*this->q[3]) + F2NC*this->q[0]*(this->kg[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 4*this->mn2 - 4*pow(p0,2) + 4*p0*this->q[0] - 2*this->q[0] * this->q[0] + this->q[1] * this->q[1]) + 2*(-2*p0*this->q[0] + this->q[0] * this->q[0] - this->kg[1]*this->q[1])*this->q[3] - this->kg[3]*this->q[3] * this->q[3])) + F2*(8*F2NC*this->kg[3]*this->mn2*this->q[0]*(-2*p0 + this->q[0]) + std::complex<double>(0,8)*FANC*this->kg[2]*this->mn2*(2*p0 - this->q[0])*this->q[1] + F1NC*(this->kg[3]*(this->q[0] - this->q[1])*(this->q[0] + this->q[1])*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] + 4*(this->mn2 + pow(p0,2) - p0*this->q[0]) + this->q[1] * this->q[1]) + (2*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3])*(2*p0 - this->q[0])*this->q[0] + this->kg[1]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] + 4*(this->mn2 + pow(p0,2) - p0*this->q[0]))*this->q[1] + this->kg[1]*pow(this->q[1],3))*this->q[3] + this->kg[3]*(this->q[0] - this->q[1])*(this->q[0] + this->q[1])*this->q[3] * this->q[3] + this->kg[1]*this->q[1]*pow(this->q[3],3))))/(16.*pow(this->mn,4))
            ;

    tr[
            1
    ][
            0
    ]=
            (F1*this->kg[1]*(this->q[1]*(this->kg[1]*(F2NC*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 4*(this->mn2 + pow(p0,2))) + 4*(F1NC + F2NC)*p0*this->q[0] - 2*(F1NC + F2NC)*this->q[0] * this->q[0]) + 2*(F1NC*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3]) + F2NC*this->q[0]*(-2*p0 + this->q[0]))*this->q[1] - F2NC*this->kg[1]*this->q[1] * this->q[1]) + this->kg[3]*(2*F1NC*(2*p0 - this->q[0])*this->q[0] + F2NC*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 4*this->mn2 - 4*pow(p0,2) + 4*p0*this->q[0] - 2*this->q[0] * this->q[0] - this->q[1] * this->q[1]))*this->q[3] + (2*F1NC*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3]) + 2*F2NC*this->q[0]*(-2*p0 + this->q[0]) - F2NC*this->kg[1]*this->q[1])*this->q[3] * this->q[3] - F2NC*this->kg[3]*pow(this->q[3],3)) + F2*(8*F2NC*this->mn2*this->q[0]*(-2*p0 + this->q[0])*this->q[1] + F1NC*(this->q[1]*(this->q[0] * this->q[0]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] + 4*(this->mn2 + pow(p0,2) - p0*this->q[0])) - 2*this->kg[1]*this->q[0]*(-2*p0 + this->q[0])*this->q[1] + (-2*(this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3]) + this->q[0] * this->q[0])*this->q[1] * this->q[1]) + 2*this->kg[1]*this->kg[3]*this->q[1] * this->q[1]*this->q[3] + (-2*(this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3])*this->q[1] + this->q[0]*(4*this->kg[1]*p0 - 2*this->kg[1]*this->q[0] + this->q[0]*this->q[1]))*this->q[3] * this->q[3] + 2*this->kg[1]*this->kg[3]*pow(this->q[3],3))))/(16.*pow(this->mn,4))
            ;

    tr[
            1
    ][
            1
    ]=
            (F1*this->kg[1]*(this->q[0]*(this->kg[1]*(F2NC*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 4*(this->mn2 + pow(p0,2))) + 4*(F1NC + F2NC)*p0*this->q[0] - 2*(F1NC + F2NC)*this->q[0] * this->q[0]) + 2*(F1NC*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3]) + F2NC*this->q[0]*(-2*p0 + this->q[0]))*this->q[1] - F2NC*this->kg[1]*this->q[1] * this->q[1]) - 2.0*(std::complex<double>(0,4)*FANC*this->kg[2]*this->mn2 + this->kg[3]*(-2*F1NC*p0 + (F1NC + F2NC)*this->q[0])*this->q[1])*this->q[3] + this->kg[1]*(F2NC*this->q[0] + 2*F1NC*(-2*p0 + this->q[0]))*this->q[3] * this->q[3]) + F2*(F1NC*this->q[0]*(this->q[0] * this->q[0]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] + 4*(this->mn2 + pow(p0,2) - p0*this->q[0])) - 2*this->kg[1]*this->q[0]*(-2*p0 + this->q[0])*this->q[1] + (-2*(this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3]) + this->q[0] * this->q[0])*this->q[1] * this->q[1]) + 2.0*(std::complex<double>(0,4)*FANC*this->kg[2]*this->mn2*this->q[1] + F1NC*this->kg[3]*(-pow(this->q[0],3) + this->q[0]*this->q[1]*(this->kg[1] + this->q[1]) + 2*p0*(this->q[0] - this->q[1])*(this->q[0] + this->q[1])))*this->q[3] - F1NC*this->q[0]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] + 4*this->mn2 + 4*pow(p0,2) - 4*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1] * this->q[1])*this->q[3] * this->q[3] + 2*F1NC*this->kg[3]*(-2*p0 + this->q[0])*pow(this->q[3],3) - F1NC*this->q[0]*pow(this->q[3],4) + 8*F2NC*this->mn2*this->q[0]*(-2*p0*this->q[0] + this->q[0] * this->q[0] - this->kg[3]*this->q[3])))/(16.*pow(this->mn,4))
            ;

    tr[
            1
    ][
            2
    ]=
            (F1*this->kg[1]*(std::complex<double>(0,8)*FANC*this->mn2*(-(this->kg[3]*this->q[1]) + this->kg[1]*this->q[3]) + 2*F1NC*this->kg[2]*(2*p0 - this->q[0])*(this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3]) + F2NC*this->kg[2]*this->q[0]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 4*this->mn2 - 4*pow(p0,2) + 4*p0*this->q[0] - 2*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3])) + 2*F2*(std::complex<double>(0,-2)*FANC*this->mn2*(this->kg[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] + 4*this->mn2 - 4*pow(p0,2) + 4*p0*this->q[0] - 2*this->q[0] * this->q[0] - this->q[1] * this->q[1]) + 2*this->q[0]*(-2*p0 + this->q[0])*this->q[3] - this->kg[3]*this->q[3] * this->q[3]) + this->kg[2]*this->q[1]*(4*F2NC*this->mn2*this->q[0] + F1NC*(2*p0 - this->q[0])*(-this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3]))))/(16.*pow(this->mn,4))
            ;

    tr[
            1
    ][
            3
    ]=
            (F1*this->kg[1]*(2.0*(std::complex<double>(0,4)*FANC*this->kg[2]*this->mn2*this->q[1] + F1NC*this->kg[3]*(2*p0 - this->q[0])*(this->q[0] - this->q[1])*(this->q[0] + this->q[1]) + F1NC*((this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3])*this->q[0] + this->kg[1]*(2*p0 - this->q[0])*this->q[1])*this->q[3]) + F2NC*this->q[0]*(this->kg[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 4*this->mn2 - 4*pow(p0,2) + 4*p0*this->q[0] - 2*this->q[0] * this->q[0] + this->q[1] * this->q[1]) + 2*(-2*p0*this->q[0] + this->q[0] * this->q[0] - this->kg[1]*this->q[1])*this->q[3] - this->kg[3]*this->q[3] * this->q[3])) + F2*(8*F2NC*this->kg[3]*this->mn2*this->q[0]*this->q[1] + std::complex<double>(0,4)*FANC*this->kg[2]*this->mn2*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] + 4*this->mn2 - 4*pow(p0,2) + 4*p0*this->q[0] - 2*this->q[0] * this->q[0] - this->q[1] * this->q[1] + this->q[3] * this->q[3]) + F1NC*(-2*this->kg[3]*(2*p0 - this->q[0])*(this->q[0] - this->q[1])*this->q[1]*(this->q[0] + this->q[1]) + this->q[0]*(2*this->kg[1]*(2*p0 - this->q[0])*this->q[0] + this->kg[1] * this->kg[1]*this->q[1] - (this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 4*(this->mn2 + pow(p0,2) - p0*this->q[0]))*this->q[1] + pow(this->q[1],3))*this->q[3] + 2*this->kg[3]*(this->kg[1]*this->q[0] + 2*p0*this->q[1] - this->q[0]*this->q[1])*this->q[3] * this->q[3] + this->q[0]*this->q[1]*pow(this->q[3],3))))/(16.*pow(this->mn,4))
            ;

    tr[
            2
    ][
            0
    ]=
            (this->kg[2]*(2*F1NC*F2*(2*p0*this->q[0] - this->q[0] * this->q[0] + this->kg[1]*this->q[1] + this->kg[3]*this->q[3])*(this->q[1] * this->q[1] + this->q[3] * this->q[3]) + F1*(this->q[1]*(this->kg[1]*(F2NC*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 4*(this->mn2 + pow(p0,2))) + 4*(F1NC + F2NC)*p0*this->q[0] - 2*(F1NC + F2NC)*this->q[0] * this->q[0]) + 2*(F1NC*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3]) + F2NC*this->q[0]*(-2*p0 + this->q[0]))*this->q[1] - F2NC*this->kg[1]*this->q[1] * this->q[1]) + this->kg[3]*(2*F1NC*(2*p0 - this->q[0])*this->q[0] + F2NC*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 4*this->mn2 - 4*pow(p0,2) + 4*p0*this->q[0] - 2*this->q[0] * this->q[0] - this->q[1] * this->q[1]))*this->q[3] + (2*F1NC*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3]) + 2*F2NC*this->q[0]*(-2*p0 + this->q[0]) - F2NC*this->kg[1]*this->q[1])*this->q[3] * this->q[3] - F2NC*this->kg[3]*pow(this->q[3],3))))/(16.*pow(this->mn,4))
            ;

    tr[
            2
    ][
            1
    ]=
            (2*F1NC*F2*this->kg[2]*this->q[0]*this->q[1]*(2*p0*this->q[0] - this->q[0] * this->q[0] + this->kg[1]*this->q[1] + this->kg[3]*this->q[3]) + std::complex<double>(0,4)*F2*FANC*this->mn2*(this->kg[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] + 4*this->mn2 - 4*pow(p0,2) + 4*p0*this->q[0] - 2*this->q[0] * this->q[0] + this->q[1] * this->q[1]) + 2*(-2*p0*this->q[0] + this->q[0] * this->q[0] - this->kg[1]*this->q[1])*this->q[3] - this->kg[3]*this->q[3] * this->q[3]) + F1*this->kg[2]*(this->q[0]*(this->kg[1]*(F2NC*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 4*(this->mn2 + pow(p0,2))) + 4*(F1NC + F2NC)*p0*this->q[0] - 2*(F1NC + F2NC)*this->q[0] * this->q[0]) + 2*(F1NC*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3]) + F2NC*this->q[0]*(-2*p0 + this->q[0]))*this->q[1] - F2NC*this->kg[1]*this->q[1] * this->q[1]) - 2.0*(std::complex<double>(0,4)*FANC*this->kg[2]*this->mn2 + this->kg[3]*(-2*F1NC*p0 + (F1NC + F2NC)*this->q[0])*this->q[1])*this->q[3] + this->kg[1]*(F2NC*this->q[0] + 2*F1NC*(-2*p0 + this->q[0]))*this->q[3] * this->q[3]))/(16.*pow(this->mn,4))
            ;

    tr[
            2
    ][
            2
    ]=
            (-8*F2*F2NC*this->mn2*this->q[0]*(2*p0*this->q[0] - this->q[0] * this->q[0] + this->kg[1]*this->q[1] + this->kg[3]*this->q[3]) + F1NC*F2*(this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3])*(this->kg[1] * this->kg[1]*this->q[0] + this->kg[1]*(4*p0*this->q[1] - 2*this->q[0]*this->q[1]) + this->q[0]*(this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] + 4*(this->mn2 + pow(p0,2) - p0*this->q[0]) + this->q[1] * this->q[1]) - 2*this->kg[3]*(-2*p0 + this->q[0])*this->q[3] + this->q[0]*this->q[3] * this->q[3]) + F1*this->kg[2]*(std::complex<double>(0,8)*FANC*this->mn2*(-(this->kg[3]*this->q[1]) + this->kg[1]*this->q[3]) + 2*F1NC*this->kg[2]*(2*p0 - this->q[0])*(this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3]) + F2NC*this->kg[2]*this->q[0]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 4*this->mn2 - 4*pow(p0,2) + 4*p0*this->q[0] - 2*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3])))/(16.*pow(this->mn,4))
            ;

    tr[
            2
    ][
            3
    ]=
            (2*F1NC*F2*this->kg[2]*this->q[0]*this->q[3]*(2*p0*this->q[0] - this->q[0] * this->q[0] + this->kg[1]*this->q[1] + this->kg[3]*this->q[3]) + 2*F1*this->kg[2]*(std::complex<double>(0,4)*FANC*this->kg[2]*this->mn2*this->q[1] + F1NC*this->kg[3]*(2*p0 - this->q[0])*(this->q[0] - this->q[1])*(this->q[0] + this->q[1]) + F1NC*((this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3])*this->q[0] + this->kg[1]*(2*p0 - this->q[0])*this->q[1])*this->q[3]) + F1*F2NC*this->kg[2]*this->q[0]*(this->kg[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 4*this->mn2 - 4*pow(p0,2) + 4*p0*this->q[0] - 2*this->q[0] * this->q[0] + this->q[1] * this->q[1]) + 2*(-2*p0*this->q[0] + this->q[0] * this->q[0] - this->kg[1]*this->q[1])*this->q[3] - this->kg[3]*this->q[3] * this->q[3]) - std::complex<double>(0,4)*F2*FANC*this->mn2*(pow(this->kg[1],3) + 2*this->q[1]*(-2*p0*this->q[0] + this->q[0] * this->q[0] - this->kg[3]*this->q[3]) + this->kg[1]*(this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] + 4*this->mn2 - 4*pow(p0,2) + 4*p0*this->q[0] - 2*this->q[0] * this->q[0] - this->q[1] * this->q[1] + this->q[3] * this->q[3])))/(16.*pow(this->mn,4))
            ;

    tr[
            3
    ][
            0
    ]=
            (F1*this->kg[3]*(this->q[1]*(this->kg[1]*(F2NC*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 4*(this->mn2 + pow(p0,2))) + 4*(F1NC + F2NC)*p0*this->q[0] - 2*(F1NC + F2NC)*this->q[0] * this->q[0]) + 2*(F1NC*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3]) + F2NC*this->q[0]*(-2*p0 + this->q[0]))*this->q[1] - F2NC*this->kg[1]*this->q[1] * this->q[1]) + this->kg[3]*(2*F1NC*(2*p0 - this->q[0])*this->q[0] + F2NC*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 4*this->mn2 - 4*pow(p0,2) + 4*p0*this->q[0] - 2*this->q[0] * this->q[0] - this->q[1] * this->q[1]))*this->q[3] + (2*F1NC*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3]) + 2*F2NC*this->q[0]*(-2*p0 + this->q[0]) - F2NC*this->kg[1]*this->q[1])*this->q[3] * this->q[3] - F2NC*this->kg[3]*pow(this->q[3],3)) + F2*(8*F2NC*this->mn2*this->q[0]*(-2*p0 + this->q[0])*this->q[3] + F1NC*(2*this->kg[3]*this->q[1] * this->q[1]*(2*p0*this->q[0] - this->q[0] * this->q[0] + this->kg[1]*this->q[1]) + (this->q[0] * this->q[0]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] + 4*(this->mn2 + pow(p0,2) - p0*this->q[0])) + (-2*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + this->q[0] * this->q[0])*this->q[1] * this->q[1])*this->q[3] + 2*this->kg[3]*(2*p0*this->q[0] - this->q[0] * this->q[0] + this->kg[1]*this->q[1])*this->q[3] * this->q[3] + (-2*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + this->q[0] * this->q[0])*pow(this->q[3],3))))/(16.*pow(this->mn,4))
            ;

    tr[
            3
    ][
            1
    ]=
            (F1*this->kg[3]*(this->q[0]*(this->kg[1]*(F2NC*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 4*(this->mn2 + pow(p0,2))) + 4*(F1NC + F2NC)*p0*this->q[0] - 2*(F1NC + F2NC)*this->q[0] * this->q[0]) + 2*(F1NC*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3]) + F2NC*this->q[0]*(-2*p0 + this->q[0]))*this->q[1] - F2NC*this->kg[1]*this->q[1] * this->q[1]) - 2.0*(std::complex<double>(0,4)*FANC*this->kg[2]*this->mn2 + this->kg[3]*(-2*F1NC*p0 + (F1NC + F2NC)*this->q[0])*this->q[1])*this->q[3] + this->kg[1]*(F2NC*this->q[0] + 2*F1NC*(-2*p0 + this->q[0]))*this->q[3] * this->q[3]) + F2*(8*F2NC*this->kg[1]*this->mn2*this->q[0]*this->q[3] - std::complex<double>(0,4)*FANC*this->kg[2]*this->mn2*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] + 4*this->mn2 - 4*pow(p0,2) + 4*p0*this->q[0] - 2*this->q[0] * this->q[0] + this->q[1] * this->q[1] - this->q[3] * this->q[3]) + F1NC*(2*this->kg[3]*this->q[0]*this->q[1]*(2*p0*this->q[0] - this->q[0] * this->q[0] + this->kg[1]*this->q[1]) + this->kg[3] * this->kg[3]*this->q[0]*this->q[1]*this->q[3] + this->q[3]*(-(this->kg[1] * this->kg[1]*this->q[0]*this->q[1]) - 2*this->kg[1]*(2*p0 - this->q[0])*(this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3]) + this->q[0]*this->q[1]*(-this->kg[2] * this->kg[2] + 4*this->mn2 + 4*p0*(p0 - this->q[0]) + this->q[1] * this->q[1] + this->q[3] * this->q[3])))))/(16.*pow(this->mn,4))
            ;

    tr[
            3
    ][
            2
    ]=
            (F1*this->kg[3]*(std::complex<double>(0,8)*FANC*this->mn2*(-(this->kg[3]*this->q[1]) + this->kg[1]*this->q[3]) + 2*F1NC*this->kg[2]*(2*p0 - this->q[0])*(this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3]) + F2NC*this->kg[2]*this->q[0]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 4*this->mn2 - 4*pow(p0,2) + 4*p0*this->q[0] - 2*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3])) + 2*F2*(std::complex<double>(0,2)*FANC*this->mn2*(pow(this->kg[1],3) + 2*this->q[0]*(-2*p0 + this->q[0])*this->q[1] + this->kg[1]*(this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] + 4*this->mn2 - 4*pow(p0,2) + 4*p0*this->q[0] - 2*this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3])) + this->kg[2]*this->q[3]*(4*F2NC*this->mn2*this->q[0] + F1NC*(2*p0 - this->q[0])*(-this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3]))))/(16.*pow(this->mn,4))
            ;

    tr[
            3
    ][
            3
    ]=
            (F1*this->kg[3]*(2.0*(std::complex<double>(0,4)*FANC*this->kg[2]*this->mn2*this->q[1] + F1NC*this->kg[3]*(2*p0 - this->q[0])*(this->q[0] - this->q[1])*(this->q[0] + this->q[1]) + F1NC*((this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3])*this->q[0] + this->kg[1]*(2*p0 - this->q[0])*this->q[1])*this->q[3]) + F2NC*this->q[0]*(this->kg[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] - 4*this->mn2 - 4*pow(p0,2) + 4*p0*this->q[0] - 2*this->q[0] * this->q[0] + this->q[1] * this->q[1]) + 2*(-2*p0*this->q[0] + this->q[0] * this->q[0] - this->kg[1]*this->q[1])*this->q[3] - this->kg[3]*this->q[3] * this->q[3])) - F2*(8*F2NC*this->mn2*this->q[0]*(2*p0*this->q[0] - this->q[0] * this->q[0] + this->kg[1]*this->q[1]) + std::complex<double>(0,8)*FANC*this->kg[2]*this->mn2*this->q[1]*this->q[3] + F1NC*(-((this->q[0] - this->q[1])*(this->q[0] + this->q[1])*(this->q[0]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3] + 4*(this->mn2 + pow(p0,2) - p0*this->q[0])) - 2*this->kg[1]*(-2*p0 + this->q[0])*this->q[1] + this->q[0]*this->q[1] * this->q[1])) + 2*this->kg[3]*this->q[0]*(-2*p0*this->q[0] + this->q[0] * this->q[0] - this->kg[1]*this->q[1])*this->q[3] + (2*this->kg[1] * this->kg[1]*this->q[0] + 2*this->kg[2] * this->kg[2]*this->q[0] - pow(this->q[0],3) + 4*this->kg[1]*p0*this->q[1] - 2*this->kg[1]*this->q[0]*this->q[1] + this->q[0]*this->q[1] * this->q[1])*this->q[3] * this->q[3])))/(16.*pow(this->mn,4))
            ;

}
