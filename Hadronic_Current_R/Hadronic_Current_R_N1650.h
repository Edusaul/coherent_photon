//
// Created by eduardo on 5/07/19.
//

#ifndef COHERENT_PHOTON_HADRONIC_CURRENT_R_N1650_H
#define COHERENT_PHOTON_HADRONIC_CURRENT_R_N1650_H


#include "Hadronic_Current_tr_J12.h"

class Hadronic_Current_R_N1650 : public Hadronic_Current_tr_J12 {

public:

    explicit Hadronic_Current_R_N1650(Nuclear_FF *nuclearFf);

    virtual ~Hadronic_Current_R_N1650();

};


#endif //COHERENT_PHOTON_HADRONIC_CURRENT_R_N1650_H
