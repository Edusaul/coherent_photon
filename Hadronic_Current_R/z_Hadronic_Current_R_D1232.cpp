//
// Created by eduardo on 8/07/19.
//

#include <Parameters_GeV.h>
#include <z_Form_Factors_D1232.h>
#include "z_Hadronic_Current_R_D1232.h"

z_Hadronic_Current_R_D1232::z_Hadronic_Current_R_D1232(Nuclear_FF *nuclearFf) : Hadronic_Current_tr_J32(nuclearFf) {
    this->mRes = Param::mDelta;
    this->mRes_2 = this->mRes * this->mRes;
    this->ff_Res = new z_Form_Factors_D1232();
    this->gamma = 0.118;

    this->parity = "positive";
    this->isospin_by_2 = 3;

    this->set_EM_FF();
}

z_Hadronic_Current_R_D1232::~z_Hadronic_Current_R_D1232() {
    delete this->ff_Res;
}
