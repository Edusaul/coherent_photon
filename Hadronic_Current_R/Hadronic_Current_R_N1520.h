//
// Created by edusaul on 28/04/19.
//

#ifndef COHERENT_PHOTON_HADRONIC_CURRENT_R_N1520_H
#define COHERENT_PHOTON_HADRONIC_CURRENT_R_N1520_H


#include "Hadronic_Current_tr_J32.h"

class Hadronic_Current_R_N1520 : public Hadronic_Current_tr_J32 {

public:
    explicit Hadronic_Current_R_N1520(Nuclear_FF *nuclearFf);

    virtual ~Hadronic_Current_R_N1520();
    
};


#endif //COHERENT_PHOTON_HADRONIC_CURRENT_R_N1520_H
