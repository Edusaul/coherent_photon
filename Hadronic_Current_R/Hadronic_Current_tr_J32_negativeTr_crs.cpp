#include "Hadronic_Current_tr_J32.h"

void Hadronic_Current_tr_J32::set_tr_crs_negative(Array4x4 &tr) {
    tr[0][0] = (4*C3vNC*C4v*this->mRes*this->mn*(4*this->mRes_2*this->mn*(p0 - this->q[0])*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) + 
       this->mn*(p0 - this->q[0])*(-(this->q[0] * this->q[0]*
             (this->q[1] * this->q[1] - 3*this->q[1]*this->kg[1] + this->q[3]*(this->q[3] - 3*this->kg[3]))) - 
          8*p0*this->q[0]*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) - 
          (this->q[1]*this->kg[1] + this->q[3]*this->kg[3])*(-4*pow(p0,2) + this->q[1]*(this->q[1] + this->kg[1]) + 
             this->q[3]*(this->q[3] + this->kg[3]))) + 
       this->mRes*(8*pow(p0,3)*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) - 
          16*pow(p0,2)*this->q[0]*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) - 
          3*this->q[0]*(this->q[0] * this->q[0]*this->q[1] * this->q[1] - this->q[1] * this->q[1]*this->kg[1] * this->kg[1] + 
             this->q[3] * this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) - 2*this->q[1]*this->q[3]*this->kg[1]*this->kg[3])  
+ p0*(-2*pow(this->q[1],3)*this->kg[1] - 2*this->q[1]*this->q[3]*this->kg[1]*(this->q[3] + 5*this->kg[3]) + 
             this->q[3] * this->q[3]*(5*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) - 2*this->q[3]*this->kg[3]) - 
             this->q[1] * this->q[1]*(5*this->kg[1] * this->kg[1] + 2*this->q[3]*this->kg[3]) + 
             this->q[0] * this->q[0]*(this->q[1] * this->q[1] - 4*this->q[3] * this->q[3] + 6*this->q[1]*this->kg[1] + 
                6*this->q[3]*this->kg[3])))) + 
    C4v*C5vNC*this->mRes*(-8*pow(p0,3)*(this->q[1] * this->q[1] + this->q[3] * this->q[3])*
        (-this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) + 
       8*this->mRes*this->mn*(p0 - this->q[0])*(pow(this->q[1],3)*this->kg[1] + this->q[1]*this->q[3] * this->q[3]*this->kg[1] - 
          this->q[1] * this->q[1]*this->kg[1] * this->kg[1] + this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
          this->q[3] * this->q[3]*this->kg[2] * this->kg[2] + 
          this->q[3]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] - 2*this->q[1]*this->kg[1])*this->kg[3] + 
          this->q[0] * this->q[0]*(this->q[1] * this->q[1] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3]) + 
          4*pow(p0,2)*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) - 4*p0*this->q[0]*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]))  
- 4*pow(p0,2)*this->q[0]*(-5*pow(this->q[1],3)*this->kg[1] + 
          this->q[0] * this->q[0]*(5*this->q[1] * this->q[1] - this->q[1]*this->kg[1] + this->q[3]*(6*this->q[3] - this->kg[3])) + 
          this->q[1]*this->q[3]*this->kg[1]*(-5*this->q[3] + 2*this->kg[3]) + 
          this->q[1] * this->q[1]*(this->kg[1] * this->kg[1] - 5*this->q[3]*this->kg[3]) - 
          this->q[3] * this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + 5*this->q[3]*this->kg[3])) + 
       2*p0*(pow(this->q[0],4)*(7*this->q[1] * this->q[1] - 4*this->q[1]*this->kg[1] + 
             this->q[3]*(11*this->q[3] - 4*this->kg[3])) + 
          pow(this->q[1] * this->q[1] + this->q[3] * this->q[3],2)*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) + 
          this->q[0] * this->q[0]*(pow(this->q[1],4) - 9*pow(this->q[1],3)*this->kg[1] + 
             this->q[3] * this->q[3]*(-4*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + 
                this->q[3]*(this->q[3] - 9*this->kg[3])) + this->q[1]*this->q[3]*this->kg[1]*(-9*this->q[3] + 8*this->kg[3]) + 
             this->q[1] * this->q[1]*(2*this->q[3] * this->q[3] + 4*this->kg[1] * this->kg[1] - 9*this->q[3]*this->kg[3]))) - 
       this->q[0]*(pow(this->q[0],4)*(3*this->q[1]*(this->q[1] - this->kg[1]) + this->q[3]*(8*this->q[3] - 3*this->kg[3])) + 
          (this->q[1] * this->q[1] + this->q[3] * this->q[3])*
           (pow(this->q[1],3)*this->kg[1] + this->q[1]*this->q[3]*this->kg[1]*(this->q[3] - 2*this->kg[3]) + 
             this->q[1] * this->q[1]*(-this->kg[1] * this->kg[1] + this->q[3]*this->kg[3]) + 
             this->q[3] * this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->q[3]*this->kg[3])) + 
          this->q[0] * this->q[0]*(pow(this->q[1],4) - 6*pow(this->q[1],3)*this->kg[1] + 
             2*this->q[1]*this->q[3]*this->kg[1]*(-3*this->q[3] + 5*this->kg[3]) + 
             this->q[1] * this->q[1]*(this->q[3] * this->q[3] + 5*this->kg[1] * this->kg[1] - 6*this->q[3]*this->kg[3]) - 
             this->q[3] * this->q[3]*(5*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + 6*this->q[3]*this->kg[3]))) + 
       4*this->mRes_2*(8*pow(p0,3)*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) - 
          8*pow(p0,2)*this->q[0]*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) + 
          this->q[0]*(-(pow(this->q[1],3)*this->kg[1]) + 
             this->q[0] * this->q[0]*(this->q[1] * this->q[1] - this->q[1]*this->kg[1] + this->q[3]*(2*this->q[3] - this->kg[3])) - 
             this->q[1]*this->q[3]*this->kg[1]*(this->q[3] - 2*this->kg[3]) + 
             this->q[1] * this->q[1]*(this->kg[1] * this->kg[1] - this->q[3]*this->kg[3]) - 
             this->q[3] * this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->q[3]*this->kg[3])) + 
          2*p0*(this->q[0] * this->q[0]*(this->q[1] * this->q[1] - this->q[1]*this->kg[1] - this->q[3]*(this->q[3] + this->kg[3])) + 
             2*(pow(this->q[1],3)*this->kg[1] + this->q[1]*this->q[3]*this->kg[1]*(this->q[3] - 2*this->kg[3]) + 
                this->q[1] * this->q[1]*(-this->kg[1] * this->kg[1] + this->q[3]*this->kg[3]) + 
                this->q[3] * this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->q[3]*this->kg[3]))))) + 
    2*C5v*(C3vNC*this->mn*(8*pow(this->mRes,3)*this->mn*p0*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) + 
          this->q[0]*(2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3])*
           (4*pow(p0,2)*(this->q[1] * this->q[1] - this->q[1]*this->kg[1] + this->q[3]*(this->q[3] - this->kg[3])) - 
             8*p0*this->q[0]*(this->q[1] * this->q[1] - this->q[1]*this->kg[1] + this->q[3]*(this->q[3] - this->kg[3])) + 
             this->q[0] * this->q[0]*(5*this->q[1] * this->q[1] + 5*this->q[3] * this->q[3] - 3*this->q[1]*this->kg[1] - 
                3*this->q[3]*this->kg[3]) - (this->q[1] * this->q[1] + this->q[3] * this->q[3])*
              (this->q[1]*(this->q[1] + this->kg[1]) + this->q[3]*(this->q[3] + this->kg[3]))) + 
          2*this->mRes*this->mn*(4*pow(p0,3)*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) - 
             8*pow(p0,2)*this->q[0]*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) - 
             2*this->q[0]*(this->q[0] * this->q[0] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3])*
              (this->q[1]*(this->q[1] + this->kg[1]) + this->q[3]*(this->q[3] + this->kg[3])) - 
             p0*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3])*(this->q[1]*(this->q[1] + this->kg[1]) + this->q[3]*(this->q[3] + this->kg[3])) + 
             p0*this->q[0] * this->q[0]*
              (3*this->q[1] * this->q[1] + 7*this->q[1]*this->kg[1] + this->q[3]*(3*this->q[3] + 7*this->kg[3]))) + 
          2*this->mRes_2*(2*this->q[0]*(this->q[1] * this->q[1] - this->q[1]*this->kg[1] + this->q[3]*(this->q[3] - this->kg[3]))*
              (this->q[0] * this->q[0] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3]) + 
             8*pow(p0,3)*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) - 
             8*pow(p0,2)*this->q[0]*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) + 
             p0*(-2*pow(this->q[1],3)*this->kg[1] - 2*this->q[1]*this->q[3]*this->kg[1]*(this->q[3] + 5*this->kg[3]) + 
                this->q[3] * this->q[3]*(5*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) - 
                   2*this->q[3]*this->kg[3]) - this->q[1] * this->q[1]*(5*this->kg[1] * this->kg[1] + 2*this->q[3]*this->kg[3]) + 
                this->q[0] * this->q[0]*(this->q[1] * this->q[1] - 4*this->q[3] * this->q[3] + 6*this->q[1]*this->kg[1] + 
                   6*this->q[3]*this->kg[3])))) + 
       C5vNC*(8*pow(this->mRes,3)*p0*
           (pow(this->q[1],3)*this->kg[1] + this->q[1]*this->q[3] * this->q[3]*this->kg[1] - 
             this->q[1] * this->q[1]*this->kg[1] * this->kg[1] + this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
             this->q[3] * this->q[3]*this->kg[2] * this->kg[2] + 
             this->q[3]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] - 2*this->q[1]*this->kg[1])*this->kg[3] + 
             2*pow(p0,2)*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) + 
             this->q[0] * this->q[0]*(this->q[1]*(-this->q[1] + this->kg[1]) + this->q[3]*(-2*this->q[3] + this->kg[3]))) + 
          4*this->mRes_2*this->mn*(p0 - this->q[0])*
           (pow(this->q[1],3)*this->kg[1] + this->q[1]*this->q[3] * this->q[3]*this->kg[1] - 
             this->q[1] * this->q[1]*this->kg[1] * this->kg[1] + this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
             this->q[3] * this->q[3]*this->kg[2] * this->kg[2] + 
             this->q[3]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] - 2*this->q[1]*this->kg[1])*this->kg[3] + 
             4*pow(p0,2)*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) + 
             this->q[0] * this->q[0]*(this->q[1]*(-this->q[1] + this->kg[1]) + this->q[3]*(-2*this->q[3] + this->kg[3]))) + 
          this->mRes*p0*(pow(this->q[0],4)*
              (8*this->q[1] * this->q[1] - 7*this->q[1]*this->kg[1] + this->q[3]*(16*this->q[3] - 7*this->kg[3])) + 
             pow(this->q[1] * this->q[1] + this->q[3] * this->q[3],2)*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) + 
             4*pow(p0,2)*(this->q[0] * this->q[0]*
                 (6*this->q[1] * this->q[1] - this->q[1]*this->kg[1] + this->q[3]*(6*this->q[3] - this->kg[3])) - 
                (this->q[1] * this->q[1] + this->q[3] * this->q[3])*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3])) - 
             4*p0*this->q[0]*(-4*pow(this->q[1],3)*this->kg[1] + 2*this->q[1]*this->q[3]*this->kg[1]*(-2*this->q[3] + this->kg[3]) + 
                this->q[0] * this->q[0]*(7*this->q[1] * this->q[1] + 8*this->q[3] * this->q[3] - 4*this->q[1]*this->kg[1] - 
                   4*this->q[3]*this->kg[3]) + this->q[1] * this->q[1]*(this->kg[1] * this->kg[1] - 4*this->q[3]*this->kg[3]) - 
                this->q[3] * this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + 4*this->q[3]*this->kg[3])) - 
             2*this->q[0] * this->q[0]*(5*pow(this->q[1],3)*this->kg[1] + 
                this->q[1]*this->q[3]*this->kg[1]*(5*this->q[3] - 8*this->kg[3]) + 
                this->q[1] * this->q[1]*(-4*this->kg[1] * this->kg[1] + 5*this->q[3]*this->kg[3]) + 
                this->q[3] * this->q[3]*(4*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + 5*this->q[3]*this->kg[3])))  
+ 4*this->mn*(p0 - this->q[0])*this->q[0]*(4*pow(p0,2)*this->q[0]*(this->q[1] * this->q[1] + this->q[3] * this->q[3]) + 
             this->q[0]*(-(pow(this->q[1],3)*this->kg[1]) + 
                this->q[0] * this->q[0]*(this->q[1] * this->q[1] - this->q[1]*this->kg[1] + this->q[3]*(2*this->q[3] - this->kg[3])) - 
                this->q[1]*this->q[3]*this->kg[1]*(this->q[3] - 2*this->kg[3]) + 
                this->q[1] * this->q[1]*(this->kg[1] * this->kg[1] - this->q[3]*this->kg[3]) - 
                this->q[3] * this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->q[3]*this->kg[3])) + 
             2*p0*((this->q[1] * this->q[1] + this->q[3] * this->q[3])*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) + 
                this->q[0] * this->q[0]*(this->q[1]*(-2*this->q[1] + this->kg[1]) + this->q[3]*(-2*this->q[3] + this->kg[3])))))) + 
    2*C3v*this->mn*(4*C3vNC*this->mn*(4*pow(this->mRes,3)*p0*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) + 
          2*this->mRes*(p0 - this->q[0])*(this->q[0] * this->q[0]*this->q[1] * this->q[1] - 
             this->q[1] * this->q[1]*this->kg[1] * this->kg[1] + 
             this->q[3] * this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) - 2*this->q[1]*this->q[3]*this->kg[1]*this->kg[3])  
+ this->mn*(p0 - this->q[0])*(-(this->q[0] * this->q[0]*
                (this->q[1] * this->q[1] - 3*this->q[1]*this->kg[1] + this->q[3]*(this->q[3] - 3*this->kg[3]))) - 
             8*p0*this->q[0]*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) - 
             (this->q[1]*this->kg[1] + this->q[3]*this->kg[3])*
              (-4*pow(p0,2) + this->q[1]*(this->q[1] + this->kg[1]) + this->q[3]*(this->q[3] + this->kg[3])))) + 
       C5vNC*(8*pow(this->mRes,3)*this->mn*p0*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) + 
          (2*p0*(this->q[1] * this->q[1] + this->q[3] * this->q[3]) - 
             this->q[0]*(this->q[1] * this->q[1] - this->q[1]*this->kg[1] + this->q[3]*(this->q[3] - this->kg[3])))*
           (3*pow(this->q[0],4) + this->q[1]*
              (this->kg[1]*(this->q[3] * this->q[3] + this->q[1]*(this->q[1] + this->kg[1])) + this->q[1]*this->kg[2] * this->kg[2]) + 
             this->q[3]*(this->q[1] * this->q[1] + this->q[3] * this->q[3])*this->kg[3] + 
             this->q[1] * this->q[1]*this->kg[3] * this->kg[3] + 
             this->q[0] * this->q[0]*(this->q[3] * this->q[3] - 5*this->q[1]*this->kg[1] - 5*this->q[3]*this->kg[3]) + 
             4*pow(p0,2)*(this->q[0] * this->q[0] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3]) + 
             8*p0*this->q[0]*(-this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3])) + 
          2*this->mRes*this->mn*(4*pow(p0,3)*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) - 
             8*pow(p0,2)*this->q[0]*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) - 
             2*this->q[0]*(this->q[1] * this->q[1] - this->q[1]*this->kg[1] + this->q[3]*(this->q[3] - this->kg[3]))*
              (this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) + 
             p0*((3*this->q[1] * this->q[1] + 3*this->q[3] * this->q[3] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3])*
                 (this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) + 
                3*this->q[0] * this->q[0]*(this->q[1]*(this->q[1] + this->kg[1]) + this->q[3]*(this->q[3] + this->kg[3])))) + 
          2*this->mRes_2*(2*this->q[0]*(this->q[1] * this->q[1] - this->q[1]*this->kg[1] + this->q[3]*(this->q[3] - this->kg[3]))*
              (this->q[0] * this->q[0] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3]) + 
             8*pow(p0,3)*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) - 
             8*pow(p0,2)*this->q[0]*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) + 
             p0*(6*pow(this->q[1],3)*this->kg[1] + 2*this->q[1]*this->q[3]*this->kg[1]*(3*this->q[3] - 5*this->kg[3]) + 
                this->q[1] * this->q[1]*(-5*this->kg[1] * this->kg[1] + 6*this->q[3]*this->kg[3]) + 
                this->q[3] * this->q[3]*(5*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + 6*this->q[3]*this->kg[3]) + 
                this->q[0] * this->q[0]*(this->q[1] * this->q[1] - 2*this->q[1]*this->kg[1] - 2*this->q[3]*(2*this->q[3] + this->kg[3]))))) 
) + C4vNC*this->mRes*(8*C4v*this->mRes*(this->mRes*
           (-(this->q[0] * this->q[0]*(this->q[0]*this->q[1] * this->q[1] + 2*p0*this->q[3] * this->q[3])) - 
             p0*this->q[1]*(-4*pow(p0,2) + 8*p0*this->q[0] - 3*this->q[0] * this->q[0] + 
                this->q[1] * this->q[1] + this->q[3] * this->q[3])*this->kg[1] + 
             (-2*p0 + this->q[0])*(this->q[1] - this->q[3])*(this->q[1] + this->q[3])*this->kg[1] * this->kg[1] + 
             (2*p0 - this->q[0])*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] - 
             this->q[3]*(-4*pow(p0,3) + 8*pow(p0,2)*this->q[0] - 2*this->q[0]*this->q[1]*this->kg[1] + 
                p0*(-3*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] + 
                   4*this->q[1]*this->kg[1]))*this->kg[3]) + 
          this->mn*(p0 - this->q[0])*(-(pow(this->q[1],3)*this->kg[1]) - this->q[1]*this->q[3] * this->q[3]*this->kg[1] - 
             this->q[1] * this->q[1]*this->kg[1] * this->kg[1] + this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
             this->q[3] * this->q[3]*this->kg[2] * this->kg[2] - 
             this->q[0] * this->q[0]*(this->q[1] * this->q[1] - 3*this->q[1]*this->kg[1] + this->q[3]*(2*this->q[3] - 3*this->kg[3])) - 
             this->q[3]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1])*this->kg[3] + 
             4*pow(p0,2)*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) - 
             8*p0*this->q[0]*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]))) + 
       4*C3v*this->mn*(4*this->mRes_2*this->mn*(p0 - this->q[0])*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) + 
          this->mn*(p0 - this->q[0])*(-(this->q[0] * this->q[0]*
                (this->q[1] * this->q[1] - 3*this->q[1]*this->kg[1] + this->q[3]*(this->q[3] - 3*this->kg[3]))) - 
             8*p0*this->q[0]*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) - 
             (this->q[1]*this->kg[1] + this->q[3]*this->kg[3])*
              (-4*pow(p0,2) + this->q[1]*(this->q[1] + this->kg[1]) + this->q[3]*(this->q[3] + this->kg[3]))) + 
          this->mRes*(8*pow(p0,3)*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) - 
             16*pow(p0,2)*this->q[0]*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) - 
             3*this->q[0]*(this->q[0] * this->q[0]*this->q[1] * this->q[1] - this->q[1] * this->q[1]*this->kg[1] * this->kg[1] + 
                this->q[3] * this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) - 
                2*this->q[1]*this->q[3]*this->kg[1]*this->kg[3]) + 
             p0*(-2*pow(this->q[1],3)*this->kg[1] - 2*this->q[1]*this->q[3]*this->kg[1]*(this->q[3] + 5*this->kg[3]) + 
                this->q[3] * this->q[3]*(5*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) - 2*this->q[3]*this->kg[3]) - 
                this->q[1] * this->q[1]*(5*this->kg[1] * this->kg[1] + 2*this->q[3]*this->kg[3]) + 
                this->q[0] * this->q[0]*(this->q[1] * this->q[1] - 4*this->q[3] * this->q[3] + 6*this->q[1]*this->kg[1] + 
                   6*this->q[3]*this->kg[3])))) + 
       C5v*(8*this->mRes*this->mn*(p0 - this->q[0])*
           (-(pow(this->q[1],3)*this->kg[1]) - this->q[1]*this->q[3] * this->q[3]*this->kg[1] - 
             this->q[1] * this->q[1]*this->kg[1] * this->kg[1] + this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
             this->q[3] * this->q[3]*this->kg[2] * this->kg[2] - 
             this->q[3]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1])*this->kg[3] + 
             4*pow(p0,2)*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) - 
             4*p0*this->q[0]*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) + 
             this->q[0] * this->q[0]*(this->q[1]*(this->q[1] + this->kg[1]) + this->q[3]*this->kg[3])) + 
          4*this->mRes_2*(8*pow(p0,3)*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) - 
             8*pow(p0,2)*this->q[0]*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) + 
             2*p0*(-(pow(this->q[1],3)*this->kg[1]) + 
                this->q[0] * this->q[0]*(this->q[1] * this->q[1] + 2*this->q[1]*this->kg[1] - this->q[3]*(this->q[3] - 2*this->kg[3])) - 
                this->q[1]*this->q[3]*this->kg[1]*(this->q[3] + 4*this->kg[3]) + 
                this->q[3] * this->q[3]*(2*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) - this->q[3]*this->kg[3]) - 
                this->q[1] * this->q[1]*(2*this->kg[1] * this->kg[1] + this->q[3]*this->kg[3])) + 
             this->q[0]*(-(pow(this->q[1],3)*this->kg[1]) + 
                this->q[0] * this->q[0]*(this->q[1] * this->q[1] - this->q[1]*this->kg[1] + this->q[3]*(2*this->q[3] - this->kg[3])) - 
                this->q[1]*this->q[3]*this->kg[1]*(this->q[3] - 2*this->kg[3]) + 
                this->q[1] * this->q[1]*(this->kg[1] * this->kg[1] - this->q[3]*this->kg[3]) - 
                this->q[3] * this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->q[3]*this->kg[3]))) - 
          this->q[0]*(pow(this->q[0],4)*(5*this->q[1] * this->q[1] - 3*this->q[1]*this->kg[1] + this->q[3]*(8*this->q[3] - 3*this->kg[3])) - 
             8*pow(p0,3)*this->q[0]*(this->q[1] * this->q[1] - this->q[1]*this->kg[1] + this->q[3]*(this->q[3] - this->kg[3])) + 
             (this->q[1] * this->q[1] + this->q[3] * this->q[3])*
              (pow(this->q[1],3)*this->kg[1] - 
                this->q[3] * this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + 
                pow(this->q[3],3)*this->kg[3] + this->q[1]*this->q[3]*this->kg[1]*(this->q[3] + 2*this->kg[3]) + 
                this->q[1] * this->q[1]*(this->kg[1] * this->kg[1] + this->q[3]*this->kg[3])) - 
             this->q[0] * this->q[0]*(pow(this->q[1],4) + 6*pow(this->q[1],3)*this->kg[1] + 
                6*this->q[1]*this->q[3]*this->kg[1]*(this->q[3] - this->kg[3]) + 
                3*this->q[3] * this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + 2*this->q[3]*this->kg[3]) + 
                this->q[1] * this->q[1]*(this->q[3] * this->q[3] - 3*this->kg[1] * this->kg[1] + 6*this->q[3]*this->kg[3])) - 
             4*pow(p0,2)*(pow(this->q[1],3)*this->kg[1] + this->q[1]*this->q[3]*this->kg[1]*(this->q[3] - 2*this->kg[3]) + 
                this->q[1] * this->q[1]*(-this->kg[1] * this->kg[1] + this->q[3]*this->kg[3]) + 
                this->q[3] * this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->q[3]*this->kg[3]) + 
                this->q[0] * this->q[0]*(5*this->q[1]*(-this->q[1] + this->kg[1]) + this->q[3]*(-6*this->q[3] + 5*this->kg[3]))) + 
             2*p0*this->q[0]*(pow(this->q[1],4) + 5*pow(this->q[1],3)*this->kg[1] + 
                this->q[1]*this->q[3]*this->kg[1]*(5*this->q[3] - 8*this->kg[3]) + 
                this->q[1] * this->q[1]*(2*this->q[3] * this->q[3] - 4*this->kg[1] * this->kg[1] + 5*this->q[3]*this->kg[3]) + 
                this->q[3] * this->q[3]*(4*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + 
                   this->q[3]*(this->q[3] + 5*this->kg[3])) + 
                this->q[0] * this->q[0]*(-9*this->q[1] * this->q[1] + 7*this->q[1]*this->kg[1] + 
                   this->q[3]*(-13*this->q[3] + 7*this->kg[3])))))))/(24.*this->mRes_2*pow(this->mn,5));
				   
    tr[0][1] = (-16*C5v*C5vNC*pow(this->mRes,3)*p0*pow(this->q[0],3)*this->q[1] + 
    4*C3vNC*C5v*this->mRes_2*this->mn*p0*pow(this->q[0],3)*this->q[1] + 
    4*C3v*C5vNC*this->mRes_2*this->mn*p0*pow(this->q[0],3)*this->q[1] - 
    8*C5v*C5vNC*this->mRes_2*this->mn*p0*pow(this->q[0],3)*this->q[1] + 
    16*C3v*C3vNC*this->mRes*this->mn2*p0*pow(this->q[0],3)*this->q[1] + 
    12*C3v*C5vNC*this->mRes*this->mn2*p0*pow(this->q[0],3)*this->q[1] - 
    8*C3v*C3vNC*pow(this->mn,3)*p0*pow(this->q[0],3)*this->q[1] + 
    48*C5v*C5vNC*this->mRes*pow(p0,3)*pow(this->q[0],3)*this->q[1] + 
    16*C3vNC*C5v*this->mn*pow(p0,3)*pow(this->q[0],3)*this->q[1] + 
    16*C3v*C5vNC*this->mn*pow(p0,3)*pow(this->q[0],3)*this->q[1] + 
    32*C5v*C5vNC*this->mn*pow(p0,3)*pow(this->q[0],3)*this->q[1] + 
    8*C3vNC*C5v*this->mRes_2*this->mn*pow(this->q[0],4)*this->q[1] + 
    8*C3v*C5vNC*this->mRes_2*this->mn*pow(this->q[0],4)*this->q[1] + 
    8*C5v*C5vNC*this->mRes_2*this->mn*pow(this->q[0],4)*this->q[1] - 
    16*C3v*C3vNC*this->mRes*this->mn2*pow(this->q[0],4)*this->q[1] - 
    8*C3vNC*C5v*this->mRes*this->mn2*pow(this->q[0],4)*this->q[1] - 
    8*C3v*C5vNC*this->mRes*this->mn2*pow(this->q[0],4)*this->q[1] + 
    8*C3v*C3vNC*pow(this->mn,3)*pow(this->q[0],4)*this->q[1] - 
    56*C5v*C5vNC*this->mRes*pow(p0,2)*pow(this->q[0],4)*this->q[1] - 
    40*C3vNC*C5v*this->mn*pow(p0,2)*pow(this->q[0],4)*this->q[1] - 
    40*C3v*C5vNC*this->mn*pow(p0,2)*pow(this->q[0],4)*this->q[1] - 
    64*C5v*C5vNC*this->mn*pow(p0,2)*pow(this->q[0],4)*this->q[1] + 
    16*C5v*C5vNC*this->mRes*p0*pow(this->q[0],5)*this->q[1] + 
    36*C3vNC*C5v*this->mn*p0*pow(this->q[0],5)*this->q[1] + 
    28*C3v*C5vNC*this->mn*p0*pow(this->q[0],5)*this->q[1] + 
    40*C5v*C5vNC*this->mn*p0*pow(this->q[0],5)*this->q[1] - 10*C3vNC*C5v*this->mn*pow(this->q[0],6)*this->q[1] - 
    6*C3v*C5vNC*this->mn*pow(this->q[0],6)*this->q[1] - 8*C5v*C5vNC*this->mn*pow(this->q[0],6)*this->q[1] - 
    4*C3vNC*C5v*this->mn*p0*pow(this->q[0],3)*pow(this->q[1],3) + 
    4*C3v*C5vNC*this->mn*p0*pow(this->q[0],3)*pow(this->q[1],3) + 
    2*C3vNC*C5v*this->mn*pow(this->q[0],4)*pow(this->q[1],3) - 
    2*C3v*C5vNC*this->mn*pow(this->q[0],4)*pow(this->q[1],3) + 
    4*C3v*C5vNC*this->mRes*this->mn2*this->q[0] * this->q[0]*this->q[1]*this->q[3] * this->q[3] - 
    4*C5v*C5vNC*this->mRes*pow(p0,2)*this->q[0] * this->q[0]*this->q[1]*this->q[3] * this->q[3] - 
    8*C3v*C5vNC*this->mn*pow(p0,2)*this->q[0] * this->q[0]*this->q[1]*this->q[3] * this->q[3] + 
    12*C5v*C5vNC*this->mRes*p0*pow(this->q[0],3)*this->q[1]*this->q[3] * this->q[3] + 
    4*C3vNC*C5v*this->mn*p0*pow(this->q[0],3)*this->q[1]*this->q[3] * this->q[3] + 
    20*C3v*C5vNC*this->mn*p0*pow(this->q[0],3)*this->q[1]*this->q[3] * this->q[3] + 
    8*C5v*C5vNC*this->mn*p0*pow(this->q[0],3)*this->q[1]*this->q[3] * this->q[3] - 
    C5v*C5vNC*this->mRes*pow(this->q[0],4)*this->q[1]*this->q[3] * this->q[3] + 
    2*C3vNC*C5v*this->mn*pow(this->q[0],4)*this->q[1]*this->q[3] * this->q[3] - 
    12*C3v*C5vNC*this->mn*pow(this->q[0],4)*this->q[1]*this->q[3] * this->q[3] - 
    8*C5v*C5vNC*this->mn*pow(this->q[0],4)*this->q[1]*this->q[3] * this->q[3] + 
    C5v*C5vNC*this->mRes*this->q[0] * this->q[0]*pow(this->q[1],3)*this->q[3] * this->q[3] + 
    2*C3v*C5vNC*this->mn*this->q[0] * this->q[0]*pow(this->q[1],3)*this->q[3] * this->q[3] + 
    C5v*C5vNC*this->mRes*this->q[0] * this->q[0]*this->q[1]*pow(this->q[3],4) + 
    2*C3v*C5vNC*this->mn*this->q[0] * this->q[0]*this->q[1]*pow(this->q[3],4) + 
    32*C3v*C3vNC*pow(this->mRes,3)*this->mn2*p0*this->q[0]*this->kg[1] + 
    16*C3vNC*C5v*pow(this->mRes,3)*this->mn2*p0*this->q[0]*this->kg[1] + 
    16*C3v*C5vNC*pow(this->mRes,3)*this->mn2*p0*this->q[0]*this->kg[1] + 
    32*C5v*C5vNC*pow(this->mRes,3)*pow(p0,3)*this->q[0]*this->kg[1] + 
    32*C3vNC*C5v*this->mRes_2*this->mn*pow(p0,3)*this->q[0]*this->kg[1] + 
    32*C3v*C5vNC*this->mRes_2*this->mn*pow(p0,3)*this->q[0]*this->kg[1] + 
    32*C5v*C5vNC*this->mRes_2*this->mn*pow(p0,3)*this->q[0]*this->kg[1] + 
    16*C3vNC*C5v*this->mRes*this->mn2*pow(p0,3)*this->q[0]*this->kg[1] + 
    16*C3v*C5vNC*this->mRes*this->mn2*pow(p0,3)*this->q[0]*this->kg[1] + 
    32*C3v*C3vNC*pow(this->mn,3)*pow(p0,3)*this->q[0]*this->kg[1] - 
    32*C3vNC*C5v*this->mRes_2*this->mn*pow(p0,2)*this->q[0] * this->q[0]*this->kg[1] - 
    32*C3v*C5vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[0] * this->q[0]*this->kg[1] - 
    32*C5v*C5vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[0] * this->q[0]*this->kg[1] - 
    32*C3vNC*C5v*this->mRes*this->mn2*pow(p0,2)*this->q[0] * this->q[0]*this->kg[1] - 
    32*C3v*C5vNC*this->mRes*this->mn2*pow(p0,2)*this->q[0] * this->q[0]*this->kg[1] - 
    96*C3v*C3vNC*pow(this->mn,3)*pow(p0,2)*this->q[0] * this->q[0]*this->kg[1] + 
    16*C5v*C5vNC*pow(this->mRes,3)*p0*pow(this->q[0],3)*this->kg[1] + 
    24*C3vNC*C5v*this->mRes_2*this->mn*p0*pow(this->q[0],3)*this->kg[1] - 
    8*C3v*C5vNC*this->mRes_2*this->mn*p0*pow(this->q[0],3)*this->kg[1] + 
    8*C5v*C5vNC*this->mRes_2*this->mn*p0*pow(this->q[0],3)*this->kg[1] + 
    28*C3vNC*C5v*this->mRes*this->mn2*p0*pow(this->q[0],3)*this->kg[1] + 
    12*C3v*C5vNC*this->mRes*this->mn2*p0*pow(this->q[0],3)*this->kg[1] + 
    88*C3v*C3vNC*pow(this->mn,3)*p0*pow(this->q[0],3)*this->kg[1] - 
    8*C5v*C5vNC*this->mRes*pow(p0,3)*pow(this->q[0],3)*this->kg[1] - 
    16*C3vNC*C5v*this->mn*pow(p0,3)*pow(this->q[0],3)*this->kg[1] - 
    8*C3vNC*C5v*this->mRes_2*this->mn*pow(this->q[0],4)*this->kg[1] - 
    8*C3v*C5vNC*this->mRes_2*this->mn*pow(this->q[0],4)*this->kg[1] - 
    8*C5v*C5vNC*this->mRes_2*this->mn*pow(this->q[0],4)*this->kg[1] - 
    8*C3vNC*C5v*this->mRes*this->mn2*pow(this->q[0],4)*this->kg[1] + 
    8*C3v*C5vNC*this->mRes*this->mn2*pow(this->q[0],4)*this->kg[1] - 
    24*C3v*C3vNC*pow(this->mn,3)*pow(this->q[0],4)*this->kg[1] + 
    32*C5v*C5vNC*this->mRes*pow(p0,2)*pow(this->q[0],4)*this->kg[1] + 
    40*C3vNC*C5v*this->mn*pow(p0,2)*pow(this->q[0],4)*this->kg[1] + 
    8*C3v*C5vNC*this->mn*pow(p0,2)*pow(this->q[0],4)*this->kg[1] + 
    16*C5v*C5vNC*this->mn*pow(p0,2)*pow(this->q[0],4)*this->kg[1] - 
    14*C5v*C5vNC*this->mRes*p0*pow(this->q[0],5)*this->kg[1] - 
    28*C3vNC*C5v*this->mn*p0*pow(this->q[0],5)*this->kg[1] - 
    16*C3v*C5vNC*this->mn*p0*pow(this->q[0],5)*this->kg[1] - 
    24*C5v*C5vNC*this->mn*p0*pow(this->q[0],5)*this->kg[1] + 6*C3vNC*C5v*this->mn*pow(this->q[0],6)*this->kg[1] + 
    6*C3v*C5vNC*this->mn*pow(this->q[0],6)*this->kg[1] + 8*C5v*C5vNC*this->mn*pow(this->q[0],6)*this->kg[1] + 
    16*C5v*C5vNC*pow(this->mRes,3)*p0*this->q[0]*this->q[1] * this->q[1]*this->kg[1] - 
    8*C3vNC*C5v*this->mRes_2*this->mn*p0*this->q[0]*this->q[1] * this->q[1]*this->kg[1] + 
    24*C3v*C5vNC*this->mRes_2*this->mn*p0*this->q[0]*this->q[1] * this->q[1]*this->kg[1] + 
    8*C5v*C5vNC*this->mRes_2*this->mn*p0*this->q[0]*this->q[1] * this->q[1]*this->kg[1] - 
    4*C3vNC*C5v*this->mRes*this->mn2*p0*this->q[0]*this->q[1] * this->q[1]*this->kg[1] + 
    12*C3v*C5vNC*this->mRes*this->mn2*p0*this->q[0]*this->q[1] * this->q[1]*this->kg[1] - 
    8*C3v*C3vNC*pow(this->mn,3)*p0*this->q[0]*this->q[1] * this->q[1]*this->kg[1] - 
    8*C5v*C5vNC*this->mRes*pow(p0,3)*this->q[0]*this->q[1] * this->q[1]*this->kg[1] - 
    16*C3v*C5vNC*this->mn*pow(p0,3)*this->q[0]*this->q[1] * this->q[1]*this->kg[1] - 
    8*C3vNC*C5v*this->mRes_2*this->mn*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[1] - 
    8*C3v*C5vNC*this->mRes_2*this->mn*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[1] - 
    8*C5v*C5vNC*this->mRes_2*this->mn*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[1] + 
    8*C3vNC*C5v*this->mRes*this->mn2*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[1] - 
    8*C3v*C5vNC*this->mRes*this->mn2*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[1] + 
    8*C3v*C3vNC*pow(this->mn,3)*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[1] + 
    32*C5v*C5vNC*this->mRes*pow(p0,2)*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[1] + 
    8*C3vNC*C5v*this->mn*pow(p0,2)*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[1] + 
    40*C3v*C5vNC*this->mn*pow(p0,2)*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[1] + 
    16*C5v*C5vNC*this->mn*pow(p0,2)*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[1] - 
    20*C5v*C5vNC*this->mRes*p0*pow(this->q[0],3)*this->q[1] * this->q[1]*this->kg[1] - 
    20*C3vNC*C5v*this->mn*p0*pow(this->q[0],3)*this->q[1] * this->q[1]*this->kg[1] - 
    36*C3v*C5vNC*this->mn*p0*pow(this->q[0],3)*this->q[1] * this->q[1]*this->kg[1] - 
    24*C5v*C5vNC*this->mn*p0*pow(this->q[0],3)*this->q[1] * this->q[1]*this->kg[1] + 
    12*C3vNC*C5v*this->mn*pow(this->q[0],4)*this->q[1] * this->q[1]*this->kg[1] + 
    12*C3v*C5vNC*this->mn*pow(this->q[0],4)*this->q[1] * this->q[1]*this->kg[1] + 
    8*C5v*C5vNC*this->mn*pow(this->q[0],4)*this->q[1] * this->q[1]*this->kg[1] + 
    2*C5v*C5vNC*this->mRes*p0*this->q[0]*pow(this->q[1],4)*this->kg[1] + 
    4*C3v*C5vNC*this->mn*p0*this->q[0]*pow(this->q[1],4)*this->kg[1] - 
    2*C3vNC*C5v*this->mn*this->q[0] * this->q[0]*pow(this->q[1],4)*this->kg[1] - 
    2*C3v*C5vNC*this->mn*this->q[0] * this->q[0]*pow(this->q[1],4)*this->kg[1] + 
    8*C3v*C3vNC*pow(this->mRes,3)*this->mn2*this->q[3] * this->q[3]*this->kg[1] - 
    8*C3v*C5vNC*pow(this->mRes,3)*this->mn2*this->q[3] * this->q[3]*this->kg[1] + 
    8*C3v*C3vNC*this->mRes_2*pow(this->mn,3)*this->q[3] * this->q[3]*this->kg[1] - 
    16*C5v*C5vNC*pow(this->mRes,3)*pow(p0,2)*this->q[3] * this->q[3]*this->kg[1] + 
    8*C3vNC*C5v*this->mRes_2*this->mn*pow(p0,2)*this->q[3] * this->q[3]*this->kg[1] - 
    16*C3v*C5vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[3] * this->q[3]*this->kg[1] - 
    16*C5v*C5vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[3] * this->q[3]*this->kg[1] - 
    16*C3v*C3vNC*this->mRes*this->mn2*pow(p0,2)*this->q[3] * this->q[3]*this->kg[1] + 
    8*C3vNC*C5v*this->mRes*this->mn2*pow(p0,2)*this->q[3] * this->q[3]*this->kg[1] - 
    8*C3v*C5vNC*this->mRes*this->mn2*pow(p0,2)*this->q[3] * this->q[3]*this->kg[1] + 
    16*C3v*C3vNC*pow(this->mn,3)*pow(p0,2)*this->q[3] * this->q[3]*this->kg[1] + 
    4*C3vNC*C5v*this->mRes_2*this->mn*p0*this->q[0]*this->q[3] * this->q[3]*this->kg[1] + 
    20*C3v*C5vNC*this->mRes_2*this->mn*p0*this->q[0]*this->q[3] * this->q[3]*this->kg[1] + 
    16*C5v*C5vNC*this->mRes_2*this->mn*p0*this->q[0]*this->q[3] * this->q[3]*this->kg[1] + 
    32*C3v*C3vNC*this->mRes*this->mn2*p0*this->q[0]*this->q[3] * this->q[3]*this->kg[1] - 
    8*C3vNC*C5v*this->mRes*this->mn2*p0*this->q[0]*this->q[3] * this->q[3]*this->kg[1] + 
    16*C3v*C5vNC*this->mRes*this->mn2*p0*this->q[0]*this->q[3] * this->q[3]*this->kg[1] - 
    32*C3v*C3vNC*pow(this->mn,3)*p0*this->q[0]*this->q[3] * this->q[3]*this->kg[1] + 
    4*C3vNC*C5v*this->mRes_2*this->mn*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[1] + 
    2*C3v*C5vNC*this->mRes_2*this->mn*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[1] - 
    20*C3v*C3vNC*this->mRes*this->mn2*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[1] - 
    12*C3v*C5vNC*this->mRes*this->mn2*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[1] + 
    16*C3v*C3vNC*pow(this->mn,3)*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[1] - 
    20*C5v*C5vNC*this->mRes*pow(p0,2)*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[1] - 
    16*C3vNC*C5v*this->mn*pow(p0,2)*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[1] - 
    8*C3v*C5vNC*this->mn*pow(p0,2)*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[1] - 
    16*C5v*C5vNC*this->mn*pow(p0,2)*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[1] + 
    10*C5v*C5vNC*this->mRes*p0*pow(this->q[0],3)*this->q[3] * this->q[3]*this->kg[1] + 
    20*C3vNC*C5v*this->mn*p0*pow(this->q[0],3)*this->q[3] * this->q[3]*this->kg[1] + 
    16*C3v*C5vNC*this->mn*p0*pow(this->q[0],3)*this->q[3] * this->q[3]*this->kg[1] + 
    24*C5v*C5vNC*this->mn*p0*pow(this->q[0],3)*this->q[3] * this->q[3]*this->kg[1] - 
    C5v*C5vNC*this->mRes*pow(this->q[0],4)*this->q[3] * this->q[3]*this->kg[1] - 
    6*C3vNC*C5v*this->mn*pow(this->q[0],4)*this->q[3] * this->q[3]*this->kg[1] - 
    4*C3v*C5vNC*this->mn*pow(this->q[0],4)*this->q[3] * this->q[3]*this->kg[1] - 
    8*C5v*C5vNC*this->mn*pow(this->q[0],4)*this->q[3] * this->q[3]*this->kg[1] - 
    2*C3v*C5vNC*this->mRes_2*this->mn*this->q[1] * this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 
    4*C3v*C3vNC*this->mRes*this->mn2*this->q[1] * this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 
    2*C5v*C5vNC*this->mRes*p0*this->q[0]*this->q[1] * this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 
    4*C3v*C5vNC*this->mn*p0*this->q[0]*this->q[1] * this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 
    C5v*C5vNC*this->mRes*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3] * this->q[3]*this->kg[1] - 
    2*C3vNC*C5v*this->mn*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3] * this->q[3]*this->kg[1] - 
    4*C3v*C5vNC*this->mn*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3] * this->q[3]*this->kg[1] - 
    2*C3v*C5vNC*this->mRes_2*this->mn*pow(this->q[3],4)*this->kg[1] + 
    4*C3v*C3vNC*this->mRes*this->mn2*pow(this->q[3],4)*this->kg[1] + 
    C5v*C5vNC*this->mRes*this->q[0] * this->q[0]*pow(this->q[3],4)*this->kg[1] - 
    2*C3v*C5vNC*this->mn*this->q[0] * this->q[0]*pow(this->q[3],4)*this->kg[1] - 
    16*C5v*C5vNC*pow(this->mRes,3)*p0*this->q[0]*this->q[1]*this->kg[1] * this->kg[1] - 
    20*C3vNC*C5v*this->mRes_2*this->mn*p0*this->q[0]*this->q[1]*this->kg[1] * this->kg[1] - 
    20*C3v*C5vNC*this->mRes_2*this->mn*p0*this->q[0]*this->q[1]*this->kg[1] * this->kg[1] - 
    8*C5v*C5vNC*this->mRes_2*this->mn*p0*this->q[0]*this->q[1]*this->kg[1] * this->kg[1] - 
    16*C3v*C3vNC*this->mRes*this->mn2*p0*this->q[0]*this->q[1]*this->kg[1] * this->kg[1] + 
    8*C3vNC*C5v*this->mRes*this->mn2*p0*this->q[0]*this->q[1]*this->kg[1] * this->kg[1] - 
    4*C3v*C5vNC*this->mRes*this->mn2*p0*this->q[0]*this->q[1]*this->kg[1] * this->kg[1] - 
    8*C3v*C3vNC*pow(this->mn,3)*p0*this->q[0]*this->q[1]*this->kg[1] * this->kg[1] + 
    8*C3vNC*C5v*this->mRes_2*this->mn*this->q[0] * this->q[0]*this->q[1]*this->kg[1] * this->kg[1] + 
    8*C3v*C5vNC*this->mRes_2*this->mn*this->q[0] * this->q[0]*this->q[1]*this->kg[1] * this->kg[1] + 
    8*C5v*C5vNC*this->mRes_2*this->mn*this->q[0] * this->q[0]*this->q[1]*this->kg[1] * this->kg[1] + 
    16*C3v*C3vNC*this->mRes*this->mn2*this->q[0] * this->q[0]*this->q[1]*this->kg[1] * this->kg[1] + 
    8*C3vNC*C5v*this->mRes*this->mn2*this->q[0] * this->q[0]*this->q[1]*this->kg[1] * this->kg[1] + 
    8*C3v*C5vNC*this->mRes*this->mn2*this->q[0] * this->q[0]*this->q[1]*this->kg[1] * this->kg[1] + 
    8*C3v*C3vNC*pow(this->mn,3)*this->q[0] * this->q[0]*this->q[1]*this->kg[1] * this->kg[1] - 
    8*C5v*C5vNC*this->mRes*pow(p0,2)*this->q[0] * this->q[0]*this->q[1]*this->kg[1] * this->kg[1] - 
    8*C3vNC*C5v*this->mn*pow(p0,2)*this->q[0] * this->q[0]*this->q[1]*this->kg[1] * this->kg[1] - 
    8*C3v*C5vNC*this->mn*pow(p0,2)*this->q[0] * this->q[0]*this->q[1]*this->kg[1] * this->kg[1] + 
    16*C5v*C5vNC*this->mRes*p0*pow(this->q[0],3)*this->q[1]*this->kg[1] * this->kg[1] + 
    16*C3vNC*C5v*this->mn*p0*pow(this->q[0],3)*this->q[1]*this->kg[1] * this->kg[1] + 
    16*C3v*C5vNC*this->mn*p0*pow(this->q[0],3)*this->q[1]*this->kg[1] * this->kg[1] + 
    8*C5v*C5vNC*this->mn*p0*pow(this->q[0],3)*this->q[1]*this->kg[1] * this->kg[1] - 
    6*C3vNC*C5v*this->mn*pow(this->q[0],4)*this->q[1]*this->kg[1] * this->kg[1] - 
    10*C3v*C5vNC*this->mn*pow(this->q[0],4)*this->q[1]*this->kg[1] * this->kg[1] - 
    8*C5v*C5vNC*this->mn*pow(this->q[0],4)*this->q[1]*this->kg[1] * this->kg[1] - 
    2*C3vNC*C5v*this->mn*this->q[0] * this->q[0]*pow(this->q[1],3)*this->kg[1] * this->kg[1] + 
    2*C3v*C5vNC*this->mn*this->q[0] * this->q[0]*pow(this->q[1],3)*this->kg[1] * this->kg[1] - 
    4*C3vNC*C5v*this->mRes_2*this->mn*this->q[1]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
    8*C3v*C5vNC*this->mRes*this->mn2*this->q[1]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
    8*C5v*C5vNC*this->mRes*pow(p0,2)*this->q[1]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
    16*C3v*C5vNC*this->mn*pow(p0,2)*this->q[1]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
    24*C5v*C5vNC*this->mRes*p0*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
    16*C3vNC*C5v*this->mn*p0*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
    32*C3v*C5vNC*this->mn*p0*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
    16*C5v*C5vNC*this->mn*p0*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
    2*C5v*C5vNC*this->mRes*this->q[0] * this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
    6*C3vNC*C5v*this->mn*this->q[0] * this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
    22*C3v*C5vNC*this->mn*this->q[0] * this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
    16*C5v*C5vNC*this->mn*this->q[0] * this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
    2*C5v*C5vNC*this->mRes*pow(this->q[1],3)*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
    4*C3v*C5vNC*this->mn*pow(this->q[1],3)*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
    2*C5v*C5vNC*this->mRes*this->q[1]*pow(this->q[3],4)*this->kg[1] * this->kg[1] - 
    4*C3v*C5vNC*this->mn*this->q[1]*pow(this->q[3],4)*this->kg[1] * this->kg[1] + 
    std::complex<double>(0,8)*C5a*C5v*pow(this->mRes,3)*this->mn2*p0*this->q[3]*this->kg[2] + 
    std::complex<double>(0,24)*C3v*C5a*this->mRes_2*pow(this->mn,3)*p0*this->q[3]*this->kg[2] + 
    std::complex<double>(0,8)*C5a*C5v*this->mRes_2*pow(this->mn,3)*p0*this->q[3]*this->kg[2] - 
    std::complex<double>(0,8)*C3v*C5a*this->mRes*pow(this->mn,4)*p0*this->q[3]*this->kg[2] - 
    std::complex<double>(0,8)*C3v*C5a*this->mRes_2*pow(this->mn,3)*this->q[0]*this->q[3]*this->kg[2] + 
    std::complex<double>(0,8)*C3v*C5a*this->mRes*pow(this->mn,4)*this->q[0]*this->q[3]*this->kg[2] + 
    std::complex<double>(0,8)*C5a*C5v*this->mRes*this->mn2*p0*this->q[0] * this->q[0]*this->q[3]*this->kg[2] - 
    std::complex<double>(0,4)*C5a*C5v*this->mRes*this->mn2*pow(this->q[0],3)*this->q[3]*this->kg[2] + 
    std::complex<double>(0,4)*C5a*C5v*this->mRes*this->mn2*this->q[0]*this->q[1]*this->q[3]*this->kg[1]*this->kg[2] + 
    12*C3vNC*C5v*this->mRes*this->mn2*p0*this->q[0]*this->q[1]*this->kg[2] * this->kg[2] - 
    4*C3v*C5vNC*this->mRes*this->mn2*this->q[1]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] + 
    4*C5v*C5vNC*this->mRes*pow(p0,2)*this->q[1]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] + 
    8*C3v*C5vNC*this->mn*pow(p0,2)*this->q[1]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] - 
    12*C5v*C5vNC*this->mRes*p0*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] - 
    8*C3vNC*C5v*this->mn*p0*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] - 
    16*C3v*C5vNC*this->mn*p0*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] - 
    8*C5v*C5vNC*this->mn*p0*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] + 
    C5v*C5vNC*this->mRes*this->q[0] * this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] + 
    10*C3v*C5vNC*this->mn*this->q[0] * this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] + 
    8*C5v*C5vNC*this->mn*this->q[0] * this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] - 
    C5v*C5vNC*this->mRes*pow(this->q[1],3)*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] - 
    2*C3v*C5vNC*this->mn*pow(this->q[1],3)*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] - 
    C5v*C5vNC*this->mRes*this->q[1]*pow(this->q[3],4)*this->kg[2] * this->kg[2] - 
    2*C3v*C5vNC*this->mn*this->q[1]*pow(this->q[3],4)*this->kg[2] * this->kg[2] + 
    this->q[3]*(-2*C3v*this->mn*(-(C5vNC*this->q[1]*
             (4*pow(this->mRes,3)*this->mn + 
               2*this->mRes*this->mn*(2*pow(p0,2) - p0*this->q[0] + this->q[0] * this->q[0]) + 
               this->mRes_2*
                (8*pow(p0,2) + 2*p0*this->q[0] - 5*this->q[0] * this->q[0] + 
                  this->q[1] * this->q[1] + this->q[3] * this->q[3]) + 
               2*this->q[0]*(-4*pow(p0,3) + 12*pow(p0,2)*this->q[0] + 
                  4*pow(this->q[0],3) + 
                  p0*(-13*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3]))))  
+ C5vNC*(2*this->mRes_2*(5*p0 - 2*this->q[0])*this->q[0] + 
             (4*pow(p0,2) - 8*p0*this->q[0] + 5*this->q[0] * this->q[0] - 
                this->q[1] * this->q[1] - this->q[3] * this->q[3])*
              (this->q[0] * this->q[0] + this->q[1] * this->q[1] - this->q[3] * this->q[3]) + 
             2*this->mRes*this->mn*((p0 - 2*this->q[0])*this->q[0] - this->q[1] * this->q[1] + this->q[3] * this->q[3]))*
           this->kg[1] + 2*C3vNC*this->mn*(this->q[1]*
              (2*pow(this->mRes,3) + 2*this->mRes_2*this->mn + 
                2*this->mn*(2*pow(p0,2) - 3*p0*this->q[0] + this->q[0] * this->q[0]) + 
                this->mRes*(-4*pow(p0,2) + 8*p0*this->q[0] - 5*this->q[0] * this->q[0] + 
                   this->q[1] * this->q[1] + this->q[3] * this->q[3])) + 
             2*(2*this->mRes + this->mn)*(p0 - this->q[0])*this->q[0]*this->kg[1])) + 
       C5v*(C5vNC*(this->mRes*this->q[0]*this->q[1]*
              (-8*pow(p0,3) + 52*pow(p0,2)*this->q[0] + 
                this->q[0]*(this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3]) + 
                2*p0*(-15*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3])) + 
             8*this->mRes_2*this->mn*(p0 - this->q[0])*
              (2*p0*this->q[1] + this->q[0]*(this->q[1] - this->kg[1])) + 
             this->mRes*(-4*pow(p0,2)*
                 (2*this->q[0] * this->q[0] + this->q[1] * this->q[1] - this->q[3] * this->q[3]) + 
                (this->q[1] - this->q[3])*(this->q[1] + this->q[3])*
                 (-this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3]) + 
                4*p0*this->q[0]*(4*this->q[0] * this->q[0] + 3*(this->q[1] - this->q[3])*(this->q[1] + this->q[3])))*this->kg[1] + 
             16*pow(this->mRes,3)*p0*((p0 + this->q[0])*this->q[1] - this->q[0]*this->kg[1]) + 
             8*this->mn*(p0 - this->q[0])*this->q[0]*
              (4*p0*this->q[0]*this->q[1] + (this->q[1] - this->q[3])*(this->q[1] + this->q[3])*this->kg[1] + 
                this->q[0] * this->q[0]*(-2*this->q[1] + this->kg[1]))) - 
          2*this->mn*(C3vNC*this->q[1]*(2*this->mRes*this->mn*
                 (2*pow(p0,2) - p0*this->q[0] - 2*this->q[0] * this->q[0]) + 
                this->mRes_2*
                 (4*pow(p0,2) + 6*p0*this->q[0] + 6*this->q[0] * this->q[0]) + 
                this->q[0] * this->q[0]*(-12*pow(p0,2) + 20*p0*this->q[0] - 
                   9*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3])) + 
             C3vNC*(2*this->mRes*this->mn*(p0 - 2*this->q[0])*this->q[0] + 
                2*this->mRes_2*
                 (5*p0*this->q[0] - 2*this->q[0] * this->q[0] - this->q[1] * this->q[1] + 
                   this->q[3] * this->q[3]) + 
                this->q[0]*(4*pow(p0,2)*this->q[0] - 8*p0*this->q[0] * this->q[0] + 
                   3*pow(this->q[0],3) - 4*p0*this->q[1] * this->q[1] + 
                   5*this->q[0]*this->q[1] * this->q[1] + (4*p0 - 3*this->q[0])*this->q[3] * this->q[3]))*this->kg[1] - 
             std::complex<double>(0,2)*C5a*this->mRes*this->mn*this->q[0]*this->q[3]*this->kg[2])))*this->kg[3] + 
    4*C3vNC*C5v*this->mn*this->q[1]*(3*this->mRes*this->mn*p0*this->q[0] + 
       (this->mRes_2 - 2*this->q[0] * this->q[0])*this->q[3] * this->q[3])*this->kg[3] * this->kg[3] + 
    C4vNC*this->mRes*(4*C4v*this->mRes*(-(this->mRes*
             (2*pow(this->q[0],4)*this->q[1] - 6*p0*pow(this->q[0],3)*this->kg[1] + 
               2*p0*this->q[0]*this->kg[1]*
                (-4*pow(p0,2) + this->q[1] * this->q[1] + 2*this->q[3] * this->q[3] + 
                  2*this->q[1]*this->kg[1]) + 
               this->q[0] * this->q[0]*((16*pow(p0,2) + this->q[3] * this->q[3])*this->kg[1] - 
                  this->q[1]*(this->q[3] * this->q[3] + 2*this->kg[1] * this->kg[1])) + 
               this->q[3] * this->q[3]*(-4*pow(p0,2)*this->kg[1] + 
                  this->q[1]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])))) + 
          this->mRes*this->q[3]*(-4*pow(p0,2)*this->q[1] + 2*p0*this->q[0]*(this->q[1] - 2*this->kg[1]) + 
             (this->q[1] - this->q[3])*(this->q[1] + this->q[3])*this->kg[1] + this->q[0] * this->q[0]*(this->q[1] + 2*this->kg[1]))*this->kg[3] + 
          2*this->mn*(p0 - this->q[0])*(-(pow(this->q[0],3)*(this->q[1] - 3*this->kg[1])) - 
             8*p0*this->q[0] * this->q[0]*this->kg[1] + 2*p0*this->q[3]*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3]) - 
             this->q[0]*(-4*pow(p0,2)*this->kg[1] + 2*this->q[3] * this->q[3]*this->kg[1] + 
                this->q[1]*this->kg[1]*(this->q[1] + this->kg[1]) + this->q[3]*(-this->q[1] + this->kg[1])*this->kg[3]))) + 
       2*C3v*this->mn*(2*this->mn*(pow(this->q[0],4)*(this->q[1] - 3*this->kg[1]) + 
             4*pow(p0,3)*this->q[0]*this->kg[1] + 
             2*this->mRes_2*this->q[3]*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3]) + 
             this->q[0] * this->q[0]*(-4*this->mRes_2*this->kg[1] + 2*this->q[3] * this->q[3]*this->kg[1] + 
                this->q[1]*this->kg[1]*(this->q[1] + this->kg[1]) + this->q[3]*(-this->q[1] + this->kg[1])*this->kg[3]) - 
             2*pow(p0,2)*
              (6*this->q[0] * this->q[0]*this->kg[1] + this->q[3]*(-(this->q[3]*this->kg[1]) + this->q[1]*this->kg[3])) - 
             p0*this->q[0]*(this->q[0] * this->q[0]*(this->q[1] - 11*this->kg[1]) - 4*this->mRes_2*this->kg[1] + 
                this->q[1] * this->q[1]*this->kg[1] + this->q[3]*this->kg[1]*(4*this->q[3] + this->kg[3]) + 
                this->q[1]*(this->kg[1] * this->kg[1] - 3*this->q[3]*this->kg[3]))) + 
          this->mRes*(-6*pow(this->q[0],4)*this->q[1] + 12*p0*pow(this->q[0],3)*this->kg[1] + 
             this->q[3]*(8*pow(p0,2) + this->q[1] * this->q[1] - 2*this->q[1]*this->kg[1] + 
                this->q[3]*(this->q[3] - 2*this->kg[3]))*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3]) + 
             this->q[0] * this->q[0]*(-32*pow(p0,2)*this->kg[1] - 5*this->q[3] * this->q[3]*this->kg[1] + 
                6*this->q[1]*this->kg[1] * this->kg[1] + this->q[3]*(5*this->q[1] + 6*this->kg[1])*this->kg[3]) + 
             2*p0*this->q[0]*(8*pow(p0,2)*this->kg[1] - 2*this->q[1] * this->q[1]*this->kg[1] - 
                this->q[3]*this->kg[1]*(3*this->q[3] + 5*this->kg[3]) + 
                this->q[1]*(-4*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3]*(this->q[3] + this->kg[3]))))) + 
       C5v*(8*this->mRes*this->mn*(p0 - this->q[0])*
           (4*pow(p0,2)*this->q[0]*this->kg[1] - 4*p0*this->q[0] * this->q[0]*this->kg[1] + 
             pow(this->q[0],3)*(this->q[1] + this->kg[1]) + 2*p0*this->q[3]*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3]) - 
             this->q[0]*(this->q[1] + this->kg[1])*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3])) + 
          4*this->mRes_2*(pow(this->q[0],4)*(this->q[1] - this->kg[1]) + 
             8*pow(p0,3)*this->q[0]*this->kg[1] + 
             2*p0*this->q[0]*(this->q[1] + 2*this->kg[1])*(this->q[0] * this->q[0] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3]) - 
             this->q[3]*(this->q[1]*this->q[3]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) - 
                this->q[1] * this->q[1]*this->kg[1]*this->kg[3] + this->q[3] * this->q[3]*this->kg[1]*this->kg[3]) - 
             4*pow(p0,2)*(2*this->q[0] * this->q[0]*this->kg[1] + 
                this->q[3]*(-(this->q[3]*this->kg[1]) + this->q[1]*this->kg[3])) + 
             this->q[0] * this->q[0]*(-(this->q[1] * this->q[1]*this->kg[1]) + this->q[3]*this->kg[1]*(this->q[3] + this->kg[3]) + 
                this->q[1]*(this->q[3] * this->q[3] + this->kg[1] * this->kg[1] - 2*this->q[3]*this->kg[3]))) - 
          this->q[0]*(8*pow(p0,3)*this->q[0] * this->q[0]*(-this->q[1] + this->kg[1]) + 
             4*pow(p0,2)*this->q[0]*
              (5*this->q[0] * this->q[0]*(this->q[1] - this->kg[1]) - this->q[1] * this->q[1]*this->kg[1] + 
                this->q[3]*this->kg[1]*(2*this->q[3] + this->kg[3]) + this->q[1]*(this->kg[1] * this->kg[1] - 3*this->q[3]*this->kg[3])) + 
             2*p0*(pow(this->q[0],4)*(-9*this->q[1] + 7*this->kg[1]) + 
                2*this->q[3]*(this->q[1]*this->q[3]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) - 
                   this->q[1] * this->q[1]*this->kg[1]*this->kg[3] + this->q[3] * this->q[3]*this->kg[1]*this->kg[3]) + 
                this->q[0] * this->q[0]*(pow(this->q[1],3) + 5*this->q[1] * this->q[1]*this->kg[1] - 
                   this->q[3]*this->kg[1]*(5*this->q[3] + 4*this->kg[3]) - 
                   this->q[1]*(this->q[3] * this->q[3] + 4*this->kg[1] * this->kg[1] - 10*this->q[3]*this->kg[3]))) + 
             this->q[0]*(pow(this->q[0],4)*(5*this->q[1] - 3*this->kg[1]) + pow(this->q[1],4)*this->kg[1] - 
                3*pow(this->q[3],3)*this->kg[1]*this->kg[3] + this->q[1] * this->q[1]*this->q[3]*this->kg[1]*(this->q[3] + 5*this->kg[3]) + 
                pow(this->q[1],3)*(this->kg[1] * this->kg[1] + this->q[3]*this->kg[3]) + 
                this->q[1]*this->q[3] * this->q[3]*
                 (-7*this->kg[1] * this->kg[1] - 4*this->kg[2] * this->kg[2] + this->q[3]*this->kg[3]) + 
                this->q[0] * this->q[0]*(-pow(this->q[1],3) - 6*this->q[1] * this->q[1]*this->kg[1] + 
                   3*this->q[3]*this->kg[1]*(this->q[3] + this->kg[3]) + 
                   3*this->q[1]*(this->q[3] * this->q[3] + this->kg[1] * this->kg[1] - 3*this->q[3]*this->kg[3])))))) + 
    C4v*this->mRes*(C5vNC*(-3*pow(this->q[0],6)*this->q[1] - pow(this->q[0],4)*pow(this->q[1],3) - 
          6*pow(this->q[0],4)*this->q[1]*this->q[3] * this->q[3] + 
          this->q[0] * this->q[0]*pow(this->q[1],3)*this->q[3] * this->q[3] + 
          this->q[0] * this->q[0]*this->q[1]*pow(this->q[3],4) + 3*pow(this->q[0],6)*this->kg[1] + 
          6*pow(this->q[0],4)*this->q[1] * this->q[1]*this->kg[1] - this->q[0] * this->q[0]*pow(this->q[1],4)*this->kg[1] - 
          2*pow(this->q[0],4)*this->q[3] * this->q[3]*this->kg[1] - 
          2*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3] * this->q[3]*this->kg[1] - 
          this->q[0] * this->q[0]*pow(this->q[3],4)*this->kg[1] - 5*pow(this->q[0],4)*this->q[1]*this->kg[1] * this->kg[1] + 
          this->q[0] * this->q[0]*pow(this->q[1],3)*this->kg[1] * this->kg[1] + 
          11*this->q[0] * this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
          2*pow(this->q[1],3)*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
          2*this->q[1]*pow(this->q[3],4)*this->kg[1] * this->kg[1] + 
          5*this->q[0] * this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] - 
          pow(this->q[1],3)*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] - 
          this->q[1]*pow(this->q[3],4)*this->kg[2] * this->kg[2] + 
          this->q[3]*(8*pow(this->q[0],4)*this->q[1] - 
             (5*this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3])*
              (this->q[0] * this->q[0] + this->q[1] * this->q[1] - this->q[3] * this->q[3])*this->kg[1])*this->kg[3] + 
          8*pow(p0,3)*this->q[0]*this->q[1]*(this->q[0] * this->q[0] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3]) + 
          8*this->mRes*this->mn*(p0 - this->q[0])*
           (pow(this->q[0],3)*(this->q[1] - this->kg[1]) - 4*p0*this->q[0] * this->q[0]*this->kg[1] + 
             this->q[0]*this->kg[1]*(4*pow(p0,2) + this->q[1] * this->q[1] + 2*this->q[3] * this->q[3] - 
                this->q[1]*this->kg[1]) - this->q[0]*this->q[3]*(this->q[1] + this->kg[1])*this->kg[3] + 
             2*p0*this->q[3]*(-(this->q[3]*this->kg[1]) + this->q[1]*this->kg[3])) + 
          2*p0*this->q[0]*(pow(this->q[0],4)*(7*this->q[1] - 4*this->kg[1]) + pow(this->q[1],4)*this->kg[1] + 
             pow(this->q[1],3)*this->q[3]*this->kg[3] - 4*pow(this->q[3],3)*this->kg[1]*this->kg[3] + 
             this->q[1] * this->q[1]*this->q[3]*this->kg[1]*(this->q[3] + 4*this->kg[3]) + 
             this->q[1]*this->q[3] * this->q[3]*(-8*this->kg[1] * this->kg[1] - 4*this->kg[2] * this->kg[2] + this->q[3]*this->kg[3]) + 
             this->q[0] * this->q[0]*(pow(this->q[1],3) - 9*this->q[1] * this->q[1]*this->kg[1] + 
                4*this->q[3]*this->kg[1]*(this->q[3] + this->kg[3]) + 
                this->q[1]*(5*this->q[3] * this->q[3] + 4*this->kg[1] * this->kg[1] - 13*this->q[3]*this->kg[3]))) - 
          4*pow(p0,2)*(pow(this->q[0],4)*(5*this->q[1] - this->kg[1]) - 
             this->q[3]*(this->q[1]*this->q[3]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) - 
                this->q[1] * this->q[1]*this->kg[1]*this->kg[3] + this->q[3] * this->q[3]*this->kg[1]*this->kg[3]) + 
             this->q[0] * this->q[0]*(-5*this->q[1] * this->q[1]*this->kg[1] + this->q[3]*this->kg[1]*(this->q[3] + this->kg[3]) + 
                this->q[1]*(this->q[3] * this->q[3] + this->kg[1] * this->kg[1] - 6*this->q[3]*this->kg[3]))) + 
          4*this->mRes_2*(8*pow(p0,3)*this->q[0]*this->kg[1] + 
             this->q[0] * this->q[0]*(this->q[1] - this->kg[1])*(this->q[0] * this->q[0] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3]) - 
             4*pow(p0,2)*(2*this->q[0] * this->q[0]*this->kg[1] + this->q[3]*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3])) + 
             2*p0*this->q[0]*(this->q[0] * this->q[0]*(this->q[1] - this->kg[1]) + 
                2*this->kg[1]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3])))) + 
       4*this->mn*(std::complex<double>(0,2)*C5a*this->mRes*this->mn*(this->mRes + this->mn)*(p0 - this->q[0])*this->q[3]*this->kg[2] + 
          C3vNC*(4*this->mRes_2*this->mn*(p0 - this->q[0])*this->q[0]*this->kg[1] + 
             this->mn*(p0 - this->q[0])*(-(pow(this->q[0],3)*(this->q[1] - 3*this->kg[1])) - 
                8*p0*this->q[0] * this->q[0]*this->kg[1] + 2*p0*this->q[3]*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3]) - 
                this->q[0]*(-4*pow(p0,2)*this->kg[1] + 2*this->q[3] * this->q[3]*this->kg[1] + 
                   this->q[1]*this->kg[1]*(this->q[1] + this->kg[1]) + this->q[3]*(-this->q[1] + this->kg[1])*this->kg[3])) + 
             this->mRes*(-3*pow(this->q[0],4)*this->q[1] + 8*pow(p0,3)*this->q[0]*this->kg[1] + 
                p0*this->q[0]*(this->q[0] * this->q[0]*(this->q[1] + 6*this->kg[1]) - 
                   this->kg[1]*(2*this->q[1] * this->q[1] + this->q[3] * this->q[3] + 5*this->q[1]*this->kg[1]) - 
                   this->q[3]*(this->q[1] + 5*this->kg[1])*this->kg[3]) - 
                2*pow(p0,2)*
                 (8*this->q[0] * this->q[0]*this->kg[1] + this->q[3]*(-(this->q[3]*this->kg[1]) + this->q[1]*this->kg[3])) + 
                this->q[0] * this->q[0]*(-(this->q[3]*this->kg[1]*(this->q[3] - 3*this->kg[3])) + 
                   this->q[1]*(this->q[3] * this->q[3] + 3*this->kg[1] * this->kg[1] + 2*this->q[3]*this->kg[3])) - 
                this->q[3]*(this->q[1]*this->q[3]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) - 
                   this->q[1] * this->q[1]*this->kg[1]*this->kg[3] + 
                   this->q[3]*this->kg[1]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3]*(this->q[3] + this->kg[3])))))) 
))/(24.*this->mRes_2*pow(this->mn,5));	
    
	tr[0][2] = (std::complex<double>(0,-4)*C5a*this->mRes*this->mn2*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3])*
     (-2*C3v*this->mn*(this->mn*(p0 - this->q[0]) + this->mRes*(-3*p0 + this->q[0])) + 
       C5v*(2*this->mRes_2*p0 + 2*this->mRes*this->mn*p0 + 
          this->q[0]*(2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3]))) + 
    C4v*this->mRes*(std::complex<double>(0,-8)*C5a*this->mRes*this->mn2*(this->mRes + this->mn)*(p0 - this->q[0])*
        (this->q[3]*this->kg[1] - this->q[1]*this->kg[3]) + this->kg[2]*
        (C5vNC*((this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3])*
              (3*pow(this->q[0],4) + 
                this->q[0] * this->q[0]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] - 5*this->q[1]*this->kg[1] - 
                   5*this->q[3]*this->kg[3]) + 
                4*pow(p0,2)*(this->q[0] * this->q[0] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3]) + 
                (this->q[1] * this->q[1] + this->q[3] * this->q[3])*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) + 
                8*p0*this->q[0]*(-this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3])) + 
             4*this->mRes_2*(8*pow(p0,3)*this->q[0] - 2*p0*pow(this->q[0],3) - 
                4*pow(p0,2)*
                 (2*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3]) + 
                4*p0*this->q[0]*(this->q[1] * this->q[1] - this->q[1]*this->kg[1] + this->q[3]*(this->q[3] - this->kg[3])) + 
                this->q[0] * this->q[0]*(-this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3])) + 
             8*this->mRes*this->mn*(p0 - this->q[0])*
              (4*pow(p0,2)*this->q[0] - 
                2*p0*(2*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3]) - 
                this->q[0]*(this->q[0] * this->q[0] + this->q[1]*(-2*this->q[1] + this->kg[1]) + this->q[3]*(-2*this->q[3] + this->kg[3])))) + 
          4*C4vNC*this->mRes*(2*this->mn*(p0 - this->q[0])*
              (4*pow(p0,2)*this->q[0] + 3*pow(this->q[0],3) + 
                2*p0*(-4*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3]) - 
                this->q[0]*this->q[1]*(2*this->q[1] + this->kg[1]) - this->q[0]*this->q[3]*(2*this->q[3] + this->kg[3])) + 
             this->mRes*(8*pow(p0,3)*this->q[0] + 6*p0*pow(this->q[0],3) + 
                4*pow(p0,2)*
                 (-4*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3]) - 
                this->q[0] * this->q[0]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] - 2*this->q[1]*this->kg[1] - 
                   2*this->q[3]*this->kg[3]) - 
                (this->q[1] * this->q[1] + this->q[3] * this->q[3])*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) - 
                4*p0*this->q[0]*(this->q[1]*(this->q[1] + this->kg[1]) + this->q[3]*(this->q[3] + this->kg[3])))) + 
          4*C3vNC*this->mn*(4*this->mRes_2*this->mn*(p0 - this->q[0])*this->q[0] + 
             this->mn*(p0 - this->q[0])*(4*pow(p0,2)*this->q[0] + 3*pow(this->q[0],3) + 
                2*p0*(-4*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3]) - 
                this->q[0]*this->q[1]*(2*this->q[1] + this->kg[1]) - this->q[0]*this->q[3]*(2*this->q[3] + this->kg[3])) + 
             this->mRes*(8*pow(p0,3)*this->q[0] + 
                2*pow(p0,2)*
                 (-8*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3]) - 
                (this->q[1] * this->q[1] + this->q[3] * this->q[3])*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) + 
                this->q[0] * this->q[0]*(-2*this->q[1] * this->q[1] - 2*this->q[3] * this->q[3] + 3*this->q[1]*this->kg[1] + 
                   3*this->q[3]*this->kg[3]) + 
                p0*this->q[0]*(6*this->q[0] * this->q[0] - this->q[1]*(this->q[1] + 5*this->kg[1]) - this->q[3]*(this->q[3] + 5*this->kg[3])) 
)))) + this->kg[2]*(2*C3v*this->mn*(2*C3vNC*this->mn*
           (2*this->mRes_2*this->mn*(this->q[1] * this->q[1] + this->q[3] * this->q[3]) + 
             2*pow(this->mRes,3)*(4*p0*this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3]) + 
             2*this->mn*(p0 - this->q[0])*
              (4*pow(p0,2)*this->q[0] + 3*pow(this->q[0],3) + 
                2*p0*(-4*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3]) - 
                this->q[0]*this->q[1]*(2*this->q[1] + this->kg[1]) - this->q[0]*this->q[3]*(2*this->q[3] + this->kg[3])) + 
             this->mRes*(-4*pow(p0,2)*(this->q[1] * this->q[1] + this->q[3] * this->q[3]) + 
                pow(this->q[1] * this->q[1] + this->q[3] * this->q[3],2) + 
                4*p0*this->q[0]*(2*this->q[1] * this->q[1] - this->q[1]*this->kg[1] + this->q[3]*(2*this->q[3] - this->kg[3])) + 
                this->q[0] * this->q[0]*(-5*this->q[1] * this->q[1] - 5*this->q[3] * this->q[3] + 4*this->q[1]*this->kg[1] + 
                   4*this->q[3]*this->kg[3]))) + 
          C5vNC*(4*pow(this->mRes,3)*this->mn*
              (2*p0*this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3]) + 
             (this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3])*
              (3*pow(this->q[0],4) + 
                this->q[0] * this->q[0]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] - 5*this->q[1]*this->kg[1] - 
                   5*this->q[3]*this->kg[3]) + 
                4*pow(p0,2)*(this->q[0] * this->q[0] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3]) + 
                (this->q[1] * this->q[1] + this->q[3] * this->q[3])*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) + 
                8*p0*this->q[0]*(-this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3])) + 
             2*this->mRes*this->mn*(4*pow(p0,3)*this->q[0] + 2*pow(this->q[0],4) - 
                2*pow(p0,2)*
                 (4*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3]) + 
                p0*this->q[0]*(3*this->q[0] * this->q[0] + 4*this->q[1] * this->q[1] + 
                   4*this->q[3] * this->q[3] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3]) - 
                (this->q[1] * this->q[1] + this->q[3] * this->q[3])*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) + 
                this->q[0] * this->q[0]*(-3*this->q[1] * this->q[1] - 3*this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1] + 
                   2*this->q[3]*this->kg[3])) + 
             this->mRes_2*(16*pow(p0,3)*this->q[0] - 4*p0*pow(this->q[0],3) - 
                4*pow(this->q[0],4) - pow(this->q[1] * this->q[1] + this->q[3] * this->q[3],2) - 
                8*pow(p0,2)*
                 (2*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3]) + 
                10*p0*this->q[0]*(this->q[1] * this->q[1] - this->q[1]*this->kg[1] + this->q[3]*(this->q[3] - this->kg[3])) + 
                this->q[0] * this->q[0]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] + 4*this->q[1]*this->kg[1] + 
                   4*this->q[3]*this->kg[3]))) + 
          C4vNC*this->mRes*(4*this->mRes_2*this->mn*
              (2*(p0 - this->q[0])*this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3]) + 
             2*this->mn*(p0 - this->q[0])*
              (4*pow(p0,2)*this->q[0] + 3*pow(this->q[0],3) + 
                2*p0*(-4*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3]) - 
                this->q[0]*this->q[1]*(2*this->q[1] + this->kg[1]) - this->q[0]*this->q[3]*(2*this->q[3] + this->kg[3])) + 
             this->mRes*(16*pow(p0,3)*this->q[0] + 
                8*pow(p0,2)*
                 (-4*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3]) + 
                2*p0*this->q[0]*(6*this->q[0] * this->q[0] - 3*this->q[1] * this->q[1] - 
                   3*this->q[3] * this->q[3] - 5*this->q[1]*this->kg[1] - 5*this->q[3]*this->kg[3]) + 
                (this->q[1] * this->q[1] + this->q[3] * this->q[3])*
                 (this->q[1] * this->q[1] + this->q[3] * this->q[3] - 2*this->q[1]*this->kg[1] - 2*this->q[3]*this->kg[3]) + 
                this->q[0] * this->q[0]*(-5*this->q[1] * this->q[1] - 5*this->q[3] * this->q[3] + 6*this->q[1]*this->kg[1] + 
                   6*this->q[3]*this->kg[3])))) + 
       C5v*(C4vNC*this->mRes*(this->q[0]*(4*pow(p0,2)*this->q[0] + 
                3*this->q[0]*(this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3]) + 
                4*p0*(-2*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3]))*
              (-2*p0*this->q[0] + this->q[0] * this->q[0] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3]) + 
             8*this->mRes*this->mn*(p0 - this->q[0])*
              (4*pow(p0,2)*this->q[0] + 
                2*p0*(-2*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3]) + 
                this->q[0]*(this->q[0] * this->q[0] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3])) + 
             4*this->mRes_2*(8*pow(p0,3)*this->q[0] + 
                4*pow(p0,2)*
                 (-2*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3]) + 
                4*p0*this->q[0]*(this->q[0] * this->q[0] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3]) - 
                (this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3])*
                 (this->q[0] * this->q[0] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3]))) + 
          2*C3vNC*this->mn*(8*pow(this->mRes,3)*this->mn*p0*this->q[0] + 
             this->q[0]*(4*pow(p0,2)*this->q[0] + 
                3*this->q[0]*(this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3]) + 
                4*p0*(-2*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3]))*
              (-2*p0*this->q[0] + this->q[0] * this->q[0] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3]) + 
             2*this->mRes_2*(8*pow(p0,3)*this->q[0] + 
                2*pow(p0,2)*
                 (-4*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3]) + 
                p0*this->q[0]*(6*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] - 
                   5*this->q[1]*this->kg[1] - 5*this->q[3]*this->kg[3]) - 
                (2*this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3])*
                 (this->q[0] * this->q[0] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3])) + 
             2*this->mRes*this->mn*(4*pow(p0,3)*this->q[0] + 
                2*pow(p0,2)*
                 (-4*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3]) + 
                2*this->q[0] * this->q[0]*(-this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) + 
                p0*this->q[0]*(7*this->q[0] * this->q[0] - this->q[1]*(2*this->q[1] + this->kg[1]) - this->q[3]*(2*this->q[3] + this->kg[3])) 
)) + C5vNC*(8*this->mn*(p0 - this->q[0])*this->q[0]*(this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3])*
              (2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) + 
             8*this->mRes_2*this->mn*(p0 - this->q[0])*
              (4*pow(p0,2)*this->q[0] - 2*p0*(this->q[1] * this->q[1] + this->q[3] * this->q[3]) + 
                this->q[0]*(this->q[0] * this->q[0] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3])) + 
             16*pow(this->mRes,3)*p0*
              (2*pow(p0,2)*this->q[0] - p0*(this->q[1] * this->q[1] + this->q[3] * this->q[3]) + 
                this->q[0]*(this->q[0] * this->q[0] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3])) - 
             this->mRes*(8*pow(p0,3)*pow(this->q[0],3) + 
                (this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3])*
                 (this->q[1] * this->q[1] + this->q[3] * this->q[3])*
                 (this->q[0] * this->q[0] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3]) - 
                4*pow(p0,2)*
                 (8*pow(this->q[0],4) + 
                   (this->q[1] * this->q[1] + this->q[3] * this->q[3])*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) - 
                   this->q[0] * this->q[0]*
                    (5*this->q[1] * this->q[1] + 5*this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1] + 2*this->q[3]*this->kg[3]))  
+ 2*p0*this->q[0]*(7*pow(this->q[0],4) + 6*(this->q[1] * this->q[1] + this->q[3] * this->q[3])*
                    (this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) - 
                   this->q[0] * this->q[0]*
                    (5*this->q[1] * this->q[1] + 5*this->q[3] * this->q[3] + 8*this->q[1]*this->kg[1] + 8*this->q[3]*this->kg[3])))) 
)))/(24.*this->mRes_2*pow(this->mn,5));
    
	tr[0][3] = (-16*C5v*C5vNC*pow(this->mRes,3)*p0*pow(this->q[0],3)*this->q[3] - 
    16*C3vNC*C5v*this->mRes_2*this->mn*p0*pow(this->q[0],3)*this->q[3] - 
    16*C3v*C5vNC*this->mRes_2*this->mn*p0*pow(this->q[0],3)*this->q[3] - 
    8*C5v*C5vNC*this->mRes_2*this->mn*p0*pow(this->q[0],3)*this->q[3] + 
    8*C3vNC*C5v*this->mRes*this->mn2*p0*pow(this->q[0],3)*this->q[3] + 
    8*C3v*C5vNC*this->mRes*this->mn2*p0*pow(this->q[0],3)*this->q[3] - 
    16*C3v*C3vNC*pow(this->mn,3)*p0*pow(this->q[0],3)*this->q[3] + 
    48*C5v*C5vNC*this->mRes*pow(p0,3)*pow(this->q[0],3)*this->q[3] + 
    16*C3vNC*C5v*this->mn*pow(p0,3)*pow(this->q[0],3)*this->q[3] + 
    16*C3v*C5vNC*this->mn*pow(p0,3)*pow(this->q[0],3)*this->q[3] + 
    32*C5v*C5vNC*this->mn*pow(p0,3)*pow(this->q[0],3)*this->q[3] + 
    8*C3vNC*C5v*this->mRes_2*this->mn*pow(this->q[0],4)*this->q[3] + 
    16*C3v*C5vNC*this->mRes_2*this->mn*pow(this->q[0],4)*this->q[3] + 
    8*C5v*C5vNC*this->mRes_2*this->mn*pow(this->q[0],4)*this->q[3] - 
    8*C3vNC*C5v*this->mRes*this->mn2*pow(this->q[0],4)*this->q[3] + 
    16*C3v*C3vNC*pow(this->mn,3)*pow(this->q[0],4)*this->q[3] - 
    64*C5v*C5vNC*this->mRes*pow(p0,2)*pow(this->q[0],4)*this->q[3] - 
    40*C3vNC*C5v*this->mn*pow(p0,2)*pow(this->q[0],4)*this->q[3] - 
    48*C3v*C5vNC*this->mn*pow(p0,2)*pow(this->q[0],4)*this->q[3] - 
    64*C5v*C5vNC*this->mn*pow(p0,2)*pow(this->q[0],4)*this->q[3] + 
    16*C5v*C5vNC*this->mRes*p0*pow(this->q[0],5)*this->q[3] + 
    36*C3vNC*C5v*this->mn*p0*pow(this->q[0],5)*this->q[3] + 
    44*C3v*C5vNC*this->mn*p0*pow(this->q[0],5)*this->q[3] + 
    40*C5v*C5vNC*this->mn*p0*pow(this->q[0],5)*this->q[3] - 10*C3vNC*C5v*this->mn*pow(this->q[0],6)*this->q[3] - 
    16*C3v*C5vNC*this->mn*pow(this->q[0],6)*this->q[3] - 8*C5v*C5vNC*this->mn*pow(this->q[0],6)*this->q[3] - 
    4*C3v*C5vNC*this->mRes*this->mn2*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3] + 
    4*C5v*C5vNC*this->mRes*pow(p0,2)*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3] + 
    8*C3v*C5vNC*this->mn*pow(p0,2)*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3] - 
    12*C5v*C5vNC*this->mRes*p0*pow(this->q[0],3)*this->q[1] * this->q[1]*this->q[3] - 
    12*C3vNC*C5v*this->mn*p0*pow(this->q[0],3)*this->q[1] * this->q[1]*this->q[3] - 
    12*C3v*C5vNC*this->mn*p0*pow(this->q[0],3)*this->q[1] * this->q[1]*this->q[3] + 
    C5v*C5vNC*this->mRes*pow(this->q[0],4)*this->q[1] * this->q[1]*this->q[3] + 
    2*C3vNC*C5v*this->mn*pow(this->q[0],4)*this->q[1] * this->q[1]*this->q[3] + 
    10*C3v*C5vNC*this->mn*pow(this->q[0],4)*this->q[1] * this->q[1]*this->q[3] - 
    C5v*C5vNC*this->mRes*this->q[0] * this->q[0]*pow(this->q[1],4)*this->q[3] - 
    2*C3v*C5vNC*this->mn*this->q[0] * this->q[0]*pow(this->q[1],4)*this->q[3] - 
    4*C3vNC*C5v*this->mn*p0*pow(this->q[0],3)*pow(this->q[3],3) + 
    4*C3v*C5vNC*this->mn*p0*pow(this->q[0],3)*pow(this->q[3],3) + 
    2*C3vNC*C5v*this->mn*pow(this->q[0],4)*pow(this->q[3],3) - 
    C5v*C5vNC*this->mRes*this->q[0] * this->q[0]*this->q[1] * this->q[1]*pow(this->q[3],3) - 
    2*C3v*C5vNC*this->mn*this->q[0] * this->q[0]*this->q[1] * this->q[1]*pow(this->q[3],3) - 
    8*C3v*C3vNC*pow(this->mRes,3)*this->mn2*this->q[1]*this->q[3]*this->kg[1] + 
    8*C3v*C5vNC*pow(this->mRes,3)*this->mn2*this->q[1]*this->q[3]*this->kg[1] - 
    8*C3v*C3vNC*this->mRes_2*pow(this->mn,3)*this->q[1]*this->q[3]*this->kg[1] + 
    16*C5v*C5vNC*pow(this->mRes,3)*pow(p0,2)*this->q[1]*this->q[3]*this->kg[1] - 
    8*C3vNC*C5v*this->mRes_2*this->mn*pow(p0,2)*this->q[1]*this->q[3]*this->kg[1] + 
    16*C3v*C5vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[1]*this->q[3]*this->kg[1] + 
    16*C5v*C5vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[1]*this->q[3]*this->kg[1] + 
    16*C3v*C3vNC*this->mRes*this->mn2*pow(p0,2)*this->q[1]*this->q[3]*this->kg[1] - 
    8*C3vNC*C5v*this->mRes*this->mn2*pow(p0,2)*this->q[1]*this->q[3]*this->kg[1] + 
    8*C3v*C5vNC*this->mRes*this->mn2*pow(p0,2)*this->q[1]*this->q[3]*this->kg[1] - 
    16*C3v*C3vNC*pow(this->mn,3)*pow(p0,2)*this->q[1]*this->q[3]*this->kg[1] + 
    16*C5v*C5vNC*pow(this->mRes,3)*p0*this->q[0]*this->q[1]*this->q[3]*this->kg[1] - 
    12*C3vNC*C5v*this->mRes_2*this->mn*p0*this->q[0]*this->q[1]*this->q[3]*this->kg[1] + 
    4*C3v*C5vNC*this->mRes_2*this->mn*p0*this->q[0]*this->q[1]*this->q[3]*this->kg[1] - 
    8*C5v*C5vNC*this->mRes_2*this->mn*p0*this->q[0]*this->q[1]*this->q[3]*this->kg[1] - 
    32*C3v*C3vNC*this->mRes*this->mn2*p0*this->q[0]*this->q[1]*this->q[3]*this->kg[1] + 
    4*C3vNC*C5v*this->mRes*this->mn2*p0*this->q[0]*this->q[1]*this->q[3]*this->kg[1] - 
    4*C3v*C5vNC*this->mRes*this->mn2*p0*this->q[0]*this->q[1]*this->q[3]*this->kg[1] + 
    24*C3v*C3vNC*pow(this->mn,3)*p0*this->q[0]*this->q[1]*this->q[3]*this->kg[1] - 
    8*C5v*C5vNC*this->mRes*pow(p0,3)*this->q[0]*this->q[1]*this->q[3]*this->kg[1] - 
    16*C3v*C5vNC*this->mn*pow(p0,3)*this->q[0]*this->q[1]*this->q[3]*this->kg[1] - 
    12*C3vNC*C5v*this->mRes_2*this->mn*this->q[0] * this->q[0]*this->q[1]*this->q[3]*this->kg[1] - 
    10*C3v*C5vNC*this->mRes_2*this->mn*this->q[0] * this->q[0]*this->q[1]*this->q[3]*this->kg[1] - 
    8*C5v*C5vNC*this->mRes_2*this->mn*this->q[0] * this->q[0]*this->q[1]*this->q[3]*this->kg[1] + 
    20*C3v*C3vNC*this->mRes*this->mn2*this->q[0] * this->q[0]*this->q[1]*this->q[3]*this->kg[1] + 
    8*C3vNC*C5v*this->mRes*this->mn2*this->q[0] * this->q[0]*this->q[1]*this->q[3]*this->kg[1] + 
    4*C3v*C5vNC*this->mRes*this->mn2*this->q[0] * this->q[0]*this->q[1]*this->q[3]*this->kg[1] - 
    8*C3v*C3vNC*pow(this->mn,3)*this->q[0] * this->q[0]*this->q[1]*this->q[3]*this->kg[1] + 
    52*C5v*C5vNC*this->mRes*pow(p0,2)*this->q[0] * this->q[0]*this->q[1]*this->q[3]*this->kg[1] + 
    24*C3vNC*C5v*this->mn*pow(p0,2)*this->q[0] * this->q[0]*this->q[1]*this->q[3]*this->kg[1] + 
    48*C3v*C5vNC*this->mn*pow(p0,2)*this->q[0] * this->q[0]*this->q[1]*this->q[3]*this->kg[1] + 
    32*C5v*C5vNC*this->mn*pow(p0,2)*this->q[0] * this->q[0]*this->q[1]*this->q[3]*this->kg[1] - 
    30*C5v*C5vNC*this->mRes*p0*pow(this->q[0],3)*this->q[1]*this->q[3]*this->kg[1] - 
    16*C3vNC*C5v*this->mn*p0*pow(this->q[0],3)*this->q[1]*this->q[3]*this->kg[1] - 
    52*C3v*C5vNC*this->mn*p0*pow(this->q[0],3)*this->q[1]*this->q[3]*this->kg[1] - 
    48*C5v*C5vNC*this->mn*p0*pow(this->q[0],3)*this->q[1]*this->q[3]*this->kg[1] + 
    C5v*C5vNC*this->mRes*pow(this->q[0],4)*this->q[1]*this->q[3]*this->kg[1] + 
    18*C3vNC*C5v*this->mn*pow(this->q[0],4)*this->q[1]*this->q[3]*this->kg[1] + 
    16*C3v*C5vNC*this->mn*pow(this->q[0],4)*this->q[1]*this->q[3]*this->kg[1] + 
    16*C5v*C5vNC*this->mn*pow(this->q[0],4)*this->q[1]*this->q[3]*this->kg[1] + 
    2*C3v*C5vNC*this->mRes_2*this->mn*pow(this->q[1],3)*this->q[3]*this->kg[1] - 
    4*C3v*C3vNC*this->mRes*this->mn2*pow(this->q[1],3)*this->q[3]*this->kg[1] + 
    2*C5v*C5vNC*this->mRes*p0*this->q[0]*pow(this->q[1],3)*this->q[3]*this->kg[1] + 
    4*C3v*C5vNC*this->mn*p0*this->q[0]*pow(this->q[1],3)*this->q[3]*this->kg[1] - 
    C5v*C5vNC*this->mRes*this->q[0] * this->q[0]*pow(this->q[1],3)*this->q[3]*this->kg[1] - 
    2*C3vNC*C5v*this->mn*this->q[0] * this->q[0]*pow(this->q[1],3)*this->q[3]*this->kg[1] + 
    2*C3v*C5vNC*this->mRes_2*this->mn*this->q[1]*pow(this->q[3],3)*this->kg[1] - 
    4*C3v*C3vNC*this->mRes*this->mn2*this->q[1]*pow(this->q[3],3)*this->kg[1] + 
    2*C5v*C5vNC*this->mRes*p0*this->q[0]*this->q[1]*pow(this->q[3],3)*this->kg[1] + 
    4*C3v*C5vNC*this->mn*p0*this->q[0]*this->q[1]*pow(this->q[3],3)*this->kg[1] - 
    C5v*C5vNC*this->mRes*this->q[0] * this->q[0]*this->q[1]*pow(this->q[3],3)*this->kg[1] - 
    2*C3vNC*C5v*this->mn*this->q[0] * this->q[0]*this->q[1]*pow(this->q[3],3)*this->kg[1] + 
    20*C3vNC*C5v*this->mRes_2*this->mn*p0*this->q[0]*this->q[3]*this->kg[1] * this->kg[1] + 
    20*C3v*C5vNC*this->mRes_2*this->mn*p0*this->q[0]*this->q[3]*this->kg[1] * this->kg[1] + 
    16*C3v*C3vNC*this->mRes*this->mn2*p0*this->q[0]*this->q[3]*this->kg[1] * this->kg[1] + 
    4*C3vNC*C5v*this->mRes*this->mn2*p0*this->q[0]*this->q[3]*this->kg[1] * this->kg[1] + 
    4*C3v*C5vNC*this->mRes*this->mn2*p0*this->q[0]*this->q[3]*this->kg[1] * this->kg[1] + 
    8*C3v*C3vNC*pow(this->mn,3)*p0*this->q[0]*this->q[3]*this->kg[1] * this->kg[1] - 
    8*C3v*C5vNC*this->mRes_2*this->mn*this->q[0] * this->q[0]*this->q[3]*this->kg[1] * this->kg[1] - 
    16*C3v*C3vNC*this->mRes*this->mn2*this->q[0] * this->q[0]*this->q[3]*this->kg[1] * this->kg[1] - 
    8*C3v*C5vNC*this->mRes*this->mn2*this->q[0] * this->q[0]*this->q[3]*this->kg[1] * this->kg[1] - 
    8*C3v*C3vNC*pow(this->mn,3)*this->q[0] * this->q[0]*this->q[3]*this->kg[1] * this->kg[1] + 
    8*C5v*C5vNC*this->mRes*pow(p0,2)*this->q[0] * this->q[0]*this->q[3]*this->kg[1] * this->kg[1] + 
    8*C3v*C5vNC*this->mn*pow(p0,2)*this->q[0] * this->q[0]*this->q[3]*this->kg[1] * this->kg[1] - 
    16*C3v*C5vNC*this->mn*p0*pow(this->q[0],3)*this->q[3]*this->kg[1] * this->kg[1] + 
    10*C3v*C5vNC*this->mn*pow(this->q[0],4)*this->q[3]*this->kg[1] * this->kg[1] + 
    4*C3vNC*C5v*this->mRes_2*this->mn*this->q[1] * this->q[1]*this->q[3]*this->kg[1] * this->kg[1] + 
    8*C3v*C5vNC*this->mRes*this->mn2*this->q[1] * this->q[1]*this->q[3]*this->kg[1] * this->kg[1] - 
    8*C5v*C5vNC*this->mRes*pow(p0,2)*this->q[1] * this->q[1]*this->q[3]*this->kg[1] * this->kg[1] - 
    16*C3v*C5vNC*this->mn*pow(p0,2)*this->q[1] * this->q[1]*this->q[3]*this->kg[1] * this->kg[1] + 
    24*C5v*C5vNC*this->mRes*p0*this->q[0]*this->q[1] * this->q[1]*this->q[3]*this->kg[1] * this->kg[1] + 
    16*C3vNC*C5v*this->mn*p0*this->q[0]*this->q[1] * this->q[1]*this->q[3]*this->kg[1] * this->kg[1] + 
    32*C3v*C5vNC*this->mn*p0*this->q[0]*this->q[1] * this->q[1]*this->q[3]*this->kg[1] * this->kg[1] + 
    8*C5v*C5vNC*this->mn*p0*this->q[0]*this->q[1] * this->q[1]*this->q[3]*this->kg[1] * this->kg[1] - 
    2*C5v*C5vNC*this->mRes*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3]*this->kg[1] * this->kg[1] - 
    8*C3vNC*C5v*this->mn*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3]*this->kg[1] * this->kg[1] - 
    22*C3v*C5vNC*this->mn*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3]*this->kg[1] * this->kg[1] - 
    8*C5v*C5vNC*this->mn*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3]*this->kg[1] * this->kg[1] + 
    2*C5v*C5vNC*this->mRes*pow(this->q[1],4)*this->q[3]*this->kg[1] * this->kg[1] + 
    4*C3v*C5vNC*this->mn*pow(this->q[1],4)*this->q[3]*this->kg[1] * this->kg[1] - 
    2*C3v*C5vNC*this->mn*this->q[0] * this->q[0]*pow(this->q[3],3)*this->kg[1] * this->kg[1] + 
    2*C5v*C5vNC*this->mRes*this->q[1] * this->q[1]*pow(this->q[3],3)*this->kg[1] * this->kg[1] + 
    4*C3v*C5vNC*this->mn*this->q[1] * this->q[1]*pow(this->q[3],3)*this->kg[1] * this->kg[1] - 
    24*C3vNC*C5v*this->mn*p0*this->q[0]*this->q[1]*this->q[3]*pow(this->kg[1],3) - 
    std::complex<double>(0,8)*C5a*C5v*pow(this->mRes,3)*this->mn2*p0*this->q[1]*this->kg[2] - 
    std::complex<double>(0,24)*C3v*C5a*this->mRes_2*pow(this->mn,3)*p0*this->q[1]*this->kg[2] - 
    std::complex<double>(0,8)*C5a*C5v*this->mRes_2*pow(this->mn,3)*p0*this->q[1]*this->kg[2] + 
    std::complex<double>(0,8)*C3v*C5a*this->mRes*pow(this->mn,4)*p0*this->q[1]*this->kg[2] + 
    std::complex<double>(0,8)*C3v*C5a*this->mRes_2*pow(this->mn,3)*this->q[0]*this->q[1]*this->kg[2] - 
    std::complex<double>(0,8)*C3v*C5a*this->mRes*pow(this->mn,4)*this->q[0]*this->q[1]*this->kg[2] - 
    std::complex<double>(0,8)*C5a*C5v*this->mRes*this->mn2*p0*this->q[0] * this->q[0]*this->q[1]*this->kg[2] + 
    std::complex<double>(0,4)*C5a*C5v*this->mRes*this->mn2*pow(this->q[0],3)*this->q[1]*this->kg[2] - 
    std::complex<double>(0,4)*C5a*C5v*this->mRes*this->mn2*this->q[0]*this->q[1] * this->q[1]*this->kg[1]*this->kg[2] + 
    20*C3vNC*C5v*this->mRes_2*this->mn*p0*this->q[0]*this->q[3]*this->kg[2] * this->kg[2] + 
    20*C3v*C5vNC*this->mRes_2*this->mn*p0*this->q[0]*this->q[3]*this->kg[2] * this->kg[2] + 
    16*C3v*C3vNC*this->mRes*this->mn2*p0*this->q[0]*this->q[3]*this->kg[2] * this->kg[2] + 
    4*C3vNC*C5v*this->mRes*this->mn2*p0*this->q[0]*this->q[3]*this->kg[2] * this->kg[2] + 
    4*C3v*C5vNC*this->mRes*this->mn2*p0*this->q[0]*this->q[3]*this->kg[2] * this->kg[2] + 
    8*C3v*C3vNC*pow(this->mn,3)*p0*this->q[0]*this->q[3]*this->kg[2] * this->kg[2] - 
    8*C3v*C5vNC*this->mRes_2*this->mn*this->q[0] * this->q[0]*this->q[3]*this->kg[2] * this->kg[2] - 
    16*C3v*C3vNC*this->mRes*this->mn2*this->q[0] * this->q[0]*this->q[3]*this->kg[2] * this->kg[2] - 
    8*C3v*C5vNC*this->mRes*this->mn2*this->q[0] * this->q[0]*this->q[3]*this->kg[2] * this->kg[2] - 
    8*C3v*C3vNC*pow(this->mn,3)*this->q[0] * this->q[0]*this->q[3]*this->kg[2] * this->kg[2] + 
    8*C5v*C5vNC*this->mRes*pow(p0,2)*this->q[0] * this->q[0]*this->q[3]*this->kg[2] * this->kg[2] + 
    8*C3v*C5vNC*this->mn*pow(p0,2)*this->q[0] * this->q[0]*this->q[3]*this->kg[2] * this->kg[2] - 
    16*C3v*C5vNC*this->mn*p0*pow(this->q[0],3)*this->q[3]*this->kg[2] * this->kg[2] + 
    10*C3v*C5vNC*this->mn*pow(this->q[0],4)*this->q[3]*this->kg[2] * this->kg[2] + 
    4*C3v*C5vNC*this->mRes*this->mn2*this->q[1] * this->q[1]*this->q[3]*this->kg[2] * this->kg[2] - 
    4*C5v*C5vNC*this->mRes*pow(p0,2)*this->q[1] * this->q[1]*this->q[3]*this->kg[2] * this->kg[2] - 
    8*C3v*C5vNC*this->mn*pow(p0,2)*this->q[1] * this->q[1]*this->q[3]*this->kg[2] * this->kg[2] + 
    12*C5v*C5vNC*this->mRes*p0*this->q[0]*this->q[1] * this->q[1]*this->q[3]*this->kg[2] * this->kg[2] + 
    8*C3vNC*C5v*this->mn*p0*this->q[0]*this->q[1] * this->q[1]*this->q[3]*this->kg[2] * this->kg[2] + 
    16*C3v*C5vNC*this->mn*p0*this->q[0]*this->q[1] * this->q[1]*this->q[3]*this->kg[2] * this->kg[2] - 
    C5v*C5vNC*this->mRes*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3]*this->kg[2] * this->kg[2] - 
    12*C3v*C5vNC*this->mn*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3]*this->kg[2] * this->kg[2] + 
    C5v*C5vNC*this->mRes*pow(this->q[1],4)*this->q[3]*this->kg[2] * this->kg[2] + 
    2*C3v*C5vNC*this->mn*pow(this->q[1],4)*this->q[3]*this->kg[2] * this->kg[2] - 
    2*C3v*C5vNC*this->mn*this->q[0] * this->q[0]*pow(this->q[3],3)*this->kg[2] * this->kg[2] + 
    C5v*C5vNC*this->mRes*this->q[1] * this->q[1]*pow(this->q[3],3)*this->kg[2] * this->kg[2] + 
    2*C3v*C5vNC*this->mn*this->q[1] * this->q[1]*pow(this->q[3],3)*this->kg[2] * this->kg[2] - 
    24*C3vNC*C5v*this->mn*p0*this->q[0]*this->q[1]*this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] + 
    32*C3v*C3vNC*pow(this->mRes,3)*this->mn2*p0*this->q[0]*this->kg[3] + 
    16*C3vNC*C5v*pow(this->mRes,3)*this->mn2*p0*this->q[0]*this->kg[3] + 
    16*C3v*C5vNC*pow(this->mRes,3)*this->mn2*p0*this->q[0]*this->kg[3] + 
    32*C5v*C5vNC*pow(this->mRes,3)*pow(p0,3)*this->q[0]*this->kg[3] + 
    32*C3vNC*C5v*this->mRes_2*this->mn*pow(p0,3)*this->q[0]*this->kg[3] + 
    32*C3v*C5vNC*this->mRes_2*this->mn*pow(p0,3)*this->q[0]*this->kg[3] + 
    32*C5v*C5vNC*this->mRes_2*this->mn*pow(p0,3)*this->q[0]*this->kg[3] + 
    16*C3vNC*C5v*this->mRes*this->mn2*pow(p0,3)*this->q[0]*this->kg[3] + 
    16*C3v*C5vNC*this->mRes*this->mn2*pow(p0,3)*this->q[0]*this->kg[3] + 
    32*C3v*C3vNC*pow(this->mn,3)*pow(p0,3)*this->q[0]*this->kg[3] - 
    32*C3vNC*C5v*this->mRes_2*this->mn*pow(p0,2)*this->q[0] * this->q[0]*this->kg[3] - 
    32*C3v*C5vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[0] * this->q[0]*this->kg[3] - 
    32*C5v*C5vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[0] * this->q[0]*this->kg[3] - 
    32*C3vNC*C5v*this->mRes*this->mn2*pow(p0,2)*this->q[0] * this->q[0]*this->kg[3] - 
    32*C3v*C5vNC*this->mRes*this->mn2*pow(p0,2)*this->q[0] * this->q[0]*this->kg[3] - 
    96*C3v*C3vNC*pow(this->mn,3)*pow(p0,2)*this->q[0] * this->q[0]*this->kg[3] + 
    16*C5v*C5vNC*pow(this->mRes,3)*p0*pow(this->q[0],3)*this->kg[3] + 
    24*C3vNC*C5v*this->mRes_2*this->mn*p0*pow(this->q[0],3)*this->kg[3] - 
    8*C3v*C5vNC*this->mRes_2*this->mn*p0*pow(this->q[0],3)*this->kg[3] + 
    8*C5v*C5vNC*this->mRes_2*this->mn*p0*pow(this->q[0],3)*this->kg[3] + 
    28*C3vNC*C5v*this->mRes*this->mn2*p0*pow(this->q[0],3)*this->kg[3] + 
    12*C3v*C5vNC*this->mRes*this->mn2*p0*pow(this->q[0],3)*this->kg[3] + 
    88*C3v*C3vNC*pow(this->mn,3)*p0*pow(this->q[0],3)*this->kg[3] - 
    8*C5v*C5vNC*this->mRes*pow(p0,3)*pow(this->q[0],3)*this->kg[3] - 
    16*C3vNC*C5v*this->mn*pow(p0,3)*pow(this->q[0],3)*this->kg[3] - 
    8*C3vNC*C5v*this->mRes_2*this->mn*pow(this->q[0],4)*this->kg[3] - 
    8*C3v*C5vNC*this->mRes_2*this->mn*pow(this->q[0],4)*this->kg[3] - 
    8*C5v*C5vNC*this->mRes_2*this->mn*pow(this->q[0],4)*this->kg[3] - 
    8*C3vNC*C5v*this->mRes*this->mn2*pow(this->q[0],4)*this->kg[3] + 
    8*C3v*C5vNC*this->mRes*this->mn2*pow(this->q[0],4)*this->kg[3] - 
    24*C3v*C3vNC*pow(this->mn,3)*pow(this->q[0],4)*this->kg[3] + 
    32*C5v*C5vNC*this->mRes*pow(p0,2)*pow(this->q[0],4)*this->kg[3] + 
    40*C3vNC*C5v*this->mn*pow(p0,2)*pow(this->q[0],4)*this->kg[3] + 
    8*C3v*C5vNC*this->mn*pow(p0,2)*pow(this->q[0],4)*this->kg[3] + 
    16*C5v*C5vNC*this->mn*pow(p0,2)*pow(this->q[0],4)*this->kg[3] - 
    14*C5v*C5vNC*this->mRes*p0*pow(this->q[0],5)*this->kg[3] - 
    28*C3vNC*C5v*this->mn*p0*pow(this->q[0],5)*this->kg[3] - 
    16*C3v*C5vNC*this->mn*p0*pow(this->q[0],5)*this->kg[3] - 
    24*C5v*C5vNC*this->mn*p0*pow(this->q[0],5)*this->kg[3] + 6*C3vNC*C5v*this->mn*pow(this->q[0],6)*this->kg[3] + 
    6*C3v*C5vNC*this->mn*pow(this->q[0],6)*this->kg[3] + 8*C5v*C5vNC*this->mn*pow(this->q[0],6)*this->kg[3] + 
    8*C3v*C3vNC*pow(this->mRes,3)*this->mn2*this->q[1] * this->q[1]*this->kg[3] - 
    8*C3v*C5vNC*pow(this->mRes,3)*this->mn2*this->q[1] * this->q[1]*this->kg[3] + 
    8*C3v*C3vNC*this->mRes_2*pow(this->mn,3)*this->q[1] * this->q[1]*this->kg[3] - 
    16*C5v*C5vNC*pow(this->mRes,3)*pow(p0,2)*this->q[1] * this->q[1]*this->kg[3] + 
    8*C3vNC*C5v*this->mRes_2*this->mn*pow(p0,2)*this->q[1] * this->q[1]*this->kg[3] - 
    16*C3v*C5vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[1] * this->q[1]*this->kg[3] - 
    16*C5v*C5vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[1] * this->q[1]*this->kg[3] - 
    16*C3v*C3vNC*this->mRes*this->mn2*pow(p0,2)*this->q[1] * this->q[1]*this->kg[3] + 
    8*C3vNC*C5v*this->mRes*this->mn2*pow(p0,2)*this->q[1] * this->q[1]*this->kg[3] - 
    8*C3v*C5vNC*this->mRes*this->mn2*pow(p0,2)*this->q[1] * this->q[1]*this->kg[3] + 
    16*C3v*C3vNC*pow(this->mn,3)*pow(p0,2)*this->q[1] * this->q[1]*this->kg[3] + 
    4*C3vNC*C5v*this->mRes_2*this->mn*p0*this->q[0]*this->q[1] * this->q[1]*this->kg[3] + 
    20*C3v*C5vNC*this->mRes_2*this->mn*p0*this->q[0]*this->q[1] * this->q[1]*this->kg[3] + 
    16*C5v*C5vNC*this->mRes_2*this->mn*p0*this->q[0]*this->q[1] * this->q[1]*this->kg[3] + 
    32*C3v*C3vNC*this->mRes*this->mn2*p0*this->q[0]*this->q[1] * this->q[1]*this->kg[3] - 
    8*C3vNC*C5v*this->mRes*this->mn2*p0*this->q[0]*this->q[1] * this->q[1]*this->kg[3] + 
    16*C3v*C5vNC*this->mRes*this->mn2*p0*this->q[0]*this->q[1] * this->q[1]*this->kg[3] - 
    32*C3v*C3vNC*pow(this->mn,3)*p0*this->q[0]*this->q[1] * this->q[1]*this->kg[3] + 
    4*C3vNC*C5v*this->mRes_2*this->mn*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[3] + 
    2*C3v*C5vNC*this->mRes_2*this->mn*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[3] - 
    20*C3v*C3vNC*this->mRes*this->mn2*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[3] - 
    12*C3v*C5vNC*this->mRes*this->mn2*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[3] + 
    16*C3v*C3vNC*pow(this->mn,3)*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[3] - 
    20*C5v*C5vNC*this->mRes*pow(p0,2)*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[3] - 
    16*C3vNC*C5v*this->mn*pow(p0,2)*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[3] - 
    8*C3v*C5vNC*this->mn*pow(p0,2)*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[3] - 
    16*C5v*C5vNC*this->mn*pow(p0,2)*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[3] + 
    10*C5v*C5vNC*this->mRes*p0*pow(this->q[0],3)*this->q[1] * this->q[1]*this->kg[3] + 
    20*C3vNC*C5v*this->mn*p0*pow(this->q[0],3)*this->q[1] * this->q[1]*this->kg[3] + 
    16*C3v*C5vNC*this->mn*p0*pow(this->q[0],3)*this->q[1] * this->q[1]*this->kg[3] + 
    24*C5v*C5vNC*this->mn*p0*pow(this->q[0],3)*this->q[1] * this->q[1]*this->kg[3] - 
    C5v*C5vNC*this->mRes*pow(this->q[0],4)*this->q[1] * this->q[1]*this->kg[3] - 
    6*C3vNC*C5v*this->mn*pow(this->q[0],4)*this->q[1] * this->q[1]*this->kg[3] - 
    4*C3v*C5vNC*this->mn*pow(this->q[0],4)*this->q[1] * this->q[1]*this->kg[3] - 
    8*C5v*C5vNC*this->mn*pow(this->q[0],4)*this->q[1] * this->q[1]*this->kg[3] - 
    2*C3v*C5vNC*this->mRes_2*this->mn*pow(this->q[1],4)*this->kg[3] + 
    4*C3v*C3vNC*this->mRes*this->mn2*pow(this->q[1],4)*this->kg[3] + 
    C5v*C5vNC*this->mRes*this->q[0] * this->q[0]*pow(this->q[1],4)*this->kg[3] - 
    2*C3v*C5vNC*this->mn*this->q[0] * this->q[0]*pow(this->q[1],4)*this->kg[3] + 
    16*C5v*C5vNC*pow(this->mRes,3)*p0*this->q[0]*this->q[3] * this->q[3]*this->kg[3] - 
    8*C3vNC*C5v*this->mRes_2*this->mn*p0*this->q[0]*this->q[3] * this->q[3]*this->kg[3] + 
    24*C3v*C5vNC*this->mRes_2*this->mn*p0*this->q[0]*this->q[3] * this->q[3]*this->kg[3] + 
    8*C5v*C5vNC*this->mRes_2*this->mn*p0*this->q[0]*this->q[3] * this->q[3]*this->kg[3] - 
    4*C3vNC*C5v*this->mRes*this->mn2*p0*this->q[0]*this->q[3] * this->q[3]*this->kg[3] + 
    12*C3v*C5vNC*this->mRes*this->mn2*p0*this->q[0]*this->q[3] * this->q[3]*this->kg[3] - 
    8*C3v*C3vNC*pow(this->mn,3)*p0*this->q[0]*this->q[3] * this->q[3]*this->kg[3] - 
    8*C5v*C5vNC*this->mRes*pow(p0,3)*this->q[0]*this->q[3] * this->q[3]*this->kg[3] - 
    16*C3v*C5vNC*this->mn*pow(p0,3)*this->q[0]*this->q[3] * this->q[3]*this->kg[3] - 
    8*C3vNC*C5v*this->mRes_2*this->mn*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[3] - 
    8*C3v*C5vNC*this->mRes_2*this->mn*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[3] - 
    8*C5v*C5vNC*this->mRes_2*this->mn*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[3] + 
    8*C3vNC*C5v*this->mRes*this->mn2*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[3] - 
    8*C3v*C5vNC*this->mRes*this->mn2*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[3] + 
    8*C3v*C3vNC*pow(this->mn,3)*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[3] + 
    32*C5v*C5vNC*this->mRes*pow(p0,2)*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[3] + 
    8*C3vNC*C5v*this->mn*pow(p0,2)*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[3] + 
    40*C3v*C5vNC*this->mn*pow(p0,2)*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[3] + 
    16*C5v*C5vNC*this->mn*pow(p0,2)*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[3] - 
    20*C5v*C5vNC*this->mRes*p0*pow(this->q[0],3)*this->q[3] * this->q[3]*this->kg[3] - 
    20*C3vNC*C5v*this->mn*p0*pow(this->q[0],3)*this->q[3] * this->q[3]*this->kg[3] - 
    36*C3v*C5vNC*this->mn*p0*pow(this->q[0],3)*this->q[3] * this->q[3]*this->kg[3] - 
    24*C5v*C5vNC*this->mn*p0*pow(this->q[0],3)*this->q[3] * this->q[3]*this->kg[3] + 
    12*C3vNC*C5v*this->mn*pow(this->q[0],4)*this->q[3] * this->q[3]*this->kg[3] + 
    12*C3v*C5vNC*this->mn*pow(this->q[0],4)*this->q[3] * this->q[3]*this->kg[3] + 
    8*C5v*C5vNC*this->mn*pow(this->q[0],4)*this->q[3] * this->q[3]*this->kg[3] - 
    2*C3v*C5vNC*this->mRes_2*this->mn*this->q[1] * this->q[1]*this->q[3] * this->q[3]*this->kg[3] + 
    4*C3v*C3vNC*this->mRes*this->mn2*this->q[1] * this->q[1]*this->q[3] * this->q[3]*this->kg[3] + 
    2*C5v*C5vNC*this->mRes*p0*this->q[0]*this->q[1] * this->q[1]*this->q[3] * this->q[3]*this->kg[3] + 
    4*C3v*C5vNC*this->mn*p0*this->q[0]*this->q[1] * this->q[1]*this->q[3] * this->q[3]*this->kg[3] + 
    C5v*C5vNC*this->mRes*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3] * this->q[3]*this->kg[3] - 
    2*C3vNC*C5v*this->mn*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3] * this->q[3]*this->kg[3] - 
    4*C3v*C5vNC*this->mn*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3] * this->q[3]*this->kg[3] + 
    2*C5v*C5vNC*this->mRes*p0*this->q[0]*pow(this->q[3],4)*this->kg[3] + 
    4*C3v*C5vNC*this->mn*p0*this->q[0]*pow(this->q[3],4)*this->kg[3] - 
    2*C3vNC*C5v*this->mn*this->q[0] * this->q[0]*pow(this->q[3],4)*this->kg[3] - 
    2*C3v*C5vNC*this->mn*this->q[0] * this->q[0]*pow(this->q[3],4)*this->kg[3] - 
    16*C5v*C5vNC*pow(this->mRes,3)*p0*this->q[0]*this->q[1]*this->kg[1]*this->kg[3] - 
    20*C3vNC*C5v*this->mRes_2*this->mn*p0*this->q[0]*this->q[1]*this->kg[1]*this->kg[3] - 
    20*C3v*C5vNC*this->mRes_2*this->mn*p0*this->q[0]*this->q[1]*this->kg[1]*this->kg[3] - 
    8*C5v*C5vNC*this->mRes_2*this->mn*p0*this->q[0]*this->q[1]*this->kg[1]*this->kg[3] - 
    16*C3v*C3vNC*this->mRes*this->mn2*p0*this->q[0]*this->q[1]*this->kg[1]*this->kg[3] - 
    4*C3vNC*C5v*this->mRes*this->mn2*p0*this->q[0]*this->q[1]*this->kg[1]*this->kg[3] - 
    4*C3v*C5vNC*this->mRes*this->mn2*p0*this->q[0]*this->q[1]*this->kg[1]*this->kg[3] - 
    8*C3v*C3vNC*pow(this->mn,3)*p0*this->q[0]*this->q[1]*this->kg[1]*this->kg[3] + 
    8*C3vNC*C5v*this->mRes_2*this->mn*this->q[0] * this->q[0]*this->q[1]*this->kg[1]*this->kg[3] + 
    8*C3v*C5vNC*this->mRes_2*this->mn*this->q[0] * this->q[0]*this->q[1]*this->kg[1]*this->kg[3] + 
    8*C5v*C5vNC*this->mRes_2*this->mn*this->q[0] * this->q[0]*this->q[1]*this->kg[1]*this->kg[3] + 
    16*C3v*C3vNC*this->mRes*this->mn2*this->q[0] * this->q[0]*this->q[1]*this->kg[1]*this->kg[3] + 
    8*C3vNC*C5v*this->mRes*this->mn2*this->q[0] * this->q[0]*this->q[1]*this->kg[1]*this->kg[3] + 
    8*C3v*C5vNC*this->mRes*this->mn2*this->q[0] * this->q[0]*this->q[1]*this->kg[1]*this->kg[3] + 
    8*C3v*C3vNC*pow(this->mn,3)*this->q[0] * this->q[0]*this->q[1]*this->kg[1]*this->kg[3] - 
    8*C5v*C5vNC*this->mRes*pow(p0,2)*this->q[0] * this->q[0]*this->q[1]*this->kg[1]*this->kg[3] - 
    8*C3vNC*C5v*this->mn*pow(p0,2)*this->q[0] * this->q[0]*this->q[1]*this->kg[1]*this->kg[3] - 
    8*C3v*C5vNC*this->mn*pow(p0,2)*this->q[0] * this->q[0]*this->q[1]*this->kg[1]*this->kg[3] + 
    16*C5v*C5vNC*this->mRes*p0*pow(this->q[0],3)*this->q[1]*this->kg[1]*this->kg[3] + 
    16*C3vNC*C5v*this->mn*p0*pow(this->q[0],3)*this->q[1]*this->kg[1]*this->kg[3] + 
    16*C3v*C5vNC*this->mn*p0*pow(this->q[0],3)*this->q[1]*this->kg[1]*this->kg[3] + 
    8*C5v*C5vNC*this->mn*p0*pow(this->q[0],3)*this->q[1]*this->kg[1]*this->kg[3] - 
    6*C3vNC*C5v*this->mn*pow(this->q[0],4)*this->q[1]*this->kg[1]*this->kg[3] - 
    10*C3v*C5vNC*this->mn*pow(this->q[0],4)*this->q[1]*this->kg[1]*this->kg[3] - 
    8*C5v*C5vNC*this->mn*pow(this->q[0],4)*this->q[1]*this->kg[1]*this->kg[3] - 
    4*C3vNC*C5v*this->mRes_2*this->mn*pow(this->q[1],3)*this->kg[1]*this->kg[3] - 
    4*C3v*C5vNC*this->mRes*this->mn2*pow(this->q[1],3)*this->kg[1]*this->kg[3] + 
    4*C5v*C5vNC*this->mRes*pow(p0,2)*pow(this->q[1],3)*this->kg[1]*this->kg[3] + 
    8*C3v*C5vNC*this->mn*pow(p0,2)*pow(this->q[1],3)*this->kg[1]*this->kg[3] - 
    12*C5v*C5vNC*this->mRes*p0*this->q[0]*pow(this->q[1],3)*this->kg[1]*this->kg[3] - 
    8*C3vNC*C5v*this->mn*p0*this->q[0]*pow(this->q[1],3)*this->kg[1]*this->kg[3] - 
    16*C3v*C5vNC*this->mn*p0*this->q[0]*pow(this->q[1],3)*this->kg[1]*this->kg[3] - 
    8*C5v*C5vNC*this->mn*p0*this->q[0]*pow(this->q[1],3)*this->kg[1]*this->kg[3] + 
    C5v*C5vNC*this->mRes*this->q[0] * this->q[0]*pow(this->q[1],3)*this->kg[1]*this->kg[3] + 
    6*C3vNC*C5v*this->mn*this->q[0] * this->q[0]*pow(this->q[1],3)*this->kg[1]*this->kg[3] + 
    12*C3v*C5vNC*this->mn*this->q[0] * this->q[0]*pow(this->q[1],3)*this->kg[1]*this->kg[3] + 
    8*C5v*C5vNC*this->mn*this->q[0] * this->q[0]*pow(this->q[1],3)*this->kg[1]*this->kg[3] - 
    C5v*C5vNC*this->mRes*pow(this->q[1],5)*this->kg[1]*this->kg[3] - 
    2*C3v*C5vNC*this->mn*pow(this->q[1],5)*this->kg[1]*this->kg[3] + 
    4*C3vNC*C5v*this->mRes_2*this->mn*this->q[1]*this->q[3] * this->q[3]*this->kg[1]*this->kg[3] + 
    4*C3v*C5vNC*this->mRes*this->mn2*this->q[1]*this->q[3] * this->q[3]*this->kg[1]*this->kg[3] - 
    4*C5v*C5vNC*this->mRes*pow(p0,2)*this->q[1]*this->q[3] * this->q[3]*this->kg[1]*this->kg[3] - 
    8*C3v*C5vNC*this->mn*pow(p0,2)*this->q[1]*this->q[3] * this->q[3]*this->kg[1]*this->kg[3] + 
    12*C5v*C5vNC*this->mRes*p0*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1]*this->kg[3] + 
    8*C3vNC*C5v*this->mn*p0*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1]*this->kg[3] + 
    16*C3v*C5vNC*this->mn*p0*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1]*this->kg[3] + 
    8*C5v*C5vNC*this->mn*p0*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1]*this->kg[3] - 
    C5v*C5vNC*this->mRes*this->q[0] * this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1]*this->kg[3] - 
    10*C3vNC*C5v*this->mn*this->q[0] * this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1]*this->kg[3] - 
    8*C3v*C5vNC*this->mn*this->q[0] * this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1]*this->kg[3] - 
    8*C5v*C5vNC*this->mn*this->q[0] * this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1]*this->kg[3] + 
    C5v*C5vNC*this->mRes*this->q[1]*pow(this->q[3],4)*this->kg[1]*this->kg[3] + 
    2*C3v*C5vNC*this->mn*this->q[1]*pow(this->q[3],4)*this->kg[1]*this->kg[3] - 
    std::complex<double>(0,4)*C5a*C5v*this->mRes*this->mn2*this->q[0]*this->q[1]*this->q[3]*this->kg[2]*this->kg[3] - 
    16*C5v*C5vNC*pow(this->mRes,3)*p0*this->q[0]*this->q[3]*this->kg[3] * this->kg[3] - 
    8*C5v*C5vNC*this->mRes_2*this->mn*p0*this->q[0]*this->q[3]*this->kg[3] * this->kg[3] + 
    8*C3vNC*C5v*this->mRes_2*this->mn*this->q[0] * this->q[0]*this->q[3]*this->kg[3] * this->kg[3] + 
    8*C5v*C5vNC*this->mRes_2*this->mn*this->q[0] * this->q[0]*this->q[3]*this->kg[3] * this->kg[3] + 
    8*C3vNC*C5v*this->mRes*this->mn2*this->q[0] * this->q[0]*this->q[3]*this->kg[3] * this->kg[3] - 
    8*C3vNC*C5v*this->mn*pow(p0,2)*this->q[0] * this->q[0]*this->q[3]*this->kg[3] * this->kg[3] + 
    16*C5v*C5vNC*this->mRes*p0*pow(this->q[0],3)*this->q[3]*this->kg[3] * this->kg[3] + 
    16*C3vNC*C5v*this->mn*p0*pow(this->q[0],3)*this->q[3]*this->kg[3] * this->kg[3] + 
    8*C5v*C5vNC*this->mn*p0*pow(this->q[0],3)*this->q[3]*this->kg[3] * this->kg[3] - 
    6*C3vNC*C5v*this->mn*pow(this->q[0],4)*this->q[3]*this->kg[3] * this->kg[3] - 
    8*C5v*C5vNC*this->mn*pow(this->q[0],4)*this->q[3]*this->kg[3] * this->kg[3] - 
    4*C3vNC*C5v*this->mRes_2*this->mn*this->q[1] * this->q[1]*this->q[3]*this->kg[3] * this->kg[3] - 
    8*C5v*C5vNC*this->mn*p0*this->q[0]*this->q[1] * this->q[1]*this->q[3]*this->kg[3] * this->kg[3] + 
    6*C3vNC*C5v*this->mn*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3]*this->kg[3] * this->kg[3] + 
    8*C5v*C5vNC*this->mn*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3]*this->kg[3] * this->kg[3] - 
    2*C3vNC*C5v*this->mn*this->q[0] * this->q[0]*pow(this->q[3],3)*this->kg[3] * this->kg[3] - 
    24*C3vNC*C5v*this->mn*p0*this->q[0]*this->q[1]*this->q[3]*this->kg[1]*this->kg[3] * this->kg[3] + 
    C4vNC*this->mRes*(2*C3v*this->mn*(4*this->mRes_2*this->mn*
           (-(this->q[1]*this->q[3]*this->kg[1]) + 2*(p0 - this->q[0])*this->q[0]*this->kg[3] + this->q[1] * this->q[1]*this->kg[3]) + 
          2*this->mn*(p0 - this->q[0])*(this->q[0]*this->q[3]*(this->kg[1]*(this->q[1] + this->kg[1]) + this->kg[2] * this->kg[2]) - 
             8*p0*this->q[0] * this->q[0]*this->kg[3] - 
             this->q[0]*(-4*pow(p0,2) + this->q[3] * this->q[3] + this->q[1]*(2*this->q[1] + this->kg[1]))*this->kg[3] + 
             pow(this->q[0],3)*(-2*this->q[3] + 3*this->kg[3]) + 2*p0*this->q[1]*(-(this->q[3]*this->kg[1]) + this->q[1]*this->kg[3]) 
) + this->mRes*(16*pow(p0,3)*this->q[0]*this->kg[3] + 
             this->q[1]*(this->q[1] * this->q[1] - 2*this->q[1]*this->kg[1] + this->q[3]*(this->q[3] - 2*this->kg[3]))*
              (-(this->q[3]*this->kg[1]) + this->q[1]*this->kg[3]) + 
             8*pow(p0,2)*(-(this->q[1]*this->q[3]*this->kg[1]) - 4*this->q[0] * this->q[0]*this->kg[3] + 
                this->q[1] * this->q[1]*this->kg[3]) - 
             2*p0*this->q[0]*(-(this->q[3]*(this->kg[1]*(this->q[1] + 5*this->kg[1]) + 5*this->kg[2] * this->kg[2])) + 
                this->q[0] * this->q[0]*(4*this->q[3] - 6*this->kg[3]) + 
                (3*this->q[1] * this->q[1] + 2*this->q[3] * this->q[3] + 5*this->q[1]*this->kg[1])*this->kg[3]) + 
             this->q[0] * this->q[0]*(-6*this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) - 
                5*this->q[1] * this->q[1]*this->kg[3] + this->q[1]*this->kg[1]*(5*this->q[3] + 6*this->kg[3])))) + 
       4*C4v*this->mRes*(2*this->mn*(p0 - this->q[0])*
           (this->q[0]*this->q[3]*(this->kg[1]*(this->q[1] + this->kg[1]) + this->kg[2] * this->kg[2]) - 
             8*p0*this->q[0] * this->q[0]*this->kg[3] - 
             this->q[0]*(-4*pow(p0,2) + this->q[3] * this->q[3] + this->q[1]*(2*this->q[1] + this->kg[1]))*this->kg[3] + 
             pow(this->q[0],3)*(-2*this->q[3] + 3*this->kg[3]) + 2*p0*this->q[1]*(-(this->q[3]*this->kg[1]) + this->q[1]*this->kg[3]) 
) + this->mRes*(6*p0*pow(this->q[0],3)*this->kg[3] + 
             2*p0*this->q[0]*(this->q[1]*this->q[3]*this->kg[1] + 4*pow(p0,2)*this->kg[3] - 
                (this->q[3] * this->q[3] + 2*this->q[1]*(this->q[1] + this->kg[1]))*this->kg[3] - 2*this->q[3]*this->kg[3] * this->kg[3])  
- this->q[0] * this->q[0]*(2*this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + 
                16*pow(p0,2)*this->kg[3] - 2*this->q[1]*this->kg[1]*this->kg[3] + 
                this->q[1] * this->q[1]*(this->q[3] + this->kg[3])) + 
             this->q[1]*(this->q[1]*this->q[3]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) - 
                this->q[1] * this->q[1]*this->kg[1]*this->kg[3] + 
                pow(p0,2)*(-4*this->q[3]*this->kg[1] + 4*this->q[1]*this->kg[3]) + 
                this->q[3]*this->kg[1]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3]*(this->q[3] + this->kg[3]))))) + 
       C5v*(8*this->mRes*this->mn*(p0 - this->q[0])*
           (4*pow(p0,2)*this->q[0]*this->kg[3] - 4*p0*this->q[0] * this->q[0]*this->kg[3] + 
             pow(this->q[0],3)*(this->q[3] + this->kg[3]) + 2*p0*this->q[1]*(-(this->q[3]*this->kg[1]) + this->q[1]*this->kg[3]) - 
             this->q[0]*(this->q[3] + this->kg[3])*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3])) + 
          4*this->mRes_2*(8*pow(p0,3)*this->q[0]*this->kg[3] + 
             4*pow(p0,2)*(-(this->q[1]*this->q[3]*this->kg[1]) - 2*this->q[0] * this->q[0]*this->kg[3] + 
                this->q[1] * this->q[1]*this->kg[3]) + 
             2*p0*this->q[0]*(this->q[3] + 2*this->kg[3])*(this->q[0] * this->q[0] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3]) + 
             (this->q[0] * this->q[0] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3])*
              (this->q[0] * this->q[0]*(this->q[3] - this->kg[3]) + this->q[1]*(-(this->q[3]*this->kg[1]) + this->q[1]*this->kg[3]))) - 
          this->q[0]*(8*pow(p0,3)*this->q[0] * this->q[0]*(-this->q[3] + this->kg[3]) + 
             4*pow(p0,2)*this->q[0]*
              (5*this->q[0] * this->q[0]*(this->q[3] - this->kg[3]) + 2*this->q[1] * this->q[1]*this->kg[3] + 
                this->q[1]*this->kg[1]*(-3*this->q[3] + this->kg[3]) + this->q[3]*this->kg[3]*(-this->q[3] + this->kg[3])) + 
             this->q[0]*(-this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3])*
              (4*this->q[1]*this->q[3]*this->kg[1] + this->q[1] * this->q[1]*(this->q[3] - 3*this->kg[3]) + 
                this->q[3] * this->q[3]*(this->q[3] + this->kg[3]) + this->q[0] * this->q[0]*(-5*this->q[3] + 3*this->kg[3])) - 
             2*p0*(pow(this->q[0],4)*(9*this->q[3] - 7*this->kg[3]) - 
                this->q[0] * this->q[0]*(this->q[1] * this->q[1]*(3*this->q[3] - 5*this->kg[3]) + 
                   4*this->q[1]*this->kg[1]*(this->q[3] - this->kg[3]) + 
                   this->q[3]*(this->q[3] * this->q[3] + 5*this->q[3]*this->kg[3] - 4*this->kg[3] * this->kg[3])) + 
                2*this->q[1]*(this->q[1]*this->q[3]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) - 
                   this->q[1] * this->q[1]*this->kg[1]*this->kg[3] + 
                   this->q[3]*this->kg[1]*(-3*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + this->q[3]*this->kg[3] - 
                      3*this->kg[3] * this->kg[3])))))) + 
    C4v*this->mRes*(C5vNC*(this->q[3]*(-8*pow(this->q[0],6) + 
             this->q[1] * this->q[1]*(this->q[1] * this->q[1] + this->q[3] * this->q[3])*
              (2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + 
             pow(this->q[0],4)*(5*this->q[1] * this->q[1] + 8*this->q[1]*this->kg[1] + 
                5*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])) - 
             this->q[0] * this->q[0]*(pow(this->q[1],4) + 
                this->q[3] * this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + 
                this->q[1] * this->q[1]*(this->q[3] * this->q[3] + 11*this->kg[1] * this->kg[1] + 
                   6*this->kg[2] * this->kg[2]))) - 
          (-3*pow(this->q[0],6) + 2*pow(this->q[0],4)*
              (this->q[1] * this->q[1] - 3*this->q[3] * this->q[3]) + 
             this->q[0] * this->q[0]*pow(this->q[1] * this->q[1] + this->q[3] * this->q[3],2) + 
             this->q[1]*(-this->q[0] * this->q[0] + this->q[1] * this->q[1] - this->q[3] * this->q[3])*
              (-5*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3])*this->kg[1])*this->kg[3] + 
          8*pow(p0,3)*this->q[0]*this->q[3]*(this->q[0] * this->q[0] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3]) - 
          4*pow(p0,2)*(this->q[0] * this->q[0] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3])*
           (this->q[0] * this->q[0]*(5*this->q[3] - this->kg[3]) + this->q[1]*(-(this->q[3]*this->kg[1]) + this->q[1]*this->kg[3])) + 
          4*this->mRes_2*(4*p0*this->q[0]*this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + 
             8*pow(p0,3)*this->q[0]*this->kg[3] + 
             4*p0*this->q[0]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] - this->q[1]*this->kg[1])*this->kg[3] - 
             2*p0*pow(this->q[0],3)*(this->q[3] + this->kg[3]) - 
             4*pow(p0,2)*(-(this->q[1]*this->q[3]*this->kg[1]) + 2*this->q[0] * this->q[0]*this->kg[3] + 
                this->q[1] * this->q[1]*this->kg[3]) + 
             this->q[0] * this->q[0]*(this->q[3] - this->kg[3])*(this->q[0] * this->q[0] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3])) + 
          8*this->mRes*this->mn*(p0 - this->q[0])*
           (2*p0*this->q[1]*this->q[3]*this->kg[1] + 4*pow(p0,2)*this->q[0]*this->kg[3] - 
             2*p0*(2*this->q[0] * this->q[0] + this->q[1] * this->q[1])*this->kg[3] + 
             this->q[0]*(-(this->q[0] * this->q[0]*this->kg[3]) + 2*this->q[1] * this->q[1]*this->kg[3] - 
                this->q[1]*this->kg[1]*(this->q[3] + this->kg[3]) + 
                this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->q[3]*this->kg[3]))) + 
          2*p0*this->q[0]*(pow(this->q[1],3)*this->kg[1]*(this->q[3] - 4*this->kg[3]) + 
             pow(this->q[0],4)*(7*this->q[3] - 4*this->kg[3]) + pow(this->q[3],4)*this->kg[3] + 
             this->q[1]*this->q[3] * this->q[3]*this->kg[1]*(this->q[3] + 4*this->kg[3]) + 
             this->q[1] * this->q[1]*this->q[3]*(8*this->kg[1] * this->kg[1] + 4*this->kg[2] * this->kg[2] + this->q[3]*this->kg[3]) + 
             this->q[0] * this->q[0]*(this->q[1]*this->kg[1]*(-13*this->q[3] + 4*this->kg[3]) + 
                this->q[1] * this->q[1]*(-3*this->q[3] + 4*this->kg[3]) + 
                this->q[3]*(this->q[3] * this->q[3] - 9*this->q[3]*this->kg[3] + 4*this->kg[3] * this->kg[3])))) + 
       4*this->mn*(std::complex<double>(0,-2)*C5a*this->mRes*this->mn*(this->mRes + this->mn)*(p0 - this->q[0])*this->q[1]*this->kg[2] + 
          C3vNC*(4*this->mRes_2*this->mn*(p0 - this->q[0])*this->q[0]*this->kg[3] + 
             this->mn*(p0 - this->q[0])*(this->q[0]*this->q[3]*(this->kg[1]*(this->q[1] + this->kg[1]) + this->kg[2] * this->kg[2]) - 
                8*p0*this->q[0] * this->q[0]*this->kg[3] - 
                this->q[0]*(-4*pow(p0,2) + this->q[3] * this->q[3] + this->q[1]*(2*this->q[1] + this->kg[1]))*
                 this->kg[3] + pow(this->q[0],3)*(-2*this->q[3] + 3*this->kg[3]) + 
                2*p0*this->q[1]*(-(this->q[3]*this->kg[1]) + this->q[1]*this->kg[3])) + 
             this->mRes*(8*pow(p0,3)*this->q[0]*this->kg[3] + 
                2*pow(p0,2)*
                 (-(this->q[1]*this->q[3]*this->kg[1]) - 8*this->q[0] * this->q[0]*this->kg[3] + this->q[1] * this->q[1]*this->kg[3]) - 
                this->q[0] * this->q[0]*(3*this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + 
                   this->q[1] * this->q[1]*(this->q[3] + 2*this->kg[3]) - this->q[1]*this->kg[1]*(this->q[3] + 3*this->kg[3])) - 
                p0*this->q[0]*(this->q[0] * this->q[0]*(4*this->q[3] - 6*this->kg[3]) + this->q[1] * this->q[1]*this->kg[3] + 
                   this->q[1]*this->kg[1]*(this->q[3] + 5*this->kg[3]) + 
                   this->q[3]*(-5*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + 2*this->q[3]*this->kg[3])) + 
                this->q[1]*(this->q[1]*this->q[3]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) - 
                   this->q[1] * this->q[1]*this->kg[1]*this->kg[3] + 
                   this->q[3]*this->kg[1]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->kg[3]*(this->q[3] + this->kg[3])))))) 
))/(24.*this->mRes_2*pow(this->mn,5));

	
    
	tr[1][0] = (32*C4v*C4vNC*pow(this->mRes,3)*pow(p0,3)*this->q[0]*this->q[1] + 
    32*C4vNC*C5v*pow(this->mRes,3)*pow(p0,3)*this->q[0]*this->q[1] + 
    32*C4v*C5vNC*pow(this->mRes,3)*pow(p0,3)*this->q[0]*this->q[1] + 
    32*C5v*C5vNC*pow(this->mRes,3)*pow(p0,3)*this->q[0]*this->q[1] + 
    32*C4v*C4vNC*this->mRes_2*this->mn*pow(p0,3)*this->q[0]*this->q[1] + 
    32*C4vNC*C5v*this->mRes_2*this->mn*pow(p0,3)*this->q[0]*this->q[1] + 
    32*C4v*C5vNC*this->mRes_2*this->mn*pow(p0,3)*this->q[0]*this->q[1] + 
    32*C5v*C5vNC*this->mRes_2*this->mn*pow(p0,3)*this->q[0]*this->q[1] - 
    48*C4v*C4vNC*pow(this->mRes,3)*pow(p0,2)*this->q[0] * this->q[0]*this->q[1] - 
    48*C4vNC*C5v*pow(this->mRes,3)*pow(p0,2)*this->q[0] * this->q[0]*this->q[1] - 
    16*C4v*C5vNC*pow(this->mRes,3)*pow(p0,2)*this->q[0] * this->q[0]*this->q[1] - 
    16*C5v*C5vNC*pow(this->mRes,3)*pow(p0,2)*this->q[0] * this->q[0]*this->q[1] - 
    80*C4v*C4vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[0] * this->q[0]*this->q[1] - 
    80*C4vNC*C5v*this->mRes_2*this->mn*pow(p0,2)*this->q[0] * this->q[0]*this->q[1] - 
    48*C4v*C5vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[0] * this->q[0]*this->q[1] - 
    48*C5v*C5vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[0] * this->q[0]*this->q[1] + 
    16*C4v*C4vNC*pow(this->mRes,3)*p0*pow(this->q[0],3)*this->q[1] + 
    16*C4vNC*C5v*pow(this->mRes,3)*p0*pow(this->q[0],3)*this->q[1] + 
    64*C4v*C4vNC*this->mRes_2*this->mn*p0*pow(this->q[0],3)*this->q[1] + 
    64*C4vNC*C5v*this->mRes_2*this->mn*p0*pow(this->q[0],3)*this->q[1] + 
    16*C4v*C5vNC*this->mRes_2*this->mn*p0*pow(this->q[0],3)*this->q[1] + 
    16*C5v*C5vNC*this->mRes_2*this->mn*p0*pow(this->q[0],3)*this->q[1] - 
    16*C4v*C4vNC*this->mRes_2*this->mn*pow(this->q[0],4)*this->q[1] - 
    16*C4vNC*C5v*this->mRes_2*this->mn*pow(this->q[0],4)*this->q[1] - 
    8*C4v*C4vNC*pow(this->mRes,3)*p0*this->q[0]*pow(this->q[1],3) - 
    8*C4vNC*C5v*pow(this->mRes,3)*p0*this->q[0]*pow(this->q[1],3) + 
    16*C4v*C5vNC*pow(this->mRes,3)*p0*this->q[0]*pow(this->q[1],3) + 
    16*C5v*C5vNC*pow(this->mRes,3)*p0*this->q[0]*pow(this->q[1],3) - 
    8*C4v*C4vNC*this->mRes_2*this->mn*p0*this->q[0]*pow(this->q[1],3) - 
    8*C4vNC*C5v*this->mRes_2*this->mn*p0*this->q[0]*pow(this->q[1],3) + 
    8*C4v*C5vNC*this->mRes_2*this->mn*p0*this->q[0]*pow(this->q[1],3) + 
    8*C5v*C5vNC*this->mRes_2*this->mn*p0*this->q[0]*pow(this->q[1],3) - 
    8*C4v*C5vNC*this->mRes*pow(p0,3)*this->q[0]*pow(this->q[1],3) - 
    8*C5v*C5vNC*this->mRes*pow(p0,3)*this->q[0]*pow(this->q[1],3) - 
    4*C4v*C4vNC*pow(this->mRes,3)*this->q[0] * this->q[0]*pow(this->q[1],3) - 
    4*C4vNC*C5v*pow(this->mRes,3)*this->q[0] * this->q[0]*pow(this->q[1],3) + 
    8*C4v*C4vNC*this->mRes_2*this->mn*this->q[0] * this->q[0]*pow(this->q[1],3) + 
    8*C4vNC*C5v*this->mRes_2*this->mn*this->q[0] * this->q[0]*pow(this->q[1],3) - 
    8*C4v*C5vNC*this->mRes_2*this->mn*this->q[0] * this->q[0]*pow(this->q[1],3) - 
    8*C5v*C5vNC*this->mRes_2*this->mn*this->q[0] * this->q[0]*pow(this->q[1],3) + 
    12*C4v*C5vNC*this->mRes*pow(p0,2)*this->q[0] * this->q[0]*pow(this->q[1],3) + 
    12*C5v*C5vNC*this->mRes*pow(p0,2)*this->q[0] * this->q[0]*pow(this->q[1],3) - 
    6*C4v*C5vNC*this->mRes*p0*pow(this->q[0],3)*pow(this->q[1],3) - 
    6*C5v*C5vNC*this->mRes*p0*pow(this->q[0],3)*pow(this->q[1],3) + 
    C4v*C5vNC*this->mRes*pow(this->q[0],4)*pow(this->q[1],3) + 
    C5v*C5vNC*this->mRes*pow(this->q[0],4)*pow(this->q[1],3) + 
    2*C4v*C5vNC*this->mRes*p0*this->q[0]*pow(this->q[1],5) + 
    2*C5v*C5vNC*this->mRes*p0*this->q[0]*pow(this->q[1],5) - 
    C4v*C5vNC*this->mRes*this->q[0] * this->q[0]*pow(this->q[1],5) - 
    C5v*C5vNC*this->mRes*this->q[0] * this->q[0]*pow(this->q[1],5) - 
    8*C4v*C4vNC*pow(this->mRes,3)*p0*this->q[0]*this->q[1]*this->q[3] * this->q[3] - 
    8*C4vNC*C5v*pow(this->mRes,3)*p0*this->q[0]*this->q[1]*this->q[3] * this->q[3] + 
    16*C4v*C5vNC*pow(this->mRes,3)*p0*this->q[0]*this->q[1]*this->q[3] * this->q[3] + 
    16*C5v*C5vNC*pow(this->mRes,3)*p0*this->q[0]*this->q[1]*this->q[3] * this->q[3] - 
    8*C4v*C4vNC*this->mRes_2*this->mn*p0*this->q[0]*this->q[1]*this->q[3] * this->q[3] - 
    8*C4vNC*C5v*this->mRes_2*this->mn*p0*this->q[0]*this->q[1]*this->q[3] * this->q[3] + 
    8*C4v*C5vNC*this->mRes_2*this->mn*p0*this->q[0]*this->q[1]*this->q[3] * this->q[3] + 
    8*C5v*C5vNC*this->mRes_2*this->mn*p0*this->q[0]*this->q[1]*this->q[3] * this->q[3] - 
    8*C4v*C5vNC*this->mRes*pow(p0,3)*this->q[0]*this->q[1]*this->q[3] * this->q[3] - 
    8*C5v*C5vNC*this->mRes*pow(p0,3)*this->q[0]*this->q[1]*this->q[3] * this->q[3] - 
    4*C4v*C4vNC*pow(this->mRes,3)*this->q[0] * this->q[0]*this->q[1]*this->q[3] * this->q[3] - 
    4*C4vNC*C5v*pow(this->mRes,3)*this->q[0] * this->q[0]*this->q[1]*this->q[3] * this->q[3] + 
    8*C4v*C4vNC*this->mRes_2*this->mn*this->q[0] * this->q[0]*this->q[1]*this->q[3] * this->q[3] + 
    8*C4vNC*C5v*this->mRes_2*this->mn*this->q[0] * this->q[0]*this->q[1]*this->q[3] * this->q[3] - 
    8*C4v*C5vNC*this->mRes_2*this->mn*this->q[0] * this->q[0]*this->q[1]*this->q[3] * this->q[3] - 
    8*C5v*C5vNC*this->mRes_2*this->mn*this->q[0] * this->q[0]*this->q[1]*this->q[3] * this->q[3] + 
    12*C4v*C5vNC*this->mRes*pow(p0,2)*this->q[0] * this->q[0]*this->q[1]*this->q[3] * this->q[3] + 
    12*C5v*C5vNC*this->mRes*pow(p0,2)*this->q[0] * this->q[0]*this->q[1]*this->q[3] * this->q[3] - 
    6*C4v*C5vNC*this->mRes*p0*pow(this->q[0],3)*this->q[1]*this->q[3] * this->q[3] - 
    6*C5v*C5vNC*this->mRes*p0*pow(this->q[0],3)*this->q[1]*this->q[3] * this->q[3] + 
    C4v*C5vNC*this->mRes*pow(this->q[0],4)*this->q[1]*this->q[3] * this->q[3] + 
    C5v*C5vNC*this->mRes*pow(this->q[0],4)*this->q[1]*this->q[3] * this->q[3] + 
    4*C4v*C5vNC*this->mRes*p0*this->q[0]*pow(this->q[1],3)*this->q[3] * this->q[3] + 
    4*C5v*C5vNC*this->mRes*p0*this->q[0]*pow(this->q[1],3)*this->q[3] * this->q[3] - 
    2*C4v*C5vNC*this->mRes*this->q[0] * this->q[0]*pow(this->q[1],3)*this->q[3] * this->q[3] - 
    2*C5v*C5vNC*this->mRes*this->q[0] * this->q[0]*pow(this->q[1],3)*this->q[3] * this->q[3] + 
    2*C4v*C5vNC*this->mRes*p0*this->q[0]*this->q[1]*pow(this->q[3],4) + 
    2*C5v*C5vNC*this->mRes*p0*this->q[0]*this->q[1]*pow(this->q[3],4) - 
    C4v*C5vNC*this->mRes*this->q[0] * this->q[0]*this->q[1]*pow(this->q[3],4) - 
    C5v*C5vNC*this->mRes*this->q[0] * this->q[0]*this->q[1]*pow(this->q[3],4) - 
    16*C4v*C4vNC*pow(this->mRes,3)*p0*this->q[0]*this->q[1] * this->q[1]*this->kg[1] - 
    8*C4vNC*C5v*pow(this->mRes,3)*p0*this->q[0]*this->q[1] * this->q[1]*this->kg[1] - 
    8*C4v*C5vNC*pow(this->mRes,3)*p0*this->q[0]*this->q[1] * this->q[1]*this->kg[1] - 
    32*C5v*C5vNC*pow(this->mRes,3)*p0*this->q[0]*this->q[1] * this->q[1]*this->kg[1] - 
    16*C4v*C4vNC*this->mRes_2*this->mn*p0*this->q[0]*this->q[1] * this->q[1]*this->kg[1] - 
    16*C5v*C5vNC*this->mRes_2*this->mn*p0*this->q[0]*this->q[1] * this->q[1]*this->kg[1] + 
    8*C4vNC*C5v*this->mRes*pow(p0,3)*this->q[0]*this->q[1] * this->q[1]*this->kg[1] + 
    8*C4v*C5vNC*this->mRes*pow(p0,3)*this->q[0]*this->q[1] * this->q[1]*this->kg[1] + 
    48*C5v*C5vNC*this->mRes*pow(p0,3)*this->q[0]*this->q[1] * this->q[1]*this->kg[1] + 
    32*C5v*C5vNC*this->mn*pow(p0,3)*this->q[0]*this->q[1] * this->q[1]*this->kg[1] - 
    4*C4v*C4vNC*pow(this->mRes,3)*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[1] + 
    8*C4vNC*C5v*pow(this->mRes,3)*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[1] + 
    4*C4v*C5vNC*pow(this->mRes,3)*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[1] + 
    16*C4v*C4vNC*this->mRes_2*this->mn*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[1] + 
    16*C5v*C5vNC*this->mRes_2*this->mn*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[1] - 
    20*C4vNC*C5v*this->mRes*pow(p0,2)*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[1] - 
    24*C4v*C5vNC*this->mRes*pow(p0,2)*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[1] - 
    60*C5v*C5vNC*this->mRes*pow(p0,2)*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[1] - 
    64*C5v*C5vNC*this->mn*pow(p0,2)*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[1] + 
    18*C4vNC*C5v*this->mRes*p0*pow(this->q[0],3)*this->q[1] * this->q[1]*this->kg[1] + 
    18*C4v*C5vNC*this->mRes*p0*pow(this->q[0],3)*this->q[1] * this->q[1]*this->kg[1] + 
    20*C5v*C5vNC*this->mRes*p0*pow(this->q[0],3)*this->q[1] * this->q[1]*this->kg[1] + 
    40*C5v*C5vNC*this->mn*p0*pow(this->q[0],3)*this->q[1] * this->q[1]*this->kg[1] - 
    5*C4vNC*C5v*this->mRes*pow(this->q[0],4)*this->q[1] * this->q[1]*this->kg[1] - 
    4*C4v*C5vNC*this->mRes*pow(this->q[0],4)*this->q[1] * this->q[1]*this->kg[1] - 
    C5v*C5vNC*this->mRes*pow(this->q[0],4)*this->q[1] * this->q[1]*this->kg[1] - 
    8*C5v*C5vNC*this->mn*pow(this->q[0],4)*this->q[1] * this->q[1]*this->kg[1] - 
    2*C4vNC*C5v*this->mRes*p0*this->q[0]*pow(this->q[1],4)*this->kg[1] + 
    2*C4v*C5vNC*this->mRes*p0*this->q[0]*pow(this->q[1],4)*this->kg[1] + 
    C4vNC*C5v*this->mRes*this->q[0] * this->q[0]*pow(this->q[1],4)*this->kg[1] + 
    C5v*C5vNC*this->mRes*this->q[0] * this->q[0]*pow(this->q[1],4)*this->kg[1] + 
    8*C4vNC*C5v*pow(this->mRes,3)*p0*this->q[0]*this->q[3] * this->q[3]*this->kg[1] + 
    8*C4v*C5vNC*pow(this->mRes,3)*p0*this->q[0]*this->q[3] * this->q[3]*this->kg[1] - 
    16*C5v*C5vNC*pow(this->mRes,3)*p0*this->q[0]*this->q[3] * this->q[3]*this->kg[1] - 
    8*C4v*C4vNC*this->mRes_2*this->mn*p0*this->q[0]*this->q[3] * this->q[3]*this->kg[1] + 
    8*C4vNC*C5v*this->mRes_2*this->mn*p0*this->q[0]*this->q[3] * this->q[3]*this->kg[1] + 
    8*C4v*C5vNC*this->mRes_2*this->mn*p0*this->q[0]*this->q[3] * this->q[3]*this->kg[1] - 
    8*C5v*C5vNC*this->mRes_2*this->mn*p0*this->q[0]*this->q[3] * this->q[3]*this->kg[1] + 
    8*C4vNC*C5v*this->mRes*pow(p0,3)*this->q[0]*this->q[3] * this->q[3]*this->kg[1] + 
    8*C4v*C5vNC*this->mRes*pow(p0,3)*this->q[0]*this->q[3] * this->q[3]*this->kg[1] + 
    48*C5v*C5vNC*this->mRes*pow(p0,3)*this->q[0]*this->q[3] * this->q[3]*this->kg[1] + 
    32*C5v*C5vNC*this->mn*pow(p0,3)*this->q[0]*this->q[3] * this->q[3]*this->kg[1] - 
    4*C4v*C4vNC*pow(this->mRes,3)*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[1] + 
    4*C4vNC*C5v*pow(this->mRes,3)*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[1] + 
    8*C4v*C5vNC*pow(this->mRes,3)*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[1] + 
    8*C4v*C4vNC*this->mRes_2*this->mn*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[1] - 
    8*C4vNC*C5v*this->mRes_2*this->mn*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[1] - 
    8*C4v*C5vNC*this->mRes_2*this->mn*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[1] + 
    8*C5v*C5vNC*this->mRes_2*this->mn*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[1] - 
    24*C4vNC*C5v*this->mRes*pow(p0,2)*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[1] - 
    20*C4v*C5vNC*this->mRes*pow(p0,2)*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[1] - 
    60*C5v*C5vNC*this->mRes*pow(p0,2)*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[1] - 
    64*C5v*C5vNC*this->mn*pow(p0,2)*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[1] + 
    26*C4vNC*C5v*this->mRes*p0*pow(this->q[0],3)*this->q[3] * this->q[3]*this->kg[1] + 
    18*C4v*C5vNC*this->mRes*p0*pow(this->q[0],3)*this->q[3] * this->q[3]*this->kg[1] + 
    28*C5v*C5vNC*this->mRes*p0*pow(this->q[0],3)*this->q[3] * this->q[3]*this->kg[1] + 
    48*C5v*C5vNC*this->mn*p0*pow(this->q[0],3)*this->q[3] * this->q[3]*this->kg[1] - 
    8*C4vNC*C5v*this->mRes*pow(this->q[0],4)*this->q[3] * this->q[3]*this->kg[1] - 
    7*C4v*C5vNC*this->mRes*pow(this->q[0],4)*this->q[3] * this->q[3]*this->kg[1] + 
    C5v*C5vNC*this->mRes*pow(this->q[0],4)*this->q[3] * this->q[3]*this->kg[1] - 
    16*C5v*C5vNC*this->mn*pow(this->q[0],4)*this->q[3] * this->q[3]*this->kg[1] - 
    4*C4vNC*C5v*this->mRes*p0*this->q[0]*this->q[1] * this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 
    4*C4v*C5vNC*this->mRes*p0*this->q[0]*this->q[1] * this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 
    C4vNC*C5v*this->mRes*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3] * this->q[3]*this->kg[1] - 
    C4v*C5vNC*this->mRes*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3] * this->q[3]*this->kg[1] - 
    2*C4vNC*C5v*this->mRes*p0*this->q[0]*pow(this->q[3],4)*this->kg[1] + 
    2*C4v*C5vNC*this->mRes*p0*this->q[0]*pow(this->q[3],4)*this->kg[1] - 
    C4v*C5vNC*this->mRes*this->q[0] * this->q[0]*pow(this->q[3],4)*this->kg[1] - 
    C5v*C5vNC*this->mRes*this->q[0] * this->q[0]*pow(this->q[3],4)*this->kg[1] - 
    16*C4v*C4vNC*pow(this->mRes,3)*pow(p0,2)*this->q[1]*this->kg[1] * this->kg[1] + 
    16*C4vNC*C5v*pow(this->mRes,3)*pow(p0,2)*this->q[1]*this->kg[1] * this->kg[1] - 
    16*C4v*C5vNC*pow(this->mRes,3)*pow(p0,2)*this->q[1]*this->kg[1] * this->kg[1] + 
    16*C5v*C5vNC*pow(this->mRes,3)*pow(p0,2)*this->q[1]*this->kg[1] * this->kg[1] - 
    16*C4v*C4vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[1]*this->kg[1] * this->kg[1] + 
    16*C4vNC*C5v*this->mRes_2*this->mn*pow(p0,2)*this->q[1]*this->kg[1] * this->kg[1] - 
    16*C4v*C5vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[1]*this->kg[1] * this->kg[1] + 
    16*C5v*C5vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[1]*this->kg[1] * this->kg[1] + 
    8*C4v*C4vNC*pow(this->mRes,3)*p0*this->q[0]*this->q[1]*this->kg[1] * this->kg[1] - 
    8*C4v*C5vNC*pow(this->mRes,3)*p0*this->q[0]*this->q[1]*this->kg[1] * this->kg[1] + 
    16*C5v*C5vNC*pow(this->mRes,3)*p0*this->q[0]*this->q[1]*this->kg[1] * this->kg[1] + 
    24*C4v*C4vNC*this->mRes_2*this->mn*p0*this->q[0]*this->q[1]*this->kg[1] * this->kg[1] - 
    24*C4vNC*C5v*this->mRes_2*this->mn*p0*this->q[0]*this->q[1]*this->kg[1] * this->kg[1] + 
    8*C4v*C5vNC*this->mRes_2*this->mn*p0*this->q[0]*this->q[1]*this->kg[1] * this->kg[1] - 
    8*C5v*C5vNC*this->mRes_2*this->mn*p0*this->q[0]*this->q[1]*this->kg[1] * this->kg[1] - 
    8*C4vNC*C5v*this->mRes*pow(p0,3)*this->q[0]*this->q[1]*this->kg[1] * this->kg[1] - 
    8*C5v*C5vNC*this->mRes*pow(p0,3)*this->q[0]*this->q[1]*this->kg[1] * this->kg[1] - 
    4*C4vNC*C5v*pow(this->mRes,3)*this->q[0] * this->q[0]*this->q[1]*this->kg[1] * this->kg[1] - 
    4*C4v*C5vNC*pow(this->mRes,3)*this->q[0] * this->q[0]*this->q[1]*this->kg[1] * this->kg[1] - 
    8*C4v*C4vNC*this->mRes_2*this->mn*this->q[0] * this->q[0]*this->q[1]*this->kg[1] * this->kg[1] + 
    8*C4vNC*C5v*this->mRes_2*this->mn*this->q[0] * this->q[0]*this->q[1]*this->kg[1] * this->kg[1] + 
    8*C4v*C5vNC*this->mRes_2*this->mn*this->q[0] * this->q[0]*this->q[1]*this->kg[1] * this->kg[1] - 
    8*C5v*C5vNC*this->mRes_2*this->mn*this->q[0] * this->q[0]*this->q[1]*this->kg[1] * this->kg[1] + 
    20*C4vNC*C5v*this->mRes*pow(p0,2)*this->q[0] * this->q[0]*this->q[1]*this->kg[1] * this->kg[1] + 
    4*C4v*C5vNC*this->mRes*pow(p0,2)*this->q[0] * this->q[0]*this->q[1]*this->kg[1] * this->kg[1] + 
    32*C5v*C5vNC*this->mRes*pow(p0,2)*this->q[0] * this->q[0]*this->q[1]*this->kg[1] * this->kg[1] + 
    16*C5v*C5vNC*this->mn*pow(p0,2)*this->q[0] * this->q[0]*this->q[1]*this->kg[1] * this->kg[1] - 
    14*C4vNC*C5v*this->mRes*p0*pow(this->q[0],3)*this->q[1]*this->kg[1] * this->kg[1] - 
    8*C4v*C5vNC*this->mRes*p0*pow(this->q[0],3)*this->q[1]*this->kg[1] * this->kg[1] - 
    14*C5v*C5vNC*this->mRes*p0*pow(this->q[0],3)*this->q[1]*this->kg[1] * this->kg[1] - 
    24*C5v*C5vNC*this->mn*p0*pow(this->q[0],3)*this->q[1]*this->kg[1] * this->kg[1] + 
    3*C4vNC*C5v*this->mRes*pow(this->q[0],4)*this->q[1]*this->kg[1] * this->kg[1] + 
    3*C4v*C5vNC*this->mRes*pow(this->q[0],4)*this->q[1]*this->kg[1] * this->kg[1] + 
    8*C5v*C5vNC*this->mn*pow(this->q[0],4)*this->q[1]*this->kg[1] * this->kg[1] + 
    4*C4v*C4vNC*pow(this->mRes,3)*pow(this->q[1],3)*this->kg[1] * this->kg[1] - 
    4*C4v*C5vNC*pow(this->mRes,3)*pow(this->q[1],3)*this->kg[1] * this->kg[1] + 
    4*C4vNC*C5v*this->mRes*pow(p0,2)*pow(this->q[1],3)*this->kg[1] * this->kg[1] + 
    8*C4v*C5vNC*this->mRes*pow(p0,2)*pow(this->q[1],3)*this->kg[1] * this->kg[1] + 
    20*C5v*C5vNC*this->mRes*pow(p0,2)*pow(this->q[1],3)*this->kg[1] * this->kg[1] + 
    16*C5v*C5vNC*this->mn*pow(p0,2)*pow(this->q[1],3)*this->kg[1] * this->kg[1] - 
    10*C4vNC*C5v*this->mRes*p0*this->q[0]*pow(this->q[1],3)*this->kg[1] * this->kg[1] - 
    12*C4v*C5vNC*this->mRes*p0*this->q[0]*pow(this->q[1],3)*this->kg[1] * this->kg[1] - 
    14*C5v*C5vNC*this->mRes*p0*this->q[0]*pow(this->q[1],3)*this->kg[1] * this->kg[1] - 
    24*C5v*C5vNC*this->mn*p0*this->q[0]*pow(this->q[1],3)*this->kg[1] * this->kg[1] + 
    6*C4vNC*C5v*this->mRes*this->q[0] * this->q[0]*pow(this->q[1],3)*this->kg[1] * this->kg[1] + 
    5*C4v*C5vNC*this->mRes*this->q[0] * this->q[0]*pow(this->q[1],3)*this->kg[1] * this->kg[1] - 
    C5v*C5vNC*this->mRes*this->q[0] * this->q[0]*pow(this->q[1],3)*this->kg[1] * this->kg[1] + 
    8*C5v*C5vNC*this->mn*this->q[0] * this->q[0]*pow(this->q[1],3)*this->kg[1] * this->kg[1] - 
    C4vNC*C5v*this->mRes*pow(this->q[1],5)*this->kg[1] * this->kg[1] + 
    C5v*C5vNC*this->mRes*pow(this->q[1],5)*this->kg[1] * this->kg[1] + 
    4*C4v*C4vNC*pow(this->mRes,3)*this->q[1]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
    4*C4v*C5vNC*pow(this->mRes,3)*this->q[1]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
    4*C4vNC*C5v*this->mRes*pow(p0,2)*this->q[1]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
    8*C4v*C5vNC*this->mRes*pow(p0,2)*this->q[1]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
    20*C5v*C5vNC*this->mRes*pow(p0,2)*this->q[1]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
    16*C5v*C5vNC*this->mn*pow(p0,2)*this->q[1]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
    10*C4vNC*C5v*this->mRes*p0*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
    12*C4v*C5vNC*this->mRes*p0*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
    14*C5v*C5vNC*this->mRes*p0*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
    24*C5v*C5vNC*this->mn*p0*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
    6*C4vNC*C5v*this->mRes*this->q[0] * this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
    5*C4v*C5vNC*this->mRes*this->q[0] * this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
    C5v*C5vNC*this->mRes*this->q[0] * this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
    8*C5v*C5vNC*this->mn*this->q[0] * this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
    2*C4vNC*C5v*this->mRes*pow(this->q[1],3)*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
    2*C5v*C5vNC*this->mRes*pow(this->q[1],3)*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
    C4vNC*C5v*this->mRes*this->q[1]*pow(this->q[3],4)*this->kg[1] * this->kg[1] + 
    C5v*C5vNC*this->mRes*this->q[1]*pow(this->q[3],4)*this->kg[1] * this->kg[1] + 
    4*C4v*C4vNC*pow(this->mRes,3)*this->q[1] * this->q[1]*pow(this->kg[1],3) + 
    4*C4v*C5vNC*pow(this->mRes,3)*this->q[1] * this->q[1]*pow(this->kg[1],3) - 
    4*C4vNC*C5v*this->mRes*pow(p0,2)*this->q[1] * this->q[1]*pow(this->kg[1],3) - 
    4*C5v*C5vNC*this->mRes*pow(p0,2)*this->q[1] * this->q[1]*pow(this->kg[1],3) + 
    8*C4vNC*C5v*this->mRes*p0*this->q[0]*this->q[1] * this->q[1]*pow(this->kg[1],3) + 
    4*C4v*C5vNC*this->mRes*p0*this->q[0]*this->q[1] * this->q[1]*pow(this->kg[1],3) + 
    12*C5v*C5vNC*this->mRes*p0*this->q[0]*this->q[1] * this->q[1]*pow(this->kg[1],3) + 
    8*C5v*C5vNC*this->mn*p0*this->q[0]*this->q[1] * this->q[1]*pow(this->kg[1],3) - 
    3*C4vNC*C5v*this->mRes*this->q[0] * this->q[0]*this->q[1] * this->q[1]*pow(this->kg[1],3) - 
    4*C4v*C5vNC*this->mRes*this->q[0] * this->q[0]*this->q[1] * this->q[1]*pow(this->kg[1],3) + 
    C5v*C5vNC*this->mRes*this->q[0] * this->q[0]*this->q[1] * this->q[1]*pow(this->kg[1],3) - 
    8*C5v*C5vNC*this->mn*this->q[0] * this->q[0]*this->q[1] * this->q[1]*pow(this->kg[1],3) - 
    C4vNC*C5v*this->mRes*pow(this->q[1],4)*pow(this->kg[1],3) - 
    C5v*C5vNC*this->mRes*pow(this->q[1],4)*pow(this->kg[1],3) - 
    4*C4v*C4vNC*pow(this->mRes,3)*this->q[3] * this->q[3]*pow(this->kg[1],3) - 
    4*C4v*C5vNC*pow(this->mRes,3)*this->q[3] * this->q[3]*pow(this->kg[1],3) + 
    4*C4vNC*C5v*this->mRes*pow(p0,2)*this->q[3] * this->q[3]*pow(this->kg[1],3) + 
    4*C5v*C5vNC*this->mRes*pow(p0,2)*this->q[3] * this->q[3]*pow(this->kg[1],3) - 
    8*C4vNC*C5v*this->mRes*p0*this->q[0]*this->q[3] * this->q[3]*pow(this->kg[1],3) - 
    4*C4v*C5vNC*this->mRes*p0*this->q[0]*this->q[3] * this->q[3]*pow(this->kg[1],3) - 
    12*C5v*C5vNC*this->mRes*p0*this->q[0]*this->q[3] * this->q[3]*pow(this->kg[1],3) - 
    8*C5v*C5vNC*this->mn*p0*this->q[0]*this->q[3] * this->q[3]*pow(this->kg[1],3) + 
    3*C4vNC*C5v*this->mRes*this->q[0] * this->q[0]*this->q[3] * this->q[3]*pow(this->kg[1],3) + 
    4*C4v*C5vNC*this->mRes*this->q[0] * this->q[0]*this->q[3] * this->q[3]*pow(this->kg[1],3) - 
    C5v*C5vNC*this->mRes*this->q[0] * this->q[0]*this->q[3] * this->q[3]*pow(this->kg[1],3) + 
    8*C5v*C5vNC*this->mn*this->q[0] * this->q[0]*this->q[3] * this->q[3]*pow(this->kg[1],3) + 
    C4vNC*C5v*this->mRes*pow(this->q[3],4)*pow(this->kg[1],3) + 
    C5v*C5vNC*this->mRes*pow(this->q[3],4)*pow(this->kg[1],3) + 
    std::complex<double>(0,8)*C4v*C5a*this->mRes*this->mn2*pow(p0,2)*this->q[0]*this->q[3]*this->kg[2] + 
    std::complex<double>(0,8)*C5a*C5v*this->mRes*this->mn2*pow(p0,2)*this->q[0]*this->q[3]*this->kg[2] - 
    std::complex<double>(0,12)*C4v*C5a*this->mRes*this->mn2*p0*this->q[0] * this->q[0]*this->q[3]*this->kg[2] - 
    std::complex<double>(0,12)*C5a*C5v*this->mRes*this->mn2*p0*this->q[0] * this->q[0]*this->q[3]*this->kg[2] + 
    std::complex<double>(0,4)*C4v*C5a*this->mRes*this->mn2*pow(this->q[0],3)*this->q[3]*this->kg[2] + 
    std::complex<double>(0,4)*C5a*C5v*this->mRes*this->mn2*pow(this->q[0],3)*this->q[3]*this->kg[2] + 
    std::complex<double>(0,4)*C4v*C5a*this->mRes*this->mn2*p0*this->q[1]*this->q[3]*this->kg[1]*this->kg[2] + 
    std::complex<double>(0,4)*C5a*C5v*this->mRes*this->mn2*p0*this->q[1]*this->q[3]*this->kg[1]*this->kg[2] - 
    std::complex<double>(0,4)*C4v*C5a*this->mRes*this->mn2*this->q[0]*this->q[1]*this->q[3]*this->kg[1]*this->kg[2] - 
    std::complex<double>(0,4)*C5a*C5v*this->mRes*this->mn2*this->q[0]*this->q[1]*this->q[3]*this->kg[1]*this->kg[2] - 
    4*C4v*C4vNC*pow(this->mRes,3)*this->q[3] * this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] - 
    4*C4v*C5vNC*pow(this->mRes,3)*this->q[3] * this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] + 
    4*C4vNC*C5v*this->mRes*pow(p0,2)*this->q[3] * this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] + 
    4*C5v*C5vNC*this->mRes*pow(p0,2)*this->q[3] * this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] - 
    8*C4vNC*C5v*this->mRes*p0*this->q[0]*this->q[3] * this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] - 
    4*C4v*C5vNC*this->mRes*p0*this->q[0]*this->q[3] * this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] - 
    12*C5v*C5vNC*this->mRes*p0*this->q[0]*this->q[3] * this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] - 
    8*C5v*C5vNC*this->mn*p0*this->q[0]*this->q[3] * this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] + 
    3*C4vNC*C5v*this->mRes*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] + 
    4*C4v*C5vNC*this->mRes*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] - 
    C5v*C5vNC*this->mRes*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] + 
    8*C5v*C5vNC*this->mn*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] + 
    C4vNC*C5v*this->mRes*this->q[1] * this->q[1]*this->q[3] * this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] + 
    C5v*C5vNC*this->mRes*this->q[1] * this->q[1]*this->q[3] * this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] + 
    C4vNC*C5v*this->mRes*pow(this->q[3],4)*this->kg[1]*this->kg[2] * this->kg[2] + 
    C5v*C5vNC*this->mRes*pow(this->q[3],4)*this->kg[1]*this->kg[2] * this->kg[2] + 
    this->q[3]*(C4v*this->mRes*(4*C4vNC*this->mRes*
           (-2*this->mn*(p0 - this->q[0])*(this->q[0]*(this->q[1] - this->kg[1]) + 2*p0*this->kg[1]) + 
             this->mRes*(this->q[0]*(-4*p0 + this->q[0])*this->q[1] + 
                (2*p0*(-2*p0 + this->q[0]) + this->q[1] * this->q[1] + this->q[3] * this->q[3])*
                 this->kg[1] + 2*this->q[1]*this->kg[1] * this->kg[1])) + 
          C5vNC*(this->q[0] * this->q[0]*this->q[1]*
              (-pow(-2*p0 + this->q[0],2) + this->q[1] * this->q[1] + this->q[3] * this->q[3]) + 
             (3*pow(this->q[0],4) + 
                5*this->q[0] * this->q[0]*(this->q[1] * this->q[1] + this->q[3] * this->q[3]) + 
                4*pow(p0,2)*
                 (this->q[0] * this->q[0] + 2*(this->q[1] * this->q[1] + this->q[3] * this->q[3])) - 
                4*p0*this->q[0]*(2*this->q[0] * this->q[0] + 
                   3*(this->q[1] * this->q[1] + this->q[3] * this->q[3])))*this->kg[1] + 
             8*(p0 - this->q[0])*this->q[0]*this->q[1]*this->kg[1] * this->kg[1] - 
             8*this->mRes*this->mn*(p0 - this->q[0])*(2*p0*this->kg[1] + this->q[0]*(this->q[1] + this->kg[1])) - 
             4*this->mRes_2*
              (4*pow(p0,2)*this->kg[1] + 2*p0*this->q[0]*(2*this->q[1] + this->kg[1]) + 
                this->kg[1]*(this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] - 2*this->q[1]*this->kg[1]) 
)) + std::complex<double>(0,4)*C5a*this->mn2*(p0 - this->q[0])*this->q[3]*this->kg[2]) + 
       C5v*(C4vNC*this->mRes*(-((8*pow(p0,3)*this->q[0] - 3*pow(this->q[0],4) - 
                  6*this->q[0] * this->q[0]*(this->q[1] * this->q[1] + this->q[3] * this->q[3]) + 
                  pow(this->q[1] * this->q[1] + this->q[3] * this->q[3],2) - 
                  4*pow(p0,2)*
                   (5*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3]) + 
                  2*p0*this->q[0]*(7*this->q[0] * this->q[0] + 
                     5*(this->q[1] * this->q[1] + this->q[3] * this->q[3])))*this->kg[1]) - 
             2*this->q[1]*(4*pow(p0,2) - 8*p0*this->q[0] + 3*this->q[0] * this->q[0] + 
                this->q[1] * this->q[1] + this->q[3] * this->q[3])*this->kg[1] * this->kg[1] + 
             4*this->mRes_2*
              (-4*p0*this->q[0]*this->q[1] + this->q[0] * this->q[0]*(this->q[1] - this->kg[1]) + 
                4*pow(p0,2)*this->kg[1]) + 
             8*this->mRes*this->mn*(p0 - this->q[0])*(2*p0*this->kg[1] - this->q[0]*(this->q[1] + this->kg[1]))) + 
          C5vNC*(16*pow(this->mRes,3)*p0*(-(this->q[0]*this->q[1]) + (p0 + this->q[0])*this->kg[1]) + 
             8*this->mn*this->kg[1]*((2*pow(p0,2) - 3*p0*this->q[0] + this->q[0] * this->q[0])*
                 (this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3]) + 
                2*(p0 - this->q[0])*this->q[0]*this->q[1]*this->kg[1]) + 
             8*this->mRes_2*this->mn*(p0 - this->q[0])*
              (2*p0*this->kg[1] + this->q[0]*(-this->q[1] + this->kg[1])) + 
             this->mRes*(-8*pow(p0,3)*this->q[0]*this->kg[1] + 
                2*p0*this->q[0]*(2*this->q[0] * this->q[0]*this->q[1] - 
                   7*(this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3])*this->kg[1] + 
                   12*this->q[1]*this->kg[1] * this->kg[1]) - 
                (this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3])*
                 (this->q[0] * this->q[0]*this->q[1] + 
                   this->kg[1]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] - 2*this->q[1]*this->kg[1])) + 
                4*pow(p0,2)*
                 (-(this->q[0] * this->q[0]*(this->q[1] - 8*this->kg[1])) + 
                   this->kg[1]*(5*(this->q[1] * this->q[1] + this->q[3] * this->q[3]) - 2*this->q[1]*this->kg[1])))) + 
          std::complex<double>(0,4)*C5a*this->mRes*this->mn2*(p0 - this->q[0])*this->q[3]*this->kg[2]))*this->kg[3] + 
    2*C3v*this->mn*(8*C5vNC*pow(this->mRes,3)*this->mn*p0*this->q[0]*this->q[1] + 
       16*C5vNC*this->mRes_2*pow(p0,3)*this->q[0]*this->q[1] + 
       8*C5vNC*this->mRes*this->mn*pow(p0,3)*this->q[0]*this->q[1] - 
       12*C5vNC*this->mRes_2*pow(p0,2)*this->q[0] * this->q[0]*this->q[1] - 
       12*C5vNC*this->mRes*this->mn*pow(p0,2)*this->q[0] * this->q[0]*this->q[1] + 
       2*C5vNC*this->mRes_2*p0*pow(this->q[0],3)*this->q[1] + 
       4*C5vNC*this->mRes*this->mn*p0*pow(this->q[0],3)*this->q[1] + 
       12*C5vNC*this->mRes_2*p0*this->q[0]*pow(this->q[1],3) + 
       6*C5vNC*this->mRes*this->mn*p0*this->q[0]*pow(this->q[1],3) - 
       8*C5vNC*pow(p0,3)*this->q[0]*pow(this->q[1],3) - 
       2*C5vNC*this->mRes_2*this->q[0] * this->q[0]*pow(this->q[1],3) - 
       4*C5vNC*this->mRes*this->mn*this->q[0] * this->q[0]*pow(this->q[1],3) + 
       12*C5vNC*pow(p0,2)*this->q[0] * this->q[0]*pow(this->q[1],3) - 
       6*C5vNC*p0*pow(this->q[0],3)*pow(this->q[1],3) + 
       C5vNC*pow(this->q[0],4)*pow(this->q[1],3) + 2*C5vNC*p0*this->q[0]*pow(this->q[1],5) - 
       C5vNC*this->q[0] * this->q[0]*pow(this->q[1],5) + 
       12*C5vNC*this->mRes_2*p0*this->q[0]*this->q[1]*this->q[3] * this->q[3] + 
       6*C5vNC*this->mRes*this->mn*p0*this->q[0]*this->q[1]*this->q[3] * this->q[3] - 
       8*C5vNC*pow(p0,3)*this->q[0]*this->q[1]*this->q[3] * this->q[3] - 
       2*C5vNC*this->mRes_2*this->q[0] * this->q[0]*this->q[1]*this->q[3] * this->q[3] - 
       4*C5vNC*this->mRes*this->mn*this->q[0] * this->q[0]*this->q[1]*this->q[3] * this->q[3] + 
       12*C5vNC*pow(p0,2)*this->q[0] * this->q[0]*this->q[1]*this->q[3] * this->q[3] - 
       6*C5vNC*p0*pow(this->q[0],3)*this->q[1]*this->q[3] * this->q[3] + 
       C5vNC*pow(this->q[0],4)*this->q[1]*this->q[3] * this->q[3] + 
       4*C5vNC*p0*this->q[0]*pow(this->q[1],3)*this->q[3] * this->q[3] - 
       2*C5vNC*this->q[0] * this->q[0]*pow(this->q[1],3)*this->q[3] * this->q[3] + 
       2*C5vNC*p0*this->q[0]*this->q[1]*pow(this->q[3],4) - C5vNC*this->q[0] * this->q[0]*this->q[1]*pow(this->q[3],4) - 
       8*C5vNC*this->mRes_2*p0*this->q[0]*this->q[1] * this->q[1]*this->kg[1] + 
       4*C5vNC*this->mRes*this->mn*p0*this->q[0]*this->q[1] * this->q[1]*this->kg[1] + 
       8*C5vNC*pow(p0,3)*this->q[0]*this->q[1] * this->q[1]*this->kg[1] + 
       6*C5vNC*this->mRes_2*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[1] - 
       24*C5vNC*pow(p0,2)*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[1] + 
       18*C5vNC*p0*pow(this->q[0],3)*this->q[1] * this->q[1]*this->kg[1] - 
       4*C5vNC*pow(this->q[0],4)*this->q[1] * this->q[1]*this->kg[1] + 
       2*C5vNC*p0*this->q[0]*pow(this->q[1],4)*this->kg[1] + 
       2*C5vNC*this->mRes_2*p0*this->q[0]*this->q[3] * this->q[3]*this->kg[1] + 
       6*C5vNC*this->mRes*this->mn*p0*this->q[0]*this->q[3] * this->q[3]*this->kg[1] + 
       8*C5vNC*pow(p0,3)*this->q[0]*this->q[3] * this->q[3]*this->kg[1] + 
       6*C5vNC*this->mRes_2*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[1] - 
       4*C5vNC*this->mRes*this->mn*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[1] - 
       20*C5vNC*pow(p0,2)*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[1] + 
       18*C5vNC*p0*pow(this->q[0],3)*this->q[3] * this->q[3]*this->kg[1] - 
       7*C5vNC*pow(this->q[0],4)*this->q[3] * this->q[3]*this->kg[1] + 
       4*C5vNC*p0*this->q[0]*this->q[1] * this->q[1]*this->q[3] * this->q[3]*this->kg[1] - 
       C5vNC*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 
       2*C5vNC*p0*this->q[0]*pow(this->q[3],4)*this->kg[1] - 
       C5vNC*this->q[0] * this->q[0]*pow(this->q[3],4)*this->kg[1] - 
       4*C5vNC*this->mRes_2*pow(p0,2)*this->q[1]*this->kg[1] * this->kg[1] - 
       4*C5vNC*this->mRes*this->mn*pow(p0,2)*this->q[1]*this->kg[1] * this->kg[1] - 
       6*C5vNC*this->mRes_2*p0*this->q[0]*this->q[1]*this->kg[1] * this->kg[1] + 
       2*C5vNC*this->mRes*this->mn*p0*this->q[0]*this->q[1]*this->kg[1] * this->kg[1] - 
       4*C5vNC*this->mRes_2*this->q[0] * this->q[0]*this->q[1]*this->kg[1] * this->kg[1] + 
       4*C5vNC*this->mRes*this->mn*this->q[0] * this->q[0]*this->q[1]*this->kg[1] * this->kg[1] + 
       4*C5vNC*pow(p0,2)*this->q[0] * this->q[0]*this->q[1]*this->kg[1] * this->kg[1] - 
       8*C5vNC*p0*pow(this->q[0],3)*this->q[1]*this->kg[1] * this->kg[1] + 
       3*C5vNC*pow(this->q[0],4)*this->q[1]*this->kg[1] * this->kg[1] - 
       2*C5vNC*this->mRes_2*pow(this->q[1],3)*this->kg[1] * this->kg[1] + 
       8*C5vNC*pow(p0,2)*pow(this->q[1],3)*this->kg[1] * this->kg[1] - 
       12*C5vNC*p0*this->q[0]*pow(this->q[1],3)*this->kg[1] * this->kg[1] + 
       5*C5vNC*this->q[0] * this->q[0]*pow(this->q[1],3)*this->kg[1] * this->kg[1] - 
       2*C5vNC*this->mRes_2*this->q[1]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
       8*C5vNC*pow(p0,2)*this->q[1]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
       12*C5vNC*p0*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
       5*C5vNC*this->q[0] * this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
       2*C5vNC*this->mRes_2*this->q[1] * this->q[1]*pow(this->kg[1],3) + 
       4*C5vNC*p0*this->q[0]*this->q[1] * this->q[1]*pow(this->kg[1],3) - 
       4*C5vNC*this->q[0] * this->q[0]*this->q[1] * this->q[1]*pow(this->kg[1],3) - 
       2*C5vNC*this->mRes_2*this->q[3] * this->q[3]*pow(this->kg[1],3) - 
       4*C5vNC*p0*this->q[0]*this->q[3] * this->q[3]*pow(this->kg[1],3) + 
       4*C5vNC*this->q[0] * this->q[0]*this->q[3] * this->q[3]*pow(this->kg[1],3) - 
       std::complex<double>(0,8)*C5a*this->mRes_2*this->mn2*p0*this->q[3]*this->kg[2] + 
       std::complex<double>(0,8)*C5a*this->mRes*pow(this->mn,3)*p0*this->q[3]*this->kg[2] - 
       std::complex<double>(0,4)*C5a*this->mRes_2*this->mn2*this->q[0]*this->q[3]*this->kg[2] - 
       std::complex<double>(0,8)*C5a*this->mRes*pow(this->mn,3)*this->q[0]*this->q[3]*this->kg[2] + 
       std::complex<double>(0,8)*C5a*this->mn2*pow(p0,2)*this->q[0]*this->q[3]*this->kg[2] - 
       std::complex<double>(0,12)*C5a*this->mn2*p0*this->q[0] * this->q[0]*this->q[3]*this->kg[2] + 
       std::complex<double>(0,4)*C5a*this->mn2*pow(this->q[0],3)*this->q[3]*this->kg[2] + 
       std::complex<double>(0,4)*C5a*this->mn2*p0*this->q[1]*this->q[3]*this->kg[1]*this->kg[2] - 
       std::complex<double>(0,4)*C5a*this->mn2*this->q[0]*this->q[1]*this->q[3]*this->kg[1]*this->kg[2] - 
       2*C5vNC*this->mRes_2*this->q[3] * this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] - 
       4*C5vNC*p0*this->q[0]*this->q[3] * this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] + 
       4*C5vNC*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] + 
       this->q[3]*(C5vNC*(this->q[0]*this->q[1]*(-2*this->mRes*this->mn*(p0 - 2*this->q[0]) + 
                2*this->mRes_2*(-5*p0 + this->q[0]) + 
                this->q[0]*(-pow(-2*p0 + this->q[0],2) + this->q[1] * this->q[1] + this->q[3] * this->q[3]) 
) + (3*pow(this->q[0],4) + 2*this->mRes*this->mn*
                 (-2*pow(p0,2) + p0*this->q[0] + 2*this->q[0] * this->q[0]) + 
                5*this->q[0] * this->q[0]*(this->q[1] * this->q[1] + this->q[3] * this->q[3]) - 
                2*this->mRes_2*
                 (2*pow(p0,2) + 3*p0*this->q[0] + 2*this->q[0] * this->q[0] + 
                   this->q[1] * this->q[1] + this->q[3] * this->q[3]) + 
                4*pow(p0,2)*
                 (this->q[0] * this->q[0] + 2*(this->q[1] * this->q[1] + this->q[3] * this->q[3])) - 
                4*p0*this->q[0]*(2*this->q[0] * this->q[0] + 
                   3*(this->q[1] * this->q[1] + this->q[3] * this->q[3])))*this->kg[1] + 
             4*(this->mRes_2 + 2*(p0 - this->q[0])*this->q[0])*this->q[1]*this->kg[1] * this->kg[1]) + 
          std::complex<double>(0,4)*C5a*this->mn2*(p0 - this->q[0])*this->q[3]*this->kg[2])*this->kg[3] + 
       2*C4vNC*this->mRes*(4*this->mRes_2*this->mn*(p0 - this->q[0])*this->q[0]*this->q[1] + 
          this->mRes*(8*pow(p0,3)*this->q[0]*this->q[1] - pow(this->q[0],4)*this->q[1] + 
             this->kg[1]*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3])*(this->q[1]*(this->q[1] + this->kg[1]) + this->q[3]*(this->q[3] + this->kg[3])) - 
             this->q[0] * this->q[0]*(pow(this->q[1],3) + this->q[1] * this->q[1]*this->kg[1] + 
                this->q[3]*this->kg[1]*(3*this->q[3] - this->kg[3]) + 
                this->q[1]*(this->q[3] * this->q[3] - this->kg[1] * this->kg[1] - 2*this->q[3]*this->kg[3])) - 
             2*pow(p0,2)*
              (7*this->q[0] * this->q[0]*this->q[1] + this->kg[1]*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3])) + 
             p0*this->q[0]*(7*this->q[0] * this->q[0]*this->q[1] - 2*pow(this->q[1],3) - 
                4*this->q[1] * this->q[1]*this->kg[1] + this->q[3]*this->kg[1]*(this->q[3] - this->kg[3]) - 
                this->q[1]*(2*this->q[3] * this->q[3] + this->kg[1] * this->kg[1] + 5*this->q[3]*this->kg[3]))) + 
          this->mn*(p0 - this->q[0])*(4*pow(p0,2)*this->q[0]*this->q[1] - 
             2*p0*(3*this->q[0] * this->q[0]*this->q[1] + this->kg[1]*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3])) + 
             this->q[0]*(2*this->q[0] * this->q[0]*this->q[1] - pow(this->q[1],3) - 2*this->q[1] * this->q[1]*this->kg[1] + 
                this->q[3]*this->kg[1]*(-this->q[3] + this->kg[3]) + this->q[1]*(this->kg[1] * this->kg[1] - this->q[3]*(this->q[3] + this->kg[3])))))  
+ 2*C3vNC*this->mn*(2*this->mRes_2*this->mn*
           (this->q[0] * this->q[0]*this->q[1] - this->kg[1]*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3])) + 
          2*pow(this->mRes,3)*(4*p0*this->q[0]*this->q[1] + this->q[0] * this->q[0]*this->q[1] - 
             this->kg[1]*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3])) + 
          this->mRes*(-3*pow(this->q[0],4)*this->q[1] + 
             (this->q[1] * this->q[1] + this->q[3] * this->q[3])*this->kg[1]*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) + 
             4*p0*this->q[0]*(2*this->q[0] * this->q[0]*this->q[1] + 
                this->kg[1]*(this->q[3] * this->q[3] - 2*this->q[1]*this->kg[1]) - this->q[3]*(this->q[1] + 2*this->kg[1])*this->kg[3]) - 
             this->q[0] * this->q[0]*(pow(this->q[1],3) + this->q[3]*this->kg[1]*(4*this->q[3] - 3*this->kg[3]) + 
                this->q[1]*(this->q[3] * this->q[3] - 3*this->kg[1] * this->kg[1] - 4*this->q[3]*this->kg[3])) + 
             4*pow(p0,2)*(-(this->q[0] * this->q[0]*this->q[1]) + this->kg[1]*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3])))  
+ 2*this->mn*(p0 - this->q[0])*(4*pow(p0,2)*this->q[0]*this->q[1] - 
             2*p0*(3*this->q[0] * this->q[0]*this->q[1] + this->kg[1]*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3])) + 
             this->q[0]*(2*this->q[0] * this->q[0]*this->q[1] - pow(this->q[1],3) - 2*this->q[1] * this->q[1]*this->kg[1] + 
                this->q[3]*this->kg[1]*(-this->q[3] + this->kg[3]) + this->q[1]*(this->kg[1] * this->kg[1] - this->q[3]*(this->q[3] + this->kg[3]))))))  
+ 2*C3vNC*this->mn*(C4v*this->mRes*(4*this->mRes_2*this->mn*
           (2*p0*this->q[0]*this->q[1] - this->q[1]*(this->q[0] * this->q[0] + this->kg[1] * this->kg[1]) - this->q[3]*this->kg[1]*this->kg[3]) + 
          this->mRes*(16*pow(p0,3)*this->q[0]*this->q[1] - pow(this->q[0],4)*this->q[1] + 
             2*p0*this->q[0]*(5*this->q[0] * this->q[0]*this->q[1] - 2*pow(this->q[1],3) - 
                4*this->q[1] * this->q[1]*this->kg[1] + this->q[3]*this->kg[1]*(this->q[3] + this->kg[3]) + 
                this->q[1]*(-2*this->q[3] * this->q[3] + this->kg[1] * this->kg[1] - 5*this->q[3]*this->kg[3])) - 
             8*pow(p0,2)*(3*this->q[0] * this->q[0]*this->q[1] + this->kg[1]*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3])) + 
             this->kg[1]*(3*pow(this->q[1],3)*this->kg[1] + this->q[1]*this->q[3]*this->kg[1]*(3*this->q[3] + 4*this->kg[3]) + 
                this->q[1] * this->q[1]*(2*this->kg[1] * this->kg[1] + 3*this->q[3]*this->kg[3]) + 
                this->q[3] * this->q[3]*(-2*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + 3*this->q[3]*this->kg[3]) 
) + this->q[0] * this->q[0]*(-3*pow(this->q[1],3) - 2*this->q[1] * this->q[1]*this->kg[1] + 
                this->q[3]*this->kg[1]*(-4*this->q[3] + this->kg[3]) + 
                this->q[1]*(-3*this->q[3] * this->q[3] + this->kg[1] * this->kg[1] + 4*this->q[3]*this->kg[3]))) + 
          2*this->mn*(p0 - this->q[0])*(4*pow(p0,2)*this->q[0]*this->q[1] - 
             2*p0*(3*this->q[0] * this->q[0]*this->q[1] + this->kg[1]*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3])) + 
             this->q[0]*(2*this->q[0] * this->q[0]*this->q[1] - pow(this->q[1],3) - 2*this->q[1] * this->q[1]*this->kg[1] + 
                this->q[3]*this->kg[1]*(-this->q[3] + this->kg[3]) + this->q[1]*(this->kg[1] * this->kg[1] - this->q[3]*(this->q[3] + this->kg[3]))))) + 
       C5v*(4*pow(this->mRes,3)*this->mn*
           (this->q[1]*(2*p0*this->q[0] - this->q[0] * this->q[0] + this->kg[1] * this->kg[1]) + this->q[3]*this->kg[1]*this->kg[3]) + 
          this->mRes_2*(16*pow(p0,3)*this->q[0]*this->q[1] - pow(this->q[0],4)*this->q[1] - 
             (this->q[1] * this->q[1] + this->q[3] * this->q[3])*this->kg[1]*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) + 
             this->q[0] * this->q[0]*(-3*pow(this->q[1],3) + 8*this->q[1] * this->q[1]*this->kg[1] - 
                3*this->q[1]*(this->q[3] * this->q[3] + this->kg[1] * this->kg[1]) + 
                this->q[3]*this->kg[1]*(4*this->q[3] - 3*this->kg[3]) + 4*this->q[1]*this->q[3]*this->kg[3]) + 
             2*p0*this->q[0]*(5*this->q[0] * this->q[0]*this->q[1] - 2*pow(this->q[1],3) - 
                4*this->q[1] * this->q[1]*this->kg[1] + this->q[3]*this->kg[1]*(this->q[3] + this->kg[3]) + 
                this->q[1]*(-2*this->q[3] * this->q[3] + this->kg[1] * this->kg[1] - 5*this->q[3]*this->kg[3])) + 
             8*pow(p0,2)*(-3*this->q[0] * this->q[0]*this->q[1] + this->kg[1]*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]))) + 
          2*this->mRes*this->mn*(4*pow(p0,3)*this->q[0]*this->q[1] - 2*pow(this->q[0],4)*this->q[1] + 
             this->kg[1]*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3])*(this->q[1]*(this->q[1] + this->kg[1]) + this->q[3]*(this->q[3] + this->kg[3])) + 
             this->q[0] * this->q[0]*(pow(this->q[1],3) - this->q[1] * this->q[1]*this->kg[1] - 
                2*this->q[3] * this->q[3]*this->kg[1] + this->q[1]*this->q[3]*(this->q[3] + this->kg[3])) + 
             2*pow(p0,2)*(-5*this->q[0] * this->q[0]*this->q[1] + this->kg[1]*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3])) + 
             p0*this->q[0]*(8*this->q[0] * this->q[0]*this->q[1] - pow(this->q[1],3) + 2*this->q[1] * this->q[1]*this->kg[1] + 
                this->q[3]*this->kg[1]*(3*this->q[3] - this->kg[3]) - this->q[1]*(this->kg[1] * this->kg[1] + this->q[3]*(this->q[3] + this->kg[3])))) - 
          this->kg[1]*(pow(this->q[0],4)*(5*this->q[1] * this->q[1] - 3*this->q[1]*this->kg[1] + this->q[3]*(8*this->q[3] - 3*this->kg[3])) - 
             8*pow(p0,3)*this->q[0]*(this->q[1] * this->q[1] - this->q[1]*this->kg[1] + this->q[3]*(this->q[3] - this->kg[3])) - 
             4*pow(p0,2)*(this->q[1] * this->q[1] - this->q[1]*this->kg[1] + this->q[3]*(this->q[3] - this->kg[3]))*
              (-5*this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) + 
             (this->q[1] * this->q[1] + this->q[3] * this->q[3])*
              (pow(this->q[1],3)*this->kg[1] - 
                this->q[3] * this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + 
                pow(this->q[3],3)*this->kg[3] + this->q[1]*this->q[3]*this->kg[1]*(this->q[3] + 2*this->kg[3]) + 
                this->q[1] * this->q[1]*(this->kg[1] * this->kg[1] + this->q[3]*this->kg[3])) - 
             this->q[0] * this->q[0]*(pow(this->q[1],4) + 6*pow(this->q[1],3)*this->kg[1] + 
                6*this->q[1]*this->q[3]*this->kg[1]*(this->q[3] - this->kg[3]) + 
                3*this->q[3] * this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + 2*this->q[3]*this->kg[3]) + 
                this->q[1] * this->q[1]*(this->q[3] * this->q[3] - 3*this->kg[1] * this->kg[1] + 6*this->q[3]*this->kg[3])) + 
             2*p0*this->q[0]*(pow(this->q[1],4) + 5*pow(this->q[1],3)*this->kg[1] + 
                this->q[1]*this->q[3]*this->kg[1]*(5*this->q[3] - 8*this->kg[3]) + 
                this->q[1] * this->q[1]*(2*this->q[3] * this->q[3] - 4*this->kg[1] * this->kg[1] + 5*this->q[3]*this->kg[3]) + 
                this->q[3] * this->q[3]*(4*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + 
                   this->q[3]*(this->q[3] + 5*this->kg[3])) + 
                this->q[0] * this->q[0]*(-9*this->q[1] * this->q[1] + 7*this->q[1]*this->kg[1] + 
                   this->q[3]*(-13*this->q[3] + 7*this->kg[3])))))))/(24.*this->mRes_2*pow(this->mn,5));
    
	tr[1][1] = -(-32*C4v*C4vNC*pow(this->mRes,3)*pow(p0,3)*this->q[0] * this->q[0] - 
     32*C4vNC*C5v*pow(this->mRes,3)*pow(p0,3)*this->q[0] * this->q[0] - 
     32*C4v*C5vNC*pow(this->mRes,3)*pow(p0,3)*this->q[0] * this->q[0] - 
     32*C5v*C5vNC*pow(this->mRes,3)*pow(p0,3)*this->q[0] * this->q[0] - 
     32*C4v*C4vNC*this->mRes_2*this->mn*pow(p0,3)*this->q[0] * this->q[0] - 
     32*C4vNC*C5v*this->mRes_2*this->mn*pow(p0,3)*this->q[0] * this->q[0] - 
     32*C4v*C5vNC*this->mRes_2*this->mn*pow(p0,3)*this->q[0] * this->q[0] - 
     32*C5v*C5vNC*this->mRes_2*this->mn*pow(p0,3)*this->q[0] * this->q[0] + 
     48*C4v*C4vNC*pow(this->mRes,3)*pow(p0,2)*pow(this->q[0],3) + 
     48*C4vNC*C5v*pow(this->mRes,3)*pow(p0,2)*pow(this->q[0],3) + 
     16*C4v*C5vNC*pow(this->mRes,3)*pow(p0,2)*pow(this->q[0],3) + 
     16*C5v*C5vNC*pow(this->mRes,3)*pow(p0,2)*pow(this->q[0],3) + 
     80*C4v*C4vNC*this->mRes_2*this->mn*pow(p0,2)*pow(this->q[0],3) + 
     80*C4vNC*C5v*this->mRes_2*this->mn*pow(p0,2)*pow(this->q[0],3) + 
     48*C4v*C5vNC*this->mRes_2*this->mn*pow(p0,2)*pow(this->q[0],3) + 
     48*C5v*C5vNC*this->mRes_2*this->mn*pow(p0,2)*pow(this->q[0],3) - 
     16*C4v*C4vNC*pow(this->mRes,3)*p0*pow(this->q[0],4) - 
     16*C4vNC*C5v*pow(this->mRes,3)*p0*pow(this->q[0],4) - 
     64*C4v*C4vNC*this->mRes_2*this->mn*p0*pow(this->q[0],4) - 
     64*C4vNC*C5v*this->mRes_2*this->mn*p0*pow(this->q[0],4) - 
     16*C4v*C5vNC*this->mRes_2*this->mn*p0*pow(this->q[0],4) - 
     16*C5v*C5vNC*this->mRes_2*this->mn*p0*pow(this->q[0],4) + 
     16*C4v*C4vNC*this->mRes_2*this->mn*pow(this->q[0],5) + 
     16*C4vNC*C5v*this->mRes_2*this->mn*pow(this->q[0],5) + 
     8*C4v*C4vNC*pow(this->mRes,3)*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1] + 
     8*C4vNC*C5v*pow(this->mRes,3)*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1] - 
     16*C4v*C5vNC*pow(this->mRes,3)*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1] - 
     16*C5v*C5vNC*pow(this->mRes,3)*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1] + 
     8*C4v*C4vNC*this->mRes_2*this->mn*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1] + 
     8*C4vNC*C5v*this->mRes_2*this->mn*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1] - 
     8*C4v*C5vNC*this->mRes_2*this->mn*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1] - 
     8*C5v*C5vNC*this->mRes_2*this->mn*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1] + 
     8*C4v*C5vNC*this->mRes*pow(p0,3)*this->q[0] * this->q[0]*this->q[1] * this->q[1] + 
     8*C5v*C5vNC*this->mRes*pow(p0,3)*this->q[0] * this->q[0]*this->q[1] * this->q[1] + 
     4*C4v*C4vNC*pow(this->mRes,3)*pow(this->q[0],3)*this->q[1] * this->q[1] + 
     4*C4vNC*C5v*pow(this->mRes,3)*pow(this->q[0],3)*this->q[1] * this->q[1] - 
     8*C4v*C4vNC*this->mRes_2*this->mn*pow(this->q[0],3)*this->q[1] * this->q[1] - 
     8*C4vNC*C5v*this->mRes_2*this->mn*pow(this->q[0],3)*this->q[1] * this->q[1] + 
     8*C4v*C5vNC*this->mRes_2*this->mn*pow(this->q[0],3)*this->q[1] * this->q[1] + 
     8*C5v*C5vNC*this->mRes_2*this->mn*pow(this->q[0],3)*this->q[1] * this->q[1] - 
     12*C4v*C5vNC*this->mRes*pow(p0,2)*pow(this->q[0],3)*this->q[1] * this->q[1] - 
     12*C5v*C5vNC*this->mRes*pow(p0,2)*pow(this->q[0],3)*this->q[1] * this->q[1] + 
     6*C4v*C5vNC*this->mRes*p0*pow(this->q[0],4)*this->q[1] * this->q[1] + 
     6*C5v*C5vNC*this->mRes*p0*pow(this->q[0],4)*this->q[1] * this->q[1] - 
     C4v*C5vNC*this->mRes*pow(this->q[0],5)*this->q[1] * this->q[1] - 
     C5v*C5vNC*this->mRes*pow(this->q[0],5)*this->q[1] * this->q[1] - 
     2*C4v*C5vNC*this->mRes*p0*this->q[0] * this->q[0]*pow(this->q[1],4) - 
     2*C5v*C5vNC*this->mRes*p0*this->q[0] * this->q[0]*pow(this->q[1],4) + 
     C4v*C5vNC*this->mRes*pow(this->q[0],3)*pow(this->q[1],4) + 
     C5v*C5vNC*this->mRes*pow(this->q[0],3)*pow(this->q[1],4) - 
     16*C4v*C4vNC*pow(this->mRes,3)*pow(p0,2)*this->q[0]*this->q[3] * this->q[3] - 
     16*C4vNC*C5v*pow(this->mRes,3)*pow(p0,2)*this->q[0]*this->q[3] * this->q[3] + 
     16*C4v*C5vNC*pow(this->mRes,3)*pow(p0,2)*this->q[0]*this->q[3] * this->q[3] + 
     16*C5v*C5vNC*pow(this->mRes,3)*pow(p0,2)*this->q[0]*this->q[3] * this->q[3] - 
     16*C4v*C4vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[0]*this->q[3] * this->q[3] - 
     16*C4vNC*C5v*this->mRes_2*this->mn*pow(p0,2)*this->q[0]*this->q[3] * this->q[3] + 
     16*C4v*C5vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[0]*this->q[3] * this->q[3] + 
     16*C5v*C5vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[0]*this->q[3] * this->q[3] - 
     16*C4v*C5vNC*pow(this->mRes,3)*p0*this->q[0] * this->q[0]*this->q[3] * this->q[3] - 
     16*C5v*C5vNC*pow(this->mRes,3)*p0*this->q[0] * this->q[0]*this->q[3] * this->q[3] + 
     16*C4v*C4vNC*this->mRes_2*this->mn*p0*this->q[0] * this->q[0]*this->q[3] * this->q[3] + 
     16*C4vNC*C5v*this->mRes_2*this->mn*p0*this->q[0] * this->q[0]*this->q[3] * this->q[3] - 
     32*C4v*C5vNC*this->mRes_2*this->mn*p0*this->q[0] * this->q[0]*this->q[3] * this->q[3] - 
     32*C5v*C5vNC*this->mRes_2*this->mn*p0*this->q[0] * this->q[0]*this->q[3] * this->q[3] + 
     16*C4v*C5vNC*this->mRes_2*this->mn*pow(this->q[0],3)*this->q[3] * this->q[3] + 
     16*C5v*C5vNC*this->mRes_2*this->mn*pow(this->q[0],3)*this->q[3] * this->q[3] - 
     2*C4v*C5vNC*this->mRes*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3] * this->q[3] - 
     2*C5v*C5vNC*this->mRes*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3] * this->q[3] + 
     C4v*C5vNC*this->mRes*pow(this->q[0],3)*this->q[1] * this->q[1]*this->q[3] * this->q[3] + 
     C5v*C5vNC*this->mRes*pow(this->q[0],3)*this->q[1] * this->q[1]*this->q[3] * this->q[3] + 
     16*C4v*C4vNC*pow(this->mRes,3)*p0*this->q[0] * this->q[0]*this->q[1]*this->kg[1] + 
     8*C4vNC*C5v*pow(this->mRes,3)*p0*this->q[0] * this->q[0]*this->q[1]*this->kg[1] + 
     8*C4v*C5vNC*pow(this->mRes,3)*p0*this->q[0] * this->q[0]*this->q[1]*this->kg[1] + 
     32*C5v*C5vNC*pow(this->mRes,3)*p0*this->q[0] * this->q[0]*this->q[1]*this->kg[1] + 
     16*C4v*C4vNC*this->mRes_2*this->mn*p0*this->q[0] * this->q[0]*this->q[1]*this->kg[1] + 
     16*C5v*C5vNC*this->mRes_2*this->mn*p0*this->q[0] * this->q[0]*this->q[1]*this->kg[1] - 
     8*C4vNC*C5v*this->mRes*pow(p0,3)*this->q[0] * this->q[0]*this->q[1]*this->kg[1] - 
     8*C4v*C5vNC*this->mRes*pow(p0,3)*this->q[0] * this->q[0]*this->q[1]*this->kg[1] - 
     48*C5v*C5vNC*this->mRes*pow(p0,3)*this->q[0] * this->q[0]*this->q[1]*this->kg[1] - 
     32*C5v*C5vNC*this->mn*pow(p0,3)*this->q[0] * this->q[0]*this->q[1]*this->kg[1] + 
     4*C4v*C4vNC*pow(this->mRes,3)*pow(this->q[0],3)*this->q[1]*this->kg[1] - 
     8*C4vNC*C5v*pow(this->mRes,3)*pow(this->q[0],3)*this->q[1]*this->kg[1] - 
     4*C4v*C5vNC*pow(this->mRes,3)*pow(this->q[0],3)*this->q[1]*this->kg[1] - 
     16*C4v*C4vNC*this->mRes_2*this->mn*pow(this->q[0],3)*this->q[1]*this->kg[1] - 
     16*C5v*C5vNC*this->mRes_2*this->mn*pow(this->q[0],3)*this->q[1]*this->kg[1] + 
     20*C4vNC*C5v*this->mRes*pow(p0,2)*pow(this->q[0],3)*this->q[1]*this->kg[1] + 
     24*C4v*C5vNC*this->mRes*pow(p0,2)*pow(this->q[0],3)*this->q[1]*this->kg[1] + 
     60*C5v*C5vNC*this->mRes*pow(p0,2)*pow(this->q[0],3)*this->q[1]*this->kg[1] + 
     64*C5v*C5vNC*this->mn*pow(p0,2)*pow(this->q[0],3)*this->q[1]*this->kg[1] - 
     18*C4vNC*C5v*this->mRes*p0*pow(this->q[0],4)*this->q[1]*this->kg[1] - 
     18*C4v*C5vNC*this->mRes*p0*pow(this->q[0],4)*this->q[1]*this->kg[1] - 
     20*C5v*C5vNC*this->mRes*p0*pow(this->q[0],4)*this->q[1]*this->kg[1] - 
     40*C5v*C5vNC*this->mn*p0*pow(this->q[0],4)*this->q[1]*this->kg[1] + 
     5*C4vNC*C5v*this->mRes*pow(this->q[0],5)*this->q[1]*this->kg[1] + 
     4*C4v*C5vNC*this->mRes*pow(this->q[0],5)*this->q[1]*this->kg[1] + 
     C5v*C5vNC*this->mRes*pow(this->q[0],5)*this->q[1]*this->kg[1] + 
     8*C5v*C5vNC*this->mn*pow(this->q[0],5)*this->q[1]*this->kg[1] + 
     2*C4vNC*C5v*this->mRes*p0*this->q[0] * this->q[0]*pow(this->q[1],3)*this->kg[1] - 
     2*C4v*C5vNC*this->mRes*p0*this->q[0] * this->q[0]*pow(this->q[1],3)*this->kg[1] - 
     C4vNC*C5v*this->mRes*pow(this->q[0],3)*pow(this->q[1],3)*this->kg[1] - 
     C5v*C5vNC*this->mRes*pow(this->q[0],3)*pow(this->q[1],3)*this->kg[1] + 
     4*C4v*C4vNC*pow(this->mRes,3)*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 
     4*C4vNC*C5v*pow(this->mRes,3)*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1] - 
     4*C4v*C5vNC*this->mRes*pow(p0,2)*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1] - 
     4*C5v*C5vNC*this->mRes*pow(p0,2)*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1] - 
     2*C4vNC*C5v*this->mRes*p0*this->q[0] * this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1] - 
     2*C4v*C5vNC*this->mRes*p0*this->q[0] * this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1] - 
     4*C5v*C5vNC*this->mRes*p0*this->q[0] * this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1] - 
     8*C5v*C5vNC*this->mn*p0*this->q[0] * this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 
     3*C4vNC*C5v*this->mRes*pow(this->q[0],3)*this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 
     3*C4v*C5vNC*this->mRes*pow(this->q[0],3)*this->q[1]*this->q[3] * this->q[3]*this->kg[1] - 
     2*C5v*C5vNC*this->mRes*pow(this->q[0],3)*this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 
     8*C5v*C5vNC*this->mn*pow(this->q[0],3)*this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 
     C4v*C5vNC*this->mRes*this->q[0]*pow(this->q[1],3)*this->q[3] * this->q[3]*this->kg[1] + 
     C5v*C5vNC*this->mRes*this->q[0]*pow(this->q[1],3)*this->q[3] * this->q[3]*this->kg[1] + 
     C4v*C5vNC*this->mRes*this->q[0]*this->q[1]*pow(this->q[3],4)*this->kg[1] + 
     C5v*C5vNC*this->mRes*this->q[0]*this->q[1]*pow(this->q[3],4)*this->kg[1] + 
     16*C4v*C4vNC*pow(this->mRes,3)*pow(p0,2)*this->q[0]*this->kg[1] * this->kg[1] - 
     16*C4vNC*C5v*pow(this->mRes,3)*pow(p0,2)*this->q[0]*this->kg[1] * this->kg[1] + 
     16*C4v*C5vNC*pow(this->mRes,3)*pow(p0,2)*this->q[0]*this->kg[1] * this->kg[1] - 
     16*C5v*C5vNC*pow(this->mRes,3)*pow(p0,2)*this->q[0]*this->kg[1] * this->kg[1] + 
     16*C4v*C4vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[0]*this->kg[1] * this->kg[1] - 
     16*C4vNC*C5v*this->mRes_2*this->mn*pow(p0,2)*this->q[0]*this->kg[1] * this->kg[1] + 
     16*C4v*C5vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[0]*this->kg[1] * this->kg[1] - 
     16*C5v*C5vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[0]*this->kg[1] * this->kg[1] - 
     8*C4v*C4vNC*pow(this->mRes,3)*p0*this->q[0] * this->q[0]*this->kg[1] * this->kg[1] + 
     8*C4v*C5vNC*pow(this->mRes,3)*p0*this->q[0] * this->q[0]*this->kg[1] * this->kg[1] - 
     16*C5v*C5vNC*pow(this->mRes,3)*p0*this->q[0] * this->q[0]*this->kg[1] * this->kg[1] - 
     24*C4v*C4vNC*this->mRes_2*this->mn*p0*this->q[0] * this->q[0]*this->kg[1] * this->kg[1] + 
     24*C4vNC*C5v*this->mRes_2*this->mn*p0*this->q[0] * this->q[0]*this->kg[1] * this->kg[1] - 
     8*C4v*C5vNC*this->mRes_2*this->mn*p0*this->q[0] * this->q[0]*this->kg[1] * this->kg[1] + 
     8*C5v*C5vNC*this->mRes_2*this->mn*p0*this->q[0] * this->q[0]*this->kg[1] * this->kg[1] + 
     8*C4vNC*C5v*this->mRes*pow(p0,3)*this->q[0] * this->q[0]*this->kg[1] * this->kg[1] + 
     8*C5v*C5vNC*this->mRes*pow(p0,3)*this->q[0] * this->q[0]*this->kg[1] * this->kg[1] + 
     4*C4vNC*C5v*pow(this->mRes,3)*pow(this->q[0],3)*this->kg[1] * this->kg[1] + 
     4*C4v*C5vNC*pow(this->mRes,3)*pow(this->q[0],3)*this->kg[1] * this->kg[1] + 
     8*C4v*C4vNC*this->mRes_2*this->mn*pow(this->q[0],3)*this->kg[1] * this->kg[1] - 
     8*C4vNC*C5v*this->mRes_2*this->mn*pow(this->q[0],3)*this->kg[1] * this->kg[1] - 
     8*C4v*C5vNC*this->mRes_2*this->mn*pow(this->q[0],3)*this->kg[1] * this->kg[1] + 
     8*C5v*C5vNC*this->mRes_2*this->mn*pow(this->q[0],3)*this->kg[1] * this->kg[1] - 
     20*C4vNC*C5v*this->mRes*pow(p0,2)*pow(this->q[0],3)*this->kg[1] * this->kg[1] - 
     4*C4v*C5vNC*this->mRes*pow(p0,2)*pow(this->q[0],3)*this->kg[1] * this->kg[1] - 
     32*C5v*C5vNC*this->mRes*pow(p0,2)*pow(this->q[0],3)*this->kg[1] * this->kg[1] - 
     16*C5v*C5vNC*this->mn*pow(p0,2)*pow(this->q[0],3)*this->kg[1] * this->kg[1] + 
     14*C4vNC*C5v*this->mRes*p0*pow(this->q[0],4)*this->kg[1] * this->kg[1] + 
     8*C4v*C5vNC*this->mRes*p0*pow(this->q[0],4)*this->kg[1] * this->kg[1] + 
     14*C5v*C5vNC*this->mRes*p0*pow(this->q[0],4)*this->kg[1] * this->kg[1] + 
     24*C5v*C5vNC*this->mn*p0*pow(this->q[0],4)*this->kg[1] * this->kg[1] - 
     3*C4vNC*C5v*this->mRes*pow(this->q[0],5)*this->kg[1] * this->kg[1] - 
     3*C4v*C5vNC*this->mRes*pow(this->q[0],5)*this->kg[1] * this->kg[1] - 
     8*C5v*C5vNC*this->mn*pow(this->q[0],5)*this->kg[1] * this->kg[1] - 
     4*C4v*C4vNC*pow(this->mRes,3)*this->q[0]*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] + 
     4*C4v*C5vNC*pow(this->mRes,3)*this->q[0]*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] - 
     4*C4vNC*C5v*this->mRes*pow(p0,2)*this->q[0]*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] - 
     8*C4v*C5vNC*this->mRes*pow(p0,2)*this->q[0]*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] - 
     20*C5v*C5vNC*this->mRes*pow(p0,2)*this->q[0]*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] - 
     16*C5v*C5vNC*this->mn*pow(p0,2)*this->q[0]*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] + 
     10*C4vNC*C5v*this->mRes*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] + 
     12*C4v*C5vNC*this->mRes*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] + 
     14*C5v*C5vNC*this->mRes*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] + 
     24*C5v*C5vNC*this->mn*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] - 
     6*C4vNC*C5v*this->mRes*pow(this->q[0],3)*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] - 
     5*C4v*C5vNC*this->mRes*pow(this->q[0],3)*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] + 
     C5v*C5vNC*this->mRes*pow(this->q[0],3)*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] - 
     8*C5v*C5vNC*this->mn*pow(this->q[0],3)*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] + 
     C4vNC*C5v*this->mRes*this->q[0]*pow(this->q[1],4)*this->kg[1] * this->kg[1] - 
     C5v*C5vNC*this->mRes*this->q[0]*pow(this->q[1],4)*this->kg[1] * this->kg[1] + 
     16*C4v*C4vNC*pow(this->mRes,3)*p0*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
     16*C5v*C5vNC*pow(this->mRes,3)*p0*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
     16*C4v*C4vNC*this->mRes_2*this->mn*p0*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
     16*C5v*C5vNC*this->mRes_2*this->mn*p0*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
     4*C4v*C4vNC*pow(this->mRes,3)*this->q[0]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
     4*C4vNC*C5v*pow(this->mRes,3)*this->q[0]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
     16*C4v*C4vNC*this->mRes_2*this->mn*this->q[0]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
     16*C5v*C5vNC*this->mRes_2*this->mn*this->q[0]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
     8*C4vNC*C5v*this->mRes*pow(p0,2)*this->q[0]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
     4*C4v*C5vNC*this->mRes*pow(p0,2)*this->q[0]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
     20*C5v*C5vNC*this->mRes*pow(p0,2)*this->q[0]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
     16*C5v*C5vNC*this->mn*pow(p0,2)*this->q[0]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
     10*C4vNC*C5v*this->mRes*p0*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
     8*C4v*C5vNC*this->mRes*p0*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
     10*C5v*C5vNC*this->mRes*p0*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
     24*C5v*C5vNC*this->mn*p0*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
     3*C4vNC*C5v*this->mRes*pow(this->q[0],3)*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
     2*C4v*C5vNC*this->mRes*pow(this->q[0],3)*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
     C5v*C5vNC*this->mRes*pow(this->q[0],3)*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
     8*C5v*C5vNC*this->mn*pow(this->q[0],3)*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
     C4vNC*C5v*this->mRes*this->q[0]*this->q[1] * this->q[1]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
     C4v*C5vNC*this->mRes*this->q[0]*this->q[1] * this->q[1]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
     2*C5v*C5vNC*this->mRes*this->q[0]*this->q[1] * this->q[1]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
     C4v*C5vNC*this->mRes*this->q[0]*pow(this->q[3],4)*this->kg[1] * this->kg[1] - 
     C5v*C5vNC*this->mRes*this->q[0]*pow(this->q[3],4)*this->kg[1] * this->kg[1] - 
     4*C4v*C4vNC*pow(this->mRes,3)*this->q[0]*this->q[1]*pow(this->kg[1],3) - 
     4*C4v*C5vNC*pow(this->mRes,3)*this->q[0]*this->q[1]*pow(this->kg[1],3) + 
     4*C4vNC*C5v*this->mRes*pow(p0,2)*this->q[0]*this->q[1]*pow(this->kg[1],3) + 
     4*C5v*C5vNC*this->mRes*pow(p0,2)*this->q[0]*this->q[1]*pow(this->kg[1],3) - 
     8*C4vNC*C5v*this->mRes*p0*this->q[0] * this->q[0]*this->q[1]*pow(this->kg[1],3) - 
     4*C4v*C5vNC*this->mRes*p0*this->q[0] * this->q[0]*this->q[1]*pow(this->kg[1],3) - 
     12*C5v*C5vNC*this->mRes*p0*this->q[0] * this->q[0]*this->q[1]*pow(this->kg[1],3) - 
     8*C5v*C5vNC*this->mn*p0*this->q[0] * this->q[0]*this->q[1]*pow(this->kg[1],3) + 
     3*C4vNC*C5v*this->mRes*pow(this->q[0],3)*this->q[1]*pow(this->kg[1],3) + 
     4*C4v*C5vNC*this->mRes*pow(this->q[0],3)*this->q[1]*pow(this->kg[1],3) - 
     C5v*C5vNC*this->mRes*pow(this->q[0],3)*this->q[1]*pow(this->kg[1],3) + 
     8*C5v*C5vNC*this->mn*pow(this->q[0],3)*this->q[1]*pow(this->kg[1],3) + 
     C4vNC*C5v*this->mRes*this->q[0]*pow(this->q[1],3)*pow(this->kg[1],3) + 
     C5v*C5vNC*this->mRes*this->q[0]*pow(this->q[1],3)*pow(this->kg[1],3) + 
     8*C4vNC*C5v*this->mRes*p0*this->q[1]*this->q[3] * this->q[3]*pow(this->kg[1],3) + 
     8*C4v*C5vNC*this->mRes*p0*this->q[1]*this->q[3] * this->q[3]*pow(this->kg[1],3) + 
     16*C5v*C5vNC*this->mRes*p0*this->q[1]*this->q[3] * this->q[3]*pow(this->kg[1],3) + 
     16*C5v*C5vNC*this->mn*p0*this->q[1]*this->q[3] * this->q[3]*pow(this->kg[1],3) - 
     7*C4vNC*C5v*this->mRes*this->q[0]*this->q[1]*this->q[3] * this->q[3]*pow(this->kg[1],3) - 
     8*C4v*C5vNC*this->mRes*this->q[0]*this->q[1]*this->q[3] * this->q[3]*pow(this->kg[1],3) + 
     C5v*C5vNC*this->mRes*this->q[0]*this->q[1]*this->q[3] * this->q[3]*pow(this->kg[1],3) - 
     16*C5v*C5vNC*this->mn*this->q[0]*this->q[1]*this->q[3] * this->q[3]*pow(this->kg[1],3) + 
     std::complex<double>(0,4)*C4v*C5a*pow(this->mRes,3)*this->mn2*this->q[1]*this->q[3]*this->kg[2] + 
     std::complex<double>(0,4)*C5a*C5v*pow(this->mRes,3)*this->mn2*this->q[1]*this->q[3]*this->kg[2] + 
     std::complex<double>(0,4)*C4v*C5a*this->mRes_2*pow(this->mn,3)*this->q[1]*this->q[3]*this->kg[2] + 
     std::complex<double>(0,4)*C5a*C5v*this->mRes_2*pow(this->mn,3)*this->q[1]*this->q[3]*this->kg[2] + 
     std::complex<double>(0,4)*C4v*C5a*this->mRes*this->mn2*p0*this->q[0]*this->q[1]*this->q[3]*this->kg[2] + 
     std::complex<double>(0,4)*C5a*C5v*this->mRes*this->mn2*p0*this->q[0]*this->q[1]*this->q[3]*this->kg[2] - 
     std::complex<double>(0,2)*C4v*C5a*this->mRes*this->mn2*this->q[0] * this->q[0]*this->q[1]*this->q[3]*this->kg[2] - 
     std::complex<double>(0,2)*C5a*C5v*this->mRes*this->mn2*this->q[0] * this->q[0]*this->q[1]*this->q[3]*this->kg[2] + 
     std::complex<double>(0,4)*C4v*C5a*pow(this->mRes,3)*this->mn2*this->q[3]*this->kg[1]*this->kg[2] - 
     std::complex<double>(0,4)*C5a*C5v*pow(this->mRes,3)*this->mn2*this->q[3]*this->kg[1]*this->kg[2] + 
     std::complex<double>(0,4)*C4v*C5a*this->mRes_2*pow(this->mn,3)*this->q[3]*this->kg[1]*this->kg[2] - 
     std::complex<double>(0,4)*C5a*C5v*this->mRes_2*pow(this->mn,3)*this->q[3]*this->kg[1]*this->kg[2] + 
     std::complex<double>(0,4)*C4v*C5a*this->mRes*this->mn2*p0*this->q[0]*this->q[3]*this->kg[1]*this->kg[2] - 
     std::complex<double>(0,4)*C5a*C5v*this->mRes*this->mn2*p0*this->q[0]*this->q[3]*this->kg[1]*this->kg[2] - 
     std::complex<double>(0,2)*C4v*C5a*this->mRes*this->mn2*this->q[0] * this->q[0]*this->q[3]*this->kg[1]*this->kg[2] + 
     std::complex<double>(0,2)*C5a*C5v*this->mRes*this->mn2*this->q[0] * this->q[0]*this->q[3]*this->kg[1]*this->kg[2] + 
     std::complex<double>(0,2)*C4v*C5a*this->mRes*this->mn2*this->q[1] * this->q[1]*this->q[3]*this->kg[1]*this->kg[2] + 
     std::complex<double>(0,2)*C5a*C5v*this->mRes*this->mn2*this->q[1] * this->q[1]*this->q[3]*this->kg[1]*this->kg[2] + 
     std::complex<double>(0,2)*C4v*C5a*this->mRes*this->mn2*this->q[1]*this->q[3]*this->kg[1] * this->kg[1]*this->kg[2] - 
     std::complex<double>(0,2)*C5a*C5v*this->mRes*this->mn2*this->q[1]*this->q[3]*this->kg[1] * this->kg[1]*this->kg[2] + 
     8*C4v*C4vNC*pow(this->mRes,3)*p0*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] + 
     8*C4vNC*C5v*pow(this->mRes,3)*p0*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] + 
     8*C4v*C5vNC*pow(this->mRes,3)*p0*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] + 
     8*C5v*C5vNC*pow(this->mRes,3)*p0*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] + 
     8*C4v*C4vNC*this->mRes_2*this->mn*p0*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] + 
     8*C4vNC*C5v*this->mRes_2*this->mn*p0*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] + 
     8*C4v*C5vNC*this->mRes_2*this->mn*p0*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] + 
     8*C5v*C5vNC*this->mRes_2*this->mn*p0*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] - 
     8*C4v*C4vNC*this->mRes_2*this->mn*this->q[0]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] - 
     8*C4vNC*C5v*this->mRes_2*this->mn*this->q[0]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] - 
     8*C4v*C5vNC*this->mRes_2*this->mn*this->q[0]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] - 
     8*C5v*C5vNC*this->mRes_2*this->mn*this->q[0]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] + 
     4*C4vNC*C5v*this->mRes*p0*this->q[1]*this->q[3] * this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] + 
     4*C4v*C5vNC*this->mRes*p0*this->q[1]*this->q[3] * this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] + 
     8*C5v*C5vNC*this->mRes*p0*this->q[1]*this->q[3] * this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] + 
     8*C5v*C5vNC*this->mn*p0*this->q[1]*this->q[3] * this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] - 
     4*C4vNC*C5v*this->mRes*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] - 
     4*C4v*C5vNC*this->mRes*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] - 
     8*C5v*C5vNC*this->mn*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] - 
     this->q[3]*(C5v*(C4vNC*this->mRes*(4*this->mRes*
               (2*this->mn*(p0 - this->q[0])*
                  (4*p0*this->q[0] - 3*this->q[0] * this->q[0] + this->q[1] * this->q[1] + 
                    this->q[3] * this->q[3]) + 
                 this->mRes*(8*pow(p0,2)*this->q[0] + this->q[0]*this->q[1] * this->q[1] + 
                    2*p0*(-3*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3]) 
)) - this->q[0]*this->q[1]*(4*this->mRes_2 - 12*pow(p0,2) + 20*p0*this->q[0] - 
                 9*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3])*this->kg[1] + 
              (-4*pow(p0,2)*this->q[0] - 3*pow(this->q[0],3) - 
                 5*this->q[0]*this->q[1] * this->q[1] + 3*this->q[0]*this->q[3] * this->q[3] + 
                 4*p0*(2*this->q[0] * this->q[0] + this->q[1] * this->q[1] - this->q[3] * this->q[3]))*
               this->kg[1] * this->kg[1]) + 
           C5vNC*(8*pow(this->mRes,3)*p0*
               (4*p0*this->q[0] - this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3] + 
                 2*this->q[1]*this->kg[1]) + 
              8*this->mRes_2*this->mn*(p0 - this->q[0])*
               (4*p0*this->q[0] - this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3] + 
                 2*this->q[1]*this->kg[1]) + 
              this->mRes*(this->q[0]*this->q[1] * this->q[1]*
                  (-pow(-2*p0 + this->q[0],2) + this->q[1] * this->q[1] + this->q[3] * this->q[3])  
+ 8*p0*(5*p0 - 3*this->q[0])*this->q[0]*this->q[1]*this->kg[1] + 
                 (-4*pow(p0,2)*this->q[0] + 12*p0*this->q[0] * this->q[0] + 
                    pow(this->q[0],3) + 8*p0*this->q[1] * this->q[1] - 
                    this->q[0]*this->q[1] * this->q[1] - (8*p0 + this->q[0])*this->q[3] * this->q[3])*
                  this->kg[1] * this->kg[1]) + 
              8*this->mn*(p0 - this->q[0])*this->kg[1]*
               (4*p0*this->q[0]*this->q[1] + (this->q[1] - this->q[3])*(this->q[1] + this->q[3])*this->kg[1] + 
                 this->q[0] * this->q[0]*(-2*this->q[1] + this->kg[1]))) - 
           std::complex<double>(0,2)*C5a*this->mRes*this->mn2*this->q[3]*(this->q[1] - this->kg[1])*this->kg[2]) + 
        C4v*this->mRes*(C5vNC*(8*this->mRes*this->mn*(p0 - this->q[0])*
               (4*p0*this->q[0] - this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3]) + 
              this->q[0]*this->q[1] * this->q[1]*
               (-pow(-2*p0 + this->q[0],2) + this->q[1] * this->q[1] + this->q[3] * this->q[3]) + 
              this->q[0]*this->q[1]*(12*pow(p0,2) - 20*p0*this->q[0] + 7*this->q[0] * this->q[0] + 
                 this->q[1] * this->q[1] + this->q[3] * this->q[3])*this->kg[1] + 
              4*(p0 - this->q[0])*(this->q[0] * this->q[0] + this->q[1] * this->q[1] - this->q[3] * this->q[3])*
               this->kg[1] * this->kg[1] + 
              4*this->mRes_2*
               (8*pow(p0,2)*this->q[0] - 
                 2*p0*(this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3]) + 
                 this->q[0]*this->kg[1]*(-this->q[1] + this->kg[1]))) + 
           4*C4vNC*this->mRes*(2*this->mn*(p0 - this->q[0])*
               (4*p0*this->q[0] - 3*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] + 
                 2*this->q[1]*this->kg[1]) + this->mRes*
               (8*pow(p0,2)*this->q[0] + this->q[0]*pow(this->q[1] + this->kg[1],2) + 
                 2*p0*(-3*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] + 
                    2*this->q[1]*this->kg[1]))) - 
           std::complex<double>(0,2)*C5a*this->mn2*this->q[3]*(this->q[1] + this->kg[1])*this->kg[2]))*this->kg[3] - 
     2*C3v*this->mn*(8*C5vNC*pow(this->mRes,3)*this->mn*p0*this->q[0] * this->q[0] + 
        16*C5vNC*this->mRes_2*pow(p0,3)*this->q[0] * this->q[0] + 
        8*C5vNC*this->mRes*this->mn*pow(p0,3)*this->q[0] * this->q[0] - 
        12*C5vNC*this->mRes_2*pow(p0,2)*pow(this->q[0],3) - 
        12*C5vNC*this->mRes*this->mn*pow(p0,2)*pow(this->q[0],3) + 
        2*C5vNC*this->mRes_2*p0*pow(this->q[0],4) + 
        4*C5vNC*this->mRes*this->mn*p0*pow(this->q[0],4) + 
        12*C5vNC*this->mRes_2*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1] + 
        6*C5vNC*this->mRes*this->mn*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1] - 
        8*C5vNC*pow(p0,3)*this->q[0] * this->q[0]*this->q[1] * this->q[1] - 
        2*C5vNC*this->mRes_2*pow(this->q[0],3)*this->q[1] * this->q[1] - 
        4*C5vNC*this->mRes*this->mn*pow(this->q[0],3)*this->q[1] * this->q[1] + 
        12*C5vNC*pow(p0,2)*pow(this->q[0],3)*this->q[1] * this->q[1] - 
        6*C5vNC*p0*pow(this->q[0],4)*this->q[1] * this->q[1] + 
        C5vNC*pow(this->q[0],5)*this->q[1] * this->q[1] + 
        2*C5vNC*p0*this->q[0] * this->q[0]*pow(this->q[1],4) - 
        C5vNC*pow(this->q[0],3)*pow(this->q[1],4) - 
        4*C5vNC*pow(this->mRes,3)*this->mn*this->q[0]*this->q[3] * this->q[3] - 
        8*C5vNC*this->mRes_2*pow(p0,2)*this->q[0]*this->q[3] * this->q[3] - 
        4*C5vNC*this->mRes*this->mn*pow(p0,2)*this->q[0]*this->q[3] * this->q[3] + 
        14*C5vNC*this->mRes_2*p0*this->q[0] * this->q[0]*this->q[3] * this->q[3] + 
        8*C5vNC*this->mRes*this->mn*p0*this->q[0] * this->q[0]*this->q[3] * this->q[3] - 
        3*C5vNC*this->mRes_2*pow(this->q[0],3)*this->q[3] * this->q[3] - 
        4*C5vNC*this->mRes*this->mn*pow(this->q[0],3)*this->q[3] * this->q[3] - 
        C5vNC*this->mRes_2*this->q[0]*this->q[1] * this->q[1]*this->q[3] * this->q[3] + 
        2*C5vNC*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3] * this->q[3] - 
        C5vNC*pow(this->q[0],3)*this->q[1] * this->q[1]*this->q[3] * this->q[3] - 
        C5vNC*this->mRes_2*this->q[0]*pow(this->q[3],4) - 
        8*C5vNC*this->mRes_2*p0*this->q[0] * this->q[0]*this->q[1]*this->kg[1] + 
        4*C5vNC*this->mRes*this->mn*p0*this->q[0] * this->q[0]*this->q[1]*this->kg[1] + 
        8*C5vNC*pow(p0,3)*this->q[0] * this->q[0]*this->q[1]*this->kg[1] + 
        6*C5vNC*this->mRes_2*pow(this->q[0],3)*this->q[1]*this->kg[1] - 
        24*C5vNC*pow(p0,2)*pow(this->q[0],3)*this->q[1]*this->kg[1] + 
        18*C5vNC*p0*pow(this->q[0],4)*this->q[1]*this->kg[1] - 4*C5vNC*pow(this->q[0],5)*this->q[1]*this->kg[1] + 
        2*C5vNC*p0*this->q[0] * this->q[0]*pow(this->q[1],3)*this->kg[1] - 
        2*C5vNC*this->mRes*this->mn*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 
        4*C5vNC*pow(p0,2)*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 
        2*C5vNC*p0*this->q[0] * this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1] - 
        3*C5vNC*pow(this->q[0],3)*this->q[1]*this->q[3] * this->q[3]*this->kg[1] - 
        C5vNC*this->q[0]*pow(this->q[1],3)*this->q[3] * this->q[3]*this->kg[1] - 
        C5vNC*this->q[0]*this->q[1]*pow(this->q[3],4)*this->kg[1] - 
        4*C5vNC*this->mRes_2*pow(p0,2)*this->q[0]*this->kg[1] * this->kg[1] - 
        4*C5vNC*this->mRes*this->mn*pow(p0,2)*this->q[0]*this->kg[1] * this->kg[1] - 
        6*C5vNC*this->mRes_2*p0*this->q[0] * this->q[0]*this->kg[1] * this->kg[1] + 
        2*C5vNC*this->mRes*this->mn*p0*this->q[0] * this->q[0]*this->kg[1] * this->kg[1] - 
        4*C5vNC*this->mRes_2*pow(this->q[0],3)*this->kg[1] * this->kg[1] + 
        4*C5vNC*this->mRes*this->mn*pow(this->q[0],3)*this->kg[1] * this->kg[1] + 
        4*C5vNC*pow(p0,2)*pow(this->q[0],3)*this->kg[1] * this->kg[1] - 
        8*C5vNC*p0*pow(this->q[0],4)*this->kg[1] * this->kg[1] + 
        3*C5vNC*pow(this->q[0],5)*this->kg[1] * this->kg[1] - 
        2*C5vNC*this->mRes_2*this->q[0]*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] + 
        8*C5vNC*pow(p0,2)*this->q[0]*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] - 
        12*C5vNC*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] + 
        5*C5vNC*pow(this->q[0],3)*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] - 
        4*C5vNC*this->mRes_2*p0*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
        4*C5vNC*this->mRes_2*this->q[0]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
        2*C5vNC*this->mRes*this->mn*this->q[0]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
        4*C5vNC*pow(p0,2)*this->q[0]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
        8*C5vNC*p0*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
        2*C5vNC*pow(this->q[0],3)*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
        C5vNC*this->q[0]*this->q[1] * this->q[1]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
        C5vNC*this->q[0]*pow(this->q[3],4)*this->kg[1] * this->kg[1] + 
        2*C5vNC*this->mRes_2*this->q[0]*this->q[1]*pow(this->kg[1],3) + 
        4*C5vNC*p0*this->q[0] * this->q[0]*this->q[1]*pow(this->kg[1],3) - 
        4*C5vNC*pow(this->q[0],3)*this->q[1]*pow(this->kg[1],3) - 
        8*C5vNC*p0*this->q[1]*this->q[3] * this->q[3]*pow(this->kg[1],3) + 
        8*C5vNC*this->q[0]*this->q[1]*this->q[3] * this->q[3]*pow(this->kg[1],3) - 
        std::complex<double>(0,2)*C5a*this->mRes_2*this->mn2*this->q[1]*this->q[3]*this->kg[2] - 
        std::complex<double>(0,2)*C5a*this->mRes*pow(this->mn,3)*this->q[1]*this->q[3]*this->kg[2] - 
        std::complex<double>(0,4)*C5a*this->mn2*p0*this->q[0]*this->q[1]*this->q[3]*this->kg[2] + 
        std::complex<double>(0,2)*C5a*this->mn2*this->q[0] * this->q[0]*this->q[1]*this->q[3]*this->kg[2] - 
        std::complex<double>(0,6)*C5a*this->mRes_2*this->mn2*this->q[3]*this->kg[1]*this->kg[2] - 
        std::complex<double>(0,2)*C5a*this->mRes*pow(this->mn,3)*this->q[3]*this->kg[1]*this->kg[2] - 
        std::complex<double>(0,4)*C5a*this->mn2*p0*this->q[0]*this->q[3]*this->kg[1]*this->kg[2] + 
        std::complex<double>(0,2)*C5a*this->mn2*this->q[0] * this->q[0]*this->q[3]*this->kg[1]*this->kg[2] - 
        std::complex<double>(0,2)*C5a*this->mn2*this->q[1] * this->q[1]*this->q[3]*this->kg[1]*this->kg[2] - 
        std::complex<double>(0,2)*C5a*this->mn2*this->q[1]*this->q[3]*this->kg[1] * this->kg[1]*this->kg[2] - 
        6*C5vNC*this->mRes_2*p0*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] - 
        2*C5vNC*this->mRes*this->mn*p0*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] + 
        2*C5vNC*this->mRes_2*this->q[0]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] + 
        2*C5vNC*this->mRes*this->mn*this->q[0]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] - 
        4*C5vNC*p0*this->q[1]*this->q[3] * this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] + 
        4*C5vNC*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] + 
        this->q[3]*(C5vNC*(4*pow(this->mRes,3)*this->mn*this->q[0] + 
              this->q[0]*this->q[1] * this->q[1]*
               (-pow(-2*p0 + this->q[0],2) + this->q[1] * this->q[1] + this->q[3] * this->q[3]) + 
              this->q[0]*this->q[1]*(12*pow(p0,2) - 20*p0*this->q[0] + 7*this->q[0] * this->q[0] + 
                 this->q[1] * this->q[1] + this->q[3] * this->q[3])*this->kg[1] + 
              4*(p0 - this->q[0])*(this->q[0] * this->q[0] + this->q[1] * this->q[1] - this->q[3] * this->q[3])*
               this->kg[1] * this->kg[1] + 
              this->mRes_2*(20*pow(p0,2)*this->q[0] - 
                 2*p0*(5*this->q[0] * this->q[0] + 
                    3*(this->q[1] * this->q[1] + this->q[3] * this->q[3]) - 2*this->q[1]*this->kg[1]) + 
                 this->q[0]*(this->q[0] * this->q[0] + 3*(this->q[1] * this->q[1] + this->q[3] * this->q[3]) - 
                    6*this->q[1]*this->kg[1] + 2*this->kg[1] * this->kg[1])) + 
              2*this->mRes*this->mn*(4*pow(p0,2)*this->q[0] - 
                 p0*(5*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3]) + 
                 this->q[0]*(this->q[0] * this->q[0] + this->q[3] * this->q[3] + this->q[1]*(2*this->q[1] + this->kg[1])))) - 
           std::complex<double>(0,2)*C5a*this->mn2*this->q[3]*(this->q[1] + this->kg[1])*this->kg[2])*this->kg[3] + 
        2*C3vNC*this->mn*(2*pow(this->mRes,3)*
            (this->q[0]*(4*p0*this->q[0] + this->q[0] * this->q[0] + this->q[3] * this->q[3] - this->kg[1] * this->kg[1]) - 
              2*(2*p0 + this->q[0])*this->q[3]*this->kg[3]) + 
           2*this->mRes_2*this->mn*
            (pow(this->q[0],3) + 4*p0*this->q[3]*this->kg[3] + 
              this->q[0]*(this->q[3] * this->q[3] - this->kg[1] * this->kg[1] - 2*this->q[3]*this->kg[3])) + 
           this->mRes*(8*p0*pow(this->q[0],4) - 3*pow(this->q[0],5) + 
              this->q[0]*(this->q[1] * this->q[1] + this->q[3] * this->q[3])*
               (this->q[3] * this->q[3] + this->kg[1] * this->kg[1]) - 
              4*p0*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] + 
              4*this->q[0]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] - 
              pow(this->q[0],3)*(this->q[1] * this->q[1] + 6*this->q[3] * this->q[3] - 
                 3*this->kg[1] * this->kg[1] - 8*this->q[3]*this->kg[3]) + 
              8*p0*this->q[0] * this->q[0]*
               (this->q[3] * this->q[3] - this->kg[1] * this->kg[1] - 2*this->q[3]*this->kg[3]) - 
              4*pow(p0,2)*this->q[0]*
               (this->q[0] * this->q[0] + this->q[3] * this->q[3] - this->kg[1] * this->kg[1] - 2*this->q[3]*this->kg[3])) + 
           2*this->mn*(p0 - this->q[0])*(4*pow(p0,2)*this->q[0] * this->q[0] + 
              2*pow(this->q[0],4) + 
              2*p0*this->q[0]*(-3*this->q[0] * this->q[0] + this->q[3] * this->q[3] - this->kg[1] * this->kg[1] + 
                 2*this->q[3]*this->kg[3]) - this->q[0] * this->q[0]*
               (this->q[1] * this->q[1] + 2*this->q[1]*this->kg[1] - this->kg[1] * this->kg[1] + 3*this->q[3]*this->kg[3]) + 
              this->q[3]*(-(this->q[3]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])) + 
                 (this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1])*this->kg[3]))) + 
        C4vNC*this->mRes*(4*this->mRes_2*this->mn*this->q[0]*
            (2*(p0 - this->q[0])*this->q[0] + this->q[3]*(this->q[3] + this->kg[3])) + 
           2*this->mn*(p0 - this->q[0])*(4*pow(p0,2)*this->q[0] * this->q[0] + 
              2*pow(this->q[0],4) + 
              2*p0*this->q[0]*(-3*this->q[0] * this->q[0] + this->q[3] * this->q[3] - this->kg[1] * this->kg[1] + 
                 2*this->q[3]*this->kg[3]) - this->q[0] * this->q[0]*
               (this->q[1] * this->q[1] + 2*this->q[1]*this->kg[1] - this->kg[1] * this->kg[1] + 3*this->q[3]*this->kg[3]) + 
              this->q[3]*(-(this->q[3]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])) + 
                 (this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1])*this->kg[3])) + 
           this->mRes*(16*pow(p0,3)*this->q[0] * this->q[0] - 
              4*pow(p0,2)*this->q[0]*
               (7*this->q[0] * this->q[0] - 2*this->q[3] * this->q[3] + this->kg[1] * this->kg[1] - 5*this->q[3]*this->kg[3]) + 
              2*p0*(7*pow(this->q[0],4) - 
                 this->q[0] * this->q[0]*(2*this->q[1] * this->q[1] - this->q[3] * this->q[3] + 4*this->q[1]*this->kg[1] + 
                    this->kg[1] * this->kg[1] + 11*this->q[3]*this->kg[3]) + 
                 this->q[3]*(-(this->q[3]*(4*this->kg[1] * this->kg[1] + 3*this->kg[2] * this->kg[2])) + 
                    3*this->q[3] * this->q[3]*this->kg[3] + this->q[1]*(3*this->q[1] + 4*this->kg[1])*this->kg[3])) + 
              this->q[0]*(-2*pow(this->q[0],4) + 
                 2*this->q[1]*this->kg[1]*(-this->q[3] * this->q[3] + this->kg[1] * this->kg[1] + 2*this->q[3]*this->kg[3]) + 
                 this->q[0] * this->q[0]*(-2*this->q[1] * this->q[1] - 3*this->q[3] * this->q[3] - 2*this->q[1]*this->kg[1] + 
                    2*this->kg[1] * this->kg[1] + 5*this->q[3]*this->kg[3]) + 
                 this->q[3]*(pow(this->q[3],3) - 2*this->q[3]*this->kg[1] * this->kg[1] + 
                    2*this->q[3]*this->kg[2] * this->kg[2] - this->q[3] * this->q[3]*this->kg[3] + 
                    2*this->kg[1] * this->kg[1]*this->kg[3]) + 
                 this->q[1] * this->q[1]*(2*this->kg[1] * this->kg[1] + this->q[3]*(this->q[3] + this->kg[3])))))) - 
     2*C3vNC*this->mn*(C4v*this->mRes*(-4*this->mRes_2*this->mn*this->q[0]*
            (-2*p0*this->q[0] + this->q[0] * this->q[0] + this->kg[1] * this->kg[1] - this->q[3]*this->kg[3]) + 
           2*this->mn*(p0 - this->q[0])*(4*pow(p0,2)*this->q[0] * this->q[0] + 
              2*pow(this->q[0],4) + 
              2*p0*this->q[0]*(-3*this->q[0] * this->q[0] + this->q[3] * this->q[3] - this->kg[1] * this->kg[1] + 
                 2*this->q[3]*this->kg[3]) - this->q[0] * this->q[0]*
               (this->q[1] * this->q[1] + 2*this->q[1]*this->kg[1] - this->kg[1] * this->kg[1] + 3*this->q[3]*this->kg[3]) + 
              this->q[3]*(-(this->q[3]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])) + 
                 (this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1])*this->kg[3])) + 
           this->mRes*(16*pow(p0,3)*this->q[0] * this->q[0] - 
              this->q[0]*(pow(this->q[0],4) - 3*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] + 
                 2*this->q[1]*(this->q[3] - this->kg[1])*this->kg[1]*(this->q[3] + this->kg[1]) + 
                 this->q[0] * this->q[0]*(3*this->q[3] * this->q[3] + (3*this->q[1] - this->kg[1])*(this->q[1] + this->kg[1])) + 
                 this->q[3] * this->q[3]*(this->kg[1] * this->kg[1] - 2*this->kg[2] * this->kg[2])) + 
              this->q[0]*this->q[3]*(3*this->q[0] * this->q[0] + 3*this->q[1] * this->q[1] + this->q[3] * this->q[3] + 
                 4*this->q[1]*this->kg[1] + 2*this->kg[1] * this->kg[1])*this->kg[3] + 
              4*pow(p0,2)*this->q[0]*
               (-6*this->q[0] * this->q[0] + this->q[3] * this->q[3] - 2*this->kg[1] * this->kg[1] + 5*this->q[3]*this->kg[3])  
+ 2*p0*(5*pow(this->q[0],4) + this->q[0] * this->q[0]*
                  (-2*this->q[1] * this->q[1] - 4*this->q[1]*this->kg[1] + this->kg[1] * this->kg[1] + 
                    3*this->q[3]*(this->q[3] - 3*this->kg[3])) + 
                 this->q[3]*(-(this->q[3]*(4*this->kg[1] * this->kg[1] + 3*this->kg[2] * this->kg[2])) + 
                    this->q[3] * this->q[3]*this->kg[3] + this->q[1]*(this->q[1] + 4*this->kg[1])*this->kg[3])))) + 
        C5v*(8*pow(p0,3)*this->q[0] * this->q[0]*(this->q[1] - this->kg[1])*this->kg[1] + 
           4*pow(this->mRes,3)*this->mn*this->q[0]*
            (2*p0*this->q[0] - this->kg[2] * this->kg[2] + (this->q[3] - this->kg[3])*this->kg[3]) - 
           4*pow(p0,2)*this->q[0]*this->kg[1]*
            (5*this->q[0] * this->q[0]*(this->q[1] - this->kg[1]) - this->q[1] * this->q[1]*this->kg[1] + 
              this->q[3]*this->kg[1]*(2*this->q[3] + this->kg[3]) + this->q[1]*(this->kg[1] * this->kg[1] - 3*this->q[3]*this->kg[3])) + 
           this->q[0]*this->kg[1]*(-(pow(this->q[1],4)*this->kg[1]) + pow(this->q[0],4)*(-5*this->q[1] + 3*this->kg[1]) + 
              3*pow(this->q[3],3)*this->kg[1]*this->kg[3] - this->q[1] * this->q[1]*this->q[3]*this->kg[1]*(this->q[3] + 5*this->kg[3]) + 
              this->q[1]*this->q[3] * this->q[3]*(7*this->kg[1] * this->kg[1] + 4*this->kg[2] * this->kg[2] - this->q[3]*this->kg[3]) + 
              pow(this->q[1],3)*(this->kg[2] * this->kg[2] - this->q[3]*this->kg[3] + this->kg[3] * this->kg[3]) + 
              this->q[0] * this->q[0]*(6*this->q[1] * this->q[1]*this->kg[1] - 3*this->q[3]*this->kg[1]*(this->q[3] + this->kg[3]) - 
                 3*this->q[1]*(this->q[3] * this->q[3] + this->kg[1] * this->kg[1] - 3*this->q[3]*this->kg[3]))) + 
           this->mRes_2*(16*pow(p0,3)*this->q[0] * this->q[0] - 
              this->q[0]*(pow(this->q[0],4) + 
                 this->kg[1]*(2*this->q[1]*this->q[3] * this->q[3] + this->q[1] * this->q[1]*this->kg[1] - 
                    5*this->q[3] * this->q[3]*this->kg[1]) + 
                 this->q[0] * this->q[0]*(3*(this->q[1] * this->q[1] + this->q[3] * this->q[3]) - 8*this->q[1]*this->kg[1] + 
                    3*this->kg[1] * this->kg[1]) - 2*this->q[3] * this->q[3]*this->kg[2] * this->kg[2]) + 
              this->q[0]*this->q[3]*(3*this->q[0] * this->q[0] + this->q[3] * this->q[3] + 3*this->q[1]*(this->q[1] - 2*this->kg[1]))*
               this->kg[3] + 4*pow(p0,2)*this->q[0]*
               (-6*this->q[0] * this->q[0] + this->q[3] * this->q[3] + 2*this->kg[1] * this->kg[1] + 5*this->q[3]*this->kg[3])  
+ 2*p0*(5*pow(this->q[0],4) + this->q[0] * this->q[0]*
                  (-2*this->q[1] * this->q[1] - 4*this->q[1]*this->kg[1] + this->kg[1] * this->kg[1] + 
                    3*this->q[3]*(this->q[3] - 3*this->kg[3])) + 
                 this->q[3]*(-(this->q[3]*(2*this->kg[1] * this->kg[1] + 3*this->kg[2] * this->kg[2])) + 
                    (this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1])*this->kg[3]))) + 
           2*this->mRes*this->mn*(4*pow(p0,3)*this->q[0] * this->q[0] + 
              2*pow(p0,2)*this->q[0]*
               (-5*this->q[0] * this->q[0] + this->q[3] * this->q[3] + this->kg[1] * this->kg[1] + 2*this->q[3]*this->kg[3]) + 
              this->q[0]*(-2*pow(this->q[0],4) + this->q[3] * this->q[3]*this->kg[2] * this->kg[2] - 
                 this->q[1]*(this->q[1] + this->kg[1])*this->kg[2] * this->kg[2] + 
                 this->q[3]*(-this->q[1] * this->q[1] - this->q[3] * this->q[3] + this->q[1]*this->kg[1] + this->kg[1] * this->kg[1])*
                  this->kg[3] - this->q[1]*(this->q[1] + this->kg[1])*this->kg[3] * this->kg[3] + 
                 this->q[0] * this->q[0]*(2*this->q[1] * this->q[1] + 3*this->q[3]*this->kg[3])) + 
              p0*(8*pow(this->q[0],4) + 
                 this->q[3]*(-(this->q[3]*this->kg[2] * this->kg[2]) + 
                    (this->q[1] * this->q[1] + this->q[3] * this->q[3])*this->kg[3]) - 
                 this->q[0] * this->q[0]*(pow(this->q[1] - this->kg[1],2) + this->q[3]*(2*this->q[3] + 7*this->kg[3])))) + 
           2*p0*(pow(this->q[0],4)*
               (-4*this->q[1] * this->q[1] + 9*this->q[1]*this->kg[1] - 7*this->kg[1] * this->kg[1]) - 
              2*this->q[3]*this->kg[1]*(this->q[1]*this->q[3]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) - 
                 this->q[1] * this->q[1]*this->kg[1]*this->kg[3] + this->q[3] * this->q[3]*this->kg[1]*this->kg[3]) + 
              this->q[0] * this->q[0]*(-(pow(this->q[1],3)*this->kg[1]) + 
                 this->q[3]*this->kg[1] * this->kg[1]*(5*this->q[3] + 4*this->kg[3]) + 
                 this->q[1]*this->kg[1]*(this->q[3] * this->q[3] + 4*this->kg[1] * this->kg[1] - 10*this->q[3]*this->kg[3]) + 
                 this->q[1] * this->q[1]*(-this->kg[1] * this->kg[1] + 
                    4*(this->kg[2] * this->kg[2] + this->kg[3] * this->kg[3])))))))/
  (24.*this->mRes_2*pow(this->mn,5));
    
	tr[1][2] = (std::complex<double>(0,-2)*C5a*this->mn2*
     (2*C3v*this->mn*(2*this->mRes*this->q[3]*(-(this->q[0]*(this->mn*p0 + this->mRes*(-3*p0 + this->q[0]))) + 
             this->mn*this->kg[1] * this->kg[1]) + 
          this->q[3]*(3*this->mRes*(this->mRes + this->mn) + 2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1]*this->kg[1])*
           this->kg[2] * this->kg[2] + (-8*pow(this->mRes,3)*this->mn + 
             this->mRes_2*(8*pow(p0,2) - 6*p0*this->q[0] + this->q[0] * this->q[0] + 
                this->q[1] * this->q[1] + this->q[3] * this->q[3]) - 
             this->mRes*this->mn*(2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1] * this->q[1] + 
                this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1]) + this->q[3] * this->q[3]*this->kg[2] * this->kg[2])*this->kg[3]) + 
       C5v*this->mRes*(2*this->q[3]*(2*this->mRes*this->mn*p0*this->q[0] + 
             2*this->mRes_2*((p0 - this->q[0])*this->q[0] + this->kg[1] * this->kg[1]) + 
             this->kg[1]*(2*p0*this->q[0]*this->kg[1] + 2*this->q[1]*this->kg[1] * this->kg[1] - 
                this->q[0] * this->q[0]*(this->q[1] + this->kg[1]))) + 
          this->q[3]*(2*this->mRes*(this->mRes - this->mn) + 2*p0*this->q[0] - this->q[0] * this->q[0] + 3*this->q[1]*this->kg[1])*
           this->kg[2] * this->kg[2] + (2*this->mRes*this->mn*
              (2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3]) + 
             2*this->mRes_2*(-2*p0*this->q[0] + this->q[0] * this->q[0] + this->q[1] * this->q[1] + 
                this->q[3] * this->q[3] - 2*this->q[1]*this->kg[1]) + 
             2*this->kg[1]*(-2*p0*this->q[0]*this->q[1] + this->q[0] * this->q[0]*this->q[1] - this->q[1] * this->q[1]*this->kg[1] + 
                this->q[3] * this->q[3]*this->kg[1]) + this->q[3] * this->q[3]*this->kg[2] * this->kg[2])*this->kg[3])) + 
    C4v*this->mRes*(std::complex<double>(0,-2)*C5a*this->mn2*
        (this->q[3]*this->kg[2] * this->kg[2]*(2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) + 
          2*this->mRes_2*(this->q[3]*(2*(p0 - this->q[0])*this->q[0] + this->kg[2] * this->kg[2]) + 
             (-2*p0*this->q[0] + this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3])*this->kg[3])  
+ 2*this->mRes*this->mn*(-(this->q[3]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])) + 
             (-this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1])*this->kg[3] + 
             2*p0*this->q[0]*(this->q[3] + this->kg[3]))) - 
       this->kg[2]*(4*C4vNC*this->mRes*(2*this->mn*(p0 - this->q[0])*
              (pow(this->q[1],3) + this->q[1]*this->q[3] * this->q[3] + 2*this->q[1] * this->q[1]*this->kg[1] + 
                this->q[3] * this->q[3]*this->kg[1] + 2*p0*this->q[0]*(2*this->q[1] + this->kg[1]) - 
                this->q[0] * this->q[0]*(2*this->q[1] + this->kg[1]) + this->q[1]*this->q[3]*this->kg[3]) + 
             this->mRes*(4*pow(p0,2)*this->q[0]*(2*this->q[1] + this->kg[1]) + 
                this->q[0]*(-(this->q[0] * this->q[0]*this->q[1]) + pow(this->q[1],3) + this->q[1] * this->q[1]*this->kg[1] + 
                   this->q[1]*(this->q[3] - this->kg[1])*(this->q[3] + this->kg[1]) + this->q[3]*this->kg[1]*(this->q[3] - this->kg[3])) + 
                2*p0*(pow(this->q[1],3) + 2*this->q[1] * this->q[1]*this->kg[1] + 
                   this->q[3] * this->q[3]*this->kg[1] - this->q[0] * this->q[0]*(this->q[1] + this->kg[1]) + 
                   this->q[1]*this->q[3]*(this->q[3] + this->kg[3])))) + 
          2*C3vNC*this->mn*(4*this->mRes_2*this->mn*this->q[0]*(this->q[1] + this->kg[1]) + 
             2*this->mn*(p0 - this->q[0])*
              (pow(this->q[1],3) + this->q[1]*this->q[3] * this->q[3] + 2*this->q[1] * this->q[1]*this->kg[1] + 
                this->q[3] * this->q[3]*this->kg[1] + 2*p0*this->q[0]*(2*this->q[1] + this->kg[1]) - 
                this->q[0] * this->q[0]*(2*this->q[1] + this->kg[1]) + this->q[1]*this->q[3]*this->kg[3]) + 
             this->mRes*(4*pow(p0,2)*this->q[0]*(5*this->q[1] + 2*this->kg[1]) - 
                this->q[0]*(this->q[1] + this->kg[1])*
                 (this->q[0] * this->q[0] - 3*this->q[1] * this->q[1] + 2*this->q[1]*this->kg[1] + 
                   this->q[3]*(-3*this->q[3] + 2*this->kg[3])) + 
                2*p0*(pow(this->q[1],3) + 4*this->q[1] * this->q[1]*this->kg[1] + 
                   this->q[3] * this->q[3]*this->kg[1] - this->q[0] * this->q[0]*(4*this->q[1] + this->kg[1]) + 
                   this->q[1]*this->q[3]*(this->q[3] + 3*this->kg[3])))) + 
          C5vNC*(8*this->mRes*this->mn*(p0 - this->q[0])*
              (-pow(this->q[1],3) + (this->q[0] - this->q[3])*(this->q[0] + this->q[3])*this->kg[1] + 
                2*p0*this->q[0]*(2*this->q[1] + this->kg[1]) + this->q[1]*this->q[3]*(-this->q[3] + this->kg[3])) + 
             4*this->mRes_2*(4*pow(p0,2)*this->q[0]*(2*this->q[1] + this->kg[1]) + 
                this->q[0]*this->kg[1]*(this->q[0] * this->q[0] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3]) + 
                2*p0*(-pow(this->q[1],3) - this->q[3] * this->q[3]*this->kg[1] + 
                   this->q[0] * this->q[0]*(this->q[1] + this->kg[1]) + this->q[1]*this->q[3]*(-this->q[3] + this->kg[3]))) + 
             (this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3])*
              (4*pow(p0,2)*this->q[0]*(this->q[1] - this->kg[1]) + 
                this->q[0]*(-pow(this->q[1],3) - this->q[1]*this->q[3] * this->q[3] + 
                   this->q[0] * this->q[0]*(this->q[1] - 3*this->kg[1]) - this->q[1] * this->q[1]*this->kg[1] - 
                   this->q[3] * this->q[3]*this->kg[1] + 4*this->q[1]*this->kg[1] * this->kg[1] + 4*this->q[3]*this->kg[1]*this->kg[3]) - 
                4*p0*(this->q[0] * this->q[0]*(this->q[1] - 2*this->kg[1]) + this->kg[1]*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3])))) 
)) + this->kg[2]*(-2*C3v*this->mn*(C4vNC*this->mRes*
           (4*this->mRes_2*this->mn*this->q[0]*this->q[1] + 
             2*this->mn*(p0 - this->q[0])*
              (pow(this->q[1],3) + this->q[1]*this->q[3] * this->q[3] + 2*this->q[1] * this->q[1]*this->kg[1] + 
                this->q[3] * this->q[3]*this->kg[1] + 2*p0*this->q[0]*(2*this->q[1] + this->kg[1]) - 
                this->q[0] * this->q[0]*(2*this->q[1] + this->kg[1]) + this->q[1]*this->q[3]*this->kg[3]) + 
             this->mRes*(4*pow(p0,2)*this->q[0]*(5*this->q[1] + this->kg[1]) + 
                this->q[0]*(pow(this->q[1],3) + this->q[1]*this->q[3] * this->q[3] + 
                   this->q[0] * this->q[0]*(this->q[1] - 2*this->kg[1]) + 2*this->q[1] * this->q[1]*this->kg[1] + 
                   4*this->q[3] * this->q[3]*this->kg[1] - 2*this->q[1]*this->kg[1] * this->kg[1] - 
                   2*this->q[3]*(this->q[1] + this->kg[1])*this->kg[3]) + 
                2*p0*(3*pow(this->q[1],3) + 4*this->q[1] * this->q[1]*this->kg[1] + 
                   this->q[3] * this->q[3]*this->kg[1] + this->q[0] * this->q[0]*(-6*this->q[1] + this->kg[1]) + 
                   3*this->q[1]*this->q[3]*(this->q[3] + this->kg[3])))) - 
          2*C3vNC*this->mn*(pow(this->mRes,3)*(8*p0*this->q[1] + 4*this->q[0]*this->q[1] - 2*this->q[0]*this->kg[1]) - 
             2*this->mRes_2*this->mn*(4*p0*this->q[1] + this->q[0]*(-2*this->q[1] + this->kg[1])) - 
             2*this->mn*(p0 - this->q[0])*
              (pow(this->q[1],3) + this->q[1]*this->q[3] * this->q[3] + 2*this->q[1] * this->q[1]*this->kg[1] + 
                this->q[3] * this->q[3]*this->kg[1] + 2*p0*this->q[0]*(2*this->q[1] + this->kg[1]) - 
                this->q[0] * this->q[0]*(2*this->q[1] + this->kg[1]) + this->q[1]*this->q[3]*this->kg[3]) + 
             this->mRes*(4*pow(p0,2)*this->q[0]*(-2*this->q[1] + this->kg[1]) + 
                this->q[0]*(this->q[1] * this->q[1]*this->kg[1] - 3*this->q[3] * this->q[3]*this->kg[1] + 
                   this->q[0] * this->q[0]*(-4*this->q[1] + 3*this->kg[1]) + 4*this->q[1]*this->q[3]*this->kg[3]) + 
                4*p0*(this->q[0] * this->q[0]*(3*this->q[1] - 2*this->kg[1]) + this->q[3]*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3])) 
)) + C5vNC*(4*pow(this->mRes,3)*this->mn*this->q[0]*this->q[1] + 
             2*this->mRes*this->mn*(2*pow(p0,2)*this->q[0]*(2*this->q[1] + this->kg[1]) - 
                p0*(pow(this->q[1],3) + this->q[3] * this->q[3]*this->kg[1] + 
                   this->q[0] * this->q[0]*(4*this->q[1] + this->kg[1]) + this->q[1]*this->q[3]*(this->q[3] - this->kg[3])) + 
                this->q[0]*(2*pow(this->q[1],3) + this->q[1] * this->q[1]*this->kg[1] + 2*this->q[3] * this->q[3]*this->kg[1] - 
                   this->q[0] * this->q[0]*(this->q[1] + 2*this->kg[1]) + this->q[1]*this->q[3]*(2*this->q[3] - this->kg[3]))) + 
             (this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3])*
              (4*pow(p0,2)*this->q[0]*(this->q[1] - this->kg[1]) + 
                this->q[0]*(-pow(this->q[1],3) - this->q[1]*this->q[3] * this->q[3] + 
                   this->q[0] * this->q[0]*(this->q[1] - 3*this->kg[1]) - this->q[1] * this->q[1]*this->kg[1] - 
                   this->q[3] * this->q[3]*this->kg[1] + 4*this->q[1]*this->kg[1] * this->kg[1] + 4*this->q[3]*this->kg[1]*this->kg[3]) - 
                4*p0*(this->q[0] * this->q[0]*(this->q[1] - 2*this->kg[1]) + this->kg[1]*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3])))  
+ this->mRes_2*(4*pow(p0,2)*this->q[0]*(5*this->q[1] + this->kg[1]) + 
                p0*(-6*pow(this->q[1],3) + 6*this->q[0] * this->q[0]*this->kg[1] + 
                   4*this->q[1] * this->q[1]*this->kg[1] - 2*this->q[3] * this->q[3]*this->kg[1] + 
                   6*this->q[1]*this->q[3]*(-this->q[3] + this->kg[3])) - 
                this->q[0]*(-3*pow(this->q[1],3) + this->q[0] * this->q[0]*(this->q[1] - 4*this->kg[1]) + 
                   4*this->q[1] * this->q[1]*this->kg[1] + 2*this->q[3]*this->kg[1]*(this->q[3] + this->kg[3]) + 
                   this->q[1]*(-3*this->q[3] * this->q[3] + 2*this->kg[1] * this->kg[1] + 2*this->q[3]*this->kg[3]))))) - 
       C5v*(C4vNC*this->mRes*((4*pow(p0,2)*this->q[0] + 
                3*this->q[0]*(this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3]) + 
                4*p0*(-2*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3]))*this->kg[1]*
              (2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) + 
             8*this->mRes*this->mn*(p0 - this->q[0])*
              (pow(this->q[1],3) + this->q[1]*this->q[3] * this->q[3] + p0*this->q[0]*(4*this->q[1] - 2*this->kg[1]) - 
                this->q[3] * this->q[3]*this->kg[1] + this->q[0] * this->q[0]*(-2*this->q[1] + this->kg[1]) + this->q[1]*this->q[3]*this->kg[3]) + 
             4*this->mRes_2*(pow(p0,2)*this->q[0]*(8*this->q[1] - 4*this->kg[1]) - 
                this->q[0]*(this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3])*(this->q[1] - this->kg[1]) + 
                2*p0*(-(this->q[0] * this->q[0]*this->q[1]) + pow(this->q[1],3) - 
                   this->q[3] * this->q[3]*this->kg[1] + this->q[1]*this->q[3]*(this->q[3] + this->kg[3])))) + 
          2*C3vNC*this->mn*(4*pow(this->mRes,3)*this->mn*this->q[0]*(this->q[1] - this->kg[1]) + 
             (4*pow(p0,2)*this->q[0] + 
                3*this->q[0]*(this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3]) + 
                4*p0*(-2*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3]))*this->kg[1]*
              (2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) + 
             this->mRes_2*(4*pow(p0,2)*this->q[0]*(5*this->q[1] - 2*this->kg[1]) - 
                this->q[0]*(-3*pow(this->q[1],3) + this->q[0] * this->q[0]*(this->q[1] - 3*this->kg[1]) + 
                   5*this->q[1] * this->q[1]*this->kg[1] + 3*this->q[3] * this->q[3]*this->kg[1] + 
                   this->q[1]*this->q[3]*(-3*this->q[3] + 2*this->kg[3])) + 
                2*p0*(pow(this->q[1],3) + 2*this->q[1] * this->q[1]*this->kg[1] - 
                   this->q[3] * this->q[3]*this->kg[1] - this->q[0] * this->q[0]*(4*this->q[1] + this->kg[1]) + 
                   this->q[1]*this->q[3]*(this->q[3] + 3*this->kg[3]))) + 
             2*this->mRes*this->mn*(pow(p0,2)*this->q[0]*(4*this->q[1] - 2*this->kg[1]) + 
                p0*(pow(this->q[1],3) - this->q[3] * this->q[3]*this->kg[1] + 
                   this->q[0] * this->q[0]*(-6*this->q[1] + this->kg[1]) + this->q[1]*this->q[3]*(this->q[3] + this->kg[3])) + 
                this->q[0]*(2*this->q[0] * this->q[0]*this->q[1] - pow(this->q[1],3) + this->q[3]*this->kg[1]*(this->q[3] - this->kg[3]) - 
                   this->q[1]*(this->kg[1] * this->kg[1] + this->q[3]*(this->q[3] + this->kg[3]))))) + 
          C5vNC*(-8*this->mn*(p0 - this->q[0])*
              (this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3])*this->kg[1]*
              (2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) + 
             8*pow(this->mRes,3)*p0*
              (-pow(this->q[1],3) - this->q[1]*this->q[3] * this->q[3] + this->q[0] * this->q[0]*(this->q[1] - 2*this->kg[1]) + 
                p0*this->q[0]*(4*this->q[1] - 2*this->kg[1]) + 2*this->q[1] * this->q[1]*this->kg[1] + 
                this->q[3] * this->q[3]*this->kg[1] + this->q[1]*this->q[3]*this->kg[3]) + 
             8*this->mRes_2*this->mn*(p0 - this->q[0])*
              (-pow(this->q[1],3) + p0*this->q[0]*(4*this->q[1] - 2*this->kg[1]) + 2*this->q[1] * this->q[1]*this->kg[1] + 
                (-this->q[0] * this->q[0] + this->q[3] * this->q[3])*this->kg[1] + this->q[1]*this->q[3]*(-this->q[3] + this->kg[3])) + 
             this->mRes*(8*pow(p0,3)*this->q[0] * this->q[0]*this->kg[1] + 
                this->q[0]*(this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3])*
                 (this->q[0] * this->q[0]*this->q[1] - pow(this->q[1],3) + this->q[1] * this->q[1]*this->kg[1] - 
                   this->q[1]*(this->q[3] * this->q[3] + this->kg[1] * this->kg[1]) + this->q[3]*this->kg[1]*(this->q[3] - this->kg[3])) + 
                4*pow(p0,2)*this->q[0]*
                 (-pow(this->q[1],3) + this->q[0] * this->q[0]*(this->q[1] - 8*this->kg[1]) + 
                   5*this->q[1] * this->q[1]*this->kg[1] + this->q[1]*(-this->q[3] * this->q[3] + this->kg[1] * this->kg[1]) + 
                   this->q[3]*this->kg[1]*(5*this->q[3] + this->kg[3])) - 
                2*p0*(pow(this->q[0],4)*(2*this->q[1] - 7*this->kg[1]) - 
                   4*(this->q[1] * this->q[1] + this->q[3] * this->q[3])*this->kg[1]*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) + 
                   this->q[0] * this->q[0]*
                    (-2*pow(this->q[1],3) + 5*this->q[1] * this->q[1]*this->kg[1] - 
                      2*this->q[1]*(this->q[3] * this->q[3] - 3*this->kg[1] * this->kg[1]) + 
                      this->q[3]*this->kg[1]*(5*this->q[3] + 6*this->kg[3]))))))))/
  (24.*this->mRes_2*pow(this->mn,5));
    
	tr[1][3] = -(2*C3v*this->mn*(-4*C5vNC*pow(this->mRes,3)*this->mn*this->q[0]*this->q[1]*this->q[3] - 
        8*C5vNC*this->mRes_2*pow(p0,2)*this->q[0]*this->q[1]*this->q[3] - 
        4*C5vNC*this->mRes*this->mn*pow(p0,2)*this->q[0]*this->q[1]*this->q[3] + 
        2*C5vNC*this->mRes_2*p0*this->q[0] * this->q[0]*this->q[1]*this->q[3] + 
        2*C5vNC*this->mRes*this->mn*p0*this->q[0] * this->q[0]*this->q[1]*this->q[3] + 
        8*C5vNC*pow(p0,3)*this->q[0] * this->q[0]*this->q[1]*this->q[3] - 
        C5vNC*this->mRes_2*pow(this->q[0],3)*this->q[1]*this->q[3] - 
        12*C5vNC*pow(p0,2)*pow(this->q[0],3)*this->q[1]*this->q[3] + 
        6*C5vNC*p0*pow(this->q[0],4)*this->q[1]*this->q[3] - C5vNC*pow(this->q[0],5)*this->q[1]*this->q[3] - 
        C5vNC*this->mRes_2*this->q[0]*pow(this->q[1],3)*this->q[3] - 
        2*C5vNC*p0*this->q[0] * this->q[0]*pow(this->q[1],3)*this->q[3] + 
        C5vNC*pow(this->q[0],3)*pow(this->q[1],3)*this->q[3] - 
        C5vNC*this->mRes_2*this->q[0]*this->q[1]*pow(this->q[3],3) - 
        2*C5vNC*p0*this->q[0] * this->q[0]*this->q[1]*pow(this->q[3],3) + 
        C5vNC*pow(this->q[0],3)*this->q[1]*pow(this->q[3],3) - 
        2*C5vNC*this->mRes_2*p0*this->q[0] * this->q[0]*this->q[3]*this->kg[1] - 
        6*C5vNC*this->mRes*this->mn*p0*this->q[0] * this->q[0]*this->q[3]*this->kg[1] - 
        8*C5vNC*pow(p0,3)*this->q[0] * this->q[0]*this->q[3]*this->kg[1] - 
        6*C5vNC*this->mRes_2*pow(this->q[0],3)*this->q[3]*this->kg[1] + 
        4*C5vNC*this->mRes*this->mn*pow(this->q[0],3)*this->q[3]*this->kg[1] + 
        20*C5vNC*pow(p0,2)*pow(this->q[0],3)*this->q[3]*this->kg[1] - 
        18*C5vNC*p0*pow(this->q[0],4)*this->q[3]*this->kg[1] + 7*C5vNC*pow(this->q[0],5)*this->q[3]*this->kg[1] - 
        2*C5vNC*this->mRes*this->mn*this->q[0]*this->q[1] * this->q[1]*this->q[3]*this->kg[1] + 
        4*C5vNC*pow(p0,2)*this->q[0]*this->q[1] * this->q[1]*this->q[3]*this->kg[1] - 
        2*C5vNC*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3]*this->kg[1] - 
        2*C5vNC*pow(this->q[0],3)*this->q[1] * this->q[1]*this->q[3]*this->kg[1] - 
        C5vNC*this->q[0]*pow(this->q[1],4)*this->q[3]*this->kg[1] - 
        2*C5vNC*p0*this->q[0] * this->q[0]*pow(this->q[3],3)*this->kg[1] + 
        C5vNC*pow(this->q[0],3)*pow(this->q[3],3)*this->kg[1] - 
        C5vNC*this->q[0]*this->q[1] * this->q[1]*pow(this->q[3],3)*this->kg[1] - 
        4*C5vNC*this->mRes_2*p0*this->q[1]*this->q[3]*this->kg[1] * this->kg[1] + 
        6*C5vNC*this->mRes_2*this->q[0]*this->q[1]*this->q[3]*this->kg[1] * this->kg[1] - 
        2*C5vNC*this->mRes*this->mn*this->q[0]*this->q[1]*this->q[3]*this->kg[1] * this->kg[1] - 
        12*C5vNC*pow(p0,2)*this->q[0]*this->q[1]*this->q[3]*this->kg[1] * this->kg[1] + 
        20*C5vNC*p0*this->q[0] * this->q[0]*this->q[1]*this->q[3]*this->kg[1] * this->kg[1] - 
        7*C5vNC*pow(this->q[0],3)*this->q[1]*this->q[3]*this->kg[1] * this->kg[1] - 
        C5vNC*this->q[0]*pow(this->q[1],3)*this->q[3]*this->kg[1] * this->kg[1] - 
        C5vNC*this->q[0]*this->q[1]*pow(this->q[3],3)*this->kg[1] * this->kg[1] + 
        2*C5vNC*this->mRes_2*this->q[0]*this->q[3]*pow(this->kg[1],3) + 
        4*C5vNC*p0*this->q[0] * this->q[0]*this->q[3]*pow(this->kg[1],3) - 
        4*C5vNC*pow(this->q[0],3)*this->q[3]*pow(this->kg[1],3) - 
        8*C5vNC*p0*this->q[1] * this->q[1]*this->q[3]*pow(this->kg[1],3) + 
        8*C5vNC*this->q[0]*this->q[1] * this->q[1]*this->q[3]*pow(this->kg[1],3) + 
        std::complex<double>(0,16)*C5a*pow(this->mRes,3)*pow(this->mn,3)*this->kg[2] - 
        std::complex<double>(0,16)*C5a*this->mRes_2*this->mn2*pow(p0,2)*this->kg[2] + 
        std::complex<double>(0,12)*C5a*this->mRes_2*this->mn2*p0*this->q[0]*this->kg[2] + 
        std::complex<double>(0,4)*C5a*this->mRes*pow(this->mn,3)*p0*this->q[0]*this->kg[2] - 
        std::complex<double>(0,2)*C5a*this->mRes_2*this->mn2*this->q[0] * this->q[0]*this->kg[2] - 
        std::complex<double>(0,2)*C5a*this->mRes*pow(this->mn,3)*this->q[0] * this->q[0]*this->kg[2] - 
        std::complex<double>(0,2)*C5a*this->mRes_2*this->mn2*this->q[1] * this->q[1]*this->kg[2] + 
        std::complex<double>(0,2)*C5a*this->mRes*pow(this->mn,3)*this->q[1] * this->q[1]*this->kg[2] + 
        std::complex<double>(0,4)*C5a*this->mRes*pow(this->mn,3)*this->q[3] * this->q[3]*this->kg[2] + 
        std::complex<double>(0,4)*C5a*this->mn2*p0*this->q[0]*this->q[3] * this->q[3]*this->kg[2] + 
        std::complex<double>(0,4)*C5a*this->mRes*pow(this->mn,3)*this->q[1]*this->kg[1]*this->kg[2] + 
        std::complex<double>(0,2)*C5a*this->mn2*this->q[1]*this->q[3] * this->q[3]*this->kg[1]*this->kg[2] - 
        std::complex<double>(0,2)*C5a*this->mn2*this->q[3] * this->q[3]*this->kg[1] * this->kg[1]*this->kg[2] - 
        6*C5vNC*this->mRes_2*p0*this->q[1]*this->q[3]*this->kg[2] * this->kg[2] - 
        2*C5vNC*this->mRes*this->mn*p0*this->q[1]*this->q[3]*this->kg[2] * this->kg[2] + 
        2*C5vNC*this->mRes_2*this->q[0]*this->q[1]*this->q[3]*this->kg[2] * this->kg[2] + 
        2*C5vNC*this->mRes*this->mn*this->q[0]*this->q[1]*this->q[3]*this->kg[2] * this->kg[2] + 
        2*C5vNC*this->mRes_2*this->q[0]*this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] + 
        4*C5vNC*p0*this->q[0] * this->q[0]*this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] - 
        4*C5vNC*pow(this->q[0],3)*this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] - 
        4*C5vNC*p0*this->q[1] * this->q[1]*this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] + 
        4*C5vNC*this->q[0]*this->q[1] * this->q[1]*this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] - 
        std::complex<double>(0,2)*C5a*this->mn2*this->q[3] * this->q[3]*pow(this->kg[2],3) + 
        (C5vNC*(4*pow(this->mRes,3)*this->mn*this->q[0]*this->q[1] + 
              this->q[0]*this->q[1]*(-this->q[0] + this->q[1])*(this->q[0] + this->q[1])*
               (-pow(-2*p0 + this->q[0],2) + this->q[1] * this->q[1] + this->q[3] * this->q[3]) + 
              this->q[0]*(-((this->q[0] - this->q[1])*(this->q[0] + this->q[1])*
                    (4*pow(p0,2) - 8*p0*this->q[0] + 3*this->q[0] * this->q[0] + 
                      this->q[1] * this->q[1])) + 
                 (-8*pow(p0,2) + 12*p0*this->q[0] - 5*this->q[0] * this->q[0] + 
                    this->q[1] * this->q[1])*this->q[3] * this->q[3])*this->kg[1] - 
              4*(p0 - this->q[0])*this->q[1]*
               (this->q[0] * this->q[0] - this->q[1] * this->q[1] + this->q[3] * this->q[3])*this->kg[1] * this->kg[1] + 
              this->mRes_2*(4*pow(p0,2)*this->q[0]*(5*this->q[1] + this->kg[1]) + 
                 p0*(-6*this->q[1]*(this->q[1] * this->q[1] + this->q[3] * this->q[3]) + 
                    6*this->q[0] * this->q[0]*this->kg[1] + 4*this->q[1] * this->q[1]*this->kg[1]) + 
                 this->q[0]*(3*pow(this->q[1],3) + 3*this->q[1]*this->q[3] * this->q[3] - 
                    this->q[0] * this->q[0]*(this->q[1] - 4*this->kg[1]) - 4*this->q[1] * this->q[1]*this->kg[1] + 
                    2*this->q[3] * this->q[3]*this->kg[1] - 2*this->q[1]*this->kg[1] * this->kg[1])) + 
              2*this->mRes*this->mn*(2*pow(p0,2)*this->q[0]*(2*this->q[1] + this->kg[1]) - 
                 pow(this->q[0],3)*(this->q[1] + 2*this->kg[1]) + 
                 this->q[0]*this->q[1]*(this->q[3] * this->q[3] + this->q[1]*(2*this->q[1] + this->kg[1])) - 
                 p0*(this->q[1]*(this->q[1] * this->q[1] + this->q[3] * this->q[3]) + 
                    this->q[0] * this->q[0]*(4*this->q[1] + this->kg[1])))) + 
           std::complex<double>(0,2)*C5a*this->mn2*this->q[3]*
            (3*this->mRes*(this->mRes + this->mn) + 2*p0*this->q[0] - this->q[0] * this->q[0] + 
              this->q[3] * this->q[3] + this->q[1]*this->kg[1])*this->kg[2])*this->kg[3] + 
        2*C3vNC*this->mn*(this->mRes*this->q[3]*(this->q[0]*
               (-4*pow(p0,2)*this->q[1] + this->q[1]*(this->q[1] * this->q[1] + this->q[3] * this->q[3]) + 
                 p0*this->q[0]*(8*this->q[1] - 4*this->kg[1]) + this->q[0] * this->q[0]*(-5*this->q[1] + 4*this->kg[1])) + 
              4*(-p0 + this->q[0])*this->q[1]*this->kg[2] * this->kg[2]) + 
           this->mRes*this->q[0]*(4*(2*pow(p0,2) - 3*p0*this->q[0] + this->q[0] * this->q[0])*this->q[1] - 
              (4*pow(p0,2) - 8*p0*this->q[0] + 3*this->q[0] * this->q[0] + 
                 this->q[1] * this->q[1] + this->q[3] * this->q[3])*this->kg[1])*this->kg[3] + 
           2*pow(this->mRes,3)*(this->q[0]*this->q[1]*(this->q[3] - 2*this->kg[3]) - 4*p0*this->q[1]*this->kg[3] + 
              this->q[0]*this->kg[1]*this->kg[3]) + 2*this->mRes_2*this->mn*
            (this->q[0]*this->q[1]*(this->q[3] - 2*this->kg[3]) + 4*p0*this->q[1]*this->kg[3] + this->q[0]*this->kg[1]*this->kg[3]) + 
           2*this->mn*(p0 - this->q[0])*(-(this->q[1]*this->q[3]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])) + 
              this->q[1]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1])*this->kg[3] + 
              this->q[0] * this->q[0]*(this->q[3]*(this->q[1] + this->kg[1]) - (2*this->q[1] + this->kg[1])*this->kg[3]) + 
              2*p0*this->q[0]*(this->kg[1]*this->kg[3] + this->q[1]*(this->q[3] + 2*this->kg[3])))) + 
        C4vNC*this->mRes*(4*this->mRes_2*this->mn*this->q[0]*this->q[1]*(this->q[3] + this->kg[3]) + 
           2*this->mn*(p0 - this->q[0])*(-(this->q[1]*this->q[3]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])) + 
              this->q[1]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1])*this->kg[3] + 
              this->q[0] * this->q[0]*(this->q[3]*(this->q[1] + this->kg[1]) - (2*this->q[1] + this->kg[1])*this->kg[3]) + 
              2*p0*this->q[0]*(this->kg[1]*this->kg[3] + this->q[1]*(this->q[3] + 2*this->kg[3]))) + 
           this->mRes*(4*pow(p0,2)*this->q[0]*(2*this->q[1]*this->q[3] + 5*this->q[1]*this->kg[3] + this->kg[1]*this->kg[3]) + 
              this->q[0]*(2*this->q[1] * this->q[1]*this->kg[1]*(-this->q[3] + this->kg[3]) + 
                 pow(this->q[1],3)*(this->q[3] + this->kg[3]) - 2*this->q[3]*this->kg[1]*this->kg[3]*(this->q[3] + this->kg[3]) + 
                 this->q[1]*(pow(this->q[3],3) - 4*this->q[3]*this->kg[1] * this->kg[1] + 
                    2*this->q[3]*this->kg[2] * this->kg[2] - this->q[3] * this->q[3]*this->kg[3] - 
                    2*this->kg[1] * this->kg[1]*this->kg[3]) + 
                 this->q[0] * this->q[0]*(6*this->q[3]*this->kg[1] - 2*this->kg[1]*this->kg[3] + this->q[1]*(-this->q[3] + this->kg[3]))) + 
              2*p0*(this->q[1]*(-(this->q[3]*(4*this->kg[1] * this->kg[1] + 3*this->kg[2] * this->kg[2])) + 
                    3*this->q[3] * this->q[3]*this->kg[3] + this->q[1]*(3*this->q[1] + 4*this->kg[1])*this->kg[3]) + 
                 this->q[0] * this->q[0]*(3*this->q[1]*(this->q[3] - 2*this->kg[3]) + this->kg[1]*(-this->q[3] + this->kg[3])))))) + 
     C4v*this->mRes*(4*C4vNC*this->mRes*(2*this->mn*(p0 - this->q[0])*
            (-(this->q[1]*this->q[3]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])) + 
              this->q[1]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1])*this->kg[3] + 
              this->q[0] * this->q[0]*(this->q[3]*(this->q[1] + this->kg[1]) - (2*this->q[1] + this->kg[1])*this->kg[3]) + 
              2*p0*this->q[0]*(this->kg[1]*this->kg[3] + this->q[1]*(this->q[3] + 2*this->kg[3]))) + 
           this->mRes*(this->q[0]*(this->q[0] * this->q[0]*(2*this->q[3]*this->kg[1] + this->q[1]*(this->q[3] - this->kg[3])) + 
                 pow(this->q[1],3)*this->kg[3] + this->q[1] * this->q[1]*this->kg[1]*(-this->q[3] + this->kg[3]) - 
                 this->q[3]*this->kg[1]*this->kg[3]*(this->q[3] + this->kg[3]) - this->q[1]*this->kg[1] * this->kg[1]*(2*this->q[3] + this->kg[3])) + 
              4*pow(p0,2)*this->q[0]*(this->kg[1]*this->kg[3] + this->q[1]*(this->q[3] + 2*this->kg[3])) + 
              2*p0*(-(this->q[1]*this->q[3]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])) + 
                 this->q[1]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1])*this->kg[3] + 
                 this->q[0] * this->q[0]*(this->q[1]*this->q[3] - (this->q[1] + this->kg[1])*this->kg[3])))) + 
        C5vNC*(-8*this->mRes*this->mn*(p0 - this->q[0])*
            (this->q[0]*this->q[3]*(2*p0*this->q[1] + this->q[0]*(-this->q[1] + this->kg[1])) + this->q[1]*this->q[3]*this->kg[2] * this->kg[2] + 
              (this->q[1]*(-4*p0*this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3]) - 
                 this->q[0]*(2*p0 + this->q[0])*this->kg[1])*this->kg[3]) + 
           (2*p0*this->q[0]*this->q[3] + this->q[0] * this->q[0]*(-this->q[3] + this->kg[3]) + 
              this->q[1]*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3]))*
            (4*pow(p0,2)*this->q[0]*(this->q[1] - this->kg[1]) + 
              this->q[0]*(-pow(this->q[1],3) - this->q[1]*this->q[3] * this->q[3] + 
                 this->q[0] * this->q[0]*(this->q[1] - 3*this->kg[1]) - this->q[1] * this->q[1]*this->kg[1] - 
                 this->q[3] * this->q[3]*this->kg[1] + 4*this->q[1]*this->kg[1] * this->kg[1] + 4*this->q[3]*this->kg[1]*this->kg[3]) - 
              4*p0*(this->q[0] * this->q[0]*(this->q[1] - 2*this->kg[1]) + this->kg[1]*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3])))  
- 4*this->mRes_2*(this->q[0]*this->kg[1]*(this->q[3] - this->kg[3])*(this->q[0] * this->q[0] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3]) + 
              4*pow(p0,2)*this->q[0]*(this->q[1]*this->q[3] - (2*this->q[1] + this->kg[1])*this->kg[3]) + 
              2*p0*(pow(this->q[1],3)*this->kg[3] + this->q[1]*this->q[3]*(this->kg[2] * this->kg[2] + this->q[3]*this->kg[3]) + 
                 this->q[0] * this->q[0]*(this->q[3]*this->kg[1] - (this->q[1] + this->kg[1])*this->kg[3])))) + 
        2*this->mn*(this->c_i*C5a*this->mn*this->kg[2]*
            (2*this->mRes_2*(2*p0*this->q[0] - this->q[0] * this->q[0] - this->q[1] * this->q[1] + 
                 this->q[3]*this->kg[3]) + this->q[3]*(this->q[3] + this->kg[3])*
               (2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) - 
              2*this->mRes*this->mn*(2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1] * this->q[1] + 
                 2*this->q[1]*this->kg[1] + this->q[3]*this->kg[3])) + 
           C3vNC*(4*this->mRes_2*this->mn*this->q[0]*(this->q[1] + this->kg[1])*this->kg[3] + 
              2*this->mn*(p0 - this->q[0])*
               (-(this->q[1]*this->q[3]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])) + 
                 this->q[1]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1])*this->kg[3] + 
                 this->q[0] * this->q[0]*(this->q[3]*(this->q[1] + this->kg[1]) - (2*this->q[1] + this->kg[1])*this->kg[3]) + 
                 2*p0*this->q[0]*(this->kg[1]*this->kg[3] + this->q[1]*(this->q[3] + 2*this->kg[3]))) + 
              this->mRes*(4*pow(p0,2)*this->q[0]*(2*this->kg[1]*this->kg[3] + this->q[1]*(this->q[3] + 5*this->kg[3])) + 
                 this->q[0]*(2*this->q[1]*this->q[3]*(-(this->kg[1]*(this->q[1] + 2*this->kg[1])) + this->kg[2] * this->kg[2]) + 
                    (3*pow(this->q[1],3) + this->q[1]*this->q[3] * this->q[3] + this->q[1] * this->q[1]*this->kg[1] - 
                       3*this->q[3] * this->q[3]*this->kg[1] - 2*this->q[1]*this->kg[1] * this->kg[1])*this->kg[3] - 
                    2*this->q[3]*this->kg[1]*this->kg[3] * this->kg[3] + 
                    this->q[0] * this->q[0]*(6*this->q[3]*this->kg[1] - (this->q[1] + this->kg[1])*this->kg[3])) + 
                 2*p0*(-(this->q[1]*this->q[3]*(4*this->kg[1] * this->kg[1] + 3*this->kg[2] * this->kg[2])) + 
                    this->q[1]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] + 4*this->q[1]*this->kg[1])*this->kg[3] + 
                    this->q[0] * this->q[0]*(this->q[1]*(5*this->q[3] - 4*this->kg[3]) - this->kg[1]*(this->q[3] + this->kg[3]))))))) + 
     C5v*(-(C5vNC*(8*this->mn*(p0 - this->q[0])*this->kg[1]*
              (this->q[3]*(4*pow(p0,2)*this->q[0] * this->q[0] + pow(this->q[0],4) - 
                   this->q[0] * this->q[0]*this->q[1]*(this->q[1] + 2*this->kg[1]) - 
                   4*p0*(pow(this->q[0],3) - this->q[0]*this->q[1]*this->kg[1]) + 
                   this->q[1] * this->q[1]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])) + 
                (this->q[0] * this->q[0] - this->q[1] * this->q[1] + this->q[3] * this->q[3])*
                 (2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1]*this->kg[1])*this->kg[3] + 
                this->q[0] * this->q[0]*this->q[3]*this->kg[3] * this->kg[3]) + 
             8*pow(this->mRes,3)*p0*
              (this->q[1]*this->q[3]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + 
                this->q[1]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] - 2*this->q[1]*this->kg[1])*this->kg[3] - 
                this->q[0] * this->q[0]*(2*this->q[3]*this->kg[1] + this->q[1]*this->kg[3] - 2*this->kg[1]*this->kg[3]) + 
                2*p0*this->q[0]*(this->q[1]*(this->q[3] - 2*this->kg[3]) + this->kg[1]*this->kg[3])) + 
             8*this->mRes_2*this->mn*(p0 - this->q[0])*
              (this->q[1]*this->q[3]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + 
                this->q[1]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] - 2*this->q[1]*this->kg[1])*this->kg[3] + 
                this->q[0] * this->q[0]*(-(this->q[3]*(this->q[1] + this->kg[1])) + this->kg[1]*this->kg[3]) + 
                2*p0*this->q[0]*(this->q[1]*(this->q[3] - 2*this->kg[3]) + this->kg[1]*this->kg[3])) + 
             this->mRes*(this->q[0]*(this->q[1]*this->q[3]*(pow(this->q[0],4) - 
                      this->q[0] * this->q[0]*
                       (this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1]) + 
                      this->q[1]*this->kg[1]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] + this->kg[1] * this->kg[1] + 
                         this->kg[2] * this->kg[2])) - 
                   (this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3])*
                    (this->q[0] * this->q[0]*this->q[1] - pow(this->q[1],3) + 
                      (this->q[1] * this->q[1] + this->q[3] * this->q[3])*this->kg[1] - this->q[1]*this->kg[1] * this->kg[1])*
                    this->kg[3] + (this->q[0] - this->q[3])*this->q[3]*(this->q[0] + this->q[3])*this->kg[1]*this->kg[3] * this->kg[3]) - 
                8*pow(p0,3)*this->q[0] * this->q[0]*(this->q[1]*this->q[3] + this->kg[1]*(-6*this->q[3] + this->kg[3])) + 
                4*pow(p0,2)*this->q[0]*
                 (this->q[1]*this->kg[1] * this->kg[1]*(10*this->q[3] - this->kg[3]) + pow(this->q[1],3)*this->kg[3] + 
                   this->q[3]*this->kg[1]*(5*this->q[3] - this->kg[3])*this->kg[3] - 
                   this->q[1] * this->q[1]*this->kg[1]*(this->q[3] + 5*this->kg[3]) + 
                   this->q[0] * this->q[0]*(3*this->q[1]*this->q[3] - 14*this->q[3]*this->kg[1] - this->q[1]*this->kg[3] + 8*this->kg[1]*this->kg[3]))  
- 2*p0*(pow(this->q[0],4)*(3*this->q[1]*this->q[3] - 8*this->q[3]*this->kg[1] - 2*this->q[1]*this->kg[3] + 7*this->kg[1]*this->kg[3]) + 
                   4*this->q[1]*this->kg[1]*(-(this->q[1]*this->q[3]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])) + 
                      (this->q[1] - this->q[3])*(this->q[1] + this->q[3])*this->kg[1]*this->kg[3]) - 
                   this->q[0] * this->q[0]*
                    (pow(this->q[1],3)*(this->q[3] - 2*this->kg[3]) + 
                      this->q[1] * this->q[1]*this->kg[1]*(-2*this->q[3] + 5*this->kg[3]) + 
                      this->q[3]*this->kg[1]*this->kg[3]*(-7*this->q[3] + 6*this->kg[3]) + 
                      this->q[1]*(pow(this->q[3],3) - 12*this->q[3]*this->kg[1] * this->kg[1] + 
                         6*this->kg[1] * this->kg[1]*this->kg[3])))))) + 
        C4vNC*this->mRes*(8*this->mRes*this->mn*(p0 - this->q[0])*
            (-(this->q[1]*this->q[3]*this->kg[2] * this->kg[2]) + this->q[1]*(this->q[1] * this->q[1] + this->q[3] * this->q[3])*this->kg[3] + 
              this->q[0] * this->q[0]*(this->q[1]*this->q[3] - this->q[3]*this->kg[1] - 2*this->q[1]*this->kg[3] + this->kg[1]*this->kg[3]) + 
              2*p0*this->q[0]*(-(this->kg[1]*this->kg[3]) + this->q[1]*(this->q[3] + 2*this->kg[3]))) + 
           4*this->mRes_2*(4*pow(p0,2)*this->q[0]*
               (-(this->kg[1]*this->kg[3]) + this->q[1]*(this->q[3] + 2*this->kg[3])) + 
              2*p0*(-(this->q[1]*this->q[3]*this->kg[2] * this->kg[2]) + 
                 this->q[1]*(this->q[1] * this->q[1] + this->q[3] * this->q[3])*this->kg[3] + 
                 this->q[0] * this->q[0]*(this->q[1]*this->q[3] - this->q[3]*this->kg[1] - this->q[1]*this->kg[3])) + 
              this->q[0]*(this->q[1] - this->kg[1])*(this->q[0] * this->q[0]*(this->q[3] - this->kg[3]) + 
                 this->q[1]*(-(this->q[3]*this->kg[1]) + this->q[1]*this->kg[3]))) + 
           this->kg[1]*(8*pow(p0,3)*this->q[0] * this->q[0]*(-this->q[3] + this->kg[3]) + 
              4*pow(p0,2)*this->q[0]*
               (5*this->q[0] * this->q[0]*(this->q[3] - this->kg[3]) + 2*this->q[1] * this->q[1]*this->kg[3] + 
                 this->q[1]*this->kg[1]*(-3*this->q[3] + this->kg[3]) + this->q[3]*this->kg[3]*(-this->q[3] + this->kg[3])) + 
              this->q[0]*(pow(this->q[1],3)*this->kg[1]*(this->q[3] - 3*this->kg[3]) + 
                 pow(this->q[0],4)*(9*this->q[3] - 3*this->kg[3]) + 
                 pow(this->q[3],3)*this->kg[3]*(this->q[3] + this->kg[3]) + 
                 this->q[1]*this->q[3] * this->q[3]*this->kg[1]*(this->q[3] + 5*this->kg[3]) + 
                 this->q[1] * this->q[1]*this->q[3]*(-4*this->kg[2] * this->kg[2] + (this->q[3] - 7*this->kg[3])*this->kg[3]) + 
                 this->q[0] * this->q[0]*(3*this->q[1]*this->kg[1]*(-3*this->q[3] + this->kg[3]) + 
                    3*this->q[1] * this->q[1]*(this->q[3] + this->kg[3]) - 
                    this->q[3]*(this->q[3] * this->q[3] + 4*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + 
                       6*this->q[3]*this->kg[3] + this->kg[3] * this->kg[3]))) + 
              2*p0*(pow(this->q[0],4)*(-13*this->q[3] + 7*this->kg[3]) + 
                 2*this->q[1]*(-(this->q[1]*this->q[3]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])) + 
                    (this->q[1] - this->q[3])*(this->q[1] + this->q[3])*this->kg[1]*this->kg[3]) + 
                 this->q[0] * this->q[0]*(this->q[1] * this->q[1]*(3*this->q[3] - 5*this->kg[3]) + 
                    2*this->q[1]*this->kg[1]*(5*this->q[3] - 2*this->kg[3]) + 
                    this->q[3]*(4*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + this->q[3]*(this->q[3] + 5*this->kg[3]))) 
))) + 2*this->mn*(this->c_i*C5a*this->mRes*this->mn*this->kg[2]*
            (-2*this->q[0] * this->q[0]*this->q[1]*this->kg[1] + this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 
              2*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] - this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
              this->q[3] * this->q[3]*this->kg[2] * this->kg[2] - this->q[0] * this->q[0]*this->q[3]*this->kg[3] + 
              pow(this->q[3],3)*this->kg[3] + 3*this->q[1]*this->q[3]*this->kg[1]*this->kg[3] - 
              2*this->mRes*this->mn*(2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3]*this->kg[3]) + 
              2*this->mRes_2*
               (2*p0*this->q[0] - this->q[0] * this->q[0] - this->q[1] * this->q[1] + 2*this->q[1]*this->kg[1] + this->q[3]*this->kg[3])  
+ 2*p0*this->q[0]*(2*this->q[1]*this->kg[1] + this->q[3]*(this->q[3] + this->kg[3]))) + 
           C3vNC*(4*pow(this->mRes,3)*this->mn*this->q[0]*(this->q[1] - this->kg[1])*this->kg[3] + 
              2*this->mRes*this->mn*(-(this->q[0]*
                    (this->q[1]*this->q[3]*(this->kg[1] - this->kg[2])*(this->kg[1] + this->kg[2]) + 
                      this->q[0] * this->q[0]*(-2*this->q[3]*this->kg[1] + this->q[1]*(this->q[3] - 2*this->kg[3])) + 
                      pow(this->q[1],3)*this->kg[3] + 
                      this->q[1]*(this->q[3] * this->q[3] + this->kg[1] * this->kg[1])*this->kg[3] + 
                      this->q[3]*this->kg[1]*this->kg[3]*(this->q[3] + this->kg[3]))) + 
                 2*pow(p0,2)*this->q[0]*(-(this->kg[1]*this->kg[3]) + this->q[1]*(this->q[3] + 2*this->kg[3])) + 
                 p0*(pow(this->q[1],3)*this->kg[3] + this->q[1]*this->q[3]*(-this->kg[2] * this->kg[2] + this->q[3]*this->kg[3]) - 
                    this->q[0] * this->q[0]*(this->q[1]*this->q[3] + 3*this->q[3]*this->kg[1] + 6*this->q[1]*this->kg[3] - this->kg[1]*this->kg[3]))) + 
              this->mRes_2*(2*this->q[0]*this->q[1]*this->q[3]*
                  (-(this->q[1]*this->kg[1]) + 3*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + 
                 this->q[0]*(3*pow(this->q[1],3) + this->q[1]*this->q[3] * this->q[3] - 
                    5*this->q[1] * this->q[1]*this->kg[1] + this->q[3] * this->q[3]*this->kg[1])*this->kg[3] - 
                 pow(this->q[0],3)*(4*this->q[3]*this->kg[1] + this->q[1]*this->kg[3] - 3*this->kg[1]*this->kg[3]) + 
                 4*pow(p0,2)*this->q[0]*(-2*this->kg[1]*this->kg[3] + this->q[1]*(this->q[3] + 5*this->kg[3])) + 
                 2*p0*(-(this->q[1]*this->q[3]*(2*this->kg[1] * this->kg[1] + 3*this->kg[2] * this->kg[2])) + 
                    this->q[1]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1])*this->kg[3] + 
                    this->q[0] * this->q[0]*(this->q[1]*(5*this->q[3] - 4*this->kg[3]) - this->kg[1]*(this->q[3] + this->kg[3])))) + 
              this->kg[1]*(8*pow(p0,3)*this->q[0] * this->q[0]*(-this->q[3] + this->kg[3]) + 
                 4*pow(p0,2)*this->q[0]*
                  (5*this->q[0] * this->q[0]*(this->q[3] - this->kg[3]) + 2*this->q[1] * this->q[1]*this->kg[3] + 
                    this->q[1]*this->kg[1]*(-3*this->q[3] + this->kg[3]) + this->q[3]*this->kg[3]*(-this->q[3] + this->kg[3])) + 
                 this->q[0]*(pow(this->q[1],3)*this->kg[1]*(this->q[3] - 3*this->kg[3]) + 
                    pow(this->q[0],4)*(9*this->q[3] - 3*this->kg[3]) + 
                    pow(this->q[3],3)*this->kg[3]*(this->q[3] + this->kg[3]) + 
                    this->q[1]*this->q[3] * this->q[3]*this->kg[1]*(this->q[3] + 5*this->kg[3]) + 
                    this->q[1] * this->q[1]*this->q[3]*(-4*this->kg[2] * this->kg[2] + (this->q[3] - 7*this->kg[3])*this->kg[3]) + 
                    this->q[0] * this->q[0]*
                     (3*this->q[1]*this->kg[1]*(-3*this->q[3] + this->kg[3]) + 3*this->q[1] * this->q[1]*(this->q[3] + this->kg[3]) - 
                       this->q[3]*(this->q[3] * this->q[3] + 4*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + 
                          6*this->q[3]*this->kg[3] + this->kg[3] * this->kg[3]))) + 
                 2*p0*(pow(this->q[0],4)*(-13*this->q[3] + 7*this->kg[3]) + 
                    2*this->q[1]*(-(this->q[1]*this->q[3]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])) + 
                       (this->q[1] - this->q[3])*(this->q[1] + this->q[3])*this->kg[1]*this->kg[3]) + 
                    this->q[0] * this->q[0]*
                     (this->q[1] * this->q[1]*(3*this->q[3] - 5*this->kg[3]) + 
                       2*this->q[1]*this->kg[1]*(5*this->q[3] - 2*this->kg[3]) + 
                       this->q[3]*(4*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + 
                          this->q[3]*(this->q[3] + 5*this->kg[3])))))))))/
  (24.*this->mRes_2*pow(this->mn,5));

	
    
	tr[2][0] = (std::complex<double>(0,8)*C3v*C5a*pow(this->mn,3)*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3])*
     (2*this->mRes*this->mn*(-p0 + this->q[0]) + this->mRes_2*(2*p0 + this->q[0]) - 
       (p0 - this->q[0])*(2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3])) - 
    2*C3v*this->mn*this->kg[2]*(2*C3vNC*this->mn*(2*pow(this->mRes,3)*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) + 
          2*this->mRes_2*this->mn*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) + 
          this->mRes*(-4*(p0 - this->q[0])*this->q[0]*(this->q[1] * this->q[1] + this->q[3] * this->q[3]) - 
             this->q[1]*(4*pow(p0,2) - 8*p0*this->q[0] + 3*this->q[0] * this->q[0] + 
                this->q[1] * this->q[1] + this->q[3] * this->q[3])*this->kg[1] - 
             this->q[3]*(4*pow(p0,2) - 8*p0*this->q[0] + 3*this->q[0] * this->q[0] + 
                this->q[1] * this->q[1] + this->q[3] * this->q[3])*this->kg[3]) + 
          2*this->mn*(p0 - this->q[0])*(this->q[0]*
              (this->q[1] * this->q[1] + this->q[3] * this->q[3] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3]) + 
             2*p0*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]))) + 
       2*C4vNC*this->mRes*(this->mn*(p0 - this->q[0])*
           (this->q[0]*(this->q[1] * this->q[1] - this->q[1]*this->kg[1] + this->q[3]*(this->q[3] - this->kg[3])) + 
             2*p0*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3])) - 
          this->mRes*(pow(this->q[1],3)*this->kg[1] + this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 
             this->q[1] * this->q[1]*this->kg[1] * this->kg[1] - this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
             this->q[3] * this->q[3]*this->kg[2] * this->kg[2] + 
             p0*this->q[0]*(this->q[1] * this->q[1] - this->q[1]*this->kg[1] + this->q[3]*(this->q[3] - this->kg[3])) + 
             this->q[3]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1])*this->kg[3] - 
             2*pow(p0,2)*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) + 
             this->q[0] * this->q[0]*(this->q[1]*(-3*this->q[1] + this->kg[1]) + this->q[3]*(-2*this->q[3] + this->kg[3])))) + 
       C5vNC*(-((2*p0*(this->q[1] * this->q[1] + this->q[3] * this->q[3]) - 
               this->q[0]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3]))*
             (4*pow(p0,2)*this->q[0] + 
               this->q[0]*(3*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] - 
                  4*this->q[1]*this->kg[1] - 4*this->q[3]*this->kg[3]) + 
               4*p0*(-2*this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3]))) + 
          2*this->mRes_2*(-(p0*this->q[0]*
                (this->q[1] * this->q[1] - 3*this->q[1]*this->kg[1] + this->q[3]*(this->q[3] - 3*this->kg[3]))) + 
             2*pow(p0,2)*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) + 
             (this->q[1] * this->q[1] - this->q[1]*this->kg[1] + this->q[3]*(this->q[3] - this->kg[3]))*
              (-2*this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3])) + 
          2*this->mRes*this->mn*(2*this->q[0] * this->q[0]*
              (this->q[1] * this->q[1] + this->q[3] * this->q[3] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3]) + 
             2*pow(p0,2)*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) - 
             p0*this->q[0]*(this->q[1]*(3*this->q[1] + this->kg[1]) + this->q[3]*(3*this->q[3] + this->kg[3]))))) - 
    C4v*this->mRes*(std::complex<double>(0,4)*C5a*this->mn2*(p0 - this->q[0])*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3])*
        (2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) + 
       this->kg[2]*(C5vNC*(8*this->mRes*this->mn*(p0 - this->q[0])*
              (-(this->q[0]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3])) + 
                2*p0*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3])) - 
             (2*p0*(this->q[1] * this->q[1] + this->q[3] * this->q[3]) - 
                this->q[0]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3]))*
              (4*pow(p0,2)*this->q[0] + 
                this->q[0]*(3*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] - 
                   4*this->q[1]*this->kg[1] - 4*this->q[3]*this->kg[3]) + 
                4*p0*(-2*this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3])) + 
             4*this->mRes_2*(-2*p0*this->q[0]*
                 (this->q[1] * this->q[1] - this->q[1]*this->kg[1] + this->q[3]*(this->q[3] - this->kg[3])) + 
                4*pow(p0,2)*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) + 
                (this->q[1] * this->q[1] - this->q[1]*this->kg[1] + this->q[3]*(this->q[3] - this->kg[3]))*
                 (-this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3]))) + 
          2*C3vNC*this->mn*(4*this->mRes_2*this->mn*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) + 
             2*this->mn*(p0 - this->q[0])*
              (this->q[0]*(this->q[1] * this->q[1] - this->q[1]*this->kg[1] + this->q[3]*(this->q[3] - this->kg[3])) + 
                2*p0*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3])) + 
             this->mRes*(this->q[0] * this->q[0]*
                 (6*this->q[1] * this->q[1] + 6*this->q[3] * this->q[3] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3]) + 
                8*pow(p0,2)*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) - 
                (this->q[1]*this->kg[1] + this->q[3]*this->kg[3])*
                 (3*this->q[1] * this->q[1] + 3*this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1] + 2*this->q[3]*this->kg[3]) - 
                2*p0*this->q[0]*(this->q[1]*(this->q[1] + this->kg[1]) + this->q[3]*(this->q[3] + this->kg[3])))) + 
          4*C4vNC*this->mRes*(2*this->mn*(p0 - this->q[0])*
              (this->q[0]*(this->q[1] * this->q[1] - this->q[1]*this->kg[1] + this->q[3]*(this->q[3] - this->kg[3])) + 
                2*p0*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3])) + 
             this->mRes*(2*this->q[0] * this->q[0]*(this->q[1] * this->q[1] + this->q[3] * this->q[3]) - 
                2*p0*this->q[0]*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) - 
                (this->q[1]*this->kg[1] + this->q[3]*this->kg[3])*
                 (-4*pow(p0,2) + this->q[1]*(this->q[1] + this->kg[1]) + this->q[3]*(this->q[3] + this->kg[3])))))) + 
    C5v*(std::complex<double>(0,-4)*C5a*this->mRes*this->mn2*(p0 - this->q[0])*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3])*
        (2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) + 
       this->kg[2]*(C4vNC*this->mRes*(8*this->mRes*this->mn*(p0 - this->q[0])*
              (this->q[0]*(this->q[1] * this->q[1] - this->q[1]*this->kg[1] + this->q[3]*(this->q[3] - this->kg[3])) + 
                2*p0*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3])) + 
             4*this->mRes_2*(2*p0 + this->q[0])*
              (this->q[0]*(this->q[1] * this->q[1] - this->q[1]*this->kg[1] + this->q[3]*(this->q[3] - this->kg[3])) + 
                2*p0*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3])) + 
             (2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3])*
              (4*pow(p0,2)*(this->q[1] * this->q[1] - this->q[1]*this->kg[1] + this->q[3]*(this->q[3] - this->kg[3])) - 
                8*p0*this->q[0]*(this->q[1] * this->q[1] - this->q[1]*this->kg[1] + this->q[3]*(this->q[3] - this->kg[3])) + 
                this->q[0] * this->q[0]*(5*this->q[1] * this->q[1] + 5*this->q[3] * this->q[3] - 3*this->q[1]*this->kg[1] - 
                   3*this->q[3]*this->kg[3]) - 
                (this->q[1] * this->q[1] + this->q[3] * this->q[3])*(this->q[1]*(this->q[1] + this->kg[1]) + this->q[3]*(this->q[3] + this->kg[3])) 
)) + 2*C3vNC*this->mn*(4*pow(this->mRes,3)*this->mn*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) + 
             2*this->mRes*this->mn*(-(this->q[0] * this->q[0]*(2*this->q[1] * this->q[1] + this->q[3] * this->q[3])) + 
                pow(this->q[1],3)*this->kg[1] + this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 
                this->q[1] * this->q[1]*this->kg[1] * this->kg[1] - this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
                this->q[3] * this->q[3]*this->kg[2] * this->kg[2] + 
                p0*this->q[0]*(3*this->q[1] * this->q[1] - this->q[1]*this->kg[1] + this->q[3]*(3*this->q[3] - this->kg[3])) + 
                this->q[3]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1])*this->kg[3] + 
                2*pow(p0,2)*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3])) + 
             this->mRes_2*(this->q[0] * this->q[0]*
                 (4*this->q[1] * this->q[1] + 4*this->q[3] * this->q[3] - 3*this->q[1]*this->kg[1] - 3*this->q[3]*this->kg[3]) + 
                8*pow(p0,2)*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) - 
                (this->q[1] * this->q[1] + this->q[3] * this->q[3])*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) + 
                2*p0*this->q[0]*(this->q[1]*(this->q[1] + this->kg[1]) + this->q[3]*(this->q[3] + this->kg[3]))) + 
             (2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3])*
              (4*pow(p0,2)*(this->q[1] * this->q[1] - this->q[1]*this->kg[1] + this->q[3]*(this->q[3] - this->kg[3])) - 
                8*p0*this->q[0]*(this->q[1] * this->q[1] - this->q[1]*this->kg[1] + this->q[3]*(this->q[3] - this->kg[3])) + 
                this->q[0] * this->q[0]*(5*this->q[1] * this->q[1] + 5*this->q[3] * this->q[3] - 3*this->q[1]*this->kg[1] - 
                   3*this->q[3]*this->kg[3]) - 
                (this->q[1] * this->q[1] + this->q[3] * this->q[3])*(this->q[1]*(this->q[1] + this->kg[1]) + this->q[3]*(this->q[3] + this->kg[3])) 
)) + C5vNC*(8*this->mn*(p0 - this->q[0])*(2*p0*(this->q[1] * this->q[1] + this->q[3] * this->q[3]) - 
                this->q[0]*(this->q[1] * this->q[1] - this->q[1]*this->kg[1] + this->q[3]*(this->q[3] - this->kg[3])))*
              (2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) + 
             16*pow(this->mRes,3)*p0*
              (-(this->q[0]*this->q[1] * this->q[1]) - this->q[0]*this->q[3] * this->q[3] + p0*this->q[1]*this->kg[1] + 
                this->q[0]*this->q[1]*this->kg[1] + (p0 + this->q[0])*this->q[3]*this->kg[3]) + 
             8*this->mRes_2*this->mn*(p0 - this->q[0])*
              (-(this->q[0]*(this->q[1] * this->q[1] - this->q[1]*this->kg[1] + this->q[3]*(this->q[3] - this->kg[3]))) + 
                2*p0*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3])) + 
             this->mRes*(8*pow(p0,3)*this->q[0]*
                 (6*this->q[1] * this->q[1] - this->q[1]*this->kg[1] + this->q[3]*(6*this->q[3] - this->kg[3])) + 
                (-this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3])*
                 (this->q[1] * this->q[1] - this->q[1]*this->kg[1] + this->q[3]*(this->q[3] - this->kg[3]))*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) - 
                4*pow(p0,2)*
                 (2*this->q[0] * this->q[0]*
                    (7*this->q[1] * this->q[1] - 4*this->q[1]*this->kg[1] + this->q[3]*(7*this->q[3] - 4*this->kg[3])) - 
                   (5*this->q[1] * this->q[1] - this->q[1]*this->kg[1] + this->q[3]*(5*this->q[3] - this->kg[3]))*
                    (this->q[1]*this->kg[1] + this->q[3]*this->kg[3])) + 
                2*p0*this->q[0]*(8*this->q[0] * this->q[0]*
                    (this->q[1] * this->q[1] - this->q[1]*this->kg[1] + this->q[3]*(this->q[3] - this->kg[3])) + 
                   (this->q[1]*this->kg[1] + this->q[3]*this->kg[3])*
                    (this->q[0] * this->q[0] - 7*this->q[1] * this->q[1] + 6*this->q[1]*this->kg[1] + 
                      this->q[3]*(-7*this->q[3] + 6*this->kg[3]))))))))/
  (24.*this->mRes_2*pow(this->mn,5));
    
	tr[2][1] = (std::complex<double>(0,2)*C5a*this->mn2*(C5v*this->mRes*
        (this->q[3]*(this->q[1] + this->kg[1])*(2*p0*this->q[0]*this->kg[1] + 2*this->q[1]*this->kg[1] * this->kg[1] - 
             this->q[0] * this->q[0]*(this->q[1] + this->kg[1])) + 
          this->q[3]*(4*p0*this->q[0] - 2*this->q[0] * this->q[0] + this->q[1] * this->q[1] + 3*this->q[1]*this->kg[1])*
           this->kg[2] * this->kg[2] + (-((this->q[1] + this->kg[1])*
                ((2*p0 - this->q[0])*this->q[0]*this->q[1] + (this->q[1] - this->q[3])*(this->q[1] + this->q[3])*this->kg[1])) + 
             2*this->q[3] * this->q[3]*this->kg[2] * this->kg[2])*this->kg[3] + 
          2*this->mRes_2*(this->q[3]*
              (2*(p0 - this->q[0])*this->q[0] + this->kg[1]*(this->q[1] + this->kg[1]) + 2*this->kg[2] * this->kg[2]) + 
             (-2*p0*this->q[0] + this->q[0] * this->q[0] + this->q[3] * this->q[3] - this->q[1]*this->kg[1])*this->kg[3]) + 
          2*this->mRes*this->mn*(this->q[3]*(this->q[1] - this->kg[1])*this->kg[1] + 
             (-this->q[0] * this->q[0] + this->q[3] * this->q[3] + this->q[1]*this->kg[1])*this->kg[3] + 
             2*p0*this->q[0]*(this->q[3] + this->kg[3]))) - 
       2*C3v*this->mn*(8*pow(this->mRes,3)*this->mn*this->kg[3] + 
          (this->q[1] + this->kg[1])*(-(this->q[3]*this->kg[1]) + this->q[1]*this->kg[3])*
           (2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) - 
          this->mRes_2*(this->q[3]*(6*p0*this->q[0] - 2*this->q[0] * this->q[0] + 
                this->kg[1]*(this->q[1] + 3*this->kg[1])) + 
             (8*pow(p0,2) - 6*p0*this->q[0] + this->q[0] * this->q[0] + this->q[3] * this->q[3] - 
                3*this->q[1]*this->kg[1])*this->kg[3]) + 
          this->mRes*this->mn*(-(this->q[3]*(this->kg[1]*(this->q[1] + 3*this->kg[1]) + 2*this->kg[2] * this->kg[2])) + 
             (-this->q[0] * this->q[0] + 2*this->q[1] * this->q[1] + this->q[3] * this->q[3] + 3*this->q[1]*this->kg[1])*this->kg[3] + 
             2*p0*this->q[0]*(this->q[3] + this->kg[3])))) + 
    C4v*this->mRes*(std::complex<double>(0,2)*C5a*this->mn2*
        (-((this->q[1] + this->kg[1])*(-(this->q[3]*this->kg[1]) + this->q[1]*this->kg[3])*
             (2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3])) + 
          2*this->mRes_2*(2*(p0 - this->q[0])*this->q[0]*this->q[3] + this->q[3]*this->kg[1]*(this->q[1] + this->kg[1]) + 
             (-2*p0*this->q[0] + this->q[0] * this->q[0] + this->q[3] * this->q[3] - this->q[1]*this->kg[1])*this->kg[3]) + 
          2*this->mRes*this->mn*(-(this->q[3]*(this->kg[1] * this->kg[1] + 2*this->kg[2] * this->kg[2])) + 
             (-this->q[0] + this->q[3])*(this->q[0] + this->q[3])*this->kg[3] + 2*p0*this->q[0]*(this->q[3] + this->kg[3]) + 
             this->q[1]*this->kg[1]*(this->q[3] + this->kg[3]))) - 
       this->kg[2]*(4*C4vNC*this->mRes*(2*this->mn*(p0 - this->q[0])*
              (this->q[0] * this->q[0]*(this->q[1] - this->kg[1]) + 2*p0*this->q[0]*this->kg[1] + 
                this->q[3]*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3])) + 
             this->mRes*(2*pow(this->q[0],3)*this->q[1] - 2*p0*this->q[0] * this->q[0]*this->kg[1] + 
                this->q[0]*this->kg[1]*(4*pow(p0,2) + this->q[3] * this->q[3] - this->q[1]*(this->q[1] + this->kg[1])) - 
                this->q[0]*this->q[3]*(2*this->q[1] + this->kg[1])*this->kg[3] + 2*p0*this->q[3]*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3]))) + 
          C5vNC*(8*this->mRes*this->mn*(p0 - this->q[0])*
              (2*p0*this->q[0]*this->kg[1] + this->q[0] * this->q[0]*(-this->q[1] + this->kg[1]) + 
                this->q[3]*(-(this->q[3]*this->kg[1]) + this->q[1]*this->kg[3])) - 
             (2*p0*this->q[0]*this->q[1] + this->q[0] * this->q[0]*(-this->q[1] + this->kg[1]) + 
                this->q[3]*(-(this->q[3]*this->kg[1]) + this->q[1]*this->kg[3]))*
              (4*pow(p0,2)*this->q[0] + 
                this->q[0]*(3*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] - 
                   4*this->q[1]*this->kg[1] - 4*this->q[3]*this->kg[3]) + 
                4*p0*(-2*this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3])) + 
             4*this->mRes_2*(4*pow(p0,2)*this->q[0]*this->kg[1] + 
                this->q[0]*(this->q[1] - this->kg[1])*(-this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) + 
                2*p0*(this->q[0] * this->q[0]*(-this->q[1] + this->kg[1]) + this->q[3]*(-(this->q[3]*this->kg[1]) + this->q[1]*this->kg[3])) 
)) + 2*C3vNC*this->mn*(4*this->mRes_2*this->mn*this->q[0]*this->kg[1] + 
             2*this->mn*(p0 - this->q[0])*
              (this->q[0] * this->q[0]*(this->q[1] - this->kg[1]) + 2*p0*this->q[0]*this->kg[1] + 
                this->q[3]*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3])) + 
             this->mRes*(8*pow(p0,2)*this->q[0]*this->kg[1] + 
                this->q[0]*(this->q[0] * this->q[0]*(6*this->q[1] - this->kg[1]) + 
                   this->kg[1]*(-3*this->q[1] * this->q[1] + 3*this->q[3] * this->q[3] - 2*this->q[1]*this->kg[1]) - 
                   2*this->q[3]*(3*this->q[1] + this->kg[1])*this->kg[3]) - 
                2*p0*(this->q[0] * this->q[0]*(this->q[1] + this->kg[1]) + this->q[3]*(-(this->q[3]*this->kg[1]) + this->q[1]*this->kg[3])))) 
)) + this->kg[2]*(C4vNC*this->mRes*(4*C3v*this->mn*
           (this->mn*(p0 - this->q[0])*(-2*p0*this->q[0]*this->kg[1] + this->q[0] * this->q[0]*(-this->q[1] + this->kg[1]) + 
                this->q[3]*(-(this->q[3]*this->kg[1]) + this->q[1]*this->kg[3])) + 
             this->mRes*(p0*this->q[0] * this->q[0]*(this->q[1] - this->kg[1]) - 2*pow(p0,2)*this->q[0]*this->kg[1] - 
                2*this->q[0]*this->q[3] * this->q[3]*this->kg[1] + pow(this->q[0],3)*(-3*this->q[1] + this->kg[1]) + 
                this->q[0]*this->q[1]*this->kg[1]*(this->q[1] + this->kg[1]) + this->q[0]*this->q[3]*(3*this->q[1] + this->kg[1])*this->kg[3] + 
                p0*this->q[3]*(-(this->q[3]*this->kg[1]) + this->q[1]*this->kg[3]))) + 
          C5v*(8*this->mRes*this->mn*(p0 - this->q[0])*
              (this->q[0] * this->q[0]*(this->q[1] - this->kg[1]) + 2*p0*this->q[0]*this->kg[1] + 
                this->q[3]*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3])) + 
             4*this->mRes_2*(2*p0 + this->q[0])*
              (this->q[0] * this->q[0]*(this->q[1] - this->kg[1]) + 2*p0*this->q[0]*this->kg[1] + 
                this->q[3]*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3])) + 
             (2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3])*
              (4*pow(p0,2)*this->q[0]*(this->q[1] - this->kg[1]) + 
                this->q[0]*(-pow(this->q[1],3) + this->q[0] * this->q[0]*(5*this->q[1] - 3*this->kg[1]) - 
                   this->q[1] * this->q[1]*this->kg[1] + 3*this->q[3] * this->q[3]*this->kg[1] - 
                   this->q[1]*this->q[3]*(this->q[3] + 4*this->kg[3])) + 
                p0*(8*this->q[0] * this->q[0]*(-this->q[1] + this->kg[1]) + 
                   4*this->q[3]*(-(this->q[3]*this->kg[1]) + this->q[1]*this->kg[3]))))) - 
       2*C3v*this->mn*(2*C3vNC*this->mn*(2*pow(this->mRes,3)*this->q[0]*this->kg[1] + 
             2*this->mRes_2*this->mn*this->q[0]*this->kg[1] + 
             2*this->mn*(p0 - this->q[0])*
              (this->q[0] * this->q[0]*(this->q[1] - this->kg[1]) + 2*p0*this->q[0]*this->kg[1] + 
                this->q[3]*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3])) - 
             this->mRes*(4*pow(p0,2)*this->q[0]*this->kg[1] + 
                this->q[0]*(this->q[1] * this->q[1]*this->kg[1] - 3*this->q[3] * this->q[3]*this->kg[1] + 
                   this->q[0] * this->q[0]*(-4*this->q[1] + 3*this->kg[1]) + 4*this->q[1]*this->q[3]*this->kg[3]) + 
                4*p0*(this->q[0] * this->q[0]*(this->q[1] - 2*this->kg[1]) + this->q[3]*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3]))))  
+ C5vNC*(-((2*p0*this->q[0]*this->q[1] + this->q[0] * this->q[0]*(-this->q[1] + this->kg[1]) + 
                  this->q[3]*(-(this->q[3]*this->kg[1]) + this->q[1]*this->kg[3]))*
                (4*pow(p0,2)*this->q[0] + 
                  this->q[0]*(3*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] - 
                     4*this->q[1]*this->kg[1] - 4*this->q[3]*this->kg[3]) + 
                  4*p0*(-2*this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3]))) + 
             2*this->mRes*this->mn*(2*pow(p0,2)*this->q[0]*this->kg[1] + 
                2*this->q[0]*(this->q[0] * this->q[0]*(this->q[1] - this->kg[1]) + this->q[3]*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3])) - 
                p0*(this->q[0] * this->q[0]*(3*this->q[1] + this->kg[1]) + this->q[3]*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3]))) + 
             2*this->mRes_2*(2*pow(p0,2)*this->q[0]*this->kg[1] + 
                p0*(-(this->q[0] * this->q[0]*(this->q[1] - 3*this->kg[1])) + 
                   this->q[3]*(-(this->q[3]*this->kg[1]) + this->q[1]*this->kg[3])) - 
                this->q[0]*(2*this->q[0] * this->q[0]*(this->q[1] - this->kg[1]) - this->q[1] * this->q[1]*this->kg[1] + 
                   this->q[3]*this->kg[1]*(this->q[3] + this->kg[3]) + this->q[1]*(this->kg[1] * this->kg[1] - 2*this->q[3]*this->kg[3]))))) + 
       C5v*(2*C3vNC*this->mn*(4*pow(this->mRes,3)*this->mn*this->q[0]*this->kg[1] + 
             2*this->mRes*this->mn*(p0*this->q[0] * this->q[0]*(3*this->q[1] - this->kg[1]) + 
                2*pow(p0,2)*this->q[0]*this->kg[1] + p0*this->q[3]*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3]) + 
                this->q[0]*(-2*this->q[0] * this->q[0]*this->q[1] - this->q[3] * this->q[3]*this->kg[1] + 
                   this->q[1]*this->kg[1]*(this->q[1] + this->kg[1]) + this->q[3]*(2*this->q[1] + this->kg[1])*this->kg[3])) + 
             this->mRes_2*(8*pow(p0,2)*this->q[0]*this->kg[1] + 
                this->q[0]*(this->q[0] * this->q[0]*(4*this->q[1] - 3*this->kg[1]) - this->q[1] * this->q[1]*this->kg[1] + 
                   3*this->q[3] * this->q[3]*this->kg[1] - 4*this->q[1]*this->q[3]*this->kg[3]) + 
                2*p0*(this->q[0] * this->q[0]*(this->q[1] + this->kg[1]) + this->q[3]*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3]))) + 
             (2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3])*
              (4*pow(p0,2)*this->q[0]*(this->q[1] - this->kg[1]) + 
                this->q[0]*(-pow(this->q[1],3) + this->q[0] * this->q[0]*(5*this->q[1] - 3*this->kg[1]) - 
                   this->q[1] * this->q[1]*this->kg[1] + 3*this->q[3] * this->q[3]*this->kg[1] - 
                   this->q[1]*this->q[3]*(this->q[3] + 4*this->kg[3])) + 
                p0*(8*this->q[0] * this->q[0]*(-this->q[1] + this->kg[1]) + 
                   4*this->q[3]*(-(this->q[3]*this->kg[1]) + this->q[1]*this->kg[3])))) + 
          C5vNC*(8*this->mRes_2*this->mn*(p0 - this->q[0])*
              (2*p0*this->q[0]*this->kg[1] + this->q[0] * this->q[0]*(-this->q[1] + this->kg[1]) + 
                this->q[3]*(-(this->q[3]*this->kg[1]) + this->q[1]*this->kg[3])) + 
             8*pow(this->mRes,3)*p0*
              (2*p0*this->q[0]*this->kg[1] + 2*this->q[0] * this->q[0]*(-this->q[1] + this->kg[1]) + 
                this->q[3]*(-(this->q[3]*this->kg[1]) + this->q[1]*this->kg[3])) + 
             8*this->mn*(p0 - this->q[0])*(4*pow(p0,2)*this->q[0] * this->q[0]*this->q[1] + 
                pow(this->q[0],4)*(this->q[1] - this->kg[1]) + 
                2*p0*this->q[0]*(this->q[1] * this->q[1]*this->kg[1] - this->q[3] * this->q[3]*this->kg[1] + 
                   this->q[0] * this->q[0]*(-2*this->q[1] + this->kg[1]) + 2*this->q[1]*this->q[3]*this->kg[3]) - 
                this->q[3]*(this->q[1]*this->q[3]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) - 
                   this->q[1] * this->q[1]*this->kg[1]*this->kg[3] + this->q[3] * this->q[3]*this->kg[1]*this->kg[3]) + 
                this->q[0] * this->q[0]*(-(this->q[1] * this->q[1]*this->kg[1]) + this->q[3]*this->kg[1]*(this->q[3] + this->kg[3]) + 
                   this->q[1]*(this->q[3] * this->q[3] + this->kg[1] * this->kg[1] - 2*this->q[3]*this->kg[3]))) + 
             this->mRes*(8*pow(p0,3)*this->q[0] * this->q[0]*(6*this->q[1] - this->kg[1]) + 
                this->q[0]*(this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3])*this->kg[1]*
                 (this->q[1]*(-this->q[1] + this->kg[1]) + this->q[3]*(-this->q[3] + this->kg[3])) - 
                4*pow(p0,2)*this->q[0]*
                 (2*this->q[0] * this->q[0]*(7*this->q[1] - 4*this->kg[1]) - 5*this->q[1] * this->q[1]*this->kg[1] + 
                   this->q[3]*this->kg[1]*(5*this->q[3] + this->kg[3]) + this->q[1]*(this->kg[1] * this->kg[1] - 10*this->q[3]*this->kg[3])) + 
                2*p0*(pow(this->q[0],4)*(8*this->q[1] - 7*this->kg[1]) - 
                   4*this->q[3]*(this->q[1]*this->q[3]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) - 
                      this->q[1] * this->q[1]*this->kg[1]*this->kg[3] + this->q[3] * this->q[3]*this->kg[1]*this->kg[3]) + 
                   this->q[0] * this->q[0]*
                    (-7*this->q[1] * this->q[1]*this->kg[1] + 6*this->q[1]*this->kg[1] * this->kg[1] + 
                      4*this->q[1]*this->q[3]*(this->q[3] - 3*this->kg[3]) + this->q[3]*this->kg[1]*(5*this->q[3] + 6*this->kg[3]))))))))/
  (24.*this->mRes_2*pow(this->mn,5));
    
	tr[2][2] = -(-32*C4v*C4vNC*pow(this->mRes,3)*pow(p0,3)*this->q[0] * this->q[0] - 
     32*C4vNC*C5v*pow(this->mRes,3)*pow(p0,3)*this->q[0] * this->q[0] - 
     32*C4v*C5vNC*pow(this->mRes,3)*pow(p0,3)*this->q[0] * this->q[0] - 
     32*C5v*C5vNC*pow(this->mRes,3)*pow(p0,3)*this->q[0] * this->q[0] - 
     32*C4v*C4vNC*this->mRes_2*this->mn*pow(p0,3)*this->q[0] * this->q[0] - 
     32*C4vNC*C5v*this->mRes_2*this->mn*pow(p0,3)*this->q[0] * this->q[0] - 
     32*C4v*C5vNC*this->mRes_2*this->mn*pow(p0,3)*this->q[0] * this->q[0] - 
     32*C5v*C5vNC*this->mRes_2*this->mn*pow(p0,3)*this->q[0] * this->q[0] + 
     48*C4v*C4vNC*pow(this->mRes,3)*pow(p0,2)*pow(this->q[0],3) + 
     48*C4vNC*C5v*pow(this->mRes,3)*pow(p0,2)*pow(this->q[0],3) + 
     16*C4v*C5vNC*pow(this->mRes,3)*pow(p0,2)*pow(this->q[0],3) + 
     16*C5v*C5vNC*pow(this->mRes,3)*pow(p0,2)*pow(this->q[0],3) + 
     80*C4v*C4vNC*this->mRes_2*this->mn*pow(p0,2)*pow(this->q[0],3) + 
     80*C4vNC*C5v*this->mRes_2*this->mn*pow(p0,2)*pow(this->q[0],3) + 
     48*C4v*C5vNC*this->mRes_2*this->mn*pow(p0,2)*pow(this->q[0],3) + 
     48*C5v*C5vNC*this->mRes_2*this->mn*pow(p0,2)*pow(this->q[0],3) - 
     16*C4v*C4vNC*pow(this->mRes,3)*p0*pow(this->q[0],4) - 
     16*C4vNC*C5v*pow(this->mRes,3)*p0*pow(this->q[0],4) - 
     64*C4v*C4vNC*this->mRes_2*this->mn*p0*pow(this->q[0],4) - 
     64*C4vNC*C5v*this->mRes_2*this->mn*p0*pow(this->q[0],4) - 
     16*C4v*C5vNC*this->mRes_2*this->mn*p0*pow(this->q[0],4) - 
     16*C5v*C5vNC*this->mRes_2*this->mn*p0*pow(this->q[0],4) + 
     16*C4v*C4vNC*this->mRes_2*this->mn*pow(this->q[0],5) + 
     16*C4vNC*C5v*this->mRes_2*this->mn*pow(this->q[0],5) - 
     16*C4v*C4vNC*pow(this->mRes,3)*pow(p0,2)*this->q[0]*this->q[1] * this->q[1] - 
     16*C4vNC*C5v*pow(this->mRes,3)*pow(p0,2)*this->q[0]*this->q[1] * this->q[1] + 
     16*C4v*C5vNC*pow(this->mRes,3)*pow(p0,2)*this->q[0]*this->q[1] * this->q[1] + 
     16*C5v*C5vNC*pow(this->mRes,3)*pow(p0,2)*this->q[0]*this->q[1] * this->q[1] - 
     16*C4v*C4vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[0]*this->q[1] * this->q[1] - 
     16*C4vNC*C5v*this->mRes_2*this->mn*pow(p0,2)*this->q[0]*this->q[1] * this->q[1] + 
     16*C4v*C5vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[0]*this->q[1] * this->q[1] + 
     16*C5v*C5vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[0]*this->q[1] * this->q[1] + 
     8*C4v*C4vNC*pow(this->mRes,3)*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1] + 
     8*C4vNC*C5v*pow(this->mRes,3)*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1] - 
     8*C4v*C5vNC*pow(this->mRes,3)*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1] - 
     8*C5v*C5vNC*pow(this->mRes,3)*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1] + 
     24*C4v*C4vNC*this->mRes_2*this->mn*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1] + 
     24*C4vNC*C5v*this->mRes_2*this->mn*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1] - 
     24*C4v*C5vNC*this->mRes_2*this->mn*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1] - 
     24*C5v*C5vNC*this->mRes_2*this->mn*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1] - 
     8*C4v*C4vNC*this->mRes_2*this->mn*pow(this->q[0],3)*this->q[1] * this->q[1] - 
     8*C4vNC*C5v*this->mRes_2*this->mn*pow(this->q[0],3)*this->q[1] * this->q[1] + 
     8*C4v*C5vNC*this->mRes_2*this->mn*pow(this->q[0],3)*this->q[1] * this->q[1] + 
     8*C5v*C5vNC*this->mRes_2*this->mn*pow(this->q[0],3)*this->q[1] * this->q[1] - 
     16*C4v*C4vNC*pow(this->mRes,3)*pow(p0,2)*this->q[0]*this->q[3] * this->q[3] - 
     16*C4vNC*C5v*pow(this->mRes,3)*pow(p0,2)*this->q[0]*this->q[3] * this->q[3] + 
     16*C4v*C5vNC*pow(this->mRes,3)*pow(p0,2)*this->q[0]*this->q[3] * this->q[3] + 
     16*C5v*C5vNC*pow(this->mRes,3)*pow(p0,2)*this->q[0]*this->q[3] * this->q[3] - 
     16*C4v*C4vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[0]*this->q[3] * this->q[3] - 
     16*C4vNC*C5v*this->mRes_2*this->mn*pow(p0,2)*this->q[0]*this->q[3] * this->q[3] + 
     16*C4v*C5vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[0]*this->q[3] * this->q[3] + 
     16*C5v*C5vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[0]*this->q[3] * this->q[3] - 
     16*C4v*C5vNC*pow(this->mRes,3)*p0*this->q[0] * this->q[0]*this->q[3] * this->q[3] - 
     16*C5v*C5vNC*pow(this->mRes,3)*p0*this->q[0] * this->q[0]*this->q[3] * this->q[3] + 
     16*C4v*C4vNC*this->mRes_2*this->mn*p0*this->q[0] * this->q[0]*this->q[3] * this->q[3] + 
     16*C4vNC*C5v*this->mRes_2*this->mn*p0*this->q[0] * this->q[0]*this->q[3] * this->q[3] - 
     32*C4v*C5vNC*this->mRes_2*this->mn*p0*this->q[0] * this->q[0]*this->q[3] * this->q[3] - 
     32*C5v*C5vNC*this->mRes_2*this->mn*p0*this->q[0] * this->q[0]*this->q[3] * this->q[3] + 
     16*C4v*C5vNC*this->mRes_2*this->mn*pow(this->q[0],3)*this->q[3] * this->q[3] + 
     16*C5v*C5vNC*this->mRes_2*this->mn*pow(this->q[0],3)*this->q[3] * this->q[3] - 
     32*C4v*C4vNC*pow(this->mRes,3)*pow(p0,2)*this->q[0]*this->q[1]*this->kg[1] - 
     32*C4vNC*C5v*pow(this->mRes,3)*pow(p0,2)*this->q[0]*this->q[1]*this->kg[1] - 
     32*C4v*C5vNC*pow(this->mRes,3)*pow(p0,2)*this->q[0]*this->q[1]*this->kg[1] - 
     32*C5v*C5vNC*pow(this->mRes,3)*pow(p0,2)*this->q[0]*this->q[1]*this->kg[1] - 
     32*C4v*C4vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[0]*this->q[1]*this->kg[1] - 
     32*C4vNC*C5v*this->mRes_2*this->mn*pow(p0,2)*this->q[0]*this->q[1]*this->kg[1] - 
     32*C4v*C5vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[0]*this->q[1]*this->kg[1] - 
     32*C5v*C5vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[0]*this->q[1]*this->kg[1] + 
     24*C4v*C4vNC*pow(this->mRes,3)*p0*this->q[0] * this->q[0]*this->q[1]*this->kg[1] + 
     24*C4vNC*C5v*pow(this->mRes,3)*p0*this->q[0] * this->q[0]*this->q[1]*this->kg[1] + 
     8*C4v*C5vNC*pow(this->mRes,3)*p0*this->q[0] * this->q[0]*this->q[1]*this->kg[1] + 
     8*C5v*C5vNC*pow(this->mRes,3)*p0*this->q[0] * this->q[0]*this->q[1]*this->kg[1] + 
     56*C4v*C4vNC*this->mRes_2*this->mn*p0*this->q[0] * this->q[0]*this->q[1]*this->kg[1] + 
     56*C4vNC*C5v*this->mRes_2*this->mn*p0*this->q[0] * this->q[0]*this->q[1]*this->kg[1] + 
     40*C4v*C5vNC*this->mRes_2*this->mn*p0*this->q[0] * this->q[0]*this->q[1]*this->kg[1] + 
     40*C5v*C5vNC*this->mRes_2*this->mn*p0*this->q[0] * this->q[0]*this->q[1]*this->kg[1] - 
     24*C4v*C4vNC*this->mRes_2*this->mn*pow(this->q[0],3)*this->q[1]*this->kg[1] - 
     24*C4vNC*C5v*this->mRes_2*this->mn*pow(this->q[0],3)*this->q[1]*this->kg[1] - 
     8*C4v*C5vNC*this->mRes_2*this->mn*pow(this->q[0],3)*this->q[1]*this->kg[1] - 
     8*C5v*C5vNC*this->mRes_2*this->mn*pow(this->q[0],3)*this->q[1]*this->kg[1] - 
     8*C4v*C4vNC*pow(this->mRes,3)*p0*pow(this->q[1],3)*this->kg[1] - 
     8*C4vNC*C5v*pow(this->mRes,3)*p0*pow(this->q[1],3)*this->kg[1] + 
     8*C4v*C5vNC*pow(this->mRes,3)*p0*pow(this->q[1],3)*this->kg[1] + 
     8*C5v*C5vNC*pow(this->mRes,3)*p0*pow(this->q[1],3)*this->kg[1] - 
     8*C4v*C4vNC*this->mRes_2*this->mn*p0*pow(this->q[1],3)*this->kg[1] - 
     8*C4vNC*C5v*this->mRes_2*this->mn*p0*pow(this->q[1],3)*this->kg[1] + 
     8*C4v*C5vNC*this->mRes_2*this->mn*p0*pow(this->q[1],3)*this->kg[1] + 
     8*C5v*C5vNC*this->mRes_2*this->mn*p0*pow(this->q[1],3)*this->kg[1] + 
     8*C4v*C4vNC*this->mRes_2*this->mn*this->q[0]*pow(this->q[1],3)*this->kg[1] + 
     8*C4vNC*C5v*this->mRes_2*this->mn*this->q[0]*pow(this->q[1],3)*this->kg[1] - 
     8*C4v*C5vNC*this->mRes_2*this->mn*this->q[0]*pow(this->q[1],3)*this->kg[1] - 
     8*C5v*C5vNC*this->mRes_2*this->mn*this->q[0]*pow(this->q[1],3)*this->kg[1] - 
     8*C4v*C4vNC*pow(this->mRes,3)*p0*this->q[1]*this->q[3] * this->q[3]*this->kg[1] - 
     8*C4vNC*C5v*pow(this->mRes,3)*p0*this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 
     8*C4v*C5vNC*pow(this->mRes,3)*p0*this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 
     8*C5v*C5vNC*pow(this->mRes,3)*p0*this->q[1]*this->q[3] * this->q[3]*this->kg[1] - 
     8*C4v*C4vNC*this->mRes_2*this->mn*p0*this->q[1]*this->q[3] * this->q[3]*this->kg[1] - 
     8*C4vNC*C5v*this->mRes_2*this->mn*p0*this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 
     8*C4v*C5vNC*this->mRes_2*this->mn*p0*this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 
     8*C5v*C5vNC*this->mRes_2*this->mn*p0*this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 
     8*C4v*C4vNC*this->mRes_2*this->mn*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 
     8*C4vNC*C5v*this->mRes_2*this->mn*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1] - 
     8*C4v*C5vNC*this->mRes_2*this->mn*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1] - 
     8*C5v*C5vNC*this->mRes_2*this->mn*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1] - 
     8*C4v*C4vNC*pow(this->mRes,3)*p0*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] - 
     8*C4vNC*C5v*pow(this->mRes,3)*p0*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] - 
     8*C4v*C5vNC*pow(this->mRes,3)*p0*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] - 
     8*C5v*C5vNC*pow(this->mRes,3)*p0*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] - 
     8*C4v*C4vNC*this->mRes_2*this->mn*p0*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] - 
     8*C4vNC*C5v*this->mRes_2*this->mn*p0*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] - 
     8*C4v*C5vNC*this->mRes_2*this->mn*p0*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] - 
     8*C5v*C5vNC*this->mRes_2*this->mn*p0*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] + 
     8*C4v*C4vNC*this->mRes_2*this->mn*this->q[0]*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] + 
     8*C4vNC*C5v*this->mRes_2*this->mn*this->q[0]*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] + 
     8*C4v*C5vNC*this->mRes_2*this->mn*this->q[0]*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] + 
     8*C5v*C5vNC*this->mRes_2*this->mn*this->q[0]*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] + 
     8*C4v*C4vNC*pow(this->mRes,3)*p0*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
     8*C4vNC*C5v*pow(this->mRes,3)*p0*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
     8*C4v*C5vNC*pow(this->mRes,3)*p0*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
     8*C5v*C5vNC*pow(this->mRes,3)*p0*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
     8*C4v*C4vNC*this->mRes_2*this->mn*p0*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
     8*C4vNC*C5v*this->mRes_2*this->mn*p0*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
     8*C4v*C5vNC*this->mRes_2*this->mn*p0*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
     8*C5v*C5vNC*this->mRes_2*this->mn*p0*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
     8*C4v*C4vNC*this->mRes_2*this->mn*this->q[0]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
     8*C4vNC*C5v*this->mRes_2*this->mn*this->q[0]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
     8*C4v*C5vNC*this->mRes_2*this->mn*this->q[0]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
     8*C5v*C5vNC*this->mRes_2*this->mn*this->q[0]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
     std::complex<double>(0,2)*C4v*C5a*this->mRes*this->mn2*this->q[0] * this->q[0]*this->q[1]*this->q[3]*this->kg[2] - 
     std::complex<double>(0,2)*C5a*C5v*this->mRes*this->mn2*this->q[0] * this->q[0]*this->q[1]*this->q[3]*this->kg[2] - 
     std::complex<double>(0,4)*C4v*C5a*pow(this->mRes,3)*this->mn2*this->q[3]*this->kg[1]*this->kg[2] + 
     std::complex<double>(0,4)*C5a*C5v*pow(this->mRes,3)*this->mn2*this->q[3]*this->kg[1]*this->kg[2] - 
     std::complex<double>(0,4)*C4v*C5a*this->mRes_2*pow(this->mn,3)*this->q[3]*this->kg[1]*this->kg[2] + 
     std::complex<double>(0,4)*C5a*C5v*this->mRes_2*pow(this->mn,3)*this->q[3]*this->kg[1]*this->kg[2] - 
     std::complex<double>(0,4)*C4v*C5a*this->mRes*this->mn2*p0*this->q[0]*this->q[3]*this->kg[1]*this->kg[2] + 
     std::complex<double>(0,4)*C5a*C5v*this->mRes*this->mn2*p0*this->q[0]*this->q[3]*this->kg[1]*this->kg[2] + 
     std::complex<double>(0,2)*C4v*C5a*this->mRes*this->mn2*this->q[0] * this->q[0]*this->q[3]*this->kg[1]*this->kg[2] - 
     std::complex<double>(0,2)*C5a*C5v*this->mRes*this->mn2*this->q[0] * this->q[0]*this->q[3]*this->kg[1]*this->kg[2] - 
     std::complex<double>(0,4)*C4v*C5a*this->mRes*this->mn2*this->q[1]*this->q[3]*this->kg[1] * this->kg[1]*this->kg[2] + 
     std::complex<double>(0,4)*C5a*C5v*this->mRes*this->mn2*this->q[1]*this->q[3]*this->kg[1] * this->kg[1]*this->kg[2] + 
     16*C4v*C4vNC*pow(this->mRes,3)*pow(p0,2)*this->q[0]*this->kg[2] * this->kg[2] - 
     16*C4vNC*C5v*pow(this->mRes,3)*pow(p0,2)*this->q[0]*this->kg[2] * this->kg[2] + 
     16*C4v*C5vNC*pow(this->mRes,3)*pow(p0,2)*this->q[0]*this->kg[2] * this->kg[2] - 
     16*C5v*C5vNC*pow(this->mRes,3)*pow(p0,2)*this->q[0]*this->kg[2] * this->kg[2] + 
     16*C4v*C4vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[0]*this->kg[2] * this->kg[2] - 
     16*C4vNC*C5v*this->mRes_2*this->mn*pow(p0,2)*this->q[0]*this->kg[2] * this->kg[2] + 
     16*C4v*C5vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[0]*this->kg[2] * this->kg[2] - 
     16*C5v*C5vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[0]*this->kg[2] * this->kg[2] - 
     8*C4v*C4vNC*pow(this->mRes,3)*p0*this->q[0] * this->q[0]*this->kg[2] * this->kg[2] + 
     8*C4v*C5vNC*pow(this->mRes,3)*p0*this->q[0] * this->q[0]*this->kg[2] * this->kg[2] - 
     16*C5v*C5vNC*pow(this->mRes,3)*p0*this->q[0] * this->q[0]*this->kg[2] * this->kg[2] - 
     24*C4v*C4vNC*this->mRes_2*this->mn*p0*this->q[0] * this->q[0]*this->kg[2] * this->kg[2] + 
     24*C4vNC*C5v*this->mRes_2*this->mn*p0*this->q[0] * this->q[0]*this->kg[2] * this->kg[2] - 
     8*C4v*C5vNC*this->mRes_2*this->mn*p0*this->q[0] * this->q[0]*this->kg[2] * this->kg[2] + 
     8*C5v*C5vNC*this->mRes_2*this->mn*p0*this->q[0] * this->q[0]*this->kg[2] * this->kg[2] + 
     8*C4vNC*C5v*this->mRes*pow(p0,3)*this->q[0] * this->q[0]*this->kg[2] * this->kg[2] + 
     8*C5v*C5vNC*this->mRes*pow(p0,3)*this->q[0] * this->q[0]*this->kg[2] * this->kg[2] + 
     4*C4vNC*C5v*pow(this->mRes,3)*pow(this->q[0],3)*this->kg[2] * this->kg[2] + 
     4*C4v*C5vNC*pow(this->mRes,3)*pow(this->q[0],3)*this->kg[2] * this->kg[2] + 
     8*C4v*C4vNC*this->mRes_2*this->mn*pow(this->q[0],3)*this->kg[2] * this->kg[2] - 
     8*C4vNC*C5v*this->mRes_2*this->mn*pow(this->q[0],3)*this->kg[2] * this->kg[2] - 
     8*C4v*C5vNC*this->mRes_2*this->mn*pow(this->q[0],3)*this->kg[2] * this->kg[2] + 
     8*C5v*C5vNC*this->mRes_2*this->mn*pow(this->q[0],3)*this->kg[2] * this->kg[2] - 
     20*C4vNC*C5v*this->mRes*pow(p0,2)*pow(this->q[0],3)*this->kg[2] * this->kg[2] - 
     4*C4v*C5vNC*this->mRes*pow(p0,2)*pow(this->q[0],3)*this->kg[2] * this->kg[2] - 
     32*C5v*C5vNC*this->mRes*pow(p0,2)*pow(this->q[0],3)*this->kg[2] * this->kg[2] - 
     16*C5v*C5vNC*this->mn*pow(p0,2)*pow(this->q[0],3)*this->kg[2] * this->kg[2] + 
     14*C4vNC*C5v*this->mRes*p0*pow(this->q[0],4)*this->kg[2] * this->kg[2] + 
     8*C4v*C5vNC*this->mRes*p0*pow(this->q[0],4)*this->kg[2] * this->kg[2] + 
     14*C5v*C5vNC*this->mRes*p0*pow(this->q[0],4)*this->kg[2] * this->kg[2] + 
     24*C5v*C5vNC*this->mn*p0*pow(this->q[0],4)*this->kg[2] * this->kg[2] - 
     3*C4vNC*C5v*this->mRes*pow(this->q[0],5)*this->kg[2] * this->kg[2] - 
     3*C4v*C5vNC*this->mRes*pow(this->q[0],5)*this->kg[2] * this->kg[2] - 
     8*C5v*C5vNC*this->mn*pow(this->q[0],5)*this->kg[2] * this->kg[2] + 
     8*C4v*C4vNC*pow(this->mRes,3)*p0*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] - 
     8*C4vNC*C5v*pow(this->mRes,3)*p0*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] - 
     8*C4v*C5vNC*pow(this->mRes,3)*p0*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] + 
     8*C5v*C5vNC*pow(this->mRes,3)*p0*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] + 
     8*C4v*C4vNC*this->mRes_2*this->mn*p0*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] - 
     8*C4vNC*C5v*this->mRes_2*this->mn*p0*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] - 
     8*C4v*C5vNC*this->mRes_2*this->mn*p0*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] + 
     8*C5v*C5vNC*this->mRes_2*this->mn*p0*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] + 
     4*C4v*C4vNC*pow(this->mRes,3)*this->q[0]*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] - 
     4*C4vNC*C5v*pow(this->mRes,3)*this->q[0]*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] - 
     8*C4v*C4vNC*this->mRes_2*this->mn*this->q[0]*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] + 
     8*C4vNC*C5v*this->mRes_2*this->mn*this->q[0]*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] + 
     8*C4v*C5vNC*this->mRes_2*this->mn*this->q[0]*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] - 
     8*C5v*C5vNC*this->mRes_2*this->mn*this->q[0]*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] + 
     8*C4vNC*C5v*this->mRes*pow(p0,2)*this->q[0]*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] + 
     4*C4v*C5vNC*this->mRes*pow(p0,2)*this->q[0]*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] + 
     20*C5v*C5vNC*this->mRes*pow(p0,2)*this->q[0]*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] + 
     16*C5v*C5vNC*this->mn*pow(p0,2)*this->q[0]*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] - 
     10*C4vNC*C5v*this->mRes*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] - 
     8*C4v*C5vNC*this->mRes*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] - 
     10*C5v*C5vNC*this->mRes*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] - 
     24*C5v*C5vNC*this->mn*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] + 
     3*C4vNC*C5v*this->mRes*pow(this->q[0],3)*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] + 
     2*C4v*C5vNC*this->mRes*pow(this->q[0],3)*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] + 
     C5v*C5vNC*this->mRes*pow(this->q[0],3)*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] + 
     8*C5v*C5vNC*this->mn*pow(this->q[0],3)*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] + 
     C4v*C5vNC*this->mRes*this->q[0]*pow(this->q[1],4)*this->kg[2] * this->kg[2] - 
     C5v*C5vNC*this->mRes*this->q[0]*pow(this->q[1],4)*this->kg[2] * this->kg[2] + 
     16*C4v*C4vNC*pow(this->mRes,3)*p0*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] + 
     16*C5v*C5vNC*pow(this->mRes,3)*p0*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] + 
     16*C4v*C4vNC*this->mRes_2*this->mn*p0*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] + 
     16*C5v*C5vNC*this->mRes_2*this->mn*p0*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] + 
     4*C4v*C4vNC*pow(this->mRes,3)*this->q[0]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] - 
     4*C4vNC*C5v*pow(this->mRes,3)*this->q[0]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] - 
     16*C4v*C4vNC*this->mRes_2*this->mn*this->q[0]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] - 
     16*C5v*C5vNC*this->mRes_2*this->mn*this->q[0]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] + 
     8*C4vNC*C5v*this->mRes*pow(p0,2)*this->q[0]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] + 
     4*C4v*C5vNC*this->mRes*pow(p0,2)*this->q[0]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] + 
     20*C5v*C5vNC*this->mRes*pow(p0,2)*this->q[0]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] + 
     16*C5v*C5vNC*this->mn*pow(p0,2)*this->q[0]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] - 
     10*C4vNC*C5v*this->mRes*p0*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] - 
     8*C4v*C5vNC*this->mRes*p0*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] - 
     10*C5v*C5vNC*this->mRes*p0*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] - 
     24*C5v*C5vNC*this->mn*p0*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] + 
     3*C4vNC*C5v*this->mRes*pow(this->q[0],3)*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] + 
     2*C4v*C5vNC*this->mRes*pow(this->q[0],3)*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] + 
     C5v*C5vNC*this->mRes*pow(this->q[0],3)*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] + 
     8*C5v*C5vNC*this->mn*pow(this->q[0],3)*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] + 
     2*C4v*C5vNC*this->mRes*this->q[0]*this->q[1] * this->q[1]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] - 
     2*C5v*C5vNC*this->mRes*this->q[0]*this->q[1] * this->q[1]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] + 
     C4v*C5vNC*this->mRes*this->q[0]*pow(this->q[3],4)*this->kg[2] * this->kg[2] - 
     C5v*C5vNC*this->mRes*this->q[0]*pow(this->q[3],4)*this->kg[2] * this->kg[2] - 
     4*C4v*C4vNC*pow(this->mRes,3)*this->q[0]*this->q[1]*this->kg[1]*this->kg[2] * this->kg[2] - 
     4*C4v*C5vNC*pow(this->mRes,3)*this->q[0]*this->q[1]*this->kg[1]*this->kg[2] * this->kg[2] + 
     4*C4vNC*C5v*this->mRes*pow(p0,2)*this->q[0]*this->q[1]*this->kg[1]*this->kg[2] * this->kg[2] + 
     4*C5v*C5vNC*this->mRes*pow(p0,2)*this->q[0]*this->q[1]*this->kg[1]*this->kg[2] * this->kg[2] - 
     8*C4vNC*C5v*this->mRes*p0*this->q[0] * this->q[0]*this->q[1]*this->kg[1]*this->kg[2] * this->kg[2] - 
     4*C4v*C5vNC*this->mRes*p0*this->q[0] * this->q[0]*this->q[1]*this->kg[1]*this->kg[2] * this->kg[2] - 
     12*C5v*C5vNC*this->mRes*p0*this->q[0] * this->q[0]*this->q[1]*this->kg[1]*this->kg[2] * this->kg[2] - 
     8*C5v*C5vNC*this->mn*p0*this->q[0] * this->q[0]*this->q[1]*this->kg[1]*this->kg[2] * this->kg[2] + 
     3*C4vNC*C5v*this->mRes*pow(this->q[0],3)*this->q[1]*this->kg[1]*this->kg[2] * this->kg[2] + 
     4*C4v*C5vNC*this->mRes*pow(this->q[0],3)*this->q[1]*this->kg[1]*this->kg[2] * this->kg[2] - 
     C5v*C5vNC*this->mRes*pow(this->q[0],3)*this->q[1]*this->kg[1]*this->kg[2] * this->kg[2] + 
     8*C5v*C5vNC*this->mn*pow(this->q[0],3)*this->q[1]*this->kg[1]*this->kg[2] * this->kg[2] + 
     4*C4vNC*C5v*this->mRes*p0*pow(this->q[1],3)*this->kg[1]*this->kg[2] * this->kg[2] + 
     4*C4v*C5vNC*this->mRes*p0*pow(this->q[1],3)*this->kg[1]*this->kg[2] * this->kg[2] + 
     8*C5v*C5vNC*this->mRes*p0*pow(this->q[1],3)*this->kg[1]*this->kg[2] * this->kg[2] + 
     8*C5v*C5vNC*this->mn*p0*pow(this->q[1],3)*this->kg[1]*this->kg[2] * this->kg[2] - 
     3*C4vNC*C5v*this->mRes*this->q[0]*pow(this->q[1],3)*this->kg[1]*this->kg[2] * this->kg[2] - 
     4*C4v*C5vNC*this->mRes*this->q[0]*pow(this->q[1],3)*this->kg[1]*this->kg[2] * this->kg[2] + 
     C5v*C5vNC*this->mRes*this->q[0]*pow(this->q[1],3)*this->kg[1]*this->kg[2] * this->kg[2] - 
     8*C5v*C5vNC*this->mn*this->q[0]*pow(this->q[1],3)*this->kg[1]*this->kg[2] * this->kg[2] + 
     4*C4vNC*C5v*this->mRes*p0*this->q[1]*this->q[3] * this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] + 
     4*C4v*C5vNC*this->mRes*p0*this->q[1]*this->q[3] * this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] + 
     8*C5v*C5vNC*this->mRes*p0*this->q[1]*this->q[3] * this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] + 
     8*C5v*C5vNC*this->mn*p0*this->q[1]*this->q[3] * this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] - 
     3*C4vNC*C5v*this->mRes*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] - 
     4*C4v*C5vNC*this->mRes*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] + 
     C5v*C5vNC*this->mRes*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] - 
     8*C5v*C5vNC*this->mn*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] - 
     std::complex<double>(0,2)*C4v*C5a*this->mRes*this->mn2*this->q[1]*this->q[3]*pow(this->kg[2],3) + 
     std::complex<double>(0,2)*C5a*C5v*this->mRes*this->mn2*this->q[1]*this->q[3]*pow(this->kg[2],3) - 
     (2*C4v*this->mRes*(4*C5vNC*this->mRes*((this->mRes + this->mn)*p0 - this->mn*this->q[0])*this->q[3]*
            (4*p0*this->q[0] - this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3] + 
              2*this->q[1]*this->kg[1]) - this->c_i*C5a*this->mn2*
            ((2*this->mRes*(this->mRes + this->mn) + 2*p0*this->q[0] - this->q[0] * this->q[0])*this->q[1] + 
              (this->q[1] - this->q[3])*(this->q[1] + this->q[3])*this->kg[1])*this->kg[2] + 
           2*C5vNC*this->q[3]*(this->mRes_2*this->q[0] + 
              (p0 - this->q[0])*(this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3]))*
            this->kg[2] * this->kg[2] + 2*C4vNC*this->mRes*this->q[3]*
            (2*((this->mRes + this->mn)*p0 - this->mn*this->q[0])*
               (4*p0*this->q[0] - 3*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] + 
                 2*this->q[1]*this->kg[1]) + this->mRes*this->q[0]*this->kg[2] * this->kg[2])) + 
        C5v*(std::complex<double>(0,2)*C5a*this->mRes*this->mn2*
            ((2*this->mRes*(this->mRes + this->mn) + 2*p0*this->q[0] - this->q[0] * this->q[0])*this->q[1] + 
              (this->q[1] - this->q[3])*(this->q[1] + this->q[3])*this->kg[1])*this->kg[2] + 
           C4vNC*this->mRes*this->q[3]*(8*this->mRes*((this->mRes + this->mn)*p0 - this->mn*this->q[0])*
               (4*p0*this->q[0] - 3*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] + 
                 2*this->q[1]*this->kg[1]) + (-4*pow(p0,2)*this->q[0] + 
                 8*p0*this->q[0] * this->q[0] - 
                 4*p0*(this->q[1] * this->q[1] + this->q[3] * this->q[3]) + 
                 3*this->q[0]*(-this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3]))*
               this->kg[2] * this->kg[2]) + 
           C5vNC*this->q[3]*(8*this->mRes_2*((this->mRes + this->mn)*p0 - this->mn*this->q[0])*
               (4*p0*this->q[0] - this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3] + 
                 2*this->q[1]*this->kg[1]) - (8*this->mn*(p0 - this->q[0])*
                  (-this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3]) + 
                 this->mRes*(4*pow(p0,2)*this->q[0] - 12*p0*this->q[0] * this->q[0] + 
                    8*p0*(this->q[1] * this->q[1] + this->q[3] * this->q[3]) + 
                    this->q[0]*(-this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3])))*
               this->kg[2] * this->kg[2])))*this->kg[3] - 
     2*C3v*this->mn*(8*C5vNC*pow(this->mRes,3)*this->mn*p0*this->q[0] * this->q[0] + 
        16*C5vNC*this->mRes_2*pow(p0,3)*this->q[0] * this->q[0] + 
        8*C5vNC*this->mRes*this->mn*pow(p0,3)*this->q[0] * this->q[0] - 
        12*C5vNC*this->mRes_2*pow(p0,2)*pow(this->q[0],3) - 
        12*C5vNC*this->mRes*this->mn*pow(p0,2)*pow(this->q[0],3) + 
        2*C5vNC*this->mRes_2*p0*pow(this->q[0],4) + 
        4*C5vNC*this->mRes*this->mn*p0*pow(this->q[0],4) - 
        4*C5vNC*pow(this->mRes,3)*this->mn*this->q[0]*this->q[1] * this->q[1] - 
        8*C5vNC*this->mRes_2*pow(p0,2)*this->q[0]*this->q[1] * this->q[1] - 
        4*C5vNC*this->mRes*this->mn*pow(p0,2)*this->q[0]*this->q[1] * this->q[1] + 
        8*C5vNC*this->mRes_2*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1] + 
        6*C5vNC*this->mRes*this->mn*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1] - 
        C5vNC*this->mRes_2*pow(this->q[0],3)*this->q[1] * this->q[1] - 
        2*C5vNC*this->mRes*this->mn*pow(this->q[0],3)*this->q[1] * this->q[1] - 
        C5vNC*this->mRes_2*this->q[0]*pow(this->q[1],4) - 
        4*C5vNC*pow(this->mRes,3)*this->mn*this->q[0]*this->q[3] * this->q[3] - 
        8*C5vNC*this->mRes_2*pow(p0,2)*this->q[0]*this->q[3] * this->q[3] - 
        4*C5vNC*this->mRes*this->mn*pow(p0,2)*this->q[0]*this->q[3] * this->q[3] + 
        14*C5vNC*this->mRes_2*p0*this->q[0] * this->q[0]*this->q[3] * this->q[3] + 
        8*C5vNC*this->mRes*this->mn*p0*this->q[0] * this->q[0]*this->q[3] * this->q[3] - 
        3*C5vNC*this->mRes_2*pow(this->q[0],3)*this->q[3] * this->q[3] - 
        4*C5vNC*this->mRes*this->mn*pow(this->q[0],3)*this->q[3] * this->q[3] - 
        2*C5vNC*this->mRes_2*this->q[0]*this->q[1] * this->q[1]*this->q[3] * this->q[3] - 
        C5vNC*this->mRes_2*this->q[0]*pow(this->q[3],4) + 
        4*C5vNC*pow(this->mRes,3)*this->mn*this->q[0]*this->q[1]*this->kg[1] + 
        20*C5vNC*this->mRes_2*pow(p0,2)*this->q[0]*this->q[1]*this->kg[1] + 
        8*C5vNC*this->mRes*this->mn*pow(p0,2)*this->q[0]*this->q[1]*this->kg[1] - 
        10*C5vNC*this->mRes_2*p0*this->q[0] * this->q[0]*this->q[1]*this->kg[1] - 
        10*C5vNC*this->mRes*this->mn*p0*this->q[0] * this->q[0]*this->q[1]*this->kg[1] + 
        C5vNC*this->mRes_2*pow(this->q[0],3)*this->q[1]*this->kg[1] + 
        2*C5vNC*this->mRes*this->mn*pow(this->q[0],3)*this->q[1]*this->kg[1] - 
        6*C5vNC*this->mRes_2*p0*pow(this->q[1],3)*this->kg[1] - 
        2*C5vNC*this->mRes*this->mn*p0*pow(this->q[1],3)*this->kg[1] + 
        3*C5vNC*this->mRes_2*this->q[0]*pow(this->q[1],3)*this->kg[1] + 
        2*C5vNC*this->mRes*this->mn*this->q[0]*pow(this->q[1],3)*this->kg[1] - 
        6*C5vNC*this->mRes_2*p0*this->q[1]*this->q[3] * this->q[3]*this->kg[1] - 
        2*C5vNC*this->mRes*this->mn*p0*this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 
        3*C5vNC*this->mRes_2*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 
        2*C5vNC*this->mRes*this->mn*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 
        6*C5vNC*this->mRes_2*p0*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] + 
        2*C5vNC*this->mRes*this->mn*p0*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] - 
        2*C5vNC*this->mRes_2*this->q[0]*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] - 
        2*C5vNC*this->mRes*this->mn*this->q[0]*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] - 
        6*C5vNC*this->mRes_2*p0*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
        2*C5vNC*this->mRes*this->mn*p0*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
        2*C5vNC*this->mRes_2*this->q[0]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
        2*C5vNC*this->mRes*this->mn*this->q[0]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
        std::complex<double>(0,2)*C5a*this->mn2*this->q[0] * this->q[0]*this->q[1]*this->q[3]*this->kg[2] + 
        std::complex<double>(0,6)*C5a*this->mRes_2*this->mn2*this->q[3]*this->kg[1]*this->kg[2] + 
        std::complex<double>(0,2)*C5a*this->mRes*pow(this->mn,3)*this->q[3]*this->kg[1]*this->kg[2] + 
        std::complex<double>(0,4)*C5a*this->mn2*p0*this->q[0]*this->q[3]*this->kg[1]*this->kg[2] - 
        std::complex<double>(0,2)*C5a*this->mn2*this->q[0] * this->q[0]*this->q[3]*this->kg[1]*this->kg[2] + 
        std::complex<double>(0,4)*C5a*this->mn2*this->q[1]*this->q[3]*this->kg[1] * this->kg[1]*this->kg[2] - 
        4*C5vNC*this->mRes_2*pow(p0,2)*this->q[0]*this->kg[2] * this->kg[2] - 
        4*C5vNC*this->mRes*this->mn*pow(p0,2)*this->q[0]*this->kg[2] * this->kg[2] - 
        6*C5vNC*this->mRes_2*p0*this->q[0] * this->q[0]*this->kg[2] * this->kg[2] + 
        2*C5vNC*this->mRes*this->mn*p0*this->q[0] * this->q[0]*this->kg[2] * this->kg[2] - 
        4*C5vNC*this->mRes_2*pow(this->q[0],3)*this->kg[2] * this->kg[2] + 
        4*C5vNC*this->mRes*this->mn*pow(this->q[0],3)*this->kg[2] * this->kg[2] + 
        4*C5vNC*pow(p0,2)*pow(this->q[0],3)*this->kg[2] * this->kg[2] - 
        8*C5vNC*p0*pow(this->q[0],4)*this->kg[2] * this->kg[2] + 
        3*C5vNC*pow(this->q[0],5)*this->kg[2] * this->kg[2] + 
        2*C5vNC*this->mRes_2*p0*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] + 
        2*C5vNC*this->mRes*this->mn*p0*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] + 
        2*C5vNC*this->mRes_2*this->q[0]*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] - 
        4*C5vNC*this->mRes*this->mn*this->q[0]*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] - 
        4*C5vNC*pow(p0,2)*this->q[0]*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] + 
        8*C5vNC*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] - 
        2*C5vNC*pow(this->q[0],3)*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] - 
        C5vNC*this->q[0]*pow(this->q[1],4)*this->kg[2] * this->kg[2] - 
        4*C5vNC*this->mRes_2*p0*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] + 
        4*C5vNC*this->mRes_2*this->q[0]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] - 
        2*C5vNC*this->mRes*this->mn*this->q[0]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] - 
        4*C5vNC*pow(p0,2)*this->q[0]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] + 
        8*C5vNC*p0*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] - 
        2*C5vNC*pow(this->q[0],3)*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] - 
        2*C5vNC*this->q[0]*this->q[1] * this->q[1]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] - 
        C5vNC*this->q[0]*pow(this->q[3],4)*this->kg[2] * this->kg[2] + 
        2*C5vNC*this->mRes_2*this->q[0]*this->q[1]*this->kg[1]*this->kg[2] * this->kg[2] + 
        4*C5vNC*p0*this->q[0] * this->q[0]*this->q[1]*this->kg[1]*this->kg[2] * this->kg[2] - 
        4*C5vNC*pow(this->q[0],3)*this->q[1]*this->kg[1]*this->kg[2] * this->kg[2] - 
        4*C5vNC*p0*pow(this->q[1],3)*this->kg[1]*this->kg[2] * this->kg[2] + 
        4*C5vNC*this->q[0]*pow(this->q[1],3)*this->kg[1]*this->kg[2] * this->kg[2] - 
        4*C5vNC*p0*this->q[1]*this->q[3] * this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] + 
        4*C5vNC*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] + 
        std::complex<double>(0,2)*C5a*this->mn2*this->q[1]*this->q[3]*pow(this->kg[2],3) + 
        (std::complex<double>(0,-2)*C5a*this->mn2*
            ((this->mRes*(3*this->mRes + this->mn) + 2*p0*this->q[0] - this->q[0] * this->q[0])*this->q[1] + 
              (this->q[1] - this->q[3])*(this->q[1] + this->q[3])*this->kg[1])*this->kg[2] + 
           C5vNC*this->q[3]*(4*pow(this->mRes,3)*this->mn*this->q[0] + 
              2*this->mRes*this->mn*(p0 - this->q[0])*
               (4*p0*this->q[0] - this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3] + 
                 2*this->q[1]*this->kg[1]) + 4*(p0 - this->q[0])*
               (this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3])*this->kg[2] * this->kg[2] + 
              this->mRes_2*(20*pow(p0,2)*this->q[0] - 
                 2*p0*(5*this->q[0] * this->q[0] + 
                    3*(this->q[1] * this->q[1] + this->q[3] * this->q[3] - 2*this->q[1]*this->kg[1])) + 
                 this->q[0]*(this->q[0] * this->q[0] + 3*this->q[1] * this->q[1] + 3*this->q[3] * this->q[3] - 
                    4*this->q[1]*this->kg[1] + 2*this->kg[2] * this->kg[2]))))*this->kg[3] + 
        2*C3vNC*this->mn*(2*pow(this->mRes,3)*
            (this->q[0]*(this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] - 2*this->q[1]*this->kg[1] - 
                 this->kg[2] * this->kg[2] - 2*this->q[3]*this->kg[3]) + 
              4*p0*(this->q[0] * this->q[0] - this->q[1]*this->kg[1] - this->q[3]*this->kg[3])) + 
           2*this->mRes_2*this->mn*
            (pow(this->q[0],3) + this->q[0]*
               (this->q[1] * this->q[1] + this->q[3] * this->q[3] - 2*this->q[1]*this->kg[1] - this->kg[2] * this->kg[2] - 
                 2*this->q[3]*this->kg[3]) + 4*p0*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3])) + 
           2*this->mn*(p0 - this->q[0])*(4*pow(p0,2)*this->q[0] * this->q[0] + 
              2*pow(this->q[0],4) + pow(this->q[1],3)*this->kg[1] + this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 
              this->q[1] * this->q[1]*this->kg[1] * this->kg[1] - this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
              this->q[1] * this->q[1]*this->kg[2] * this->kg[2] - 2*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] + 
              this->q[3]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1])*this->kg[3] + 
              2*p0*this->q[0]*(-3*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] + 
                 2*this->q[1]*this->kg[1] - this->kg[2] * this->kg[2] + 2*this->q[3]*this->kg[3]) - 
              this->q[0] * this->q[0]*(this->q[1] * this->q[1] + 3*this->q[1]*this->kg[1] - this->kg[2] * this->kg[2] + 
                 3*this->q[3]*this->kg[3])) + 
           this->mRes*(-4*pow(p0,2)*this->q[0]*
               (this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] - 2*this->q[1]*this->kg[1] - 
                 this->kg[2] * this->kg[2] - 2*this->q[3]*this->kg[3]) + 
              4*p0*(2*pow(this->q[0],4) - pow(this->q[3]*this->kg[1] - this->q[1]*this->kg[3],2) + 
                 2*this->q[0] * this->q[0]*
                  (this->q[1] * this->q[1] + this->q[3] * this->q[3] - 2*this->q[1]*this->kg[1] - this->kg[2] * this->kg[2] - 
                    2*this->q[3]*this->kg[3])) + 
              this->q[0]*(-3*pow(this->q[0],4) + pow(this->q[1],4) + 
                 this->q[1] * this->q[1]*(2*this->q[3] * this->q[3] - 4*this->kg[1] * this->kg[1] - 
                    3*this->kg[2] * this->kg[2]) + 
                 this->q[3] * this->q[3]*(this->q[3] * this->q[3] + 4*this->kg[1] * this->kg[1] + 
                    this->kg[2] * this->kg[2]) - 8*this->q[1]*this->q[3]*this->kg[1]*this->kg[3] + 
                 this->q[0] * this->q[0]*(-2*this->q[1] * this->q[1] - 6*this->q[3] * this->q[3] + 8*this->q[1]*this->kg[1] + 
                    3*this->kg[2] * this->kg[2] + 8*this->q[3]*this->kg[3])))) + 
        C4vNC*this->mRes*(4*this->mRes_2*this->mn*this->q[0]*
            (2*(p0 - this->q[0])*this->q[0] + this->q[1]*(this->q[1] + this->kg[1]) + this->q[3]*(this->q[3] + this->kg[3])) + 
           2*this->mn*(p0 - this->q[0])*(4*pow(p0,2)*this->q[0] * this->q[0] + 
              2*pow(this->q[0],4) + pow(this->q[1],3)*this->kg[1] + this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 
              this->q[1] * this->q[1]*this->kg[1] * this->kg[1] - this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
              this->q[1] * this->q[1]*this->kg[2] * this->kg[2] - 2*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] + 
              this->q[3]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1])*this->kg[3] + 
              2*p0*this->q[0]*(-3*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] + 
                 2*this->q[1]*this->kg[1] - this->kg[2] * this->kg[2] + 2*this->q[3]*this->kg[3]) - 
              this->q[0] * this->q[0]*(this->q[1] * this->q[1] + 3*this->q[1]*this->kg[1] - this->kg[2] * this->kg[2] + 
                 3*this->q[3]*this->kg[3])) + this->mRes*
            (16*pow(p0,3)*this->q[0] * this->q[0] + 
              4*pow(p0,2)*this->q[0]*
               (-7*this->q[0] * this->q[0] + 2*this->q[1] * this->q[1] + 2*this->q[3] * this->q[3] + 
                 5*this->q[1]*this->kg[1] - this->kg[2] * this->kg[2] + 5*this->q[3]*this->kg[3]) - 
              this->q[0]*(2*pow(this->q[0],4) - pow(this->q[1],4) + pow(this->q[1],3)*this->kg[1] + 
                 this->q[0] * this->q[0]*(this->q[1] * this->q[1] + 3*this->q[3] * this->q[3] - 5*this->q[1]*this->kg[1] - 
                    2*this->kg[2] * this->kg[2] - 5*this->q[3]*this->kg[3]) + 
                 this->q[1] * this->q[1]*(-2*this->q[3] * this->q[3] + 2*this->kg[1] * this->kg[1] + 
                    4*this->kg[2] * this->kg[2] + this->q[3]*this->kg[3]) + 
                 this->q[1]*this->kg[1]*(this->q[3] * this->q[3] - 2*this->kg[2] * this->kg[2] + 4*this->q[3]*this->kg[3]) - 
                 this->q[3]*(pow(this->q[3],3) + 2*this->q[3]*(this->kg[1] - this->kg[2])*(this->kg[1] + this->kg[2]) - 
                    this->q[3] * this->q[3]*this->kg[3] + 2*this->kg[2] * this->kg[2]*this->kg[3])) + 
              2*p0*(7*pow(this->q[0],4) + 3*pow(this->q[1],3)*this->kg[1] + 
                 3*this->q[1]*this->q[3]*this->kg[1]*(this->q[3] + 2*this->kg[3]) + 
                 this->q[3] * this->q[3]*(-3*this->kg[1] * this->kg[1] - 4*this->kg[2] * this->kg[2] + 
                    3*this->q[3]*this->kg[3]) - 
                 this->q[0] * this->q[0]*(3*this->q[1] * this->q[1] - this->q[3] * this->q[3] + 11*this->q[1]*this->kg[1] + 
                    this->kg[2] * this->kg[2] + 11*this->q[3]*this->kg[3]) + 
                 this->q[1] * this->q[1]*(4*this->kg[1] * this->kg[1] + 3*this->q[3]*this->kg[3] + this->kg[3] * this->kg[3])))))  
- 2*C3vNC*this->mn*(C4v*this->mRes*(-4*this->mRes_2*this->mn*this->q[0]*
            (-2*p0*this->q[0] + this->q[0] * this->q[0] - this->q[1]*this->kg[1] + this->kg[2] * this->kg[2] - this->q[3]*this->kg[3]) + 
           2*this->mn*(p0 - this->q[0])*(4*pow(p0,2)*this->q[0] * this->q[0] + 
              2*pow(this->q[0],4) + pow(this->q[1],3)*this->kg[1] + this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 
              this->q[1] * this->q[1]*this->kg[1] * this->kg[1] - this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
              this->q[1] * this->q[1]*this->kg[2] * this->kg[2] - 2*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] + 
              this->q[3]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1])*this->kg[3] + 
              2*p0*this->q[0]*(-3*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] + 
                 2*this->q[1]*this->kg[1] - this->kg[2] * this->kg[2] + 2*this->q[3]*this->kg[3]) - 
              this->q[0] * this->q[0]*(this->q[1] * this->q[1] + 3*this->q[1]*this->kg[1] - this->kg[2] * this->kg[2] + 
                 3*this->q[3]*this->kg[3])) + this->mRes*
            (16*pow(p0,3)*this->q[0] * this->q[0] + 
              4*pow(p0,2)*this->q[0]*
               (-6*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] + 5*this->q[1]*this->kg[1] - 
                 2*this->kg[2] * this->kg[2] + 5*this->q[3]*this->kg[3]) + 
              2*p0*(5*pow(this->q[0],4) + pow(this->q[1],3)*this->kg[1] + 
                 this->q[0] * this->q[0]*(-(this->q[1]*(this->q[1] + 9*this->kg[1])) + this->kg[2] * this->kg[2] + 
                    3*this->q[3]*(this->q[3] - 3*this->kg[3])) + this->q[1]*this->q[3]*this->kg[1]*(this->q[3] + 6*this->kg[3]) + 
                 this->q[3] * this->q[3]*
                  (-3*this->kg[1] * this->kg[1] - 4*this->kg[2] * this->kg[2] + this->q[3]*this->kg[3]) + 
                 this->q[1] * this->q[1]*(4*this->kg[1] * this->kg[1] + this->kg[3]*(this->q[3] + this->kg[3]))) + 
              this->q[0]*(-pow(this->q[0],4) + pow(this->q[1],3)*this->kg[1] + 
                 this->q[1]*this->kg[1]*(this->q[3] * this->q[3] + 2*this->kg[2] * this->kg[2] - 4*this->q[3]*this->kg[3]) + 
                 this->q[3]*(2*this->q[3]*this->kg[1] * this->kg[1] - this->q[3]*this->kg[2] * this->kg[2] + 
                    this->q[3] * this->q[3]*this->kg[3] + 2*this->kg[2] * this->kg[2]*this->kg[3]) + 
                 this->q[0] * this->q[0]*(-2*this->q[1] * this->q[1] + 3*this->q[1]*this->kg[1] + this->kg[2] * this->kg[2] + 
                    3*this->q[3]*(-this->q[3] + this->kg[3])) + 
                 this->q[1] * this->q[1]*(-this->kg[1] * this->kg[1] - 2*this->kg[2] * this->kg[2] + 
                    this->kg[3]*(this->q[3] + this->kg[3]))))) + 
        C5v*(-((4*pow(p0,2)*this->q[0] + 
                3*this->q[0]*(this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3]) + 
                4*p0*(-2*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3]))*
              this->kg[2] * this->kg[2]*(2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3])) + 
           4*pow(this->mRes,3)*this->mn*this->q[0]*
            (2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->kg[2] * this->kg[2] + this->q[3]*this->kg[3]) + 
           2*this->mRes*this->mn*(4*pow(p0,3)*this->q[0] * this->q[0] + 
              2*pow(p0,2)*this->q[0]*
               (-5*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1] + 
                 this->kg[2] * this->kg[2] + 2*this->q[3]*this->kg[3]) + 
              p0*(8*pow(this->q[0],4) + pow(this->q[1],3)*this->kg[1] + 
                 this->q[1] * this->q[1]*(this->q[3] - this->kg[3])*this->kg[3] + this->q[1]*this->q[3]*this->kg[1]*(this->q[3] + 2*this->kg[3]) + 
                 this->q[3] * this->q[3]*(-this->kg[1] * this->kg[1] + this->q[3]*this->kg[3]) - 
                 this->q[0] * this->q[0]*(2*this->q[1] * this->q[1] + 2*this->q[3] * this->q[3] + 7*this->q[1]*this->kg[1] + 
                    this->kg[2] * this->kg[2] + 7*this->q[3]*this->kg[3])) + 
              this->q[0]*(-2*pow(this->q[0],4) - pow(this->q[1],3)*this->kg[1] + 
                 this->q[1] * this->q[1]*this->kg[3]*(-this->q[3] + this->kg[3]) + 
                 3*this->q[0] * this->q[0]*(this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) + 
                 this->q[3]*(this->q[3]*this->kg[1] * this->kg[1] - this->q[3] * this->q[3]*this->kg[3] + 
                    this->kg[2] * this->kg[2]*this->kg[3]) + 
                 this->q[1]*this->kg[1]*(this->kg[2] * this->kg[2] - this->q[3]*(this->q[3] + 2*this->kg[3])))) + 
           this->mRes_2*(16*pow(p0,3)*this->q[0] * this->q[0] + 
              4*pow(p0,2)*this->q[0]*
               (-6*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] + 5*this->q[1]*this->kg[1] + 
                 2*this->kg[2] * this->kg[2] + 5*this->q[3]*this->kg[3]) + 
              2*p0*(5*pow(this->q[0],4) + pow(this->q[1],3)*this->kg[1] + 
                 this->q[0] * this->q[0]*(this->q[1] * this->q[1] - 9*this->q[1]*this->kg[1] + this->kg[2] * this->kg[2] + 
                    3*this->q[3]*(this->q[3] - 3*this->kg[3])) + this->q[1]*this->q[3]*this->kg[1]*(this->q[3] + 6*this->kg[3]) + 
                 this->q[3] * this->q[3]*(-3*this->kg[1] * this->kg[1] - 2*this->kg[2] * this->kg[2] + this->q[3]*this->kg[3]) + 
                 this->q[1] * this->q[1]*(2*this->kg[1] * this->kg[1] + (this->q[3] - this->kg[3])*this->kg[3])) + 
              this->q[0]*(-pow(this->q[0],4) + pow(this->q[1],3)*this->kg[1] + this->q[1]*this->q[3]*this->kg[1]*(this->q[3] - 4*this->kg[3]) + 
                 this->q[1] * this->q[1]*(-2*this->kg[1] * this->kg[1] + 3*this->kg[2] * this->kg[2] + this->q[3]*this->kg[3]) + 
                 this->q[3] * this->q[3]*(2*this->kg[1] * this->kg[1] + 5*this->kg[2] * this->kg[2] + this->q[3]*this->kg[3]) - 
                 this->q[0] * this->q[0]*(this->q[1] * this->q[1] - 3*this->q[1]*this->kg[1] + 
                    3*(this->q[3] * this->q[3] + this->kg[2] * this->kg[2] - this->q[3]*this->kg[3])))))))/
  (24.*this->mRes_2*pow(this->mn,5));
    
	tr[2][3] = (std::complex<double>(0,-2)*C5a*this->mn2*
     (-2*C3v*this->mn*(8*pow(this->mRes,3)*this->mn*this->kg[1] + 
          (this->q[3] + this->kg[3])*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3])*
           (2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) - 
          this->mRes_2*(6*p0*this->q[0]*(this->q[1] - this->kg[1]) + 8*pow(p0,2)*this->kg[1] + 
             this->q[1]*(this->q[1] - 3*this->kg[1])*this->kg[1] + this->q[0] * this->q[0]*(this->q[1] + this->kg[1]) - 
             3*this->q[1]*this->kg[2] * this->kg[2] + this->q[3]*(this->q[1] - 3*this->kg[1])*this->kg[3]) + 
          this->mRes*this->mn*(this->q[1] * this->q[1]*this->kg[1] + 2*this->q[3] * this->q[3]*this->kg[1] + 
             3*this->q[1]*this->kg[1] * this->kg[1] + 2*p0*this->q[0]*(this->q[1] + this->kg[1]) - 
             this->q[0] * this->q[0]*(3*this->q[1] + this->kg[1]) + this->q[1]*this->kg[2] * this->kg[2] - this->q[1]*this->q[3]*this->kg[3] + 
             3*this->q[3]*this->kg[1]*this->kg[3])) + C5v*this->mRes*
        (-(pow(this->q[0],4)*this->q[1]) + this->q[0] * this->q[0]*this->q[1]*this->q[3] * this->q[3] + 
          this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[1] + this->q[0] * this->q[0]*this->q[1]*this->kg[1] * this->kg[1] - 
          2*this->q[1]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - this->q[1] * this->q[1]*pow(this->kg[1],3) + 
          this->q[3] * this->q[3]*pow(this->kg[1],3) - this->q[0] * this->q[0]*this->q[1]*this->kg[2] * this->kg[2] - 
          this->q[1]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] + this->q[1] * this->q[1]*this->kg[1]*this->kg[2] * this->kg[2] + 
          this->q[3] * this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] + 
          this->q[3]*(this->kg[1]*(this->q[0] * this->q[0] + this->q[1] * this->q[1] - this->q[3] * this->q[3] - 2*this->q[1]*this->kg[1]) + 
             this->q[1]*this->kg[2] * this->kg[2])*this->kg[3] + 
          2*this->mRes_2*(2*p0*this->q[0]*(this->q[1] - this->kg[1]) + 
             this->q[0] * this->q[0]*(-this->q[1] + this->kg[1]) + 
             this->q[1]*((this->q[1] - this->kg[1])*this->kg[1] + this->kg[2] * this->kg[2]) + this->q[3]*(this->q[1] - this->kg[1])*this->kg[3]) + 
          2*p0*this->q[0]*(-(this->q[3] * this->q[3]*this->kg[1]) + 
             this->q[1]*(this->q[0] * this->q[0] - this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + 
             this->q[3]*(this->q[1] - this->kg[1])*this->kg[3]) + 
          2*this->mRes*this->mn*(-(this->q[0] * this->q[0]*this->kg[1]) + this->q[1] * this->q[1]*this->kg[1] + 
             2*p0*this->q[0]*(this->q[1] + this->kg[1]) + this->q[3]*(this->q[1] + this->kg[1])*this->kg[3] - this->q[1]*this->kg[3] * this->kg[3])))  
+ C4v*this->mRes*(std::complex<double>(0,-2)*C5a*this->mn2*
        (-((this->q[3] + this->kg[3])*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3])*
             (2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3])) - 
          2*this->mRes_2*(this->q[0] * this->q[0]*(this->q[1] - this->kg[1]) - this->q[1] * this->q[1]*this->kg[1] + 
             this->q[1]*this->kg[1] * this->kg[1] + 2*p0*this->q[0]*(-this->q[1] + this->kg[1]) + this->q[1]*this->kg[2] * this->kg[2] - 
             this->q[1]*this->q[3]*this->kg[3] + this->q[3]*this->kg[1]*this->kg[3]) + 
          2*this->mRes*this->mn*(2*p0*this->q[0]*(this->q[1] + this->kg[1]) - this->q[0] * this->q[0]*(this->q[1] + this->kg[1]) + 
             this->q[1]*this->kg[1]*(this->q[1] + this->kg[1]) - this->q[1]*this->kg[2] * this->kg[2] + this->q[3]*(this->q[1] + this->kg[1])*this->kg[3])) - 
       this->kg[2]*(4*C4vNC*this->mRes*(2*this->mn*(p0 - this->q[0])*
              (this->q[0] * this->q[0]*(this->q[3] - this->kg[3]) + 2*p0*this->q[0]*this->kg[3] + 
                this->q[1]*(-(this->q[3]*this->kg[1]) + this->q[1]*this->kg[3])) + 
             this->mRes*(2*pow(this->q[0],3)*this->q[3] - 2*p0*this->q[0] * this->q[0]*this->kg[3] + 
                2*p0*this->q[1]*(-(this->q[3]*this->kg[1]) + this->q[1]*this->kg[3]) + 
                this->q[0]*(4*pow(p0,2)*this->kg[3] + this->q[1] * this->q[1]*this->kg[3] - 
                   this->q[3]*this->kg[3]*(this->q[3] + this->kg[3]) - this->q[1]*this->kg[1]*(2*this->q[3] + this->kg[3])))) + 
          C5vNC*(8*this->mRes*this->mn*(p0 - this->q[0])*
              (2*p0*this->q[0]*this->kg[3] + this->q[0] * this->q[0]*(-this->q[3] + this->kg[3]) + 
                this->q[1]*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3])) - 
             (2*p0*this->q[0]*this->q[3] + this->q[0] * this->q[0]*(-this->q[3] + this->kg[3]) + 
                this->q[1]*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3]))*
              (4*pow(p0,2)*this->q[0] + 
                this->q[0]*(3*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] - 
                   4*this->q[1]*this->kg[1] - 4*this->q[3]*this->kg[3]) + 
                4*p0*(-2*this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3])) + 
             4*this->mRes_2*(4*pow(p0,2)*this->q[0]*this->kg[3] + 
                this->q[0]*(this->q[3] - this->kg[3])*(-this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) + 
                2*p0*(this->q[0] * this->q[0]*(-this->q[3] + this->kg[3]) + this->q[1]*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3]))))  
+ 2*C3vNC*this->mn*(4*this->mRes_2*this->mn*this->q[0]*this->kg[3] + 
             2*this->mn*(p0 - this->q[0])*
              (this->q[0] * this->q[0]*(this->q[3] - this->kg[3]) + 2*p0*this->q[0]*this->kg[3] + 
                this->q[1]*(-(this->q[3]*this->kg[1]) + this->q[1]*this->kg[3])) + 
             this->mRes*(8*pow(p0,2)*this->q[0]*this->kg[3] + 
                this->q[0]*(this->q[0] * this->q[0]*(6*this->q[3] - this->kg[3]) + 3*this->q[1] * this->q[1]*this->kg[3] - 
                   2*this->q[1]*this->kg[1]*(3*this->q[3] + this->kg[3]) - this->q[3]*this->kg[3]*(3*this->q[3] + 2*this->kg[3])) - 
                2*p0*(this->q[0] * this->q[0]*(this->q[3] + this->kg[3]) + this->q[1]*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3]))))))  
+ this->kg[2]*(C4vNC*this->mRes*(4*C3v*this->mn*(this->mRes*
              ((p0 - 3*this->q[0])*this->q[0] * this->q[0]*this->q[3] + (p0 + 3*this->q[0])*this->q[1]*this->q[3]*this->kg[1] + 
                (-2*pow(p0,2)*this->q[0] - 
                   p0*(this->q[0] * this->q[0] + this->q[1] * this->q[1]) + 
                   this->q[0]*(this->q[0] * this->q[0] + this->q[3] * this->q[3] + this->q[1]*(-2*this->q[1] + this->kg[1])))*
                 this->kg[3] + this->q[0]*this->q[3]*this->kg[3] * this->kg[3]) - 
             this->mn*(p0 - this->q[0])*(this->q[0] * this->q[0]*(this->q[3] - this->kg[3]) + 2*p0*this->q[0]*this->kg[3] + 
                this->q[1]*(-(this->q[3]*this->kg[1]) + this->q[1]*this->kg[3]))) + 
          C5v*(8*this->mRes*this->mn*(p0 - this->q[0])*
              (this->q[0] * this->q[0]*(this->q[3] - this->kg[3]) + 2*p0*this->q[0]*this->kg[3] + 
                this->q[1]*(-(this->q[3]*this->kg[1]) + this->q[1]*this->kg[3])) + 
             4*this->mRes_2*(2*p0 + this->q[0])*
              (this->q[0] * this->q[0]*(this->q[3] - this->kg[3]) + 2*p0*this->q[0]*this->kg[3] + 
                this->q[1]*(-(this->q[3]*this->kg[1]) + this->q[1]*this->kg[3])) + 
             (2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3])*
              (pow(this->q[0],3)*(5*this->q[3] - 3*this->kg[3]) + 
                4*pow(p0,2)*this->q[0]*(this->q[3] - this->kg[3]) - 
                this->q[0]*(4*this->q[1]*this->q[3]*this->kg[1] + this->q[1] * this->q[1]*(this->q[3] - 3*this->kg[3]) + 
                   this->q[3] * this->q[3]*(this->q[3] + this->kg[3])) + 
                p0*(8*this->q[0] * this->q[0]*(-this->q[3] + this->kg[3]) + 4*this->q[1]*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3])))) 
) - 2*C3v*this->mn*(2*C3vNC*this->mn*(2*(-2*this->mRes + this->mn)*(p0 - this->q[0])*this->q[3]*
              (this->q[0] * this->q[0] - this->q[1]*this->kg[1]) + 
             (2*pow(this->mRes,3)*this->q[0] + 2*this->mRes_2*this->mn*this->q[0] + 
                2*this->mn*(p0 - this->q[0])*
                 (2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1] * this->q[1]) - 
                this->mRes*(4*pow(p0,2)*this->q[0] + 
                   4*p0*(-2*this->q[0] * this->q[0] + this->q[1] * this->q[1]) + 
                   this->q[0]*(3*this->q[0] * this->q[0] - 3*this->q[1] * this->q[1] + this->q[3] * this->q[3])))*this->kg[3])  
+ C5vNC*(2*this->mRes_2*(-((p0 + 2*this->q[0])*this->q[3]*(this->q[0] * this->q[0] - this->q[1]*this->kg[1])) + 
                (2*pow(p0,2)*this->q[0] + 
                   p0*(3*this->q[0] * this->q[0] - this->q[1] * this->q[1]) + 
                   this->q[0]*(2*this->q[0] * this->q[0] + this->q[3] * this->q[3] - this->q[1]*(this->q[1] + this->kg[1])))*this->kg[3]  
- this->q[0]*this->q[3]*this->kg[3] * this->kg[3]) - (2*p0*this->q[0]*this->q[3] + this->q[0] * this->q[0]*(-this->q[3] + this->kg[3]) + 
                this->q[1]*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3]))*
              (4*pow(p0,2)*this->q[0] + 
                this->q[0]*(3*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] - 
                   4*this->q[1]*this->kg[1] - 4*this->q[3]*this->kg[3]) + 
                4*p0*(-2*this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3])) + 
             2*this->mRes*this->mn*(2*pow(p0,2)*this->q[0]*this->kg[3] + 
                2*this->q[0]*(this->q[0] * this->q[0]*(this->q[3] - this->kg[3]) + this->q[1]*(-(this->q[3]*this->kg[1]) + this->q[1]*this->kg[3])) - 
                p0*(this->q[0] * this->q[0]*(3*this->q[3] + this->kg[3]) + this->q[1]*(-(this->q[3]*this->kg[1]) + this->q[1]*this->kg[3])))) 
) + C5v*(2*C3vNC*this->mn*(4*pow(this->mRes,3)*this->mn*this->q[0]*this->kg[3] + 
             this->mRes_2*(2*(p0 + 2*this->q[0])*this->q[3]*(this->q[0] * this->q[0] - this->q[1]*this->kg[1]) + 
                (8*pow(p0,2)*this->q[0] + 
                   2*p0*(this->q[0] * this->q[0] + this->q[1] * this->q[1]) - 
                   this->q[0]*(3*this->q[0] * this->q[0] - 3*this->q[1] * this->q[1] + this->q[3] * this->q[3]))*this->kg[3])  
+ 2*this->mRes*this->mn*(p0*this->q[0] * this->q[0]*(3*this->q[3] - this->kg[3]) + 2*pow(p0,2)*this->q[0]*this->kg[3] + 
                p0*this->q[1]*(-(this->q[3]*this->kg[1]) + this->q[1]*this->kg[3]) + 
                this->q[0]*(-2*this->q[0] * this->q[0]*this->q[3] - this->q[1] * this->q[1]*this->kg[3] + 
                   this->q[3]*this->kg[3]*(this->q[3] + this->kg[3]) + this->q[1]*this->kg[1]*(2*this->q[3] + this->kg[3]))) + 
             (2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3])*
              (pow(this->q[0],3)*(5*this->q[3] - 3*this->kg[3]) + 
                4*pow(p0,2)*this->q[0]*(this->q[3] - this->kg[3]) - 
                this->q[0]*(4*this->q[1]*this->q[3]*this->kg[1] + this->q[1] * this->q[1]*(this->q[3] - 3*this->kg[3]) + 
                   this->q[3] * this->q[3]*(this->q[3] + this->kg[3])) + 
                p0*(8*this->q[0] * this->q[0]*(-this->q[3] + this->kg[3]) + 4*this->q[1]*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3]))))  
+ C5vNC*(8*this->mn*(p0 - this->q[0])*(this->q[3]*
                 (4*pow(p0,2)*this->q[0] * this->q[0] + pow(this->q[0],4) - 
                   this->q[0] * this->q[0]*this->q[1]*(this->q[1] + 2*this->kg[1]) - 
                   4*p0*(pow(this->q[0],3) - this->q[0]*this->q[1]*this->kg[1]) + 
                   this->q[1] * this->q[1]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])) + 
                (this->q[0] * this->q[0] - this->q[1] * this->q[1] + this->q[3] * this->q[3])*
                 (2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1]*this->kg[1])*this->kg[3] + 
                this->q[0] * this->q[0]*this->q[3]*this->kg[3] * this->kg[3]) + 
             8*this->mRes_2*this->mn*(p0 - this->q[0])*
              (2*p0*this->q[0]*this->kg[3] + this->q[0] * this->q[0]*(-this->q[3] + this->kg[3]) + 
                this->q[1]*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3])) + 
             8*pow(this->mRes,3)*p0*
              (2*p0*this->q[0]*this->kg[3] + 2*this->q[0] * this->q[0]*(-this->q[3] + this->kg[3]) + 
                this->q[1]*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3])) + 
             this->mRes*(8*pow(p0,3)*this->q[0] * this->q[0]*(6*this->q[3] - this->kg[3]) - 
                4*pow(p0,2)*this->q[0]*
                 (2*this->q[0] * this->q[0]*(7*this->q[3] - 4*this->kg[3]) + 5*this->q[1] * this->q[1]*this->kg[3] + 
                   this->q[1]*this->kg[1]*(-10*this->q[3] + this->kg[3]) + this->q[3]*this->kg[3]*(-5*this->q[3] + this->kg[3])) + 
                this->q[0]*(this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3])*this->kg[3]*
                 (this->q[1]*(-this->q[1] + this->kg[1]) + this->q[3]*(-this->q[3] + this->kg[3])) + 
                2*p0*(pow(this->q[0],4)*(8*this->q[3] - 7*this->kg[3]) + 
                   4*this->q[1]*(this->q[1]*this->q[3]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) - 
                      this->q[1] * this->q[1]*this->kg[1]*this->kg[3] + this->q[3] * this->q[3]*this->kg[1]*this->kg[3]) + 
                   this->q[0] * this->q[0]*
                    (6*this->q[1]*this->kg[1]*(-2*this->q[3] + this->kg[3]) + this->q[1] * this->q[1]*(-4*this->q[3] + 5*this->kg[3]) + 
                      this->q[3]*this->kg[3]*(-7*this->q[3] + 6*this->kg[3]))))))))/
  (24.*this->mRes_2*pow(this->mn,5));

	
    
	tr[3][0] = -(-32*C4v*C4vNC*pow(this->mRes,3)*pow(p0,3)*this->q[0]*this->q[3] - 
     32*C4vNC*C5v*pow(this->mRes,3)*pow(p0,3)*this->q[0]*this->q[3] - 
     32*C4v*C5vNC*pow(this->mRes,3)*pow(p0,3)*this->q[0]*this->q[3] - 
     32*C5v*C5vNC*pow(this->mRes,3)*pow(p0,3)*this->q[0]*this->q[3] - 
     32*C4v*C4vNC*this->mRes_2*this->mn*pow(p0,3)*this->q[0]*this->q[3] - 
     32*C4vNC*C5v*this->mRes_2*this->mn*pow(p0,3)*this->q[0]*this->q[3] - 
     32*C4v*C5vNC*this->mRes_2*this->mn*pow(p0,3)*this->q[0]*this->q[3] - 
     32*C5v*C5vNC*this->mRes_2*this->mn*pow(p0,3)*this->q[0]*this->q[3] + 
     64*C4v*C4vNC*pow(this->mRes,3)*pow(p0,2)*this->q[0] * this->q[0]*this->q[3] + 
     32*C4vNC*C5v*pow(this->mRes,3)*pow(p0,2)*this->q[0] * this->q[0]*this->q[3] + 
     32*C4v*C5vNC*pow(this->mRes,3)*pow(p0,2)*this->q[0] * this->q[0]*this->q[3] + 
     96*C4v*C4vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[0] * this->q[0]*this->q[3] + 
     64*C4vNC*C5v*this->mRes_2*this->mn*pow(p0,2)*this->q[0] * this->q[0]*this->q[3] + 
     64*C4v*C5vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[0] * this->q[0]*this->q[3] + 
     32*C5v*C5vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[0] * this->q[0]*this->q[3] - 
     24*C4v*C4vNC*pow(this->mRes,3)*p0*pow(this->q[0],3)*this->q[3] - 
     16*C4vNC*C5v*pow(this->mRes,3)*p0*pow(this->q[0],3)*this->q[3] + 
     8*C4v*C5vNC*pow(this->mRes,3)*p0*pow(this->q[0],3)*this->q[3] - 
     16*C5v*C5vNC*pow(this->mRes,3)*p0*pow(this->q[0],3)*this->q[3] - 
     88*C4v*C4vNC*this->mRes_2*this->mn*p0*pow(this->q[0],3)*this->q[3] - 
     40*C4vNC*C5v*this->mRes_2*this->mn*p0*pow(this->q[0],3)*this->q[3] - 
     24*C4v*C5vNC*this->mRes_2*this->mn*p0*pow(this->q[0],3)*this->q[3] - 
     8*C5v*C5vNC*this->mRes_2*this->mn*p0*pow(this->q[0],3)*this->q[3] + 
     8*C4vNC*C5v*this->mRes*pow(p0,3)*pow(this->q[0],3)*this->q[3] + 
     8*C5v*C5vNC*this->mRes*pow(p0,3)*pow(this->q[0],3)*this->q[3] + 
     4*C4vNC*C5v*pow(this->mRes,3)*pow(this->q[0],4)*this->q[3] + 
     4*C4v*C5vNC*pow(this->mRes,3)*pow(this->q[0],4)*this->q[3] + 
     24*C4v*C4vNC*this->mRes_2*this->mn*pow(this->q[0],4)*this->q[3] + 
     8*C4vNC*C5v*this->mRes_2*this->mn*pow(this->q[0],4)*this->q[3] - 
     8*C4v*C5vNC*this->mRes_2*this->mn*pow(this->q[0],4)*this->q[3] + 
     8*C5v*C5vNC*this->mRes_2*this->mn*pow(this->q[0],4)*this->q[3] - 
     20*C4vNC*C5v*this->mRes*pow(p0,2)*pow(this->q[0],4)*this->q[3] - 
     4*C4v*C5vNC*this->mRes*pow(p0,2)*pow(this->q[0],4)*this->q[3] - 
     32*C5v*C5vNC*this->mRes*pow(p0,2)*pow(this->q[0],4)*this->q[3] - 
     16*C5v*C5vNC*this->mn*pow(p0,2)*pow(this->q[0],4)*this->q[3] + 
     14*C4vNC*C5v*this->mRes*p0*pow(this->q[0],5)*this->q[3] + 
     8*C4v*C5vNC*this->mRes*p0*pow(this->q[0],5)*this->q[3] + 
     14*C5v*C5vNC*this->mRes*p0*pow(this->q[0],5)*this->q[3] + 
     24*C5v*C5vNC*this->mn*p0*pow(this->q[0],5)*this->q[3] - 
     3*C4vNC*C5v*this->mRes*pow(this->q[0],6)*this->q[3] - 3*C4v*C5vNC*this->mRes*pow(this->q[0],6)*this->q[3] - 
     8*C5v*C5vNC*this->mn*pow(this->q[0],6)*this->q[3] + 
     8*C4v*C4vNC*pow(this->mRes,3)*p0*this->q[0]*this->q[1] * this->q[1]*this->q[3] + 
     8*C4vNC*C5v*pow(this->mRes,3)*p0*this->q[0]*this->q[1] * this->q[1]*this->q[3] - 
     16*C4v*C5vNC*pow(this->mRes,3)*p0*this->q[0]*this->q[1] * this->q[1]*this->q[3] - 
     16*C5v*C5vNC*pow(this->mRes,3)*p0*this->q[0]*this->q[1] * this->q[1]*this->q[3] + 
     8*C4v*C4vNC*this->mRes_2*this->mn*p0*this->q[0]*this->q[1] * this->q[1]*this->q[3] + 
     8*C4vNC*C5v*this->mRes_2*this->mn*p0*this->q[0]*this->q[1] * this->q[1]*this->q[3] - 
     8*C4v*C5vNC*this->mRes_2*this->mn*p0*this->q[0]*this->q[1] * this->q[1]*this->q[3] - 
     8*C5v*C5vNC*this->mRes_2*this->mn*p0*this->q[0]*this->q[1] * this->q[1]*this->q[3] + 
     8*C4v*C5vNC*this->mRes*pow(p0,3)*this->q[0]*this->q[1] * this->q[1]*this->q[3] + 
     8*C5v*C5vNC*this->mRes*pow(p0,3)*this->q[0]*this->q[1] * this->q[1]*this->q[3] + 
     4*C4vNC*C5v*pow(this->mRes,3)*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3] + 
     4*C4v*C5vNC*pow(this->mRes,3)*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3] - 
     8*C4v*C4vNC*this->mRes_2*this->mn*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3] - 
     8*C4vNC*C5v*this->mRes_2*this->mn*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3] + 
     8*C4v*C5vNC*this->mRes_2*this->mn*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3] + 
     8*C5v*C5vNC*this->mRes_2*this->mn*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3] - 
     4*C4vNC*C5v*this->mRes*pow(p0,2)*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3] - 
     20*C4v*C5vNC*this->mRes*pow(p0,2)*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3] - 
     32*C5v*C5vNC*this->mRes*pow(p0,2)*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3] - 
     16*C5v*C5vNC*this->mn*pow(p0,2)*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3] + 
     10*C4vNC*C5v*this->mRes*p0*pow(this->q[0],3)*this->q[1] * this->q[1]*this->q[3] + 
     18*C4v*C5vNC*this->mRes*p0*pow(this->q[0],3)*this->q[1] * this->q[1]*this->q[3] + 
     20*C5v*C5vNC*this->mRes*p0*pow(this->q[0],3)*this->q[1] * this->q[1]*this->q[3] + 
     24*C5v*C5vNC*this->mn*p0*pow(this->q[0],3)*this->q[1] * this->q[1]*this->q[3] - 
     6*C4vNC*C5v*this->mRes*pow(this->q[0],4)*this->q[1] * this->q[1]*this->q[3] - 
     6*C4v*C5vNC*this->mRes*pow(this->q[0],4)*this->q[1] * this->q[1]*this->q[3] - 
     8*C5v*C5vNC*this->mn*pow(this->q[0],4)*this->q[1] * this->q[1]*this->q[3] - 
     2*C4v*C5vNC*this->mRes*p0*this->q[0]*pow(this->q[1],4)*this->q[3] - 
     2*C5v*C5vNC*this->mRes*p0*this->q[0]*pow(this->q[1],4)*this->q[3] + 
     C4vNC*C5v*this->mRes*this->q[0] * this->q[0]*pow(this->q[1],4)*this->q[3] + 
     C4v*C5vNC*this->mRes*this->q[0] * this->q[0]*pow(this->q[1],4)*this->q[3] + 
     8*C4v*C4vNC*pow(this->mRes,3)*p0*this->q[0]*pow(this->q[3],3) + 
     8*C4vNC*C5v*pow(this->mRes,3)*p0*this->q[0]*pow(this->q[3],3) - 
     16*C4v*C5vNC*pow(this->mRes,3)*p0*this->q[0]*pow(this->q[3],3) - 
     16*C5v*C5vNC*pow(this->mRes,3)*p0*this->q[0]*pow(this->q[3],3) + 
     8*C4v*C4vNC*this->mRes_2*this->mn*p0*this->q[0]*pow(this->q[3],3) + 
     8*C4vNC*C5v*this->mRes_2*this->mn*p0*this->q[0]*pow(this->q[3],3) - 
     8*C4v*C5vNC*this->mRes_2*this->mn*p0*this->q[0]*pow(this->q[3],3) - 
     8*C5v*C5vNC*this->mRes_2*this->mn*p0*this->q[0]*pow(this->q[3],3) + 
     8*C4v*C5vNC*this->mRes*pow(p0,3)*this->q[0]*pow(this->q[3],3) + 
     8*C5v*C5vNC*this->mRes*pow(p0,3)*this->q[0]*pow(this->q[3],3) + 
     4*C4vNC*C5v*pow(this->mRes,3)*this->q[0] * this->q[0]*pow(this->q[3],3) + 
     4*C4v*C5vNC*pow(this->mRes,3)*this->q[0] * this->q[0]*pow(this->q[3],3) - 
     8*C4v*C4vNC*this->mRes_2*this->mn*this->q[0] * this->q[0]*pow(this->q[3],3) - 
     8*C4vNC*C5v*this->mRes_2*this->mn*this->q[0] * this->q[0]*pow(this->q[3],3) + 
     8*C4v*C5vNC*this->mRes_2*this->mn*this->q[0] * this->q[0]*pow(this->q[3],3) + 
     8*C5v*C5vNC*this->mRes_2*this->mn*this->q[0] * this->q[0]*pow(this->q[3],3) - 
     4*C4vNC*C5v*this->mRes*pow(p0,2)*this->q[0] * this->q[0]*pow(this->q[3],3) - 
     20*C4v*C5vNC*this->mRes*pow(p0,2)*this->q[0] * this->q[0]*pow(this->q[3],3) - 
     32*C5v*C5vNC*this->mRes*pow(p0,2)*this->q[0] * this->q[0]*pow(this->q[3],3) - 
     16*C5v*C5vNC*this->mn*pow(p0,2)*this->q[0] * this->q[0]*pow(this->q[3],3) + 
     10*C4vNC*C5v*this->mRes*p0*pow(this->q[0],3)*pow(this->q[3],3) + 
     18*C4v*C5vNC*this->mRes*p0*pow(this->q[0],3)*pow(this->q[3],3) + 
     20*C5v*C5vNC*this->mRes*p0*pow(this->q[0],3)*pow(this->q[3],3) + 
     24*C5v*C5vNC*this->mn*p0*pow(this->q[0],3)*pow(this->q[3],3) - 
     6*C4vNC*C5v*this->mRes*pow(this->q[0],4)*pow(this->q[3],3) - 
     6*C4v*C5vNC*this->mRes*pow(this->q[0],4)*pow(this->q[3],3) - 
     8*C5v*C5vNC*this->mn*pow(this->q[0],4)*pow(this->q[3],3) - 
     4*C4v*C5vNC*this->mRes*p0*this->q[0]*this->q[1] * this->q[1]*pow(this->q[3],3) - 
     4*C5v*C5vNC*this->mRes*p0*this->q[0]*this->q[1] * this->q[1]*pow(this->q[3],3) + 
     2*C4vNC*C5v*this->mRes*this->q[0] * this->q[0]*this->q[1] * this->q[1]*pow(this->q[3],3) + 
     2*C4v*C5vNC*this->mRes*this->q[0] * this->q[0]*this->q[1] * this->q[1]*pow(this->q[3],3) - 
     2*C4v*C5vNC*this->mRes*p0*this->q[0]*pow(this->q[3],5) - 
     2*C5v*C5vNC*this->mRes*p0*this->q[0]*pow(this->q[3],5) + 
     C4vNC*C5v*this->mRes*this->q[0] * this->q[0]*pow(this->q[3],5) + 
     C4v*C5vNC*this->mRes*this->q[0] * this->q[0]*pow(this->q[3],5) + 
     16*C4v*C4vNC*pow(this->mRes,3)*p0*this->q[0]*this->q[1]*this->q[3]*this->kg[1] + 
     16*C4vNC*C5v*pow(this->mRes,3)*p0*this->q[0]*this->q[1]*this->q[3]*this->kg[1] + 
     16*C4v*C5vNC*pow(this->mRes,3)*p0*this->q[0]*this->q[1]*this->q[3]*this->kg[1] + 
     16*C5v*C5vNC*pow(this->mRes,3)*p0*this->q[0]*this->q[1]*this->q[3]*this->kg[1] + 
     8*C4v*C4vNC*this->mRes_2*this->mn*p0*this->q[0]*this->q[1]*this->q[3]*this->kg[1] + 
     8*C4vNC*C5v*this->mRes_2*this->mn*p0*this->q[0]*this->q[1]*this->q[3]*this->kg[1] + 
     8*C4v*C5vNC*this->mRes_2*this->mn*p0*this->q[0]*this->q[1]*this->q[3]*this->kg[1] + 
     8*C5v*C5vNC*this->mRes_2*this->mn*p0*this->q[0]*this->q[1]*this->q[3]*this->kg[1] - 
     12*C4v*C4vNC*pow(this->mRes,3)*this->q[0] * this->q[0]*this->q[1]*this->q[3]*this->kg[1] - 
     4*C4vNC*C5v*pow(this->mRes,3)*this->q[0] * this->q[0]*this->q[1]*this->q[3]*this->kg[1] - 
     8*C4v*C5vNC*pow(this->mRes,3)*this->q[0] * this->q[0]*this->q[1]*this->q[3]*this->kg[1] - 
     8*C4v*C4vNC*this->mRes_2*this->mn*this->q[0] * this->q[0]*this->q[1]*this->q[3]*this->kg[1] - 
     8*C4vNC*C5v*this->mRes_2*this->mn*this->q[0] * this->q[0]*this->q[1]*this->q[3]*this->kg[1] - 
     8*C4v*C5vNC*this->mRes_2*this->mn*this->q[0] * this->q[0]*this->q[1]*this->q[3]*this->kg[1] - 
     8*C5v*C5vNC*this->mRes_2*this->mn*this->q[0] * this->q[0]*this->q[1]*this->q[3]*this->kg[1] + 
     8*C4vNC*C5v*this->mRes*pow(p0,2)*this->q[0] * this->q[0]*this->q[1]*this->q[3]*this->kg[1] + 
     4*C4v*C5vNC*this->mRes*pow(p0,2)*this->q[0] * this->q[0]*this->q[1]*this->q[3]*this->kg[1] + 
     12*C5v*C5vNC*this->mRes*pow(p0,2)*this->q[0] * this->q[0]*this->q[1]*this->q[3]*this->kg[1] - 
     16*C4vNC*C5v*this->mRes*p0*pow(this->q[0],3)*this->q[1]*this->q[3]*this->kg[1] - 
     12*C4v*C5vNC*this->mRes*p0*pow(this->q[0],3)*this->q[1]*this->q[3]*this->kg[1] - 
     28*C5v*C5vNC*this->mRes*p0*pow(this->q[0],3)*this->q[1]*this->q[3]*this->kg[1] - 
     16*C5v*C5vNC*this->mn*p0*pow(this->q[0],3)*this->q[1]*this->q[3]*this->kg[1] + 
     6*C4vNC*C5v*this->mRes*pow(this->q[0],4)*this->q[1]*this->q[3]*this->kg[1] + 
     9*C4v*C5vNC*this->mRes*pow(this->q[0],4)*this->q[1]*this->q[3]*this->kg[1] - 
     C5v*C5vNC*this->mRes*pow(this->q[0],4)*this->q[1]*this->q[3]*this->kg[1] + 
     16*C5v*C5vNC*this->mn*pow(this->q[0],4)*this->q[1]*this->q[3]*this->kg[1] + 
     2*C4vNC*C5v*this->mRes*this->q[0] * this->q[0]*pow(this->q[1],3)*this->q[3]*this->kg[1] - 
     C4v*C5vNC*this->mRes*this->q[0] * this->q[0]*pow(this->q[1],3)*this->q[3]*this->kg[1] + 
     C5v*C5vNC*this->mRes*this->q[0] * this->q[0]*pow(this->q[1],3)*this->q[3]*this->kg[1] + 
     2*C4vNC*C5v*this->mRes*this->q[0] * this->q[0]*this->q[1]*pow(this->q[3],3)*this->kg[1] - 
     C4v*C5vNC*this->mRes*this->q[0] * this->q[0]*this->q[1]*pow(this->q[3],3)*this->kg[1] + 
     C5v*C5vNC*this->mRes*this->q[0] * this->q[0]*this->q[1]*pow(this->q[3],3)*this->kg[1] - 
     16*C4v*C4vNC*pow(this->mRes,3)*pow(p0,2)*this->q[3]*this->kg[1] * this->kg[1] + 
     16*C4vNC*C5v*pow(this->mRes,3)*pow(p0,2)*this->q[3]*this->kg[1] * this->kg[1] - 
     16*C4v*C5vNC*pow(this->mRes,3)*pow(p0,2)*this->q[3]*this->kg[1] * this->kg[1] + 
     16*C5v*C5vNC*pow(this->mRes,3)*pow(p0,2)*this->q[3]*this->kg[1] * this->kg[1] - 
     16*C4v*C4vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[3]*this->kg[1] * this->kg[1] + 
     16*C4vNC*C5v*this->mRes_2*this->mn*pow(p0,2)*this->q[3]*this->kg[1] * this->kg[1] - 
     16*C4v*C5vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[3]*this->kg[1] * this->kg[1] + 
     16*C5v*C5vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[3]*this->kg[1] * this->kg[1] + 
     8*C4v*C4vNC*pow(this->mRes,3)*p0*this->q[0]*this->q[3]*this->kg[1] * this->kg[1] - 
     8*C4v*C5vNC*pow(this->mRes,3)*p0*this->q[0]*this->q[3]*this->kg[1] * this->kg[1] + 
     16*C5v*C5vNC*pow(this->mRes,3)*p0*this->q[0]*this->q[3]*this->kg[1] * this->kg[1] + 
     24*C4v*C4vNC*this->mRes_2*this->mn*p0*this->q[0]*this->q[3]*this->kg[1] * this->kg[1] - 
     24*C4vNC*C5v*this->mRes_2*this->mn*p0*this->q[0]*this->q[3]*this->kg[1] * this->kg[1] + 
     8*C4v*C5vNC*this->mRes_2*this->mn*p0*this->q[0]*this->q[3]*this->kg[1] * this->kg[1] - 
     8*C5v*C5vNC*this->mRes_2*this->mn*p0*this->q[0]*this->q[3]*this->kg[1] * this->kg[1] - 
     8*C4vNC*C5v*this->mRes*pow(p0,3)*this->q[0]*this->q[3]*this->kg[1] * this->kg[1] - 
     8*C5v*C5vNC*this->mRes*pow(p0,3)*this->q[0]*this->q[3]*this->kg[1] * this->kg[1] - 
     4*C4vNC*C5v*pow(this->mRes,3)*this->q[0] * this->q[0]*this->q[3]*this->kg[1] * this->kg[1] - 
     4*C4v*C5vNC*pow(this->mRes,3)*this->q[0] * this->q[0]*this->q[3]*this->kg[1] * this->kg[1] - 
     8*C4v*C4vNC*this->mRes_2*this->mn*this->q[0] * this->q[0]*this->q[3]*this->kg[1] * this->kg[1] + 
     8*C4vNC*C5v*this->mRes_2*this->mn*this->q[0] * this->q[0]*this->q[3]*this->kg[1] * this->kg[1] + 
     8*C4v*C5vNC*this->mRes_2*this->mn*this->q[0] * this->q[0]*this->q[3]*this->kg[1] * this->kg[1] - 
     8*C5v*C5vNC*this->mRes_2*this->mn*this->q[0] * this->q[0]*this->q[3]*this->kg[1] * this->kg[1] + 
     20*C4vNC*C5v*this->mRes*pow(p0,2)*this->q[0] * this->q[0]*this->q[3]*this->kg[1] * this->kg[1] + 
     4*C4v*C5vNC*this->mRes*pow(p0,2)*this->q[0] * this->q[0]*this->q[3]*this->kg[1] * this->kg[1] + 
     32*C5v*C5vNC*this->mRes*pow(p0,2)*this->q[0] * this->q[0]*this->q[3]*this->kg[1] * this->kg[1] + 
     16*C5v*C5vNC*this->mn*pow(p0,2)*this->q[0] * this->q[0]*this->q[3]*this->kg[1] * this->kg[1] - 
     14*C4vNC*C5v*this->mRes*p0*pow(this->q[0],3)*this->q[3]*this->kg[1] * this->kg[1] - 
     8*C4v*C5vNC*this->mRes*p0*pow(this->q[0],3)*this->q[3]*this->kg[1] * this->kg[1] - 
     14*C5v*C5vNC*this->mRes*p0*pow(this->q[0],3)*this->q[3]*this->kg[1] * this->kg[1] - 
     24*C5v*C5vNC*this->mn*p0*pow(this->q[0],3)*this->q[3]*this->kg[1] * this->kg[1] + 
     3*C4vNC*C5v*this->mRes*pow(this->q[0],4)*this->q[3]*this->kg[1] * this->kg[1] + 
     3*C4v*C5vNC*this->mRes*pow(this->q[0],4)*this->q[3]*this->kg[1] * this->kg[1] + 
     8*C5v*C5vNC*this->mn*pow(this->q[0],4)*this->q[3]*this->kg[1] * this->kg[1] + 
     4*C4v*C4vNC*pow(this->mRes,3)*this->q[1] * this->q[1]*this->q[3]*this->kg[1] * this->kg[1] - 
     4*C4v*C5vNC*pow(this->mRes,3)*this->q[1] * this->q[1]*this->q[3]*this->kg[1] * this->kg[1] + 
     4*C4vNC*C5v*this->mRes*pow(p0,2)*this->q[1] * this->q[1]*this->q[3]*this->kg[1] * this->kg[1] + 
     8*C4v*C5vNC*this->mRes*pow(p0,2)*this->q[1] * this->q[1]*this->q[3]*this->kg[1] * this->kg[1] + 
     20*C5v*C5vNC*this->mRes*pow(p0,2)*this->q[1] * this->q[1]*this->q[3]*this->kg[1] * this->kg[1] + 
     16*C5v*C5vNC*this->mn*pow(p0,2)*this->q[1] * this->q[1]*this->q[3]*this->kg[1] * this->kg[1] - 
     10*C4vNC*C5v*this->mRes*p0*this->q[0]*this->q[1] * this->q[1]*this->q[3]*this->kg[1] * this->kg[1] - 
     12*C4v*C5vNC*this->mRes*p0*this->q[0]*this->q[1] * this->q[1]*this->q[3]*this->kg[1] * this->kg[1] - 
     14*C5v*C5vNC*this->mRes*p0*this->q[0]*this->q[1] * this->q[1]*this->q[3]*this->kg[1] * this->kg[1] - 
     24*C5v*C5vNC*this->mn*p0*this->q[0]*this->q[1] * this->q[1]*this->q[3]*this->kg[1] * this->kg[1] + 
     6*C4vNC*C5v*this->mRes*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3]*this->kg[1] * this->kg[1] + 
     5*C4v*C5vNC*this->mRes*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3]*this->kg[1] * this->kg[1] - 
     C5v*C5vNC*this->mRes*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3]*this->kg[1] * this->kg[1] + 
     8*C5v*C5vNC*this->mn*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3]*this->kg[1] * this->kg[1] - 
     C4vNC*C5v*this->mRes*pow(this->q[1],4)*this->q[3]*this->kg[1] * this->kg[1] + 
     C5v*C5vNC*this->mRes*pow(this->q[1],4)*this->q[3]*this->kg[1] * this->kg[1] + 
     4*C4v*C4vNC*pow(this->mRes,3)*pow(this->q[3],3)*this->kg[1] * this->kg[1] - 
     4*C4v*C5vNC*pow(this->mRes,3)*pow(this->q[3],3)*this->kg[1] * this->kg[1] + 
     4*C4vNC*C5v*this->mRes*pow(p0,2)*pow(this->q[3],3)*this->kg[1] * this->kg[1] + 
     8*C4v*C5vNC*this->mRes*pow(p0,2)*pow(this->q[3],3)*this->kg[1] * this->kg[1] + 
     20*C5v*C5vNC*this->mRes*pow(p0,2)*pow(this->q[3],3)*this->kg[1] * this->kg[1] + 
     16*C5v*C5vNC*this->mn*pow(p0,2)*pow(this->q[3],3)*this->kg[1] * this->kg[1] - 
     10*C4vNC*C5v*this->mRes*p0*this->q[0]*pow(this->q[3],3)*this->kg[1] * this->kg[1] - 
     12*C4v*C5vNC*this->mRes*p0*this->q[0]*pow(this->q[3],3)*this->kg[1] * this->kg[1] - 
     14*C5v*C5vNC*this->mRes*p0*this->q[0]*pow(this->q[3],3)*this->kg[1] * this->kg[1] - 
     24*C5v*C5vNC*this->mn*p0*this->q[0]*pow(this->q[3],3)*this->kg[1] * this->kg[1] + 
     6*C4vNC*C5v*this->mRes*this->q[0] * this->q[0]*pow(this->q[3],3)*this->kg[1] * this->kg[1] + 
     5*C4v*C5vNC*this->mRes*this->q[0] * this->q[0]*pow(this->q[3],3)*this->kg[1] * this->kg[1] - 
     C5v*C5vNC*this->mRes*this->q[0] * this->q[0]*pow(this->q[3],3)*this->kg[1] * this->kg[1] + 
     8*C5v*C5vNC*this->mn*this->q[0] * this->q[0]*pow(this->q[3],3)*this->kg[1] * this->kg[1] - 
     2*C4vNC*C5v*this->mRes*this->q[1] * this->q[1]*pow(this->q[3],3)*this->kg[1] * this->kg[1] + 
     2*C5v*C5vNC*this->mRes*this->q[1] * this->q[1]*pow(this->q[3],3)*this->kg[1] * this->kg[1] - 
     C4vNC*C5v*this->mRes*pow(this->q[3],5)*this->kg[1] * this->kg[1] + 
     C5v*C5vNC*this->mRes*pow(this->q[3],5)*this->kg[1] * this->kg[1] + 
     8*C4v*C4vNC*pow(this->mRes,3)*this->q[1]*this->q[3]*pow(this->kg[1],3) + 
     8*C4v*C5vNC*pow(this->mRes,3)*this->q[1]*this->q[3]*pow(this->kg[1],3) - 
     8*C4vNC*C5v*this->mRes*pow(p0,2)*this->q[1]*this->q[3]*pow(this->kg[1],3) - 
     8*C5v*C5vNC*this->mRes*pow(p0,2)*this->q[1]*this->q[3]*pow(this->kg[1],3) + 
     16*C4vNC*C5v*this->mRes*p0*this->q[0]*this->q[1]*this->q[3]*pow(this->kg[1],3) + 
     8*C4v*C5vNC*this->mRes*p0*this->q[0]*this->q[1]*this->q[3]*pow(this->kg[1],3) + 
     24*C5v*C5vNC*this->mRes*p0*this->q[0]*this->q[1]*this->q[3]*pow(this->kg[1],3) + 
     16*C5v*C5vNC*this->mn*p0*this->q[0]*this->q[1]*this->q[3]*pow(this->kg[1],3) - 
     6*C4vNC*C5v*this->mRes*this->q[0] * this->q[0]*this->q[1]*this->q[3]*pow(this->kg[1],3) - 
     8*C4v*C5vNC*this->mRes*this->q[0] * this->q[0]*this->q[1]*this->q[3]*pow(this->kg[1],3) + 
     2*C5v*C5vNC*this->mRes*this->q[0] * this->q[0]*this->q[1]*this->q[3]*pow(this->kg[1],3) - 
     16*C5v*C5vNC*this->mn*this->q[0] * this->q[0]*this->q[1]*this->q[3]*pow(this->kg[1],3) - 
     2*C4vNC*C5v*this->mRes*pow(this->q[1],3)*this->q[3]*pow(this->kg[1],3) - 
     2*C5v*C5vNC*this->mRes*pow(this->q[1],3)*this->q[3]*pow(this->kg[1],3) - 
     2*C4vNC*C5v*this->mRes*this->q[1]*pow(this->q[3],3)*pow(this->kg[1],3) - 
     2*C5v*C5vNC*this->mRes*this->q[1]*pow(this->q[3],3)*pow(this->kg[1],3) + 
     std::complex<double>(0,8)*C4v*C5a*this->mRes*this->mn2*pow(p0,2)*this->q[0]*this->q[1]*this->kg[2] + 
     std::complex<double>(0,8)*C5a*C5v*this->mRes*this->mn2*pow(p0,2)*this->q[0]*this->q[1]*this->kg[2] - 
     std::complex<double>(0,12)*C4v*C5a*this->mRes*this->mn2*p0*this->q[0] * this->q[0]*this->q[1]*this->kg[2] - 
     std::complex<double>(0,12)*C5a*C5v*this->mRes*this->mn2*p0*this->q[0] * this->q[0]*this->q[1]*this->kg[2] + 
     std::complex<double>(0,4)*C4v*C5a*this->mRes*this->mn2*pow(this->q[0],3)*this->q[1]*this->kg[2] + 
     std::complex<double>(0,4)*C5a*C5v*this->mRes*this->mn2*pow(this->q[0],3)*this->q[1]*this->kg[2] + 
     std::complex<double>(0,4)*C4v*C5a*this->mRes*this->mn2*p0*this->q[1] * this->q[1]*this->kg[1]*this->kg[2] + 
     std::complex<double>(0,4)*C5a*C5v*this->mRes*this->mn2*p0*this->q[1] * this->q[1]*this->kg[1]*this->kg[2] - 
     std::complex<double>(0,4)*C4v*C5a*this->mRes*this->mn2*this->q[0]*this->q[1] * this->q[1]*this->kg[1]*this->kg[2] - 
     std::complex<double>(0,4)*C5a*C5v*this->mRes*this->mn2*this->q[0]*this->q[1] * this->q[1]*this->kg[1]*this->kg[2] - 
     16*C4v*C4vNC*pow(this->mRes,3)*pow(p0,2)*this->q[3]*this->kg[2] * this->kg[2] + 
     16*C4vNC*C5v*pow(this->mRes,3)*pow(p0,2)*this->q[3]*this->kg[2] * this->kg[2] - 
     16*C4v*C5vNC*pow(this->mRes,3)*pow(p0,2)*this->q[3]*this->kg[2] * this->kg[2] + 
     16*C5v*C5vNC*pow(this->mRes,3)*pow(p0,2)*this->q[3]*this->kg[2] * this->kg[2] - 
     16*C4v*C4vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[3]*this->kg[2] * this->kg[2] + 
     16*C4vNC*C5v*this->mRes_2*this->mn*pow(p0,2)*this->q[3]*this->kg[2] * this->kg[2] - 
     16*C4v*C5vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[3]*this->kg[2] * this->kg[2] + 
     16*C5v*C5vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[3]*this->kg[2] * this->kg[2] + 
     8*C4v*C4vNC*pow(this->mRes,3)*p0*this->q[0]*this->q[3]*this->kg[2] * this->kg[2] - 
     8*C4v*C5vNC*pow(this->mRes,3)*p0*this->q[0]*this->q[3]*this->kg[2] * this->kg[2] + 
     16*C5v*C5vNC*pow(this->mRes,3)*p0*this->q[0]*this->q[3]*this->kg[2] * this->kg[2] + 
     24*C4v*C4vNC*this->mRes_2*this->mn*p0*this->q[0]*this->q[3]*this->kg[2] * this->kg[2] - 
     24*C4vNC*C5v*this->mRes_2*this->mn*p0*this->q[0]*this->q[3]*this->kg[2] * this->kg[2] + 
     8*C4v*C5vNC*this->mRes_2*this->mn*p0*this->q[0]*this->q[3]*this->kg[2] * this->kg[2] - 
     8*C5v*C5vNC*this->mRes_2*this->mn*p0*this->q[0]*this->q[3]*this->kg[2] * this->kg[2] - 
     8*C4vNC*C5v*this->mRes*pow(p0,3)*this->q[0]*this->q[3]*this->kg[2] * this->kg[2] - 
     8*C5v*C5vNC*this->mRes*pow(p0,3)*this->q[0]*this->q[3]*this->kg[2] * this->kg[2] - 
     4*C4vNC*C5v*pow(this->mRes,3)*this->q[0] * this->q[0]*this->q[3]*this->kg[2] * this->kg[2] - 
     4*C4v*C5vNC*pow(this->mRes,3)*this->q[0] * this->q[0]*this->q[3]*this->kg[2] * this->kg[2] - 
     8*C4v*C4vNC*this->mRes_2*this->mn*this->q[0] * this->q[0]*this->q[3]*this->kg[2] * this->kg[2] + 
     8*C4vNC*C5v*this->mRes_2*this->mn*this->q[0] * this->q[0]*this->q[3]*this->kg[2] * this->kg[2] + 
     8*C4v*C5vNC*this->mRes_2*this->mn*this->q[0] * this->q[0]*this->q[3]*this->kg[2] * this->kg[2] - 
     8*C5v*C5vNC*this->mRes_2*this->mn*this->q[0] * this->q[0]*this->q[3]*this->kg[2] * this->kg[2] + 
     20*C4vNC*C5v*this->mRes*pow(p0,2)*this->q[0] * this->q[0]*this->q[3]*this->kg[2] * this->kg[2] + 
     4*C4v*C5vNC*this->mRes*pow(p0,2)*this->q[0] * this->q[0]*this->q[3]*this->kg[2] * this->kg[2] + 
     32*C5v*C5vNC*this->mRes*pow(p0,2)*this->q[0] * this->q[0]*this->q[3]*this->kg[2] * this->kg[2] + 
     16*C5v*C5vNC*this->mn*pow(p0,2)*this->q[0] * this->q[0]*this->q[3]*this->kg[2] * this->kg[2] - 
     14*C4vNC*C5v*this->mRes*p0*pow(this->q[0],3)*this->q[3]*this->kg[2] * this->kg[2] - 
     8*C4v*C5vNC*this->mRes*p0*pow(this->q[0],3)*this->q[3]*this->kg[2] * this->kg[2] - 
     14*C5v*C5vNC*this->mRes*p0*pow(this->q[0],3)*this->q[3]*this->kg[2] * this->kg[2] - 
     24*C5v*C5vNC*this->mn*p0*pow(this->q[0],3)*this->q[3]*this->kg[2] * this->kg[2] + 
     3*C4vNC*C5v*this->mRes*pow(this->q[0],4)*this->q[3]*this->kg[2] * this->kg[2] + 
     3*C4v*C5vNC*this->mRes*pow(this->q[0],4)*this->q[3]*this->kg[2] * this->kg[2] + 
     8*C5v*C5vNC*this->mn*pow(this->q[0],4)*this->q[3]*this->kg[2] * this->kg[2] + 
     4*C4v*C4vNC*pow(this->mRes,3)*this->q[1] * this->q[1]*this->q[3]*this->kg[2] * this->kg[2] - 
     4*C4v*C5vNC*pow(this->mRes,3)*this->q[1] * this->q[1]*this->q[3]*this->kg[2] * this->kg[2] + 
     4*C4vNC*C5v*this->mRes*pow(p0,2)*this->q[1] * this->q[1]*this->q[3]*this->kg[2] * this->kg[2] + 
     8*C4v*C5vNC*this->mRes*pow(p0,2)*this->q[1] * this->q[1]*this->q[3]*this->kg[2] * this->kg[2] + 
     20*C5v*C5vNC*this->mRes*pow(p0,2)*this->q[1] * this->q[1]*this->q[3]*this->kg[2] * this->kg[2] + 
     16*C5v*C5vNC*this->mn*pow(p0,2)*this->q[1] * this->q[1]*this->q[3]*this->kg[2] * this->kg[2] - 
     10*C4vNC*C5v*this->mRes*p0*this->q[0]*this->q[1] * this->q[1]*this->q[3]*this->kg[2] * this->kg[2] - 
     12*C4v*C5vNC*this->mRes*p0*this->q[0]*this->q[1] * this->q[1]*this->q[3]*this->kg[2] * this->kg[2] - 
     14*C5v*C5vNC*this->mRes*p0*this->q[0]*this->q[1] * this->q[1]*this->q[3]*this->kg[2] * this->kg[2] - 
     24*C5v*C5vNC*this->mn*p0*this->q[0]*this->q[1] * this->q[1]*this->q[3]*this->kg[2] * this->kg[2] + 
     6*C4vNC*C5v*this->mRes*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3]*this->kg[2] * this->kg[2] + 
     5*C4v*C5vNC*this->mRes*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3]*this->kg[2] * this->kg[2] - 
     C5v*C5vNC*this->mRes*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3]*this->kg[2] * this->kg[2] + 
     8*C5v*C5vNC*this->mn*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3]*this->kg[2] * this->kg[2] - 
     C4vNC*C5v*this->mRes*pow(this->q[1],4)*this->q[3]*this->kg[2] * this->kg[2] + 
     C5v*C5vNC*this->mRes*pow(this->q[1],4)*this->q[3]*this->kg[2] * this->kg[2] + 
     4*C4v*C4vNC*pow(this->mRes,3)*pow(this->q[3],3)*this->kg[2] * this->kg[2] - 
     4*C4v*C5vNC*pow(this->mRes,3)*pow(this->q[3],3)*this->kg[2] * this->kg[2] + 
     4*C4vNC*C5v*this->mRes*pow(p0,2)*pow(this->q[3],3)*this->kg[2] * this->kg[2] + 
     8*C4v*C5vNC*this->mRes*pow(p0,2)*pow(this->q[3],3)*this->kg[2] * this->kg[2] + 
     20*C5v*C5vNC*this->mRes*pow(p0,2)*pow(this->q[3],3)*this->kg[2] * this->kg[2] + 
     16*C5v*C5vNC*this->mn*pow(p0,2)*pow(this->q[3],3)*this->kg[2] * this->kg[2] - 
     10*C4vNC*C5v*this->mRes*p0*this->q[0]*pow(this->q[3],3)*this->kg[2] * this->kg[2] - 
     12*C4v*C5vNC*this->mRes*p0*this->q[0]*pow(this->q[3],3)*this->kg[2] * this->kg[2] - 
     14*C5v*C5vNC*this->mRes*p0*this->q[0]*pow(this->q[3],3)*this->kg[2] * this->kg[2] - 
     24*C5v*C5vNC*this->mn*p0*this->q[0]*pow(this->q[3],3)*this->kg[2] * this->kg[2] + 
     6*C4vNC*C5v*this->mRes*this->q[0] * this->q[0]*pow(this->q[3],3)*this->kg[2] * this->kg[2] + 
     5*C4v*C5vNC*this->mRes*this->q[0] * this->q[0]*pow(this->q[3],3)*this->kg[2] * this->kg[2] - 
     C5v*C5vNC*this->mRes*this->q[0] * this->q[0]*pow(this->q[3],3)*this->kg[2] * this->kg[2] + 
     8*C5v*C5vNC*this->mn*this->q[0] * this->q[0]*pow(this->q[3],3)*this->kg[2] * this->kg[2] - 
     2*C4vNC*C5v*this->mRes*this->q[1] * this->q[1]*pow(this->q[3],3)*this->kg[2] * this->kg[2] + 
     2*C5v*C5vNC*this->mRes*this->q[1] * this->q[1]*pow(this->q[3],3)*this->kg[2] * this->kg[2] - 
     C4vNC*C5v*this->mRes*pow(this->q[3],5)*this->kg[2] * this->kg[2] + 
     C5v*C5vNC*this->mRes*pow(this->q[3],5)*this->kg[2] * this->kg[2] + 
     8*C4v*C4vNC*pow(this->mRes,3)*this->q[1]*this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] + 
     8*C4v*C5vNC*pow(this->mRes,3)*this->q[1]*this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] - 
     8*C4vNC*C5v*this->mRes*pow(p0,2)*this->q[1]*this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] - 
     8*C5v*C5vNC*this->mRes*pow(p0,2)*this->q[1]*this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] + 
     16*C4vNC*C5v*this->mRes*p0*this->q[0]*this->q[1]*this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] + 
     8*C4v*C5vNC*this->mRes*p0*this->q[0]*this->q[1]*this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] + 
     24*C5v*C5vNC*this->mRes*p0*this->q[0]*this->q[1]*this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] + 
     16*C5v*C5vNC*this->mn*p0*this->q[0]*this->q[1]*this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] - 
     6*C4vNC*C5v*this->mRes*this->q[0] * this->q[0]*this->q[1]*this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] - 
     8*C4v*C5vNC*this->mRes*this->q[0] * this->q[0]*this->q[1]*this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] + 
     2*C5v*C5vNC*this->mRes*this->q[0] * this->q[0]*this->q[1]*this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] - 
     16*C5v*C5vNC*this->mn*this->q[0] * this->q[0]*this->q[1]*this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] - 
     2*C4vNC*C5v*this->mRes*pow(this->q[1],3)*this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] - 
     2*C5v*C5vNC*this->mRes*pow(this->q[1],3)*this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] - 
     2*C4vNC*C5v*this->mRes*this->q[1]*pow(this->q[3],3)*this->kg[1]*this->kg[2] * this->kg[2] - 
     2*C5v*C5vNC*this->mRes*this->q[1]*pow(this->q[3],3)*this->kg[1]*this->kg[2] * this->kg[2] - 
     (std::complex<double>(0,-4)*C4v*C5a*this->mRes*this->mn2*(p0 - this->q[0])*this->q[1]*this->q[3]*this->kg[2] - 
        std::complex<double>(0,4)*C5a*C5v*this->mRes*this->mn2*(p0 - this->q[0])*this->q[1]*this->q[3]*this->kg[2] + 
        C4v*C5vNC*this->mRes*(8*pow(p0,3)*this->q[0]*(this->q[1] * this->q[1] + this->q[3] * this->q[3]) - 
           8*this->mRes*this->mn*(p0 - this->q[0])*this->q[1]*(2*p0*this->kg[1] + this->q[0]*(-this->q[1] + this->kg[1])) + 
           4*pow(p0,2)*(2*this->q[1]*(this->q[1] * this->q[1] + this->q[3] * this->q[3])*this->kg[1] + 
              this->q[0] * this->q[0]*(-6*this->q[3] * this->q[3] + this->q[1]*(-5*this->q[1] + this->kg[1]))) - 
           4*this->mRes_2*(4*pow(p0,2)*this->q[1]*this->kg[1] - 
              this->q[0] * this->q[0]*(this->q[1] * this->q[1] + 2*this->q[3] * this->q[3] - this->q[1]*this->kg[1]) + 
              this->kg[1]*(pow(this->q[1],3) + this->q[1]*this->q[3] * this->q[3] - this->q[1] * this->q[1]*this->kg[1] + 
                 this->q[3] * this->q[3]*this->kg[1]) + 
              2*p0*this->q[0]*(this->q[3] * this->q[3] + this->q[1]*(-this->q[1] + this->kg[1])) + 
              this->q[3] * this->q[3]*this->kg[2] * this->kg[2]) - 
           this->q[0] * this->q[0]*(pow(this->q[1],4) + 
              this->q[0] * this->q[0]*(8*this->q[3] * this->q[3] + 3*this->q[1]*(this->q[1] - this->kg[1])) - 
              5*pow(this->q[1],3)*this->kg[1] - 5*this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 
              this->q[1] * this->q[1]*(this->q[3] * this->q[3] + 4*this->kg[1] * this->kg[1]) - 
              4*this->q[3] * this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])) + 
           2*p0*this->q[0]*(pow(this->q[1],4) + pow(this->q[3],4) - 6*pow(this->q[1],3)*this->kg[1] - 
              6*this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 
              this->q[0] * this->q[0]*(7*this->q[1] * this->q[1] + 11*this->q[3] * this->q[3] - 4*this->q[1]*this->kg[1]) + 
              2*this->q[1] * this->q[1]*(this->q[3] * this->q[3] + this->kg[1] * this->kg[1]) - 
              2*this->q[3] * this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]))) + 
        C5v*C5vNC*(16*pow(this->mRes,3)*p0*
            (-(this->q[0]*(this->q[1] * this->q[1] + 2*this->q[3] * this->q[3])) + (p0 + this->q[0])*this->q[1]*this->kg[1]) + 
           8*this->mRes_2*this->mn*(p0 - this->q[0])*
            (-(this->q[0]*(this->q[1] * this->q[1] + 2*this->q[3] * this->q[3])) + (2*p0 + this->q[0])*this->q[1]*this->kg[1])  
+ 8*this->mn*(p0 - this->q[0])*(4*pow(p0,2)*this->q[0]*(this->q[1] * this->q[1] + this->q[3] * this->q[3]) - 
              4*p0*this->q[0] * this->q[0]*(this->q[1] * this->q[1] + this->q[3] * this->q[3]) + 
              2*p0*this->q[1]*(this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3])*this->kg[1] + 
              pow(this->q[0],3)*(this->q[1] * this->q[1] + 2*this->q[3] * this->q[3] - this->q[1]*this->kg[1]) - 
              this->q[0]*this->kg[1]*(pow(this->q[1],3) + this->q[1]*this->q[3] * this->q[3] - this->q[1] * this->q[1]*this->kg[1] + 
                 this->q[3] * this->q[3]*this->kg[1]) - this->q[0]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2]) + 
           this->mRes*(8*pow(p0,3)*this->q[0]*
               (6*(this->q[1] * this->q[1] + this->q[3] * this->q[3]) - this->q[1]*this->kg[1]) + 
              2*p0*this->q[0]*(this->q[0] * this->q[0]*
                  (8*this->q[1] * this->q[1] + 16*this->q[3] * this->q[3] - 7*this->q[1]*this->kg[1]) + 
                 this->kg[1]*(-7*this->q[1]*(this->q[1] * this->q[1] + this->q[3] * this->q[3]) + 
                    6*(this->q[1] - this->q[3])*(this->q[1] + this->q[3])*this->kg[1]) - 
                 6*this->q[3] * this->q[3]*this->kg[2] * this->kg[2]) - 
              (this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3])*
               (this->kg[1]*(pow(this->q[1],3) + this->q[1]*this->q[3] * this->q[3] - this->q[1] * this->q[1]*this->kg[1] + 
                    this->q[3] * this->q[3]*this->kg[1]) + this->q[3] * this->q[3]*this->kg[2] * this->kg[2]) + 
              4*pow(p0,2)*
               (-2*this->q[0] * this->q[0]*
                  (7*this->q[1] * this->q[1] + 8*this->q[3] * this->q[3] - 4*this->q[1]*this->kg[1]) + 
                 this->kg[1]*(5*pow(this->q[1],3) + 5*this->q[1]*this->q[3] * this->q[3] - 
                    this->q[1] * this->q[1]*this->kg[1] + this->q[3] * this->q[3]*this->kg[1]) + 
                 this->q[3] * this->q[3]*this->kg[2] * this->kg[2]))) + 
        C4vNC*this->mRes*(-4*C4v*this->mRes*
            (2*this->mn*(p0 - this->q[0])*
               (2*p0*this->q[1]*this->kg[1] + 
                 this->q[0]*(this->q[1] * this->q[1] + 2*this->q[3] * this->q[3] - this->q[1]*this->kg[1])) + 
              this->mRes*(2*this->q[0] * this->q[0]*this->q[1] * this->q[1] + 
                 p0*this->q[0]*(4*this->q[3] * this->q[3] - 2*this->q[1]*this->kg[1]) - 
                 this->kg[1]*(-4*pow(p0,2)*this->q[1] + pow(this->q[1],3) + 
                    this->q[1]*this->q[3] * this->q[3] + this->q[1] * this->q[1]*this->kg[1] - this->q[3] * this->q[3]*this->kg[1])  
+ this->q[3] * this->q[3]*this->kg[2] * this->kg[2])) + 
           C5v*(-5*pow(this->q[0],4)*this->q[1] * this->q[1] + this->q[0] * this->q[0]*pow(this->q[1],4) - 
              8*pow(this->q[0],4)*this->q[3] * this->q[3] + 
              this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3] * this->q[3] + 
              3*pow(this->q[0],4)*this->q[1]*this->kg[1] + 6*this->q[0] * this->q[0]*pow(this->q[1],3)*this->kg[1] - 
              pow(this->q[1],5)*this->kg[1] + 6*this->q[0] * this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1] - 
              2*pow(this->q[1],3)*this->q[3] * this->q[3]*this->kg[1] - this->q[1]*pow(this->q[3],4)*this->kg[1] - 
              3*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] - 
              pow(this->q[1],4)*this->kg[1] * this->kg[1] + 
              3*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
              pow(this->q[3],4)*this->kg[1] * this->kg[1] + 
              8*this->mRes*this->mn*(p0 - this->q[0])*this->q[1]*(this->q[0]*(this->q[1] - this->kg[1]) + 2*p0*this->kg[1]) + 
              8*pow(p0,3)*this->q[0]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] - this->q[1]*this->kg[1]) + 
              4*this->mRes_2*
               (this->q[0]*(2*p0 + this->q[0])*this->q[1] * this->q[1] + 
                 2*this->q[0]*(-p0 + this->q[0])*this->q[3] * this->q[3] + 
                 (4*pow(p0,2) - this->q[0] * this->q[0])*this->q[1]*this->kg[1]) + 
              this->q[3] * this->q[3]*(3*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3])*
               this->kg[2] * this->kg[2] + 
              4*pow(p0,2)*
               (this->kg[1]*(pow(this->q[1],3) + this->q[1]*this->q[3] * this->q[3] - this->q[1] * this->q[1]*this->kg[1] + 
                    this->q[3] * this->q[3]*this->kg[1]) + 
                 this->q[0] * this->q[0]*(-6*this->q[3] * this->q[3] + 5*this->q[1]*(-this->q[1] + this->kg[1])) + 
                 this->q[3] * this->q[3]*this->kg[2] * this->kg[2]) + 
              2*p0*this->q[0]*(-pow(this->q[1],4) - 5*pow(this->q[1],3)*this->kg[1] - 
                 5*this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 
                 this->q[0] * this->q[0]*
                  (9*this->q[1] * this->q[1] + 13*this->q[3] * this->q[3] - 7*this->q[1]*this->kg[1]) - 
                 2*this->q[1] * this->q[1]*(this->q[3] * this->q[3] - 2*this->kg[1] * this->kg[1]) - 
                 this->q[3] * this->q[3]*(this->q[3] * this->q[3] + 
                    4*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]))))))*this->kg[3] - 
     2*C3vNC*this->mn*(C4v*this->mRes*(4*this->mRes_2*this->mn*
            (this->q[3]*(2*(p0 - this->q[0])*this->q[0] + this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) - 
              this->q[1]*this->kg[1]*this->kg[3]) + 2*this->mn*(p0 - this->q[0])*
            (4*pow(p0,2)*this->q[0]*this->q[3] + 3*pow(this->q[0],3)*this->q[3] - 
              this->q[0]*this->q[3]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] + this->q[1]*this->kg[1] + 
                 this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) - 
              this->q[0]*(this->q[1] * this->q[1] + 2*this->q[3] * this->q[3] - this->q[1]*this->kg[1])*this->kg[3] + 
              2*p0*(-4*this->q[0] * this->q[0]*this->q[3] + 
                 this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) - this->q[1]*this->kg[1]*this->kg[3])) + 
           this->mRes*(16*pow(p0,3)*this->q[0]*this->q[3] + 
              this->q[0] * this->q[0]*this->q[3]*(8*this->q[1]*this->kg[1] - this->kg[1] * this->kg[1] - this->kg[2] * this->kg[2]) - 
              this->q[3]*(3*(this->q[1] * this->q[1] + this->q[3] * this->q[3]) + 4*this->q[1]*this->kg[1])*
               (this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + 
              (this->q[0] * this->q[0]*this->q[1]*(-6*this->q[1] + this->kg[1]) + 
                 this->kg[1]*(3*this->q[1]*(this->q[1] * this->q[1] + this->q[3] * this->q[3]) + 
                    2*(this->q[1] - this->q[3])*(this->q[1] + this->q[3])*this->kg[1]) - 
                 2*this->q[3] * this->q[3]*this->kg[2] * this->kg[2])*this->kg[3] + 
              8*pow(p0,2)*
               (-4*this->q[0] * this->q[0]*this->q[3] + this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) - 
                 this->q[1]*this->kg[1]*this->kg[3]) + 
              2*p0*this->q[0]*(6*this->q[0] * this->q[0]*this->q[3] - 
                 this->q[3]*(2*this->q[1] * this->q[1] + 2*this->q[3] * this->q[3] + 5*this->q[1]*this->kg[1] + 
                    this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) - 4*this->q[3] * this->q[3]*this->kg[3] + 
                 this->q[1]*(this->q[1] + this->kg[1])*this->kg[3]))) + 
        C5v*(3*pow(this->q[0],6)*this->q[3] + 6*pow(this->q[0],4)*this->q[1] * this->q[1]*this->q[3] - 
           this->q[0] * this->q[0]*pow(this->q[1],4)*this->q[3] + 6*pow(this->q[0],4)*pow(this->q[3],3) - 
           2*this->q[0] * this->q[0]*this->q[1] * this->q[1]*pow(this->q[3],3) - 
           this->q[0] * this->q[0]*pow(this->q[3],5) - 6*pow(this->q[0],4)*this->q[1]*this->q[3]*this->kg[1] - 
           2*this->q[0] * this->q[0]*pow(this->q[1],3)*this->q[3]*this->kg[1] - 
           2*this->q[0] * this->q[0]*this->q[1]*pow(this->q[3],3)*this->kg[1] - 
           3*pow(this->q[0],4)*this->q[3]*this->kg[1] * this->kg[1] - 
           6*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3]*this->kg[1] * this->kg[1] + 
           pow(this->q[1],4)*this->q[3]*this->kg[1] * this->kg[1] - 
           6*this->q[0] * this->q[0]*pow(this->q[3],3)*this->kg[1] * this->kg[1] + 
           2*this->q[1] * this->q[1]*pow(this->q[3],3)*this->kg[1] * this->kg[1] + 
           pow(this->q[3],5)*this->kg[1] * this->kg[1] + 6*this->q[0] * this->q[0]*this->q[1]*this->q[3]*pow(this->kg[1],3) + 
           2*pow(this->q[1],3)*this->q[3]*pow(this->kg[1],3) + 2*this->q[1]*pow(this->q[3],3)*pow(this->kg[1],3) - 
           3*pow(this->q[0],4)*this->q[3]*this->kg[2] * this->kg[2] - 
           6*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3]*this->kg[2] * this->kg[2] + 
           pow(this->q[1],4)*this->q[3]*this->kg[2] * this->kg[2] - 
           6*this->q[0] * this->q[0]*pow(this->q[3],3)*this->kg[2] * this->kg[2] + 
           2*this->q[1] * this->q[1]*pow(this->q[3],3)*this->kg[2] * this->kg[2] + 
           pow(this->q[3],5)*this->kg[2] * this->kg[2] + 
           6*this->q[0] * this->q[0]*this->q[1]*this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] + 
           2*pow(this->q[1],3)*this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] + 
           2*this->q[1]*pow(this->q[3],3)*this->kg[1]*this->kg[2] * this->kg[2] + 
           (pow(this->q[0],4)*(-5*this->q[1] * this->q[1] - 8*this->q[3] * this->q[3] + 3*this->q[1]*this->kg[1]) - 
              (this->q[1] * this->q[1] + this->q[3] * this->q[3])*
               (pow(this->q[1],3)*this->kg[1] + this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 
                 this->q[1] * this->q[1]*this->kg[1] * this->kg[1] - 
                 this->q[3] * this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])) + 
              this->q[0] * this->q[0]*(pow(this->q[1],4) + 6*pow(this->q[1],3)*this->kg[1] + 
                 6*this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 
                 this->q[1] * this->q[1]*(this->q[3] * this->q[3] - 3*this->kg[1] * this->kg[1]) + 
                 3*this->q[3] * this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])))*this->kg[3] + 
           8*pow(p0,3)*this->q[0]*(this->q[1] * this->q[1] - this->q[1]*this->kg[1] + this->q[3]*(this->q[3] - this->kg[3]))*this->kg[3] + 
           4*pow(this->mRes,3)*this->mn*
            (2*p0*this->q[0]*this->q[3] - this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + this->q[1]*this->kg[1]*this->kg[3])  
+ 4*pow(p0,2)*(this->q[1] * this->q[1] - this->q[1]*this->kg[1] + this->q[3]*(this->q[3] - this->kg[3]))*
            (-(this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])) + 
              this->q[0] * this->q[0]*(this->q[3] - 5*this->kg[3]) + this->q[1]*this->kg[1]*this->kg[3]) + 
           2*p0*this->q[0]*(this->q[3]*(this->q[0] * this->q[0] - 5*(this->q[1] * this->q[1] + this->q[3] * this->q[3]) + 
                 8*this->q[1]*this->kg[1])*(this->q[0] * this->q[0] - this->kg[1] * this->kg[1] - this->kg[2] * this->kg[2]) - 
              (pow(this->q[1],4) + pow(this->q[3],4) + 5*pow(this->q[1],3)*this->kg[1] + 
                 5*this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 
                 this->q[0] * this->q[0]*
                  (-9*this->q[1] * this->q[1] - 13*this->q[3] * this->q[3] + 7*this->q[1]*this->kg[1]) + 
                 2*this->q[1] * this->q[1]*(this->q[3] * this->q[3] - 2*this->kg[1] * this->kg[1]) + 
                 4*this->q[3] * this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]))*this->kg[3] - 
              8*this->q[0] * this->q[0]*this->q[3]*this->kg[3] * this->kg[3]) + 
           this->mRes_2*(16*pow(p0,3)*this->q[0]*this->q[3] - 
              (this->q[1] * this->q[1] + this->q[3] * this->q[3])*
               (-(this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])) + this->q[1]*this->kg[1]*this->kg[3]) + 
              8*pow(p0,2)*
               (-(this->q[3]*(2*this->q[0] * this->q[0] + this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])) + 
                 this->q[1]*this->kg[1]*this->kg[3]) + 
              2*p0*this->q[0]*(6*this->q[0] * this->q[0]*this->q[3] - 
                 this->q[3]*(2*this->q[1] * this->q[1] + 2*this->q[3] * this->q[3] + 5*this->q[1]*this->kg[1] + 
                    this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) - 4*this->q[3] * this->q[3]*this->kg[3] + 
                 this->q[1]*(this->q[1] + this->kg[1])*this->kg[3]) - 
              this->q[0] * this->q[0]*(this->q[3]*
                  (this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + 4*pow(this->q[3] - this->kg[3],2)) + 
                 4*this->q[1] * this->q[1]*(this->q[3] - this->kg[3]) + this->q[1]*this->kg[1]*(-4*this->q[3] + 3*this->kg[3]))) + 
           2*this->mRes*this->mn*(4*pow(p0,3)*this->q[0]*this->q[3] - 2*pow(this->q[0],4)*this->q[3] + 
              2*this->q[0] * this->q[0]*(pow(this->q[3],3) + this->q[1]*this->q[3]*this->kg[1] + 
                 this->q[1] * this->q[1]*(this->q[3] - this->kg[3])) + 
              2*pow(p0,2)*
               (-(this->q[3]*(4*this->q[0] * this->q[0] + this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])) + 
                 this->q[1]*this->kg[1]*this->kg[3]) + 
              (-(this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])) + this->q[1]*this->kg[1]*this->kg[3])*
               (this->q[1]*(this->q[1] + this->kg[1]) + this->q[3]*(this->q[3] + this->kg[3])) + 
              p0*this->q[0]*(7*this->q[0] * this->q[0]*this->q[3] - this->q[1] * this->q[1]*(this->q[3] - 3*this->kg[3]) - 
                 this->q[1]*this->kg[1]*(this->q[3] + this->kg[3]) + 
                 this->q[3]*(-this->q[3] * this->q[3] + this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + 2*this->q[3]*this->kg[3]) 
)))) - 2*C3v*this->mn*(8*C5vNC*pow(this->mRes,3)*this->mn*p0*this->q[0]*this->q[3] + 
        16*C5vNC*this->mRes_2*pow(p0,3)*this->q[0]*this->q[3] + 
        8*C5vNC*this->mRes*this->mn*pow(p0,3)*this->q[0]*this->q[3] - 
        16*C5vNC*this->mRes_2*pow(p0,2)*this->q[0] * this->q[0]*this->q[3] - 
        16*C5vNC*this->mRes*this->mn*pow(p0,2)*this->q[0] * this->q[0]*this->q[3] - 
        4*C5vNC*this->mRes_2*p0*pow(this->q[0],3)*this->q[3] + 
        6*C5vNC*this->mRes*this->mn*p0*pow(this->q[0],3)*this->q[3] - 
        4*C5vNC*this->mRes_2*pow(this->q[0],4)*this->q[3] + 
        4*C5vNC*this->mRes*this->mn*pow(this->q[0],4)*this->q[3] + 
        4*C5vNC*pow(p0,2)*pow(this->q[0],4)*this->q[3] - 
        8*C5vNC*p0*pow(this->q[0],5)*this->q[3] + 3*C5vNC*pow(this->q[0],6)*this->q[3] + 
        12*C5vNC*this->mRes_2*p0*this->q[0]*this->q[1] * this->q[1]*this->q[3] + 
        6*C5vNC*this->mRes*this->mn*p0*this->q[0]*this->q[1] * this->q[1]*this->q[3] - 
        8*C5vNC*pow(p0,3)*this->q[0]*this->q[1] * this->q[1]*this->q[3] - 
        4*C5vNC*this->mRes_2*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3] - 
        4*C5vNC*this->mRes*this->mn*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3] + 
        20*C5vNC*pow(p0,2)*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3] - 
        18*C5vNC*p0*pow(this->q[0],3)*this->q[1] * this->q[1]*this->q[3] + 
        6*C5vNC*pow(this->q[0],4)*this->q[1] * this->q[1]*this->q[3] + 
        2*C5vNC*p0*this->q[0]*pow(this->q[1],4)*this->q[3] - C5vNC*this->q[0] * this->q[0]*pow(this->q[1],4)*this->q[3] + 
        12*C5vNC*this->mRes_2*p0*this->q[0]*pow(this->q[3],3) + 
        6*C5vNC*this->mRes*this->mn*p0*this->q[0]*pow(this->q[3],3) - 
        8*C5vNC*pow(p0,3)*this->q[0]*pow(this->q[3],3) - 
        4*C5vNC*this->mRes_2*this->q[0] * this->q[0]*pow(this->q[3],3) - 
        4*C5vNC*this->mRes*this->mn*this->q[0] * this->q[0]*pow(this->q[3],3) + 
        20*C5vNC*pow(p0,2)*this->q[0] * this->q[0]*pow(this->q[3],3) - 
        18*C5vNC*p0*pow(this->q[0],3)*pow(this->q[3],3) + 
        6*C5vNC*pow(this->q[0],4)*pow(this->q[3],3) + 
        4*C5vNC*p0*this->q[0]*this->q[1] * this->q[1]*pow(this->q[3],3) - 
        2*C5vNC*this->q[0] * this->q[0]*this->q[1] * this->q[1]*pow(this->q[3],3) + 
        2*C5vNC*p0*this->q[0]*pow(this->q[3],5) - C5vNC*this->q[0] * this->q[0]*pow(this->q[3],5) - 
        10*C5vNC*this->mRes_2*p0*this->q[0]*this->q[1]*this->q[3]*this->kg[1] - 
        2*C5vNC*this->mRes*this->mn*p0*this->q[0]*this->q[1]*this->q[3]*this->kg[1] + 
        6*C5vNC*this->mRes_2*this->q[0] * this->q[0]*this->q[1]*this->q[3]*this->kg[1] + 
        4*C5vNC*this->mRes*this->mn*this->q[0] * this->q[0]*this->q[1]*this->q[3]*this->kg[1] - 
        4*C5vNC*pow(p0,2)*this->q[0] * this->q[0]*this->q[1]*this->q[3]*this->kg[1] + 
        12*C5vNC*p0*pow(this->q[0],3)*this->q[1]*this->q[3]*this->kg[1] - 
        9*C5vNC*pow(this->q[0],4)*this->q[1]*this->q[3]*this->kg[1] + 
        C5vNC*this->q[0] * this->q[0]*pow(this->q[1],3)*this->q[3]*this->kg[1] + 
        C5vNC*this->q[0] * this->q[0]*this->q[1]*pow(this->q[3],3)*this->kg[1] + 
        4*C5vNC*this->mRes_2*pow(p0,2)*this->q[3]*this->kg[1] * this->kg[1] + 
        4*C5vNC*this->mRes*this->mn*pow(p0,2)*this->q[3]*this->kg[1] * this->kg[1] + 
        6*C5vNC*this->mRes_2*p0*this->q[0]*this->q[3]*this->kg[1] * this->kg[1] - 
        2*C5vNC*this->mRes*this->mn*p0*this->q[0]*this->q[3]*this->kg[1] * this->kg[1] + 
        4*C5vNC*this->mRes_2*this->q[0] * this->q[0]*this->q[3]*this->kg[1] * this->kg[1] - 
        4*C5vNC*this->mRes*this->mn*this->q[0] * this->q[0]*this->q[3]*this->kg[1] * this->kg[1] - 
        4*C5vNC*pow(p0,2)*this->q[0] * this->q[0]*this->q[3]*this->kg[1] * this->kg[1] + 
        8*C5vNC*p0*pow(this->q[0],3)*this->q[3]*this->kg[1] * this->kg[1] - 
        3*C5vNC*pow(this->q[0],4)*this->q[3]*this->kg[1] * this->kg[1] + 
        2*C5vNC*this->mRes_2*this->q[1] * this->q[1]*this->q[3]*this->kg[1] * this->kg[1] - 
        8*C5vNC*pow(p0,2)*this->q[1] * this->q[1]*this->q[3]*this->kg[1] * this->kg[1] + 
        12*C5vNC*p0*this->q[0]*this->q[1] * this->q[1]*this->q[3]*this->kg[1] * this->kg[1] - 
        5*C5vNC*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3]*this->kg[1] * this->kg[1] + 
        2*C5vNC*this->mRes_2*pow(this->q[3],3)*this->kg[1] * this->kg[1] - 
        8*C5vNC*pow(p0,2)*pow(this->q[3],3)*this->kg[1] * this->kg[1] + 
        12*C5vNC*p0*this->q[0]*pow(this->q[3],3)*this->kg[1] * this->kg[1] - 
        5*C5vNC*this->q[0] * this->q[0]*pow(this->q[3],3)*this->kg[1] * this->kg[1] - 
        4*C5vNC*this->mRes_2*this->q[1]*this->q[3]*pow(this->kg[1],3) - 
        8*C5vNC*p0*this->q[0]*this->q[1]*this->q[3]*pow(this->kg[1],3) + 
        8*C5vNC*this->q[0] * this->q[0]*this->q[1]*this->q[3]*pow(this->kg[1],3) + 
        std::complex<double>(0,8)*C5a*this->mRes_2*this->mn2*p0*this->q[1]*this->kg[2] - 
        std::complex<double>(0,8)*C5a*this->mRes*pow(this->mn,3)*p0*this->q[1]*this->kg[2] + 
        std::complex<double>(0,4)*C5a*this->mRes_2*this->mn2*this->q[0]*this->q[1]*this->kg[2] + 
        std::complex<double>(0,8)*C5a*this->mRes*pow(this->mn,3)*this->q[0]*this->q[1]*this->kg[2] - 
        std::complex<double>(0,8)*C5a*this->mn2*pow(p0,2)*this->q[0]*this->q[1]*this->kg[2] + 
        std::complex<double>(0,12)*C5a*this->mn2*p0*this->q[0] * this->q[0]*this->q[1]*this->kg[2] - 
        std::complex<double>(0,4)*C5a*this->mn2*pow(this->q[0],3)*this->q[1]*this->kg[2] - 
        std::complex<double>(0,4)*C5a*this->mn2*p0*this->q[1] * this->q[1]*this->kg[1]*this->kg[2] + 
        std::complex<double>(0,4)*C5a*this->mn2*this->q[0]*this->q[1] * this->q[1]*this->kg[1]*this->kg[2] + 
        4*C5vNC*this->mRes_2*pow(p0,2)*this->q[3]*this->kg[2] * this->kg[2] + 
        4*C5vNC*this->mRes*this->mn*pow(p0,2)*this->q[3]*this->kg[2] * this->kg[2] + 
        6*C5vNC*this->mRes_2*p0*this->q[0]*this->q[3]*this->kg[2] * this->kg[2] - 
        2*C5vNC*this->mRes*this->mn*p0*this->q[0]*this->q[3]*this->kg[2] * this->kg[2] + 
        4*C5vNC*this->mRes_2*this->q[0] * this->q[0]*this->q[3]*this->kg[2] * this->kg[2] - 
        4*C5vNC*this->mRes*this->mn*this->q[0] * this->q[0]*this->q[3]*this->kg[2] * this->kg[2] - 
        4*C5vNC*pow(p0,2)*this->q[0] * this->q[0]*this->q[3]*this->kg[2] * this->kg[2] + 
        8*C5vNC*p0*pow(this->q[0],3)*this->q[3]*this->kg[2] * this->kg[2] - 
        3*C5vNC*pow(this->q[0],4)*this->q[3]*this->kg[2] * this->kg[2] + 
        2*C5vNC*this->mRes_2*this->q[1] * this->q[1]*this->q[3]*this->kg[2] * this->kg[2] - 
        8*C5vNC*pow(p0,2)*this->q[1] * this->q[1]*this->q[3]*this->kg[2] * this->kg[2] + 
        12*C5vNC*p0*this->q[0]*this->q[1] * this->q[1]*this->q[3]*this->kg[2] * this->kg[2] - 
        5*C5vNC*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3]*this->kg[2] * this->kg[2] + 
        2*C5vNC*this->mRes_2*pow(this->q[3],3)*this->kg[2] * this->kg[2] - 
        8*C5vNC*pow(p0,2)*pow(this->q[3],3)*this->kg[2] * this->kg[2] + 
        12*C5vNC*p0*this->q[0]*pow(this->q[3],3)*this->kg[2] * this->kg[2] - 
        5*C5vNC*this->q[0] * this->q[0]*pow(this->q[3],3)*this->kg[2] * this->kg[2] - 
        4*C5vNC*this->mRes_2*this->q[1]*this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] - 
        8*C5vNC*p0*this->q[0]*this->q[1]*this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] + 
        8*C5vNC*this->q[0] * this->q[0]*this->q[1]*this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] + 
        (std::complex<double>(0,-4)*C5a*this->mn2*(p0 - this->q[0])*this->q[1]*this->q[3]*this->kg[2] + 
           C5vNC*(8*pow(p0,3)*this->q[0]*(this->q[1] * this->q[1] + this->q[3] * this->q[3]) + 
              4*pow(p0,2)*
               (2*this->q[1]*(this->q[1] * this->q[1] + this->q[3] * this->q[3])*this->kg[1] + 
                 this->q[0] * this->q[0]*(-6*this->q[3] * this->q[3] + this->q[1]*(-5*this->q[1] + this->kg[1]))) + 
              2*this->mRes*this->mn*(-2*pow(p0,2)*this->q[1]*this->kg[1] + 
                 2*this->q[0] * this->q[0]*this->q[1]*(-this->q[1] + this->kg[1]) + 
                 p0*this->q[0]*(2*this->q[3] * this->q[3] + this->q[1]*(3*this->q[1] + this->kg[1]))) - 
              2*this->mRes_2*
               (2*pow(p0,2)*this->q[1]*this->kg[1] - 
                 2*this->q[0] * this->q[0]*(this->q[1] * this->q[1] + 2*this->q[3] * this->q[3] - this->q[1]*this->kg[1]) + 
                 p0*this->q[0]*(-this->q[1] * this->q[1] + 4*this->q[3] * this->q[3] + 3*this->q[1]*this->kg[1]) + 
                 this->kg[1]*(pow(this->q[1],3) + this->q[1]*this->q[3] * this->q[3] - this->q[1] * this->q[1]*this->kg[1] + 
                    this->q[3] * this->q[3]*this->kg[1]) + this->q[3] * this->q[3]*this->kg[2] * this->kg[2]) - 
              this->q[0] * this->q[0]*(pow(this->q[1],4) + 
                 this->q[0] * this->q[0]*(8*this->q[3] * this->q[3] + 3*this->q[1]*(this->q[1] - this->kg[1])) - 
                 5*pow(this->q[1],3)*this->kg[1] - 5*this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 
                 this->q[1] * this->q[1]*(this->q[3] * this->q[3] + 4*this->kg[1] * this->kg[1]) - 
                 4*this->q[3] * this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])) + 
              2*p0*this->q[0]*(pow(this->q[1],4) + pow(this->q[3],4) - 6*pow(this->q[1],3)*this->kg[1] - 
                 6*this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 
                 this->q[0] * this->q[0]*
                  (7*this->q[1] * this->q[1] + 11*this->q[3] * this->q[3] - 4*this->q[1]*this->kg[1]) + 
                 2*this->q[1] * this->q[1]*(this->q[3] * this->q[3] + this->kg[1] * this->kg[1]) - 
                 2*this->q[3] * this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]))))*this->kg[3] + 
        2*C3vNC*this->mn*(this->mRes*this->q[3]*(this->kg[1]*
               (4*this->q[0]*(-p0 + this->q[0])*this->q[1] - 
                 (4*pow(p0,2) - 8*p0*this->q[0] + 3*this->q[0] * this->q[0] + 
                    this->q[1] * this->q[1] + this->q[3] * this->q[3])*this->kg[1]) - 
              (4*pow(p0,2) - 8*p0*this->q[0] + 3*this->q[0] * this->q[0] + 
                 this->q[1] * this->q[1] + this->q[3] * this->q[3])*this->kg[2] * this->kg[2]) + 
           this->mRes*this->q[1]*(4*(p0 - this->q[0])*this->q[0]*this->q[1] + 
              (4*pow(p0,2) - 8*p0*this->q[0] + 3*this->q[0] * this->q[0] + 
                 this->q[1] * this->q[1] + this->q[3] * this->q[3])*this->kg[1])*this->kg[3] + 
           2*this->mRes_2*this->mn*
            (this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) - this->q[1]*this->kg[1]*this->kg[3]) + 
           2*pow(this->mRes,3)*(this->q[3]*
               (4*p0*this->q[0] + this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) - this->q[1]*this->kg[1]*this->kg[3]) + 
           2*this->mn*(p0 - this->q[0])*(-(this->q[3]*
                 (-4*pow(p0,2)*this->q[0] + 8*p0*this->q[0] * this->q[0] - 
                   2*p0*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + 
                   this->q[0]*(-3*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] + 
                      this->q[1]*this->kg[1] + this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]))) - 
              (2*p0*this->q[1]*this->kg[1] + this->q[0]*(this->q[1] * this->q[1] + 2*this->q[3] * this->q[3] - this->q[1]*this->kg[1]))*
               this->kg[3])) + 2*C4vNC*this->mRes*
         (4*this->mRes_2*this->mn*(p0 - this->q[0])*this->q[0]*this->q[3] + 
           this->mn*(p0 - this->q[0])*(4*pow(p0,2)*this->q[0]*this->q[3] + 3*pow(this->q[0],3)*this->q[3] - 
              this->q[0]*this->q[3]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] + this->q[1]*this->kg[1] + this->kg[1] * this->kg[1] + 
                 this->kg[2] * this->kg[2]) - 
              this->q[0]*(this->q[1] * this->q[1] + 2*this->q[3] * this->q[3] - this->q[1]*this->kg[1])*this->kg[3] + 
              2*p0*(-4*this->q[0] * this->q[0]*this->q[3] + 
                 this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) - this->q[1]*this->kg[1]*this->kg[3])) + 
           this->mRes*(8*pow(p0,3)*this->q[0]*this->q[3] + 
              2*pow(p0,2)*(-8*this->q[0] * this->q[0]*this->q[3] + 
                 this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) - this->q[1]*this->kg[1]*this->kg[3]) + 
              (-(this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])) + this->q[1]*this->kg[1]*this->kg[3])*
               (this->q[1]*(this->q[1] + this->kg[1]) + this->q[3]*(this->q[3] + this->kg[3])) - 
              this->q[0] * this->q[0]*(this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + 
                 3*this->q[1] * this->q[1]*this->kg[3] - this->q[1]*this->kg[1]*(3*this->q[3] + this->kg[3])) + 
              p0*this->q[0]*(6*this->q[0] * this->q[0]*this->q[3] + this->q[1] * this->q[1]*(-2*this->q[3] + this->kg[3]) - 
                 this->q[1]*this->kg[1]*(5*this->q[3] + this->kg[3]) + 
                 this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] - 2*this->q[3]*(this->q[3] + 2*this->kg[3])))))))/
  (24.*this->mRes_2*pow(this->mn,5));
    
	tr[3][1] = -(2*C3v*this->mn*(-4*C5vNC*pow(this->mRes,3)*this->mn*this->q[0]*this->q[1]*this->q[3] - 
        8*C5vNC*this->mRes_2*pow(p0,2)*this->q[0]*this->q[1]*this->q[3] - 
        4*C5vNC*this->mRes*this->mn*pow(p0,2)*this->q[0]*this->q[1]*this->q[3] - 
        2*C5vNC*this->mRes_2*p0*this->q[0] * this->q[0]*this->q[1]*this->q[3] + 
        2*C5vNC*this->mRes*this->mn*p0*this->q[0] * this->q[0]*this->q[1]*this->q[3] + 
        8*C5vNC*pow(p0,3)*this->q[0] * this->q[0]*this->q[1]*this->q[3] + 
        5*C5vNC*this->mRes_2*pow(this->q[0],3)*this->q[1]*this->q[3] - 
        2*C5vNC*this->mRes*this->mn*pow(this->q[0],3)*this->q[1]*this->q[3] - 
        24*C5vNC*pow(p0,2)*pow(this->q[0],3)*this->q[1]*this->q[3] + 
        26*C5vNC*p0*pow(this->q[0],4)*this->q[1]*this->q[3] - 8*C5vNC*pow(this->q[0],5)*this->q[1]*this->q[3] - 
        C5vNC*this->mRes_2*this->q[0]*pow(this->q[1],3)*this->q[3] - 
        2*C5vNC*p0*this->q[0] * this->q[0]*pow(this->q[1],3)*this->q[3] - 
        C5vNC*this->mRes_2*this->q[0]*this->q[1]*pow(this->q[3],3) - 
        2*C5vNC*p0*this->q[0] * this->q[0]*this->q[1]*pow(this->q[3],3) + 
        4*C5vNC*pow(this->mRes,3)*this->mn*this->q[0]*this->q[3]*this->kg[1] + 
        20*C5vNC*this->mRes_2*pow(p0,2)*this->q[0]*this->q[3]*this->kg[1] + 
        8*C5vNC*this->mRes*this->mn*pow(p0,2)*this->q[0]*this->q[3]*this->kg[1] - 
        8*C5vNC*this->mRes*this->mn*p0*this->q[0] * this->q[0]*this->q[3]*this->kg[1] - 
        3*C5vNC*this->mRes_2*pow(this->q[0],3)*this->q[3]*this->kg[1] - 
        2*C5vNC*this->mRes*this->mn*pow(this->q[0],3)*this->q[3]*this->kg[1] + 
        4*C5vNC*pow(p0,2)*pow(this->q[0],3)*this->q[3]*this->kg[1] - 
        8*C5vNC*p0*pow(this->q[0],4)*this->q[3]*this->kg[1] + 5*C5vNC*pow(this->q[0],5)*this->q[3]*this->kg[1] - 
        6*C5vNC*this->mRes_2*p0*this->q[1] * this->q[1]*this->q[3]*this->kg[1] - 
        2*C5vNC*this->mRes*this->mn*p0*this->q[1] * this->q[1]*this->q[3]*this->kg[1] + 
        3*C5vNC*this->mRes_2*this->q[0]*this->q[1] * this->q[1]*this->q[3]*this->kg[1] + 
        2*C5vNC*this->mRes*this->mn*this->q[0]*this->q[1] * this->q[1]*this->q[3]*this->kg[1] - 
        4*C5vNC*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3]*this->kg[1] + 
        3*C5vNC*pow(this->q[0],3)*this->q[1] * this->q[1]*this->q[3]*this->kg[1] - 
        6*C5vNC*this->mRes_2*p0*pow(this->q[3],3)*this->kg[1] - 
        2*C5vNC*this->mRes*this->mn*p0*pow(this->q[3],3)*this->kg[1] + 
        3*C5vNC*this->mRes_2*this->q[0]*pow(this->q[3],3)*this->kg[1] + 
        4*C5vNC*this->mRes*this->mn*this->q[0]*pow(this->q[3],3)*this->kg[1] - 
        4*C5vNC*pow(p0,2)*this->q[0]*pow(this->q[3],3)*this->kg[1] + 
        8*C5vNC*p0*this->q[0] * this->q[0]*pow(this->q[3],3)*this->kg[1] - 
        6*C5vNC*pow(this->q[0],3)*pow(this->q[3],3)*this->kg[1] + 
        C5vNC*this->q[0]*this->q[1] * this->q[1]*pow(this->q[3],3)*this->kg[1] + C5vNC*this->q[0]*pow(this->q[3],5)*this->kg[1] + 
        4*C5vNC*this->mRes_2*p0*this->q[1]*this->q[3]*this->kg[1] * this->kg[1] - 
        6*C5vNC*this->mRes_2*this->q[0]*this->q[1]*this->q[3]*this->kg[1] * this->kg[1] + 
        2*C5vNC*this->mRes*this->mn*this->q[0]*this->q[1]*this->q[3]*this->kg[1] * this->kg[1] + 
        12*C5vNC*pow(p0,2)*this->q[0]*this->q[1]*this->q[3]*this->kg[1] * this->kg[1] - 
        20*C5vNC*p0*this->q[0] * this->q[0]*this->q[1]*this->q[3]*this->kg[1] * this->kg[1] + 
        7*C5vNC*pow(this->q[0],3)*this->q[1]*this->q[3]*this->kg[1] * this->kg[1] + 
        C5vNC*this->q[0]*pow(this->q[1],3)*this->q[3]*this->kg[1] * this->kg[1] + 
        C5vNC*this->q[0]*this->q[1]*pow(this->q[3],3)*this->kg[1] * this->kg[1] + 
        2*C5vNC*this->mRes_2*this->q[0]*this->q[3]*pow(this->kg[1],3) + 
        4*C5vNC*p0*this->q[0] * this->q[0]*this->q[3]*pow(this->kg[1],3) - 
        4*C5vNC*pow(this->q[0],3)*this->q[3]*pow(this->kg[1],3) + 
        4*C5vNC*p0*this->q[1] * this->q[1]*this->q[3]*pow(this->kg[1],3) - 
        4*C5vNC*this->q[0]*this->q[1] * this->q[1]*this->q[3]*pow(this->kg[1],3) - 
        4*C5vNC*p0*pow(this->q[3],3)*pow(this->kg[1],3) + 
        4*C5vNC*this->q[0]*pow(this->q[3],3)*pow(this->kg[1],3) - 
        std::complex<double>(0,16)*C5a*pow(this->mRes,3)*pow(this->mn,3)*this->kg[2] + 
        std::complex<double>(0,16)*C5a*this->mRes_2*this->mn2*pow(p0,2)*this->kg[2] - 
        std::complex<double>(0,12)*C5a*this->mRes_2*this->mn2*p0*this->q[0]*this->kg[2] - 
        std::complex<double>(0,4)*C5a*this->mRes*pow(this->mn,3)*p0*this->q[0]*this->kg[2] + 
        std::complex<double>(0,2)*C5a*this->mRes_2*this->mn2*this->q[0] * this->q[0]*this->kg[2] + 
        std::complex<double>(0,2)*C5a*this->mRes*pow(this->mn,3)*this->q[0] * this->q[0]*this->kg[2] - 
        std::complex<double>(0,4)*C5a*this->mRes*pow(this->mn,3)*this->q[1] * this->q[1]*this->kg[2] - 
        std::complex<double>(0,4)*C5a*this->mn2*p0*this->q[0]*this->q[1] * this->q[1]*this->kg[2] + 
        std::complex<double>(0,2)*C5a*this->mn2*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[2] + 
        std::complex<double>(0,2)*C5a*this->mRes_2*this->mn2*this->q[3] * this->q[3]*this->kg[2] - 
        std::complex<double>(0,2)*C5a*this->mRes*pow(this->mn,3)*this->q[3] * this->q[3]*this->kg[2] - 
        std::complex<double>(0,6)*C5a*this->mRes_2*this->mn2*this->q[1]*this->kg[1]*this->kg[2] - 
        std::complex<double>(0,6)*C5a*this->mRes*pow(this->mn,3)*this->q[1]*this->kg[1]*this->kg[2] - 
        std::complex<double>(0,4)*C5a*this->mn2*p0*this->q[0]*this->q[1]*this->kg[1]*this->kg[2] + 
        std::complex<double>(0,2)*C5a*this->mn2*this->q[0] * this->q[0]*this->q[1]*this->kg[1]*this->kg[2] - 
        std::complex<double>(0,2)*C5a*this->mn2*pow(this->q[1],3)*this->kg[1]*this->kg[2] - 
        std::complex<double>(0,2)*C5a*this->mn2*this->q[1] * this->q[1]*this->kg[1] * this->kg[1]*this->kg[2] - 
        2*C5vNC*this->mRes_2*p0*this->q[1]*this->q[3]*this->kg[2] * this->kg[2] - 
        2*C5vNC*this->mRes*this->mn*p0*this->q[1]*this->q[3]*this->kg[2] * this->kg[2] - 
        4*C5vNC*this->mRes_2*this->q[0]*this->q[1]*this->q[3]*this->kg[2] * this->kg[2] + 
        4*C5vNC*this->mRes*this->mn*this->q[0]*this->q[1]*this->q[3]*this->kg[2] * this->kg[2] + 
        12*C5vNC*pow(p0,2)*this->q[0]*this->q[1]*this->q[3]*this->kg[2] * this->kg[2] - 
        20*C5vNC*p0*this->q[0] * this->q[0]*this->q[1]*this->q[3]*this->kg[2] * this->kg[2] + 
        7*C5vNC*pow(this->q[0],3)*this->q[1]*this->q[3]*this->kg[2] * this->kg[2] + 
        C5vNC*this->q[0]*pow(this->q[1],3)*this->q[3]*this->kg[2] * this->kg[2] + 
        C5vNC*this->q[0]*this->q[1]*pow(this->q[3],3)*this->kg[2] * this->kg[2] + 
        2*C5vNC*this->mRes_2*this->q[0]*this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] + 
        4*C5vNC*p0*this->q[0] * this->q[0]*this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] - 
        4*C5vNC*pow(this->q[0],3)*this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] + 
        4*C5vNC*p0*this->q[1] * this->q[1]*this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] - 
        4*C5vNC*this->q[0]*this->q[1] * this->q[1]*this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] - 
        4*C5vNC*p0*pow(this->q[3],3)*this->kg[1]*this->kg[2] * this->kg[2] + 
        4*C5vNC*this->q[0]*pow(this->q[3],3)*this->kg[1]*this->kg[2] * this->kg[2] + 
        (std::complex<double>(0,-2)*C5a*this->mn2*this->q[3]*(2*this->mRes*this->mn + this->q[1]*(this->q[1] + this->kg[1]))*
            this->kg[2] + C5vNC*(-8*pow(p0,3)*this->q[0] * this->q[0]*this->q[1] + 
              3*pow(this->q[0],5)*(this->q[1] - this->kg[1]) + 
              4*pow(p0,2)*this->q[0]*
               (this->q[1]*this->q[3] * this->q[3] + this->q[0] * this->q[0]*(5*this->q[1] - this->kg[1]) - 
                 2*this->q[1] * this->q[1]*this->kg[1] + this->q[3] * this->q[3]*this->kg[1]) + 
              pow(this->q[0],3)*(pow(this->q[1],3) + 6*this->q[1]*this->q[3] * this->q[3] - 
                 5*this->q[1] * this->q[1]*this->kg[1] + 2*this->q[3] * this->q[3]*this->kg[1] + 
                 4*this->q[1]*this->kg[1] * this->kg[1]) + 
              2*this->mRes_2*
               (-(this->q[0] * this->q[0]*(p0 + 2*this->q[0])*this->q[1]) + 
                 this->q[0]*(2*pow(p0,2) + 3*p0*this->q[0] + 2*this->q[0] * this->q[0] + 
                    this->q[1] * this->q[1])*this->kg[1] + 2*(p0 - this->q[0])*this->q[3] * this->q[3]*this->kg[1] - 
                 this->q[0]*this->q[1]*this->kg[1] * this->kg[1]) + 
              2*this->mRes*this->mn*this->q[0]*((2*this->q[0] * this->q[0] - this->q[3] * this->q[3])*(this->q[1] - this->kg[1]) + 
                 2*pow(p0,2)*this->kg[1] - p0*this->q[0]*(3*this->q[1] + this->kg[1])) - 
              2*p0*(pow(this->q[0],4)*(7*this->q[1] - 4*this->kg[1]) + 
                 this->q[0] * this->q[0]*
                  (pow(this->q[1],3) + 5*this->q[1]*this->q[3] * this->q[3] - 
                    6*this->q[1] * this->q[1]*this->kg[1] + 4*this->q[3] * this->q[3]*this->kg[1] + 
                    2*this->q[1]*this->kg[1] * this->kg[1]) - 
                 2*this->q[1]*this->q[3] * this->q[3]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])) - 
              this->q[0]*this->q[3] * this->q[3]*(pow(this->q[1],3) - this->q[1] * this->q[1]*this->kg[1] - 
                 this->q[3] * this->q[3]*this->kg[1] + 
                 this->q[1]*(this->q[3] * this->q[3] + 8*this->kg[1] * this->kg[1] + 4*this->kg[2] * this->kg[2]))))*this->kg[3]  
+ 2*C3vNC*this->mn*(this->mRes*this->q[3]*(this->q[0]*this->q[1]*
               (-4*pow(p0,2) + 8*p0*this->q[0] - 5*this->q[0] * this->q[0] + 
                 this->q[1] * this->q[1] + this->q[3] * this->q[3]) + 
              4*this->q[0]*(-2*p0 + this->q[0])*(-p0 + this->q[0])*this->kg[1] + 
              4*(-p0 + this->q[0])*this->q[1]*this->kg[2] * this->kg[2]) + 
           this->mRes*this->q[0]*(4*this->q[0]*(-p0 + this->q[0])*this->q[1] - 
              (4*pow(p0,2) - 8*p0*this->q[0] + 3*this->q[0] * this->q[0] + 
                 this->q[1] * this->q[1] + this->q[3] * this->q[3])*this->kg[1])*this->kg[3] + 
           2*pow(this->mRes,3)*(this->q[0]*this->q[1]*this->q[3] - 4*p0*this->q[3]*this->kg[1] + 
              this->q[0]*this->kg[1]*(-2*this->q[3] + this->kg[3])) + 
           2*this->mRes_2*this->mn*
            (this->q[0]*this->q[1]*this->q[3] + 4*p0*this->q[3]*this->kg[1] + this->q[0]*this->kg[1]*(-2*this->q[3] + this->kg[3])) + 
           2*this->mn*(p0 - this->q[0])*(2*p0*this->q[0]*(this->q[1]*this->q[3] + this->kg[1]*(2*this->q[3] + this->kg[3])) - 
              this->q[0] * this->q[0]*(this->q[1]*(this->q[3] - this->kg[3]) + this->kg[1]*(2*this->q[3] + this->kg[3])) + 
              this->q[3]*(this->q[1] * this->q[1]*this->kg[1] + this->q[1]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + 
                 this->q[3]*this->kg[1]*(this->q[3] + 2*this->kg[3])))) + 
        C4vNC*this->mRes*(4*this->mRes_2*this->mn*this->q[0]*this->q[3]*(this->q[1] + this->kg[1]) + 
           2*this->mn*(p0 - this->q[0])*(2*p0*this->q[0]*(this->q[1]*this->q[3] + this->kg[1]*(2*this->q[3] + this->kg[3])) - 
              this->q[0] * this->q[0]*(this->q[1]*(this->q[3] - this->kg[3]) + this->kg[1]*(2*this->q[3] + this->kg[3])) + 
              this->q[3]*(this->q[1] * this->q[1]*this->kg[1] + this->q[1]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + 
                 this->q[3]*this->kg[1]*(this->q[3] + 2*this->kg[3]))) + 
           this->mRes*(this->q[0]*this->q[3]*(pow(this->q[1],3) - this->q[1] * this->q[1]*this->kg[1] + 
                 this->q[1]*(this->q[3] * this->q[3] + 4*this->kg[1] * this->kg[1] + 6*this->kg[2] * this->kg[2]) + 
                 this->kg[1]*(this->q[3] * this->q[3] + 2*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]))) - 
              2*this->q[0]*(this->q[1] * this->q[1]*this->kg[1] - this->q[3] * this->q[3]*this->kg[1] + 
                 this->q[1]*(this->q[3] * this->q[3] + this->kg[1] * this->kg[1]))*this->kg[3] - 
              pow(this->q[0],3)*(5*this->q[1]*this->q[3] + this->q[3]*this->kg[1] - 6*this->q[1]*this->kg[3] + 2*this->kg[1]*this->kg[3]) + 
              4*pow(p0,2)*this->q[0]*(2*this->q[1]*this->q[3] + this->kg[1]*(5*this->q[3] + this->kg[3])) + 
              2*p0*(-(this->q[0] * this->q[0]*(this->kg[1]*(6*this->q[3] - this->kg[3]) + this->q[1]*(this->q[3] + this->kg[3]))) + 
                 this->q[3]*(3*this->q[1] * this->q[1]*this->kg[1] + 
                    this->q[1]*(4*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + 
                    this->q[3]*this->kg[1]*(3*this->q[3] + 4*this->kg[3])))))) + 
     C5v*(C4vNC*this->mRes*(this->q[3]*(this->q[0]*this->q[1]*
               (-12*pow(p0,2) + 20*p0*this->q[0] - 9*this->q[0] * this->q[0] + 
                 this->q[1] * this->q[1] + this->q[3] * this->q[3]) + 
              (4*pow(p0,2)*this->q[0] - 8*p0*this->q[0] * this->q[0] + 
                 3*pow(this->q[0],3) - 4*p0*this->q[1] * this->q[1] + 
                 5*this->q[0]*this->q[1] * this->q[1] + (4*p0 - 3*this->q[0])*this->q[3] * this->q[3])*this->kg[1])*
            (this->q[0] * this->q[0] - this->kg[1] * this->kg[1] - this->kg[2] * this->kg[2]) + 
           (8*pow(p0,3)*this->q[0] * this->q[0]*(-this->q[1] + this->kg[1]) + 
              4*pow(p0,2)*this->q[0]*
               (5*this->q[0] * this->q[0]*(this->q[1] - this->kg[1]) + 2*this->q[3] * this->q[3]*this->kg[1] + 
                 this->q[1]*this->kg[1]*(-this->q[1] + this->kg[1])) + 
              this->q[0]*(pow(this->q[0],4)*(5*this->q[1] - 3*this->kg[1]) + 
                 this->q[1]*this->kg[1]*(pow(this->q[1],3) + this->q[1]*this->q[3] * this->q[3] + 
                    this->q[1] * this->q[1]*this->kg[1] - 7*this->q[3] * this->q[3]*this->kg[1]) + 
                 this->q[0] * this->q[0]*
                  (-pow(this->q[1],3) - 6*this->q[1] * this->q[1]*this->kg[1] + 
                    3*this->q[3] * this->q[3]*this->kg[1] + 
                    3*this->q[1]*(this->q[3] * this->q[3] + this->kg[1] * this->kg[1])) - 
                 4*this->q[1]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2]) - 
              2*p0*(pow(this->q[0],4)*(9*this->q[1] - 7*this->kg[1]) + 
                 this->q[0] * this->q[0]*
                  (-pow(this->q[1],3) + this->q[1]*this->q[3] * this->q[3] - 5*this->q[1] * this->q[1]*this->kg[1] + 
                    5*this->q[3] * this->q[3]*this->kg[1] + 4*this->q[1]*this->kg[1] * this->kg[1]) - 
                 2*this->q[1]*this->q[3] * this->q[3]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])))*this->kg[3] + 
           8*this->mRes*this->mn*(p0 - this->q[0])*
            (this->q[3]*(this->q[0] * this->q[0]*(this->q[1] - 2*this->kg[1]) + 
                 (this->q[1] * this->q[1] + this->q[3] * this->q[3])*this->kg[1] + 
                 2*p0*this->q[0]*(this->q[1] + 2*this->kg[1]) - this->q[1]*this->kg[2] * this->kg[2]) + 
              this->q[0]*(-2*p0*this->kg[1] + this->q[0]*(-this->q[1] + this->kg[1]))*this->kg[3]) + 
           4*this->mRes_2*(2*p0*this->q[3]*
               (this->q[0] * this->q[0]*(this->q[1] - this->kg[1]) + 
                 (this->q[1] * this->q[1] + this->q[3] * this->q[3])*this->kg[1] - this->q[1]*this->kg[2] * this->kg[2]) - 
              2*p0*this->q[0] * this->q[0]*this->q[1]*this->kg[3] + 
              4*pow(p0,2)*this->q[0]*(this->q[1]*this->q[3] + 2*this->q[3]*this->kg[1] - this->kg[1]*this->kg[3]) + 
              this->q[0]*(this->q[3] - this->kg[3])*(this->q[0] * this->q[0]*(this->q[1] - this->kg[1]) + 
                 this->q[3]*(this->q[3]*this->kg[1] - this->q[1]*this->kg[3])))) + 
        2*this->mn*(std::complex<double>(0,-1)*C5a*this->mRes*this->mn*this->kg[2]*
            (-(this->q[0] * this->q[0]*this->q[1] * this->q[1]) + 2*this->q[0] * this->q[0]*this->q[3] * this->q[3] - 
              this->q[0] * this->q[0]*this->q[1]*this->kg[1] + pow(this->q[1],3)*this->kg[1] + 
              this->q[1] * this->q[1]*this->kg[1] * this->kg[1] - 2*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
              2*this->mRes*this->mn*(2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[3] * this->q[3] + 
                 this->q[1]*this->kg[1]) - 2*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] - 
              2*this->q[0] * this->q[0]*this->q[3]*this->kg[3] + this->q[1] * this->q[1]*this->q[3]*this->kg[3] + 
              3*this->q[1]*this->q[3]*this->kg[1]*this->kg[3] + 
              2*this->mRes_2*
               (2*p0*this->q[0] - this->q[0] * this->q[0] - this->q[3] * this->q[3] + this->q[1]*this->kg[1] + 
                 2*this->q[3]*this->kg[3]) + 2*p0*this->q[0]*(this->q[1]*(this->q[1] + this->kg[1]) + 2*this->q[3]*this->kg[3])) + 
           C3vNC*(this->q[3]*(this->q[0]*this->q[1]*(-12*pow(p0,2) + 20*p0*this->q[0] - 
                    9*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3]) + 
                 (4*pow(p0,2)*this->q[0] - 8*p0*this->q[0] * this->q[0] + 
                    3*pow(this->q[0],3) - 4*p0*this->q[1] * this->q[1] + 
                    5*this->q[0]*this->q[1] * this->q[1] + (4*p0 - 3*this->q[0])*this->q[3] * this->q[3])*this->kg[1])*
               (this->q[0] * this->q[0] - this->kg[1] * this->kg[1] - this->kg[2] * this->kg[2]) + 
              4*pow(this->mRes,3)*this->mn*this->q[0]*this->kg[1]*(this->q[3] - this->kg[3]) + 
              (8*pow(p0,3)*this->q[0] * this->q[0]*(-this->q[1] + this->kg[1]) + 
                 4*pow(p0,2)*this->q[0]*
                  (5*this->q[0] * this->q[0]*(this->q[1] - this->kg[1]) + 2*this->q[3] * this->q[3]*this->kg[1] + 
                    this->q[1]*this->kg[1]*(-this->q[1] + this->kg[1])) + 
                 this->q[0]*(pow(this->q[0],4)*(5*this->q[1] - 3*this->kg[1]) + 
                    this->q[1]*this->kg[1]*(pow(this->q[1],3) + this->q[1]*this->q[3] * this->q[3] + 
                       this->q[1] * this->q[1]*this->kg[1] - 7*this->q[3] * this->q[3]*this->kg[1]) + 
                    this->q[0] * this->q[0]*
                     (-pow(this->q[1],3) - 6*this->q[1] * this->q[1]*this->kg[1] + 
                       3*this->q[3] * this->q[3]*this->kg[1] + 
                       3*this->q[1]*(this->q[3] * this->q[3] + this->kg[1] * this->kg[1])) - 
                    4*this->q[1]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2]) - 
                 2*p0*(pow(this->q[0],4)*(9*this->q[1] - 7*this->kg[1]) + 
                    this->q[0] * this->q[0]*
                     (-pow(this->q[1],3) + this->q[1]*this->q[3] * this->q[3] - 
                       5*this->q[1] * this->q[1]*this->kg[1] + 5*this->q[3] * this->q[3]*this->kg[1] + 
                       4*this->q[1]*this->kg[1] * this->kg[1]) - 
                    2*this->q[1]*this->q[3] * this->q[3]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])))*
               this->kg[3] + 2*this->mRes*this->mn*
               (this->q[3]*(2*pow(p0,2)*this->q[0]*(this->q[1] + 2*this->kg[1]) + 
                    p0*((this->q[1] * this->q[1] + this->q[3] * this->q[3])*this->kg[1] - 
                       this->q[0] * this->q[0]*(this->q[1] + 6*this->kg[1]) - this->q[1]*this->kg[2] * this->kg[2]) + 
                    this->q[0]*(-((this->q[1] * this->q[1] + this->q[3] * this->q[3])*this->kg[1]) + 
                       this->q[1]*this->kg[1] * this->kg[1] + pow(this->kg[1],3) + 
                       this->q[0] * this->q[0]*(-2*this->q[1] + this->kg[1]) + 
                       (2*this->q[1] + this->kg[1])*this->kg[2] * this->kg[2])) - 
                 this->q[0]*(-2*this->q[0] * this->q[0]*this->q[1] + p0*this->q[0]*(3*this->q[1] - this->kg[1]) + 
                    2*pow(p0,2)*this->kg[1] + this->q[1]*this->kg[1]*(this->q[1] + this->kg[1]))*this->kg[3]) + 
              this->mRes_2*(2*p0*this->q[3]*
                  (this->q[0] * this->q[0]*(3*this->q[1] - 4*this->kg[1]) + 
                    this->kg[1]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1]) - 
                    this->q[1]*this->kg[2] * this->kg[2]) - 
                 2*p0*(-2*this->q[3] * this->q[3]*this->kg[1] + this->q[0] * this->q[0]*(this->q[1] + this->kg[1]))*
                  this->kg[3] + 4*pow(p0,2)*this->q[0]*
                  (this->q[1]*this->q[3] + 5*this->q[3]*this->kg[1] - 2*this->kg[1]*this->kg[3]) + 
                 this->q[0]*(this->q[3] * this->q[3]*this->kg[1]*(3*this->q[3] - 5*this->kg[3]) + 
                    this->q[1] * this->q[1]*this->kg[1]*(this->q[3] + this->kg[3]) + 
                    this->q[0] * this->q[0]*
                     (4*this->q[1]*this->q[3] - this->q[3]*this->kg[1] - 4*this->q[1]*this->kg[3] + 3*this->kg[1]*this->kg[3]) - 
                    2*this->q[1]*this->q[3]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + 
                       (this->q[3] - this->kg[3])*this->kg[3]))))) + 
        C5vNC*(-8*this->mn*(p0 - this->q[0])*
            (this->q[3]*(4*p0*this->q[0]*this->q[1] + (this->q[1] - this->q[3])*(this->q[1] + this->q[3])*this->kg[1] + 
                 this->q[0] * this->q[0]*(-2*this->q[1] + this->kg[1]))*
               (this->q[0] * this->q[0] - this->kg[1] * this->kg[1] - this->kg[2] * this->kg[2]) + 
              (4*pow(p0,2)*this->q[0] * this->q[0]*this->q[1] - 4*p0*pow(this->q[0],3)*this->q[1] + 
                 pow(this->q[0],4)*(this->q[1] - this->kg[1]) + 
                 2*p0*this->q[0]*(this->q[0] * this->q[0] + this->q[1] * this->q[1] - this->q[3] * this->q[3])*
                  this->kg[1] + this->q[0] * this->q[0]*
                  (-(this->q[1] * this->q[1]*this->kg[1]) + this->q[3] * this->q[3]*this->kg[1] + 
                    this->q[1]*(this->q[3] * this->q[3] + this->kg[1] * this->kg[1])) - 
                 this->q[1]*this->q[3] * this->q[3]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]))*this->kg[3]) - 
           8*this->mRes_2*this->mn*(p0 - this->q[0])*
            (this->q[3]*(this->q[1] * this->q[1]*this->kg[1] - this->q[1]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + 
                 this->q[3]*this->kg[1]*(this->q[3] - 2*this->kg[3])) + 
              this->q[0] * this->q[0]*(this->q[1]*(this->q[3] - this->kg[3]) + this->kg[1]*this->kg[3]) + 
              2*p0*this->q[0]*(this->q[1]*this->q[3] + this->kg[1]*(-2*this->q[3] + this->kg[3]))) - 
           8*pow(this->mRes,3)*p0*
            (this->q[3]*(this->q[1] * this->q[1]*this->kg[1] - this->q[1]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + 
                 this->q[3]*this->kg[1]*(this->q[3] - 2*this->kg[3])) + 
              this->q[0] * this->q[0]*(2*this->q[1]*this->q[3] - this->q[3]*this->kg[1] - 2*this->q[1]*this->kg[3] + 2*this->kg[1]*this->kg[3]) + 
              2*p0*this->q[0]*(this->q[1]*this->q[3] + this->kg[1]*(-2*this->q[3] + this->kg[3]))) + 
           this->mRes*(8*pow(p0,3)*this->q[0] * this->q[0]*(this->q[1]*(this->q[3] - 6*this->kg[3]) + this->kg[1]*this->kg[3]) - 
              4*pow(p0,2)*this->q[0]*
               (pow(this->q[3],3)*this->kg[1] - 
                 this->q[3]*(10*this->q[1] - this->kg[1])*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + 
                 this->q[1]*(5*this->q[1] - this->kg[1])*this->kg[1]*this->kg[3] - this->q[3] * this->q[3]*(this->q[1] + 5*this->kg[1])*this->kg[3] + 
                 this->q[0] * this->q[0]*(13*this->q[1]*this->q[3] - 2*this->q[3]*this->kg[1] - 14*this->q[1]*this->kg[3] + 8*this->kg[1]*this->kg[3]) 
) + 2*p0*(pow(this->q[0],4)*(15*this->q[1]*this->q[3] - 8*this->q[3]*this->kg[1] - 8*this->q[1]*this->kg[3] + 7*this->kg[1]*this->kg[3]) + 
                 4*this->q[3]*((this->q[1] - this->q[3])*(this->q[1] + this->q[3])*this->kg[1]*
                     (this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + 
                    this->q[1]*this->q[3]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])*this->kg[3]) - 
                 this->q[0] * this->q[0]*(pow(this->q[1],3)*this->q[3] + 
                    this->q[1]*this->q[3]*(this->q[3] * this->q[3] + 
                       12*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])) + 
                    this->q[1] * this->q[1]*this->kg[1]*(4*this->q[3] - 7*this->kg[3]) + 
                    6*this->q[1]*(this->q[3] * this->q[3] + this->kg[1] * this->kg[1])*this->kg[3] + 
                    this->q[3]*this->kg[1]*(-6*
                        (this->q[3] * this->q[3] + this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + 
                       5*this->q[3]*this->kg[3]))) + 
              this->q[0]*(-(pow(this->q[0],4)*this->q[1]*this->q[3]) + 
                 pow(this->q[3],3)*this->kg[1]*
                  (this->q[1] * this->q[1] + this->q[3] * this->q[3] - this->kg[1] * this->kg[1] - 
                    this->kg[2] * this->kg[2]) - 
                 (this->q[1] * this->q[1] + this->q[3] * this->q[3])*
                  (this->q[1] * this->q[1]*this->kg[1] + this->q[3] * this->q[3]*this->kg[1] + 
                    this->q[1]*(this->q[3] - this->kg[1])*(this->q[3] + this->kg[1]))*this->kg[3] + 
                 this->q[1] * this->q[1]*this->q[3]*this->kg[1]*this->kg[3] * this->kg[3] + 
                 this->q[0] * this->q[0]*(pow(this->q[1],3)*this->q[3] + 
                    this->q[1] * this->q[1]*this->kg[1]*(-this->q[3] + this->kg[3]) + 
                    this->q[1]*(pow(this->q[3],3) + this->q[3] * this->q[3]*this->kg[3] - 
                       this->kg[1] * this->kg[1]*this->kg[3]) + 
                    this->q[3]*this->kg[1]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->q[3]*(-this->q[3] + this->kg[3])))) 
))) + C4v*this->mRes*(C5vNC*(-8*this->mRes*this->mn*(p0 - this->q[0])*
            (this->q[3]*(this->q[1] * this->q[1] + this->q[3] * this->q[3])*this->kg[1] + this->q[1]*this->q[3]*this->kg[2] * this->kg[2] - 
              this->q[0] * this->q[0]*(this->q[1]*this->q[3] - this->q[1]*this->kg[3] + this->kg[1]*this->kg[3]) + 
              2*p0*this->q[0]*(this->q[1]*this->q[3] - this->kg[1]*(2*this->q[3] + this->kg[3]))) - 
           4*this->mRes_2*(-(this->q[0]*(this->q[1] - this->kg[1])*
                 (-(this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])) + 
                   this->q[0] * this->q[0]*(this->q[3] - this->kg[3]) + this->q[1]*this->kg[1]*this->kg[3])) + 
              2*p0*(this->q[3]*(this->q[1] * this->q[1] + this->q[3] * this->q[3])*this->kg[1] + 
                 this->q[1]*this->q[3]*this->kg[2] * this->kg[2] + this->q[0] * this->q[0]*this->q[1]*this->kg[3] - 
                 this->q[0] * this->q[0]*this->kg[1]*(this->q[3] + this->kg[3])) + 
              4*pow(p0,2)*this->q[0]*(this->q[1]*this->q[3] - this->kg[1]*(2*this->q[3] + this->kg[3]))) + 
           (2*p0*this->q[0]*this->q[1] + this->q[0] * this->q[0]*(-this->q[1] + this->kg[1]) + 
              this->q[3]*(-(this->q[3]*this->kg[1]) + this->q[1]*this->kg[3]))*
            (4*pow(p0,2)*this->q[0]*(this->q[3] - this->kg[3]) + 
              4*p0*(this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) - this->q[1]*this->kg[1]*this->kg[3] + 
                 2*this->q[0] * this->q[0]*(-this->q[3] + this->kg[3])) + 
              this->q[0]*(this->q[0] * this->q[0]*(5*this->q[3] - 3*this->kg[3]) + 4*this->q[1]*this->kg[1]*this->kg[3] - 
                 this->q[1] * this->q[1]*(this->q[3] + this->kg[3]) - 
                 this->q[3]*(4*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + this->q[3]*(this->q[3] + this->kg[3]))))) + 
        4*C4vNC*this->mRes*(2*this->mn*(p0 - this->q[0])*
            (2*p0*this->q[0]*(this->q[1]*this->q[3] + this->kg[1]*(2*this->q[3] + this->kg[3])) - 
              this->q[0] * this->q[0]*(this->q[1]*(this->q[3] - this->kg[3]) + this->kg[1]*(2*this->q[3] + this->kg[3])) + 
              this->q[3]*(this->q[1] * this->q[1]*this->kg[1] + this->q[1]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + 
                 this->q[3]*this->kg[1]*(this->q[3] + 2*this->kg[3]))) + 
           this->mRes*(this->q[0]*(pow(this->q[3],3)*this->kg[1] + 
                 this->q[3]*(2*this->q[1] + this->kg[1])*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) - 
                 this->q[0] * this->q[0]*(2*this->q[3]*this->kg[1] + this->q[1]*(this->q[3] - 2*this->kg[3])) + 
                 this->q[3] * this->q[3]*(-this->q[1] + this->kg[1])*this->kg[3] - this->q[1]*this->kg[1]*(this->q[1] + this->kg[1])*this->kg[3]) + 
              4*pow(p0,2)*this->q[0]*(this->q[1]*this->q[3] + this->kg[1]*(2*this->q[3] + this->kg[3])) + 
              2*p0*(-(this->q[0] * this->q[0]*(this->q[1]*this->q[3] + this->kg[1]*(this->q[3] + this->kg[3]))) + 
                 this->q[3]*(this->q[1] * this->q[1]*this->kg[1] + 
                    this->q[1]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + 
                    this->q[3]*this->kg[1]*(this->q[3] + 2*this->kg[3]))))) + 
        2*this->mn*(std::complex<double>(0,-1)*C5a*this->mn*this->kg[2]*
            (2*this->mRes_2*(2*p0*this->q[0] - this->q[0] * this->q[0] - this->q[3] * this->q[3] + 
                 this->q[1]*this->kg[1]) + this->q[1]*(this->q[1] + this->kg[1])*
               (2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) - 
              2*this->mRes*this->mn*(2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[3] * this->q[3] + this->q[1]*this->kg[1] + 
                 2*this->q[3]*this->kg[3])) + C3vNC*
            (4*this->mRes_2*this->mn*this->q[0]*this->kg[1]*(this->q[3] + this->kg[3]) + 
              2*this->mn*(p0 - this->q[0])*
               (2*p0*this->q[0]*(this->q[1]*this->q[3] + this->kg[1]*(2*this->q[3] + this->kg[3])) - 
                 this->q[0] * this->q[0]*(this->q[1]*(this->q[3] - this->kg[3]) + this->kg[1]*(2*this->q[3] + this->kg[3])) + 
                 this->q[3]*(this->q[1] * this->q[1]*this->kg[1] + 
                    this->q[1]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + 
                    this->q[3]*this->kg[1]*(this->q[3] + 2*this->kg[3]))) + 
              this->mRes*(4*pow(p0,2)*this->q[0]*(this->q[1]*this->q[3] + 5*this->q[3]*this->kg[1] + 2*this->kg[1]*this->kg[3]) + 
                 this->q[0]*(2*this->q[1]*this->q[3]*(2*this->kg[1] * this->kg[1] + 3*this->kg[2] * this->kg[2]) + 
                    this->q[1] * this->q[1]*this->kg[1]*(this->q[3] - 3*this->kg[3]) - 
                    2*this->q[1]*(this->q[3] * this->q[3] + this->kg[1] * this->kg[1])*this->kg[3] + 
                    this->q[3]*this->kg[1]*(3*this->q[3] - 2*this->kg[3])*(this->q[3] + this->kg[3]) - 
                    this->q[0] * this->q[0]*(this->q[1]*(4*this->q[3] - 6*this->kg[3]) + this->kg[1]*(this->q[3] + this->kg[3]))) + 
                 2*p0*(this->q[0] * this->q[0]*(this->q[3]*(this->q[1] - 4*this->kg[1]) - (this->q[1] + this->kg[1])*this->kg[3]) + 
                    this->q[3]*(this->q[1] * this->q[1]*this->kg[1] + 
                       this->q[1]*(4*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + 
                       this->q[3]*this->kg[1]*(this->q[3] + 4*this->kg[3]))))))))/
  (24.*this->mRes_2*pow(this->mn,5));
    
	tr[3][2] = (std::complex<double>(0,-2)*C5a*this->mn2*
     (C5v*this->mRes*(-4*p0*pow(this->q[0],3)*this->q[1] - 2*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[1] + 
          2*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[1] + 4*p0*this->q[0]*this->q[1]*this->kg[1] * this->kg[1] + 
          2*this->q[1] * this->q[1]*pow(this->kg[1],3) - 2*this->q[3] * this->q[3]*pow(this->kg[1],3) + 
          2*p0*this->q[0]*this->q[1]*this->kg[2] * this->kg[2] + this->q[0] * this->q[0]*this->q[1]*this->kg[2] * this->kg[2] + 
          this->q[1] * this->q[1]*this->kg[1]*this->kg[2] * this->kg[2] - 2*this->q[3] * this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] - 
          2*this->mRes*this->mn*(-(this->q[0] * this->q[0]*this->kg[1]) + 
             (this->q[1] * this->q[1] + this->q[3] * this->q[3])*this->kg[1] + 2*p0*this->q[0]*(this->q[1] + this->kg[1]) - 
             this->q[1]*this->kg[2] * this->kg[2]) + 
          this->q[3]*(4*p0*this->q[0]*this->kg[1] - 2*this->q[0] * this->q[0]*(this->q[1] + this->kg[1]) + 
             this->q[1]*(4*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]))*this->kg[3] + 
          2*this->q[0] * this->q[0]*this->q[1]*this->kg[3] * this->kg[3] - 
          2*this->mRes_2*(2*p0*this->q[0]*(this->q[1] - this->kg[1]) + this->q[0] * this->q[0]*this->kg[1] + 
             this->q[1] * this->q[1]*this->kg[1] + this->q[3] * this->q[3]*this->kg[1] - 2*this->q[1]*this->kg[1] * this->kg[1] - 
             this->q[1]*this->kg[2] * this->kg[2] - 2*this->q[3]*this->kg[1]*this->kg[3])) + 
       2*C3v*this->mn*(8*pow(this->mRes,3)*this->mn*this->kg[1] - 
          this->mRes_2*(6*p0*this->q[0]*(this->q[1] - this->kg[1]) + 8*pow(p0,2)*this->kg[1] + 
             (this->q[1] * this->q[1] + this->q[3] * this->q[3])*this->kg[1] + 
             this->q[0] * this->q[0]*(-2*this->q[1] + this->kg[1]) + 3*this->q[1]*this->kg[2] * this->kg[2]) - 
          this->q[1]*this->kg[2] * this->kg[2]*(2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) + 
          this->mRes*this->mn*(this->q[1] * this->q[1]*this->kg[1] + this->q[3] * this->q[3]*this->kg[1] + 2*this->q[1]*this->kg[1] * this->kg[1] + 
             2*p0*this->q[0]*(this->q[1] + this->kg[1]) - this->q[0] * this->q[0]*(2*this->q[1] + this->kg[1]) - 
             this->q[1]*this->kg[2] * this->kg[2] + 2*this->q[3]*this->kg[1]*this->kg[3]))) + 
    C4v*this->mRes*(std::complex<double>(0,2)*C5a*this->mn2*
        (2*this->mRes_2*(2*p0*this->q[0]*(this->q[1] - this->kg[1]) + 
             (this->q[1] * this->q[1] + this->q[3] * this->q[3])*this->kg[1] + 
             this->q[0] * this->q[0]*(-2*this->q[1] + this->kg[1]) + this->q[1]*this->kg[2] * this->kg[2]) + 
          this->q[1]*this->kg[2] * this->kg[2]*(2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1]*this->kg[1] + this->q[3]*this->kg[3]) + 
          2*this->mRes*this->mn*(this->q[1] * this->q[1]*this->kg[1] + this->q[3] * this->q[3]*this->kg[1] + 
             2*this->q[1]*this->kg[1] * this->kg[1] + 2*p0*this->q[0]*(this->q[1] + this->kg[1]) - 
             this->q[0] * this->q[0]*(2*this->q[1] + this->kg[1]) + this->q[1]*this->kg[2] * this->kg[2] + 2*this->q[3]*this->kg[1]*this->kg[3])) - 
       this->kg[2]*(2*C3vNC*this->mn*(4*this->mRes_2*this->mn*this->q[0]*(this->q[3] + this->kg[3]) + 
             2*this->mn*(p0 - this->q[0])*
              (this->q[1] * this->q[1]*this->q[3] + pow(this->q[3],3) + this->q[1]*this->q[3]*this->kg[1] + 
                this->q[1] * this->q[1]*this->kg[3] + 2*this->q[3] * this->q[3]*this->kg[3] + 
                2*p0*this->q[0]*(2*this->q[3] + this->kg[3]) - this->q[0] * this->q[0]*(2*this->q[3] + this->kg[3])) + 
             this->mRes*(4*pow(p0,2)*this->q[0]*(5*this->q[3] + 2*this->kg[3]) - 
                this->q[0]*(this->q[3] + this->kg[3])*
                 (this->q[0] * this->q[0] - 3*this->q[1] * this->q[1] + 2*this->q[1]*this->kg[1] + 
                   this->q[3]*(-3*this->q[3] + 2*this->kg[3])) + 
                2*p0*(3*this->q[1]*this->q[3]*this->kg[1] + this->q[1] * this->q[1]*(this->q[3] + this->kg[3]) - 
                   this->q[0] * this->q[0]*(4*this->q[3] + this->kg[3]) + this->q[3] * this->q[3]*(this->q[3] + 4*this->kg[3]))))  
+ 4*C4vNC*this->mRes*(2*this->mn*(p0 - this->q[0])*
              (this->q[1] * this->q[1]*this->q[3] + pow(this->q[3],3) + this->q[1]*this->q[3]*this->kg[1] + 
                this->q[1] * this->q[1]*this->kg[3] + 2*this->q[3] * this->q[3]*this->kg[3] + 
                2*p0*this->q[0]*(2*this->q[3] + this->kg[3]) - this->q[0] * this->q[0]*(2*this->q[3] + this->kg[3])) + 
             this->mRes*(4*pow(p0,2)*this->q[0]*(2*this->q[3] + this->kg[3]) + 
                2*p0*(this->q[1]*this->q[3]*this->kg[1] - this->q[0] * this->q[0]*(this->q[3] + this->kg[3]) + 
                   this->q[1] * this->q[1]*(this->q[3] + this->kg[3]) + this->q[3] * this->q[3]*(this->q[3] + 2*this->kg[3])) + 
                this->q[0]*(-2*this->q[0] * this->q[0]*this->q[3] - this->q[1]*this->kg[1]*this->kg[3] + 
                   this->q[1] * this->q[1]*(this->q[3] + this->kg[3]) + 
                   this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + this->q[3]*(this->q[3] + this->kg[3]))))) + 
          C5vNC*(8*this->mRes*this->mn*(p0 - this->q[0])*
              (-pow(this->q[3],3) + this->q[1]*this->q[3]*this->kg[1] + this->q[0] * this->q[0]*this->kg[3] - 
                this->q[1] * this->q[1]*(this->q[3] + this->kg[3]) + 2*p0*this->q[0]*(2*this->q[3] + this->kg[3])) + 
             4*this->mRes_2*(4*pow(p0,2)*this->q[0]*(2*this->q[3] + this->kg[3]) + 
                this->q[0]*(this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) - this->q[1]*this->kg[1]*this->kg[3] + 
                   this->q[0] * this->q[0]*(-this->q[3] + this->kg[3])) - 
                2*p0*(pow(this->q[3],3) - this->q[1]*this->q[3]*this->kg[1] - 
                   this->q[0] * this->q[0]*(this->q[3] + this->kg[3]) + this->q[1] * this->q[1]*(this->q[3] + this->kg[3]))) + 
             (this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3])*
              (4*pow(p0,2)*this->q[0]*(this->q[3] - this->kg[3]) + 
                4*p0*(this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) - this->q[1]*this->kg[1]*this->kg[3] + 
                   2*this->q[0] * this->q[0]*(-this->q[3] + this->kg[3])) + 
                this->q[0]*(this->q[0] * this->q[0]*(5*this->q[3] - 3*this->kg[3]) + 4*this->q[1]*this->kg[1]*this->kg[3] - 
                   this->q[1] * this->q[1]*(this->q[3] + this->kg[3]) - 
                   this->q[3]*(4*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + this->q[3]*(this->q[3] + this->kg[3])))))) 
) - this->kg[2]*(C5v*(C4vNC*this->mRes*(4*this->mRes_2*
              (2*p0*(-(this->q[0] * this->q[0]*this->q[3]) + pow(this->q[3],3) + this->q[1]*this->q[3]*this->kg[1] + 
                   this->q[1] * this->q[1]*(this->q[3] - this->kg[3])) + 
                pow(p0,2)*this->q[0]*(8*this->q[3] - 4*this->kg[3]) - 
                this->q[0]*(this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3])*(this->q[3] - this->kg[3]))  
+ 8*this->mRes*this->mn*(p0 - this->q[0])*(this->q[3]*(4*p0*this->q[0] - 2*this->q[0] * this->q[0] + this->q[3] * this->q[3] + 
                   this->q[1]*(this->q[1] + this->kg[1])) + 
                (-2*p0*this->q[0] + this->q[0] * this->q[0] - this->q[1] * this->q[1])*this->kg[3]) + 
             (4*pow(p0,2)*this->q[0] + 
                3*this->q[0]*(this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3]) + 
                4*p0*(-2*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3]))*
              (-(this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])) + 
                this->q[0] * this->q[0]*(this->q[3] - this->kg[3]) + 2*p0*this->q[0]*this->kg[3] + this->q[1]*this->kg[1]*this->kg[3])) + 
          2*C3vNC*this->mn*(4*pow(this->mRes,3)*this->mn*this->q[0]*(this->q[3] - this->kg[3]) + 
             (4*pow(p0,2)*this->q[0] + 
                3*this->q[0]*(this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3]) + 
                4*p0*(-2*this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3]))*
              (-(this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])) + 
                this->q[0] * this->q[0]*(this->q[3] - this->kg[3]) + 2*p0*this->q[0]*this->kg[3] + this->q[1]*this->kg[1]*this->kg[3]) + 
             2*this->mRes*this->mn*(pow(p0,2)*this->q[0]*(4*this->q[3] - 2*this->kg[3]) + 
                p0*(pow(this->q[3],3) + this->q[1]*this->q[3]*this->kg[1] + 
                   this->q[1] * this->q[1]*(this->q[3] - this->kg[3]) + this->q[0] * this->q[0]*(-6*this->q[3] + this->kg[3])) + 
                this->q[0]*(this->q[0] * this->q[0]*this->q[3] + 
                   this->q[3]*(-this->q[3] * this->q[3] + this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + 
                   this->q[1] * this->q[1]*(-this->q[3] + this->kg[3]) - this->q[1]*this->kg[1]*(this->q[3] + this->kg[3]))) + 
             this->mRes_2*(4*pow(p0,2)*this->q[0]*(5*this->q[3] - 2*this->kg[3]) + 
                2*p0*(3*this->q[1]*this->q[3]*this->kg[1] + this->q[1] * this->q[1]*(this->q[3] - this->kg[3]) - 
                   this->q[0] * this->q[0]*(4*this->q[3] + this->kg[3]) + this->q[3] * this->q[3]*(this->q[3] + 2*this->kg[3])) - 
                this->q[0]*(2*this->q[1]*this->q[3]*this->kg[1] + this->q[0] * this->q[0]*(this->q[3] - 3*this->kg[3]) + 
                   3*this->q[1] * this->q[1]*(-this->q[3] + this->kg[3]) + this->q[3] * this->q[3]*(-3*this->q[3] + 5*this->kg[3]) 
))) + C5vNC*(8*pow(this->mRes,3)*p0*
              (-(this->q[3]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] - this->q[1]*this->kg[1])) + 
                this->q[0] * this->q[0]*(this->q[3] - 2*this->kg[3]) + p0*this->q[0]*(4*this->q[3] - 2*this->kg[3]) + 
                (this->q[1] * this->q[1] + 2*this->q[3] * this->q[3])*this->kg[3]) + 
             8*this->mRes_2*this->mn*(p0 - this->q[0])*
              (-(this->q[3]*(-4*p0*this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3] - this->q[1]*this->kg[1])) + 
                (-(this->q[0]*(2*p0 + this->q[0])) + this->q[1] * this->q[1] + 2*this->q[3] * this->q[3])*this->kg[3]) - 
             8*this->mn*(p0 - this->q[0])*(this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3])*
              (-(this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])) + 
                this->q[0] * this->q[0]*(this->q[3] - this->kg[3]) + 2*p0*this->q[0]*this->kg[3] + this->q[1]*this->kg[1]*this->kg[3]) + 
             this->mRes*(8*pow(p0,3)*this->q[0] * this->q[0]*this->kg[3] + 
                4*pow(p0,2)*this->q[0]*
                 (-(this->q[3]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] + this->kg[1] * this->kg[1] + 
                        this->kg[2] * this->kg[2])) + 2*this->q[0] * this->q[0]*(this->q[3] - 4*this->kg[3]) + 
                   5*(this->q[1] * this->q[1] + this->q[3] * this->q[3])*this->kg[3] + this->q[1]*this->kg[1]*this->kg[3]) + 
                this->q[0]*(-this->q[0] * this->q[0] + this->q[1] * this->q[1] + this->q[3] * this->q[3])*
                 (this->q[1] * this->q[1]*(this->q[3] - this->kg[3]) + this->q[1]*this->kg[1]*this->kg[3] + 
                   this->q[3]*(this->q[3] * this->q[3] - this->kg[1] * this->kg[1] - this->kg[2] * this->kg[2] - this->q[3]*this->kg[3]) 
) - 2*p0*(pow(this->q[0],4)*(8*this->q[3] - 7*this->kg[3]) + 
                   4*(this->q[1] * this->q[1] + this->q[3] * this->q[3])*
                    (this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) - this->q[1]*this->kg[1]*this->kg[3]) + 
                   this->q[0] * this->q[0]*
                    (-6*this->q[3]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] + this->kg[1] * this->kg[1] + 
                         this->kg[2] * this->kg[2]) + 
                      5*(this->q[1] * this->q[1] + this->q[3] * this->q[3])*this->kg[3] + 6*this->q[1]*this->kg[1]*this->kg[3])))))  
+ 2*C3v*this->mn*(2*C3vNC*this->mn*(4*this->mRes*(p0 - this->q[0])*this->q[3]*
              (2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1]*this->kg[1]) - 
             this->mRes*(4*pow(p0,2)*this->q[0] + 
                4*p0*(-2*this->q[0] * this->q[0] + this->q[1] * this->q[1]) + 
                this->q[0]*(3*this->q[0] * this->q[0] - 3*this->q[1] * this->q[1] + this->q[3] * this->q[3]))*this->kg[3] + 
             pow(this->mRes,3)*(-4*(2*p0 + this->q[0])*this->q[3] + 2*this->q[0]*this->kg[3]) + 
             2*this->mn*(p0 - this->q[0])*
              (this->q[3]*(4*p0*this->q[0] - 2*this->q[0] * this->q[0] + this->q[3] * this->q[3] + 
                   this->q[1]*(this->q[1] + this->kg[1])) + 
                (2*p0*this->q[0] - this->q[0] * this->q[0] + this->q[1] * this->q[1] + 2*this->q[3] * this->q[3])*
                 this->kg[3]) + 2*this->mRes_2*this->mn*(4*p0*this->q[3] + this->q[0]*(-2*this->q[3] + this->kg[3])))  
+ C5vNC*(4*pow(this->mRes,3)*this->mn*this->q[0]*this->q[3] + 
             this->mRes_2*(-6*p0*this->q[3]*
                 (this->q[1] * this->q[1] + this->q[3] * this->q[3] - this->q[1]*this->kg[1]) + 
                this->q[0]*this->q[3]*(3*this->q[1] * this->q[1] + 3*this->q[3] * this->q[3] - 2*this->q[1]*this->kg[1] + 
                   2*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])) + 
                2*p0*(3*this->q[0] * this->q[0] - this->q[1] * this->q[1] + 2*this->q[3] * this->q[3])*
                 this->kg[3] - 2*this->q[0]*(2*this->q[3] * this->q[3] + this->q[1]*(this->q[1] + this->kg[1]))*this->kg[3] + 
                4*pow(p0,2)*this->q[0]*(5*this->q[3] + this->kg[3]) + 
                pow(this->q[0],3)*(-3*this->q[3] + 4*this->kg[3])) + 
             2*this->mRes*this->mn*(2*pow(p0,2)*this->q[0]*(2*this->q[3] + this->kg[3]) - 
                p0*(pow(this->q[3],3) - this->q[1]*this->q[3]*this->kg[1] + this->q[1] * this->q[1]*(this->q[3] + this->kg[3]) + 
                   this->q[0] * this->q[0]*(4*this->q[3] + this->kg[3])) - 
                this->q[0]*(this->q[1]*this->q[3]*this->kg[1] - 2*this->q[1] * this->q[1]*(this->q[3] + this->kg[3]) - 
                   this->q[3] * this->q[3]*(2*this->q[3] + this->kg[3]) + this->q[0] * this->q[0]*(this->q[3] + 2*this->kg[3]))) + 
             (this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3])*
              (4*pow(p0,2)*this->q[0]*(this->q[3] - this->kg[3]) + 
                4*p0*(this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) - this->q[1]*this->kg[1]*this->kg[3] + 
                   2*this->q[0] * this->q[0]*(-this->q[3] + this->kg[3])) + 
                this->q[0]*(this->q[0] * this->q[0]*(5*this->q[3] - 3*this->kg[3]) + 4*this->q[1]*this->kg[1]*this->kg[3] - 
                   this->q[1] * this->q[1]*(this->q[3] + this->kg[3]) - 
                   this->q[3]*(4*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + this->q[3]*(this->q[3] + this->kg[3])))))  
+ C4vNC*this->mRes*(4*this->mRes_2*this->mn*this->q[0]*this->q[3] + 
             2*this->mn*(p0 - this->q[0])*(this->q[1] * this->q[1]*this->q[3] + pow(this->q[3],3) + this->q[1]*this->q[3]*this->kg[1] + 
                this->q[1] * this->q[1]*this->kg[3] + 2*this->q[3] * this->q[3]*this->kg[3] + 
                2*p0*this->q[0]*(2*this->q[3] + this->kg[3]) - this->q[0] * this->q[0]*(2*this->q[3] + this->kg[3])) + 
             this->mRes*(6*p0*this->q[3]*(-2*this->q[0] * this->q[0] + this->q[3] * this->q[3] + 
                   this->q[1]*(this->q[1] + this->kg[1])) + 
                2*p0*(this->q[0] * this->q[0] + this->q[1] * this->q[1] + 4*this->q[3] * this->q[3])*this->kg[3] + 
                4*pow(p0,2)*this->q[0]*(5*this->q[3] + this->kg[3]) + 
                this->q[0]*(-2*this->q[1]*this->kg[1]*(this->q[3] + this->kg[3]) - this->q[0] * this->q[0]*(this->q[3] + 2*this->kg[3]) + 
                   this->q[1] * this->q[1]*(this->q[3] + 4*this->kg[3]) + 
                   this->q[3]*(2*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + this->q[3]*(this->q[3] + 2*this->kg[3]))))) 
)))/(24.*this->mRes_2*pow(this->mn,5));
    
	tr[3][3] = (32*C4v*C4vNC*pow(this->mRes,3)*pow(p0,3)*this->q[0] * this->q[0] + 
    32*C4vNC*C5v*pow(this->mRes,3)*pow(p0,3)*this->q[0] * this->q[0] + 
    32*C4v*C5vNC*pow(this->mRes,3)*pow(p0,3)*this->q[0] * this->q[0] + 
    32*C5v*C5vNC*pow(this->mRes,3)*pow(p0,3)*this->q[0] * this->q[0] + 
    32*C4v*C4vNC*this->mRes_2*this->mn*pow(p0,3)*this->q[0] * this->q[0] + 
    32*C4vNC*C5v*this->mRes_2*this->mn*pow(p0,3)*this->q[0] * this->q[0] + 
    32*C4v*C5vNC*this->mRes_2*this->mn*pow(p0,3)*this->q[0] * this->q[0] + 
    32*C5v*C5vNC*this->mRes_2*this->mn*pow(p0,3)*this->q[0] * this->q[0] - 
    64*C4v*C4vNC*pow(this->mRes,3)*pow(p0,2)*pow(this->q[0],3) - 
    32*C4vNC*C5v*pow(this->mRes,3)*pow(p0,2)*pow(this->q[0],3) - 
    32*C4v*C5vNC*pow(this->mRes,3)*pow(p0,2)*pow(this->q[0],3) - 
    96*C4v*C4vNC*this->mRes_2*this->mn*pow(p0,2)*pow(this->q[0],3) - 
    64*C4vNC*C5v*this->mRes_2*this->mn*pow(p0,2)*pow(this->q[0],3) - 
    64*C4v*C5vNC*this->mRes_2*this->mn*pow(p0,2)*pow(this->q[0],3) - 
    32*C5v*C5vNC*this->mRes_2*this->mn*pow(p0,2)*pow(this->q[0],3) + 
    24*C4v*C4vNC*pow(this->mRes,3)*p0*pow(this->q[0],4) + 
    16*C4vNC*C5v*pow(this->mRes,3)*p0*pow(this->q[0],4) - 
    8*C4v*C5vNC*pow(this->mRes,3)*p0*pow(this->q[0],4) + 
    16*C5v*C5vNC*pow(this->mRes,3)*p0*pow(this->q[0],4) + 
    88*C4v*C4vNC*this->mRes_2*this->mn*p0*pow(this->q[0],4) + 
    40*C4vNC*C5v*this->mRes_2*this->mn*p0*pow(this->q[0],4) + 
    24*C4v*C5vNC*this->mRes_2*this->mn*p0*pow(this->q[0],4) + 
    8*C5v*C5vNC*this->mRes_2*this->mn*p0*pow(this->q[0],4) - 
    8*C4vNC*C5v*this->mRes*pow(p0,3)*pow(this->q[0],4) - 
    8*C5v*C5vNC*this->mRes*pow(p0,3)*pow(this->q[0],4) - 
    4*C4vNC*C5v*pow(this->mRes,3)*pow(this->q[0],5) - 
    4*C4v*C5vNC*pow(this->mRes,3)*pow(this->q[0],5) - 
    24*C4v*C4vNC*this->mRes_2*this->mn*pow(this->q[0],5) - 
    8*C4vNC*C5v*this->mRes_2*this->mn*pow(this->q[0],5) + 
    8*C4v*C5vNC*this->mRes_2*this->mn*pow(this->q[0],5) - 
    8*C5v*C5vNC*this->mRes_2*this->mn*pow(this->q[0],5) + 
    20*C4vNC*C5v*this->mRes*pow(p0,2)*pow(this->q[0],5) + 
    4*C4v*C5vNC*this->mRes*pow(p0,2)*pow(this->q[0],5) + 
    32*C5v*C5vNC*this->mRes*pow(p0,2)*pow(this->q[0],5) + 
    16*C5v*C5vNC*this->mn*pow(p0,2)*pow(this->q[0],5) - 
    14*C4vNC*C5v*this->mRes*p0*pow(this->q[0],6) - 
    8*C4v*C5vNC*this->mRes*p0*pow(this->q[0],6) - 
    14*C5v*C5vNC*this->mRes*p0*pow(this->q[0],6) - 24*C5v*C5vNC*this->mn*p0*pow(this->q[0],6) + 
    3*C4vNC*C5v*this->mRes*pow(this->q[0],7) + 3*C4v*C5vNC*this->mRes*pow(this->q[0],7) + 
    8*C5v*C5vNC*this->mn*pow(this->q[0],7) + 
    16*C4v*C4vNC*pow(this->mRes,3)*pow(p0,2)*this->q[0]*this->q[1] * this->q[1] + 
    16*C4vNC*C5v*pow(this->mRes,3)*pow(p0,2)*this->q[0]*this->q[1] * this->q[1] - 
    16*C4v*C5vNC*pow(this->mRes,3)*pow(p0,2)*this->q[0]*this->q[1] * this->q[1] - 
    16*C5v*C5vNC*pow(this->mRes,3)*pow(p0,2)*this->q[0]*this->q[1] * this->q[1] + 
    16*C4v*C4vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[0]*this->q[1] * this->q[1] + 
    16*C4vNC*C5v*this->mRes_2*this->mn*pow(p0,2)*this->q[0]*this->q[1] * this->q[1] - 
    16*C4v*C5vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[0]*this->q[1] * this->q[1] - 
    16*C5v*C5vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[0]*this->q[1] * this->q[1] - 
    16*C4v*C4vNC*pow(this->mRes,3)*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1] + 
    16*C4v*C5vNC*pow(this->mRes,3)*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1] - 
    32*C4v*C4vNC*this->mRes_2*this->mn*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1] - 
    16*C4vNC*C5v*this->mRes_2*this->mn*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1] + 
    32*C4v*C5vNC*this->mRes_2*this->mn*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1] + 
    16*C5v*C5vNC*this->mRes_2*this->mn*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1] - 
    4*C4v*C4vNC*pow(this->mRes,3)*pow(this->q[0],3)*this->q[1] * this->q[1] + 
    4*C4vNC*C5v*pow(this->mRes,3)*pow(this->q[0],3)*this->q[1] * this->q[1] + 
    16*C4v*C4vNC*this->mRes_2*this->mn*pow(this->q[0],3)*this->q[1] * this->q[1] - 
    16*C4v*C5vNC*this->mRes_2*this->mn*pow(this->q[0],3)*this->q[1] * this->q[1] - 
    8*C4vNC*C5v*this->mRes*pow(p0,2)*pow(this->q[0],3)*this->q[1] * this->q[1] - 
    4*C4v*C5vNC*this->mRes*pow(p0,2)*pow(this->q[0],3)*this->q[1] * this->q[1] - 
    20*C5v*C5vNC*this->mRes*pow(p0,2)*pow(this->q[0],3)*this->q[1] * this->q[1] - 
    16*C5v*C5vNC*this->mn*pow(p0,2)*pow(this->q[0],3)*this->q[1] * this->q[1] + 
    10*C4vNC*C5v*this->mRes*p0*pow(this->q[0],4)*this->q[1] * this->q[1] + 
    8*C4v*C5vNC*this->mRes*p0*pow(this->q[0],4)*this->q[1] * this->q[1] + 
    10*C5v*C5vNC*this->mRes*p0*pow(this->q[0],4)*this->q[1] * this->q[1] + 
    24*C5v*C5vNC*this->mn*p0*pow(this->q[0],4)*this->q[1] * this->q[1] - 
    3*C4vNC*C5v*this->mRes*pow(this->q[0],5)*this->q[1] * this->q[1] - 
    2*C4v*C5vNC*this->mRes*pow(this->q[0],5)*this->q[1] * this->q[1] - 
    C5v*C5vNC*this->mRes*pow(this->q[0],5)*this->q[1] * this->q[1] - 
    8*C5v*C5vNC*this->mn*pow(this->q[0],5)*this->q[1] * this->q[1] - 
    C4v*C5vNC*this->mRes*pow(this->q[0],3)*pow(this->q[1],4) + 
    C5v*C5vNC*this->mRes*pow(this->q[0],3)*pow(this->q[1],4) - 
    8*C4v*C4vNC*pow(this->mRes,3)*p0*this->q[0] * this->q[0]*this->q[3] * this->q[3] - 
    8*C4vNC*C5v*pow(this->mRes,3)*p0*this->q[0] * this->q[0]*this->q[3] * this->q[3] + 
    16*C4v*C5vNC*pow(this->mRes,3)*p0*this->q[0] * this->q[0]*this->q[3] * this->q[3] + 
    16*C5v*C5vNC*pow(this->mRes,3)*p0*this->q[0] * this->q[0]*this->q[3] * this->q[3] - 
    8*C4v*C4vNC*this->mRes_2*this->mn*p0*this->q[0] * this->q[0]*this->q[3] * this->q[3] - 
    8*C4vNC*C5v*this->mRes_2*this->mn*p0*this->q[0] * this->q[0]*this->q[3] * this->q[3] + 
    8*C4v*C5vNC*this->mRes_2*this->mn*p0*this->q[0] * this->q[0]*this->q[3] * this->q[3] + 
    8*C5v*C5vNC*this->mRes_2*this->mn*p0*this->q[0] * this->q[0]*this->q[3] * this->q[3] - 
    8*C4v*C5vNC*this->mRes*pow(p0,3)*this->q[0] * this->q[0]*this->q[3] * this->q[3] - 
    8*C5v*C5vNC*this->mRes*pow(p0,3)*this->q[0] * this->q[0]*this->q[3] * this->q[3] - 
    4*C4vNC*C5v*pow(this->mRes,3)*pow(this->q[0],3)*this->q[3] * this->q[3] - 
    4*C4v*C5vNC*pow(this->mRes,3)*pow(this->q[0],3)*this->q[3] * this->q[3] + 
    8*C4v*C4vNC*this->mRes_2*this->mn*pow(this->q[0],3)*this->q[3] * this->q[3] + 
    8*C4vNC*C5v*this->mRes_2*this->mn*pow(this->q[0],3)*this->q[3] * this->q[3] - 
    8*C4v*C5vNC*this->mRes_2*this->mn*pow(this->q[0],3)*this->q[3] * this->q[3] - 
    8*C5v*C5vNC*this->mRes_2*this->mn*pow(this->q[0],3)*this->q[3] * this->q[3] + 
    4*C4vNC*C5v*this->mRes*pow(p0,2)*pow(this->q[0],3)*this->q[3] * this->q[3] + 
    20*C4v*C5vNC*this->mRes*pow(p0,2)*pow(this->q[0],3)*this->q[3] * this->q[3] + 
    32*C5v*C5vNC*this->mRes*pow(p0,2)*pow(this->q[0],3)*this->q[3] * this->q[3] + 
    16*C5v*C5vNC*this->mn*pow(p0,2)*pow(this->q[0],3)*this->q[3] * this->q[3] - 
    10*C4vNC*C5v*this->mRes*p0*pow(this->q[0],4)*this->q[3] * this->q[3] - 
    18*C4v*C5vNC*this->mRes*p0*pow(this->q[0],4)*this->q[3] * this->q[3] - 
    20*C5v*C5vNC*this->mRes*p0*pow(this->q[0],4)*this->q[3] * this->q[3] - 
    24*C5v*C5vNC*this->mn*p0*pow(this->q[0],4)*this->q[3] * this->q[3] + 
    6*C4vNC*C5v*this->mRes*pow(this->q[0],5)*this->q[3] * this->q[3] + 
    6*C4v*C5vNC*this->mRes*pow(this->q[0],5)*this->q[3] * this->q[3] + 
    8*C5v*C5vNC*this->mn*pow(this->q[0],5)*this->q[3] * this->q[3] + 
    2*C4v*C5vNC*this->mRes*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3] * this->q[3] + 
    2*C5v*C5vNC*this->mRes*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3] * this->q[3] - 
    C4vNC*C5v*this->mRes*pow(this->q[0],3)*this->q[1] * this->q[1]*this->q[3] * this->q[3] - 
    2*C4v*C5vNC*this->mRes*pow(this->q[0],3)*this->q[1] * this->q[1]*this->q[3] * this->q[3] + 
    C5v*C5vNC*this->mRes*pow(this->q[0],3)*this->q[1] * this->q[1]*this->q[3] * this->q[3] + 
    2*C4v*C5vNC*this->mRes*p0*this->q[0] * this->q[0]*pow(this->q[3],4) + 
    2*C5v*C5vNC*this->mRes*p0*this->q[0] * this->q[0]*pow(this->q[3],4) - 
    C4vNC*C5v*this->mRes*pow(this->q[0],3)*pow(this->q[3],4) - 
    C4v*C5vNC*this->mRes*pow(this->q[0],3)*pow(this->q[3],4) + 
    32*C4v*C4vNC*pow(this->mRes,3)*pow(p0,2)*this->q[0]*this->q[1]*this->kg[1] + 
    32*C4vNC*C5v*pow(this->mRes,3)*pow(p0,2)*this->q[0]*this->q[1]*this->kg[1] + 
    32*C4v*C5vNC*pow(this->mRes,3)*pow(p0,2)*this->q[0]*this->q[1]*this->kg[1] + 
    32*C5v*C5vNC*pow(this->mRes,3)*pow(p0,2)*this->q[0]*this->q[1]*this->kg[1] + 
    32*C4v*C4vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[0]*this->q[1]*this->kg[1] + 
    32*C4vNC*C5v*this->mRes_2*this->mn*pow(p0,2)*this->q[0]*this->q[1]*this->kg[1] + 
    32*C4v*C5vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[0]*this->q[1]*this->kg[1] + 
    32*C5v*C5vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[0]*this->q[1]*this->kg[1] - 
    24*C4v*C4vNC*pow(this->mRes,3)*p0*this->q[0] * this->q[0]*this->q[1]*this->kg[1] - 
    24*C4vNC*C5v*pow(this->mRes,3)*p0*this->q[0] * this->q[0]*this->q[1]*this->kg[1] - 
    8*C4v*C5vNC*pow(this->mRes,3)*p0*this->q[0] * this->q[0]*this->q[1]*this->kg[1] - 
    8*C5v*C5vNC*pow(this->mRes,3)*p0*this->q[0] * this->q[0]*this->q[1]*this->kg[1] - 
    56*C4v*C4vNC*this->mRes_2*this->mn*p0*this->q[0] * this->q[0]*this->q[1]*this->kg[1] - 
    56*C4vNC*C5v*this->mRes_2*this->mn*p0*this->q[0] * this->q[0]*this->q[1]*this->kg[1] - 
    40*C4v*C5vNC*this->mRes_2*this->mn*p0*this->q[0] * this->q[0]*this->q[1]*this->kg[1] - 
    40*C5v*C5vNC*this->mRes_2*this->mn*p0*this->q[0] * this->q[0]*this->q[1]*this->kg[1] + 
    4*C4v*C4vNC*pow(this->mRes,3)*pow(this->q[0],3)*this->q[1]*this->kg[1] + 
    4*C4v*C5vNC*pow(this->mRes,3)*pow(this->q[0],3)*this->q[1]*this->kg[1] + 
    24*C4v*C4vNC*this->mRes_2*this->mn*pow(this->q[0],3)*this->q[1]*this->kg[1] + 
    24*C4vNC*C5v*this->mRes_2*this->mn*pow(this->q[0],3)*this->q[1]*this->kg[1] + 
    8*C4v*C5vNC*this->mRes_2*this->mn*pow(this->q[0],3)*this->q[1]*this->kg[1] + 
    8*C5v*C5vNC*this->mRes_2*this->mn*pow(this->q[0],3)*this->q[1]*this->kg[1] - 
    4*C4vNC*C5v*this->mRes*pow(p0,2)*pow(this->q[0],3)*this->q[1]*this->kg[1] - 
    4*C5v*C5vNC*this->mRes*pow(p0,2)*pow(this->q[0],3)*this->q[1]*this->kg[1] + 
    8*C4vNC*C5v*this->mRes*p0*pow(this->q[0],4)*this->q[1]*this->kg[1] + 
    4*C4v*C5vNC*this->mRes*p0*pow(this->q[0],4)*this->q[1]*this->kg[1] + 
    12*C5v*C5vNC*this->mRes*p0*pow(this->q[0],4)*this->q[1]*this->kg[1] + 
    8*C5v*C5vNC*this->mn*p0*pow(this->q[0],4)*this->q[1]*this->kg[1] - 
    3*C4vNC*C5v*this->mRes*pow(this->q[0],5)*this->q[1]*this->kg[1] - 
    4*C4v*C5vNC*this->mRes*pow(this->q[0],5)*this->q[1]*this->kg[1] + 
    C5v*C5vNC*this->mRes*pow(this->q[0],5)*this->q[1]*this->kg[1] - 8*C5v*C5vNC*this->mn*pow(this->q[0],5)*this->q[1]*this->kg[1] + 
    8*C4v*C4vNC*pow(this->mRes,3)*p0*pow(this->q[1],3)*this->kg[1] + 
    8*C4vNC*C5v*pow(this->mRes,3)*p0*pow(this->q[1],3)*this->kg[1] - 
    8*C4v*C5vNC*pow(this->mRes,3)*p0*pow(this->q[1],3)*this->kg[1] - 
    8*C5v*C5vNC*pow(this->mRes,3)*p0*pow(this->q[1],3)*this->kg[1] + 
    8*C4v*C4vNC*this->mRes_2*this->mn*p0*pow(this->q[1],3)*this->kg[1] + 
    8*C4vNC*C5v*this->mRes_2*this->mn*p0*pow(this->q[1],3)*this->kg[1] - 
    8*C4v*C5vNC*this->mRes_2*this->mn*p0*pow(this->q[1],3)*this->kg[1] - 
    8*C5v*C5vNC*this->mRes_2*this->mn*p0*pow(this->q[1],3)*this->kg[1] - 
    8*C4v*C4vNC*this->mRes_2*this->mn*this->q[0]*pow(this->q[1],3)*this->kg[1] - 
    8*C4vNC*C5v*this->mRes_2*this->mn*this->q[0]*pow(this->q[1],3)*this->kg[1] + 
    8*C4v*C5vNC*this->mRes_2*this->mn*this->q[0]*pow(this->q[1],3)*this->kg[1] + 
    8*C5v*C5vNC*this->mRes_2*this->mn*this->q[0]*pow(this->q[1],3)*this->kg[1] - 
    4*C4vNC*C5v*this->mRes*p0*this->q[0] * this->q[0]*pow(this->q[1],3)*this->kg[1] - 
    4*C4v*C5vNC*this->mRes*p0*this->q[0] * this->q[0]*pow(this->q[1],3)*this->kg[1] - 
    8*C5v*C5vNC*this->mRes*p0*this->q[0] * this->q[0]*pow(this->q[1],3)*this->kg[1] - 
    8*C5v*C5vNC*this->mn*p0*this->q[0] * this->q[0]*pow(this->q[1],3)*this->kg[1] + 
    3*C4vNC*C5v*this->mRes*pow(this->q[0],3)*pow(this->q[1],3)*this->kg[1] + 
    4*C4v*C5vNC*this->mRes*pow(this->q[0],3)*pow(this->q[1],3)*this->kg[1] - 
    C5v*C5vNC*this->mRes*pow(this->q[0],3)*pow(this->q[1],3)*this->kg[1] + 
    8*C5v*C5vNC*this->mn*pow(this->q[0],3)*pow(this->q[1],3)*this->kg[1] + 
    8*C4v*C4vNC*pow(this->mRes,3)*p0*this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 
    8*C4vNC*C5v*pow(this->mRes,3)*p0*this->q[1]*this->q[3] * this->q[3]*this->kg[1] - 
    8*C4v*C5vNC*pow(this->mRes,3)*p0*this->q[1]*this->q[3] * this->q[3]*this->kg[1] - 
    8*C5v*C5vNC*pow(this->mRes,3)*p0*this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 
    8*C4v*C4vNC*this->mRes_2*this->mn*p0*this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 
    8*C4vNC*C5v*this->mRes_2*this->mn*p0*this->q[1]*this->q[3] * this->q[3]*this->kg[1] - 
    8*C4v*C5vNC*this->mRes_2*this->mn*p0*this->q[1]*this->q[3] * this->q[3]*this->kg[1] - 
    8*C5v*C5vNC*this->mRes_2*this->mn*p0*this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 
    4*C4v*C4vNC*pow(this->mRes,3)*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 
    4*C4vNC*C5v*pow(this->mRes,3)*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1] - 
    8*C4v*C4vNC*this->mRes_2*this->mn*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1] - 
    8*C4vNC*C5v*this->mRes_2*this->mn*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 
    8*C4v*C5vNC*this->mRes_2*this->mn*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 
    8*C5v*C5vNC*this->mRes_2*this->mn*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1] - 
    4*C4v*C5vNC*this->mRes*pow(p0,2)*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1] - 
    4*C5v*C5vNC*this->mRes*pow(p0,2)*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 
    4*C4vNC*C5v*this->mRes*p0*this->q[0] * this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 
    8*C4v*C5vNC*this->mRes*p0*this->q[0] * this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 
    12*C5v*C5vNC*this->mRes*p0*this->q[0] * this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 
    8*C5v*C5vNC*this->mn*p0*this->q[0] * this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1] - 
    5*C4vNC*C5v*this->mRes*pow(this->q[0],3)*this->q[1]*this->q[3] * this->q[3]*this->kg[1] - 
    5*C4v*C5vNC*this->mRes*pow(this->q[0],3)*this->q[1]*this->q[3] * this->q[3]*this->kg[1] - 
    2*C5v*C5vNC*this->mRes*pow(this->q[0],3)*this->q[1]*this->q[3] * this->q[3]*this->kg[1] - 
    8*C5v*C5vNC*this->mn*pow(this->q[0],3)*this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 
    C4v*C5vNC*this->mRes*this->q[0]*pow(this->q[1],3)*this->q[3] * this->q[3]*this->kg[1] + 
    C5v*C5vNC*this->mRes*this->q[0]*pow(this->q[1],3)*this->q[3] * this->q[3]*this->kg[1] + 
    C4v*C5vNC*this->mRes*this->q[0]*this->q[1]*pow(this->q[3],4)*this->kg[1] + 
    C5v*C5vNC*this->mRes*this->q[0]*this->q[1]*pow(this->q[3],4)*this->kg[1] + 
    16*C4v*C4vNC*pow(this->mRes,3)*pow(p0,2)*this->q[0]*this->kg[1] * this->kg[1] - 
    16*C4vNC*C5v*pow(this->mRes,3)*pow(p0,2)*this->q[0]*this->kg[1] * this->kg[1] + 
    16*C4v*C5vNC*pow(this->mRes,3)*pow(p0,2)*this->q[0]*this->kg[1] * this->kg[1] - 
    16*C5v*C5vNC*pow(this->mRes,3)*pow(p0,2)*this->q[0]*this->kg[1] * this->kg[1] + 
    16*C4v*C4vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[0]*this->kg[1] * this->kg[1] - 
    16*C4vNC*C5v*this->mRes_2*this->mn*pow(p0,2)*this->q[0]*this->kg[1] * this->kg[1] + 
    16*C4v*C5vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[0]*this->kg[1] * this->kg[1] - 
    16*C5v*C5vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[0]*this->kg[1] * this->kg[1] - 
    8*C4v*C4vNC*pow(this->mRes,3)*p0*this->q[0] * this->q[0]*this->kg[1] * this->kg[1] + 
    8*C4v*C5vNC*pow(this->mRes,3)*p0*this->q[0] * this->q[0]*this->kg[1] * this->kg[1] - 
    16*C5v*C5vNC*pow(this->mRes,3)*p0*this->q[0] * this->q[0]*this->kg[1] * this->kg[1] - 
    24*C4v*C4vNC*this->mRes_2*this->mn*p0*this->q[0] * this->q[0]*this->kg[1] * this->kg[1] + 
    24*C4vNC*C5v*this->mRes_2*this->mn*p0*this->q[0] * this->q[0]*this->kg[1] * this->kg[1] - 
    8*C4v*C5vNC*this->mRes_2*this->mn*p0*this->q[0] * this->q[0]*this->kg[1] * this->kg[1] + 
    8*C5v*C5vNC*this->mRes_2*this->mn*p0*this->q[0] * this->q[0]*this->kg[1] * this->kg[1] + 
    8*C4vNC*C5v*this->mRes*pow(p0,3)*this->q[0] * this->q[0]*this->kg[1] * this->kg[1] + 
    8*C5v*C5vNC*this->mRes*pow(p0,3)*this->q[0] * this->q[0]*this->kg[1] * this->kg[1] + 
    4*C4vNC*C5v*pow(this->mRes,3)*pow(this->q[0],3)*this->kg[1] * this->kg[1] + 
    4*C4v*C5vNC*pow(this->mRes,3)*pow(this->q[0],3)*this->kg[1] * this->kg[1] + 
    8*C4v*C4vNC*this->mRes_2*this->mn*pow(this->q[0],3)*this->kg[1] * this->kg[1] - 
    8*C4vNC*C5v*this->mRes_2*this->mn*pow(this->q[0],3)*this->kg[1] * this->kg[1] - 
    8*C4v*C5vNC*this->mRes_2*this->mn*pow(this->q[0],3)*this->kg[1] * this->kg[1] + 
    8*C5v*C5vNC*this->mRes_2*this->mn*pow(this->q[0],3)*this->kg[1] * this->kg[1] - 
    20*C4vNC*C5v*this->mRes*pow(p0,2)*pow(this->q[0],3)*this->kg[1] * this->kg[1] - 
    4*C4v*C5vNC*this->mRes*pow(p0,2)*pow(this->q[0],3)*this->kg[1] * this->kg[1] - 
    32*C5v*C5vNC*this->mRes*pow(p0,2)*pow(this->q[0],3)*this->kg[1] * this->kg[1] - 
    16*C5v*C5vNC*this->mn*pow(p0,2)*pow(this->q[0],3)*this->kg[1] * this->kg[1] + 
    14*C4vNC*C5v*this->mRes*p0*pow(this->q[0],4)*this->kg[1] * this->kg[1] + 
    8*C4v*C5vNC*this->mRes*p0*pow(this->q[0],4)*this->kg[1] * this->kg[1] + 
    14*C5v*C5vNC*this->mRes*p0*pow(this->q[0],4)*this->kg[1] * this->kg[1] + 
    24*C5v*C5vNC*this->mn*p0*pow(this->q[0],4)*this->kg[1] * this->kg[1] - 
    3*C4vNC*C5v*this->mRes*pow(this->q[0],5)*this->kg[1] * this->kg[1] - 
    3*C4v*C5vNC*this->mRes*pow(this->q[0],5)*this->kg[1] * this->kg[1] - 
    8*C5v*C5vNC*this->mn*pow(this->q[0],5)*this->kg[1] * this->kg[1] + 
    16*C4v*C4vNC*pow(this->mRes,3)*p0*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] + 
    16*C5v*C5vNC*pow(this->mRes,3)*p0*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] + 
    16*C4v*C4vNC*this->mRes_2*this->mn*p0*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] + 
    16*C5v*C5vNC*this->mRes_2*this->mn*p0*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] + 
    4*C4v*C4vNC*pow(this->mRes,3)*this->q[0]*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] - 
    4*C4vNC*C5v*pow(this->mRes,3)*this->q[0]*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] - 
    16*C4v*C4vNC*this->mRes_2*this->mn*this->q[0]*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] - 
    16*C5v*C5vNC*this->mRes_2*this->mn*this->q[0]*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] + 
    8*C4vNC*C5v*this->mRes*pow(p0,2)*this->q[0]*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] + 
    4*C4v*C5vNC*this->mRes*pow(p0,2)*this->q[0]*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] + 
    20*C5v*C5vNC*this->mRes*pow(p0,2)*this->q[0]*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] + 
    16*C5v*C5vNC*this->mn*pow(p0,2)*this->q[0]*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] - 
    10*C4vNC*C5v*this->mRes*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] - 
    8*C4v*C5vNC*this->mRes*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] - 
    10*C5v*C5vNC*this->mRes*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] - 
    24*C5v*C5vNC*this->mn*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] + 
    3*C4vNC*C5v*this->mRes*pow(this->q[0],3)*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] + 
    2*C4v*C5vNC*this->mRes*pow(this->q[0],3)*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] + 
    C5v*C5vNC*this->mRes*pow(this->q[0],3)*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] + 
    8*C5v*C5vNC*this->mn*pow(this->q[0],3)*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] + 
    C4v*C5vNC*this->mRes*this->q[0]*pow(this->q[1],4)*this->kg[1] * this->kg[1] - 
    C5v*C5vNC*this->mRes*this->q[0]*pow(this->q[1],4)*this->kg[1] * this->kg[1] - 
    4*C4v*C4vNC*pow(this->mRes,3)*this->q[0]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
    4*C4v*C5vNC*pow(this->mRes,3)*this->q[0]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
    4*C4vNC*C5v*this->mRes*pow(p0,2)*this->q[0]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
    8*C4v*C5vNC*this->mRes*pow(p0,2)*this->q[0]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
    20*C5v*C5vNC*this->mRes*pow(p0,2)*this->q[0]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
    16*C5v*C5vNC*this->mn*pow(p0,2)*this->q[0]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
    10*C4vNC*C5v*this->mRes*p0*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
    12*C4v*C5vNC*this->mRes*p0*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
    14*C5v*C5vNC*this->mRes*p0*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
    24*C5v*C5vNC*this->mn*p0*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
    6*C4vNC*C5v*this->mRes*pow(this->q[0],3)*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
    5*C4v*C5vNC*this->mRes*pow(this->q[0],3)*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
    C5v*C5vNC*this->mRes*pow(this->q[0],3)*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
    8*C5v*C5vNC*this->mn*pow(this->q[0],3)*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
    C4vNC*C5v*this->mRes*this->q[0]*this->q[1] * this->q[1]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
    C4v*C5vNC*this->mRes*this->q[0]*this->q[1] * this->q[1]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
    2*C5v*C5vNC*this->mRes*this->q[0]*this->q[1] * this->q[1]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
    C4vNC*C5v*this->mRes*this->q[0]*pow(this->q[3],4)*this->kg[1] * this->kg[1] - 
    C5v*C5vNC*this->mRes*this->q[0]*pow(this->q[3],4)*this->kg[1] * this->kg[1] - 
    4*C4v*C4vNC*pow(this->mRes,3)*this->q[0]*this->q[1]*pow(this->kg[1],3) - 
    4*C4v*C5vNC*pow(this->mRes,3)*this->q[0]*this->q[1]*pow(this->kg[1],3) + 
    4*C4vNC*C5v*this->mRes*pow(p0,2)*this->q[0]*this->q[1]*pow(this->kg[1],3) + 
    4*C5v*C5vNC*this->mRes*pow(p0,2)*this->q[0]*this->q[1]*pow(this->kg[1],3) - 
    8*C4vNC*C5v*this->mRes*p0*this->q[0] * this->q[0]*this->q[1]*pow(this->kg[1],3) - 
    4*C4v*C5vNC*this->mRes*p0*this->q[0] * this->q[0]*this->q[1]*pow(this->kg[1],3) - 
    12*C5v*C5vNC*this->mRes*p0*this->q[0] * this->q[0]*this->q[1]*pow(this->kg[1],3) - 
    8*C5v*C5vNC*this->mn*p0*this->q[0] * this->q[0]*this->q[1]*pow(this->kg[1],3) + 
    3*C4vNC*C5v*this->mRes*pow(this->q[0],3)*this->q[1]*pow(this->kg[1],3) + 
    4*C4v*C5vNC*this->mRes*pow(this->q[0],3)*this->q[1]*pow(this->kg[1],3) - 
    C5v*C5vNC*this->mRes*pow(this->q[0],3)*this->q[1]*pow(this->kg[1],3) + 
    8*C5v*C5vNC*this->mn*pow(this->q[0],3)*this->q[1]*pow(this->kg[1],3) + 
    4*C4vNC*C5v*this->mRes*p0*pow(this->q[1],3)*pow(this->kg[1],3) + 
    4*C4v*C5vNC*this->mRes*p0*pow(this->q[1],3)*pow(this->kg[1],3) + 
    8*C5v*C5vNC*this->mRes*p0*pow(this->q[1],3)*pow(this->kg[1],3) + 
    8*C5v*C5vNC*this->mn*p0*pow(this->q[1],3)*pow(this->kg[1],3) - 
    3*C4vNC*C5v*this->mRes*this->q[0]*pow(this->q[1],3)*pow(this->kg[1],3) - 
    4*C4v*C5vNC*this->mRes*this->q[0]*pow(this->q[1],3)*pow(this->kg[1],3) + 
    C5v*C5vNC*this->mRes*this->q[0]*pow(this->q[1],3)*pow(this->kg[1],3) - 
    8*C5v*C5vNC*this->mn*this->q[0]*pow(this->q[1],3)*pow(this->kg[1],3) - 
    4*C4vNC*C5v*this->mRes*p0*this->q[1]*this->q[3] * this->q[3]*pow(this->kg[1],3) - 
    4*C4v*C5vNC*this->mRes*p0*this->q[1]*this->q[3] * this->q[3]*pow(this->kg[1],3) - 
    8*C5v*C5vNC*this->mRes*p0*this->q[1]*this->q[3] * this->q[3]*pow(this->kg[1],3) - 
    8*C5v*C5vNC*this->mn*p0*this->q[1]*this->q[3] * this->q[3]*pow(this->kg[1],3) + 
    5*C4vNC*C5v*this->mRes*this->q[0]*this->q[1]*this->q[3] * this->q[3]*pow(this->kg[1],3) + 
    4*C4v*C5vNC*this->mRes*this->q[0]*this->q[1]*this->q[3] * this->q[3]*pow(this->kg[1],3) + 
    C5v*C5vNC*this->mRes*this->q[0]*this->q[1]*this->q[3] * this->q[3]*pow(this->kg[1],3) + 
    8*C5v*C5vNC*this->mn*this->q[0]*this->q[1]*this->q[3] * this->q[3]*pow(this->kg[1],3) + 
    std::complex<double>(0,4)*C4v*C5a*pow(this->mRes,3)*this->mn2*this->q[1]*this->q[3]*this->kg[2] + 
    std::complex<double>(0,4)*C5a*C5v*pow(this->mRes,3)*this->mn2*this->q[1]*this->q[3]*this->kg[2] + 
    std::complex<double>(0,4)*C4v*C5a*this->mRes_2*pow(this->mn,3)*this->q[1]*this->q[3]*this->kg[2] + 
    std::complex<double>(0,4)*C5a*C5v*this->mRes_2*pow(this->mn,3)*this->q[1]*this->q[3]*this->kg[2] + 
    std::complex<double>(0,4)*C4v*C5a*this->mRes*this->mn2*p0*this->q[0]*this->q[1]*this->q[3]*this->kg[2] + 
    std::complex<double>(0,4)*C5a*C5v*this->mRes*this->mn2*p0*this->q[0]*this->q[1]*this->q[3]*this->kg[2] - 
    std::complex<double>(0,4)*C5a*C5v*this->mRes*this->mn2*this->q[0] * this->q[0]*this->q[1]*this->q[3]*this->kg[2] + 
    std::complex<double>(0,2)*C4v*C5a*this->mRes*this->mn2*this->q[1] * this->q[1]*this->q[3]*this->kg[1]*this->kg[2] + 
    std::complex<double>(0,2)*C5a*C5v*this->mRes*this->mn2*this->q[1] * this->q[1]*this->q[3]*this->kg[1]*this->kg[2] - 
    std::complex<double>(0,2)*C4v*C5a*this->mRes*this->mn2*this->q[1]*this->q[3]*this->kg[1] * this->kg[1]*this->kg[2] + 
    std::complex<double>(0,2)*C5a*C5v*this->mRes*this->mn2*this->q[1]*this->q[3]*this->kg[1] * this->kg[1]*this->kg[2] + 
    16*C4v*C4vNC*pow(this->mRes,3)*pow(p0,2)*this->q[0]*this->kg[2] * this->kg[2] - 
    16*C4vNC*C5v*pow(this->mRes,3)*pow(p0,2)*this->q[0]*this->kg[2] * this->kg[2] + 
    16*C4v*C5vNC*pow(this->mRes,3)*pow(p0,2)*this->q[0]*this->kg[2] * this->kg[2] - 
    16*C5v*C5vNC*pow(this->mRes,3)*pow(p0,2)*this->q[0]*this->kg[2] * this->kg[2] + 
    16*C4v*C4vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[0]*this->kg[2] * this->kg[2] - 
    16*C4vNC*C5v*this->mRes_2*this->mn*pow(p0,2)*this->q[0]*this->kg[2] * this->kg[2] + 
    16*C4v*C5vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[0]*this->kg[2] * this->kg[2] - 
    16*C5v*C5vNC*this->mRes_2*this->mn*pow(p0,2)*this->q[0]*this->kg[2] * this->kg[2] - 
    8*C4v*C4vNC*pow(this->mRes,3)*p0*this->q[0] * this->q[0]*this->kg[2] * this->kg[2] + 
    8*C4v*C5vNC*pow(this->mRes,3)*p0*this->q[0] * this->q[0]*this->kg[2] * this->kg[2] - 
    16*C5v*C5vNC*pow(this->mRes,3)*p0*this->q[0] * this->q[0]*this->kg[2] * this->kg[2] - 
    24*C4v*C4vNC*this->mRes_2*this->mn*p0*this->q[0] * this->q[0]*this->kg[2] * this->kg[2] + 
    24*C4vNC*C5v*this->mRes_2*this->mn*p0*this->q[0] * this->q[0]*this->kg[2] * this->kg[2] - 
    8*C4v*C5vNC*this->mRes_2*this->mn*p0*this->q[0] * this->q[0]*this->kg[2] * this->kg[2] + 
    8*C5v*C5vNC*this->mRes_2*this->mn*p0*this->q[0] * this->q[0]*this->kg[2] * this->kg[2] + 
    8*C4vNC*C5v*this->mRes*pow(p0,3)*this->q[0] * this->q[0]*this->kg[2] * this->kg[2] + 
    8*C5v*C5vNC*this->mRes*pow(p0,3)*this->q[0] * this->q[0]*this->kg[2] * this->kg[2] + 
    4*C4vNC*C5v*pow(this->mRes,3)*pow(this->q[0],3)*this->kg[2] * this->kg[2] + 
    4*C4v*C5vNC*pow(this->mRes,3)*pow(this->q[0],3)*this->kg[2] * this->kg[2] + 
    8*C4v*C4vNC*this->mRes_2*this->mn*pow(this->q[0],3)*this->kg[2] * this->kg[2] - 
    8*C4vNC*C5v*this->mRes_2*this->mn*pow(this->q[0],3)*this->kg[2] * this->kg[2] - 
    8*C4v*C5vNC*this->mRes_2*this->mn*pow(this->q[0],3)*this->kg[2] * this->kg[2] + 
    8*C5v*C5vNC*this->mRes_2*this->mn*pow(this->q[0],3)*this->kg[2] * this->kg[2] - 
    20*C4vNC*C5v*this->mRes*pow(p0,2)*pow(this->q[0],3)*this->kg[2] * this->kg[2] - 
    4*C4v*C5vNC*this->mRes*pow(p0,2)*pow(this->q[0],3)*this->kg[2] * this->kg[2] - 
    32*C5v*C5vNC*this->mRes*pow(p0,2)*pow(this->q[0],3)*this->kg[2] * this->kg[2] - 
    16*C5v*C5vNC*this->mn*pow(p0,2)*pow(this->q[0],3)*this->kg[2] * this->kg[2] + 
    14*C4vNC*C5v*this->mRes*p0*pow(this->q[0],4)*this->kg[2] * this->kg[2] + 
    8*C4v*C5vNC*this->mRes*p0*pow(this->q[0],4)*this->kg[2] * this->kg[2] + 
    14*C5v*C5vNC*this->mRes*p0*pow(this->q[0],4)*this->kg[2] * this->kg[2] + 
    24*C5v*C5vNC*this->mn*p0*pow(this->q[0],4)*this->kg[2] * this->kg[2] - 
    3*C4vNC*C5v*this->mRes*pow(this->q[0],5)*this->kg[2] * this->kg[2] - 
    3*C4v*C5vNC*this->mRes*pow(this->q[0],5)*this->kg[2] * this->kg[2] - 
    8*C5v*C5vNC*this->mn*pow(this->q[0],5)*this->kg[2] * this->kg[2] + 
    8*C4v*C4vNC*pow(this->mRes,3)*p0*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] - 
    8*C4vNC*C5v*pow(this->mRes,3)*p0*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] - 
    8*C4v*C5vNC*pow(this->mRes,3)*p0*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] + 
    8*C5v*C5vNC*pow(this->mRes,3)*p0*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] + 
    8*C4v*C4vNC*this->mRes_2*this->mn*p0*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] - 
    8*C4vNC*C5v*this->mRes_2*this->mn*p0*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] - 
    8*C4v*C5vNC*this->mRes_2*this->mn*p0*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] + 
    8*C5v*C5vNC*this->mRes_2*this->mn*p0*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] + 
    4*C4v*C4vNC*pow(this->mRes,3)*this->q[0]*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] - 
    4*C4vNC*C5v*pow(this->mRes,3)*this->q[0]*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] - 
    8*C4v*C4vNC*this->mRes_2*this->mn*this->q[0]*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] + 
    8*C4vNC*C5v*this->mRes_2*this->mn*this->q[0]*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] + 
    8*C4v*C5vNC*this->mRes_2*this->mn*this->q[0]*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] - 
    8*C5v*C5vNC*this->mRes_2*this->mn*this->q[0]*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] + 
    8*C4vNC*C5v*this->mRes*pow(p0,2)*this->q[0]*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] + 
    4*C4v*C5vNC*this->mRes*pow(p0,2)*this->q[0]*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] + 
    20*C5v*C5vNC*this->mRes*pow(p0,2)*this->q[0]*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] + 
    16*C5v*C5vNC*this->mn*pow(p0,2)*this->q[0]*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] - 
    10*C4vNC*C5v*this->mRes*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] - 
    8*C4v*C5vNC*this->mRes*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] - 
    10*C5v*C5vNC*this->mRes*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] - 
    24*C5v*C5vNC*this->mn*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] + 
    3*C4vNC*C5v*this->mRes*pow(this->q[0],3)*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] + 
    2*C4v*C5vNC*this->mRes*pow(this->q[0],3)*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] + 
    C5v*C5vNC*this->mRes*pow(this->q[0],3)*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] + 
    8*C5v*C5vNC*this->mn*pow(this->q[0],3)*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] + 
    C4v*C5vNC*this->mRes*this->q[0]*pow(this->q[1],4)*this->kg[2] * this->kg[2] - 
    C5v*C5vNC*this->mRes*this->q[0]*pow(this->q[1],4)*this->kg[2] * this->kg[2] - 
    4*C4v*C4vNC*pow(this->mRes,3)*this->q[0]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] + 
    4*C4v*C5vNC*pow(this->mRes,3)*this->q[0]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] - 
    4*C4vNC*C5v*this->mRes*pow(p0,2)*this->q[0]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] - 
    8*C4v*C5vNC*this->mRes*pow(p0,2)*this->q[0]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] - 
    20*C5v*C5vNC*this->mRes*pow(p0,2)*this->q[0]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] - 
    16*C5v*C5vNC*this->mn*pow(p0,2)*this->q[0]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] + 
    10*C4vNC*C5v*this->mRes*p0*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] + 
    12*C4v*C5vNC*this->mRes*p0*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] + 
    14*C5v*C5vNC*this->mRes*p0*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] + 
    24*C5v*C5vNC*this->mn*p0*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] - 
    6*C4vNC*C5v*this->mRes*pow(this->q[0],3)*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] - 
    5*C4v*C5vNC*this->mRes*pow(this->q[0],3)*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] + 
    C5v*C5vNC*this->mRes*pow(this->q[0],3)*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] - 
    8*C5v*C5vNC*this->mn*pow(this->q[0],3)*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] + 
    C4vNC*C5v*this->mRes*this->q[0]*this->q[1] * this->q[1]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] + 
    C4v*C5vNC*this->mRes*this->q[0]*this->q[1] * this->q[1]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] - 
    2*C5v*C5vNC*this->mRes*this->q[0]*this->q[1] * this->q[1]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] + 
    C4vNC*C5v*this->mRes*this->q[0]*pow(this->q[3],4)*this->kg[2] * this->kg[2] - 
    C5v*C5vNC*this->mRes*this->q[0]*pow(this->q[3],4)*this->kg[2] * this->kg[2] - 
    4*C4v*C4vNC*pow(this->mRes,3)*this->q[0]*this->q[1]*this->kg[1]*this->kg[2] * this->kg[2] - 
    4*C4v*C5vNC*pow(this->mRes,3)*this->q[0]*this->q[1]*this->kg[1]*this->kg[2] * this->kg[2] + 
    4*C4vNC*C5v*this->mRes*pow(p0,2)*this->q[0]*this->q[1]*this->kg[1]*this->kg[2] * this->kg[2] + 
    4*C5v*C5vNC*this->mRes*pow(p0,2)*this->q[0]*this->q[1]*this->kg[1]*this->kg[2] * this->kg[2] - 
    8*C4vNC*C5v*this->mRes*p0*this->q[0] * this->q[0]*this->q[1]*this->kg[1]*this->kg[2] * this->kg[2] - 
    4*C4v*C5vNC*this->mRes*p0*this->q[0] * this->q[0]*this->q[1]*this->kg[1]*this->kg[2] * this->kg[2] - 
    12*C5v*C5vNC*this->mRes*p0*this->q[0] * this->q[0]*this->q[1]*this->kg[1]*this->kg[2] * this->kg[2] - 
    8*C5v*C5vNC*this->mn*p0*this->q[0] * this->q[0]*this->q[1]*this->kg[1]*this->kg[2] * this->kg[2] + 
    3*C4vNC*C5v*this->mRes*pow(this->q[0],3)*this->q[1]*this->kg[1]*this->kg[2] * this->kg[2] + 
    4*C4v*C5vNC*this->mRes*pow(this->q[0],3)*this->q[1]*this->kg[1]*this->kg[2] * this->kg[2] - 
    C5v*C5vNC*this->mRes*pow(this->q[0],3)*this->q[1]*this->kg[1]*this->kg[2] * this->kg[2] + 
    8*C5v*C5vNC*this->mn*pow(this->q[0],3)*this->q[1]*this->kg[1]*this->kg[2] * this->kg[2] + 
    4*C4vNC*C5v*this->mRes*p0*pow(this->q[1],3)*this->kg[1]*this->kg[2] * this->kg[2] + 
    4*C4v*C5vNC*this->mRes*p0*pow(this->q[1],3)*this->kg[1]*this->kg[2] * this->kg[2] + 
    8*C5v*C5vNC*this->mRes*p0*pow(this->q[1],3)*this->kg[1]*this->kg[2] * this->kg[2] + 
    8*C5v*C5vNC*this->mn*p0*pow(this->q[1],3)*this->kg[1]*this->kg[2] * this->kg[2] - 
    3*C4vNC*C5v*this->mRes*this->q[0]*pow(this->q[1],3)*this->kg[1]*this->kg[2] * this->kg[2] - 
    4*C4v*C5vNC*this->mRes*this->q[0]*pow(this->q[1],3)*this->kg[1]*this->kg[2] * this->kg[2] + 
    C5v*C5vNC*this->mRes*this->q[0]*pow(this->q[1],3)*this->kg[1]*this->kg[2] * this->kg[2] - 
    8*C5v*C5vNC*this->mn*this->q[0]*pow(this->q[1],3)*this->kg[1]*this->kg[2] * this->kg[2] - 
    4*C4vNC*C5v*this->mRes*p0*this->q[1]*this->q[3] * this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] - 
    4*C4v*C5vNC*this->mRes*p0*this->q[1]*this->q[3] * this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] - 
    8*C5v*C5vNC*this->mRes*p0*this->q[1]*this->q[3] * this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] - 
    8*C5v*C5vNC*this->mn*p0*this->q[1]*this->q[3] * this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] + 
    5*C4vNC*C5v*this->mRes*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] + 
    4*C4v*C5vNC*this->mRes*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] + 
    C5v*C5vNC*this->mRes*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] + 
    8*C5v*C5vNC*this->mn*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] - 
    std::complex<double>(0,2)*C4v*C5a*this->mRes*this->mn2*this->q[1]*this->q[3]*pow(this->kg[2],3) + 
    std::complex<double>(0,2)*C5a*C5v*this->mRes*this->mn2*this->q[1]*this->q[3]*pow(this->kg[2],3) + 
    (C4v*this->mRes*(std::complex<double>(0,2)*C5a*this->mn2*this->q[1]*
           (2*this->mRes*(this->mRes + this->mn) + 2*p0*this->q[0] - this->q[0] * this->q[0] + 
             this->q[3] * this->q[3] + this->q[1]*this->kg[1])*this->kg[2] - 
          4*C4vNC*this->mRes*this->q[3]*(4*this->mRes*p0*(this->q[0] * this->q[0] - this->q[1]*this->kg[1]) + 
             4*this->mn*(p0 - this->q[0])*(this->q[0] * this->q[0] - this->q[1]*this->kg[1]) + 
             this->mRes*this->q[0]*(pow(this->q[1] - this->kg[1],2) + this->kg[2] * this->kg[2])) + 
          C5vNC*this->q[3]*(8*pow(p0,3)*this->q[0] * this->q[0] - 8*pow(this->q[0],5) + 
             4*pow(p0,2)*this->q[0]*(-6*this->q[0] * this->q[0] + this->q[1]*(this->q[1] + 3*this->kg[1])) - 
             4*this->mRes_2*this->q[0]*
              (2*(p0 - this->q[0])*this->q[0] + this->kg[1]*(this->q[1] + this->kg[1]) + this->kg[2] * this->kg[2]) + 
             pow(this->q[0],3)*(5*this->q[1] * this->q[1] + 7*this->q[1]*this->kg[1] + 
                4*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])) - 
             this->q[0]*this->q[1]*(pow(this->q[1],3) - this->q[1] * this->q[1]*this->kg[1] - this->q[3] * this->q[3]*this->kg[1] + 
                this->q[1]*(this->q[3] * this->q[3] + 8*this->kg[1] * this->kg[1] + 4*this->kg[2] * this->kg[2])) + 
             2*p0*(11*pow(this->q[0],4) + 
                2*this->q[1] * this->q[1]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + 
                this->q[0] * this->q[0]*(-3*this->q[1] * this->q[1] + this->q[3] * this->q[3] - 10*this->q[1]*this->kg[1] - 
                   2*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]))))) + 
       C5v*(std::complex<double>(0,-2)*C5a*this->mRes*this->mn2*this->q[1]*
           (2*this->mRes*(this->mRes + this->mn) + 2*p0*this->q[0] - this->q[0] * this->q[0] - 
             this->q[3] * this->q[3] + this->q[1]*this->kg[1])*this->kg[2] + 
          C4vNC*this->mRes*this->q[3]*(8*pow(p0,3)*this->q[0] * this->q[0] + 
             26*p0*pow(this->q[0],4) - 8*pow(this->q[0],5) - 
             this->q[0]*this->kg[1]*(pow(this->q[1],3) + this->q[1]*this->q[3] * this->q[3] + 7*this->q[1] * this->q[1]*this->kg[1] - 
                this->q[3] * this->q[3]*this->kg[1]) - 
             4*this->mRes_2*this->q[0]*(2*(p0 - this->q[0])*this->q[0] + this->q[1]*(this->q[1] + this->kg[1])) + 
             this->q[0]*(-3*this->q[1] * this->q[1] + this->q[3] * this->q[3])*this->kg[2] * this->kg[2] + 
             4*pow(p0,2)*this->q[0]*
              (-6*this->q[0] * this->q[0] + 3*this->q[1]*this->kg[1] + this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])  
+ 4*p0*this->q[1] * this->q[1]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + 
             pow(this->q[0],3)*(4*this->q[1] * this->q[1] + 9*this->q[1]*this->kg[1] + 
                3*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])) - 
             2*p0*this->q[0] * this->q[0]*
              (3*this->q[1] * this->q[1] + this->q[3] * this->q[3] + 10*this->q[1]*this->kg[1] + 
                4*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]))) + 
          C5vNC*this->q[3]*(-16*this->mRes_2*this->mn*(p0 - this->q[0])*
              (this->q[0] * this->q[0] - this->q[1]*this->kg[1]) + 
             16*pow(this->mRes,3)*p0*(-2*this->q[0] * this->q[0] + this->q[1]*this->kg[1]) + 
             8*this->mn*(p0 - this->q[0])*
              (4*pow(p0,2)*this->q[0] * this->q[0] + 2*pow(this->q[0],4) - 
                4*p0*(pow(this->q[0],3) - this->q[0]*this->q[1]*this->kg[1]) + 
                this->q[1] * this->q[1]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) - 
                this->q[0] * this->q[0]*(pow(this->q[1] + this->kg[1],2) + this->kg[2] * this->kg[2])) + 
             this->mRes*(48*pow(p0,3)*this->q[0] * this->q[0] + 
                this->q[0]*(this->q[0] * this->q[0] - this->q[1] * this->q[1] - this->q[3] * this->q[3])*
                 (this->q[1] * this->q[1] - this->kg[1] * this->kg[1] - this->kg[2] * this->kg[2]) + 
                4*pow(p0,2)*this->q[0]*
                 (-16*this->q[0] * this->q[0] + this->q[1] * this->q[1] + 10*this->q[1]*this->kg[1] + 
                   this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + 
                4*p0*(8*pow(this->q[0],4) + 
                   2*this->q[1] * this->q[1]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) - 
                   3*this->q[0] * this->q[0]*(pow(this->q[1] + this->kg[1],2) + this->kg[2] * this->kg[2]))))))*
     this->kg[3] + 2*C3vNC*this->mn*(C5v*(4*pow(this->mRes,3)*this->mn*this->q[0]*
           (2*p0*this->q[0] + (this->q[1] - this->kg[1])*this->kg[1] - this->kg[2] * this->kg[2]) + 
          8*pow(p0,3)*this->q[0] * this->q[0]*(this->q[3] - this->kg[3])*this->kg[3] + 
          4*pow(p0,2)*this->q[0]*((5*this->q[0] * this->q[0] + this->q[3] * this->q[3] - 
                this->q[1]*(2*this->q[1] + this->kg[1]))*
              (this->q[0] * this->q[0] - this->kg[1] * this->kg[1] - this->kg[2] * this->kg[2]) + 
             this->q[3]*(-6*this->q[0] * this->q[0] + 3*this->q[1]*this->kg[1] + this->kg[1] * this->kg[1] + 
                this->kg[2] * this->kg[2])*this->kg[3]) + 
          2*p0*(-((7*pow(this->q[0],4) + 2*this->q[1]*(this->q[1] - this->q[3])*(this->q[1] + this->q[3])*this->kg[1] + 
                  this->q[0] * this->q[0]*(5*this->q[3] * this->q[3] - 4*this->q[1]*this->kg[1]))*
                (this->q[0] * this->q[0] - this->kg[1] * this->kg[1] - this->kg[2] * this->kg[2])) - 
             this->q[3]*(-13*pow(this->q[0],4) - 
                2*this->q[1] * this->q[1]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + 
                this->q[0] * this->q[0]*(3*this->q[1] * this->q[1] + this->q[3] * this->q[3] + 10*this->q[1]*this->kg[1] + 
                   4*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])))*this->kg[3] + 
             5*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[3] * this->kg[3]) + 
          this->q[0]*((this->q[0] * this->q[0]*(5*this->q[3] * this->q[3] + this->q[1]*this->kg[1]) - 
                this->q[3] * this->q[3]*(this->q[1] * this->q[1] + this->q[3] * this->q[3] + 5*this->q[1]*this->kg[1]))*
              (this->q[0] * this->q[0] - this->kg[1] * this->kg[1] - this->kg[2] * this->kg[2]) + 
             this->q[3]*(-8*pow(this->q[0],4) - 
                this->kg[1]*(pow(this->q[1],3) + this->q[1]*this->q[3] * this->q[3] + 
                   7*this->q[1] * this->q[1]*this->kg[1] - this->q[3] * this->q[3]*this->kg[1]) + 
                (-3*this->q[1] * this->q[1] + this->q[3] * this->q[3])*this->kg[2] * this->kg[2] + 
                this->q[0] * this->q[0]*(4*this->q[1] * this->q[1] + 9*this->q[1]*this->kg[1] + 
                   3*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])))*this->kg[3] + 
             (3*pow(this->q[0],4) + 3*pow(this->q[1],3)*this->kg[1] + 
                this->q[0] * this->q[0]*(-3*this->q[1] * this->q[1] + this->q[3] * this->q[3] - 4*this->q[1]*this->kg[1]))*
              this->kg[3] * this->kg[3]) + 
          2*this->mRes*this->mn*(4*pow(p0,3)*this->q[0] * this->q[0] - 
             2*pow(p0,2)*this->q[0]*
              (4*this->q[0] * this->q[0] - this->q[1] * this->q[1] - 2*this->q[1]*this->kg[1] + this->kg[1] * this->kg[1] + 
                this->kg[2] * this->kg[2]) - 
             this->q[0]*(2*pow(this->q[0],4) + pow(this->q[1],3)*this->kg[1] - 
                2*this->q[0] * this->q[0]*(this->q[3] * this->q[3] + 2*this->q[1]*this->kg[1]) - 
                this->q[1] * this->q[1]*this->kg[2] * this->kg[2] + 
                this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])*(this->q[3] + this->kg[3]) + 
                this->q[1]*this->kg[1]*(this->q[3] * this->q[3] + this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] - 
                   this->q[3]*this->kg[3])) + 
             p0*(7*pow(this->q[0],4) + 
                this->q[1]*((this->q[1] * this->q[1] + this->q[3] * this->q[3])*this->kg[1] - this->q[1]*this->kg[2] * this->kg[2]) + 
                this->q[0] * this->q[0]*(-2*this->q[1] * this->q[1] - this->q[3] * this->q[3] - 7*this->q[1]*this->kg[1] + 
                   this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + 2*this->q[3]*this->kg[3]))) + 
          this->mRes_2*(16*pow(p0,3)*this->q[0] * this->q[0] + 
             4*pow(p0,2)*this->q[0]*
              (-4*this->q[0] * this->q[0] + this->q[1] * this->q[1] + 5*this->q[1]*this->kg[1] - 
                2*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])) + 
             this->q[0]*(pow(this->q[1],3)*this->kg[1] + 
                this->q[3] * this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) - 
                this->q[0] * this->q[0]*(-3*this->q[1]*this->kg[1] + this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + 
                   4*pow(this->q[3] - this->kg[3],2)) + 3*this->q[1]*this->q[3]*this->kg[1]*(this->q[3] - 2*this->kg[3]) - 
                this->q[1] * this->q[1]*(3*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2] + 
                   2*(this->q[3] - this->kg[3])*this->kg[3])) + 
             2*p0*(6*pow(this->q[0],4) + 
                this->q[0] * this->q[0]*(this->q[1] * this->q[1] - 9*this->q[1]*this->kg[1] - this->kg[1] * this->kg[1] - 
                   this->kg[2] * this->kg[2] - 2*this->q[3]*(this->q[3] + 2*this->kg[3])) + 
                this->q[1]*(this->q[1] * this->q[1]*this->kg[1] + 
                   this->q[1]*(2*this->kg[1] * this->kg[1] - this->kg[2] * this->kg[2]) + 
                   this->q[3]*this->kg[1]*(this->q[3] + 2*this->kg[3]))))) + 
       C4v*this->mRes*(4*this->mRes_2*this->mn*this->q[0]*
           (2*(p0 - this->q[0])*this->q[0] + this->kg[1]*(this->q[1] + this->kg[1]) + this->kg[2] * this->kg[2]) + 
          2*this->mn*(p0 - this->q[0])*(4*pow(p0,2)*this->q[0] * this->q[0] + 3*pow(this->q[0],4) + 
             2*p0*this->q[0]*(-4*this->q[0] * this->q[0] + pow(this->q[1] + this->kg[1],2) + 
                this->kg[2] * this->kg[2]) - 
             this->q[0] * this->q[0]*(this->q[3] * this->q[3] + (this->q[1] + this->kg[1])*(2*this->q[1] + this->kg[1]) + 
                this->kg[2] * this->kg[2] + 2*this->q[3]*this->kg[3]) + 
             this->q[1]*(this->q[1] * this->q[1]*this->kg[1] + this->q[1]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + 
                this->q[3]*this->kg[1]*(this->q[3] + 2*this->kg[3]))) + 
          this->mRes*(16*pow(p0,3)*this->q[0] * this->q[0] + 
             4*pow(p0,2)*this->q[0]*
              (-8*this->q[0] * this->q[0] + this->q[1] * this->q[1] + 5*this->q[1]*this->kg[1] + 
                2*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])) + 
             2*p0*(6*pow(this->q[0],4) - 
                this->q[0] * this->q[0]*(this->q[1] * this->q[1] + 9*this->q[1]*this->kg[1] + this->kg[1] * this->kg[1] + 
                   this->kg[2] * this->kg[2] + 2*this->q[3]*(this->q[3] + 2*this->kg[3])) + 
                this->q[1]*(this->q[1] * this->q[1]*this->kg[1] + 
                   this->q[1]*(4*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + 
                   this->q[3]*this->kg[1]*(this->q[3] + 4*this->kg[3]))) + 
             this->q[0]*(pow(this->q[1],3)*this->kg[1] - 
                this->q[0] * this->q[0]*(2*this->q[1] * this->q[1] - 5*this->q[1]*this->kg[1] + this->kg[1] * this->kg[1] + 
                   this->kg[2] * this->kg[2]) - 
                this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])*(3*this->q[3] + 2*this->kg[3]) - 
                this->q[1] * this->q[1]*(this->kg[1] * this->kg[1] - this->kg[2] * this->kg[2] + 
                   2*this->kg[3]*(this->q[3] + this->kg[3])) + 
                this->q[1]*this->kg[1]*(-2*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + 
                   this->q[3]*(3*this->q[3] + 4*this->kg[3])))))) + 
    2*C3v*this->mn*(8*C5vNC*pow(this->mRes,3)*this->mn*p0*this->q[0] * this->q[0] + 
       16*C5vNC*this->mRes_2*pow(p0,3)*this->q[0] * this->q[0] + 
       8*C5vNC*this->mRes*this->mn*pow(p0,3)*this->q[0] * this->q[0] - 
       16*C5vNC*this->mRes_2*pow(p0,2)*pow(this->q[0],3) - 
       16*C5vNC*this->mRes*this->mn*pow(p0,2)*pow(this->q[0],3) - 
       4*C5vNC*this->mRes_2*p0*pow(this->q[0],4) + 
       6*C5vNC*this->mRes*this->mn*p0*pow(this->q[0],4) - 
       4*C5vNC*this->mRes_2*pow(this->q[0],5) + 4*C5vNC*this->mRes*this->mn*pow(this->q[0],5) + 
       4*C5vNC*pow(p0,2)*pow(this->q[0],5) - 8*C5vNC*p0*pow(this->q[0],6) + 
       3*C5vNC*pow(this->q[0],7) - 4*C5vNC*pow(this->mRes,3)*this->mn*this->q[0]*this->q[1] * this->q[1] - 
       8*C5vNC*this->mRes_2*pow(p0,2)*this->q[0]*this->q[1] * this->q[1] - 
       4*C5vNC*this->mRes*this->mn*pow(p0,2)*this->q[0]*this->q[1] * this->q[1] + 
       10*C5vNC*this->mRes_2*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1] + 
       8*C5vNC*this->mRes*this->mn*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1] + 
       C5vNC*this->mRes_2*pow(this->q[0],3)*this->q[1] * this->q[1] - 
       6*C5vNC*this->mRes*this->mn*pow(this->q[0],3)*this->q[1] * this->q[1] - 
       4*C5vNC*pow(p0,2)*pow(this->q[0],3)*this->q[1] * this->q[1] + 
       8*C5vNC*p0*pow(this->q[0],4)*this->q[1] * this->q[1] - 
       2*C5vNC*pow(this->q[0],5)*this->q[1] * this->q[1] - 
       C5vNC*this->mRes_2*this->q[0]*pow(this->q[1],4) - C5vNC*pow(this->q[0],3)*pow(this->q[1],4) + 
       12*C5vNC*this->mRes_2*p0*this->q[0] * this->q[0]*this->q[3] * this->q[3] + 
       6*C5vNC*this->mRes*this->mn*p0*this->q[0] * this->q[0]*this->q[3] * this->q[3] - 
       8*C5vNC*pow(p0,3)*this->q[0] * this->q[0]*this->q[3] * this->q[3] - 
       4*C5vNC*this->mRes_2*pow(this->q[0],3)*this->q[3] * this->q[3] - 
       4*C5vNC*this->mRes*this->mn*pow(this->q[0],3)*this->q[3] * this->q[3] + 
       20*C5vNC*pow(p0,2)*pow(this->q[0],3)*this->q[3] * this->q[3] - 
       18*C5vNC*p0*pow(this->q[0],4)*this->q[3] * this->q[3] + 
       6*C5vNC*pow(this->q[0],5)*this->q[3] * this->q[3] - 
       C5vNC*this->mRes_2*this->q[0]*this->q[1] * this->q[1]*this->q[3] * this->q[3] + 
       2*C5vNC*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->q[3] * this->q[3] - 
       2*C5vNC*pow(this->q[0],3)*this->q[1] * this->q[1]*this->q[3] * this->q[3] + 
       2*C5vNC*p0*this->q[0] * this->q[0]*pow(this->q[3],4) - 
       C5vNC*pow(this->q[0],3)*pow(this->q[3],4) + 4*C5vNC*pow(this->mRes,3)*this->mn*this->q[0]*this->q[1]*this->kg[1] + 
       20*C5vNC*this->mRes_2*pow(p0,2)*this->q[0]*this->q[1]*this->kg[1] + 
       8*C5vNC*this->mRes*this->mn*pow(p0,2)*this->q[0]*this->q[1]*this->kg[1] - 
       10*C5vNC*this->mRes_2*p0*this->q[0] * this->q[0]*this->q[1]*this->kg[1] - 
       10*C5vNC*this->mRes*this->mn*p0*this->q[0] * this->q[0]*this->q[1]*this->kg[1] + 
       3*C5vNC*this->mRes_2*pow(this->q[0],3)*this->q[1]*this->kg[1] + 
       2*C5vNC*this->mRes*this->mn*pow(this->q[0],3)*this->q[1]*this->kg[1] + 
       4*C5vNC*p0*pow(this->q[0],4)*this->q[1]*this->kg[1] - 4*C5vNC*pow(this->q[0],5)*this->q[1]*this->kg[1] - 
       6*C5vNC*this->mRes_2*p0*pow(this->q[1],3)*this->kg[1] - 
       2*C5vNC*this->mRes*this->mn*p0*pow(this->q[1],3)*this->kg[1] + 
       3*C5vNC*this->mRes_2*this->q[0]*pow(this->q[1],3)*this->kg[1] + 
       2*C5vNC*this->mRes*this->mn*this->q[0]*pow(this->q[1],3)*this->kg[1] - 
       4*C5vNC*p0*this->q[0] * this->q[0]*pow(this->q[1],3)*this->kg[1] + 
       4*C5vNC*pow(this->q[0],3)*pow(this->q[1],3)*this->kg[1] - 
       6*C5vNC*this->mRes_2*p0*this->q[1]*this->q[3] * this->q[3]*this->kg[1] - 
       2*C5vNC*this->mRes*this->mn*p0*this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 
       3*C5vNC*this->mRes_2*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 
       4*C5vNC*this->mRes*this->mn*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1] - 
       4*C5vNC*pow(p0,2)*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 
       8*C5vNC*p0*this->q[0] * this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1] - 
       5*C5vNC*pow(this->q[0],3)*this->q[1]*this->q[3] * this->q[3]*this->kg[1] + 
       C5vNC*this->q[0]*pow(this->q[1],3)*this->q[3] * this->q[3]*this->kg[1] + C5vNC*this->q[0]*this->q[1]*pow(this->q[3],4)*this->kg[1] + 
       4*C5vNC*this->mRes_2*pow(p0,2)*this->q[0]*this->kg[1] * this->kg[1] + 
       4*C5vNC*this->mRes*this->mn*pow(p0,2)*this->q[0]*this->kg[1] * this->kg[1] + 
       6*C5vNC*this->mRes_2*p0*this->q[0] * this->q[0]*this->kg[1] * this->kg[1] - 
       2*C5vNC*this->mRes*this->mn*p0*this->q[0] * this->q[0]*this->kg[1] * this->kg[1] + 
       4*C5vNC*this->mRes_2*pow(this->q[0],3)*this->kg[1] * this->kg[1] - 
       4*C5vNC*this->mRes*this->mn*pow(this->q[0],3)*this->kg[1] * this->kg[1] - 
       4*C5vNC*pow(p0,2)*pow(this->q[0],3)*this->kg[1] * this->kg[1] + 
       8*C5vNC*p0*pow(this->q[0],4)*this->kg[1] * this->kg[1] - 
       3*C5vNC*pow(this->q[0],5)*this->kg[1] * this->kg[1] + 
       4*C5vNC*this->mRes_2*p0*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] - 
       4*C5vNC*this->mRes_2*this->q[0]*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] + 
       2*C5vNC*this->mRes*this->mn*this->q[0]*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] + 
       4*C5vNC*pow(p0,2)*this->q[0]*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] - 
       8*C5vNC*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] + 
       2*C5vNC*pow(this->q[0],3)*this->q[1] * this->q[1]*this->kg[1] * this->kg[1] + 
       C5vNC*this->q[0]*pow(this->q[1],4)*this->kg[1] * this->kg[1] + 
       2*C5vNC*this->mRes_2*this->q[0]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
       8*C5vNC*pow(p0,2)*this->q[0]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
       12*C5vNC*p0*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
       5*C5vNC*pow(this->q[0],3)*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] + 
       C5vNC*this->q[0]*this->q[1] * this->q[1]*this->q[3] * this->q[3]*this->kg[1] * this->kg[1] - 
       2*C5vNC*this->mRes_2*this->q[0]*this->q[1]*pow(this->kg[1],3) - 
       4*C5vNC*p0*this->q[0] * this->q[0]*this->q[1]*pow(this->kg[1],3) + 
       4*C5vNC*pow(this->q[0],3)*this->q[1]*pow(this->kg[1],3) + 
       4*C5vNC*p0*pow(this->q[1],3)*pow(this->kg[1],3) - 
       4*C5vNC*this->q[0]*pow(this->q[1],3)*pow(this->kg[1],3) - 
       4*C5vNC*p0*this->q[1]*this->q[3] * this->q[3]*pow(this->kg[1],3) + 
       4*C5vNC*this->q[0]*this->q[1]*this->q[3] * this->q[3]*pow(this->kg[1],3) + 
       std::complex<double>(0,2)*C5a*this->mRes_2*this->mn2*this->q[1]*this->q[3]*this->kg[2] + 
       std::complex<double>(0,2)*C5a*this->mRes*pow(this->mn,3)*this->q[1]*this->q[3]*this->kg[2] + 
       std::complex<double>(0,4)*C5a*this->mn2*p0*this->q[0]*this->q[1]*this->q[3]*this->kg[2] + 
       std::complex<double>(0,2)*C5a*this->mn2*this->q[1] * this->q[1]*this->q[3]*this->kg[1]*this->kg[2] - 
       std::complex<double>(0,2)*C5a*this->mn2*this->q[1]*this->q[3]*this->kg[1] * this->kg[1]*this->kg[2] + 
       4*C5vNC*this->mRes_2*pow(p0,2)*this->q[0]*this->kg[2] * this->kg[2] + 
       4*C5vNC*this->mRes*this->mn*pow(p0,2)*this->q[0]*this->kg[2] * this->kg[2] + 
       6*C5vNC*this->mRes_2*p0*this->q[0] * this->q[0]*this->kg[2] * this->kg[2] - 
       2*C5vNC*this->mRes*this->mn*p0*this->q[0] * this->q[0]*this->kg[2] * this->kg[2] + 
       4*C5vNC*this->mRes_2*pow(this->q[0],3)*this->kg[2] * this->kg[2] - 
       4*C5vNC*this->mRes*this->mn*pow(this->q[0],3)*this->kg[2] * this->kg[2] - 
       4*C5vNC*pow(p0,2)*pow(this->q[0],3)*this->kg[2] * this->kg[2] + 
       8*C5vNC*p0*pow(this->q[0],4)*this->kg[2] * this->kg[2] - 
       3*C5vNC*pow(this->q[0],5)*this->kg[2] * this->kg[2] - 
       2*C5vNC*this->mRes_2*p0*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] - 
       2*C5vNC*this->mRes*this->mn*p0*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] - 
       2*C5vNC*this->mRes_2*this->q[0]*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] + 
       4*C5vNC*this->mRes*this->mn*this->q[0]*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] + 
       4*C5vNC*pow(p0,2)*this->q[0]*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] - 
       8*C5vNC*p0*this->q[0] * this->q[0]*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] + 
       2*C5vNC*pow(this->q[0],3)*this->q[1] * this->q[1]*this->kg[2] * this->kg[2] + 
       C5vNC*this->q[0]*pow(this->q[1],4)*this->kg[2] * this->kg[2] + 
       2*C5vNC*this->mRes_2*this->q[0]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] - 
       8*C5vNC*pow(p0,2)*this->q[0]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] + 
       12*C5vNC*p0*this->q[0] * this->q[0]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] - 
       5*C5vNC*pow(this->q[0],3)*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] + 
       C5vNC*this->q[0]*this->q[1] * this->q[1]*this->q[3] * this->q[3]*this->kg[2] * this->kg[2] - 
       2*C5vNC*this->mRes_2*this->q[0]*this->q[1]*this->kg[1]*this->kg[2] * this->kg[2] - 
       4*C5vNC*p0*this->q[0] * this->q[0]*this->q[1]*this->kg[1]*this->kg[2] * this->kg[2] + 
       4*C5vNC*pow(this->q[0],3)*this->q[1]*this->kg[1]*this->kg[2] * this->kg[2] + 
       4*C5vNC*p0*pow(this->q[1],3)*this->kg[1]*this->kg[2] * this->kg[2] - 
       4*C5vNC*this->q[0]*pow(this->q[1],3)*this->kg[1]*this->kg[2] * this->kg[2] - 
       4*C5vNC*p0*this->q[1]*this->q[3] * this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] + 
       4*C5vNC*this->q[0]*this->q[1]*this->q[3] * this->q[3]*this->kg[1]*this->kg[2] * this->kg[2] - 
       std::complex<double>(0,2)*C5a*this->mn2*this->q[1]*this->q[3]*pow(this->kg[2],3) + 
       (std::complex<double>(0,2)*C5a*this->mn2*this->q[1]*
           (this->mRes*(3*this->mRes + this->mn) + 2*p0*this->q[0] - this->q[0] * this->q[0] + 
             this->q[3] * this->q[3] + this->q[1]*this->kg[1])*this->kg[2] + 
          C5vNC*this->q[3]*(8*pow(p0,3)*this->q[0] * this->q[0] - 8*pow(this->q[0],5) + 
             2*this->mRes*this->mn*this->q[0]*(2*p0*this->q[0] + this->q[1]*(-this->q[1] + this->kg[1])) + 
             4*pow(p0,2)*this->q[0]*(-6*this->q[0] * this->q[0] + this->q[1]*(this->q[1] + 3*this->kg[1])) + 
             pow(this->q[0],3)*(5*this->q[1] * this->q[1] + 7*this->q[1]*this->kg[1] + 
                4*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])) - 
             2*this->mRes_2*(p0*(4*this->q[0] * this->q[0] - 2*this->q[1]*this->kg[1]) + 
                this->q[0]*(-4*this->q[0] * this->q[0] + 3*this->q[1]*this->kg[1] + this->kg[1] * this->kg[1] + 
                   this->kg[2] * this->kg[2])) - 
             this->q[0]*this->q[1]*(pow(this->q[1],3) - this->q[1] * this->q[1]*this->kg[1] - this->q[3] * this->q[3]*this->kg[1] + 
                this->q[1]*(this->q[3] * this->q[3] + 8*this->kg[1] * this->kg[1] + 4*this->kg[2] * this->kg[2])) + 
             2*p0*(11*pow(this->q[0],4) + 
                2*this->q[1] * this->q[1]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + 
                this->q[0] * this->q[0]*(-3*this->q[1] * this->q[1] + this->q[3] * this->q[3] - 10*this->q[1]*this->kg[1] - 
                   2*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])))))*this->kg[3] + 
       2*C3vNC*this->mn*(this->mRes*this->q[0]*(this->q[1] - this->kg[1])*
           (8*p0*this->q[0]*(this->q[1] - this->kg[1]) + 4*pow(p0,2)*(-this->q[1] + this->kg[1]) + 
             (this->q[1] * this->q[1] + this->q[3] * this->q[3])*(this->q[1] + this->kg[1]) + 
             this->q[0] * this->q[0]*(-5*this->q[1] + 3*this->kg[1])) - 
          this->mRes*(4*pow(p0,2)*this->q[0] + 
             4*p0*(-2*this->q[0] * this->q[0] + this->q[1] * this->q[1]) + 
             this->q[0]*(3*this->q[0] * this->q[0] - 3*this->q[1] * this->q[1] + this->q[3] * this->q[3]))*
           this->kg[2] * this->kg[2] + 2*this->mRes_2*this->mn*
           (4*p0*this->q[1]*this->kg[1] + this->q[0]*(pow(this->q[1] - this->kg[1],2) + this->kg[2] * this->kg[2])) + 
          2*pow(this->mRes,3)*(4*p0*(this->q[0] * this->q[0] - this->q[1]*this->kg[1]) + 
             this->q[0]*(pow(this->q[1] - this->kg[1],2) + this->kg[2] * this->kg[2])) + 
          2*this->mn*(p0 - this->q[0])*(4*pow(p0,2)*this->q[0] * this->q[0] + 3*pow(this->q[0],4) + 
             2*p0*this->q[0]*(-4*this->q[0] * this->q[0] + pow(this->q[1] + this->kg[1],2) + 
                this->kg[2] * this->kg[2]) - 
             this->q[0] * this->q[0]*(this->q[3] * this->q[3] + (this->q[1] + this->kg[1])*(2*this->q[1] + this->kg[1]) + 
                this->kg[2] * this->kg[2] + 2*this->q[3]*this->kg[3]) + 
             this->q[1]*(this->q[1] * this->q[1]*this->kg[1] + this->q[1]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + 
                this->q[3]*this->kg[1]*(this->q[3] + 2*this->kg[3])))) + 
       C4vNC*this->mRes*(4*this->mRes_2*this->mn*this->q[0]*
           (2*(p0 - this->q[0])*this->q[0] + this->q[1]*(this->q[1] + this->kg[1])) + 
          2*this->mn*(p0 - this->q[0])*(4*pow(p0,2)*this->q[0] * this->q[0] + 3*pow(this->q[0],4) + 
             2*p0*this->q[0]*(-4*this->q[0] * this->q[0] + pow(this->q[1] + this->kg[1],2) + 
                this->kg[2] * this->kg[2]) - 
             this->q[0] * this->q[0]*(this->q[3] * this->q[3] + (this->q[1] + this->kg[1])*(2*this->q[1] + this->kg[1]) + 
                this->kg[2] * this->kg[2] + 2*this->q[3]*this->kg[3]) + 
             this->q[1]*(this->q[1] * this->q[1]*this->kg[1] + this->q[1]*(2*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + 
                this->q[3]*this->kg[1]*(this->q[3] + 2*this->kg[3]))) + 
          this->mRes*(16*pow(p0,3)*this->q[0] * this->q[0] + 
             4*pow(p0,2)*this->q[0]*
              (-8*this->q[0] * this->q[0] + 2*this->q[1] * this->q[1] + 5*this->q[1]*this->kg[1] + this->kg[1] * this->kg[1] + 
                this->kg[2] * this->kg[2]) + 
             this->q[0]*(pow(this->q[1],4) - pow(this->q[1],3)*this->kg[1] - 
                this->q[0] * this->q[0]*(5*this->q[1] * this->q[1] - 7*this->q[1]*this->kg[1] + 
                   2*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])) - 
                2*this->q[3]*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2])*(this->q[3] + this->kg[3]) + 
                this->q[1] * this->q[1]*(this->q[3] * this->q[3] + 2*this->kg[1] * this->kg[1] + 
                   4*this->kg[2] * this->kg[2] - 2*this->q[3]*this->kg[3]) + 
                this->q[1]*this->kg[1]*(-2*(this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + this->q[3]*(this->q[3] + 4*this->kg[3])) 
) + 2*p0*(6*pow(this->q[0],4) + this->q[0] * this->q[0]*
                 (-3*this->q[1] * this->q[1] - 11*this->q[1]*this->kg[1] + this->kg[1] * this->kg[1] + 
                   this->kg[2] * this->kg[2] - 2*this->q[3]*(this->q[3] + 2*this->kg[3])) + 
                this->q[1]*(3*this->q[1] * this->q[1]*this->kg[1] + 
                   this->q[1]*(4*this->kg[1] * this->kg[1] + this->kg[2] * this->kg[2]) + 
                   this->q[3]*this->kg[1]*(3*this->q[3] + 4*this->kg[3])))))))/
  (24.*this->mRes_2*pow(this->mn,5));
}



