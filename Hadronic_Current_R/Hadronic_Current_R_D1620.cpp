//
// Created by eduardo on 5/07/19.
//

#include <Parameters_GeV.h>
#include <Form_Factors_D1620.h>
#include "Hadronic_Current_R_D1620.h"

Hadronic_Current_R_D1620::Hadronic_Current_R_D1620(Nuclear_FF *nuclearFf) : Hadronic_Current_tr_J12(nuclearFf) {
    this->mRes = Param::mD1620;
    this->mRes_2 = this->mRes * this->mRes;
    this->ff_Res = new Form_Factors_D1620();
    this->gamma = Param::Gamma_D1620;

    this->parity = "negative";
    this->isospin_by_2 = 3;

    this->set_EM_FF();
}

Hadronic_Current_R_D1620::~Hadronic_Current_R_D1620() {
    delete this->ff_Res;
}
