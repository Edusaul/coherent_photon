//
// Created by edusaul on 5/04/19.
//

#ifndef COHERENT_PHOTON_DELTA_IN_MEDIUM_APPROX_H
#define COHERENT_PHOTON_DELTA_IN_MEDIUM_APPROX_H


#include "Delta_in_medium.h"

class Delta_in_medium_approx : public Delta_in_medium{
protected:
    double real_sigma; // real part of the Delta selfenergy
    double fr; // average density in units of rho0
    double gamspr0; // in meadium spreading at rho0
    double gamspr;


public:
    Delta_in_medium_approx(const std::string &nucleus, const std::string &nucleon);

    std::complex<double> Delta_propagator_dir(double) override;

};


#endif //COHERENT_PHOTON_DELTA_IN_MEDIUM_APPROX_H
