//
// Created by edusaul on 22/04/19.
//

#include <Nucleus_FF_DeVries.h>
#include <Nuclear_FF_HO.h>
#include <Hadronic_Current_R_N1440.h>
#include <dsdEg_dphig_dthg_dth.h>
#include <dsdEg_dphig_dthg.h>
#include <dsdEg_dphig.h>
#include <dsdEg.h>
#include "Admin_of_Clases_N1440.h"

Admin_of_Clases_N1440::Admin_of_Clases_N1440(Arguments_base *parameters) : Admin_of_Clases_base(parameters) {
    this->nuclearFF = new Nucleus_FF_DeVries(this->parameters->getNucleus());
//    this->nuclearFF = new Nuclear_FF_HO;
    this->current_R = new Hadronic_Current_R_N1440(this->nuclearFF);
    this->create_dsdEg_clases();
}

Admin_of_Clases_N1440::~Admin_of_Clases_N1440() {
    this->delete_dsdEg_clases();
    delete this->nuclearFF;
}
