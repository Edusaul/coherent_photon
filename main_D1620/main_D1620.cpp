//
// Created by eduardo on 4/07/19.
//

#include <Measure_time.h>
#include <Arguments_base.h>
#include "Admin_of_Clases_D1620.h"

int main(int argc, char** argv) {
    Measure_time time;

    std::string distribution_type = "dsdEg_D1620";

    Arguments_base parameters(argc, argv, distribution_type);

    Admin_of_Clases_D1620 admin(&parameters);

    admin.write_dsdEgamma();

    time.getTime();
    return 0;
}