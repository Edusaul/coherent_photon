//
// Created by eduardo on 12/07/19.
//

#include <Nucleus_FF_DeVries.h>
#include <dsdEg_dphig_dthg_dth.h>
#include <dsdThG_dEg_dphig.h>
#include <dsdThG_dEg.h>
#include <dsdThGamma.h>
#include <Hadronic_Current_R_D1620.h>
#include "Admin_of_Clases_D1620_dThg.h"

Admin_of_Clases_D1620_dThg::Admin_of_Clases_D1620_dThg(Arguments_base *parameters) : Admin_of_Clases_base(parameters) {
    this->nuclearFF = new Nucleus_FF_DeVries(this->parameters->getNucleus());
//    this->nuclearFF = new Nuclear_FF_HO;

    this->current_R = new Hadronic_Current_R_D1620(this->nuclearFF);
    this->create_dsdThg_clases();
}

Admin_of_Clases_D1620_dThg::~Admin_of_Clases_D1620_dThg() {
    this->delete_dsdThg_clases();
    delete this->nuclearFF;
}
