//
// Created by edusaul on 28/03/19.
//

#include <Parameters_GeV.h>
#include <IntegralGauss20.h>
#include "dsdThGamma_dEg.h"

dsdThGamma_dEg::dsdThGamma_dEg(Integrable_w_change_param<double> *dsdthgDEgDth) : dsdthg_dEg_dth(dsdthgDEgDth) {
    this->th_min = 0.0;
    this->th_max = Param::pi;
    this->npts = 1;

}

dsdThGamma_dEg::dsdThGamma_dEg(Integrable_w_change_param<double> *dsdthgDEgDth, int npts) : dsdthg_dEg_dth(
        dsdthgDEgDth), npts(npts) {
    this->th_min = 0.0;
    this->th_max = Param::pi;
}

double dsdThGamma_dEg::integrand(double k0g) {
    double kp0 = this->k0 - k0g;
    double factors = k0g * kp0 / this->k0;

    std::vector<double> param = {this->k0, this->thg, k0g};
    this->dsdthg_dEg_dth->change_other_parameters(param);

    auto ds = IntegralGauss20::integrate<double>(this->dsdthg_dEg_dth, this->th_min, this->th_max, this->npts);

    return ds * factors;
}

void dsdThGamma_dEg::change_other_parameters(std::vector<double>k0_thg) {
    this->k0 = k0_thg[0];
    this->thg = k0_thg[1];
}




