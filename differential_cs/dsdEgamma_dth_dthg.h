//
// Created by eduardo on 8/03/19.
//

#ifndef COHERENT_PHOTON_DSDEGAMMA_DTH_DTHG_H
#define COHERENT_PHOTON_DSDEGAMMA_DTH_DTHG_H


#include <Integrable_w_change_param.h>

class dsdEgamma_dth_dthg : public Integrable_w_change_param<double> {
protected:
    Integrable_w_change_param<double> *dsdEg_dth_dthg_dphig;
    double phig_min;
    double phig_max;
    int npts;

    double k0;
    double k0g;
    double th;

public:
    explicit dsdEgamma_dth_dthg(Integrable_w_change_param<double> *dsdEg_dth_dthg_dphig);

    dsdEgamma_dth_dthg(Integrable_w_change_param<double> *dsdEg_dth_dthg_dphig, int npts);

    double integrand(double) override;

    void change_other_parameters(std::vector<double>) override;

};


#endif //COHERENT_PHOTON_DSDEGAMMA_DTH_DTHG_H
