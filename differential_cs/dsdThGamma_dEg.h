//
// Created by edusaul on 28/03/19.
//

#ifndef COHERENT_PHOTON_DSDTHGAMMA_DEG_H
#define COHERENT_PHOTON_DSDTHGAMMA_DEG_H

#include "Integrable_w_change_param.h"

class dsdThGamma_dEg : public Integrable_w_change_param<double> {
protected:
    Integrable_w_change_param<double> *dsdthg_dEg_dth;
    double th_min;
    double th_max;
    int npts;

    double k0;
    double thg;

public:
    explicit dsdThGamma_dEg(Integrable_w_change_param<double> *dsdthgDEgDth);

    dsdThGamma_dEg(Integrable_w_change_param<double> *dsdthgDEgDth, int npts);

    double integrand(double) override;

    void change_other_parameters(std::vector<double>) override;
};


#endif //COHERENT_PHOTON_DSDTHGAMMA_DEG_H
