//
// Created by edusaul on 28/03/19.
//

#include <Parameters_GeV.h>
#include <IntegralGauss20.h>
#include "dsdThGamma.h"

dsdThGamma::dsdThGamma(Integrable_w_change_param<double> *dsdthgDEg) : dsdthg_dEg(dsdthgDEg) {
    double e2 = 4.0 * Param::pi * Param::alpha;
    this->constant_factors = Param::Gf2 * e2 / (2.0 * 8.0 * pow(2.0*Param::pi,4) );
    this->from_GeV2_to_cm2 = Param::hccm2;
    this->Eg_min = 0.0;
    this->npts = 1;
    this->Eg_integral_limit = 0;
}

dsdThGamma::dsdThGamma(Integrable_w_change_param<double> *dsdthgDEg, int npts) : dsdthg_dEg(dsdthgDEg), npts(npts) {
    double e2 = 4.0 * Param::pi * Param::alpha;
    this->constant_factors = Param::Gf2 * e2 / (2.0 * 8.0 * pow(2.0*Param::pi,4) );
    this->from_GeV2_to_cm2 = Param::hccm2;
    this->Eg_min = 0.0;
    this->Eg_integral_limit = 0;
}

dsdThGamma::dsdThGamma(Integrable_w_change_param<double> *dsdthgDEg, double egIntegralLimit) : dsdthg_dEg(dsdthgDEg),
                                                                                               Eg_integral_limit(
                                                                                                       egIntegralLimit) {
    double e2 = 4.0 * Param::pi * Param::alpha;
    this->constant_factors = Param::Gf2 * e2 / (2.0 * 8.0 * pow(2.0 * Param::pi, 4));
    this->from_GeV2_to_cm2 = Param::hccm2;
    this->Eg_min = 0.0;
    this->npts = 1;
}

dsdThGamma::dsdThGamma(Integrable_w_change_param<double> *dsdthgDEg, double egIntegralLimit, int npts) : dsdthg_dEg(
        dsdthgDEg), Eg_integral_limit(egIntegralLimit), npts(npts) {
    double e2 = 4.0 * Param::pi * Param::alpha;
    this->constant_factors = Param::Gf2 * e2 / (2.0 * 8.0 * pow(2.0 * Param::pi, 4));
    this->from_GeV2_to_cm2 = Param::hccm2;
    this->Eg_min = 0.0;
}

double dsdThGamma::integrand(double thg) {

    this->Eg_max = this->k0;
    if (Eg_integral_limit !=0 && this->Eg_max > 2.5) this->Eg_max=2.5;
    std::vector<double> param = {this->k0, thg};
    this->dsdthg_dEg->change_other_parameters(param);

    auto ds = IntegralGauss20::integrate<double>(this->dsdthg_dEg, this->Eg_min, this->Eg_max, this->npts);

    return ds * this->constant_factors * this->from_GeV2_to_cm2 * sin(thg);
}

void dsdThGamma::change_other_parameters(std::vector<double> k0) {
    this->k0 = k0[0];
}




