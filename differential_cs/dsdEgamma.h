//
// Created by edusaul on 7/03/19.
//

#ifndef COHERENT_PHOTON_DSDEGAMMA_H
#define COHERENT_PHOTON_DSDEGAMMA_H


#include "Integrable_w_change_param.h"

class dsdEgamma : public Integrable_w_change_param<double> {
protected:
    Integrable_w_change_param<double> *dsdEg_dth;
    double constant_factors;
    double from_GeV2_to_cm2;
    double k0;
    double th_min;
    double th_max;
    int npts;

public:
    explicit dsdEgamma(Integrable_w_change_param<double> *dsdEg_dth);

    dsdEgamma(Integrable_w_change_param<double> *dsdEg_dth, int npts);

    double integrand(double) override;

    void change_other_parameters(std::vector<double>) override;

};


#endif //COHERENT_PHOTON_DSDEGAMMA_H
