//
// Created by edusaul on 28/03/19.
//

#include <Parameters_GeV.h>
#include <IntegralGauss20.h>
#include "dsdThGamma_dEg_dth.h"

dsdThGamma_dEg_dth::dsdThGamma_dEg_dth(Integrable_w_change_param<double> *dsdEgDthDthgDphig) : dsdEg_dth_dthg_dphig(
        dsdEgDthDthgDphig) {
    this->phig_min = 0.0;
    this->phig_max = 2.0 * Param::pi;
    this->npts = 1;
}

dsdThGamma_dEg_dth::dsdThGamma_dEg_dth(Integrable_w_change_param<double> *dsdEgDthDthgDphig, int npts)
        : dsdEg_dth_dthg_dphig(dsdEgDthDthgDphig), npts(npts) {
    this->phig_min = 0.0;
    this->phig_max = 2.0 * Param::pi;
}

double dsdThGamma_dEg_dth::integrand(double th) {
    std::vector<double> param = {this->k0, this->k0g, th, this->thg};
    this->dsdEg_dth_dthg_dphig->change_other_parameters(param);

    auto ds = IntegralGauss20::integrate<double>(this->dsdEg_dth_dthg_dphig, this->phig_min, this->phig_max, this->npts);

    return ds * sin(th);
}

void dsdThGamma_dEg_dth::change_other_parameters(std::vector<double> k0_thg_k0g) {
    this->k0 =  k0_thg_k0g[0];
    this->thg = k0_thg_k0g[1];
    this->k0g = k0_thg_k0g[2];
}






