//
// Created by edusaul on 7/03/19.
//

#ifndef COHERENT_PHOTON_DSDEGAMMA_DTH_H
#define COHERENT_PHOTON_DSDEGAMMA_DTH_H


#include "Integrable_w_change_param.h"

class dsdEgamma_dth : public Integrable_w_change_param<double> {
protected:
    Integrable_w_change_param<double> *dsdEg_dth_dthg;
    double thg_min;
    double thg_max;
    int npts;

    double k0;
    double k0g;

public:

    explicit dsdEgamma_dth(Integrable_w_change_param<double> *dsdEg_dth_dthg);

    dsdEgamma_dth(Integrable_w_change_param<double> *dsdEg_dth_dthg, int npts);

    double integrand(double) override;

    void change_other_parameters(std::vector<double>) override;

};


#endif //COHERENT_PHOTON_DSDEGAMMA_DTH_H
