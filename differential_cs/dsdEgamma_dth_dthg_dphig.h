//
// Created by eduardo on 8/03/19.
//

#ifndef COHERENT_PHOTON_DSDEGAMMA_DTH_DTHG_DPHIG_H
#define COHERENT_PHOTON_DSDEGAMMA_DTH_DTHG_DPHIG_H


#include <Integrable_w_change_param.h>
#include <complex>
#include "Hadronic_Current_R.h"

class dsdEgamma_dth_dthg_dphig : public Integrable_w_change_param<double> {
protected:
    std::string mode;
    std::vector<double> k;
    std::vector<double> kp;
    std::vector<double> q;
    std::vector<double> kg;
    double kappa_m;

    std::complex<double> c_i;

    double k0g;
    double thg;

    Hadronic_Current_R *current_R;


public:
    dsdEgamma_dth_dthg_dphig(const std::string &mode, Hadronic_Current_R *current_R);

    dsdEgamma_dth_dthg_dphig(const std::string &mode, Hadronic_Current_R *currentR, double kappa_m);

    double integrand(double) override;

    void change_other_parameters(std::vector<double>) override;

    double LH_contraction_neutrino();

    double LH_contraction_antineutrino();

    std::complex<double> H(int, int, int, int);
};


#endif //COHERENT_PHOTON_DSDEGAMMA_DTH_DTHG_DPHIG_H
