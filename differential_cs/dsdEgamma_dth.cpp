//
// Created by edusaul on 7/03/19.
//

#include <Parameters_GeV.h>
#include <IntegralGauss20.h>
#include "dsdEgamma_dth.h"


dsdEgamma_dth::dsdEgamma_dth(Integrable_w_change_param<double> *dsdEg_dth_dthg) : dsdEg_dth_dthg(dsdEg_dth_dthg) {
    this->thg_min = 0.0;
    this->thg_max = Param::pi;
    this->npts = 1;
}

dsdEgamma_dth::dsdEgamma_dth(Integrable_w_change_param<double> *dsdEg_dth_dthg, int npts) : dsdEg_dth_dthg(
        dsdEg_dth_dthg), npts(npts) {
    this->thg_min = 0.0;
    this->thg_max = Param::pi;
}

double dsdEgamma_dth::integrand(double th) {

    std::vector<double> param = {this->k0, this->k0g, th};
    this->dsdEg_dth_dthg->change_other_parameters(param);

    auto ds = IntegralGauss20::integrate<double>(this->dsdEg_dth_dthg, this->thg_min, this->thg_max, this->npts);
    return ds * sin(th);
}

void dsdEgamma_dth::change_other_parameters(std::vector<double>k0_k0g) {
    this->k0 = k0_k0g[0];
    this->k0g = k0_k0g[1];
}



