//
// Created by edusaul on 7/03/19.
//

#include <Parameters_GeV.h>
#include <IntegralGauss20.h>
#include "dsdEgamma.h"

dsdEgamma::dsdEgamma(Integrable_w_change_param<double> *dsdEg_dth) : dsdEg_dth(dsdEg_dth) {
    double e2 = 4.0 * Param::pi * Param::alpha;
    this->constant_factors = Param::Gf2 * e2 / (2.0 * 8.0 * pow(2.0*Param::pi,4) );
    this->from_GeV2_to_cm2 = Param::hccm2;
    this->th_min = 0.0;
    this->th_max = Param::pi;
    this->npts = 1;
}

dsdEgamma::dsdEgamma(Integrable_w_change_param<double> *dsdEg_dth, int npts) : dsdEg_dth(dsdEg_dth), npts(npts) {
    double e2 = 4.0 * Param::pi * Param::alpha;
    this->constant_factors = Param::Gf2 * e2 / (2.0 * 8.0 * pow(2.0*Param::pi,4) );
    this->from_GeV2_to_cm2 = Param::hccm2;
    this->th_min = 0.0;
    this->th_max = Param::pi;
}



double dsdEgamma::integrand(double k0g) {
    double kp0 = this->k0 - k0g;
    double factors = k0g * kp0 / this->k0;

    std::vector<double> param = {this->k0, k0g};
    this->dsdEg_dth->change_other_parameters(param);

    auto ds = IntegralGauss20::integrate<double>(this->dsdEg_dth, this->th_min, this->th_max, this->npts);

    return ds * factors * this->constant_factors * this->from_GeV2_to_cm2;
}

void dsdEgamma::change_other_parameters(std::vector<double> k0) {
    this->k0 = k0[0];
}

