//
// Created by edusaul on 28/03/19.
//

#ifndef COHERENT_PHOTON_DSDTHGAMMA_H
#define COHERENT_PHOTON_DSDTHGAMMA_H


#include <Integrable_w_change_param.h>

class dsdThGamma : public Integrable_w_change_param<double> {
protected:
    Integrable_w_change_param<double> *dsdthg_dEg;
    double constant_factors;
    double from_GeV2_to_cm2;
    double k0;
    double Eg_min;
    double Eg_max;
    double Eg_integral_limit;
    int npts;

public:
    explicit dsdThGamma(Integrable_w_change_param<double> *dsdthgDEg);

    dsdThGamma(Integrable_w_change_param<double> *dsdthgDEg, int npts);

    dsdThGamma(Integrable_w_change_param<double> *dsdthgDEg, double egIntegralLimit);

    dsdThGamma(Integrable_w_change_param<double> *dsdthgDEg, double egIntegralLimit, int npts);

    double integrand(double) override;

    void change_other_parameters(std::vector<double>) override;
};


#endif //COHERENT_PHOTON_DSDTHGAMMA_H
