//
// Created by eduardo on 8/03/19.
//

#include <Parameters_GeV.h>
#include <IntegralGauss20.h>
#include "dsdEgamma_dth_dthg.h"

dsdEgamma_dth_dthg::dsdEgamma_dth_dthg(Integrable_w_change_param<double> *dsdEg_dth_dthg_dphig) : dsdEg_dth_dthg_dphig(
        dsdEg_dth_dthg_dphig) {
    this->phig_min = 0.0;
    this->phig_max = 2.0 * Param::pi;
    this->npts = 1;
}

dsdEgamma_dth_dthg::dsdEgamma_dth_dthg(Integrable_w_change_param<double> *dsdEg_dth_dthg_dphig, int npts)
        : dsdEg_dth_dthg_dphig(dsdEg_dth_dthg_dphig), npts(npts) {
    this->phig_min = 0.0;
    this->phig_max = 2.0 * Param::pi;
}

double dsdEgamma_dth_dthg::integrand(double thg) {

    std::vector<double> param = {this->k0, this->k0g, this->th, thg};
    this->dsdEg_dth_dthg_dphig->change_other_parameters(param);

    auto ds = IntegralGauss20::integrate<double>(this->dsdEg_dth_dthg_dphig, this->phig_min, this->phig_max, this->npts);
    return ds * sin(thg);
}

void dsdEgamma_dth_dthg::change_other_parameters(std::vector<double>k0_k0g_th) {
    this->k0 = k0_k0g_th[0];
    this->k0g = k0_k0g_th[1];
    this->th = k0_k0g_th[2];
}
