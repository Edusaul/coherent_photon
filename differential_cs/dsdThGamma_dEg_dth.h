//
// Created by edusaul on 28/03/19.
//

#ifndef COHERENT_PHOTON_DSDTHGAMMA_DEG_DTH_H
#define COHERENT_PHOTON_DSDTHGAMMA_DEG_DTH_H

#include <Integrable_w_change_param.h>

class dsdThGamma_dEg_dth : public Integrable_w_change_param<double> {
protected:
    Integrable_w_change_param<double> *dsdEg_dth_dthg_dphig;
    double phig_min;
    double phig_max;
    int npts;

    double k0;
    double k0g;
    double thg;

public:
    explicit dsdThGamma_dEg_dth(Integrable_w_change_param<double> *dsdEgDthDthgDphig);

    dsdThGamma_dEg_dth(Integrable_w_change_param<double> *dsdEgDthDthgDphig, int npts);

    double integrand(double) override;

    void change_other_parameters(std::vector<double>) override;

};


#endif //COHERENT_PHOTON_DSDTHGAMMA_DEG_DTH_H
