//
// Created by eduardo on 8/03/19.
//

#include <iostream>
#include <algorithm>
#include <Parameters_GeV.h>
#include "dsdEgamma_dth_dthg_dphig.h"

dsdEgamma_dth_dthg_dphig::dsdEgamma_dth_dthg_dphig(const std::string &mode, Hadronic_Current_R *current_R) : mode(mode),
                                                                                                             current_R(
                                                                                                                     current_R) {
    this->c_i = {0,1};
    this->kappa_m = 0.0;
}


dsdEgamma_dth_dthg_dphig::dsdEgamma_dth_dthg_dphig(const std::string &mode, Hadronic_Current_R *currentR, double kappa_m)
        : mode(mode), current_R(currentR), kappa_m(kappa_m) {
    this->c_i = {0,1};
}


double dsdEgamma_dth_dthg_dphig::integrand(double phig) {

    this->kg = {this->k0g, this->k0g * sin(this->thg) * cos(phig),
                this->k0g * sin(this->thg) * sin(phig), this->k0g * cos(this->thg)};

    std::vector<double> kappa_4vec = this->k;
    std::transform(kappa_4vec.begin( ), kappa_4vec.end( ), this->kp.begin( ), kappa_4vec.begin( ),std::minus<>( ));
    std::transform(kappa_4vec.begin( ), kappa_4vec.end( ), this->kg.begin( ), kappa_4vec.begin( ),std::minus<>( ));
    double kappa_vec_mod = kappa_4vec[1]*kappa_4vec[1] + kappa_4vec[2]*kappa_4vec[2] + kappa_4vec[3]*kappa_4vec[3];
    if(kappa_vec_mod > this->kappa_m && this->kappa_m != 0.0){
//        std::cout<<kappa_vec_mod<<"  "<<this->kappa_m<<std::endl;
        return 0.0;
    } else {

        std::vector<double> p = this->kg;
        std::transform(p.begin(), p.end(), this->q.begin(), p.begin(), std::minus<>());
        std::transform(p.begin(), p.end(), p.begin(), [](double i) { return i / 2.0; });
        double pv2 = p[1] * p[1] + p[2] * p[2] + p[3] * p[3];
        p[0] = sqrt(Param::mp2 + pv2);

        this->current_R->setP(p);
        this->current_R->setKg(this->kg);

        double lh;
        if (this->mode == "nu") lh = this->LH_contraction_neutrino();
        else lh = this->LH_contraction_antineutrino();
        return lh;
    }
}

void dsdEgamma_dth_dthg_dphig::change_other_parameters(std::vector<double> k0_k0g_th_thg) {
    double k0 = k0_k0g_th_thg[0];
    this->k0g = k0_k0g_th_thg[1];
    double th = k0_k0g_th_thg[2];
    this->thg = k0_k0g_th_thg[3];

    //q0 = k0g approx
    double kp0 = k0 - this->k0g;

    this->k = {k0, 0., 0., k0};
    this->kp = {kp0, kp0 * sin(th), 0., kp0 * cos(th)};

    this->q = this->k;
    std::transform(q.begin( ), q.end( ), kp.begin( ), q.begin( ),std::minus<>( ));

    this->current_R->setQ(this->q);

}

double dsdEgamma_dth_dthg_dphig::LH_contraction_neutrino() {
    auto lh = -8*k[0]*((2*k[0] - this->q[0] - this->q[3])*H(0,0,0,0) + this->q[1]*H(0,0,0,1) - this->c_i*this->q[1]*H(0,0,0,2) +
                     (-2*k[0] + this->q[0] + this->q[3])*H(0,0,0,3) + this->q[1]*H(0,1,0,0) + (-this->q[0] + this->q[3])*H(0,1,0,1) +
                     this->c_i*(this->q[0] - this->q[3])*H(0,1,0,2) - this->q[1]*H(0,1,0,3) + this->c_i*this->q[1]*H(0,2,0,0) -
                     this->c_i*(this->q[0] - this->q[3])*H(0,2,0,1) + (-this->q[0] + this->q[3])*H(0,2,0,2) - this->c_i*this->q[1]*H(0,2,0,3) +
                     (-2*k[0] + this->q[0] + this->q[3])*H(0,3,0,0) - this->q[1]*H(0,3,0,1) + this->c_i*this->q[1]*H(0,3,0,2) +
                     (2*k[0] - this->q[0] - this->q[3])*H(0,3,0,3) + (-2*k[0] + this->q[0] + this->q[3])*H(1,0,1,0) - this->q[1]*H(1,0,1,1) +
                     this->c_i*this->q[1]*H(1,0,1,2) + (2*k[0] - this->q[0] - this->q[3])*H(1,0,1,3) - this->q[1]*H(1,1,1,0) +
                     (this->q[0] - this->q[3])*H(1,1,1,1) - this->c_i*(this->q[0] - this->q[3])*H(1,1,1,2) + this->q[1]*H(1,1,1,3) -
                     this->c_i*this->q[1]*H(1,2,1,0) + this->c_i*(this->q[0] - this->q[3])*H(1,2,1,1) + (this->q[0] - this->q[3])*H(1,2,1,2) +
                     this->c_i*this->q[1]*H(1,2,1,3) + (2*k[0] - this->q[0] - this->q[3])*H(1,3,1,0) + this->q[1]*H(1,3,1,1) -
                     this->c_i*this->q[1]*H(1,3,1,2) + (-2*k[0] + this->q[0] + this->q[3])*H(1,3,1,3) + (-2*k[0] + this->q[0] + this->q[3])*H(2,0,2,0) -
                     this->q[1]*H(2,0,2,1) + this->c_i*this->q[1]*H(2,0,2,2) + (2*k[0] - this->q[0] - this->q[3])*H(2,0,2,3) - this->q[1]*H(2,1,2,0) +
                     (this->q[0] - this->q[3])*H(2,1,2,1) - this->c_i*(this->q[0] - this->q[3])*H(2,1,2,2) + this->q[1]*H(2,1,2,3) -
                     this->c_i*this->q[1]*H(2,2,2,0) + this->c_i*(this->q[0] - this->q[3])*H(2,2,2,1) + (this->q[0] - this->q[3])*H(2,2,2,2) +
                     this->c_i*this->q[1]*H(2,2,2,3) + (2*k[0] - this->q[0] - this->q[3])*H(2,3,2,0) + this->q[1]*H(2,3,2,1) -
                     this->c_i*this->q[1]*H(2,3,2,2) + (-2*k[0] + this->q[0] + this->q[3])*H(2,3,2,3) + (-2*k[0] + this->q[0] + this->q[3])*H(3,0,3,0) -
                     this->q[1]*H(3,0,3,1) + this->c_i*this->q[1]*H(3,0,3,2) + (2*k[0] - this->q[0] - this->q[3])*H(3,0,3,3) - this->q[1]*H(3,1,3,0) +
                     (this->q[0] - this->q[3])*H(3,1,3,1) - this->c_i*(this->q[0] - this->q[3])*H(3,1,3,2) + this->q[1]*H(3,1,3,3) -
                     this->c_i*this->q[1]*H(3,2,3,0) + this->c_i*(this->q[0] - this->q[3])*H(3,2,3,1) + (this->q[0] - this->q[3])*H(3,2,3,2) +
                     this->c_i*this->q[1]*H(3,2,3,3) + (2*k[0] - this->q[0] - this->q[3])*H(3,3,3,0) + this->q[1]*H(3,3,3,1) -
                     this->c_i*this->q[1]*H(3,3,3,2) + (-2*k[0] + this->q[0] + this->q[3])*H(3,3,3,3));

//    if(lh.real()<0.0) std::cout<<"WARNING: negative lh "<<std::endl;

    return lh.real();
}

double dsdEgamma_dth_dthg_dphig::LH_contraction_antineutrino() {
    auto lh = -8*this->k[0]*((2*this->k[0] - this->q[0] - this->q[3])*H(0,0,0,0) + this->q[1]*H(0,0,0,1) + this->c_i*this->q[1]*H(0,0,0,2) +
                             (-2*this->k[0] + this->q[0] + this->q[3])*H(0,0,0,3) + this->q[1]*H(0,1,0,0) + (-this->q[0] + this->q[3])*H(0,1,0,1) -
                             this->c_i*(this->q[0] - this->q[3])*H(0,1,0,2) - this->q[1]*H(0,1,0,3) - this->c_i*this->q[1]*H(0,2,0,0) +
                             this->c_i*(this->q[0] - this->q[3])*H(0,2,0,1) + (-this->q[0] + this->q[3])*H(0,2,0,2) + this->c_i*this->q[1]*H(0,2,0,3) +
                             (-2*this->k[0] + this->q[0] + this->q[3])*H(0,3,0,0) - this->q[1]*H(0,3,0,1) - this->c_i*this->q[1]*H(0,3,0,2) +
                             (2*this->k[0] - this->q[0] - this->q[3])*H(0,3,0,3) + (-2*this->k[0] + this->q[0] + this->q[3])*H(1,0,1,0) - this->q[1]*H(1,0,1,1) -
                             this->c_i*this->q[1]*H(1,0,1,2) + (2*this->k[0] - this->q[0] - this->q[3])*H(1,0,1,3) - this->q[1]*H(1,1,1,0) +
                             (this->q[0] - this->q[3])*H(1,1,1,1) + this->c_i*(this->q[0] - this->q[3])*H(1,1,1,2) + this->q[1]*H(1,1,1,3) +
                             this->c_i*this->q[1]*H(1,2,1,0) - this->c_i*(this->q[0] - this->q[3])*H(1,2,1,1) + (this->q[0] - this->q[3])*H(1,2,1,2) -
                             this->c_i*this->q[1]*H(1,2,1,3) + (2*this->k[0] - this->q[0] - this->q[3])*H(1,3,1,0) + this->q[1]*H(1,3,1,1) +
                             this->c_i*this->q[1]*H(1,3,1,2) + (-2*this->k[0] + this->q[0] + this->q[3])*H(1,3,1,3) + (-2*this->k[0] + this->q[0] + this->q[3])*H(2,0,2,0) -
                             this->q[1]*H(2,0,2,1) - this->c_i*this->q[1]*H(2,0,2,2) + (2*this->k[0] - this->q[0] - this->q[3])*H(2,0,2,3) - this->q[1]*H(2,1,2,0) +
                             (this->q[0] - this->q[3])*H(2,1,2,1) + this->c_i*(this->q[0] - this->q[3])*H(2,1,2,2) + this->q[1]*H(2,1,2,3) +
                             this->c_i*this->q[1]*H(2,2,2,0) - this->c_i*(this->q[0] - this->q[3])*H(2,2,2,1) + (this->q[0] - this->q[3])*H(2,2,2,2) -
                             this->c_i*this->q[1]*H(2,2,2,3) + (2*this->k[0] - this->q[0] - this->q[3])*H(2,3,2,0) + this->q[1]*H(2,3,2,1) +
                             this->c_i*this->q[1]*H(2,3,2,2) + (-2*this->k[0] + this->q[0] + this->q[3])*H(2,3,2,3) + (-2*this->k[0] + this->q[0] + this->q[3])*H(3,0,3,0) -
                             this->q[1]*H(3,0,3,1) - this->c_i*this->q[1]*H(3,0,3,2) + (2*this->k[0] - this->q[0] - this->q[3])*H(3,0,3,3) - this->q[1]*H(3,1,3,0) +
                             (this->q[0] - this->q[3])*H(3,1,3,1) + this->c_i*(this->q[0] - this->q[3])*H(3,1,3,2) + this->q[1]*H(3,1,3,3) +
                             this->c_i*this->q[1]*H(3,2,3,0) - this->c_i*(this->q[0] - this->q[3])*H(3,2,3,1) + (this->q[0] - this->q[3])*H(3,2,3,2) -
                             this->c_i*this->q[1]*H(3,2,3,3) + (2*this->k[0] - this->q[0] - this->q[3])*H(3,3,3,0) + this->q[1]*H(3,3,3,1) +
                             this->c_i*this->q[1]*H(3,3,3,2) + (-2*this->k[0] + this->q[0] + this->q[3])*H(3,3,3,3));

//    if(lh.real()<0.0) std::cout<<"WARNING: negative lh "<<std::endl;

    return lh.real();
}


std::complex<double> dsdEgamma_dth_dthg_dphig::H(int l, int m, int n, int o) {
    std::complex<double> r1c = std::conj(this->current_R->getR(l,m));
    std::complex<double> r1 = this->current_R->getR(n,o);
    std::complex<double> r = r1 * r1c;
    return r;
}





