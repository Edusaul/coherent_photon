cmake_minimum_required(VERSION 3.13)
project(Coherent_Photon_N1720)

set(CMAKE_CXX_STANDARD 14)


add_executable(Coherent_Photon_N1720 main_N1720.cpp Admin_of_Clases_N1720.cpp Admin_of_Clases_N1720.h)

target_compile_options(Coherent_Photon_N1720 PUBLIC -O3)

target_link_libraries(Coherent_Photon_N1720 libtime_lib.a Coherent_base_lib
        diff_cs_dEg Hadronic_Current_N1720)



add_executable(Coherent_Photon_N1720_dThg main_N1720_dThg.cpp Admin_of_Clases_N1720_dThg.cpp Admin_of_Clases_N1720_dThg.h)

target_compile_options(Coherent_Photon_N1720_dThg PUBLIC -O3)

target_link_libraries(Coherent_Photon_N1720_dThg libtime_lib.a Coherent_base_lib
        diff_cs_dThg Hadronic_Current_N1720)