//
// Created by edusaul on 28/03/19.
//

#include <Measure_time.h>
#include "Arguments_base.h"
#include "Admin_of_Clases_nucleon_thg.h"

int main(int argc, char** argv) {
    Measure_time time;

    std::string distribution_type = "dsdthg_Nucleon";

    Arguments_base parameters(argc, argv, distribution_type);

    Admin_of_Clases_nucleon_thg admin(&parameters);

    admin.write_dsdthg();

    time.getTime();
    return 0;
}