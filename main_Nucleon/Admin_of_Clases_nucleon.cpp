//
// Created by edusaul on 22/03/19.
//

#include "Admin_of_Clases_nucleon.h"
#include "Hadronic_Current_R_Nucleon.h"
#include "Nucleus_FF_DeVries.h"
#include <dsdEg_dphig_dthg.h>
#include <dsdEg_dphig.h>
#include <dsdEg.h>
#include <dsdEg_dphig_dthg_dth.h>
#include <iostream>
#include <Parameters_GeV.h>
#include <Nuclear_FF_HO.h>


Admin_of_Clases_nucleon::Admin_of_Clases_nucleon(Arguments_base *parameters) : Admin_of_Clases_base(parameters) {
//    this->nuclearFF = new Nucleus_FF_DeVries(this->parameters->getNucleus());
    this->nuclearFF = new Nuclear_FF_HO;
    this->current_R = new Hadronic_Current_R_Nucleon(this->nuclearFF);
    this->create_dsdEg_clases();
}

Admin_of_Clases_nucleon::~Admin_of_Clases_nucleon() {
    this->delete_dsdEg_clases();
    delete this->nuclearFF;
}