//
// Created by edusaul on 22/03/19.
//

#include <dsdEgamma.h>
#include <Nucleus_FF_DeVries.h>
#include <Parameters_GeV.h>

#include <Hadronic_Current_R_Nucleon.h>
#include <Nuclear_FF_HO.h>
#include <dsdEg_dphig_dthg_dth.h>
#include <dsdThG_dEg_dphig.h>
#include <dsdThG_dEg.h>
#include <dsdThGamma.h>
#include "Admin_of_Clases_nucleon_thg.h"

Admin_of_Clases_nucleon_thg::Admin_of_Clases_nucleon_thg(Arguments_base *parameters) : Admin_of_Clases_base(parameters) {
//    this->nuclearFF = new Nucleus_FF_DeVries(this->parameters->getNucleus());
    this->nuclearFF = new Nuclear_FF_HO;
    this->current_R = new Hadronic_Current_R_Nucleon(this->nuclearFF);
    this->create_dsdThg_clases();
}

Admin_of_Clases_nucleon_thg::~Admin_of_Clases_nucleon_thg() {
    this->delete_dsdThg_clases();
    delete this->nuclearFF;
}
