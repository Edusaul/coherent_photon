//
// Created by edusaul on 22/03/19.
//

#ifndef COHERENT_PHOTON_ADMIN_OF_CLASES_NUCLEON_THG_H
#define COHERENT_PHOTON_ADMIN_OF_CLASES_NUCLEON_THG_H


#include <Admin_of_Clases_base.h>
#include "Nuclear_FF.h"

class Admin_of_Clases_nucleon_thg : public Admin_of_Clases_base {
protected:
    Nuclear_FF *nuclearFF;
public:
    explicit Admin_of_Clases_nucleon_thg(Arguments_base *parameters);

    ~Admin_of_Clases_nucleon_thg() override;

};


#endif //COHERENT_PHOTON_ADMIN_OF_CLASES_NUCLEON_THG_H
