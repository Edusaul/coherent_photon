//
// Created by edusaul on 22/03/19.
//

#ifndef COHERENT_PHOTON_ADMIN_OF_CLASES_NUCLEON_H
#define COHERENT_PHOTON_ADMIN_OF_CLASES_NUCLEON_H

#include <Nuclear_FF.h>
#include "Admin_of_Clases_base.h"


class Admin_of_Clases_nucleon : public Admin_of_Clases_base {
protected:
    Nuclear_FF *nuclearFF;
public:
    explicit Admin_of_Clases_nucleon(Arguments_base *parameters);

    ~Admin_of_Clases_nucleon() override;

};


#endif //COHERENT_PHOTON_ADMIN_OF_CLASES_NUCLEON_H
