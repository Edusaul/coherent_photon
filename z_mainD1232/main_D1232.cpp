//
// Created by eduardo on 8/07/19.
//

#include <Measure_time.h>
#include <Arguments_base.h>
#include "Admin_of_Clases_D1232.h"

int main(int argc, char** argv) {
    Measure_time time;

    std::string distribution_type = "z_dsdEg_D1232";

    Arguments_base parameters(argc, argv, distribution_type);

    Admin_of_Clases_D1232 admin(&parameters);

    admin.write_dsdEgamma();

    time.getTime();
    return 0;
}
