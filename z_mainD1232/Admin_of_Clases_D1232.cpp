//
// Created by eduardo on 8/07/19.
//

#include <Nucleus_FF_DeVries.h>
#include <z_Hadronic_Current_R_D1232.h>
#include <dsdEg_dphig_dthg_dth.h>
#include <dsdEg_dphig_dthg.h>
#include <dsdEg_dphig.h>
#include <dsdEg.h>
#include "Admin_of_Clases_D1232.h"

Admin_of_Clases_D1232::Admin_of_Clases_D1232(Arguments_base *parameters) : Admin_of_Clases_base(parameters) {
    this->nuclearFF = new Nucleus_FF_DeVries(this->parameters->getNucleus());
//    this->nuclearFF = new Nuclear_FF_HO;
    this->current_R = new z_Hadronic_Current_R_D1232(this->nuclearFF);
    this->create_dsdEg_clases();

}

Admin_of_Clases_D1232::~Admin_of_Clases_D1232() {
    this->delete_dsdEg_clases();
    delete this->nuclearFF;
}