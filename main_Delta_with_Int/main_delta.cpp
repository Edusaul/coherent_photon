#include <Measure_time.h>
#include "Arguments_base.h"
#include "Admin_of_Clases_delta_Int.h"

int main(int argc, char** argv) {
    Measure_time time;

    std::string distribution_type = "dsdEg_Delta_int";

    Arguments_base parameters(argc, argv, distribution_type);

    Admin_of_Clases_delta_Int admin(&parameters);

    admin.write_dsdEgamma();

    time.getTime();
    return 0;
}