//
// Created by edusaul on 22/03/19.
//

#ifndef COHERENT_PHOTON_ADMIN_OF_CLASES_DELTA_INT_H
#define COHERENT_PHOTON_ADMIN_OF_CLASES_DELTA_INT_H


#include <Admin_of_Clases_base.h>
#include <Delta_in_medium.h>

class Admin_of_Clases_delta_Int : public Admin_of_Clases_base {
protected:
    std::string nucleon;
    Delta_in_medium *Delta;

public:
    explicit Admin_of_Clases_delta_Int(Arguments_base *parameters);

    ~Admin_of_Clases_delta_Int() override;

};


#endif //COHERENT_PHOTON_ADMIN_OF_CLASES_DELTA_INT_H
