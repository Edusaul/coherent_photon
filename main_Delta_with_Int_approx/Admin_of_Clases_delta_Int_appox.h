//
// Created by edusaul on 22/03/19.
//

#ifndef COHERENT_PHOTON_ADMIN_OF_CLASES_DELTA_INT_APPROX_H
#define COHERENT_PHOTON_ADMIN_OF_CLASES_DELTA_INT_APPROX_H


#include <Admin_of_Clases_base.h>
#include <Delta_in_medium.h>

class Admin_of_Clases_delta_Int_approx : public Admin_of_Clases_base {
protected:
    std::string nucleon;
    Delta_in_medium *Delta;

public:
    explicit Admin_of_Clases_delta_Int_approx(Arguments_base *parameters);

    ~Admin_of_Clases_delta_Int_approx() override;

};


#endif //COHERENT_PHOTON_ADMIN_OF_CLASES_DELTA_INT_APPROX_H
