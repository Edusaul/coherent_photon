//
// Created by edusaul on 22/03/19.
//

//#include <Hadronic_Current_R_Delta.h>
#include <dsdEg_dphig_dthg_dth.h>
#include <dsdEg_dphig_dthg.h>
#include <dsdEg_dphig.h>
#include <dsdEg.h>
#include <Parameters_GeV.h>
#include <iostream>
#include <Hadronic_Current_R_Delta_with_Int.h>
#include <Delta_in_medium_approx.h>
#include "Admin_of_Clases_delta_Int_appox.h"

Admin_of_Clases_delta_Int_approx::Admin_of_Clases_delta_Int_approx(Arguments_base *parameters) : Admin_of_Clases_base(parameters) {
    this->nucleon = "p";
    this->Delta = new Delta_in_medium_approx(this->parameters->getNucleus(),this->nucleon);
    this->current_R = new Hadronic_Current_R_Delta_with_Int(this->Delta->getRmax(), this->Delta);

    this->create_dsdEg_clases();

//    this->dsdEgamma_dphig_dthg_dth = new dsdEg_dphig_dthg_dth(this->parameters->getMode(), this->current_R);
//    this->dsdk0_dphig_dthg = new dsdEg_dphig_dthg(this->dsdEgamma_dphig_dthg_dth);
//    this->dsdk0_dphig = new dsdEg_dphig(this->dsdk0_dphig_dthg);
//    this->dsdk0 = new dsdEg(this->dsdk0_dphig);
//
//    std::vector<double> k0 = {this->k0};
//    this->dsdk0->change_other_parameters(k0);


//    int N = 50;
//    double r = 0.0;
//    double rmax =this->Delta->getRmax();
//    for (int i = 0; i < N; ++i) {
//        std::cout<<r<<"  "<<this->Delta->getRho_r(r)<<std::endl;
//        r += (rmax - 0.0)/double(N-1);
//    }

//    std::vector<std::complex<double>> param(2);
//    param[0] = 0.478163;
//    param[1] = 1.67108;
//    this->Delta->change_other_parameters(param);
//    std::cout<<"int  3.1  =  "<<this->Delta->integrand(6.)<<std::endl;

}

Admin_of_Clases_delta_Int_approx::~Admin_of_Clases_delta_Int_approx() {
    this->delete_dsdEg_clases();
    delete this->Delta;
}
