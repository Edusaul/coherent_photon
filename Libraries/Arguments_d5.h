//
// Created by edusaul on 27/03/19.
//

#ifndef COHERENT_PHOTON_ARGUMENTS_D5_H
#define COHERENT_PHOTON_ARGUMENTS_D5_H

#include <string>
#include "Arguments_base.h"

class Arguments_d5 : public Arguments_base{
protected:
    double kg0;
    double theta;
    double thetag;
    double phig;

public:
    Arguments_d5(int argc, char **argv, std::string distributionType);

    double getKg0() const;

    double getTheta() const;

    double getThetag() const;

    double getPhig() const;

};


#endif //COHERENT_PHOTON_ARGUMENTS_D5_H
