//
// Created by edusaul on 7/03/19.
//

#ifndef COHERENT_PHOTON_INTEGRABLE_W_CHANGE_PARAM_H
#define COHERENT_PHOTON_INTEGRABLE_W_CHANGE_PARAM_H


#include <Integrable.h>
#include <vector>

template <typename T>
class Integrable_w_change_param : public Integrable<T>{
public:
    virtual void change_other_parameters (std::vector<T>) {}
};


#endif //COHERENT_PHOTON_INTEGRABLE_W_CHANGE_PARAM_H
