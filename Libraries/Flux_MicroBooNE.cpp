//
// Created by edusaul on 17/04/19.
//

#include <fstream>
#include <sstream>
#include <iostream>
#include <Parameters_GeV.h>
#include "Flux_MicroBooNE.h"

Flux_MicroBooNE::Flux_MicroBooNE(const std::string &path, int nbins, double sizeBin, double nPOT) : Flux(path, nbins, sizeBin, nPOT) {
    if (this->path == "default") this->path = "../../Flux_data/microboone_fluxes.dat";
    if (this->nbins == 0) this->nbins = 55;
    if (this->sizeBin == 0) this->sizeBin = 0.05; // GeV
    if (this->nPOT == 0) this->nPOT = 6.6e20;

    double dimensions = 2.325e2 * 2.5604e2 * 10.368e2; //cm^3
    double mass = 1395.4e-3 * dimensions; // g (LAr density = 1395 kg/m^3)
    double molecular_mass = 40.0; //
    this->nTargets = Param::NA * mass / molecular_mass;

    this->read_flux();
}

void Flux_MicroBooNE::read_flux() {
    std::ifstream file(this->path.c_str());//opening imput document

    std::string line;

    while(std::getline(file, line)) // read one line from ifs
    {
        std::istringstream iss(line); // access line as a stream

        double columns[7];

        iss >> columns[0] >> columns[1] >> columns[2] >> columns[3]
                >> columns[4] >> columns[5] >> columns[6] ; // no need to read further

        this->EnuBinMin.push_back(columns[0] - this->sizeBin/2.0); // bins in GeV
        this->EnuBinMax.push_back(columns[0] + this->sizeBin/2.0);
        this->N_Events.push_back(columns[5] * 1e-10); // file has the events by 10^6 POT and m^2
    }



//    if(file.is_open()){
//        double x;
//        for (int i=0; i<this->nbins; i++){
//            file>>x;
//
//            this->EnuBinMin[i] = x*1e3 - 25.0;
//            this->EnuBinMax[i] = x*1e3 + 25.0;
//
//            this->EnuBinMin[i]=double(i)*this->sizeBin;
//            this->EnuBinMax[i]=this->EnuBinMin[i]+this->sizeBin;
//            this->N_Events[i]=data_Y*0.1; // neutrinos/cm^2/POT
//            tot+=this->N_Events[i];
//        }
//        for (i=0;i<this->nbins;i++){
//            file>>data_dY;
//            this->delta_N_Events[i]=data_dY*0.1; // neutrinos/cm^2/POT;
//        }
//// 		this->total=tot;
//    }
//    file.close();
}
