//
// Created by edusaul on 26/04/19.
//

#include <cmath>
#include <Parameters_GeV.h>
#include "Flux_Minerva.h"
#include <fstream>
#include <sstream>

Flux_Minerva::Flux_Minerva(const std::string &path, int nbins, double sizeBin, double nPot) : Flux(path, nbins, sizeBin,
                                                                                                   nPot) {
    if (this->path == "default") this->path = "../../Flux_data/cvwgt_flux_nu.dat";
    if (this->nbins == 0) this->nbins = 150;
    if (this->sizeBin == 0) this->sizeBin = 0.1; // GeV
    if (this->nPOT == 0) this->nPOT = 1.15736e21;

    double apotema = 85.0;//cm
    double area_hexagon = 6.0 * apotema*apotema / sqrt(3);
    double depth = 1.7; //cm
    double volume = area_hexagon*depth;
    double number_planes = 84.0*2.0;
    double percent_of_plane = 89.47; // % of plastic in each plane
    double percent_of_12C = 87.62; // % of carbon in the plastic of the plane
    double density = 2.21; // g/cm^3
    double mass = volume * number_planes * percent_of_plane * percent_of_12C * density;
    double molecular_mass = 12.0; //
    this->nTargets = Param::NA * mass / molecular_mass;

    this->read_flux();

}

void Flux_Minerva::read_flux() {
    std::ifstream file(this->path.c_str());//opening imput document

    std::string line;

    int i =0;
    while(std::getline(file, line)) // read one line from ifs
    {
        if(i<this->nbins) {
            std::istringstream iss(line); // access line as a stream

            double columns[7];

            iss >> columns[0] >> columns[1] >> columns[2]; // no need to read further

            this->EnuBinMin.push_back(columns[0]); // bins in GeV
            this->EnuBinMax.push_back(columns[1]);
            this->N_Events.push_back(columns[2]); // file has the events by GeV, POT and cm^2
        }
    }
}
