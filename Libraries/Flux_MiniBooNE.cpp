//
// Created by edusaul on 30/04/19.
//

#include <Parameters_GeV.h>
#include "Flux_MiniBooNE.h"

Flux_MiniBooNE::Flux_MiniBooNE(const std::string &path, int nbins, double sizeBin, double nPot) : Flux(path, nbins,
                                                                                                       sizeBin, nPot) {
    if (this->path == "default") this->path = "../../Flux_data/miniboone_fluxes.dat";
    if (this->nbins == 0) this->nbins = 60;
    if (this->sizeBin == 0) this->sizeBin = 0.05; // GeV
    if (this->nPOT == 0) this->nPOT = 6.46e20;

    double rMB=610.6; //(cm)
    double rhoMB=0.845; //(g/cm^3)
    double mMB=4.0/3.0 * Param::pi*rMB*rMB*rMB*rhoMB;
    double dimensions = 2.325e2 * 2.5604e2 * 10.368e2; //cm^3
    double mass = 1395.4e-3 * dimensions; // g (LAr density = 1395 kg/m^3)
    double molecular_mass = 40.0; //
    this->nTargets = Param::NA * mass / molecular_mass;

    this->read_flux();
}

void Flux_MiniBooNE::read_flux() {

}
