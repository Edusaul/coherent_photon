//
// Created by edusaul on 26/04/19.
//

#ifndef COHERENT_PHOTON_FLUX_MINERVA_H
#define COHERENT_PHOTON_FLUX_MINERVA_H


#include "Flux.h"

class Flux_Minerva : public Flux{
public:
    Flux_Minerva(const std::string &path, int nbins, double sizeBin, double nPot);

    void read_flux() override;
};


#endif //COHERENT_PHOTON_FLUX_MINERVA_H
