//
// Created by edusaul on 17/04/19.
//

#include <utility>
#include <IntegralGauss20.h>
#include "Flux.h"


Flux::Flux(std::string path, int nbins, double sizeBin, double nPOT) : path(std::move(path)), nbins(nbins), sizeBin(sizeBin), nPOT(nPOT) {

//    this->npts_int = 1;
//    this->intFlux = -1;
}

double Flux::integrand(double Enu) {
    // Imput Enu in MeV
    double aux=-1.0;
    double res;

    for(int i=0; i<this->nbins; i++){
        if(Enu<this->EnuBinMax[i]){
            aux=this->N_Events[i];
            break;
        }
    }
    if(aux>0.) res=aux;
    else res=0.0;
    return res; // neutrinos/cm^2/POT;
}

int Flux::getNbins() const {
    return nbins;
}

double Flux::getSizeBin() const {
    return sizeBin;
}

double Flux::getNpot() const {
    return nPOT;
}

double Flux::getNTargets() const {
    return nTargets;
}

//double Flux::integrated_flux() {
//    if(this->intFlux != -1) return this->intFlux;
//    else {
//        return IntegralGauss20::integrate<double>(&this, this->EnuBinMin[0], this->EnuBinMax.end(), this->npts_int);
//    }
//}
