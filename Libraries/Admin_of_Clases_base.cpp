//
// Created by edusaul on 7/03/19.
//

#include <Write_static.h>
#include <iostream>
#include <Parameters_GeV.h>
#include <IntegralGauss20.h>
#include <dsdEg_dphig_dthg_dth.h>
#include <dsdEg_dphig_dthg.h>
#include <dsdEg_dphig.h>
#include <dsdEg.h>
#include <dsdThG_dEg_dphig.h>
#include <dsdThG_dEg.h>
#include <dsdThGamma.h>
#include "Admin_of_Clases_base.h"
#include "Flux_MicroBooNE.h"
#include "Flux_Minerva.h"

Admin_of_Clases_base::Admin_of_Clases_base(Arguments_base *parameters) : parameters(parameters) {
    this->ds_data.resize(2);

    double e2 = 4.0 * Param::pi * Param::alpha;
    this->constant_factors = Param::Gf2 * e2 / (2.0 * 8.0 * pow(2.0*Param::pi,4) ) * Param::hccm2;
    this->k0 = this->parameters->getK0();

    double A = 0.0;
    if(this->parameters->getNucleus() == "12C" ) A = Param::AC;
    else if(this->parameters->getNucleus() == "40Ar" ) A = Param::AAr;
    this->kappa_m = 10.0/(1.2*pow(A,1.0/3.0));
}


Admin_of_Clases_base::~Admin_of_Clases_base() = default;

void Admin_of_Clases_base::write_dsdEgamma() {

    double k0_gamma = this->parameters->getEgamma_min();
    double k0_step = (this->parameters->getEgamma_max() - k0_gamma)/double(this->parameters->getNpts()-1);
    for (int i = 0; i < this->parameters->getNpts(); ++i) {
        this->ds_data[0].push_back(k0_gamma);
        this->ds_data[1].push_back(this->dsdk0->integrand(k0_gamma));
        k0_gamma += k0_step;

        std::cout<<this->ds_data[0][i]<<"  "<<this->ds_data[1][i]<<std::endl;
    }
    Write_static::Write_to_file(this->parameters->getFilePath(), this->ds_data);
}



void Admin_of_Clases_base::write_dsdthg() {

    double th_gamma = this->parameters->getThg_min();
    double thg_step = (this->parameters->getThg_max() - th_gamma)/double(this->parameters->getNpts()-1);
    for (int i = 0; i < this->parameters->getNpts(); ++i) {
        this->ds_data[0].push_back(th_gamma);
        this->ds_data[1].push_back(this->dsdthg->integrand(th_gamma));
        th_gamma += thg_step;

        std::cout<<this->ds_data[0][i]<<"  "<<this->ds_data[1][i]<<std::endl;
    }
    Write_static::Write_to_file(this->parameters->getFilePath(), this->ds_data);
}



double Admin_of_Clases_base::get_d5s(double kg0, double theta, double thetag, double phig) {
    std::vector<double> param = {this->parameters->getK0(), kg0, phig, thetag};
    this->dsdEgamma_dphig_dthg_dth->change_other_parameters(param);

    double factors = kg0 * (this->k0-kg0) / k0;

    // here we have to divide by sin(theta) because in the function the cs is
    // multiplied by this factor because of the integral
    double d5 = this->dsdEgamma_dphig_dthg_dth->integrand(theta) /sin(theta) * constant_factors * factors / (2.0*Param::pi);

    std::cout<<"k0 = "<<this->k0<<" ,  kg0 = "<<kg0<<" ,  theta = "<<theta
    <<" , theta_g  = "<<thetag<<" , phi_g = "<<phig
    <<" ,  ds5 = "<<d5<<std::endl;

    return d5;
}

void Admin_of_Clases_base::write_dNdEg_with_Flux() {
//    Flux_MicroBooNE flux(this->parameters->getFileFluxPath(),this->parameters->getFluxNBins(),this->parameters->getFluxBinSize(), this->parameters->getNpot());

    std::vector<double> enu(1);
    double sum;


    double k0_gamma = this->parameters->getEgamma_min();
    double k0_step = (this->parameters->getEgamma_max() - k0_gamma)/double(this->parameters->getNpts()-1);
    for (int i = 0; i < this->parameters->getNpts(); ++i) {
        this->ds_data[0].push_back(k0_gamma);

        sum=0.0;
        enu[0]= this->flux->getSizeBin()/2.0;
        for (int j = 0; j < this->flux->getNbins(); ++j) {

            if (enu[0] > k0_gamma) {
                this->dsdk0->change_other_parameters(enu);
                // here we should multiply by the width of the bin, but this cancels with the flux factor
                // because in MicroBooNE we have to divide the flux by the binsize
                sum += this->dsdk0->integrand(k0_gamma) * this->flux->integrand(enu[0]); // * flux.getSizeBin());
            }
//            std::cout<<"    "<<k0_gamma<<"  "<<enu[0]<<"  "<<sum<<std::endl;

            enu[0] += this->flux->getSizeBin();
        }


        this->ds_data[1].push_back(sum * this->flux->getNpot() * this->flux->getNTargets());// / flux.getSizeBin()); // Events/GeV
        k0_gamma += k0_step;

        std::cout<<this->ds_data[0][i]<<"  "<<this->ds_data[1][i]<<std::endl;
    }
    Write_static::Write_to_file(this->parameters->getFilePath(), this->ds_data);


}

void Admin_of_Clases_base::write_dNdthg_with_Flux() {
//    Flux_MicroBooNE flux(this->parameters->getFileFluxPath(),this->parameters->getFluxNBins(),this->parameters->getFluxBinSize(), this->parameters->getNpot());

    std::vector<double> enu(1);
    double sum;

    double th_gamma = this->parameters->getThg_min();
    double thg_step = (this->parameters->getThg_max() - th_gamma)/double(this->parameters->getNpts()-1);
    for (int i = 0; i < this->parameters->getNpts(); ++i) {
        this->ds_data[0].push_back(th_gamma);

        sum=0.0;
        enu[0]= this->flux->getSizeBin()/2.0;
        for (int j = 0; j < this->flux->getNbins(); ++j) {

            this->dsdthg->change_other_parameters(enu);
            // here we should multiply by the width of the bin, but this cancels with the flux factor
            // because in MicroBooNE we have to divide the flux by the binsize
            sum += this->dsdthg->integrand(th_gamma) * this->flux->integrand(enu[0]); // * flux.getSizeBin());

//            std::cout<<"    "<<this->dsdthg->integrand(th_gamma)<<"  "<<flux.integrand(enu[0])<<std::endl;
//            std::cout<<"    "<<th_gamma<<"  "<<enu[0]<<"  "<<sum<<std::endl<<std::endl;

            enu[0] += this->flux->getSizeBin();
        }


        this->ds_data[1].push_back(sum * this->flux->getNpot() * this->flux->getNTargets());// / flux.getSizeBin()); // Events/rad
        th_gamma += thg_step;

        std::cout<<this->ds_data[0][i]<<"  "<<this->ds_data[1][i]<<std::endl;
    }
    Write_static::Write_to_file(this->parameters->getFilePath(), this->ds_data);
}

void Admin_of_Clases_base::setExpFlux(Flux *expFlux) {
    flux = expFlux;
}

void Admin_of_Clases_base::write_dNdEg_MicroBooNE() {
    Flux_MicroBooNE fluxMicroBooNE(this->parameters->getFileFluxPath(),this->parameters->getFluxNBins(),this->parameters->getFluxBinSize(), this->parameters->getNpot());
    this->setExpFlux(&fluxMicroBooNE);
    this->write_dNdEg_with_Flux();
}

void Admin_of_Clases_base::write_dNdthg_MicroBooNE() {
    Flux_MicroBooNE fluxMicroBooNE(this->parameters->getFileFluxPath(),this->parameters->getFluxNBins(),this->parameters->getFluxBinSize(), this->parameters->getNpot());
    this->setExpFlux(&fluxMicroBooNE);
    this->write_dNdthg_with_Flux();
}

void Admin_of_Clases_base::write_dNdEg_Minerva() {
    Flux_Minerva fluxMinerva(this->parameters->getFileFluxPath(),this->parameters->getFluxNBins(),this->parameters->getFluxBinSize(), this->parameters->getNpot());
    this->setExpFlux(&fluxMinerva);
    this->write_dNdEg_with_Flux();
}

void Admin_of_Clases_base::write_dNdthg_Minerva() {
    Flux_Minerva fluxMinerva(this->parameters->getFileFluxPath(),this->parameters->getFluxNBins(),this->parameters->getFluxBinSize(), this->parameters->getNpot());
    this->setExpFlux(&fluxMinerva);
    this->write_dNdthg_with_Flux();
}

void Admin_of_Clases_base::write_Integrated_CS() {

    std::vector<double> k0v(1);
    k0v[0] = {parameters->getK0Min()};
    double k0step = (this->parameters->getK0Max() - k0v[0])/double(this->parameters->getNpts()-1);
    double k0g_min = 0.0;
    int npts = 1;

    double cs;

    for (int i = 0; i < this->parameters->getNpts(); ++i) {
        this->ds_data[0].push_back(k0v[0]);


        this->dsdk0->change_other_parameters(k0v);
        cs = IntegralGauss20::integrate<double>(this->dsdk0, k0g_min, k0v[0], npts);

        this->ds_data[1].push_back(cs);
        k0v[0] += k0step;

        std::cout<<this->ds_data[0][i]<<"  "<<this->ds_data[1][i]<<std::endl;
    }
    Write_static::Write_to_file(this->parameters->getFilePath(), this->ds_data);
}

void Admin_of_Clases_base::create_dsdEg_clases() {
    int precision = 1;
    int precision_int_phi = 2;
    this->dsdEgamma_dphig_dthg_dth = new dsdEg_dphig_dthg_dth(this->parameters->getMode(), this->current_R);
    this->dsdk0_dphig_dthg = new dsdEg_dphig_dthg(this->dsdEgamma_dphig_dthg_dth, precision, this->kappa_m);
    this->dsdk0_dphig = new dsdEg_dphig(this->dsdk0_dphig_dthg, precision_int_phi, this->kappa_m);

//    int precision = 10;
//    this->dsdEgamma_dphig_dthg_dth = new dsdEg_dphig_dthg_dth(this->parameters->getMode(), this->current_R, this->kappa_m);
//    this->dsdk0_dphig_dthg = new dsdEg_dphig_dthg(this->dsdEgamma_dphig_dthg_dth, precision);
//    this->dsdk0_dphig = new dsdEg_dphig(this->dsdk0_dphig_dthg, precision_int_phi);

    this->dsdk0 = new dsdEg(this->dsdk0_dphig);



    std::vector<double> k0v = {this->k0};
    this->dsdk0->change_other_parameters(k0v);
}

void Admin_of_Clases_base::delete_dsdEg_clases() {
    delete this->dsdk0;
    delete this->dsdk0_dphig;
    delete this->dsdk0_dphig_dthg;
    delete this->dsdEgamma_dphig_dthg_dth;
    delete this->current_R;
}

void Admin_of_Clases_base::create_dsdThg_clases() {
    int precision = 5;
    int precision2 = 1;
    int precision3 = 1;
    double Eg_integral_limit = 2.5; //GeV
    this->dsdEgamma_dphig_dthg_dth = new dsdEg_dphig_dthg_dth(this->parameters->getMode(), this->current_R, this->kappa_m);
    this->dsdthg_dEg_dphig = new dsdThG_dEg_dphig(this->dsdEgamma_dphig_dthg_dth, precision);
    this->dsdthg_dEg = new dsdThG_dEg(this->dsdthg_dEg_dphig, precision2);
    this->dsdthg = new dsdThGamma(this->dsdthg_dEg, Eg_integral_limit, precision3);

    std::vector<double> k0v = {this->k0};
    this->dsdthg->change_other_parameters(k0v);
}

void Admin_of_Clases_base::delete_dsdThg_clases() {
    delete this->dsdthg;
    delete this->dsdthg_dEg;
    delete this->dsdthg_dEg_dphig;
    delete this->dsdEgamma_dphig_dthg_dth;
    delete this->current_R;
}

