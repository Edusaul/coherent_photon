#include <utility>

//
// Created by edusaul on 7/03/19.
//

#include "Arguments_base.h"

#include <tclap/CmdLine.h>
#include <ToString.h>
#include <Parameters_GeV.h>

using namespace TCLAP;
using namespace std;

Arguments_base::Arguments_base(int argc, char **argv, string distributionType) : distribution_type(std::move(distributionType)) {

//Arguments_base::Arguments_base(int argc, char **argv) {
    try {
        CmdLine cmd("Program to coherent photoproduction", ' ', "0.1");

        ValueArg<string> filePath("","path","Set the path to write file",false, "default", "string");
        cmd.add( filePath );
        ValueArg<double> k0("","k0","Energy of the incident neutrino in GeV (default 1 GeV)",false, 1.0, "double");
        cmd.add( k0 );
        ValueArg<double> Egamma_min("","Egmin","Minimum value for the photon energy in GeV (default 0 GeV)",false, 0.0, "double");
        cmd.add( Egamma_min );
        ValueArg<double> Egamma_max("","Egmax","Maximum value for the photon energy in GeV (default 1 GeV)",false, 1.0, "double");
        cmd.add( Egamma_max );
        ValueArg<double> thg_min("","thmin","Minimum value for the photon angle in rad (default 0)",false, 0.0, "double");
        cmd.add( thg_min );
        ValueArg<double> thg_max("","thmax","Maximum value for the photon angle in rad (default pi/2)",false, Param::pi/2.0, "double");
        cmd.add( thg_max );
        ValueArg<int> npts("n","Npts","Number of points to write (default 50)",false, 50, "int");
        cmd.add( npts );
        ValueArg<string> mode("m","mode","Select nu or nubar mode",false, "nu", "string");
        cmd.add( mode );
        ValueArg<string> nucleus("N","nucleus","Select the scattering nucleus (default 12C)",false, "12C", "string");
        cmd.add( nucleus );
        ValueArg<string> fileFluxPath("","FluxPath","Set the path to read the flux file",false, "default", "string");
        cmd.add( fileFluxPath );
        ValueArg<int> FluxNBins("","FluxNBins","Number of Bins in FluxFile",false, 0, "int");
        cmd.add( FluxNBins );
        ValueArg<double> FluxBinSize("","FluxBinSize","Size of bins in GeV in flux file",false, 0.0, "double");
        cmd.add( FluxBinSize );
        ValueArg<double> nPOT("","nPOT","number of POT",false, 0.0, "double");
        cmd.add( nPOT );

        ValueArg<double> k0min("","k0min","Minimum neutrino energy for integrated CS in GeV (default 0 GeV)",false, 0.0, "double");
        cmd.add( k0min );
        ValueArg<double> k0max("","k0max","Maximum neutrino energy for integrated CS in GeV (default 1.5 GeV)",false, 1.5, "double");
        cmd.add( k0max );



        cmd.parse( argc, argv );

        this->k0 = k0.getValue();
        this->Egamma_min = Egamma_min.getValue();
        this->Egamma_max = Egamma_max.getValue();
        this->thg_min = thg_min.getValue();
        this->thg_max = thg_max.getValue();
        this->npts = npts.getValue();
        this->mode = mode.getValue();
        this->nucleus = nucleus.getValue();
        this->filePath = filePath.getValue();
        if (filePath.getValue() == "default"){
            this->filePath = "../../Data/" + this->distribution_type + "_k0_" + ToString::to_string(this->k0) + "GeV_" + this->mode + "_" + this->nucleus +".dat";
        } else this->filePath = filePath.getValue();

        this->fileFluxPath = fileFluxPath.getValue();
        this->FluxNBins = FluxNBins.getValue();
        this->thg_max = thg_max.getValue();
        this->nPOT = nPOT.getValue();

        this->k0min = k0min.getValue();
        this->k0max = k0max.getValue();

    }catch (ArgException &e)  // catch any exceptions
	{ cerr << "error: " << e.error() << " for arg " << e.argId() << endl; }
}

const string &Arguments_base::getFilePath() const {
    return filePath;
}

double Arguments_base::getK0() const {
    return k0;
}

double Arguments_base::getEgamma_min() const {
    return Egamma_min;
}

double Arguments_base::getEgamma_max() const {
    return Egamma_max;
}

int Arguments_base::getNpts() const {
    return npts;
}

const string &Arguments_base::getMode() const {
    return mode;
}

const string &Arguments_base::getNucleus() const {
    return nucleus;
}

double Arguments_base::getThg_min() const {
    return thg_min;
}

double Arguments_base::getThg_max() const {
    return thg_max;
}

const string &Arguments_base::getFileFluxPath() const {
    return fileFluxPath;
}

int Arguments_base::getFluxNBins() const {
    return FluxNBins;
}

double Arguments_base::getFluxBinSize() const {
    return FluxBinSize;
}

double Arguments_base::getNpot() const {
    return nPOT;
}

double Arguments_base::getK0Min() const {
    return k0min;
}

double Arguments_base::getK0Max() const {
    return k0max;
}





