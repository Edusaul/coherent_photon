//
// Created by edusaul on 17/04/19.
//

#ifndef COHERENT_PHOTON_FLUX_MICROBOONE_H
#define COHERENT_PHOTON_FLUX_MICROBOONE_H


#include "Flux.h"

class Flux_MicroBooNE : public Flux{
public:
    Flux_MicroBooNE(const std::string &path, int nbins, double sizeBin, double nPOT);

    void read_flux() override;

};


#endif //COHERENT_PHOTON_FLUX_MICROBOONE_H
