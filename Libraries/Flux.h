//
// Created by edusaul on 17/04/19.
//

#ifndef COHERENT_PHOTON_FLUX_H
#define COHERENT_PHOTON_FLUX_H

#include <Integrable.h>
#include <string>
#include <vector>

class Flux  : public Integrable<double>{
protected:
    std::string path;
    int nbins;
    double sizeBin;
    std::vector<double> EnuBinMin;
    std::vector<double> EnuBinMax;
    std::vector<double> N_Events;
    std::vector<double> delta_N_Events;
    double intFlux;
    int npts_int;
    double nPOT;
    double nTargets;

public:
    Flux(std::string path, int nbins, double sizeBin, double nPOT);

    double integrand(double) override;

    virtual void read_flux() = 0;

//    double integrated_flux();

    int getNbins() const;

    double getSizeBin() const;

    double getNpot() const;

    double getNTargets() const;

};


#endif //COHERENT_PHOTON_FLUX_H
