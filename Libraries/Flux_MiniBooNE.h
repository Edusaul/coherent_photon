//
// Created by edusaul on 30/04/19.
//

#ifndef COHERENT_PHOTON_FLUX_MINIBOONE_H
#define COHERENT_PHOTON_FLUX_MINIBOONE_H


#include "Flux.h"

class Flux_MiniBooNE : public Flux{
public:
    Flux_MiniBooNE(const std::string &path, int nbins, double sizeBin, double nPot);

    void read_flux() override;
};


#endif //COHERENT_PHOTON_FLUX_MINIBOONE_H
