//
// Created by edusaul on 7/03/19.
//

#ifndef COHERENT_PHOTON_ARGUMENTS_BASE_H
#define COHERENT_PHOTON_ARGUMENTS_BASE_H

#include <string>

class Arguments_base {
protected:
    std::string filePath;
    double k0;
    double Egamma_min;
    double Egamma_max;
    double thg_min;
    double thg_max;
    int npts;
    std::string mode;
    std::string nucleus;
    std::string distribution_type;

    std::string fileFluxPath;
    int FluxNBins;
    double FluxBinSize;
    double nPOT;

    double k0min;
    double k0max;

public:

    Arguments_base(int argc, char** argv, std::string distributionType);

//    Arguments_base(int argc, char** argv);

    const std::string &getFilePath() const;

    double getK0() const;

    double getEgamma_min() const;

    double getEgamma_max() const;

    int getNpts() const;

    const std::string &getMode() const;

    const std::string &getNucleus() const;

    double getThg_min() const;

    double getThg_max() const;

    const std::string &getFileFluxPath() const;

    int getFluxNBins() const;

    double getFluxBinSize() const;

    double getNpot() const;

    double getK0Min() const;

    double getK0Max() const;

};


#endif //COHERENT_PHOTON_ARGUMENTS_BASE_H
