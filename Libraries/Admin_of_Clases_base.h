//
// Created by edusaul on 7/03/19.
//

#ifndef COHERENT_PHOTON_ADMIN_OF_CLASES_H
#define COHERENT_PHOTON_ADMIN_OF_CLASES_H


#include "Arguments_base.h"
#include "Integrable_w_change_param.h"
#include "Hadronic_Current_R.h"
#include "Flux.h"
#include <vector>


class Admin_of_Clases_base {
protected:
    Arguments_base *parameters;
    std::vector<std::vector<double>> ds_data;
    double constant_factors;
    double k0;
    double kappa_m;

    // classes for energy distribution
    Integrable_w_change_param<double> *dsdk0;
    Integrable_w_change_param<double> *dsdk0_dphig;
    Integrable_w_change_param<double> *dsdk0_dphig_dthg;

    // classes for angular distribution
    Integrable_w_change_param<double> *dsdthg;
    Integrable_w_change_param<double> *dsdthg_dEg;
    Integrable_w_change_param<double> *dsdthg_dEg_dphig;

    // classes for d5 (common classes for all methods)
    Integrable_w_change_param<double> *dsdEgamma_dphig_dthg_dth;
    Hadronic_Current_R *current_R;

    Flux *flux;

    void create_dsdEg_clases();
    void delete_dsdEg_clases();
    void create_dsdThg_clases();
    void delete_dsdThg_clases();

public:
    explicit Admin_of_Clases_base(Arguments_base *parameters);

    virtual ~Admin_of_Clases_base();

    virtual void write_dsdEgamma();

    virtual void write_dsdthg();

    virtual double get_d5s(double, double, double, double);

    virtual void write_dNdEg_with_Flux();

    virtual void write_dNdthg_with_Flux();

    virtual void setExpFlux(Flux *expFlux);

    virtual void write_dNdEg_MicroBooNE();

    virtual void write_dNdthg_MicroBooNE();

    virtual void write_dNdEg_Minerva();

    virtual void write_dNdthg_Minerva();

    virtual void write_Integrated_CS();


};


#endif //COHERENT_PHOTON_ADMIN_OF_CLASES_H
