//
// Created by edusaul on 27/03/19.
//

#include "Arguments_d5.h"

#include <tclap/CmdLine.h>
#include <ToString.h>
#include <Parameters_GeV.h>

using namespace TCLAP;
using namespace std;

Arguments_d5::Arguments_d5(int argc, char **argv, std::string distributionType) : Arguments_base(argc, argv, distributionType) {
    try {
        CmdLine cmd("Program to calculate differential cross section for coherent photoproduction", ' ', "0.1");

        ValueArg<string> filePath("","path","Set the path to write file",false, "default", "string");
        cmd.add( filePath );
        ValueArg<double> kg0("","kg0","Energy of the outgoing photon in GeV  (default 0.3 GeV)",false, 0.3, "double");
        cmd.add( kg0 );
        ValueArg<double> theta("","theta","Angle of the outgoing neutrino with respect to the incident one in degrees (default 10º)",false, 10.0, "double");
        cmd.add( theta );
        ValueArg<double> thetag("","thetag","Theta angle of the outgoing photon with respect to the incident neutrino in degrees (default 10º)",false, 10.0, "double");
        cmd.add( thetag );
        ValueArg<double> phig("","phig","Phi angle of the outgoing photon with respect to the incident neutrino in degrees (default 20º)",false, 20.0, "double");
        cmd.add( phig );

        cmd.parse( argc, argv );

        this->kg0 = kg0.getValue();
        this->theta = theta.getValue()*Param::pi/180;
        this->thetag = thetag.getValue()*Param::pi/180;
        this->phig = phig.getValue()*Param::pi/180;
        if (filePath.getValue() == "default"){
            this->filePath = "../../Data/ds5_k0_" + ToString::to_string(this->k0) + "GeV__kg0_"
                             + ToString::to_string(this->kg0) + "GeV__th_"
                             + ToString::to_string(theta.getValue()) + "__thg_"
                             + ToString::to_string(thetag.getValue()) + "__phig_"
                             + ToString::to_string(phig.getValue()) + "__" + this->mode
                             + "_" + this->nucleus +".dat";
        } else this->filePath = filePath.getValue();

    }catch (ArgException &e)  // catch any exceptions
    { cerr << "error: " << e.error() << " for arg " << e.argId() << endl; }
}

double Arguments_d5::getKg0() const {
    return kg0;
}

double Arguments_d5::getTheta() const {
    return theta;
}

double Arguments_d5::getThetag() const {
    return thetag;
}

double Arguments_d5::getPhig() const {
    return phig;
}


