import numpy as np
from matplotlib import pyplot as plt

# data = np.loadtxt("/home/edusaul/CLionProjects/Coherent_Photon/Data/dsdEg_Delta_avg_k0_1GeV_nu_12C.dat")
# data = np.loadtxt("../Data/dsdEg_Delta_avg_k0_1GeV_nu_12C_cnst_gamma.dat")
data_no_med = np.loadtxt("../Data/dsdEg_Delta_avg_k0_1GeV_nu_12C_no_med_HO.dat")
# data_150 = np.loadtxt("../Data/dsdEg_Delta_avg_k0_1GeV_nu_12C_150pts.dat")
data_avg_HO = np.loadtxt("../Data/dsdEg_Delta_avg_k0_1GeV_nu_12C_HO.dat")
data_D1232_new = np.loadtxt("../Data/z_dsdEg_D1232_k0_1GeV_nu_12C.dat")
data_D1232_old = np.loadtxt("../Data/z_dsdEg_D1232_k0_1GeV_nu_12C_old_HO.dat")
# data_new_v0 = np.loadtxt("../Data/z_dsdEg_D1232_k0_1GeV_nu_12C_v0.dat")
# data_new_v1 = np.loadtxt("../Data/z_dsdEg_D1232_k0_1GeV_nu_12C_v1.dat")
# data_Luis = np.loadtxt("../Data/fort.6008")
# data_int = np.loadtxt("../Data/dsdEg_Delta_int_k0_1GeV_nu_12C.dat")


# plt.plot(data)

# to get Luis fort.6008 results besides param one has to change rho(0) to rho0 in Delta_in_medium

# plt.plot(data[:, 0], data[:, 1], '-', label='Delta const $\Gamma$')
plt.plot(data_no_med[:, 0], data_no_med[:, 1], '-', label='Delta no med')
# plt.plot(data_150[:, 0], data_150[:, 1], '-', label='Delta with med')
plt.plot(data_avg_HO[:, 0], data_avg_HO[:, 1], '-', label='Delta with med')
plt.plot(data_D1232_new[:, 0], data_D1232_new[:, 1], '.', label='D1232 new Tr')
plt.plot(data_D1232_old[:, 0], data_D1232_old[:, 1], '.', label='D1232 old Tr')
# plt.plot(data_new_v1[:, 0], data_new_v1[:, 1], '.', label='D1232 with $\Delta$ FF')
# plt.plot(data_Luis[:, 0]*1e-3, data_Luis[:, 1]*1e-38, '-r', label='Luis')
# plt.plot(data_int[:, 0], data_int[:, 1], 'go', label='Integral')

# X = data[:, 0]
# Y = data[:, 1]

# plt.plot(X, Y, ':ro')

# plt.title(r'$\frac{d\sigma}{d E_\gamma}$', fontsize=16)
plt.ylabel(r'$\frac{d\sigma}{d E_\gamma} \, (cm^2 GeV^-1)$', fontsize=14)
plt.xlabel(r'$E_\gamma (GeV)$', fontsize=14)
# plt.grid()

plt.legend()

plt.show()
