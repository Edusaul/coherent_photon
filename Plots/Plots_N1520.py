import numpy as np
from matplotlib import pyplot as plt

data = np.loadtxt("../Data/dsdEg_N1520_k0_1GeV_nu_12C.dat")
# data2 = np.loadtxt("../Data/dsdEg_N1520_A32_k0_1GeV_nu_12C.dat")
# data3 = np.loadtxt("../Data/dsdEg_N1520_s12_0_k0_1GeV_nu_12C.dat")
# data4 = np.loadtxt("../Data/dsdEg_N1520_s12_0_a12p_0_k0_1GeV_nu_12C.dat")
# data2 = np.loadtxt("../Data/dsdEg_N1520_c3c5_k0_6GeV_nu_12C.dat")
plt.figure(0)

plt.plot(data[:, 0], data[:, 1], label=r'N1520')
# plt.plot(data2[:, 0], data2[:, 1], label=r'N1520$_{c3c5}$')
# plt.plot(data2[:, 0], data2[:, 1], label=r'N1520 only $A_{3/2}$')
# plt.plot(data3[:, 0], data3[:, 1], label=r'N1520 only $A_{3/2}$')
# plt.plot(data4[:, 0], data4[:, 1], label=r'N1520 only $A_{3/2}$')

plt.axhline(y=0, color='black', linestyle=':')
plt.axvline(x=0, color='black', linestyle=':')


plt.title(r'$E_\nu = 1 GeV$', fontsize=16)
plt.ylabel(r'$\frac{d\sigma}{d E_\gamma} \, (cm^2 GeV^{-1})$', fontsize=14)
plt.xlabel(r'$E_\gamma (GeV)$', fontsize=14)
# plt.grid()

plt.legend()

plt.show()


# data = np.loadtxt("../Data/dsdEg_N1520_k0_10GeV_nu_12C.dat")
# plt.figure(0)
#
# plt.plot(data[:, 0], data[:, 1], label=r'N1520')
#
# plt.axhline(y=0, color='black', linestyle=':')
# plt.axvline(x=0, color='black', linestyle=':')
#
#
# # plt.title(r'$\frac{d\sigma}{d E_\gamma}$', fontsize=16)
# plt.ylabel(r'$\frac{d\sigma}{d E_\gamma} \, (cm^2 GeV^-1)$', fontsize=14)
# plt.xlabel(r'$E_\gamma (GeV)$', fontsize=14)
# # plt.grid()
#
# plt.legend()
#
# plt.show()