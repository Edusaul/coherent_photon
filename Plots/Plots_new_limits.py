import numpy as np
from matplotlib import pyplot as plt

# data_original = np.loadtxt("../Data/dsdEg_Delta_avg_k0_10GeV_nu_12C_original.dat")
# data_high_prec_g = np.loadtxt("../Data/dsdEg_Delta_avg_k0_10GeV_nu_12C_p10InThg.dat")
# data_high_prec_l = np.loadtxt("../Data/dsdEg_Delta_avg_k0_10GeV_nu_12C_p10InThl.dat")
# data_l1 = np.loadtxt("../Data/dsdEg_Delta_avg_k0_10GeV_nu_12C_l1.dat")
# data_l2 = np.loadtxt("../Data/dsdEg_Delta_avg_k0_10GeV_nu_12C_l2.dat")
# data_l3 = np.loadtxt("../Data/dsdEg_Delta_avg_k0_10GeV_nu_12C_l3.dat")
# data_l1l2 = np.loadtxt("../Data/dsdEg_Delta_avg_k0_10GeV_nu_12C_l1l2.dat")
# data_l1l2l3 = np.loadtxt("../Data/dsdEg_Delta_avg_k0_10GeV_nu_12C_l1l2l3.dat")
# data_l1l2l3max = np.loadtxt("../Data/dsdEg_Delta_avg_k0_10GeV_nu_12C_l1l2l3max.dat")
# plt.figure(0)
#
# plt.plot(data_original[:, 0], data_original[:, 1], 'o', label=r'Original')
# plt.plot(data_high_prec_g[:, 0], data_high_prec_g[:, 1], '*', label=r'High precision $\gamma$')
# plt.plot(data_high_prec_l[:, 0], data_high_prec_l[:, 1], '-x', label=r'High precision l')
# plt.plot(data_l1[:, 0], data_l1[:, 1], '-', label=r'l1')
# plt.plot(data_l2[:, 0], data_l2[:, 1], '+', label=r'l2')
# plt.plot(data_l3[:, 0], data_l3[:, 1], '+', label=r'l3')
# plt.plot(data_l1l2[:, 0], data_l1l2[:, 1], '.', label=r'l1 & l2')
# plt.plot(data_l1l2l3[:, 0], data_l1l2l3[:, 1], '-', label=r'l1, l2, l3')
# plt.plot(data_l1l2l3max[:, 0], data_l1l2l3max[:, 1], '-', label=r'l1, l2, l3max')
# plt.axhline(y=0, color='black', linestyle=':')
# plt.axvline(x=0, color='black', linestyle=':')
#
#
# # plt.title(r'$\frac{d\sigma}{d E_\gamma}$', fontsize=16)
# plt.ylabel(r'$\sigma (E_\nu) \, (cm^2)$', fontsize=14)
# plt.xlabel(r'$E_\nu (GeV)$', fontsize=14)
# # plt.grid()
#
# plt.legend()
#
# plt.show()
#
# data_1_original = np.loadtxt("../Data/dsdEg_Delta_avg_k0_1GeV_nu_12C_original.dat")
# data_1_high_prec_g = np.loadtxt("../Data/dsdEg_Delta_avg_k0_1GeV_nu_12C_p10InThg.dat")
# data_1_high_prec_l = np.loadtxt("../Data/dsdEg_Delta_avg_k0_1GeV_nu_12C_p10InThl.dat")
# data_1_l1 = np.loadtxt("../Data/dsdEg_Delta_avg_k0_1GeV_nu_12C_l1.dat")
# data_1_l2 = np.loadtxt("../Data/dsdEg_Delta_avg_k0_1GeV_nu_12C_l2.dat")
# data_1_l3 = np.loadtxt("../Data/dsdEg_Delta_avg_k0_1GeV_nu_12C_l3.dat")
# data_1_l1l2 = np.loadtxt("../Data/dsdEg_Delta_avg_k0_1GeV_nu_12C_l1l2.dat")
# data_1_l1l2l3 = np.loadtxt("../Data/dsdEg_Delta_avg_k0_1GeV_nu_12C_l1l2l3.dat")
# data_1_l1l2l3max = np.loadtxt("../Data/dsdEg_Delta_avg_k0_1GeV_nu_12C_l1l2l3max.dat")
# # plt.figure(1)
#
# plt.plot(data_1_original[:, 0], data_1_original[:, 1], 'o', label=r'Original')
# plt.plot(data_1_high_prec_g[:, 0], data_1_high_prec_g[:, 1], '*', label=r'High precision $\gamma$')
# plt.plot(data_1_high_prec_l[:, 0], data_1_high_prec_l[:, 1], '-x', label=r'High precision l')
# plt.plot(data_1_l1[:, 0], data_1_l1[:, 1], '-', label=r'l1')
# plt.plot(data_1_l2[:, 0], data_1_l2[:, 1], '+', label=r'l2')
# plt.plot(data_1_l3[:, 0], data_1_l3[:, 1], '+', label=r'l3')
# plt.plot(data_1_l1l2[:, 0], data_1_l1l2[:, 1], '.', label=r'l1 & l2')
# plt.plot(data_1_l1l2l3[:, 0], data_1_l1l2l3[:, 1], '-', label=r'l1, l2, l3')
# plt.plot(data_1_l1l2l3max[:, 0], data_1_l1l2l3max[:, 1], '-', label=r'l1, l2, l3max')
# plt.axhline(y=0, color='black', linestyle=':')
# plt.axvline(x=0, color='black', linestyle=':')
#
#
# # plt.title(r'$\frac{d\sigma}{d E_\gamma}$', fontsize=16)
# plt.ylabel(r'$\sigma (E_\nu) \, (cm^2)$', fontsize=14)
# plt.xlabel(r'$E_\nu (GeV)$', fontsize=14)
# # plt.grid()
#
# plt.legend()
#
# plt.show()


data_10_1_original = np.loadtxt("../Data/dsdEg_Delta_avg_k0_10GeV_nu_12C_Eg_1GeV_original.dat")
data_10_1_high_prec_g = np.loadtxt("../Data/dsdEg_Delta_avg_k0_10GeV_nu_12C_Eg_1GeV_p10InThg.dat")
data_10_1_high_prec_l = np.loadtxt("../Data/dsdEg_Delta_avg_k0_10GeV_nu_12C_Eg_1GeV_p10InThl.dat")
data_10_1_l1 = np.loadtxt("../Data/dsdEg_Delta_avg_k0_10GeV_nu_12C_Eg_1GeV_l1.dat")
data_10_1_l2 = np.loadtxt("../Data/dsdEg_Delta_avg_k0_10GeV_nu_12C_Eg_1GeV_l2.dat")
data_10_1_l3 = np.loadtxt("../Data/dsdEg_Delta_avg_k0_10GeV_nu_12C_Eg_1GeV_l3.dat")
data_10_1_l1l2l3 = np.loadtxt("../Data/dsdEg_Delta_avg_k0_10GeV_nu_12C_Eg_1GeV_l1l2l3.dat")
data_10_1_l1l2l3max = np.loadtxt("../Data/dsdEg_Delta_avg_k0_10GeV_nu_12C_Eg_1GeV_l1l2l3max.dat")
# plt.figure(1)

plt.plot(data_10_1_original[:, 0], data_10_1_original[:, 1], 'o', label=r'Original')
plt.plot(data_10_1_high_prec_g[:, 0], data_10_1_high_prec_g[:, 1], '*', label=r'High precision $\gamma$')
plt.plot(data_10_1_high_prec_l[:, 0], data_10_1_high_prec_l[:, 1], '-x', label=r'High precision l')
plt.plot(data_10_1_l1[:, 0], data_10_1_l1[:, 1], '-', label=r'l1')
plt.plot(data_10_1_l2[:, 0], data_10_1_l2[:, 1], '+', label=r'l2')
plt.plot(data_10_1_l3[:, 0], data_10_1_l3[:, 1], '+', label=r'l3')
plt.plot(data_10_1_l1l2l3[:, 0], data_10_1_l1l2l3[:, 1], '-', label=r'l1, l2, l3')
plt.plot(data_10_1_l1l2l3max[:, 0], data_10_1_l1l2l3max[:, 1], '-', label=r'l1, l2, l3max')
plt.axhline(y=0, color='black', linestyle=':')
plt.axvline(x=0, color='black', linestyle=':')


# plt.title(r'$\frac{d\sigma}{d E_\gamma}$', fontsize=16)
plt.ylabel(r'$\sigma (E_\nu) \, (cm^2)$', fontsize=14)
plt.xlabel(r'$E_\nu (GeV)$', fontsize=14)
# plt.grid()

plt.legend()

plt.show()


# data_20_original = np.loadtxt("../Data/dsdEg_Delta_avg_k0_20GeV_nu_12C_original.dat")
# data_20_high_prec_l = np.loadtxt("../Data/dsdEg_Delta_avg_k0_20GeV_nu_12C_p15InThl.dat")
# data_20_l1l2l3 = np.loadtxt("../Data/dsdEg_Delta_avg_k0_20GeV_nu_12C_l1l2l3.dat")
# # plt.figure(1)
#
# plt.plot(data_20_original[:, 0], data_20_original[:, 1], 'o', label=r'Original')
# plt.plot(data_20_high_prec_l[:, 0], data_20_high_prec_l[:, 1], '-x', label=r'High precision l')
# plt.plot(data_20_l1l2l3[:, 0], data_20_l1l2l3[:, 1], '-', label=r'l1, l2, l3')
# plt.axhline(y=0, color='black', linestyle=':')
# plt.axvline(x=0, color='black', linestyle=':')
#
#
# # plt.title(r'$\frac{d\sigma}{d E_\gamma}$', fontsize=16)
# plt.ylabel(r'$\sigma (E_\nu) \, (cm^2)$', fontsize=14)
# plt.xlabel(r'$E_\nu (GeV)$', fontsize=14)
# # plt.grid()
#
# plt.legend()
#
# plt.show()