import numpy as np
from matplotlib import pyplot as plt


N1700 = np.loadtxt("../Data/dsdEg_D1700_k0_1GeV_nu_12C.dat")

plt.plot(N1700[:, 0], N1700[:, 1]*1e-38, '-', label=r'N1700')

plt.axhline(y=0, color='black', linestyle=':')
plt.axvline(x=0, color='black', linestyle=':')

# plt.title(r'$\frac{d\sigma}{d E_\gamma}$', fontsize=16)
plt.ylabel(r'$\frac{d\sigma}{d E_\gamma} \, (cm^2 GeV^{-1})$', fontsize=14)
plt.xlabel(r'$E_\gamma (GeV)$', fontsize=14)
# plt.grid()

plt.legend()

plt.show()

