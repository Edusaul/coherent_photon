import numpy as np
from matplotlib import pyplot as plt


data = np.loadtxt("../Data/Check_FF_D1232_HAmp.dat")
plt.figure(0)

plt.plot(data[:, 0], data[:, 1], color='red', label=r'D1232 $A12N$')
plt.plot(data[:, 0], data[:, 2], 'blue', label=r'D1232 $A32N$')
plt.plot(data[:, 0], data[:, 3], 'green', label=r'D1232 $S12N$')
plt.axhline(y=0, color='black', linestyle='-')
plt.axvline(x=0, color='black', linestyle='-')


# plt.title(r'$\frac{d\sigma}{d E_\gamma}$', fontsize=16)
plt.ylabel(r'helicity amplitudes $(GeV^{-1/2})$', fontsize=14)
plt.xlabel(r'$Q^2 (GeV^2)$', fontsize=14)
# plt.grid()

plt.legend()

plt.show()





data = np.loadtxt("../Data/Check_FF_D1232_EM_FF.dat")
plt.figure(0)

plt.plot(data[:, 0], data[:, 1], 'red'  , label=r'D1232 $C3N$')
plt.plot(data[:, 0], data[:, 2], 'blue' , label=r'D1232 $C4N$')
plt.plot(data[:, 0], data[:, 3], 'green', label=r'D1232 $C5N$')
plt.axhline(y=0, color='black', linestyle=':')
plt.axvline(x=0, color='black', linestyle=':')


# plt.title(r'$\frac{d\sigma}{d E_\gamma}$', fontsize=16)
# plt.ylabel(r'$\frac{d\sigma}{d E_\gamma} \, (cm^2 GeV^-1)$', fontsize=14)
plt.xlabel(r'$Q^2 (GeV^2)$', fontsize=14)
# plt.grid()

plt.legend()

plt.show()


data = np.loadtxt("../Data/Check_FF_D1232_NC_FF.dat")
plt.figure(1)

plt.plot(data[:, 0], data[:, 1], 'red' , label=r'D1232 $C3N^{NC}$')
plt.plot(data[:, 0], data[:, 2], 'blue', label=r'D1232 $C4N^{NC}$')
plt.plot(data[:, 0], data[:, 3], 'green', label=r'D1232 $C5N^{NC}$')
plt.axhline(y=0, color='black', linestyle=':')
plt.axvline(x=0, color='black', linestyle=':')


# plt.title(r'$\frac{d\sigma}{d E_\gamma}$', fontsize=16)
# plt.ylabel(r'$\frac{d\sigma}{d E_\gamma} \, (cm^2 GeV^-1)$', fontsize=14)
plt.xlabel(r'$Q^2 (GeV^2)$', fontsize=14)
# plt.grid()

plt.legend()

plt.show()


plt.figure(2)

plt.plot(data[:, 0], data[:, 4], label=r'D1232 $Ca^{NC}$')
plt.axhline(y=0, color='black', linestyle=':')
plt.axvline(x=0, color='black', linestyle=':')


# plt.title(r'$\frac{d\sigma}{d E_\gamma}$', fontsize=16)
# plt.ylabel(r'$\frac{d\sigma}{d E_\gamma} \, (cm^2 GeV^-1)$', fontsize=14)
plt.xlabel(r'$Q^2 (GeV^2)$', fontsize=14)
# plt.grid()

plt.legend()

plt.show()