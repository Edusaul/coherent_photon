import numpy as np
from matplotlib import pyplot as plt

# Delta = np.loadtxt("../Data/dsdEg_Delta_avg_k0_1GeV_nu_12C.dat")
Delta_avg_noMed = np.loadtxt("../Data/dsdEg_Delta_avg_k0_1GeV_nu_12C_no_med_HO.dat")
Delta_int_noMed = np.loadtxt("../Data/dsdEg_Delta_int_k0_1GeV_nu_12C_no_med.dat")

D1232 = np.loadtxt("../Data/z_dsdEg_D1232_k0_1GeV_nu_12C_new.dat")
D1232_AeAs0 = np.loadtxt("../Data/z_dsdEg_D1232_k0_1GeV_nu_12C_new_AeAs0.dat")
D1232_oldTr = np.loadtxt("../Data/z_dsdEg_D1232_k0_1GeV_nu_12C_oldTr.dat")
D1232_oldTr_AeAs0 = np.loadtxt("../Data/z_dsdEg_D1232_k0_1GeV_nu_12C_oldTr_AeAs0.dat")

# plt.plot(Delta[:, 0], Delta[:, 1], '-', label=r'$\Delta$')
plt.plot(Delta_avg_noMed[:, 0], Delta_avg_noMed[:, 1], '-', label=r'$\Delta$ no med')
# plt.plot(Delta_int_noMed[:, 0], Delta_int_noMed[:, 1], '-', label=r'$\Delta$ int no med')

plt.plot(D1232[:, 0], D1232[:, 1], '.', label=r'$\Delta(1232)$ new')
plt.plot(D1232_AeAs0[:, 0], D1232_AeAs0[:, 1], ':', label=r'$\Delta(1232)$ new $A_E=A_S=0$')
plt.plot(D1232_oldTr[:, 0], D1232_oldTr[:, 1], '.', label=r'$\Delta(1232)$ old Tr')
plt.plot(D1232_oldTr_AeAs0[:, 0], D1232_oldTr_AeAs0[:, 1], ':', label=r'$\Delta(1232)$ old Tr, $A_E=A_S=0$')

plt.axhline(y=0, color='black', linestyle=':')
plt.axvline(x=0, color='black', linestyle=':')

# plt.title(r'$\frac{d\sigma}{d E_\gamma}$', fontsize=16)
plt.ylabel(r'$\frac{d\sigma}{d E_\gamma} \, (cm^2 GeV^{-1})$', fontsize=14)
plt.xlabel(r'$E_\gamma (GeV)$', fontsize=14)
# plt.grid()

plt.legend()

plt.show()
