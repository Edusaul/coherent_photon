import numpy as np
from matplotlib import pyplot as plt

Avg_sW_c12_r17 = np.loadtxt("../Data/dsdEg_Delta_avg_sW_c5a12_r17_k0_1GeV_nu_12C.dat")
Avg_sW_c12_r0 = np.loadtxt("../Data/dsdEg_Delta_avg_sW_c5a12_r0_k0_1GeV_nu_12C.dat")

int_sW_c12_r17 = np.loadtxt("../Data/dsdEg_Delta_int_sW_c5a12_r17_k0_1GeV_nu_12C.dat")
int_sW_c12_r0 = np.loadtxt("../Data/dsdEg_Delta_int_sW_c5a12_r0_k0_1GeV_nu_12C.dat")
int_sO_c12_r0 = np.loadtxt("../Data/dsdEg_Delta_int_sO_c5a12_r0_k0_1GeV_nu_12C.dat")
int_sW_mpL_c12_r17 = np.loadtxt("../Data/dsdEg_Delta_int_sW_mpL_c5a12_r17_k0_1GeV_nu_12C.dat")

old = np.loadtxt("../Data/BK/dsdEg_Delta_int_k0_1GeV_nu_12C_orig.dat")
old_LP = np.loadtxt("../Data/BK/dsdEg_Delta_int_k0_1GeV_nu_12C_Luis_Param.dat")

fort_773 = np.loadtxt("../Data/fort.773")
fort_775 = np.loadtxt("../Data/fort.775")
fort_7751 = np.loadtxt("../Data/fort.7751")
fort_776 = np.loadtxt("../Data/fort.776")
fort_6008 = np.loadtxt("../Data/fort.6008")
fort_6006 = np.loadtxt("../Data/fort.6006")
fort_20668 = np.loadtxt("../Data/fort.20668")

# plt.plot(Avg_sW_c12_r17[:, 0], Avg_sW_c12_r17[:, 1], ':', label=r'$\Delta: avg; C_5^A = 1.2$; sW; r17')
# plt.plot(Avg_sW_c12_r0[:, 0], Avg_sW_c12_r0[:, 1], ':', label=r'$\Delta: avg; C_5^A = 1.2$; sW; r0')

plt.plot(int_sW_c12_r17[:, 0], int_sW_c12_r17[:, 1], '--', label=r'$\Delta: int; C_5^A = 1.2$; sW; r17')
# plt.plot(int_sW_c12_r0[:, 0], int_sW_c12_r0[:, 1], '--', label=r'$\Delta: int; C_5^A = 1.2$; sW; r0')
# plt.plot(int_sO_c12_r0[:, 0], int_sO_c12_r0[:, 1], '--', label=r'$\Delta: int; C_5^A = 1.2$; sO; r0')
plt.plot(int_sW_mpL_c12_r17[:, 0], int_sW_mpL_c12_r17[:, 1], '--', label=r'$\Delta: int; C_5^A = 1.2$; sW; mpL; r0')

# plt.plot(old[:, 0], old[:, 1], 'x', label=r'old')
plt.plot(old_LP[:, 0], old_LP[:, 1], 'x', label=r'old LP')

# plt.plot(fort_773[:, 0], fort_773[:, 1]*1e-38, '-', label=r'fort.773')
plt.plot(fort_775[:, 0], fort_775[:, 1]*1e-38, '-', label=r'fort.775')
# plt.plot(fort_7751[:, 0], fort_7751[:, 1]*1e-38, '-', label=r'fort.7751')
plt.plot(fort_776[:, 0], fort_776[:, 1]*1e-38, '-', label=r'fort.776')
# plt.plot(fort_6008[:, 0]*1e-3, fort_6008[:, 1]*1e-38, '.', label=r'fort.6008')
# plt.plot(fort_6006[:, 0]*1e-3, fort_6006[:, 1] * 1e-38, '-', label=r'fort.6006')
# plt.plot(fort_20668[:, 0], fort_20668[:, 1] * 1e-38, '-', label=r'fort.20668')

plt.axhline(y=0, color='black', linestyle=':')
plt.axvline(x=0, color='black', linestyle=':')

# plt.title(r'$\frac{d\sigma}{d E_\gamma}$', fontsize=16)
plt.ylabel(r'$\frac{d\sigma}{d E_\gamma} \, (cm^2 GeV^{-1})$', fontsize=14)
plt.xlabel(r'$E_\gamma (GeV)$', fontsize=14)
# plt.grid()

plt.legend()

plt.show()
