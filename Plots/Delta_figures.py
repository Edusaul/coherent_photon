import numpy as np
from matplotlib import pyplot as plt

Avg_sW_c12_r17 = np.loadtxt("../Data/dsdEg_Delta_avg_sW_c5a12_r17_k0_1GeV_nu_12C.dat")
Avg_sW_c12_r0 = np.loadtxt("../Data/dsdEg_Delta_avg_sW_c5a12_r0_k0_1GeV_nu_12C.dat")

int_sW_c12_r17 = np.loadtxt("../Data/dsdEg_Delta_int_sW_c5a12_r17_k0_1GeV_nu_12C.dat")
int_sW_c12_r0 = np.loadtxt("../Data/dsdEg_Delta_int_sW_c5a12_r0_k0_1GeV_nu_12C.dat")
int_sW_mpL_c12_r17 = np.loadtxt("../Data/dsdEg_Delta_int_sW_mpL_c5a12_r17_k0_1GeV_nu_12C.dat")

fort_775 = np.loadtxt("../Data/fort.775")
fort_776 = np.loadtxt("../Data/fort.776")
fort_777 = np.loadtxt("../Data/fort.777")

# plt.plot(Avg_sW_c12_r17[:, 0], Avg_sW_c12_r17[:, 1], ':', label=r'$\Delta: avg; C_5^A = 1.2$; sW; r17')
# plt.plot(Avg_sW_c12_r0[:, 0], Avg_sW_c12_r0[:, 1], ':', label=r'$\Delta: avg; C_5^A = 1.2$; sW; r0')

# plt.plot(int_sW_c12_r17[:, 0], int_sW_c12_r17[:, 1], '--', label=r'$\Delta: int; C_5^A = 1.2$; sW; r17')
# plt.plot(int_sW_c12_r0[:, 0], int_sW_c12_r0[:, 1], '--', label=r'$\Delta: int; C_5^A = 1.2$; sW; r0')
# plt.plot(int_sW_mpL_c12_r17[:, 0], int_sW_mpL_c12_r17[:, 1], '--', label=r'$\Delta: int; C_5^A = 1.2$; sW; mpL; r0')

plt.plot(fort_775[:, 0], fort_775[:, 1]*1e-38, '-', label=r'FF Factorization')
plt.plot(fort_776[:, 0], fort_776[:, 1]*1e-38, '-', label=r'Approx in self-energy')
plt.plot(fort_777[:, 0], fort_777[:, 1]*1e-38, '-', label=r'Oset-Salcedo self-energy')

plt.axhline(y=0, color='black', linestyle=':')
plt.axvline(x=0, color='black', linestyle=':')

# plt.title(r'$\frac{d\sigma}{d E_\gamma}$', fontsize=16)
plt.ylabel(r'$\frac{d\sigma}{d E_\gamma} \, (cm^2 GeV^{-1})$', fontsize=14)
plt.xlabel(r'$E_\gamma (GeV)$', fontsize=14)
# plt.grid()

plt.legend()

plt.show()
