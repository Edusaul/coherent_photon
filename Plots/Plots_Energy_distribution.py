import numpy as np
from matplotlib import pyplot as plt

data_Delta = np.loadtxt("../Data/dsdEg_Delta_avg_k0_1GeV_nu_12C.dat")
data_N1520 = np.loadtxt("../Data/dsdEg_N1520_k0_1GeV_nu_12C.dat")
data_N1520_c3c5 = np.loadtxt("../Data/dsdEg_N1520_c3c5_k0_1GeV_nu_12C.dat")
data_N1520_c5a_allEM = np.loadtxt("../Data/dsdEg_N1520_c5a_allEM_k0_1GeV_nu_12C.dat")
data_N1520_c5a_c3EM = np.loadtxt("../Data/dsdEg_N1520_c5a_c3EM_k0_1GeV_nu_12C.dat")
data_N1440 = np.loadtxt("../Data/dsdEg_N1440_k0_1GeV_nu_12C.dat")
data_N1535 = np.loadtxt("../Data/dsdEg_N1535_k0_1GeV_nu_12C.dat")
data_nucleon = np.loadtxt("../Data/dsdEg_Nucleon_k0_1GeV_nu_12C.dat")

plt.figure(0)

plt.plot(data_Delta[:, 0], data_Delta[:, 1], '.', label=r'$\Delta 1232$')
plt.plot(data_N1520[:, 0], data_N1520[:, 1],             '-', label=r'N1520')
# plt.plot(data_N1520_c3c5[:, 0], data_N1520_c3c5[:, 1],   '-', label=r'N1520 c3c5')
# plt.plot(data_N1520_c5a_allEM[:, 0], data_N1520_c5a_allEM[:, 1],   '-', label=r'N1520 c5a allEM')
# plt.plot(data_N1520_c5a_c3EM[:, 0], data_N1520_c5a_c3EM[:, 1], '-', label=r'N1520 c5a c3vEM')
plt.plot(data_N1440[:, 0], data_N1440[:, 1],             '-', label=r'N1440')
plt.plot(data_N1535[:, 0], data_N1535[:, 1],             '-', label=r'N1535')
plt.plot(data_nucleon[:, 0], data_nucleon[:, 1],         '-', label=r'Nucleon')
# plt.plot(data_Delta[:, 0], data_Delta[:, 1] + data_N1520[:, 1] + data_N1440[:, 1] + data_N1535[:, 1]
#          + data_nucleon[:, 1], '-', label=r'Total')

plt.axhline(y=0, color='black', linestyle=':')
plt.axvline(x=0, color='black', linestyle=':')


# plt.title(r'$\frac{d\sigma}{d E_\gamma}$', fontsize=16)
plt.ylabel(r'$\frac{d\sigma}{d E_\gamma} \, (cm^2 GeV^{-1})$', fontsize=14)
plt.xlabel(r'$E_\gamma (GeV)$', fontsize=14)
# plt.grid()

plt.legend()

plt.show()
#
#
# data_Sum = np.loadtxt("../Data/dsdEg_Sum_k0_1GeV_nu_12C.dat")
#
# plt.figure(1)
#
# plt.plot(data_Delta[:, 0], data_Delta[:, 1], '+', label=r'$\Delta 1232$')
# plt.plot(data_N1520[:, 0], data_N1520[:, 1], '.', label=r'N1520')
# # plt.plot(data_N1520_c3c5[:, 0], data_N1520_c3c5[:, 1], '.', label=r'N1520 c3c5')
# # plt.plot(data_N1520_c5a_allEM[:, 0], data_N1520_c5a_allEM[:, 1],   '.', label=r'N1520 c5a allEM')
# # plt.plot(data_N1520_c5a_c3EM[:, 0], data_N1520_c5a_c3EM[:, 1], '.', label=r'N1520 c5a c3vEM')
# plt.plot(data_Sum[:, 0], data_Sum[:, 1], '-', label=r'Sum of amplitudes')
# # plt.plot(data_Delta[:, 0], data_Delta[:, 1] + data_N1520[:, 1] + data_N1440[:, 1] + data_N1535[:, 1]
# #          + data_nucleon[:, 1], '-', label=r'Total')
#
# plt.axhline(y=0, color='black', linestyle=':')
# plt.axvline(x=0, color='black', linestyle=':')
#
#
# plt.title(r'$E_\nu = 1 GeV$', fontsize=16)
# plt.ylabel(r'$\frac{d\sigma}{d E_\gamma} \, (cm^2 GeV^{-1})$', fontsize=14)
# plt.xlabel(r'$E_\gamma (GeV)$', fontsize=14)
# # plt.grid()
#
# plt.legend()
#
# plt.show()
#
#
data_Delta_10 = np.loadtxt("../Data/dsdEg_Delta_avg_k0_10GeV_nu_12C.dat")
data_N1520_10 = np.loadtxt("../Data/dsdEg_N1520_k0_10GeV_nu_12C.dat")
data_N1520_10_v0 = np.loadtxt("../Data/dsdEg_N1520_k0_10GeV_nu_12C_v0.dat")
data_N1440_10 = np.loadtxt("../Data/dsdEg_N1440_k0_10GeV_nu_12C.dat")
data_N1535_10 = np.loadtxt("../Data/dsdEg_N1535_k0_10GeV_nu_12C.dat")

data_Sum_10 = np.loadtxt("../Data/dsdEg_Sum_k0_10GeV_nu_12C.dat")

plt.figure(2)

plt.plot(data_Delta_10[:, 0], data_Delta_10[:, 1], '+', label=r'$\Delta 1232$')
plt.plot(data_N1520_10[:, 0], data_N1520_10[:, 1], '-', label=r'N1520')
plt.plot(data_N1520_10_v0[:, 0], data_N1520_10_v0[:, 1], '.', label=r'N1520 v0')
# plt.plot(data_N1440_10[:, 0], data_N1440_10[:, 1], '.', label=r'N1440')
# plt.plot(data_N1535_10[:, 0], data_N1535_10[:, 1], '.', label=r'N1535')
# plt.plot(data_Sum_10[:, 0], data_Sum_10[:, 1], '-', label=r'Sum of amplitudes')


plt.axhline(y=0, color='black', linestyle=':')
plt.axvline(x=0, color='black', linestyle=':')


plt.title(r'$E_\nu = 10 GeV$', fontsize=16)
plt.ylabel(r'$\frac{d\sigma}{d E_\gamma} \, (cm^2 GeV^{-1})$', fontsize=14)
plt.xlabel(r'$E_\gamma (GeV)$', fontsize=14)
# plt.grid()

plt.legend()

plt.show()


# data_Delta_6 = np.loadtxt("../Data/dsdEg_Delta_avg_k0_6GeV_nu_12C.dat")
# data_N1520_6 = np.loadtxt("../Data/dsdEg_N1520_k0_6GeV_nu_12C.dat")
# data_Sum_6 = np.loadtxt("../Data/dsdEg_Sum_k0_6GeV_nu_12C.dat")
#
# plt.figure(2)
#
# plt.plot(data_Delta_6[:, 0], data_Delta_6[:, 1], '+', label=r'$\Delta 1232$')
# plt.plot(data_N1520_6[:, 0], data_N1520_6[:, 1], '.', label=r'N1520')
# plt.plot(data_Sum_6[:, 0], data_Sum_6[:, 1], '-', label=r'Sum of amplitudes')
#
#
# plt.axhline(y=0, color='black', linestyle=':')
# plt.axvline(x=0, color='black', linestyle=':')
#
#
# plt.title(r'$E_\nu = 6 GeV$', fontsize=16)
# plt.ylabel(r'$\frac{d\sigma}{d E_\gamma} \, (cm^2 GeV^{-1})$', fontsize=14)
# plt.xlabel(r'$E_\gamma (GeV)$', fontsize=14)
# # plt.grid()
#
# plt.legend()
#
# plt.show()




# data_Sum = np.loadtxt("../Data/dsdEg_Sum_k0_1GeV_nu_12C.dat")
# data_Sum_vec = np.loadtxt("../Data/dsdEg_Sum_vectorial_k0_1GeV_nu_12C.dat")
# data_Sum_axial = np.loadtxt("../Data/dsdEg_Sum_axial_k0_1GeV_nu_12C.dat")
#
# plt.figure(1)
#
# plt.plot(data_Sum[:, 0], data_Sum[:, 1], '-', label=r'Sum of amplitudes')
# plt.plot(data_Sum_vec[:, 0], data_Sum_vec[:, 1], '.', label=r'Vector')
# plt.plot(data_Sum_axial[:, 0], data_Sum_axial[:, 1], '+', label=r'Axial')
#
# plt.axhline(y=0, color='black', linestyle=':')
# plt.axvline(x=0, color='black', linestyle=':')
#
#
# plt.title(r'$E_\nu = 1 GeV$', fontsize=16)
# plt.ylabel(r'$\frac{d\sigma}{d E_\gamma} \, (cm^2 GeV^{-1})$', fontsize=14)
# plt.xlabel(r'$E_\gamma (GeV)$', fontsize=14)
# # plt.grid()
#
# plt.legend()
#
# plt.show()


# data_Sum = np.loadtxt("../Data/dsdEg_Sum_1_5GeV_k0_10GeV_nu_12C.dat")
# data_Sum_vec = np.loadtxt("../Data/dsdEg_Sum_vectorial_k0_10GeV_nu_12C.dat")
# data_Sum_axial = np.loadtxt("../Data/dsdEg_Sum_axial_k0_10GeV_nu_12C.dat")
#
# plt.figure(1)
#
# plt.plot(data_Sum[:, 0], data_Sum[:, 1], '-', label=r'Sum of amplitudes')
# plt.plot(data_Sum_vec[:, 0], data_Sum_vec[:, 1], '.', label=r'Vector')
# plt.plot(data_Sum_axial[:, 0], data_Sum_axial[:, 1], '+', label=r'Axial')
#
# plt.axhline(y=0, color='black', linestyle=':')
# plt.axvline(x=0, color='black', linestyle=':')
#
#
# plt.title(r'$E_\nu = 10 GeV$', fontsize=16)
# plt.ylabel(r'$\frac{d\sigma}{d E_\gamma} \, (cm^2 GeV^{-1})$', fontsize=14)
# plt.xlabel(r'$E_\gamma (GeV)$', fontsize=14)
# # plt.grid()
#
# plt.legend()

# plt.show()