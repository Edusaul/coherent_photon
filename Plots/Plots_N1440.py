import numpy as np
from matplotlib import pyplot as plt

# data_Delta = np.loadtxt("../Data/dsdEg_Delta_avg_k0_1GeV_nu_12C.dat")
data = np.loadtxt("../Data/dsdEg_N1440_k0_1GeV_nu_12C.dat")
# data_N1535 = np.loadtxt("../Data/dsdEg_N1535_k0_1GeV_nu_12C.dat")
data_nucleon = np.loadtxt("../Data/dsdEg_Nucleon_k0_1GeV_nu_12C.dat")


plt.plot(data[:, 0], data[:, 1], '-b', label='N1440')
# plt.plot(data_N1535[:, 0], data_N1535[:, 1], '-r', label='N1535')
plt.plot(data_nucleon[:, 0], data_nucleon[:, 1], '-g', label='Nucleon')
# plt.plot(data_Delta[:, 0], data_Delta[:, 1], 'r', label='Delta')
plt.axhline(y=0, color='black', linestyle=':')
plt.axvline(x=0, color='black', linestyle=':')

# plt.title(r'$\frac{d\sigma}{d E_\gamma}$', fontsize=16)
plt.ylabel(r'$\frac{d\sigma}{d E_\gamma} \, (cm^2 GeV^{-1})$', fontsize=14)
plt.xlabel(r'$E_\gamma (GeV)$', fontsize=14)
# plt.grid()

plt.legend()

plt.show()


# data = np.loadtxt("../Data/Check_FF_N1440_HAmp.dat")
# plt.figure(0)
#
# plt.plot(data[:, 0], data[:, 1], label=r'N1440 $A_p$')
# plt.plot(data[:, 0], data[:, 2], label=r'N1440 $A_n$')
# plt.plot(data[:, 0], data[:, 3], label=r'N1440 $S_p$')
# plt.plot(data[:, 0], data[:, 4], label=r'N1440 $S_n$')
# plt.axhline(y=0, color='black', linestyle=':')
# plt.axvline(x=0, color='black', linestyle=':')
#
#
# # plt.title(r'$\frac{d\sigma}{d E_\gamma}$', fontsize=16)
# # plt.ylabel(r'$\frac{d\sigma}{d E_\gamma} \, (cm^2 GeV^-1)$', fontsize=14)
# plt.xlabel(r'$Q^2 (GeV^2)$', fontsize=14)
# # plt.grid()
#
# plt.legend()
#
# plt.show()
#
#
# dataFF = np.loadtxt("../Data/Check_FF_N1440_FF.dat")
# plt.figure(1)
#
# plt.plot(dataFF[:, 0], dataFF[:, 1], 'red', label=r'N1440 $F_{p1}$')
# plt.plot(dataFF[:, 0], dataFF[:, 2], 'blue', label=r'N1440 $F_{p2}$')
# plt.plot(dataFF[:, 0], dataFF[:, 3], 'green', label=r'N1440 $F_{n1}$')
# plt.plot(dataFF[:, 0], dataFF[:, 4], 'black', label=r'N1440 $F_{n2}$')
# plt.axhline(y=0, color='black', linestyle=':')
# plt.axvline(x=0, color='black', linestyle=':')
#
#
# # plt.title(r'$\frac{d\sigma}{d E_\gamma}$', fontsize=16)
# # plt.ylabel(r'$\frac{d\sigma}{d E_\gamma} \, (cm^2 GeV^-1)$', fontsize=14)
# plt.xlabel(r'$Q^2 (GeV^2)$', fontsize=14)
# # plt.grid()
#
# plt.legend()
#
# plt.show()
#
#
# dataFF_NC = np.loadtxt("../Data/Check_FF_N1440_FF_NC.dat")
# plt.figure(2)
#
# plt.plot(dataFF_NC[:, 0], dataFF_NC[:, 1], 'red', label=r'N1440 $F_{p1}^{NC}$')
# plt.plot(dataFF_NC[:, 0], dataFF_NC[:, 2], 'blue', label=r'N1440 $F_{p2}^{NC}$')
# plt.plot(dataFF_NC[:, 0], dataFF_NC[:, 3], 'green', label=r'N1440 $F_{n1}^{NC}$')
# plt.plot(dataFF_NC[:, 0], dataFF_NC[:, 4], 'black', label=r'N1440 $F_{n2}^{NC}$')
# plt.axhline(y=0, color='black', linestyle=':')
# plt.axvline(x=0, color='black', linestyle=':')
#
#
# # plt.title(r'$\frac{d\sigma}{d E_\gamma}$', fontsize=16)
# # plt.ylabel(r'$\frac{d\sigma}{d E_\gamma} \, (cm^2 GeV^-1)$', fontsize=14)
# plt.xlabel(r'$Q^2 (GeV^2)$', fontsize=14)
# # plt.grid()
#
# plt.legend()
#
# plt.show()
#
# plt.figure(3)
#
# plt.plot(dataFF_NC[:, 0], dataFF_NC[:, 5], label=r'N1440 $F_A$')
# plt.axhline(y=0, color='black', linestyle=':')
# plt.axvline(x=0, color='black', linestyle=':')
#
#
# # plt.title(r'$\frac{d\sigma}{d E_\gamma}$', fontsize=16)
# # plt.ylabel(r'$\frac{d\sigma}{d E_\gamma} \, (cm^2 GeV^-1)$', fontsize=14)
# plt.xlabel(r'$Q^2 (GeV^2)$', fontsize=14)
# # plt.grid()
#
# plt.legend()
#
# plt.show()