import numpy as np
from matplotlib import pyplot as plt


data = np.loadtxt("../Data/Check_FF_N1520_EM_FF.dat")
plt.figure(0)

plt.plot(data[:, 0], data[:, 1], label=r'N1520 $C3p$')
plt.plot(data[:, 0], data[:, 2], label=r'N1520 $C4p$')
plt.plot(data[:, 0], data[:, 3], label=r'N1520 $C5p$')
plt.plot(data[:, 0], data[:, 4], label=r'N1520 $C3n$')
plt.plot(data[:, 0], data[:, 5], label=r'N1520 $C4n$')
plt.plot(data[:, 0], data[:, 6], label=r'N1520 $C5n$')
plt.axhline(y=0, color='black', linestyle=':')
plt.axvline(x=0, color='black', linestyle=':')


# plt.title(r'$\frac{d\sigma}{d E_\gamma}$', fontsize=16)
# plt.ylabel(r'$\frac{d\sigma}{d E_\gamma} \, (cm^2 GeV^-1)$', fontsize=14)
plt.xlabel(r'$Q^2 (GeV^2)$', fontsize=14)
# plt.grid()

plt.legend()

plt.show()


data = np.loadtxt("../Data/Check_FF_N1520_NC_FF.dat")
plt.figure(1)

plt.plot(data[:, 0], data[:, 1], label=r'N1520 $C3p^{NC}$')
plt.plot(data[:, 0], data[:, 2], label=r'N1520 $C4p^{NC}$')
plt.plot(data[:, 0], data[:, 3], label=r'N1520 $C5p^{NC}$')
plt.plot(data[:, 0], data[:, 4], label=r'N1520 $C3n^{NC}$')
plt.plot(data[:, 0], data[:, 5], label=r'N1520 $C4n^{NC}$')
plt.plot(data[:, 0], data[:, 6], label=r'N1520 $C5n^{NC}$')
plt.axhline(y=0, color='black', linestyle=':')
plt.axvline(x=0, color='black', linestyle=':')


# plt.title(r'$\frac{d\sigma}{d E_\gamma}$', fontsize=16)
# plt.ylabel(r'$\frac{d\sigma}{d E_\gamma} \, (cm^2 GeV^-1)$', fontsize=14)
plt.xlabel(r'$Q^2 (GeV^2)$', fontsize=14)
# plt.grid()

plt.legend()

plt.show()


plt.figure(2)

plt.plot(data[:, 0], data[:, 7], label=r'N1520 $Ca^{NC}$')
plt.axhline(y=0, color='black', linestyle=':')
plt.axvline(x=0, color='black', linestyle=':')


# plt.title(r'$\frac{d\sigma}{d E_\gamma}$', fontsize=16)
# plt.ylabel(r'$\frac{d\sigma}{d E_\gamma} \, (cm^2 GeV^-1)$', fontsize=14)
plt.xlabel(r'$Q^2 (GeV^2)$', fontsize=14)
# plt.grid()

plt.legend()

plt.show()