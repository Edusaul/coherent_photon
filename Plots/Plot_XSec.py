import numpy as np
from matplotlib import pyplot as plt


# For 12C
# data = np.loadtxt("../Data/Cross_Section_Delta_avg_nu_12C.dat")
# data2 = np.loadtxt("/home/eduardo/Genie_test/XSec_2piLess_8GeV.dat")
# plt.figure(0)
#
# plt.plot(data[:, 0], data[:, 1], label=r'External')
# plt.plot(data2[:, 0], data2[:, 1]*(197.33e-16)**2, label=r'GENIE')
# plt.axhline(y=0, color='black', linestyle=':')
# plt.axvline(x=0, color='black', linestyle=':')
#
#
# # plt.title(r'$\frac{d\sigma}{d E_\gamma}$', fontsize=16)
# plt.ylabel(r'$\sigma (E_\nu) \, (cm^2)$', fontsize=14)
# plt.xlabel(r'$E_\nu (GeV)$', fontsize=14)
# # plt.grid()
#
# plt.legend()
#
# plt.show()


# For 40Ar
dataAr = np.loadtxt("../Data/Cross_Section_Delta_avg_nu_40Ar.dat")
dataAr2 = np.loadtxt("/home/eduardo/Genie_test/XSec_40Ar_2piLess_8GeV.dat")
# plt.figure(0)

plt.plot(dataAr[:, 0], dataAr[:, 1], label=r'External')
plt.plot(dataAr2[:, 0], dataAr2[:, 1]*(197.33e-16)**2, label=r'GENIE')
plt.axhline(y=0, color='black', linestyle=':')
plt.axvline(x=0, color='black', linestyle=':')


# plt.title(r'$\frac{d\sigma}{d E_\gamma}$', fontsize=16)
plt.ylabel(r'$\sigma (E_\nu) \, (cm^2)$', fontsize=14)
plt.xlabel(r'$E_\nu (GeV)$', fontsize=14)
# plt.grid()

plt.legend()

plt.show()