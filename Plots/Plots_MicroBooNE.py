import numpy as np
from matplotlib import pyplot as plt

data = np.loadtxt("../Data/MicroBooNE_dsdEg_Delta_avg_nu_40Ar.dat")

plt.figure(0)
plt.plot(data[:, 0], data[:, 1], '-b+', label='Average')

plt.ylabel(r'$\frac{dN}{d E_\gamma} \, [Events / GeV]$', fontsize=14)
plt.xlabel(r'$E_\gamma (GeV)$', fontsize=14)
# plt.grid()

plt.legend()

plt.show()


data2 = np.loadtxt("../Data/MicroBooNE_dsdthg_Delta_avg_nu_40Ar.dat")
plt.figure(1)
plt.plot(data2[:, 0], data2[:, 1], '-b+', label='Average')

plt.ylabel(r'$\frac{dN}{d \theta_\gamma} \, [Events / GeV]$', fontsize=14)
plt.xlabel(r'$\theta_\gamma (GeV)$', fontsize=14)
# plt.grid()

plt.legend()

plt.show()
