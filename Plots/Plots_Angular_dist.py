import numpy as np
from matplotlib import pyplot as plt

# data_Delta = np.loadtxt("../Data/dsdthg_Delta_avg_k0_1GeV_nu_12C.dat")
# data_N1520 = np.loadtxt("../Data/dsdthg_N1520_k0_1GeV_nu_12C.dat")
# data_N1440 = np.loadtxt("../Data/dsdthg_N1440_k0_1GeV_nu_12C.dat")
# data_N1535 = np.loadtxt("../Data/dsdthg_N1535_k0_1GeV_nu_12C.dat")
# data_Sum = np.loadtxt("../Data/dsdthg_Sum_k0_1GeV_nu_12C.dat")
#
#
# plt.figure(0)
#
# # plt.plot(np.cos(data_Delta[:, 0]), data_Delta[:, 1]/np.sin(data_Delta[:, 0]), '.', label=r'$\Delta 1232$')
# # plt.plot(np.cos(data_N1520[:, 0]), data_N1520[:, 1]/np.sin(data_N1520[:, 0]), '.', label=r'$N1520$')
# # plt.plot(np.cos(data_Sum[:, 0]), data_Sum[:, 1]/np.sin(data_Sum[:, 0]), '-', label=r'Total')
# plt.plot(data_Delta[:, 0], data_Delta[:, 1], '.', label=r'$\Delta 1232$')
# plt.plot(data_N1520[:, 0], data_N1520[:, 1], '.', label=r'$N1520$')
# plt.plot(data_Sum[:, 0], data_Sum[:, 1], '-', label=r'Total')
# # plt.plot(data_N1440[:, 0], data_N1440[:, 1],             '-', label=r'N1440')
# # plt.plot(data_N1535[:, 0], data_N1535[:, 1],             '-', label=r'N1535')
# # plt.plot(data_Sum[:, 0], data_Sum[:, 1],             '-', label=r'Total')
# # plt.plot(data_nucleon[:, 0], data_nucleon[:, 1],         '-', label=r'Nucleon')
# # plt.plot(data_Delta[:, 0], data_Delta[:, 1] + data_N1520[:, 1] + data_N1440[:, 1] + data_N1535[:, 1]
# #          + data_nucleon[:, 1], '-', label=r'Total')
#
# plt.axhline(y=0, color='black', linestyle=':')
# plt.axvline(x=0, color='black', linestyle=':')
#
#
# # plt.title(r'$\frac{d\sigma}{d E_\gamma}$', fontsize=16)
# plt.ylabel(r'$\frac{d\sigma}{d \theta_\gamma} \, (cm^2 rad^{-1})$', fontsize=14)
# plt.xlabel(r'$\theta_\gamma$ (rad)', fontsize=14)
# # plt.grid()
#
# plt.legend()
#
# plt.show()


data_Delta = np.loadtxt("../Data/dsdthg_Delta_avg_k0_10GeV_nu_12C.dat")
data_Delta_HP = np.loadtxt("../Data/dsdthg_Delta_avg_k0_10GeV_nu_12C_high_prec.dat")
data_Delta_p10 = np.loadtxt("../Data/dsdthg_Delta_avg_k0_10GeV_nu_12C_prec10.dat")
data_Delta_p100 = np.loadtxt("../Data/dsdthg_Delta_avg_k0_10GeV_nu_12C_prec100.dat")
data_Delta_150 = np.loadtxt("../Data/dsdthg_Delta_avg_k0_10GeV_nu_12C_150pts.dat")


plt.figure(1)

plt.plot(data_Delta[:, 0], data_Delta[:, 1], '.', label=r'$\Delta 1232$')
plt.plot(data_Delta_HP[:, 0], data_Delta_HP[:, 1], ':', label=r'$\Delta 1232 High Prec$')
plt.plot(data_Delta_p10[:, 0], data_Delta_p10[:, 1], '+', label=r'$\Delta 1232$ p 10')
plt.plot(data_Delta_p100[:, 0], data_Delta_p100[:, 1], '-', label=r'$\Delta 1232$ p 100')
plt.plot(data_Delta_150[:, 0], data_Delta_150[:, 1], '-', label=r'$\Delta 1232$')

plt.axhline(y=0, color='black', linestyle=':')
plt.axvline(x=0, color='black', linestyle=':')


plt.title(r'$E_\nu = 10$ GeV', fontsize=16)
plt.ylabel(r'$\frac{d\sigma}{d \theta_\gamma} \, (cm^2 rad^{-1})$', fontsize=14)
plt.xlabel(r'$\theta_\gamma$ (rad)', fontsize=14)
# plt.grid()

plt.legend()

plt.show()