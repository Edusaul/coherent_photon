import numpy as np
from matplotlib import pyplot as plt

data = np.loadtxt("../Data/Check_FF_N1675_HAmp.dat")
plt.figure(0)

plt.plot(data[:, 0], data[:, 1], ':', color='black', label=r'N1520 $A12N$')
plt.plot(data[:, 0], data[:, 2], 'pink', label=r'N1520 $A32N$')
plt.plot(data[:, 0], data[:, 3], 'grey', label=r'N1520 $S12N$')
plt.plot(data[:, 0], data[:, 4], 'red', label=r'N1520 $A12P$')
plt.plot(data[:, 0], data[:, 5], 'blue', label=r'N1520 $A32P$')
plt.plot(data[:, 0], data[:, 6], 'green', label=r'N1520 $S12P$')
plt.axhline(y=0, color='black', linestyle='-')
plt.axvline(x=0, color='black', linestyle='-')


# plt.title(r'$\frac{d\sigma}{d E_\gamma}$', fontsize=16)
plt.ylabel(r'helicity amplitudes $(GeV^{-1/2})$', fontsize=14)
plt.xlabel(r'$Q^2 (GeV^2)$', fontsize=14)
# plt.grid()

plt.legend()

plt.show()


dataFF_EM = np.loadtxt("../Data/Check_FF_N1675_EM_FF.dat")
plt.figure(0)

plt.plot(dataFF_EM[:, 0], dataFF_EM[:, 1], 'red', label=r'N1520 $C3p$')
plt.plot(dataFF_EM[:, 0], dataFF_EM[:, 2], 'blue', label=r'N1520 $C4p$')
plt.plot(dataFF_EM[:, 0], dataFF_EM[:, 3], 'green', label=r'N1520 $C5p$')
plt.plot(dataFF_EM[:, 0], dataFF_EM[:, 4], 'black', label=r'N1520 $C3n$')
plt.plot(dataFF_EM[:, 0], dataFF_EM[:, 5], 'pink', label=r'N1520 $C4n$')
plt.plot(dataFF_EM[:, 0], dataFF_EM[:, 6], 'grey', label=r'N1520 $C5n$')
plt.axhline(y=0, color='black', linestyle=':')
plt.axvline(x=0, color='black', linestyle=':')


# plt.title(r'$\frac{d\sigma}{d E_\gamma}$', fontsize=16)
# plt.ylabel(r'$\frac{d\sigma}{d E_\gamma} \, (cm^2 GeV^-1)$', fontsize=14)
plt.xlabel(r'$Q^2 (GeV^2)$', fontsize=14)
# plt.grid()

plt.legend()

plt.show()


dataFF_NC = np.loadtxt("../Data/Check_FF_N1675_NC_FF.dat")

plt.figure(1)

plt.plot(dataFF_NC[:, 0], dataFF_NC[:, 1], 'red',label=r'N1520 $C3vNC^p$')
plt.plot(dataFF_NC[:, 0], dataFF_NC[:, 2], 'blue',label=r'N1520 $C4vNC^p$')
plt.plot(dataFF_NC[:, 0], dataFF_NC[:, 3], 'green',label=r'N1520 $C5vNC^p$')
plt.plot(dataFF_NC[:, 0], dataFF_NC[:, 4], 'black',label=r'N1520 $C3vNC^n$')
plt.plot(dataFF_NC[:, 0], dataFF_NC[:, 5], 'pink',label=r'N1520 $C4vNC^n$')
plt.plot(dataFF_NC[:, 0], dataFF_NC[:, 6], 'grey',label=r'N1520 $C5vNC^n$')
plt.axhline(y=0, color='black', linestyle=':')
plt.axvline(x=0, color='black', linestyle=':')


# plt.title(r'$\frac{d\sigma}{d E_\gamma}$', fontsize=16)
plt.ylabel(r'Divided by 2', fontsize=14)
plt.xlabel(r'$Q^2 (GeV^2)$', fontsize=14)
# plt.grid()

plt.legend()

plt.show()


plt.figure(2)

plt.plot(dataFF_NC[:, 0], dataFF_NC[:, 7], 'red', label=r'N1520 $C5aNC_p$')
plt.plot(dataFF_NC[:, 0], dataFF_NC[:, 8], 'blue', label=r'N1520 $C5aNC_n$')
plt.axhline(y=0, color='black', linestyle=':')
plt.axvline(x=0, color='black', linestyle=':')


# plt.title(r'$\frac{d\sigma}{d E_\gamma}$', fontsize=16)
# plt.ylabel(r'$\frac{d\sigma}{d E_\gamma} \, (cm^2 GeV^-1)$', fontsize=14)
plt.xlabel(r'$Q^2 (GeV^2)$', fontsize=14)
# plt.grid()

plt.legend()

plt.show()




