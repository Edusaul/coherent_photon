import numpy as np
from matplotlib import pyplot as plt


data_nucleon = np.loadtxt("../Data/dsdEg_Nucleon_k0_1GeV_nu_12C.dat")
data_Delta = np.loadtxt("../Data/dsdEg_Delta_avg_k0_1GeV_nu_12C_150pts.dat")
data_N1440 = np.loadtxt("../Data/dsdEg_N1440_k0_1GeV_nu_12C.dat")
data_N1520 = np.loadtxt("../Data/dsdEg_N1520_k0_1GeV_nu_12C.dat")
data_N1535 = np.loadtxt("../Data/dsdEg_N1535_k0_1GeV_nu_12C.dat")

data_D1620 = np.loadtxt("../Data/dsdEg_D1620_k0_1GeV_nu_12C.dat")
data_N1650 = np.loadtxt("../Data/dsdEg_N1650_k0_1GeV_nu_12C.dat")
data_N1675 = np.loadtxt("../Data/dsdEg_N1675_k0_1GeV_nu_12C.dat")
data_N1680 = np.loadtxt("../Data/dsdEg_N1680_k0_1GeV_nu_12C.dat")
data_D1700 = np.loadtxt("../Data/dsdEg_D1700_k0_1GeV_nu_12C.dat")
data_N1720 = np.loadtxt("../Data/dsdEg_N1720_k0_1GeV_nu_12C.dat")
data_D1905 = np.loadtxt("../Data/dsdEg_D1905_k0_1GeV_nu_12C.dat")
data_D1910 = np.loadtxt("../Data/dsdEg_D1910_k0_1GeV_nu_12C.dat")
data_D1950 = np.loadtxt("../Data/dsdEg_D1950_k0_1GeV_nu_12C.dat")

data_Sum = np.loadtxt("../Data/dsdEg_Sum_k0_1GeV_nu_12C.dat")
data_Sum_2 = np.loadtxt("../Data/dsdEg_Sum_Delta_N1520_D1700_k0_1GeV_nu_12C.dat")


plt.figure(0)

plt.plot(data_nucleon[:, 0], data_nucleon[:, 1], '.', label=r'Nucleon')
plt.plot(data_Delta[:, 0], data_Delta[:, 1], '.', label=r'$\Delta 1232$')
plt.plot(data_N1520[:, 0], data_N1520[:, 1], '.', label=r'$N1520$')
plt.plot(data_N1440[:, 0], data_N1440[:, 1], '.', label=r'$N1440$')
plt.plot(data_N1535[:, 0], data_N1535[:, 1], '.', label=r'$N1535$')

plt.plot(data_D1620[:, 0], data_D1620[:, 1], '.', label=r'$\Delta 1620$')
plt.plot(data_N1650[:, 0], data_N1650[:, 1], '.', label=r'$N1650$')
plt.plot(data_N1675[:, 0], data_N1675[:, 1], '.', label=r'$N1675$')
plt.plot(data_N1680[:, 0], data_N1680[:, 1], '.', label=r'$N1680$')
plt.plot(data_D1700[:, 0], data_D1700[:, 1], '.', label=r'$\Delta 1700$')
plt.plot(data_N1720[:, 0], data_N1720[:, 1], '.', label=r'$N1720$')
plt.plot(data_D1905[:, 0], data_D1905[:, 1], '.', label=r'$\Delta 1905$')
plt.plot(data_D1910[:, 0], data_D1910[:, 1], '.', label=r'$\Delta 1910$')
plt.plot(data_D1950[:, 0], data_D1950[:, 1], '.', label=r'$\Delta 1950$')

# plt.plot(data_Sum[:, 0], data_Sum[:, 1], '-', label=r'Sum of amplitudes')
plt.plot(data_Sum_2[:, 0], data_Sum_2[:, 1], '-', label=r'Sum of amplitudes')

plt.axhline(y=0, color='black', linestyle=':')
plt.axvline(x=0, color='black', linestyle=':')


plt.title(r'$E_\nu = 1$ GeV', fontsize=16)
plt.ylabel(r'$\frac{d\sigma}{d E_\gamma} \, (cm^2 GeV^{-1})$', fontsize=14)
plt.xlabel(r'$E_\gamma (GeV)$', fontsize=14)
# plt.grid()

plt.legend()

plt.show()



# data_10GeV_nucleon = np.loadtxt("../Data/dsdEg_Nucleon_k0_10GeV_nu_12C.dat")
data_10GeV_Delta = np.loadtxt("../Data/dsdEg_Delta_avg_k0_10GeV_nu_12C_150pts.dat")
# data_10GeV_Delta = np.loadtxt("../Data/dsdEg_Delta_avg_k0_10GeV_nu_12C.dat")
data_10GeV_N1440 = np.loadtxt("../Data/dsdEg_N1440_k0_10GeV_nu_12C.dat")
data_10GeV_N1520 = np.loadtxt("../Data/dsdEg_N1520_k0_10GeV_nu_12C.dat")
data_10GeV_N1535 = np.loadtxt("../Data/dsdEg_N1535_k0_10GeV_nu_12C.dat")
data_10GeV_D1620 = np.loadtxt("../Data/dsdEg_D1620_k0_10GeV_nu_12C.dat")
data_10GeV_N1650 = np.loadtxt("../Data/dsdEg_N1650_k0_10GeV_nu_12C.dat")
data_10GeV_N1675 = np.loadtxt("../Data/dsdEg_N1675_k0_10GeV_nu_12C.dat")
data_10GeV_N1680 = np.loadtxt("../Data/dsdEg_N1680_k0_10GeV_nu_12C.dat")
data_10GeV_D1700 = np.loadtxt("../Data/dsdEg_D1700_k0_10GeV_nu_12C.dat")
data_10GeV_N1720 = np.loadtxt("../Data/dsdEg_N1720_k0_10GeV_nu_12C.dat")
data_10GeV_D1905 = np.loadtxt("../Data/dsdEg_D1905_k0_10GeV_nu_12C.dat")
data_10GeV_D1910 = np.loadtxt("../Data/dsdEg_D1910_k0_10GeV_nu_12C.dat")
data_10GeV_D1950 = np.loadtxt("../Data/dsdEg_D1950_k0_10GeV_nu_12C.dat")

data_10GeV_Sum = np.loadtxt("../Data/dsdEg_Sum_k0_10GeV_nu_12C.dat")
data_10GeV_Sum_2 = np.loadtxt("../Data/dsdEg_Sum_Delta_N1520_D1700_k0_10GeV_nu_12C.dat")



plt.figure(1)

# plt.plot(data_10GeV_nucleon[:, 0], data_10GeV_nucleon[:, 1], '.', label=r'Nucleon')
plt.plot(data_10GeV_Delta[:, 0], data_10GeV_Delta[:, 1], '.', label=r'$\Delta 1232$')
plt.plot(data_10GeV_N1520[:, 0], data_10GeV_N1520[:, 1], '.', label=r'$N1520$')
plt.plot(data_10GeV_N1440[:, 0], data_10GeV_N1440[:, 1], '.', label=r'$N1440$')
plt.plot(data_10GeV_N1535[:, 0], data_10GeV_N1535[:, 1], '.', label=r'$N1535$')
plt.plot(data_10GeV_D1620[:, 0], data_10GeV_D1620[:, 1], '.', label=r'$\Delta 1620$')
plt.plot(data_10GeV_N1650[:, 0], data_10GeV_N1650[:, 1], '.', label=r'$N1650$')
plt.plot(data_10GeV_N1675[:, 0], data_10GeV_N1675[:, 1], '.', label=r'$N1675$')
plt.plot(data_10GeV_N1680[:, 0], data_10GeV_N1680[:, 1], '.', label=r'$N1680$')
plt.plot(data_10GeV_D1700[:, 0], data_10GeV_D1700[:, 1], '.', label=r'$\Delta 1700$')
plt.plot(data_10GeV_N1720[:, 0], data_10GeV_N1720[:, 1], '.', label=r'$N1720$')
plt.plot(data_10GeV_D1905[:, 0], data_10GeV_D1905[:, 1], '.', label=r'$\Delta 1905$')
plt.plot(data_10GeV_D1910[:, 0], data_10GeV_D1910[:, 1], '.', label=r'$\Delta 1910$')
plt.plot(data_10GeV_D1950[:, 0], data_10GeV_D1950[:, 1], '.', label=r'$\Delta 1950$')

# plt.plot(data_10GeV_Sum[:, 0], data_10GeV_Sum[:, 1], '-', label=r'Sum of amplitudes')
plt.plot(data_10GeV_Sum_2[:, 0], data_10GeV_Sum_2[:, 1], '-', label=r'Sum of amplitudes')


plt.axhline(y=0, color='black', linestyle=':')
plt.axvline(x=0, color='black', linestyle=':')


plt.title(r'$E_\nu = 10$ GeV', fontsize=16)
plt.ylabel(r'$\frac{d\sigma}{d E_\gamma} \, (cm^2 GeV^{-1})$', fontsize=14)
plt.xlabel(r'$E_\gamma (GeV)$', fontsize=14)
# plt.grid()

plt.legend()

plt.show()