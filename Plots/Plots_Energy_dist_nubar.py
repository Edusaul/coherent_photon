import numpy as np
from matplotlib import pyplot as plt

data_Delta = np.loadtxt("../Data/dsdEg_Delta_avg_k0_1GeV_nubar_12C.dat")
data_N1520 = np.loadtxt("../Data/dsdEg_N1520_k0_1GeV_nubar_12C.dat")
data_N1440 = np.loadtxt("../Data/dsdEg_N1440_k0_1GeV_nubar_12C.dat")
data_N1535 = np.loadtxt("../Data/dsdEg_N1535_k0_1GeV_nubar_12C.dat")

plt.figure(0)

# plt.plot(data_Delta[:, 0], data_Delta[:, 1], '.', label=r'$\Delta 1232$')
plt.plot(data_N1520[:, 0], data_N1520[:, 1],             '-', label=r'N1520')
plt.plot(data_N1440[:, 0], data_N1440[:, 1],             '-', label=r'N1440')
plt.plot(data_N1535[:, 0], data_N1535[:, 1],             '-', label=r'N1535')

plt.axhline(y=0, color='black', linestyle=':')
plt.axvline(x=0, color='black', linestyle=':')


# plt.title(r'$\frac{d\sigma}{d E_\gamma}$', fontsize=16)
plt.ylabel(r'$\frac{d\sigma}{d E_\gamma} \, (cm^2 GeV^{-1})$', fontsize=14)
plt.xlabel(r'$E_\gamma (GeV)$', fontsize=14)
# plt.grid()

plt.legend()


plt.show()

data_Delta_nu = np.loadtxt("../Data/dsdEg_Delta_avg_k0_1GeV_nu_12C_l1l2l3.dat")
data_N1520_nu = np.loadtxt("../Data/dsdEg_N1520_k0_1GeV_nu_12C.dat")
data_N1440_nu = np.loadtxt("../Data/dsdEg_N1440_k0_1GeV_nu_12C.dat")
data_N1535_nu = np.loadtxt("../Data/dsdEg_N1535_k0_1GeV_nu_12C.dat")
data_nucleon_nu = np.loadtxt("../Data/dsdEg_Nucleon_k0_1GeV_nu_12C.dat")

plt.figure(2)

# plt.plot(data_Delta[:, 0], data_Delta[:, 1], '.', label=r'$\Delta 1232$')
plt.plot(data_N1520[:, 0], data_N1520[:, 1],             '-', label=r'N1520 $\overline{\nu}$')
plt.plot(data_N1440[:, 0], data_N1440[:, 1],             '-', label=r'N1440 $\overline{\nu}$')
plt.plot(data_N1535[:, 0], data_N1535[:, 1],             '-', label=r'N1535 $\overline{\nu}$')
plt.plot(data_N1520_nu[:, 0], data_N1520_nu[:, 1],             '.', label=r'N1520 $\nu$')
plt.plot(data_N1440_nu[:, 0], data_N1440_nu[:, 1],             '.', label=r'N1440 $\nu$')
plt.plot(data_N1535_nu[:, 0], data_N1535_nu[:, 1],             '.', label=r'N1535 $\nu$')

plt.axhline(y=0, color='black', linestyle=':')
plt.axvline(x=0, color='black', linestyle=':')


# plt.title(r'$\frac{d\sigma}{d E_\gamma}$', fontsize=16)
plt.ylabel(r'$\frac{d\sigma}{d E_\gamma} \, (cm^2 GeV^{-1})$', fontsize=14)
plt.xlabel(r'$E_\gamma (GeV)$', fontsize=14)
# plt.grid()

plt.legend()

plt.show()


data_Sum = np.loadtxt("../Data/dsdEg_Sum_k0_1GeV_nubar_12C.dat")

plt.figure(1)

plt.plot(data_Delta[:, 0], data_Delta[:, 1], '+', label=r'$\Delta 1232$')
plt.plot(data_N1520[:, 0], data_N1520[:, 1], '.', label=r'N1520')
plt.plot(data_Sum[:, 0], data_Sum[:, 1], '-', label=r'Sum of amplitudes')

plt.axhline(y=0, color='black', linestyle=':')
plt.axvline(x=0, color='black', linestyle=':')


plt.title(r'$E_\nu = 1 GeV$', fontsize=16)
plt.ylabel(r'$\frac{d\sigma}{d E_\gamma} \, (cm^2 GeV^{-1})$', fontsize=14)
plt.xlabel(r'$E_\gamma (GeV)$', fontsize=14)
# plt.grid()

plt.legend()

plt.show()


