//
// Created by eduardo on 8/07/19.
//

#include <Measure_time.h>
#include <Form_Factors_N1675.h>
#include <vector>
#include <Write_static.h>

int main(int argc, char** argv) {
    Measure_time time;

    Form_Factors_N1675 ff;

    std::vector<std::vector<double>> data_HAmp(7);
    std::vector<std::vector<double>> data_FF_EM(7);
    std::vector<std::vector<double>> data_FF_NC(9);
    double Q2 = 0.0;
    int N = 100;
    for (int i = 0; i < N; ++i) {

        ff.setFF(Q2);

        data_HAmp[0].push_back(Q2);
        data_HAmp[1].push_back(ff.getA12N());
        data_HAmp[2].push_back(ff.getA32N());
        data_HAmp[3].push_back(ff.getS12N());
        data_HAmp[4].push_back(ff.getA12P());
        data_HAmp[5].push_back(ff.getA32P());
        data_HAmp[6].push_back(ff.getS12P());

        data_FF_EM[0].push_back(Q2);
        data_FF_EM[1].push_back(ff.getC3P());
        data_FF_EM[2].push_back(ff.getC4P());
        data_FF_EM[3].push_back(ff.getC5P());
        data_FF_EM[4].push_back(ff.getC3N());
        data_FF_EM[5].push_back(ff.getC4N());
        data_FF_EM[6].push_back(ff.getC5N());

        data_FF_NC[0].push_back(Q2);
        data_FF_NC[1].push_back(ff.getC3VNcP());
        data_FF_NC[2].push_back(ff.getC4VNcP());
        data_FF_NC[3].push_back(ff.getC5VNcP());
        data_FF_NC[4].push_back(ff.getC3VNcN());
        data_FF_NC[5].push_back(ff.getC4VNcN());
        data_FF_NC[6].push_back(ff.getC5VNcN());

        data_FF_NC[7].push_back(ff.getC5ANcP());
        data_FF_NC[8].push_back(ff.getC5ANcN());

//        std::cout<<Q2<<"  "<<data_HAmp[1][i]<<"  "<<data_HAmp[2][i]<<"  "<<data_HAmp[3][i]<<"  "<<data_HAmp[4][i]<<std::endl;

        Q2 += 2.0/double(N-1);
    }

    Write_static::Write_to_file("../../Data/Check_FF_N1675_HAmp.dat", data_HAmp);
    Write_static::Write_to_file("../../Data/Check_FF_N1675_EM_FF.dat", data_FF_EM);
    Write_static::Write_to_file("../../Data/Check_FF_N1675_NC_FF.dat", data_FF_NC);

    time.getTime();
    return 0;
}