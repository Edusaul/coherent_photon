//
// Created by edusaul on 6/04/19.
//

#include <Parameters_GeV.h>
#include <IntegralGauss20.h>
#include "Density_integral.h"

Density_integral::Density_integral() {
    this->nucleus = "12C";
    this->rho = new density_profile(this->nucleus);
}

Density_integral::~Density_integral() {
    delete this->rho;
}

std::complex<double> Density_integral::integrand(double r) {
    std::complex<double> rho_r = this->rho->get_rhop_cent(r) *  Param::hc * Param::hc * Param::hc;
    return (r/Param::hc * sin(this->p * r/Param::hc) * rho_r * 4.0*Param::pi/this->p)/Param::hc;

//    std::complex<double> rho_r = this->rho->get_rhop_cent(r);
//    return r * sin(this->p * r/Param::hc) * rho_r * 4.0*Param::pi/this->p * Param::hc;

}

void Density_integral::change_other_parameters(std::vector<std::complex<double>> param) {
    this->p = param[0].real();
}

double Density_integral::getRhoMax() {
    return this->rho->getRmax();
}

std::complex<double> Density_integral::getFF(double p) {
    this->p = p;

    return IntegralGauss20::integrate<std::complex<double>>(this, 0.0, this->rho->getRmax(), 3);
}

