//
// Created by eduardo on 11/07/19.
//

#include <Measure_time.h>
#include <Write_static.h>
#include <Form_Factors_D1700.h>

int main(int argc, char** argv) {
    Measure_time time;

    Form_Factors_D1700 ff;

    std::vector<std::vector<double>> data_HAmp(4);
    std::vector<std::vector<double>> data_FF_EM(4);
    std::vector<std::vector<double>> data_FF_NC(5);
    double Q2 = 0.0;
    int N = 100;
    for (int i = 0; i < N; ++i) {

        ff.setFF(Q2);

        data_HAmp[0].push_back(Q2);
        data_HAmp[1].push_back(ff.getA12P());
        data_HAmp[2].push_back(ff.getA32P());
        data_HAmp[3].push_back(ff.getS12P());


        data_FF_EM[0].push_back(Q2);
        data_FF_EM[1].push_back(ff.getC3Pn());
        data_FF_EM[2].push_back(ff.getC4Pn());
        data_FF_EM[3].push_back(ff.getC5Pn());


        data_FF_NC[0].push_back(Q2);
        data_FF_NC[1].push_back(ff.getC3VNcPn());
        data_FF_NC[2].push_back(ff.getC4VNcPn());
        data_FF_NC[3].push_back(ff.getC5VNcPn());


        data_FF_NC[4].push_back(ff.getC5ANcPn());

//        std::cout<<Q2<<"  "<<data_HAmp[1][i]<<"  "<<data_HAmp[2][i]<<"  "<<data_HAmp[3][i]<<"  "<<data_HAmp[4][i]<<std::endl;

        Q2 += 2.0/double(N-1);
    }

    Write_static::Write_to_file("../../Data/Check_FF_N1520_HAmp.dat", data_HAmp);
    Write_static::Write_to_file("../../Data/Check_FF_N1520_EM_FF.dat", data_FF_EM);
    Write_static::Write_to_file("../../Data/Check_FF_N1520_NC_FF.dat", data_FF_NC);

    time.getTime();
    return 0;
}