//
// Created by eduardo on 14/06/19.
//

#include <Measure_time.h>
#include <complex>
#include <vector>
#include <Write_static.h>
#include <iostream>
#include <Form_Factors_N1535.h>

int main(int argc, char** argv) {
    Measure_time time;

    Form_Factors_N1535 ff;

    std::vector<std::vector<double>> data_HAmp(5);
    std::vector<std::vector<double>> data_FF(5);
    std::vector<std::vector<double>> data_FF_NC(6);
    double Q2 = 0.0;
    int N = 100;
    for (int i = 0; i < N; ++i) {

        ff.setFF(Q2);

        data_HAmp[0].push_back(Q2);
        data_HAmp[1].push_back(ff.getAP());
        data_HAmp[2].push_back(ff.getAN());
        data_HAmp[3].push_back(ff.getSP());
        data_HAmp[4].push_back(ff.getSN());


        data_FF[0].push_back(Q2);
        data_FF[1].push_back(ff.getF1P());
        data_FF[2].push_back(ff.getF2P());
        data_FF[3].push_back(ff.getF1N());
        data_FF[4].push_back(ff.getF2N());

        data_FF_NC[0].push_back(Q2);
        data_FF_NC[1].push_back(ff.getF1Zp());
        data_FF_NC[2].push_back(ff.getF2Zp());
        data_FF_NC[3].push_back(ff.getF1Zn());
        data_FF_NC[4].push_back(ff.getF2Zn());

        data_FF_NC[5].push_back(ff.getFaZp());

//        std::cout<<Q2<<"  "<<data_HAmp[1][i]<<"  "<<data_HAmp[2][i]<<"  "<<data_HAmp[3][i]<<"  "<<data_HAmp[4][i]<<std::endl;

        Q2 += 2.0/double(N-1);
    }

    Write_static::Write_to_file("../../Data/Check_FF_N1535_HAmp.dat", data_HAmp);
    Write_static::Write_to_file("../../Data/Check_FF_N1535_FF.dat", data_FF);
    Write_static::Write_to_file("../../Data/Check_FF_N1535_FF_NC.dat", data_FF_NC);

    time.getTime();
    return 0;
}