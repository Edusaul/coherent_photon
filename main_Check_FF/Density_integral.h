//
// Created by edusaul on 6/04/19.
//

#ifndef COHERENT_PHOTON_DENSITY_INTEGRAL_H
#define COHERENT_PHOTON_DENSITY_INTEGRAL_H

#include <Integrable_w_change_param.h>
#include <complex>
#include <density_profile.h>

class Density_integral : public Integrable_w_change_param<std::complex<double>> {
protected:
    std::string nucleus;
    density_profile *rho;
    double p;

public:

    Density_integral();

    virtual ~Density_integral();

    std::complex<double> integrand(double) override;

    void change_other_parameters(std::vector<std::complex<double>> param) override;

    double getRhoMax();

    std::complex<double> getFF(double);

};


#endif //COHERENT_PHOTON_DENSITY_INTEGRAL_H
