//
// Created by edusaul on 26/03/19.
//

#include <Measure_time.h>
#include <Nucleus_FF_DeVries.h>
#include <Nuclear_FF_HO.h>
#include <iostream>
#include <vector>
#include <Write_static.h>
#include <IntegralGauss20.h>
#include "Density_integral.h"

int main(int argc, char** argv) {
    Measure_time time;

    Nucleus_FF_DeVries ff_deVries("12C");
    Nuclear_FF_HO ff_ho;
    Density_integral density;

    std::vector<std::vector<double>> data(4);
    std::vector<std::vector<double>> data_squared(4);
    double p = 0.001;
    int N = 100;
    std::complex<double> deV;
    std::complex<double> ho;
    std::complex<double> rhoInt;
    for (int i = 0; i < N; ++i) {
        ff_deVries.setFF(p);
        ff_ho.setFF(p);
        deV = ff_deVries.getFfP();
        ho = ff_ho.getFfP();
        rhoInt = density.getFF(p);

        data[0].push_back(p);
        data[1].push_back(deV.real());
        data[2].push_back(ho.real());
        data[3].push_back(rhoInt.real());

        data_squared[0].push_back(p);
        data_squared[1].push_back(deV.real()*deV.real());
        data_squared[2].push_back(ho.real()*ho.real());
        data_squared[2].push_back(rhoInt.real()*rhoInt.real());

        std::cout<<p<<"  "<<data[1][i]<<"  "<<data[2][i]<<"  "<<data[3][i]<<"   "<<data[2][i]/data[3][i]<<std::endl;

        p += .5/double(N-1);
    }

        Write_static::Write_to_file("../../Data/Check_FF.dat", data);
        Write_static::Write_to_file("../../Data/Check_FF_squared.dat", data_squared);

    time.getTime();
    return 0;
}