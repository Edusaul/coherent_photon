//
// Created by eduardo on 5/07/19.
//

#ifndef COHERENT_PHOTON_ADMIN_OF_CLASES_N1675_H
#define COHERENT_PHOTON_ADMIN_OF_CLASES_N1675_H


#include <Nuclear_FF.h>
#include <Admin_of_Clases_base.h>

class Admin_of_Clases_N1675: public Admin_of_Clases_base {
protected:
    Nuclear_FF *nuclearFF;

public:
    explicit Admin_of_Clases_N1675(Arguments_base *parameters);

    ~Admin_of_Clases_N1675() override;
};


#endif //COHERENT_PHOTON_ADMIN_OF_CLASES_N1675_H
