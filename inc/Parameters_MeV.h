#ifndef _PARAMETERS_H_
#define _PARAMETERS_H_
#include <cmath>

namespace Param
{


    const double pi= 4.0*atan(1.0);
    const double alpha= 1.0/137.0;
    const double hc=197.33; // (MeV x fm)
    const double hccm=197.33e-13; // (MeV x cm)
    const double hbar=6.58212e-22; // (MeV x s)
    const double Gf=1.16639e-11; // MeV^-2
    const double cosc=0.975;       // Cabbibo angle
    const double cosw2=0.23122;    // Weak angle squared
    const double sinw2=0.2223;
    const double cvacuum=3.0e8; //(m/s) light speed in vacuum
    const double cvacuumcm=3.0e10; //(cm/s) light speed in vacuum
    const double NA=6.022e23; //Avogadro's number
    const double Vus=0.2252; //matrix element of CKM matrix
    const double ga=1.2723; //from the PDG value for gA/gV (measured in beta decay: http://pdglive.lbl.gov/DataBlock.action?node=S017AV&init=0) using gV=1 as expected.
    const double gas=0.15;
    const double F_chiPT=0.463;
    const double D_chiPT=0.804;
    const double MA = 1049.0; // MeV Axial mass

    //nuclear matter density (in fm^-3 * hc^3)
    const double rho0=0.17 * hc*hc*hc;

    //lepton mases
    const double me= 0.51099892; //MeV
    const double mmu= 105.6583715; //MeV

    //messon mass
    const double mpi=139.57; //MeV
    const double mk=493.677; //MeV
    const double m_eta=547.83; //MeV

    //form factor
    const double fk=113.0; //MeV kaon decay constant
    const double fpi=92.4; //MeV pion decay constant

    //nucleon
    const double mp= 938.27203; //MeV
    const double mn= 938.27203; //MeV
    const double mup = 1.7928; //proton anomalous magnetic moment
    const double mun = -1.913; //neutron anomalous magnetic moment
        //for the form factors:
    const double lambdanN=5.6;
    const double MD = 843.0; // MeV

    //barion mass
    const double mLambda_1115= 1115.; //MeV
    const double mSigma_1190= 1197.449; //MeV
    const double mDelta= 1232.0; //MeV

    const double coupling_DeltaNpi= 2.14;
    const double N_Delta_MA= 930.0; //MeV
    const double Delta_V0= 80.0; //MeV

    //Carbon nucleus
    const double AC= 12.0;
    const double ZC= 6.0;
    const double MC= 12.0*mp;

    //Argon nucleus
    const double AAr= 40.0;
    const double ZAr= 18.0;
    const double MAr= 40.0*mp;

    //Powers

    const double hccm2= hccm*hccm;
    const double Gf2=Gf*Gf;
    const double mpi2=mpi*mpi;
    const double mp2=mp*mp;
    const double mk2=mk*mk;
    const double m_eta2=m_eta*m_eta;
    const double MA2 = MA*MA; // axial mass squared

    //Delta FF parameters
    const double parameter_001 = 0.01e-6; //MeV^-2
    const double parameter_023 = 0.23e-6; //MeV^-2
    const double parameter_071 = 0.71e6; //MeV^2
    const double parameter_093 = 0.93e6; //MeV^2
    const double parameter_03 = 0.3/sqrt(1.0e3); //MeV^(-1/2)

}

#endif
