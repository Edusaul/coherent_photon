#ifndef _PARAMETERS_H_
#define _PARAMETERS_H_
#include <cmath>

namespace Param
{


    const double pi= 4.0*atan(1.0);
    const double alpha= 1.0/137.0;
    const double hc=0.19733; // (GeV x fm)
    const double hccm=197.33e-16; // (GeV x cm)
    const double hbar=6.58212e-25; // (GeV x s)
    const double Gf=1.16639e-5; // GeV^-2
    const double cosc=0.975;       // Cabbibo angle
    const double sinw2=0.23153;  // Weak angle squared
    const double cvacuum=3.0e8; //(m/s) light speed in vacuum
    const double cvacuumcm=3.0e10; //(cm/s) light speed in vacuum
    const double NA=6.022e23; //Avogadro's number
    const double Vus=0.2252; //matrix element of CKM matrix
    const double ga=1.2723; //from the PDG value for gA/gV (measured in beta decay: http://pdglive.lbl.gov/DataBlock.action?node=S017AV&init=0) using gV=1 as expected.
    const double gas=0.15;
    const double F_chiPT=0.463;
    const double D_chiPT=0.804;
    const double MA = 1.049; // GeV Axial mass

    //nuclear matter density (in fm^-3 * hc^3)
    const double rho0=0.17 * hc*hc*hc;

    //lepton mases
    const double me= 0.51099892e-3; //GeV
    const double mmu= 0.1056583715; //GeV

    //messon mass
    const double mpi=0.13957; //GeV
    const double mk=0.493677; //GeV
    const double m_eta=0.54783; //GeV

    //form factor
    const double fk=0.113; //GeV kaon decay constant
    const double fpi=0.0924; //GeV pion decay constant

    //nucleon
    const double mp= 0.93827203; //GeV
    const double mn= 0.93827203; //GeV
    const double mup = 1.7928; //proton anomalous magnetic moment
    const double mun = -1.913; //neutron anomalous magnetic moment
        //for the form factors:
    const double lambdanN=5.6;
    const double MD = 0.843; // GeV

    //barion mass
    const double mLambda_1115= 1.115; //GeV
    const double mSigma_1190= 1.197449; //GeV
    const double mDelta= 1.232; //GeV
    const double mN1440= 1.462; //GeV
    const double mN1520= 1.524; //GeV
    const double mN1535= 1.534; //GeV
    const double mD1620= 1.672; //GeV
    const double mN1650= 1.659; //GeV
    const double mN1675= 1.676; //GeV
    const double mN1680= 1.684; //GeV
    const double mD1700= 1.762; //GeV
    const double mN1720= 1.717; //GeV
    const double mD1905= 1.881; //GeV
    const double mD1910= 1.882; //GeV
    const double mD1950= 1.945; //GeV

    const double coupling_DeltaNpi= 2.14;
    const double N_Delta_MA= 0.93; //GeV
    const double Delta_V0= 0.08; //GeV
    const double Gamma_N1440 = 0.391; //GeV
    const double Gamma_N1520 = 0.124; //GeV
    const double Gamma_N1535 = 0.151; //GeV
    const double Gamma_D1620 = 0.154; //GeV
    const double Gamma_N1650 = 0.173; //GeV
    const double Gamma_N1675 = 0.159; //GeV
    const double Gamma_N1680 = 0.139; //GeV
    const double Gamma_D1700 = 0.599; //GeV
    const double Gamma_N1720 = 0.383; //GeV
    const double Gamma_D1905 = 0.327; //GeV
    const double Gamma_D1910 = 0.239; //GeV
    const double Gamma_D1950 = 0.300; //GeV

    const double branching_pi_N_N1440 = 0.69;
    const double branching_pi_N_N1520 = 0.59;
    const double branching_pi_N_N1535 = 0.51;
    const double branching_pi_N_D1620 = 0.09;
    const double branching_pi_N_N1650 = 0.89;
    const double branching_pi_N_N1675 = 0.47;
    const double branching_pi_N_N1680 = 0.70;
    const double branching_pi_N_D1700 = 0.14;
    const double branching_pi_N_N1720 = 0.13;
    const double branching_pi_N_D1905 = 0.12;
    const double branching_pi_N_D1910 = 0.23;
    const double branching_pi_N_D1950 = 0.38;

    const double MA_R= 1.0; //GeV Axial mass for resonances dipole

    //Carbon nucleus
    const double AC= 12.0;
    const double ZC= 6.0;
    const double MC= 12.0*mp;

    //Argon nucleus
    const double AAr= 40.0;
    const double ZAr= 18.0;
    const double MAr= 40.0*mp;

    //Powers

    const double hccm2= hccm*hccm;
    const double Gf2=Gf*Gf;
    const double mpi2=mpi*mpi;
    const double mp2=mp*mp;
    const double mk2=mk*mk;
    const double m_eta2=m_eta*m_eta;
    const double MA2 = MA*MA; // axial mass squared
    const double MA_R_2= MA_R*MA_R; //GeV

    //Delta FF parameters
    const double parameter_001 = 0.01; //GeV^-2
    const double parameter_023 = 0.23; //GeV^-2
    const double parameter_071 = 0.71; //GeV^2
    const double parameter_093 = 0.93; //GeV^2
    const double parameter_03 = 0.3; //GeV^(-1/2)

    //N1440 FF parameters
//    const double Fa0_N1440 = -0.47;

    //N1520 FF parameters
//    const double c5a_0_N1520 = -2.15; //GeV^-2
    const double MA_c5a = 1.0; //GeV
//    const double units = 1.0; //GeV

//    //N1535 FF parameters
//    const double Fa0_N1535 = -0.23;

//    //other Delta and N FF parameters
//    const double Fa0_D1620 = 0.05;
//    const double Fa0_N1650 = -0.25;
//    const double c5a_0_N1675 = -1.38;
//    const double c5a_0_N1680 = -0.43;
//    const double c5a_0_D1700 = 0.84;
//    const double c5a_0_N1720 = -0.29;
//    const double c5a_0_D1905 = 0.15;
//    const double Fa0_D1910 = 0.08;
//    const double c5a_0_D1950 = 0.08;



}

#endif
