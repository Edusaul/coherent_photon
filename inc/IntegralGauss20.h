//
// Created by edusaul on 3/07/18.
//

#ifndef LIBRARIES_INTEGRALGAUSS20_H
#define LIBRARIES_INTEGRALGAUSS20_H

#include <complex>
#include "Integrable.h"


class IntegralGauss20{
protected:
    static const double g20[10];
    static const double w20[10];
public:
    template <typename T>
    static T integrate (Integrable<T>* , double, double, int n = 1);
};



template <typename T>
T IntegralGauss20::integrate (Integrable<T>* integrable, double x1, double x2, int n) {

    T res=0;
    double A=(x2-x1)/2.0/double(n);
    for (int i=0;i<n;i++) {
        double B=(x1+x2)/2.0+(double(2*i+1-n))*A;
        for (int j=0; j<10; j++){
            res+=(integrable->integrand(B+A*g20[j])+integrable->integrand(B-A*g20[j]))*w20[j];
        }
    }
    return res*A;
}


#endif //LIBRARIES_INTEGRALGAUSS20_H
