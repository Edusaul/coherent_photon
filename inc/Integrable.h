//
// Created by edusaul on 3/07/18.
//

#ifndef LIBRARIES_INTEGRABLE_H
#define LIBRARIES_INTEGRABLE_H

template <typename T>
class Integrable {
public:
    virtual T integrand (double)= 0;
};

#endif //LIBRARIES_INTEGRABLE_H
