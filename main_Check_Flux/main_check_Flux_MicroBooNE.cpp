//
// Created by edusaul on 17/04/19.
//

#include <Measure_time.h>
#include <Flux_MicroBooNE.h>
#include <iostream>

int main(int argc, char** argv) {
    Measure_time time;

    Flux_MicroBooNE flx("default",0,0.0,6.6e20);

    double enu = 0.0;
    for (int i = 0; i < 55; ++i) {
        std::cout<<enu<<"  "<<flx.integrand(enu)<<std::endl;
        enu += 0.05;
    }

    time.getTime();
    return 0;
}