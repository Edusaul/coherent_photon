//
// Created by eduardo on 5/07/19.
//

#ifndef COHERENT_PHOTON_ADMIN_OF_CLASES_D1950_H
#define COHERENT_PHOTON_ADMIN_OF_CLASES_D1950_H


#include <Nuclear_FF.h>
#include <Admin_of_Clases_base.h>

class Admin_of_Clases_D1950 : public Admin_of_Clases_base {
protected:
    Nuclear_FF *nuclearFF;

public:
    explicit Admin_of_Clases_D1950(Arguments_base *parameters);

    ~Admin_of_Clases_D1950() override;
};


#endif //COHERENT_PHOTON_ADMIN_OF_CLASES_D1950_H
